--
-- PostgreSQL database dump
--

-- Dumped from database version 9.1.11
-- Dumped by pg_dump version 9.1.9
-- Started on 2015-03-21 10:10:40 VET

SET statement_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

--
-- TOC entry 6 (class 2615 OID 283841)
-- Name: acl; Type: SCHEMA; Schema: -; Owner: postgres
--

CREATE SCHEMA acl;


ALTER SCHEMA acl OWNER TO postgres;

--
-- TOC entry 225 (class 3079 OID 11646)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2330 (class 0 OID 0)
-- Dependencies: 225
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = acl, pg_catalog;

--
-- TOC entry 237 (class 1255 OID 285001)
-- Dependencies: 6 690
-- Name: add_operation_to_admin(); Type: FUNCTION; Schema: acl; Owner: postgres
--

CREATE FUNCTION add_operation_to_admin() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
  BEGIN
   INSERT INTO acl.group_operation(id_operation, id_group) VALUES (NEW.id, 1);
   RETURN NEW;
  END;
$$;


ALTER FUNCTION acl.add_operation_to_admin() OWNER TO postgres;

SET search_path = public, pg_catalog;

--
-- TOC entry 238 (class 1255 OID 285307)
-- Dependencies: 7 690
-- Name: ft_actualizar_historico_precio(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION ft_actualizar_historico_precio() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
    BEGIN

        INSERT INTO productos_historico_precio (
            producto_id,
            precio_anterior,
            precio_nuevo,
            usuario_id,
            modificado_en
        )
        VALUES (
            NEW.id,
            OLD.precio,
            NEW.precio,
            NEW.usuario_id,
            NOW()
        );

        RETURN NEW;
    END;
$$;


ALTER FUNCTION public.ft_actualizar_historico_precio() OWNER TO postgres;

--
-- TOC entry 2331 (class 0 OID 0)
-- Dependencies: 238
-- Name: FUNCTION ft_actualizar_historico_precio(); Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON FUNCTION ft_actualizar_historico_precio() IS '
Inserta un registro por cada actualización que se realiza en el precio de un producto.
Mantiene en el histórico el precio anterior, el precio nuevo, fecha de cambio y quien lo realizó.

Autor: Darwin Serrano
Version: V1.0 23/01/2015
';


--
-- TOC entry 239 (class 1255 OID 334416)
-- Dependencies: 690 7
-- Name: ft_auditoria(); Type: FUNCTION; Schema: public; Owner: postgres
--

CREATE FUNCTION ft_auditoria() RETURNS trigger
    LANGUAGE plpgsql
    AS $_$
DECLARE
	ri 			RECORD;
	t 			TEXT;
	sql_auditoria		TEXT;
	old_data		TEXT := '';
	new_data		TEXT := '';
	usuario_id		INTEGER;
	auditoria_id		BIGINT;
	registro_id     BIGINT;
BEGIN
	IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
		usuario_id := NEW.usuario_id;
		registro_id := NEW.id;
		FOR ri IN
			SELECT ordinal_position, column_name, data_type
			FROM information_schema.columns
			WHERE table_schema = quote_ident(TG_TABLE_SCHEMA)
			AND table_name = quote_ident(TG_TABLE_NAME)
			ORDER BY ordinal_position
		LOOP
			EXECUTE 'SELECT ($1).' || ri.column_name || '::text' INTO t USING NEW;
			IF (new_data != '') THEN
				new_data := new_data::text || ', ' ;
			END IF;
			new_data := new_data::text || '"'|| ri.column_name::text ||'": "'|| COALESCE(t::text, '') ||'"';
		END LOOP;
	END IF;

	IF TG_OP = 'DELETE' OR TG_OP = 'UPDATE' THEN
		IF TG_OP = 'DELETE' THEN
			usuario_id := 0;
			registro_id := OLD.id;
		END IF;

		FOR ri IN
			SELECT ordinal_position, column_name, data_type
			FROM information_schema.columns
			WHERE table_schema = quote_ident(TG_TABLE_SCHEMA)
			AND table_name = quote_ident(TG_TABLE_NAME)
			ORDER BY ordinal_position
		LOOP
			EXECUTE 'SELECT ($1).' || ri.column_name || '::text' INTO t USING OLD;
			IF old_data != '' THEN
				old_data := old_data::text || ', ' ;
			END IF;
			old_data := old_data::text || '"'|| ri.column_name::text ||'": "'|| COALESCE(t::text, '') ||'"';
		END LOOP;
	END IF;


	--raise notice 'new_data = %', new_data;
	new_data := '{'|| new_data ||'}';
	old_data := '{'|| old_data ||'}';

	-- Por problemas con framework Codeigniter, la tabla de auditoria no maneja una secuencia para el id
	-- ya que Codeigniter pide LASTVAL en el método insert_id y al disparar un insert dentro de un trigger
	-- se reemplaza el valor de LASTVAL y es erroneo el id dado por Codeigniter.

	-- En este punto se construye el id para la tabla auditorias
	-- SELECT COALESCE(MAX(id), 0) INTO auditoria_id FROM auditorias;

	INSERT INTO auditorias(registro_id, usuario_id, esquema, tabla, fecha, transaccion, datos_previos, datos_actuales)
		VALUES (registro_id, usuario_id, TG_TABLE_SCHEMA, TG_TABLE_NAME, NOW(), TG_OP, old_data, new_data);

    IF TG_OP = 'DELETE' THEN
      RETURN OLD;
    END IF;
    RETURN NEW;
END;
$_$;


ALTER FUNCTION public.ft_auditoria() OWNER TO postgres;

SET search_path = acl, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 204 (class 1259 OID 284196)
-- Dependencies: 2102 2103 2104 6
-- Name: ci_sessions; Type: TABLE; Schema: acl; Owner: postgres; Tablespace: 
--

CREATE TABLE ci_sessions (
    session_id character varying(40) DEFAULT '0'::character varying NOT NULL,
    ip_address character varying(45) DEFAULT '0'::character varying NOT NULL,
    user_agent character varying(120) NOT NULL,
    last_activity integer DEFAULT 0 NOT NULL,
    user_data text NOT NULL
);


ALTER TABLE acl.ci_sessions OWNER TO postgres;

--
-- TOC entry 207 (class 1259 OID 284273)
-- Dependencies: 2114 6
-- Name: group_operation; Type: TABLE; Schema: acl; Owner: postgres; Tablespace: 
--

CREATE TABLE group_operation (
    _date date DEFAULT now(),
    id_operation integer NOT NULL,
    id_group integer NOT NULL
);


ALTER TABLE acl.group_operation OWNER TO postgres;

--
-- TOC entry 2332 (class 0 OID 0)
-- Dependencies: 207
-- Name: TABLE group_operation; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON TABLE group_operation IS 'Entidad que contiene las operaciones asignadas a los grupos de usuarios del sistema';


--
-- TOC entry 2333 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN group_operation._date; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN group_operation._date IS 'Fecha de creacion de la asignacion de la operacion al grupo';


--
-- TOC entry 2334 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN group_operation.id_operation; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN group_operation.id_operation IS 'Identificador de la operacion';


--
-- TOC entry 2335 (class 0 OID 0)
-- Dependencies: 207
-- Name: COLUMN group_operation.id_group; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN group_operation.id_group IS 'Identificador del grupo';


--
-- TOC entry 162 (class 1259 OID 283842)
-- Dependencies: 2057 2058 2059 2060 6
-- Name: groups; Type: TABLE; Schema: acl; Owner: postgres; Tablespace: 
--

CREATE TABLE groups (
    id integer NOT NULL,
    name character varying(20) NOT NULL,
    description character varying(100) NOT NULL,
    _disabled boolean DEFAULT false NOT NULL,
    usuario_id integer DEFAULT 0 NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT check_id CHECK ((id >= 0))
);


ALTER TABLE acl.groups OWNER TO postgres;

--
-- TOC entry 2337 (class 0 OID 0)
-- Dependencies: 162
-- Name: COLUMN groups._disabled; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN groups._disabled IS 'Permite desactivar o activar el grupo';


--
-- TOC entry 2338 (class 0 OID 0)
-- Dependencies: 162
-- Name: COLUMN groups.usuario_id; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN groups.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2339 (class 0 OID 0)
-- Dependencies: 162
-- Name: COLUMN groups.modificado_en; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN groups.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 163 (class 1259 OID 283846)
-- Dependencies: 162 6
-- Name: groups_id_seq; Type: SEQUENCE; Schema: acl; Owner: postgres
--

CREATE SEQUENCE groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE acl.groups_id_seq OWNER TO postgres;

--
-- TOC entry 2340 (class 0 OID 0)
-- Dependencies: 163
-- Name: groups_id_seq; Type: SEQUENCE OWNED BY; Schema: acl; Owner: postgres
--

ALTER SEQUENCE groups_id_seq OWNED BY groups.id;


--
-- TOC entry 206 (class 1259 OID 284241)
-- Dependencies: 2106 2107 2108 2109 2110 2111 2112 2113 6
-- Name: operation; Type: TABLE; Schema: acl; Owner: postgres; Tablespace: 
--

CREATE TABLE operation (
    id integer NOT NULL,
    _name character varying(150) NOT NULL,
    url text,
    _order integer,
    visible bit(1) DEFAULT B'1'::"bit",
    icon character varying(50) DEFAULT NULL::character varying,
    help text,
    tooltip text,
    id_operation integer,
    chk_render_on character varying(25) DEFAULT 'sidebar'::character varying NOT NULL,
    chk_visual_type character varying(25) DEFAULT 'link'::character varying NOT NULL,
    chk_target_on character varying(25) DEFAULT 'content'::character varying NOT NULL,
    descripcion character varying,
    imagen character varying,
    CONSTRAINT chk_render_on CHECK (((chk_render_on)::text = ANY (ARRAY[('grid'::character varying)::text, ('sidebar'::character varying)::text, ('topbar'::character varying)::text, ('actionbar'::character varying)::text]))),
    CONSTRAINT chk_target_on CHECK (((chk_target_on)::text = ANY (ARRAY[('content'::character varying)::text, ('tab'::character varying)::text, ('tab_content'::character varying)::text, ('window'::character varying)::text, ('window2'::character varying)::text]))),
    CONSTRAINT chk_visual_type CHECK (((chk_visual_type)::text = ANY (ARRAY[('menu'::character varying)::text, ('submenu'::character varying)::text, ('method_js'::character varying)::text, ('link'::character varying)::text])))
);


ALTER TABLE acl.operation OWNER TO postgres;

--
-- TOC entry 2341 (class 0 OID 0)
-- Dependencies: 206
-- Name: TABLE operation; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON TABLE operation IS 'Entidad que contiene las operaciones del sistema';


--
-- TOC entry 2342 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.id; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.id IS 'Identificador del registro';


--
-- TOC entry 2343 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation._name; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation._name IS 'Nombre visible de la operacion, se utiliza en la etiqueta del boton o en el tooltip del icono';


--
-- TOC entry 2344 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.url; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.url IS 'URL con la accion de la operacion';


--
-- TOC entry 2345 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation._order; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation._order IS 'Numero que define el orden de las operaciones';


--
-- TOC entry 2346 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.visible; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.visible IS 'Define si la operacion es visible o no';


--
-- TOC entry 2347 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.icon; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.icon IS 'Ruta del icono de la operacion';


--
-- TOC entry 2348 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.help; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.help IS 'Ayuda del campo, es usada en la documentacion';


--
-- TOC entry 2349 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.tooltip; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.tooltip IS 'Ayuda del campo, es usada en el tooltip';


--
-- TOC entry 2350 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.id_operation; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.id_operation IS 'Identificador de la operacion|';


--
-- TOC entry 2351 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.chk_render_on; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.chk_render_on IS 'Identificador de la categoria que define el lugar donde se muestra la operacion';


--
-- TOC entry 2352 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.chk_visual_type; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.chk_visual_type IS 'Identificador de la categoria que define el formato visual como se mostrara la operacion';


--
-- TOC entry 2353 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.chk_target_on; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.chk_target_on IS 'Indica donde se abrira la operacion luego de hacer click, si en window o content';


--
-- TOC entry 2354 (class 0 OID 0)
-- Dependencies: 206
-- Name: COLUMN operation.descripcion; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN operation.descripcion IS 'contiene la descripcion de los procesos realizados por la operacion';


--
-- TOC entry 205 (class 1259 OID 284239)
-- Dependencies: 6 206
-- Name: operation_id_seq; Type: SEQUENCE; Schema: acl; Owner: postgres
--

CREATE SEQUENCE operation_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE acl.operation_id_seq OWNER TO postgres;

--
-- TOC entry 2356 (class 0 OID 0)
-- Dependencies: 205
-- Name: operation_id_seq; Type: SEQUENCE OWNED BY; Schema: acl; Owner: postgres
--

ALTER SEQUENCE operation_id_seq OWNED BY operation.id;


--
-- TOC entry 164 (class 1259 OID 283861)
-- Dependencies: 2062 2063 2064 2065 6
-- Name: users; Type: TABLE; Schema: acl; Owner: postgres; Tablespace: 
--

CREATE TABLE users (
    id integer NOT NULL,
    ip_address character varying(15),
    username character varying(100) NOT NULL,
    password character varying(255) NOT NULL,
    salt character varying(255),
    email character varying(100) NOT NULL,
    activation_code character varying(40),
    forgotten_password_code character varying(40),
    forgotten_password_time integer,
    remember_code character varying(40),
    created_on integer NOT NULL,
    last_login integer,
    active integer,
    first_name character varying(50),
    last_name character varying(50),
    direccion character varying(300),
    phone character varying(20),
    cedula character varying NOT NULL,
    usuario_id integer DEFAULT 0 NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL,
    CONSTRAINT check_active CHECK ((active >= 0)),
    CONSTRAINT check_id CHECK ((id >= 0))
);


ALTER TABLE acl.users OWNER TO postgres;

--
-- TOC entry 2357 (class 0 OID 0)
-- Dependencies: 164
-- Name: COLUMN users.usuario_id; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN users.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2358 (class 0 OID 0)
-- Dependencies: 164
-- Name: COLUMN users.modificado_en; Type: COMMENT; Schema: acl; Owner: postgres
--

COMMENT ON COLUMN users.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 165 (class 1259 OID 283869)
-- Dependencies: 2067 2068 2069 6
-- Name: users_groups; Type: TABLE; Schema: acl; Owner: postgres; Tablespace: 
--

CREATE TABLE users_groups (
    id integer NOT NULL,
    user_id integer NOT NULL,
    group_id integer NOT NULL,
    CONSTRAINT users_groups_check_group_id CHECK ((group_id >= 0)),
    CONSTRAINT users_groups_check_id CHECK ((id >= 0)),
    CONSTRAINT users_groups_check_user_id CHECK ((user_id >= 0))
);


ALTER TABLE acl.users_groups OWNER TO postgres;

--
-- TOC entry 166 (class 1259 OID 283875)
-- Dependencies: 165 6
-- Name: users_groups_id_seq; Type: SEQUENCE; Schema: acl; Owner: postgres
--

CREATE SEQUENCE users_groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE acl.users_groups_id_seq OWNER TO postgres;

--
-- TOC entry 2359 (class 0 OID 0)
-- Dependencies: 166
-- Name: users_groups_id_seq; Type: SEQUENCE OWNED BY; Schema: acl; Owner: postgres
--

ALTER SEQUENCE users_groups_id_seq OWNED BY users_groups.id;


--
-- TOC entry 167 (class 1259 OID 283877)
-- Dependencies: 6 164
-- Name: users_id_seq; Type: SEQUENCE; Schema: acl; Owner: postgres
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE acl.users_id_seq OWNER TO postgres;

--
-- TOC entry 2360 (class 0 OID 0)
-- Dependencies: 167
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: acl; Owner: postgres
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


SET search_path = public, pg_catalog;

--
-- TOC entry 224 (class 1259 OID 335915)
-- Dependencies: 2131 7
-- Name: auditorias; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE auditorias (
    id integer NOT NULL,
    usuario_id integer NOT NULL,
    esquema character varying,
    tabla character varying,
    registro_id bigint,
    fecha timestamp without time zone DEFAULT now() NOT NULL,
    transaccion character varying NOT NULL,
    datos_previos character varying,
    datos_actuales character varying,
    info_adicional character varying
);


ALTER TABLE public.auditorias OWNER TO postgres;

--
-- TOC entry 2361 (class 0 OID 0)
-- Dependencies: 224
-- Name: TABLE auditorias; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE auditorias IS 'Almacena la trama de lo sucedido con los registros';


--
-- TOC entry 2362 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN auditorias.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN auditorias.id IS 'Identificador de la auditoría';


--
-- TOC entry 2363 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN auditorias.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN auditorias.usuario_id IS 'Usuario que realiza la transacción';


--
-- TOC entry 2364 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN auditorias.esquema; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN auditorias.esquema IS 'Nombre del esquema de bd al que pertence la tabla que es modificada';


--
-- TOC entry 2365 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN auditorias.tabla; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN auditorias.tabla IS 'tabla sobre la cual se realiza una transacción';


--
-- TOC entry 2366 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN auditorias.fecha; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN auditorias.fecha IS 'Fecha y hora en la que se realiza la transacción';


--
-- TOC entry 2367 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN auditorias.transaccion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN auditorias.transaccion IS 'Indica qué transacción se realizó sobre el registro INSERT, UPDATE, DELETE';


--
-- TOC entry 2368 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN auditorias.datos_previos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN auditorias.datos_previos IS 'Estructura de los datos antes de ser modificados. JSON con formato campo: valor';


--
-- TOC entry 2369 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN auditorias.datos_actuales; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN auditorias.datos_actuales IS 'Estructura de los datos final luego de ser modificados. JSON con formato campo: valor';


--
-- TOC entry 2370 (class 0 OID 0)
-- Dependencies: 224
-- Name: COLUMN auditorias.info_adicional; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN auditorias.info_adicional IS 'Datos adicionales de conexión o cualquiera que sea necesario para la auditoría';


--
-- TOC entry 223 (class 1259 OID 335913)
-- Dependencies: 224 7
-- Name: auditorias_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE auditorias_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.auditorias_id_seq OWNER TO postgres;

--
-- TOC entry 2371 (class 0 OID 0)
-- Dependencies: 223
-- Name: auditorias_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE auditorias_id_seq OWNED BY auditorias.id;


--
-- TOC entry 211 (class 1259 OID 285405)
-- Dependencies: 2117 7
-- Name: cargos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cargos (
    id integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.cargos OWNER TO postgres;

--
-- TOC entry 2372 (class 0 OID 0)
-- Dependencies: 211
-- Name: TABLE cargos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE cargos IS 'Almacena la tipificación de cargos asociados a contactos';


--
-- TOC entry 2373 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN cargos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cargos.id IS 'Identificador del tipo de cargos asociados al contactos';


--
-- TOC entry 2374 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN cargos.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cargos.descripcion IS 'Descripción del tipo de cargos asociados al contactos';


--
-- TOC entry 2375 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN cargos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cargos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2376 (class 0 OID 0)
-- Dependencies: 211
-- Name: COLUMN cargos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cargos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 210 (class 1259 OID 285403)
-- Dependencies: 211 7
-- Name: cargos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cargos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cargos_id_seq OWNER TO postgres;

--
-- TOC entry 2377 (class 0 OID 0)
-- Dependencies: 210
-- Name: cargos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cargos_id_seq OWNED BY cargos.id;


--
-- TOC entry 168 (class 1259 OID 283888)
-- Dependencies: 7
-- Name: categorias_detalles_minutas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE categorias_detalles_minutas (
    id integer NOT NULL,
    descripcion character varying(50) NOT NULL
);


ALTER TABLE public.categorias_detalles_minutas OWNER TO postgres;

--
-- TOC entry 2378 (class 0 OID 0)
-- Dependencies: 168
-- Name: TABLE categorias_detalles_minutas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE categorias_detalles_minutas IS 'Categorías de los ítems detallados en la minuta';


--
-- TOC entry 2379 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN categorias_detalles_minutas.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN categorias_detalles_minutas.id IS 'Identificador de la categoría';


--
-- TOC entry 2380 (class 0 OID 0)
-- Dependencies: 168
-- Name: COLUMN categorias_detalles_minutas.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN categorias_detalles_minutas.descripcion IS 'Descripcion de la categoría del item de la minuta';


--
-- TOC entry 169 (class 1259 OID 283891)
-- Dependencies: 7 168
-- Name: categorias_detalles_minutas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE categorias_detalles_minutas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.categorias_detalles_minutas_id_seq OWNER TO postgres;

--
-- TOC entry 2381 (class 0 OID 0)
-- Dependencies: 169
-- Name: categorias_detalles_minutas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE categorias_detalles_minutas_id_seq OWNED BY categorias_detalles_minutas.id;


--
-- TOC entry 170 (class 1259 OID 283893)
-- Dependencies: 2071 2073 7
-- Name: clientes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE clientes (
    id integer NOT NULL,
    nombre character varying(100),
    tipo character(1) NOT NULL,
    dni character varying(20) NOT NULL,
    razon_social character varying(255) NOT NULL,
    direccion_fiscal character varying(1024) NOT NULL,
    direccion_correspondencia character varying(1024),
    es_direccion_fiscal boolean DEFAULT false NOT NULL,
    telefonos character varying(50) NOT NULL,
    fax character varying(50),
    correo_electronico character varying(255),
    sector_id integer NOT NULL,
    sitio_web character varying(255),
    es_prospecto boolean DEFAULT true NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.clientes OWNER TO postgres;

--
-- TOC entry 2382 (class 0 OID 0)
-- Dependencies: 170
-- Name: TABLE clientes; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE clientes IS 'Tabla para almacenar los datos de clientes';


--
-- TOC entry 2383 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.id IS 'Identificador del cliente';


--
-- TOC entry 2384 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.nombre IS 'Nombre del cliente';


--
-- TOC entry 2385 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.tipo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.tipo IS 'Indica el tipo de cliente, almacena
V, E, J, G';


--
-- TOC entry 2386 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.dni; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.dni IS 'DNI Identificación del cliente';


--
-- TOC entry 2387 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.razon_social; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.razon_social IS 'Razón Social';


--
-- TOC entry 2388 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.direccion_fiscal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.direccion_fiscal IS 'Dirección Fiscal';


--
-- TOC entry 2389 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.direccion_correspondencia; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.direccion_correspondencia IS 'Dirección de correspondencia';


--
-- TOC entry 2390 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.es_direccion_fiscal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.es_direccion_fiscal IS 'Indica si la dirección de correspondencia es igual a la dirección fiscal';


--
-- TOC entry 2391 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.telefonos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.telefonos IS 'Teléfonos del cliente';


--
-- TOC entry 2392 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.fax; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.fax IS 'Fax del cliente';


--
-- TOC entry 2393 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.correo_electronico; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.correo_electronico IS 'Correo Electrónico del cliente';


--
-- TOC entry 2394 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.sector_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.sector_id IS 'Sector económico del cliente';


--
-- TOC entry 2395 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.sitio_web; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.sitio_web IS 'Sitio Web del cliente';


--
-- TOC entry 2396 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.es_prospecto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.es_prospecto IS 'Indica si un cliente es prospecto. Deja de ser prospecto al adquirir un producto';


--
-- TOC entry 2397 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2398 (class 0 OID 0)
-- Dependencies: 170
-- Name: COLUMN clientes.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN clientes.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 171 (class 1259 OID 283900)
-- Dependencies: 170 7
-- Name: clientes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE clientes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.clientes_id_seq OWNER TO postgres;

--
-- TOC entry 2399 (class 0 OID 0)
-- Dependencies: 171
-- Name: clientes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE clientes_id_seq OWNED BY clientes.id;


--
-- TOC entry 172 (class 1259 OID 283902)
-- Dependencies: 7
-- Name: compromisos_pagos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE compromisos_pagos (
    id integer NOT NULL,
    codigo character varying NOT NULL,
    factura_id integer NOT NULL,
    cantidad_cuotas integer NOT NULL,
    frecuencia_pago_id integer NOT NULL,
    fecha_inicio timestamp without time zone NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.compromisos_pagos OWNER TO postgres;

--
-- TOC entry 2400 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN compromisos_pagos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN compromisos_pagos.id IS 'Identificador del compromiso de pago';


--
-- TOC entry 2401 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN compromisos_pagos.codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN compromisos_pagos.codigo IS 'Código';


--
-- TOC entry 2402 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN compromisos_pagos.factura_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN compromisos_pagos.factura_id IS 'Identificador de la factura';


--
-- TOC entry 2403 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN compromisos_pagos.cantidad_cuotas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN compromisos_pagos.cantidad_cuotas IS 'Cantidad cuotas en la que acuerda el cliente realizar el pago';


--
-- TOC entry 2404 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN compromisos_pagos.frecuencia_pago_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN compromisos_pagos.frecuencia_pago_id IS 'Indica la frecuencia con la que realizará los pagos, 7 semanal, 15 quincenal, 30 mensual, 90 trimestral. Se representa por un número de días.';


--
-- TOC entry 2405 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN compromisos_pagos.fecha_inicio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN compromisos_pagos.fecha_inicio IS 'Fecha en que acuerdan las partes inciar con los pagos.';


--
-- TOC entry 2406 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN compromisos_pagos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN compromisos_pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2407 (class 0 OID 0)
-- Dependencies: 172
-- Name: COLUMN compromisos_pagos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN compromisos_pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 173 (class 1259 OID 283908)
-- Dependencies: 172 7
-- Name: compromisos_pagos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE compromisos_pagos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.compromisos_pagos_id_seq OWNER TO postgres;

--
-- TOC entry 2408 (class 0 OID 0)
-- Dependencies: 173
-- Name: compromisos_pagos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE compromisos_pagos_id_seq OWNED BY compromisos_pagos.id;


--
-- TOC entry 174 (class 1259 OID 283910)
-- Dependencies: 7
-- Name: configuraciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE configuraciones (
    id integer NOT NULL,
    clave character varying(50) NOT NULL,
    valor character varying(50) NOT NULL
);


ALTER TABLE public.configuraciones OWNER TO postgres;

--
-- TOC entry 2409 (class 0 OID 0)
-- Dependencies: 174
-- Name: TABLE configuraciones; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE configuraciones IS 'Configuracion del sistema';


--
-- TOC entry 2410 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN configuraciones.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN configuraciones.id IS 'Identificador de la configuración';


--
-- TOC entry 2411 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN configuraciones.clave; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN configuraciones.clave IS 'Nombre o clave que representa la configuración';


--
-- TOC entry 2412 (class 0 OID 0)
-- Dependencies: 174
-- Name: COLUMN configuraciones.valor; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN configuraciones.valor IS 'Valor asignado a la configuración';


--
-- TOC entry 175 (class 1259 OID 283913)
-- Dependencies: 174 7
-- Name: configuraciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE configuraciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.configuraciones_id_seq OWNER TO postgres;

--
-- TOC entry 2413 (class 0 OID 0)
-- Dependencies: 175
-- Name: configuraciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE configuraciones_id_seq OWNED BY configuraciones.id;


--
-- TOC entry 176 (class 1259 OID 283915)
-- Dependencies: 2077 2078 2079 2080 7
-- Name: cotizaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cotizaciones (
    id integer NOT NULL,
    codigo character varying NOT NULL,
    cliente_id integer NOT NULL,
    fecha_creacion timestamp without time zone NOT NULL,
    fecha_expiracion timestamp without time zone NOT NULL,
    descuento real NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL,
    con_impuestos boolean NOT NULL,
    porcentaje_impuesto double precision,
    es_terminada boolean DEFAULT false NOT NULL,
    subtotal double precision,
    total_impuesto double precision,
    total_descuento double precision,
    total double precision,
    motivo_descuento character varying(255),
    fue_enviada boolean DEFAULT false NOT NULL,
    aprobada boolean DEFAULT false,
    costo_hora_adicional double precision DEFAULT 0,
    subtotal_con_descuento double precision,
    promocion boolean
);


ALTER TABLE public.cotizaciones OWNER TO postgres;

--
-- TOC entry 2414 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.id IS 'Identificador de la cotización';


--
-- TOC entry 2415 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.codigo IS 'Código';


--
-- TOC entry 2416 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.cliente_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.cliente_id IS 'Identificador del cliente';


--
-- TOC entry 2417 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.fecha_creacion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.fecha_creacion IS 'Fecha de creación de la cotización';


--
-- TOC entry 2418 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.fecha_expiracion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.fecha_expiracion IS 'Fecha en que expira la cotización';


--
-- TOC entry 2419 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.descuento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.descuento IS 'Descuento sobre la cotización que otorga el usuario que genera la cotización';


--
-- TOC entry 2420 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2421 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 2422 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.con_impuestos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.con_impuestos IS 'Indica si la cotización incluye o no los impuestos como el IVA';


--
-- TOC entry 2423 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.porcentaje_impuesto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.porcentaje_impuesto IS 'Indica el valor en porcentaje del impuesto a calcular';


--
-- TOC entry 2424 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.es_terminada; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.es_terminada IS 'Indica si la cotización fue culmidad, es decir, al finalizar el proceso la cotización se almacena con éxito.';


--
-- TOC entry 2425 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.subtotal; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.subtotal IS 'SubTotal de la cotización';


--
-- TOC entry 2426 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.total_impuesto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.total_impuesto IS 'Total calculado a aplicar por razón de impuestos';


--
-- TOC entry 2427 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.total_descuento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.total_descuento IS 'Total calculado del porcentaje de descuento aplicado.';


--
-- TOC entry 2428 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.total; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.total IS 'Total de la cotización.';


--
-- TOC entry 2429 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.motivo_descuento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.motivo_descuento IS 'Motivo por el cual se le dio un descuento en la cotización';


--
-- TOC entry 2430 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.fue_enviada; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.fue_enviada IS 'Indica si la cotización fue enviada por correo o no';


--
-- TOC entry 2431 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.aprobada; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.aprobada IS 'Indica si la cotización fue aprobada para su facturación';


--
-- TOC entry 2432 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.costo_hora_adicional; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.costo_hora_adicional IS 'Costo de la hora adicional por requerimientos no contemplados';


--
-- TOC entry 2433 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.subtotal_con_descuento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.subtotal_con_descuento IS 'Subtotal menos el descuento realizado';


--
-- TOC entry 2434 (class 0 OID 0)
-- Dependencies: 176
-- Name: COLUMN cotizaciones.promocion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones.promocion IS 'Indica que la cotización es una promoción que se está haciendo';


--
-- TOC entry 222 (class 1259 OID 299356)
-- Dependencies: 2128 2129 7
-- Name: cotizaciones_formas_pagos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cotizaciones_formas_pagos (
    id integer NOT NULL,
    cotizacion_id integer,
    forma_pago_id integer,
    tipo_plazo character varying(10),
    dias smallint,
    porcentaje double precision,
    usuario_id integer DEFAULT 1 NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL,
    intervalo character varying
);


ALTER TABLE public.cotizaciones_formas_pagos OWNER TO postgres;

--
-- TOC entry 2435 (class 0 OID 0)
-- Dependencies: 222
-- Name: TABLE cotizaciones_formas_pagos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE cotizaciones_formas_pagos IS 'Almacena las formas de pago relacionadas en la cotización';


--
-- TOC entry 2436 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN cotizaciones_formas_pagos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_formas_pagos.id IS 'Identificador de la relación cotización forma de pago';


--
-- TOC entry 2437 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN cotizaciones_formas_pagos.cotizacion_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_formas_pagos.cotizacion_id IS 'Identificador de la cotización';


--
-- TOC entry 2438 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN cotizaciones_formas_pagos.forma_pago_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_formas_pagos.forma_pago_id IS 'Identificador de la forma de pago';


--
-- TOC entry 2439 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN cotizaciones_formas_pagos.tipo_plazo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_formas_pagos.tipo_plazo IS 'Indica el tipo de plazo';


--
-- TOC entry 2440 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN cotizaciones_formas_pagos.dias; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_formas_pagos.dias IS 'Indica el número de días dado para cumplir con el plazo';


--
-- TOC entry 2441 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN cotizaciones_formas_pagos.porcentaje; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_formas_pagos.porcentaje IS 'Indica el porcentaje a cancelar en el plazo indicado';


--
-- TOC entry 2442 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN cotizaciones_formas_pagos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_formas_pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2443 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN cotizaciones_formas_pagos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_formas_pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 2444 (class 0 OID 0)
-- Dependencies: 222
-- Name: COLUMN cotizaciones_formas_pagos.intervalo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_formas_pagos.intervalo IS 'Almacena condiciones de pagos por intervalos';


--
-- TOC entry 221 (class 1259 OID 299354)
-- Dependencies: 222 7
-- Name: cotizaciones_formas_pagos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cotizaciones_formas_pagos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cotizaciones_formas_pagos_id_seq OWNER TO postgres;

--
-- TOC entry 2445 (class 0 OID 0)
-- Dependencies: 221
-- Name: cotizaciones_formas_pagos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cotizaciones_formas_pagos_id_seq OWNED BY cotizaciones_formas_pagos.id;


--
-- TOC entry 177 (class 1259 OID 283921)
-- Dependencies: 176 7
-- Name: cotizaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cotizaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cotizaciones_id_seq OWNER TO postgres;

--
-- TOC entry 2446 (class 0 OID 0)
-- Dependencies: 177
-- Name: cotizaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cotizaciones_id_seq OWNED BY cotizaciones.id;


--
-- TOC entry 217 (class 1259 OID 286513)
-- Dependencies: 2123 7
-- Name: cotizaciones_recursos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE cotizaciones_recursos (
    id integer NOT NULL,
    cotizacion_id integer,
    recurso_id integer,
    cantidad smallint,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL,
    orden smallint
);


ALTER TABLE public.cotizaciones_recursos OWNER TO postgres;

--
-- TOC entry 2447 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN cotizaciones_recursos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_recursos.id IS 'Identificador del registro';


--
-- TOC entry 2448 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN cotizaciones_recursos.cotizacion_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_recursos.cotizacion_id IS 'Identificador de la cotización';


--
-- TOC entry 2449 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN cotizaciones_recursos.recurso_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_recursos.recurso_id IS 'Identificador del recurso';


--
-- TOC entry 2450 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN cotizaciones_recursos.cantidad; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_recursos.cantidad IS 'Cantidad de unidades por recurso';


--
-- TOC entry 2451 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN cotizaciones_recursos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_recursos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2452 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN cotizaciones_recursos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_recursos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 2453 (class 0 OID 0)
-- Dependencies: 217
-- Name: COLUMN cotizaciones_recursos.orden; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN cotizaciones_recursos.orden IS 'Indica el orden en que se mostraran los recursos en la cotización';


--
-- TOC entry 216 (class 1259 OID 286511)
-- Dependencies: 7 217
-- Name: cotizaciones_recursos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE cotizaciones_recursos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.cotizaciones_recursos_id_seq OWNER TO postgres;

--
-- TOC entry 2454 (class 0 OID 0)
-- Dependencies: 216
-- Name: cotizaciones_recursos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE cotizaciones_recursos_id_seq OWNED BY cotizaciones_recursos.id;


--
-- TOC entry 178 (class 1259 OID 283923)
-- Dependencies: 7
-- Name: facturas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE facturas (
    id integer NOT NULL,
    cliente_id integer NOT NULL,
    codigo character varying NOT NULL,
    fecha_creacion timestamp without time zone NOT NULL,
    con_impuestos boolean NOT NULL,
    porcentaje_impuesto real NOT NULL,
    descuento real NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.facturas OWNER TO postgres;

--
-- TOC entry 2455 (class 0 OID 0)
-- Dependencies: 178
-- Name: TABLE facturas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE facturas IS 'Almacena los datos generales de la factura';


--
-- TOC entry 2456 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN facturas.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN facturas.id IS 'Identificador de la factura';


--
-- TOC entry 2457 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN facturas.cliente_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN facturas.cliente_id IS 'Identificador del cliente';


--
-- TOC entry 2458 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN facturas.codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN facturas.codigo IS 'Código';


--
-- TOC entry 2459 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN facturas.fecha_creacion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN facturas.fecha_creacion IS 'Fecha de creación de la cotización';


--
-- TOC entry 2460 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN facturas.con_impuestos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN facturas.con_impuestos IS 'Indica si la cotización incluye o no los impuestos como el IVA';


--
-- TOC entry 2461 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN facturas.porcentaje_impuesto; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN facturas.porcentaje_impuesto IS 'Indica el valor en porcentaje del impuesto a calcular';


--
-- TOC entry 2462 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN facturas.descuento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN facturas.descuento IS 'Descuento sobre la cotización que otorga el usuario que genera la cotización';


--
-- TOC entry 2463 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN facturas.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN facturas.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2464 (class 0 OID 0)
-- Dependencies: 178
-- Name: COLUMN facturas.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN facturas.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 179 (class 1259 OID 283929)
-- Dependencies: 178 7
-- Name: facturas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE facturas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.facturas_id_seq OWNER TO postgres;

--
-- TOC entry 2465 (class 0 OID 0)
-- Dependencies: 179
-- Name: facturas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE facturas_id_seq OWNED BY facturas.id;


--
-- TOC entry 220 (class 1259 OID 299331)
-- Dependencies: 2125 2126 7
-- Name: formas_pagos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE formas_pagos (
    id integer NOT NULL,
    descripcion character varying(50),
    usuario_id integer DEFAULT 1 NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.formas_pagos OWNER TO postgres;

--
-- TOC entry 2466 (class 0 OID 0)
-- Dependencies: 220
-- Name: TABLE formas_pagos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE formas_pagos IS 'Almacena las formas de pagos para las cotizaciones y facturas';


--
-- TOC entry 2467 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN formas_pagos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN formas_pagos.id IS 'Identificador de la forma de pago';


--
-- TOC entry 2468 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN formas_pagos.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN formas_pagos.descripcion IS 'Descripción o nombre de la forma de pago. Ejemplo, contado, crédito, etc.';


--
-- TOC entry 2469 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN formas_pagos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN formas_pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2470 (class 0 OID 0)
-- Dependencies: 220
-- Name: COLUMN formas_pagos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN formas_pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 219 (class 1259 OID 299329)
-- Dependencies: 220 7
-- Name: formas_pagos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE formas_pagos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.formas_pagos_id_seq OWNER TO postgres;

--
-- TOC entry 2471 (class 0 OID 0)
-- Dependencies: 219
-- Name: formas_pagos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE formas_pagos_id_seq OWNED BY formas_pagos.id;


--
-- TOC entry 180 (class 1259 OID 283931)
-- Dependencies: 7
-- Name: frecuencias_pagos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE frecuencias_pagos (
    id integer NOT NULL,
    dias integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.frecuencias_pagos OWNER TO postgres;

--
-- TOC entry 2472 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN frecuencias_pagos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN frecuencias_pagos.id IS 'Identificador de la frecuencia de pago';


--
-- TOC entry 2473 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN frecuencias_pagos.dias; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN frecuencias_pagos.dias IS 'ndica la frecuencia en días con la que realizará los pagos, 7 semanal, 15 quincenal, 30 mensual, 90 trimestral. Se representa por un número de días.';


--
-- TOC entry 2474 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN frecuencias_pagos.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN frecuencias_pagos.descripcion IS 'Nombre descriptivo para la frecuencia de pago';


--
-- TOC entry 2475 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN frecuencias_pagos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN frecuencias_pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2476 (class 0 OID 0)
-- Dependencies: 180
-- Name: COLUMN frecuencias_pagos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN frecuencias_pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 181 (class 1259 OID 283934)
-- Dependencies: 7 180
-- Name: frecuencias_pagos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE frecuencias_pagos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.frecuencias_pagos_id_seq OWNER TO postgres;

--
-- TOC entry 2477 (class 0 OID 0)
-- Dependencies: 181
-- Name: frecuencias_pagos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE frecuencias_pagos_id_seq OWNED BY frecuencias_pagos.id;


--
-- TOC entry 213 (class 1259 OID 285453)
-- Dependencies: 2119 7
-- Name: horarios; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE horarios (
    id integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.horarios OWNER TO postgres;

--
-- TOC entry 2478 (class 0 OID 0)
-- Dependencies: 213
-- Name: TABLE horarios; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE horarios IS 'Almacena la tipificación de los Horarios para comunicarse con los contactos';


--
-- TOC entry 2479 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN horarios.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN horarios.id IS 'Identificador del horarios';


--
-- TOC entry 2480 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN horarios.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN horarios.descripcion IS 'Descripción del horarios';


--
-- TOC entry 2481 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN horarios.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN horarios.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2482 (class 0 OID 0)
-- Dependencies: 213
-- Name: COLUMN horarios.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN horarios.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 212 (class 1259 OID 285451)
-- Dependencies: 213 7
-- Name: horarios_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE horarios_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.horarios_id_seq OWNER TO postgres;

--
-- TOC entry 2483 (class 0 OID 0)
-- Dependencies: 212
-- Name: horarios_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE horarios_id_seq OWNED BY horarios.id;


--
-- TOC entry 182 (class 1259 OID 283936)
-- Dependencies: 7
-- Name: minutas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE minutas (
    id integer NOT NULL,
    cliente_id integer NOT NULL,
    descripcion character varying NOT NULL,
    fecha_creacion timestamp without time zone NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.minutas OWNER TO postgres;

--
-- TOC entry 2484 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN minutas.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas.id IS 'Identificador del la minuta';


--
-- TOC entry 2485 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN minutas.cliente_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas.cliente_id IS 'Identificador del cliente';


--
-- TOC entry 2486 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN minutas.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas.descripcion IS 'Descripción de la minuta, puede almacenar puntos a tratar';


--
-- TOC entry 2487 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN minutas.fecha_creacion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas.fecha_creacion IS 'Fecha de creación de la cotización';


--
-- TOC entry 2488 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN minutas.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2489 (class 0 OID 0)
-- Dependencies: 182
-- Name: COLUMN minutas.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 183 (class 1259 OID 283942)
-- Dependencies: 7
-- Name: minutas_detalles; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE minutas_detalles (
    id integer NOT NULL,
    minuta_id integer NOT NULL,
    acuerdo character varying NOT NULL,
    producto_id integer NOT NULL,
    categoria_id integer NOT NULL,
    fecha_respuesta timestamp without time zone NOT NULL,
    fecha_notificacion timestamp without time zone,
    administrativo boolean NOT NULL,
    tecnico boolean NOT NULL,
    realizado boolean NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.minutas_detalles OWNER TO postgres;

--
-- TOC entry 2490 (class 0 OID 0)
-- Dependencies: 183
-- Name: TABLE minutas_detalles; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE minutas_detalles IS 'Tabla donde se crean los ítems de la reunión';


--
-- TOC entry 2491 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.id IS 'Identificador del ítem de acuerdos en la reunión';


--
-- TOC entry 2492 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.minuta_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.minuta_id IS 'Identificador de la minuta';


--
-- TOC entry 2493 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.acuerdo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.acuerdo IS 'Descripción del acuerdo establacido en la reunión';


--
-- TOC entry 2494 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.producto_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.producto_id IS 'Identificador del producto al que se llegó adquirir en el cuerdo';


--
-- TOC entry 2495 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.categoria_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.categoria_id IS 'Categoría del ítem de la minuta';


--
-- TOC entry 2496 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.fecha_respuesta; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.fecha_respuesta IS 'Fecha en que se dará respuesta sobre el acuerdo';


--
-- TOC entry 2497 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.fecha_notificacion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.fecha_notificacion IS 'Fecha en que se envió la notificación de acuerdo por vencer';


--
-- TOC entry 2498 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.administrativo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.administrativo IS 'Indica si el ítem de es de tipo administrativo';


--
-- TOC entry 2499 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.tecnico; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.tecnico IS 'Indica si el item es de tipo técnico';


--
-- TOC entry 2500 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.realizado; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.realizado IS 'Indica si el ítem levantado en la minuta se realizó o no.';


--
-- TOC entry 2501 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2502 (class 0 OID 0)
-- Dependencies: 183
-- Name: COLUMN minutas_detalles.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN minutas_detalles.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 184 (class 1259 OID 283948)
-- Dependencies: 183 7
-- Name: minutas_detalles_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE minutas_detalles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.minutas_detalles_id_seq OWNER TO postgres;

--
-- TOC entry 2503 (class 0 OID 0)
-- Dependencies: 184
-- Name: minutas_detalles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE minutas_detalles_id_seq OWNED BY minutas_detalles.id;


--
-- TOC entry 185 (class 1259 OID 283950)
-- Dependencies: 182 7
-- Name: minutas_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE minutas_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.minutas_id_seq OWNER TO postgres;

--
-- TOC entry 2504 (class 0 OID 0)
-- Dependencies: 185
-- Name: minutas_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE minutas_id_seq OWNED BY minutas.id;


--
-- TOC entry 186 (class 1259 OID 283952)
-- Dependencies: 2085 7
-- Name: pagos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE pagos (
    id integer NOT NULL,
    compromiso_pago_id integer NOT NULL,
    fecha_pago timestamp without time zone NOT NULL,
    comprobante_pago character varying(50) NOT NULL,
    fecha_inicia_periodo timestamp without time zone NOT NULL,
    fecha_fin_periodo timestamp without time zone NOT NULL,
    es_pago boolean DEFAULT false NOT NULL,
    es_vencido boolean,
    fecha_vencimiento timestamp without time zone,
    fecha_notificacion_vencimiento timestamp without time zone,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.pagos OWNER TO postgres;

--
-- TOC entry 2505 (class 0 OID 0)
-- Dependencies: 186
-- Name: TABLE pagos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE pagos IS 'Almacena los pagos sobre los compromisos adquiridos';


--
-- TOC entry 2506 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.id IS 'Identificador del compromiso de pago';


--
-- TOC entry 2507 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.compromiso_pago_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.compromiso_pago_id IS 'Identificador del compromiso de pago';


--
-- TOC entry 2508 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.fecha_pago; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.fecha_pago IS 'Fecha de pago';


--
-- TOC entry 2509 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.comprobante_pago; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.comprobante_pago IS 'Código del comprobante del pago realizado';


--
-- TOC entry 2510 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.fecha_inicia_periodo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.fecha_inicia_periodo IS 'Fecha en la que inicia el período del pago';


--
-- TOC entry 2511 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.fecha_fin_periodo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.fecha_fin_periodo IS 'Fecha en que finaliza el período del pago';


--
-- TOC entry 2512 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.es_pago; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.es_pago IS 'Indica si el pago fue realizado.';


--
-- TOC entry 2513 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.es_vencido; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.es_vencido IS 'Indica si el pago se encuentra vencido';


--
-- TOC entry 2514 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.fecha_vencimiento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.fecha_vencimiento IS 'Indica la fecha en que el pago venció,';


--
-- TOC entry 2515 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.fecha_notificacion_vencimiento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.fecha_notificacion_vencimiento IS 'Fecha en que se envió la notificación de vencimiento de acuerdo de pago';


--
-- TOC entry 2516 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2517 (class 0 OID 0)
-- Dependencies: 186
-- Name: COLUMN pagos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 187 (class 1259 OID 283956)
-- Dependencies: 186 7
-- Name: pagos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE pagos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pagos_id_seq OWNER TO postgres;

--
-- TOC entry 2518 (class 0 OID 0)
-- Dependencies: 187
-- Name: pagos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE pagos_id_seq OWNED BY pagos.id;


--
-- TOC entry 188 (class 1259 OID 283958)
-- Dependencies: 7
-- Name: participantes; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE participantes (
    id integer NOT NULL,
    persona_contacto_id integer NOT NULL,
    minuta_id integer NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.participantes OWNER TO postgres;

--
-- TOC entry 2519 (class 0 OID 0)
-- Dependencies: 188
-- Name: TABLE participantes; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE participantes IS 'Participantes de la reunión registrada en minuta';


--
-- TOC entry 2520 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN participantes.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN participantes.id IS 'Identificador del participante';


--
-- TOC entry 2521 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN participantes.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN participantes.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2522 (class 0 OID 0)
-- Dependencies: 188
-- Name: COLUMN participantes.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN participantes.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 189 (class 1259 OID 283961)
-- Dependencies: 188 7
-- Name: participantes_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE participantes_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.participantes_id_seq OWNER TO postgres;

--
-- TOC entry 2523 (class 0 OID 0)
-- Dependencies: 189
-- Name: participantes_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE participantes_id_seq OWNED BY participantes.id;


--
-- TOC entry 190 (class 1259 OID 283963)
-- Dependencies: 7
-- Name: personas_contactos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE personas_contactos (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    apellido character varying(20),
    cedula character varying(15),
    direccion character varying(1024) NOT NULL,
    telefono_1 character varying(20) NOT NULL,
    telefono_2 character varying,
    correo_electronico_1 character varying,
    correo_electronico_2 character varying,
    cliente_id integer NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL,
    horario_id integer,
    cargo_id integer,
    es_interno boolean
);


ALTER TABLE public.personas_contactos OWNER TO postgres;

--
-- TOC entry 2524 (class 0 OID 0)
-- Dependencies: 190
-- Name: TABLE personas_contactos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE personas_contactos IS 'Personas de contacto';


--
-- TOC entry 2525 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.id IS 'Identificador del contacto';


--
-- TOC entry 2526 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.nombre IS 'Nombre de la persona de contacto';


--
-- TOC entry 2527 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.apellido; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.apellido IS 'Apellido de la persona de contacto';


--
-- TOC entry 2528 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.cedula; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.cedula IS 'Cédula del contacto';


--
-- TOC entry 2529 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.direccion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.direccion IS 'Dirección de la persona de contacto';


--
-- TOC entry 2530 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.telefono_1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.telefono_1 IS 'Teléfono de contacto principal';


--
-- TOC entry 2531 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.telefono_2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.telefono_2 IS 'Teléfono de contacto secundario';


--
-- TOC entry 2532 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.correo_electronico_1; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.correo_electronico_1 IS 'Correo electrónico de contacto principal';


--
-- TOC entry 2533 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.correo_electronico_2; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.correo_electronico_2 IS 'Correo electrónico de contacto secundario';


--
-- TOC entry 2534 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.cliente_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.cliente_id IS 'Identificador del cliente al que pertenece el contacto';


--
-- TOC entry 2535 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2536 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 2537 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.horario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.horario_id IS 'Horario de disponibilidad del contacto';


--
-- TOC entry 2538 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.cargo_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.cargo_id IS 'Identificador del cargo del contacto';


--
-- TOC entry 2539 (class 0 OID 0)
-- Dependencies: 190
-- Name: COLUMN personas_contactos.es_interno; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN personas_contactos.es_interno IS 'Indica si un contacto pertenece a la empresa (interno) o no (externo)';


--
-- TOC entry 191 (class 1259 OID 283969)
-- Dependencies: 7 190
-- Name: personas_contactos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE personas_contactos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personas_contactos_id_seq OWNER TO postgres;

--
-- TOC entry 2540 (class 0 OID 0)
-- Dependencies: 191
-- Name: personas_contactos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE personas_contactos_id_seq OWNED BY personas_contactos.id;


--
-- TOC entry 192 (class 1259 OID 283971)
-- Dependencies: 2089 2091 7
-- Name: productos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productos (
    id integer NOT NULL,
    codigo character varying(25) NOT NULL,
    nombre character varying(255) NOT NULL,
    descripcion character varying(1024) NOT NULL,
    precio real NOT NULL,
    tipo_producto_id integer NOT NULL,
    requisitos character varying NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.productos OWNER TO postgres;

--
-- TOC entry 2541 (class 0 OID 0)
-- Dependencies: 192
-- Name: TABLE productos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE productos IS 'Almacena los productos a cotizar';


--
-- TOC entry 2542 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.id IS 'Identificador del producto';


--
-- TOC entry 2543 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.codigo IS 'Código del producto';


--
-- TOC entry 2544 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.nombre IS 'Nombre del producto';


--
-- TOC entry 2545 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.descripcion IS 'Descripción del producto';


--
-- TOC entry 2546 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.precio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.precio IS 'Precio del producto';


--
-- TOC entry 2547 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.tipo_producto_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.tipo_producto_id IS 'Identificador del tipo de producto';


--
-- TOC entry 2548 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.requisitos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.requisitos IS 'Requisitos para la adquisición del producto';


--
-- TOC entry 2549 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.activo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.activo IS 'Indica si el producto se encuentra activo o no';


--
-- TOC entry 2550 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2551 (class 0 OID 0)
-- Dependencies: 192
-- Name: COLUMN productos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 193 (class 1259 OID 283978)
-- Dependencies: 2093 7
-- Name: productos_cotizaciones; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productos_cotizaciones (
    id integer NOT NULL,
    cotizacion_id integer NOT NULL,
    producto_id integer NOT NULL,
    precio real NOT NULL,
    descuento real NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL,
    cantidad integer DEFAULT 1 NOT NULL,
    precio_total double precision
);


ALTER TABLE public.productos_cotizaciones OWNER TO postgres;

--
-- TOC entry 2552 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN productos_cotizaciones.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_cotizaciones.id IS 'Identificador del ítem';


--
-- TOC entry 2553 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN productos_cotizaciones.cotizacion_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_cotizaciones.cotizacion_id IS 'Identificador de la cotización, clave for{anea proveniente de la tabla cotizaciones';


--
-- TOC entry 2554 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN productos_cotizaciones.producto_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_cotizaciones.producto_id IS 'Identificador del producto, campo foráneo de la tabla productos';


--
-- TOC entry 2555 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN productos_cotizaciones.precio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_cotizaciones.precio IS 'Precio extablecido para el producto en el momento en que se crea la cotización';


--
-- TOC entry 2556 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN productos_cotizaciones.descuento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_cotizaciones.descuento IS 'Porcentaje de descuento sobre el precio del producto, otorgado por el iusuario que genera la cotización';


--
-- TOC entry 2557 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN productos_cotizaciones.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_cotizaciones.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2558 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN productos_cotizaciones.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_cotizaciones.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 2559 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN productos_cotizaciones.cantidad; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_cotizaciones.cantidad IS 'Cantidad de productos a adquirir';


--
-- TOC entry 2560 (class 0 OID 0)
-- Dependencies: 193
-- Name: COLUMN productos_cotizaciones.precio_total; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_cotizaciones.precio_total IS 'Precio calculado de la cantidad de productos por el valor unitario.';


--
-- TOC entry 194 (class 1259 OID 283981)
-- Dependencies: 193 7
-- Name: productos_cotizaciones_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE productos_cotizaciones_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productos_cotizaciones_id_seq OWNER TO postgres;

--
-- TOC entry 2561 (class 0 OID 0)
-- Dependencies: 194
-- Name: productos_cotizaciones_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE productos_cotizaciones_id_seq OWNED BY productos_cotizaciones.id;


--
-- TOC entry 195 (class 1259 OID 283983)
-- Dependencies: 2095 2096 7
-- Name: productos_facturas; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productos_facturas (
    id integer NOT NULL,
    factura_id integer NOT NULL,
    producto_id integer NOT NULL,
    fecha_adquisicion timestamp without time zone,
    fecha_activacion timestamp without time zone,
    fecha_vencimiento timestamp without time zone,
    precio real NOT NULL,
    descuento real NOT NULL,
    activo boolean DEFAULT true NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL,
    cantidad smallint DEFAULT 1 NOT NULL
);


ALTER TABLE public.productos_facturas OWNER TO postgres;

--
-- TOC entry 2562 (class 0 OID 0)
-- Dependencies: 195
-- Name: TABLE productos_facturas; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE productos_facturas IS 'Productos facturados';


--
-- TOC entry 2563 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.id IS 'Identificador del registro';


--
-- TOC entry 2564 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.factura_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.factura_id IS 'Identificador de la factura';


--
-- TOC entry 2565 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.fecha_adquisicion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.fecha_adquisicion IS 'Fecha en la que el producto fue adquirido';


--
-- TOC entry 2566 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.fecha_activacion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.fecha_activacion IS 'Fecha en la que se activa el producto y a partir del cual se contabiliza el tiempo en que estará activo.';


--
-- TOC entry 2567 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.fecha_vencimiento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.fecha_vencimiento IS 'Fecha en la que vence el producto adquirido según el contrato';


--
-- TOC entry 2568 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.precio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.precio IS 'Precio extablecido para el producto en el momento en que se crea la cotización';


--
-- TOC entry 2569 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.descuento; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.descuento IS 'Porcentaje de descuento sobre el precio del producto, otorgado por el iusuario que genera la cotización';


--
-- TOC entry 2570 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.activo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.activo IS 'Indica si el producto se encuentra activo o no';


--
-- TOC entry 2571 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2572 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 2573 (class 0 OID 0)
-- Dependencies: 195
-- Name: COLUMN productos_facturas.cantidad; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas.cantidad IS 'Cantidad de productos a adquirir';


--
-- TOC entry 196 (class 1259 OID 283986)
-- Dependencies: 195 7
-- Name: productos_factura_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE productos_factura_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productos_factura_id_seq OWNER TO postgres;

--
-- TOC entry 2574 (class 0 OID 0)
-- Dependencies: 196
-- Name: productos_factura_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE productos_factura_id_seq OWNED BY productos_facturas.id;


--
-- TOC entry 197 (class 1259 OID 283988)
-- Dependencies: 7
-- Name: productos_facturas_estados; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productos_facturas_estados (
    id integer NOT NULL,
    estado character varying(10) NOT NULL,
    producto_factura_id integer NOT NULL,
    cliente_producto_id integer NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.productos_facturas_estados OWNER TO postgres;

--
-- TOC entry 2575 (class 0 OID 0)
-- Dependencies: 197
-- Name: TABLE productos_facturas_estados; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE productos_facturas_estados IS 'Almacena el histórico de estados que tiene un producto facturado para un cliente';


--
-- TOC entry 2576 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN productos_facturas_estados.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas_estados.id IS 'Identificador del estado';


--
-- TOC entry 2577 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN productos_facturas_estados.estado; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas_estados.estado IS 'Estado en que se encuetra el producto con respecto al cliente. Se registra un histórico y por aplicación se dará siempre el último estado.';


--
-- TOC entry 2578 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN productos_facturas_estados.cliente_producto_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas_estados.cliente_producto_id IS 'Identificador de la relación clientes_productos';


--
-- TOC entry 2579 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN productos_facturas_estados.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas_estados.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2580 (class 0 OID 0)
-- Dependencies: 197
-- Name: COLUMN productos_facturas_estados.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_facturas_estados.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 198 (class 1259 OID 283991)
-- Dependencies: 197 7
-- Name: productos_facturas_estados_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE productos_facturas_estados_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productos_facturas_estados_id_seq OWNER TO postgres;

--
-- TOC entry 2581 (class 0 OID 0)
-- Dependencies: 198
-- Name: productos_facturas_estados_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE productos_facturas_estados_id_seq OWNED BY productos_facturas_estados.id;


--
-- TOC entry 209 (class 1259 OID 285286)
-- Dependencies: 7
-- Name: productos_historico_precio; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE productos_historico_precio (
    id integer NOT NULL,
    producto_id integer NOT NULL,
    precio_anterior double precision,
    precio_nuevo double precision,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone NOT NULL
);


ALTER TABLE public.productos_historico_precio OWNER TO postgres;

--
-- TOC entry 2582 (class 0 OID 0)
-- Dependencies: 209
-- Name: TABLE productos_historico_precio; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE productos_historico_precio IS 'Almacena un histórico del cambio de precios en para los productos.';


--
-- TOC entry 2583 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN productos_historico_precio.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_historico_precio.id IS 'Identificador del registro';


--
-- TOC entry 2584 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN productos_historico_precio.producto_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_historico_precio.producto_id IS 'Identificador del producto al que se actualiza el precio';


--
-- TOC entry 2585 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN productos_historico_precio.precio_anterior; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_historico_precio.precio_anterior IS 'Precio que tenía el producto antes de ser actualizado';


--
-- TOC entry 2586 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN productos_historico_precio.precio_nuevo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_historico_precio.precio_nuevo IS 'Precio al que se actualiza el producto.';


--
-- TOC entry 2587 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN productos_historico_precio.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_historico_precio.usuario_id IS 'Usuario que ejecutó la acción';


--
-- TOC entry 2588 (class 0 OID 0)
-- Dependencies: 209
-- Name: COLUMN productos_historico_precio.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN productos_historico_precio.modificado_en IS 'Fecha Hora en que se ejecutó la acción';


--
-- TOC entry 208 (class 1259 OID 285284)
-- Dependencies: 7 209
-- Name: productos_historico_precio_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE productos_historico_precio_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productos_historico_precio_id_seq OWNER TO postgres;

--
-- TOC entry 2589 (class 0 OID 0)
-- Dependencies: 208
-- Name: productos_historico_precio_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE productos_historico_precio_id_seq OWNED BY productos_historico_precio.id;


--
-- TOC entry 199 (class 1259 OID 283993)
-- Dependencies: 7 192
-- Name: productos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE productos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.productos_id_seq OWNER TO postgres;

--
-- TOC entry 2590 (class 0 OID 0)
-- Dependencies: 199
-- Name: productos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE productos_id_seq OWNED BY productos.id;


--
-- TOC entry 215 (class 1259 OID 286403)
-- Dependencies: 2121 7
-- Name: recursos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE recursos (
    id integer NOT NULL,
    descripcion character varying(255),
    codigo character varying(50),
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL,
    informacion_adicional text
);


ALTER TABLE public.recursos OWNER TO postgres;

--
-- TOC entry 2591 (class 0 OID 0)
-- Dependencies: 215
-- Name: TABLE recursos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE recursos IS 'Recursos a utilizar en el proyecto';


--
-- TOC entry 2592 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN recursos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN recursos.id IS 'Identificador del recurso';


--
-- TOC entry 2593 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN recursos.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN recursos.descripcion IS 'Descripción del recurso';


--
-- TOC entry 2594 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN recursos.codigo; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN recursos.codigo IS 'Código del recurso';


--
-- TOC entry 2595 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN recursos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN recursos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2596 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN recursos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN recursos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 2597 (class 0 OID 0)
-- Dependencies: 215
-- Name: COLUMN recursos.informacion_adicional; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN recursos.informacion_adicional IS 'Información adicional para definir el recurso. Almacenará un json con información relevante';


--
-- TOC entry 214 (class 1259 OID 286401)
-- Dependencies: 215 7
-- Name: recursos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE recursos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.recursos_id_seq OWNER TO postgres;

--
-- TOC entry 2598 (class 0 OID 0)
-- Dependencies: 214
-- Name: recursos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE recursos_id_seq OWNED BY recursos.id;


--
-- TOC entry 200 (class 1259 OID 283995)
-- Dependencies: 2099 7
-- Name: sectores; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE sectores (
    id integer NOT NULL,
    nombre character varying(50) NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.sectores OWNER TO postgres;

--
-- TOC entry 2599 (class 0 OID 0)
-- Dependencies: 200
-- Name: TABLE sectores; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE sectores IS 'Sectores para clasificar el tipo de actividad que realiza el cliente';


--
-- TOC entry 2600 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN sectores.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sectores.id IS 'Identificador del sector';


--
-- TOC entry 2601 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN sectores.nombre; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sectores.nombre IS 'Nombre del sector';


--
-- TOC entry 2602 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN sectores.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sectores.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2603 (class 0 OID 0)
-- Dependencies: 200
-- Name: COLUMN sectores.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN sectores.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 201 (class 1259 OID 283998)
-- Dependencies: 200 7
-- Name: sectores_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE sectores_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.sectores_id_seq OWNER TO postgres;

--
-- TOC entry 2604 (class 0 OID 0)
-- Dependencies: 201
-- Name: sectores_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE sectores_id_seq OWNED BY sectores.id;


--
-- TOC entry 218 (class 1259 OID 286579)
-- Dependencies: 7
-- Name: seq_codigo_cotizacion; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE seq_codigo_cotizacion
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    MAXVALUE 99999
    CACHE 1
    CYCLE;


ALTER TABLE public.seq_codigo_cotizacion OWNER TO postgres;

--
-- TOC entry 2605 (class 0 OID 0)
-- Dependencies: 218
-- Name: SEQUENCE seq_codigo_cotizacion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON SEQUENCE seq_codigo_cotizacion IS 'Secuencia que maneja el código de la cotización';


--
-- TOC entry 202 (class 1259 OID 284000)
-- Dependencies: 2101 7
-- Name: tipos_productos; Type: TABLE; Schema: public; Owner: postgres; Tablespace: 
--

CREATE TABLE tipos_productos (
    id integer NOT NULL,
    descripcion character varying(50) NOT NULL,
    usuario_id integer NOT NULL,
    modificado_en timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public.tipos_productos OWNER TO postgres;

--
-- TOC entry 2606 (class 0 OID 0)
-- Dependencies: 202
-- Name: TABLE tipos_productos; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON TABLE tipos_productos IS 'Almacena la tipificación de los productos';


--
-- TOC entry 2607 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN tipos_productos.id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tipos_productos.id IS 'Identificador del tipo de productos';


--
-- TOC entry 2608 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN tipos_productos.descripcion; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tipos_productos.descripcion IS 'Descripción del tipo de productos';


--
-- TOC entry 2609 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN tipos_productos.usuario_id; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tipos_productos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';


--
-- TOC entry 2610 (class 0 OID 0)
-- Dependencies: 202
-- Name: COLUMN tipos_productos.modificado_en; Type: COMMENT; Schema: public; Owner: postgres
--

COMMENT ON COLUMN tipos_productos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


--
-- TOC entry 203 (class 1259 OID 284003)
-- Dependencies: 202 7
-- Name: tipos_productos_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE tipos_productos_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.tipos_productos_id_seq OWNER TO postgres;

--
-- TOC entry 2611 (class 0 OID 0)
-- Dependencies: 203
-- Name: tipos_productos_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE tipos_productos_id_seq OWNED BY tipos_productos.id;


SET search_path = acl, pg_catalog;

--
-- TOC entry 2056 (class 2604 OID 284005)
-- Dependencies: 163 162
-- Name: id; Type: DEFAULT; Schema: acl; Owner: postgres
--

ALTER TABLE ONLY groups ALTER COLUMN id SET DEFAULT nextval('groups_id_seq'::regclass);


--
-- TOC entry 2105 (class 2604 OID 284244)
-- Dependencies: 206 205 206
-- Name: id; Type: DEFAULT; Schema: acl; Owner: postgres
--

ALTER TABLE ONLY operation ALTER COLUMN id SET DEFAULT nextval('operation_id_seq'::regclass);


--
-- TOC entry 2061 (class 2604 OID 284008)
-- Dependencies: 167 164
-- Name: id; Type: DEFAULT; Schema: acl; Owner: postgres
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- TOC entry 2066 (class 2604 OID 284009)
-- Dependencies: 166 165
-- Name: id; Type: DEFAULT; Schema: acl; Owner: postgres
--

ALTER TABLE ONLY users_groups ALTER COLUMN id SET DEFAULT nextval('users_groups_id_seq'::regclass);


SET search_path = public, pg_catalog;

--
-- TOC entry 2130 (class 2604 OID 335918)
-- Dependencies: 223 224 224
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY auditorias ALTER COLUMN id SET DEFAULT nextval('auditorias_id_seq'::regclass);


--
-- TOC entry 2116 (class 2604 OID 285408)
-- Dependencies: 210 211 211
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cargos ALTER COLUMN id SET DEFAULT nextval('cargos_id_seq'::regclass);


--
-- TOC entry 2070 (class 2604 OID 284011)
-- Dependencies: 169 168
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY categorias_detalles_minutas ALTER COLUMN id SET DEFAULT nextval('categorias_detalles_minutas_id_seq'::regclass);


--
-- TOC entry 2072 (class 2604 OID 284012)
-- Dependencies: 171 170
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY clientes ALTER COLUMN id SET DEFAULT nextval('clientes_id_seq'::regclass);


--
-- TOC entry 2074 (class 2604 OID 284013)
-- Dependencies: 173 172
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compromisos_pagos ALTER COLUMN id SET DEFAULT nextval('compromisos_pagos_id_seq'::regclass);


--
-- TOC entry 2075 (class 2604 OID 284014)
-- Dependencies: 175 174
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY configuraciones ALTER COLUMN id SET DEFAULT nextval('configuraciones_id_seq'::regclass);


--
-- TOC entry 2076 (class 2604 OID 284015)
-- Dependencies: 177 176
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cotizaciones ALTER COLUMN id SET DEFAULT nextval('cotizaciones_id_seq'::regclass);


--
-- TOC entry 2127 (class 2604 OID 299359)
-- Dependencies: 222 221 222
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cotizaciones_formas_pagos ALTER COLUMN id SET DEFAULT nextval('cotizaciones_formas_pagos_id_seq'::regclass);


--
-- TOC entry 2122 (class 2604 OID 286516)
-- Dependencies: 216 217 217
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cotizaciones_recursos ALTER COLUMN id SET DEFAULT nextval('cotizaciones_recursos_id_seq'::regclass);


--
-- TOC entry 2081 (class 2604 OID 284016)
-- Dependencies: 179 178
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY facturas ALTER COLUMN id SET DEFAULT nextval('facturas_id_seq'::regclass);


--
-- TOC entry 2124 (class 2604 OID 299334)
-- Dependencies: 220 219 220
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY formas_pagos ALTER COLUMN id SET DEFAULT nextval('formas_pagos_id_seq'::regclass);


--
-- TOC entry 2082 (class 2604 OID 284017)
-- Dependencies: 181 180
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY frecuencias_pagos ALTER COLUMN id SET DEFAULT nextval('frecuencias_pagos_id_seq'::regclass);


--
-- TOC entry 2118 (class 2604 OID 285456)
-- Dependencies: 212 213 213
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY horarios ALTER COLUMN id SET DEFAULT nextval('horarios_id_seq'::regclass);


--
-- TOC entry 2083 (class 2604 OID 284018)
-- Dependencies: 185 182
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY minutas ALTER COLUMN id SET DEFAULT nextval('minutas_id_seq'::regclass);


--
-- TOC entry 2084 (class 2604 OID 284019)
-- Dependencies: 184 183
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY minutas_detalles ALTER COLUMN id SET DEFAULT nextval('minutas_detalles_id_seq'::regclass);


--
-- TOC entry 2086 (class 2604 OID 284020)
-- Dependencies: 187 186
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pagos ALTER COLUMN id SET DEFAULT nextval('pagos_id_seq'::regclass);


--
-- TOC entry 2087 (class 2604 OID 284021)
-- Dependencies: 189 188
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participantes ALTER COLUMN id SET DEFAULT nextval('participantes_id_seq'::regclass);


--
-- TOC entry 2088 (class 2604 OID 284022)
-- Dependencies: 191 190
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personas_contactos ALTER COLUMN id SET DEFAULT nextval('personas_contactos_id_seq'::regclass);


--
-- TOC entry 2090 (class 2604 OID 284208)
-- Dependencies: 199 192
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos ALTER COLUMN id SET DEFAULT nextval('productos_id_seq'::regclass);


--
-- TOC entry 2092 (class 2604 OID 284024)
-- Dependencies: 194 193
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_cotizaciones ALTER COLUMN id SET DEFAULT nextval('productos_cotizaciones_id_seq'::regclass);


--
-- TOC entry 2094 (class 2604 OID 284025)
-- Dependencies: 196 195
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_facturas ALTER COLUMN id SET DEFAULT nextval('productos_factura_id_seq'::regclass);


--
-- TOC entry 2097 (class 2604 OID 284026)
-- Dependencies: 198 197
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_facturas_estados ALTER COLUMN id SET DEFAULT nextval('productos_facturas_estados_id_seq'::regclass);


--
-- TOC entry 2115 (class 2604 OID 285289)
-- Dependencies: 209 208 209
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_historico_precio ALTER COLUMN id SET DEFAULT nextval('productos_historico_precio_id_seq'::regclass);


--
-- TOC entry 2120 (class 2604 OID 286406)
-- Dependencies: 215 214 215
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY recursos ALTER COLUMN id SET DEFAULT nextval('recursos_id_seq'::regclass);


--
-- TOC entry 2098 (class 2604 OID 284027)
-- Dependencies: 201 200
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY sectores ALTER COLUMN id SET DEFAULT nextval('sectores_id_seq'::regclass);


--
-- TOC entry 2100 (class 2604 OID 284028)
-- Dependencies: 203 202
-- Name: id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY tipos_productos ALTER COLUMN id SET DEFAULT nextval('tipos_productos_id_seq'::regclass);


SET search_path = acl, pg_catalog;

--
-- TOC entry 2302 (class 0 OID 284196)
-- Dependencies: 204 2323
-- Data for Name: ci_sessions; Type: TABLE DATA; Schema: acl; Owner: postgres
--

COPY ci_sessions (session_id, ip_address, user_agent, last_activity, user_data) FROM stdin;
fc852e46c03013a249965a988f3003e4	127.0.0.1	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.89 Safari/537.36	1426881102	
1e6acfa61113addb36a99cf6214e8501	127.0.0.1	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.89 Safari/537.36	1426881102	a:13:{s:9:"user_data";s:0:"";s:8:"identity";s:13:"administrator";s:8:"username";s:13:"administrator";s:5:"email";s:15:"admin@admin.com";s:7:"user_id";s:1:"3";s:14:"old_last_login";s:10:"1426873693";s:11:"permissions";a:43:{i:1000;a:9:{s:2:"id";s:4:"1000";s:5:"_name";s:8:"Clientes";s:3:"url";N;s:12:"id_operation";N;s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:4:"menu";s:4:"icon";s:29:"glyphicon glyphicon-phone-alt";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1100;a:9:{s:2:"id";s:4:"1100";s:5:"_name";s:9:"Productos";s:3:"url";N;s:12:"id_operation";N;s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:4:"menu";s:4:"icon";s:31:"glyphicon glyphicon-folder-open";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1101;a:9:{s:2:"id";s:4:"1101";s:5:"_name";s:11:"Administrar";s:3:"url";s:8:"products";s:12:"id_operation";s:4:"1100";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";N;s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1200;a:9:{s:2:"id";s:4:"1200";s:5:"_name";s:12:"Cotizaciones";s:3:"url";N;s:12:"id_operation";N;s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:4:"menu";s:4:"icon";s:23:"glyphicon glyphicon-usd";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1201;a:9:{s:2:"id";s:4:"1201";s:5:"_name";s:11:"Administrar";s:3:"url";s:10:"cotizacion";s:12:"id_operation";s:4:"1200";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";N;s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1300;a:9:{s:2:"id";s:4:"1300";s:5:"_name";s:8:"Facturas";s:3:"url";N;s:12:"id_operation";N;s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:4:"menu";s:4:"icon";s:33:"glyphicon glyphicon-shopping-cart";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1301;a:9:{s:2:"id";s:4:"1301";s:5:"_name";s:11:"Administrar";s:3:"url";s:7:"factura";s:12:"id_operation";s:4:"1300";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";N;s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1400;a:9:{s:2:"id";s:4:"1400";s:5:"_name";s:15:"Administración";s:3:"url";N;s:12:"id_operation";N;s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:4:"menu";s:4:"icon";s:22:"glyphicon glyphicon-th";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1401;a:9:{s:2:"id";s:4:"1401";s:5:"_name";s:8:"Usuarios";s:3:"url";s:9:"list_user";s:12:"id_operation";s:4:"1400";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";s:24:"glyphicon glyphicon-user";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1402;a:9:{s:2:"id";s:4:"1402";s:5:"_name";s:13:"Nuevo Usuario";s:3:"url";s:11:"create_user";s:12:"id_operation";s:4:"1401";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1403;a:9:{s:2:"id";s:4:"1403";s:5:"_name";s:14:"Editar Usuario";s:3:"url";s:9:"edit_user";s:12:"id_operation";s:4:"1401";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1202;a:9:{s:2:"id";s:4:"1202";s:5:"_name";s:17:"Crear Cotización";s:3:"url";s:18:"cotizacion_cliente";s:12:"id_operation";s:4:"1201";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1203;a:9:{s:2:"id";s:4:"1203";s:5:"_name";s:3:"Ver";s:3:"url";s:14:"cotizacion_ver";s:12:"id_operation";s:4:"1201";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:27:"glyphicon glyphicon-zoom-in";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1110;a:9:{s:2:"id";s:4:"1110";s:5:"_name";s:8:"Recursos";s:3:"url";s:0:"";s:12:"id_operation";N;s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:4:"menu";s:4:"icon";s:29:"glyphicon glyphicon-briefcase";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1111;a:9:{s:2:"id";s:4:"1111";s:5:"_name";s:11:"Administrar";s:3:"url";s:7:"recurso";s:12:"id_operation";s:4:"1110";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";s:0:"";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1404;a:9:{s:2:"id";s:4:"1404";s:5:"_name";s:17:"Grupos deUsuarios";s:3:"url";s:10:"list_group";s:12:"id_operation";s:4:"1400";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";s:24:"glyphicon glyphicon-user";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1405;a:9:{s:2:"id";s:4:"1405";s:5:"_name";s:11:"Nuevo Grupo";s:3:"url";s:12:"create_group";s:12:"id_operation";s:4:"1404";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1406;a:9:{s:2:"id";s:4:"1406";s:5:"_name";s:12:"Editar Grupo";s:3:"url";s:10:"edit_group";s:12:"id_operation";s:4:"1404";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1407;a:9:{s:2:"id";s:4:"1407";s:5:"_name";s:20:"Operaciones de Grupo";s:3:"url";s:15:"group_operation";s:12:"id_operation";s:4:"1404";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-list";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1211;a:9:{s:2:"id";s:4:"1211";s:5:"_name";s:14:"Formas de Pago";s:3:"url";s:10:"forma_pago";s:12:"id_operation";s:4:"1200";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";s:0:"";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1409;a:9:{s:2:"id";s:4:"1409";s:5:"_name";s:36:"Activacion-Desactivacion de Usuarios";s:3:"url";s:7:"io_user";s:12:"id_operation";s:4:"1401";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:9:"method_js";s:4:"icon";s:25:"glyphicon glyphicon-check";s:9:"target_on";s:7:"content";s:5:"order";s:1:"1";}i:1070;a:9:{s:2:"id";s:4:"1070";s:5:"_name";s:8:"Horarios";s:3:"url";s:9:"schedules";s:12:"id_operation";s:4:"1000";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";s:33:"glyphicon glyphicon-icon-calendar";s:9:"target_on";s:7:"content";s:5:"order";s:1:"2";}i:1001;a:9:{s:2:"id";s:4:"1001";s:5:"_name";s:11:"Administrar";s:3:"url";s:11:"list_client";s:12:"id_operation";s:4:"1000";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";N;s:9:"target_on";s:7:"content";s:5:"order";s:1:"2";}i:1150;a:9:{s:2:"id";s:4:"1150";s:5:"_name";s:5:"Tipos";s:3:"url";s:12:"productTypes";s:12:"id_operation";s:4:"1100";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";s:32:"glyphicon glyphicon-icon-sampler";s:9:"target_on";s:7:"content";s:5:"order";s:1:"2";}i:1050;a:9:{s:2:"id";s:4:"1050";s:5:"_name";s:6:"Cargos";s:3:"url";s:9:"positions";s:12:"id_operation";s:4:"1000";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";s:36:"glyphicon glyphicon-icon-pin-classic";s:9:"target_on";s:7:"content";s:5:"order";s:1:"2";}i:1060;a:9:{s:2:"id";s:4:"1060";s:5:"_name";s:8:"Sectores";s:3:"url";s:7:"sectors";s:12:"id_operation";s:4:"1000";s:9:"render_on";s:7:"sidebar";s:11:"visual_type";s:7:"submenu";s:4:"icon";s:43:"glyphicon glyphicon-icon-vector-path-square";s:9:"target_on";s:7:"content";s:5:"order";s:1:"2";}i:1072;a:9:{s:2:"id";s:4:"1072";s:5:"_name";s:6:"Editar";s:3:"url";s:13:"schedule/edit";s:12:"id_operation";s:4:"1070";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1004;a:9:{s:2:"id";s:4:"1004";s:5:"_name";s:9:"Contactos";s:3:"url";s:9:"contactos";s:12:"id_operation";s:4:"1001";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-user";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1112;a:9:{s:2:"id";s:4:"1112";s:5:"_name";s:13:"Nuevo Recurso";s:3:"url";s:11:"recurso/add";s:12:"id_operation";s:4:"1111";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1113;a:9:{s:2:"id";s:4:"1113";s:5:"_name";s:14:"Editar Recurso";s:3:"url";s:12:"recurso/edit";s:12:"id_operation";s:4:"1111";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1213;a:9:{s:2:"id";s:4:"1213";s:5:"_name";s:20:"Editar Forma de Pago";s:3:"url";s:15:"forma_pago/edit";s:12:"id_operation";s:4:"1211";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1212;a:9:{s:2:"id";s:4:"1212";s:5:"_name";s:19:"Nueva Forma de Pago";s:3:"url";s:14:"forma_pago/add";s:12:"id_operation";s:4:"1211";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1062;a:9:{s:2:"id";s:4:"1062";s:5:"_name";s:6:"Editar";s:3:"url";s:11:"sector/edit";s:12:"id_operation";s:4:"1060";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1151;a:9:{s:2:"id";s:4:"1151";s:5:"_name";s:22:"Nuevo Tipo de Producto";s:3:"url";s:16:"productTypes/add";s:12:"id_operation";s:4:"1150";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1152;a:9:{s:2:"id";s:4:"1152";s:5:"_name";s:23:"Editar Tipo de Producto";s:3:"url";s:17:"productTypes/edit";s:12:"id_operation";s:4:"1150";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1102;a:9:{s:2:"id";s:4:"1102";s:5:"_name";s:14:"Nuevo Producto";s:3:"url";s:11:"product/add";s:12:"id_operation";s:4:"1101";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1103;a:9:{s:2:"id";s:4:"1103";s:5:"_name";s:15:"Editar Producto";s:3:"url";s:12:"product/edit";s:12:"id_operation";s:4:"1101";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1061;a:9:{s:2:"id";s:4:"1061";s:5:"_name";s:5:"Nuevo";s:3:"url";s:10:"sector/add";s:12:"id_operation";s:4:"1060";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1051;a:9:{s:2:"id";s:4:"1051";s:5:"_name";s:5:"Nuevo";s:3:"url";s:12:"position/add";s:12:"id_operation";s:4:"1050";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1071;a:9:{s:2:"id";s:4:"1071";s:5:"_name";s:5:"Nuevo";s:3:"url";s:12:"schedule/add";s:12:"id_operation";s:4:"1070";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1052;a:9:{s:2:"id";s:4:"1052";s:5:"_name";s:6:"Editar";s:3:"url";s:13:"position/edit";s:12:"id_operation";s:4:"1050";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1002;a:9:{s:2:"id";s:4:"1002";s:5:"_name";s:13:"Nuevo Cliente";s:3:"url";s:13:"create_client";s:12:"id_operation";s:4:"1001";s:9:"render_on";s:9:"actionbar";s:11:"visual_type";s:4:"link";s:4:"icon";s:24:"glyphicon glyphicon-plus";s:9:"target_on";s:7:"content";s:5:"order";N;}i:1003;a:9:{s:2:"id";s:4:"1003";s:5:"_name";s:14:"Editar Cliente";s:3:"url";s:11:"edit_client";s:12:"id_operation";s:4:"1001";s:9:"render_on";s:4:"grid";s:11:"visual_type";s:4:"link";s:4:"icon";s:26:"glyphicon glyphicon-pencil";s:9:"target_on";s:7:"content";s:5:"order";N;}}s:7:"sidebar";a:6:{i:1000;a:3:{s:5:"label";s:8:"Clientes";s:4:"icon";s:29:"glyphicon glyphicon-phone-alt";s:7:"submenu";a:4:{i:1070;a:2:{s:5:"label";s:8:"Horarios";s:3:"url";s:9:"schedules";}i:1001;a:2:{s:5:"label";s:11:"Administrar";s:3:"url";s:11:"list_client";}i:1050;a:2:{s:5:"label";s:6:"Cargos";s:3:"url";s:9:"positions";}i:1060;a:2:{s:5:"label";s:8:"Sectores";s:3:"url";s:7:"sectors";}}}i:1100;a:3:{s:5:"label";s:9:"Productos";s:4:"icon";s:31:"glyphicon glyphicon-folder-open";s:7:"submenu";a:2:{i:1101;a:2:{s:5:"label";s:11:"Administrar";s:3:"url";s:8:"products";}i:1150;a:2:{s:5:"label";s:5:"Tipos";s:3:"url";s:12:"productTypes";}}}i:1200;a:3:{s:5:"label";s:12:"Cotizaciones";s:4:"icon";s:23:"glyphicon glyphicon-usd";s:7:"submenu";a:2:{i:1201;a:2:{s:5:"label";s:11:"Administrar";s:3:"url";s:10:"cotizacion";}i:1211;a:2:{s:5:"label";s:14:"Formas de Pago";s:3:"url";s:10:"forma_pago";}}}i:1300;a:3:{s:5:"label";s:8:"Facturas";s:4:"icon";s:33:"glyphicon glyphicon-shopping-cart";s:7:"submenu";a:1:{i:1301;a:2:{s:5:"label";s:11:"Administrar";s:3:"url";s:7:"factura";}}}i:1400;a:3:{s:5:"label";s:15:"Administración";s:4:"icon";s:22:"glyphicon glyphicon-th";s:7:"submenu";a:2:{i:1401;a:2:{s:5:"label";s:8:"Usuarios";s:3:"url";s:9:"list_user";}i:1404;a:2:{s:5:"label";s:17:"Grupos deUsuarios";s:3:"url";s:10:"list_group";}}}i:1110;a:3:{s:5:"label";s:8:"Recursos";s:4:"icon";s:29:"glyphicon glyphicon-briefcase";s:7:"submenu";a:1:{i:1111;a:2:{s:5:"label";s:11:"Administrar";s:3:"url";s:7:"recurso";}}}}s:7:"role_id";s:1:"1";s:9:"role_name";s:13:"Administrador";s:15:"user_first_name";s:5:"Admin";s:14:"user_last_name";s:8:"istrator";s:10:"user_email";s:15:"admin@admin.com";}
69de67c6dd99ef6e7782c31e25c2c4e1	127.0.0.1	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36	1426888284	
51a91b25d4b6283c100d8e8995109dd0	127.0.0.1	Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/41.0.2272.101 Safari/537.36	1426888284	
\.


--
-- TOC entry 2305 (class 0 OID 284273)
-- Dependencies: 207 2323
-- Data for Name: group_operation; Type: TABLE DATA; Schema: acl; Owner: postgres
--

COPY group_operation (_date, id_operation, id_group) FROM stdin;
2015-01-13	1000	1
2015-01-13	1001	1
2015-01-13	1002	1
2015-01-13	1003	1
2015-01-20	1100	1
2015-01-20	1101	1
2015-01-20	1200	1
2015-01-20	1201	1
2015-01-20	1300	1
2015-01-20	1301	1
2015-01-21	1150	1
2015-01-23	1151	1
2015-01-23	1152	1
2015-01-23	1102	1
2015-01-23	1103	1
2015-01-23	1050	1
2015-01-23	1051	1
2015-01-23	1052	1
2015-01-23	1060	1
2015-01-23	1061	1
2015-01-23	1062	1
2015-01-23	1070	1
2015-01-23	1071	1
2015-01-23	1072	1
2015-01-30	1004	1
2015-02-09	1400	1
2015-02-09	1401	1
2015-02-09	1402	1
2015-02-09	1403	1
2015-02-13	1202	1
2015-02-13	1203	1
2015-02-19	1110	1
2015-02-19	1111	1
2015-02-19	1112	1
2015-02-19	1113	1
2015-02-26	1404	1
2015-02-26	1405	1
2015-02-26	1406	1
2015-02-27	1407	1
2015-02-27	1211	1
2015-02-27	1212	1
2015-02-27	1213	1
2015-03-02	1408	1
2015-03-03	1000	2
2015-03-03	1001	2
2015-03-03	1002	2
2015-03-03	1003	2
2015-03-03	1004	2
2015-03-03	1050	2
2015-03-03	1051	2
2015-03-03	1052	2
2015-03-03	1060	2
2015-03-03	1061	2
2015-03-03	1062	2
2015-03-03	1070	2
2015-03-03	1071	2
2015-03-03	1072	2
2015-03-05	1409	1
\.


--
-- TOC entry 2260 (class 0 OID 283842)
-- Dependencies: 162 2323
-- Data for Name: groups; Type: TABLE DATA; Schema: acl; Owner: postgres
--

COPY groups (id, name, description, _disabled, usuario_id, modificado_en) FROM stdin;
2	QA	Prueba funcionalidades del sistema	f	0	2015-03-20 16:08:36.031739
1	Administrador	Rol de administrador de sistema	t	0	2015-03-20 16:08:36.031739
3	Prueba	Rol de prueba de usuario	f	0	2015-03-20 16:08:36.031739
4	Invitado	Usuarios sin permisos	f	3	2015-03-20 16:35:48
5	Otro grupo	Otro grupo	f	3	2015-03-20 17:08:11
\.


--
-- TOC entry 2612 (class 0 OID 0)
-- Dependencies: 163
-- Name: groups_id_seq; Type: SEQUENCE SET; Schema: acl; Owner: postgres
--

SELECT pg_catalog.setval('groups_id_seq', 5, true);


--
-- TOC entry 2304 (class 0 OID 284241)
-- Dependencies: 206 2323
-- Data for Name: operation; Type: TABLE DATA; Schema: acl; Owner: postgres
--

COPY operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) FROM stdin;
1110	Recursos		1	1	glyphicon glyphicon-briefcase			\N	sidebar	menu	content		
1111	Administrar	recurso	1	1				1110	sidebar	submenu	content		
1112	Nuevo Recurso	recurso/add	\N	1	glyphicon glyphicon-plus			1111	actionbar	link	content		
1113	Editar Recurso	recurso/edit	\N	1	glyphicon glyphicon-pencil			1111	grid	link	content		
1003	Editar Cliente	edit_client	\N	1	glyphicon glyphicon-pencil	\N	\N	1001	grid	link	content	\N	\N
1002	Nuevo Cliente	create_client	\N	1	glyphicon glyphicon-plus	\N	\N	1001	actionbar	link	content	\N	\N
1000	Clientes	\N	1	1	glyphicon glyphicon-phone-alt	\N	\N	\N	sidebar	menu	content	\N	\N
1100	Productos	\N	1	1	glyphicon glyphicon-folder-open	\N	\N	\N	sidebar	menu	content	\N	\N
1101	Administrar	products	1	1	\N	\N	\N	1100	sidebar	submenu	content	\N	\N
1200	Cotizaciones	\N	1	1	glyphicon glyphicon-usd	\N	\N	\N	sidebar	menu	content	\N	\N
1201	Administrar	cotizacion	1	1	\N	\N	\N	1200	sidebar	submenu	content	\N	\N
1300	Facturas	\N	1	1	glyphicon glyphicon-shopping-cart	\N	\N	\N	sidebar	menu	content	\N	\N
1301	Administrar	factura	1	1	\N	\N	\N	1300	sidebar	submenu	content	\N	\N
1203	Ver	cotizacion_ver	1	1	glyphicon glyphicon-zoom-in			1201	grid	link	content		
1150	Tipos	productTypes	2	1	glyphicon glyphicon-icon-sampler	\N	\N	1100	sidebar	submenu	content	\N	\N
1211	Formas de Pago	forma_pago	1	1				1200	sidebar	submenu	content		
1151	Nuevo Tipo de Producto	productTypes/add	\N	1	glyphicon glyphicon-plus	\N	\N	1150	actionbar	link	content	\N	\N
1152	Editar Tipo de Producto	productTypes/edit	\N	1	glyphicon glyphicon-pencil	\N	\N	1150	grid	link	content	\N	\N
1102	Nuevo Producto	product/add	\N	1	glyphicon glyphicon-plus	\N	\N	1101	actionbar	link	content	\N	\N
1103	Editar Producto	product/edit	\N	1	glyphicon glyphicon-pencil	\N	\N	1101	grid	link	content	\N	\N
1050	Cargos	positions	2	1	glyphicon glyphicon-icon-pin-classic	\N	\N	1000	sidebar	submenu	content	\N	\N
1051	Nuevo	position/add	\N	1	glyphicon glyphicon-plus	\N	\N	1050	actionbar	link	content	\N	\N
1052	Editar	position/edit	\N	1	glyphicon glyphicon-pencil	\N	\N	1050	grid	link	content	\N	\N
1060	Sectores	sectors	2	1	glyphicon glyphicon-icon-vector-path-square	\N	\N	1000	sidebar	submenu	content	\N	\N
1061	Nuevo	sector/add	\N	1	glyphicon glyphicon-plus	\N	\N	1060	actionbar	link	content	\N	\N
1062	Editar	sector/edit	\N	1	glyphicon glyphicon-pencil	\N	\N	1060	grid	link	content	\N	\N
1070	Horarios	schedules	2	1	glyphicon glyphicon-icon-calendar	\N	\N	1000	sidebar	submenu	content	\N	\N
1071	Nuevo	schedule/add	\N	1	glyphicon glyphicon-plus	\N	\N	1070	actionbar	link	content	\N	\N
1072	Editar	schedule/edit	\N	1	glyphicon glyphicon-pencil	\N	\N	1070	grid	link	content	\N	\N
1001	Administrar	list_client	2	1	\N	\N	\N	1000	sidebar	submenu	content	\N	\N
1004	Contactos	contactos	\N	1	glyphicon glyphicon-user	\N	\N	1001	grid	link	content	\N	\N
1401	Usuarios	list_user	1	1	glyphicon glyphicon-user	\N	\N	1400	sidebar	submenu	content	\N	\N
1403	Editar Usuario	edit_user	1	1	glyphicon glyphicon-pencil	\N	\N	1401	grid	link	content	\N	\N
1400	Administración	\N	1	1	glyphicon glyphicon-th	\N	\N	\N	sidebar	menu	content	\N	\N
1202	Crear Cotización	cotizacion_cliente	1	1	glyphicon glyphicon-plus			1201	actionbar	link	content		
1404	Grupos deUsuarios	list_group	1	1	glyphicon glyphicon-user	\N	\N	1400	sidebar	submenu	content	Listado para la gestion de grupo de usuarios	\N
1405	Nuevo Grupo	create_group	1	1	glyphicon glyphicon-plus	\N	\N	1404	actionbar	link	content	Permite la creacion de un nuevo grupo de usuario	\N
1213	Editar Forma de Pago	forma_pago/edit	\N	1	glyphicon glyphicon-pencil			1211	grid	link	content		
1212	Nueva Forma de Pago	forma_pago/add	\N	1	glyphicon glyphicon-plus			1211	actionbar	link	content		
1406	Editar Grupo	edit_group	1	1	glyphicon glyphicon-pencil	\N	\N	1404	grid	link	content	Permite  editar los datos de los grupos de usuarios	\N
1407	Operaciones de Grupo	group_operation	1	1	glyphicon glyphicon-list	\N	\N	1404	grid	link	content	Permite  editar los datos de los grupos de usuarios	\N
1408	Activacion-Desactivacion de Grupos	io_operation	1	0	glyphicon glyphicon-check	\N	\N	1404	grid	link	content	Permite activar o desactivar usuarios asociados al grupo	\N
1402	Nuevo Usuario	create_user	1	1	glyphicon glyphicon-plus	\N	\N	1401	actionbar	link	content	\N	\N
1409	Activacion-Desactivacion de Usuarios	io_user	1	1	glyphicon glyphicon-check	\N	\N	1401	grid	method_js	content	Permite activar o desactivar usuarios	\N
\.


--
-- TOC entry 2613 (class 0 OID 0)
-- Dependencies: 205
-- Name: operation_id_seq; Type: SEQUENCE SET; Schema: acl; Owner: postgres
--

SELECT pg_catalog.setval('operation_id_seq', 1, true);


--
-- TOC entry 2262 (class 0 OID 283861)
-- Dependencies: 164 2323
-- Data for Name: users; Type: TABLE DATA; Schema: acl; Owner: postgres
--

COPY users (id, ip_address, username, password, salt, email, activation_code, forgotten_password_code, forgotten_password_time, remember_code, created_on, last_login, active, first_name, last_name, direccion, phone, cedula, usuario_id, modificado_en) FROM stdin;
5	127.0.0.1	asasa	$2y$08$VrY5LWAOs/pSRGdlAQaXFuXfuJNA96hOg1s2yMhc1JoW/u4Nq8i66	\N	asasa@asad.com	\N	\N	\N	\N	1424812241	\N	1	Juan	Lopez de la  caridad	wdqxwedw	(0412) 123-1212	V-17252780	0	2015-03-20 15:53:49.238739
4	127.0.0.1	jlara	$2y$08$YmoytJn5bMHm7ZFwTRPKjuNP0P76ebxGQkGPr7mlI1oFzL./LwNpO	\N	jlara@ast.com.ve	\N	\N	\N	\N	1423475555	1425671726	1	jefferson arturo	lara molinas	los teques	(0414) 466-4081	V-17042979	0	2015-03-20 15:53:49.238739
9	127.0.0.1	asdasd	$2y$08$oHbmvWEod8RZIIKli8gjgeAeGsoRx3xi9CAA/XuhfFB0A2rU7u6T2	\N	asdasd@asda.sdda	\N	\N	\N	\N	1426871000	\N	1	asdasdasdas	asdasda	0	(1245) 346-57565	V-13231231	0	2015-03-20 15:53:49.238739
3	127.0.0.1	administrator	$2y$08$AJjXGh9uP8i/zJ8v7UJqneHxH0158OzUGjybRcUBMF/fd2V/MxTP2		admin@admin.com		\N	\N	\N	1268889823	1426881118	1	Admin	istrator		(0414) 222-2222	V-15393859	0	2015-03-20 15:53:49.238739
6	127.0.0.1	frederickdanielb	$2y$08$O10NqoFq3fzzWXJyCqPufenJ.gMs2w2TT.6C9kQX0LCt.3zGDpduu	\N	frederickdanielb@gmail.com	\N	\N	\N	\N	1425418058	1425680089	1	Frederick	Bustamante	Calle principal	(0414) 280-489	V-15393859	0	2015-03-20 15:53:49.238739
7	127.0.0.1	dserrano.ast	$2y$08$vPWlJEV64/2Xy19LtIJQ6Oei.hx0oiT1xY5oOdl7AwbHgpVBON4K.	\N	dserrano.ast@gmail.com	\N	x.-n86qHxdgQu14jZrCyOO2909134a0c40503459	1426264666	\N	1426089316	1426112778	1	Darwin	Serrano	asdasdad	(1231) 231-23121	V-15759958	0	2015-03-20 15:53:49.238739
8	127.0.0.1	uprueba	$2y$08$J6SnbpTrNL7MSPzh7vwkmOB.BImIEiCdpHSTFD4ePmWaVrf1vx1Pi	\N	uprueba@ast.com.ve	\N	\N	\N	\N	1426089944	\N	1	Usuario	Prueba	Dirección de prueba	(1293) 812-30911	V-12345678	3	2015-03-20 16:15:26
\.


--
-- TOC entry 2263 (class 0 OID 283869)
-- Dependencies: 165 2323
-- Data for Name: users_groups; Type: TABLE DATA; Schema: acl; Owner: postgres
--

COPY users_groups (id, user_id, group_id) FROM stdin;
36	3	1
42	4	1
45	6	1
46	5	1
47	7	1
50	9	3
51	8	3
\.


--
-- TOC entry 2614 (class 0 OID 0)
-- Dependencies: 166
-- Name: users_groups_id_seq; Type: SEQUENCE SET; Schema: acl; Owner: postgres
--

SELECT pg_catalog.setval('users_groups_id_seq', 51, true);


--
-- TOC entry 2615 (class 0 OID 0)
-- Dependencies: 167
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: acl; Owner: postgres
--

SELECT pg_catalog.setval('users_id_seq', 9, true);


SET search_path = public, pg_catalog;

--
-- TOC entry 2322 (class 0 OID 335915)
-- Dependencies: 224 2323
-- Data for Name: auditorias; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY auditorias (id, usuario_id, esquema, tabla, registro_id, fecha, transaccion, datos_previos, datos_actuales, info_adicional) FROM stdin;
319	3	\N	\N	\N	2015-03-18 16:41:51.60716	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:51:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/forma_pago"}
320	3	public	cotizaciones_formas_pagos	44	2015-03-18 16:41:51.638038	INSERT	{}	{"id": "44", "cotizacion_id": "95", "forma_pago_id": "1", "tipo_plazo": "entrega", "dias": "1", "porcentaje": "35", "usuario_id": "3", "modificado_en": "2015-03-18 16:51:28", "intervalo": "{"dias":""}"}	\N
321	3	\N	\N	\N	2015-03-18 16:45:06.584115	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:54:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
322	3	\N	\N	\N	2015-03-18 16:45:06.632587	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:54:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
323	3	\N	\N	\N	2015-03-18 16:45:06.639705	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:54:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
324	3	\N	\N	\N	2015-03-18 16:45:06.649066	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:54:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
325	3	\N	\N	\N	2015-03-18 16:45:11.831563	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:54:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
326	3	\N	\N	\N	2015-03-18 16:45:11.987319	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:54:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
327	3	\N	\N	\N	2015-03-18 16:45:11.991821	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:54:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
328	3	\N	\N	\N	2015-03-18 16:46:06.476027	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:55:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
329	3	\N	\N	\N	2015-03-18 16:46:06.512021	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:55:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
330	3	\N	\N	\N	2015-03-18 16:46:06.512593	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:55:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
331	3	\N	\N	\N	2015-03-18 16:46:06.513002	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:55:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
332	3	\N	\N	\N	2015-03-18 16:46:32.738948	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:56:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
333	3	\N	\N	\N	2015-03-18 16:49:44.811847	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:59:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
334	3	\N	\N	\N	2015-03-18 16:49:44.910542	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:59:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
335	3	\N	\N	\N	2015-03-18 16:49:44.919188	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:59:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
336	3	\N	\N	\N	2015-03-18 16:50:09.504892	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:59:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
337	3	\N	\N	\N	2015-03-18 16:50:09.577996	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:59:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
338	3	\N	\N	\N	2015-03-18 16:50:09.58976	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:59:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
339	3	\N	\N	\N	2015-03-18 16:50:09.590635	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:59:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
340	3	\N	\N	\N	2015-03-18 16:50:12.859081	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 16:59:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
341	3	\N	\N	\N	2015-03-18 16:51:01.788358	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:00:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
342	3	\N	\N	\N	2015-03-18 16:51:44.210107	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:01:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
343	3	\N	\N	\N	2015-03-18 16:56:47.290439	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:06:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
344	0	public	productos_cotizaciones	47	2015-03-18 16:56:47.3269	DELETE	{"id": "47", "cotizacion_id": "95", "producto_id": "1", "precio": "150000", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-18 16:37:12.453726", "cantidad": "1", "precio_total": "150000"}	{}	\N
345	0	public	cotizaciones_recursos	18	2015-03-18 16:56:47.347879	DELETE	{"id": "18", "cotizacion_id": "95", "recurso_id": "3", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-18 16:35:39.982673", "orden": "3"}	{}	\N
346	0	public	cotizaciones_recursos	14	2015-03-18 16:56:47.347879	DELETE	{"id": "14", "cotizacion_id": "95", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-10 12:26:41.936971", "orden": "1"}	{}	\N
347	0	public	cotizaciones_recursos	15	2015-03-18 16:56:47.347879	DELETE	{"id": "15", "cotizacion_id": "95", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-10 12:26:59.404667", "orden": "2"}	{}	\N
348	3	\N	\N	\N	2015-03-18 16:58:20.152469	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:07:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
350	3	\N	\N	\N	2015-03-18 16:59:05.207108	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:08:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
375	3	\N	\N	\N	2015-03-18 17:06:02.717713	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
377	3	\N	\N	\N	2015-03-18 17:06:02.794184	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
378	3	\N	\N	\N	2015-03-18 17:06:03.032176	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
351	3	\N	\N	\N	2015-03-18 16:59:57.218166	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:09:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
355	3	\N	\N	\N	2015-03-18 17:05:34.917852	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
365	3	\N	\N	\N	2015-03-18 17:05:38.335191	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
367	3	\N	\N	\N	2015-03-18 17:05:38.444705	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
368	3	\N	\N	\N	2015-03-18 17:05:38.741573	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
371	3	\N	\N	\N	2015-03-18 17:05:46.506237	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
374	3	\N	\N	\N	2015-03-18 17:06:02.580227	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
376	3	\N	\N	\N	2015-03-18 17:06:02.719495	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
392	3	\N	\N	\N	2015-03-18 17:09:52.493732	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
394	3	\N	\N	\N	2015-03-18 17:09:52.958688	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
395	3	\N	\N	\N	2015-03-18 17:09:54.430399	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
396	3	\N	\N	\N	2015-03-18 17:09:55.392426	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
397	3	\N	\N	\N	2015-03-18 17:09:58.333116	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
398	3	public	cotizaciones_recursos	19	2015-03-18 17:09:58.397081	INSERT	{}	{"id": "19", "cotizacion_id": "99", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:09:58.397081", "orden": "1"}	\N
399	3	\N	\N	\N	2015-03-18 17:09:58.479206	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
353	3	\N	\N	\N	2015-03-18 17:05:34.776883	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
364	3	\N	\N	\N	2015-03-18 17:05:38.158874	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
366	3	\N	\N	\N	2015-03-18 17:05:38.335778	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
354	3	\N	\N	\N	2015-03-18 17:05:34.914975	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
407	3	\N	\N	\N	2015-03-18 17:10:15.685726	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
408	3	public	cotizaciones	99	2015-03-18 17:10:15.746154	UPDATE	{"id": "99", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:05:38.001757", "fecha_expiracion": "2015-03-28 17:05:38.001757", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-18 17:05:38.001757", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id": "99", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:05:38.001757", "fecha_expiracion": "2015-03-28 00:00:00", "descuento": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:05:38.001757", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": "false"}	\N
409	3	\N	\N	\N	2015-03-18 17:10:15.839939	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
410	3	\N	\N	\N	2015-03-18 17:10:18.299658	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
411	3	public	cotizaciones	99	2015-03-18 17:10:18.32943	UPDATE	{"id": "99", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:05:38.001757", "fecha_expiracion": "2015-03-28 00:00:00", "descuento": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:05:38.001757", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": "false"}	{"id": "99", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:05:38.001757", "fecha_expiracion": "2015-03-28 00:00:00", "descuento": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:05:38.001757", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "sdasdasdasda", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": "false"}	\N
412	3	\N	\N	\N	2015-03-18 17:10:18.418074	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
356	3	\N	\N	\N	2015-03-18 17:05:34.918231	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
357	3	\N	\N	\N	2015-03-18 17:05:37.874072	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
358	0	public	cotizaciones_formas_pagos	31	2015-03-18 17:05:37.925086	DELETE	{"id": "31", "cotizacion_id": "95", "forma_pago_id": "1", "tipo_plazo": "inicio", "dias": "1", "porcentaje": "50", "usuario_id": "3", "modificado_en": "2015-03-02 10:44:23", "intervalo": ""}	{}	\N
359	0	public	cotizaciones_formas_pagos	32	2015-03-18 17:05:37.925086	DELETE	{"id": "32", "cotizacion_id": "95", "forma_pago_id": "3", "tipo_plazo": "dias", "dias": "15", "porcentaje": "15", "usuario_id": "3", "modificado_en": "2015-03-02 10:44:43", "intervalo": ""}	{}	\N
360	0	public	cotizaciones_formas_pagos	44	2015-03-18 17:05:37.925086	DELETE	{"id": "44", "cotizacion_id": "95", "forma_pago_id": "1", "tipo_plazo": "entrega", "dias": "1", "porcentaje": "35", "usuario_id": "3", "modificado_en": "2015-03-18 16:51:28", "intervalo": "{"dias":""}"}	{}	\N
361	0	public	cotizaciones	95	2015-03-18 17:05:37.945547	DELETE	{"id": "95", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-02-27 15:32:58.378629", "fecha_expiracion": "2015-03-09 00:00:00", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-02-27 15:32:58.378629", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": "true"}	{}	\N
362	3	public	cotizaciones	99	2015-03-18 17:05:38.001757	INSERT	{}	{"id": "99", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:05:38.001757", "fecha_expiracion": "2015-03-28 17:05:38.001757", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-18 17:05:38.001757", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	\N
363	3	\N	\N	\N	2015-03-18 17:05:38.148681	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
369	3	\N	\N	\N	2015-03-18 17:05:46.451432	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
381	3	\N	\N	\N	2015-03-18 17:09:45.113346	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
390	3	\N	\N	\N	2015-03-18 17:09:52.245887	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
393	3	\N	\N	\N	2015-03-18 17:09:52.499526	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
405	3	\N	\N	\N	2015-03-18 17:10:06.11841	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/forma_pago"}
406	3	public	cotizaciones_formas_pagos	45	2015-03-18 17:10:06.135619	INSERT	{}	{"id": "45", "cotizacion_id": "99", "forma_pago_id": "1", "tipo_plazo": "entrega", "dias": "1", "porcentaje": "100", "usuario_id": "3", "modificado_en": "2015-03-18 17:19:42", "intervalo": "{"dias":""}"}	\N
370	3	\N	\N	\N	2015-03-18 17:05:46.50542	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
372	3	\N	\N	\N	2015-03-18 17:05:46.801237	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
380	3	\N	\N	\N	2015-03-18 17:09:44.913374	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
382	3	\N	\N	\N	2015-03-18 17:09:45.117865	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
383	3	\N	\N	\N	2015-03-18 17:09:45.231461	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
384	3	\N	\N	\N	2015-03-18 17:09:45.583003	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
385	3	\N	\N	\N	2015-03-18 17:09:47.47317	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
386	3	\N	\N	\N	2015-03-18 17:09:48.875261	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
387	3	\N	\N	\N	2015-03-18 17:09:51.962796	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
388	3	public	productos_cotizaciones	48	2015-03-18 17:09:51.986661	INSERT	{}	{"id": "48", "cotizacion_id": "99", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-18 17:09:51.986661", "cantidad": "1", "precio_total": "500"}	\N
389	3	\N	\N	\N	2015-03-18 17:09:52.205127	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
400	3	\N	\N	\N	2015-03-18 17:09:58.509495	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
401	3	\N	\N	\N	2015-03-18 17:09:58.644651	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
373	3	\N	\N	\N	2015-03-18 17:06:02.57672	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:15:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
379	3	\N	\N	\N	2015-03-18 17:09:44.872761	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
402	3	\N	\N	\N	2015-03-18 17:09:58.718926	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
391	3	\N	\N	\N	2015-03-18 17:09:52.492871	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
403	3	\N	\N	\N	2015-03-18 17:09:58.759827	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
404	3	\N	\N	\N	2015-03-18 17:09:58.949328	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:19:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
413	3	\N	\N	\N	2015-03-18 17:10:53.581655	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
414	3	\N	\N	\N	2015-03-18 17:10:53.685891	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
415	3	\N	\N	\N	2015-03-18 17:10:53.685294	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
416	3	\N	\N	\N	2015-03-18 17:10:53.708755	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
417	3	\N	\N	\N	2015-03-18 17:11:08.90841	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
418	3	\N	\N	\N	2015-03-18 17:11:08.922339	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
419	3	\N	\N	\N	2015-03-18 17:11:09.078962	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
420	3	\N	\N	\N	2015-03-18 17:11:09.079688	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
421	3	\N	\N	\N	2015-03-18 17:11:09.159448	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
422	3	\N	\N	\N	2015-03-18 17:11:09.409434	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
423	3	\N	\N	\N	2015-03-18 17:11:11.174974	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:20:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/pdf_cotizacion\\/preview\\/99"}
424	3	\N	\N	\N	2015-03-18 17:20:03.55384	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
425	3	\N	\N	\N	2015-03-18 17:20:03.604966	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
426	3	\N	\N	\N	2015-03-18 17:20:03.606047	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_producto"}
427	3	\N	\N	\N	2015-03-18 17:20:03.626977	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
428	3	public	productos_cotizaciones	48	2015-03-18 17:20:03.638751	DELETE	{"id": "48", "cotizacion_id": "99", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-18 17:09:51.986661", "cantidad": "1", "precio_total": "500"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_producto"}
429	3	\N	\N	\N	2015-03-18 17:20:03.773792	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
430	3	\N	\N	\N	2015-03-18 17:20:03.782941	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
431	3	\N	\N	\N	2015-03-18 17:20:03.875502	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
432	3	\N	\N	\N	2015-03-18 17:20:04.153976	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
433	3	\N	\N	\N	2015-03-18 17:20:16.968738	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
434	3	\N	\N	\N	2015-03-18 17:20:16.982203	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
435	3	\N	\N	\N	2015-03-18 17:20:16.989129	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
436	3	\N	\N	\N	2015-03-18 17:20:17.00302	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
438	3	\N	\N	\N	2015-03-18 17:20:17.143752	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
444	3	\N	\N	\N	2015-03-18 17:22:03.339067	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
437	3	public	cotizaciones_recursos	19	2015-03-18 17:20:17.014477	DELETE	{"id": "19", "cotizacion_id": "99", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:09:58.397081", "orden": "1"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
439	3	\N	\N	\N	2015-03-18 17:20:17.153313	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
451	3	\N	\N	\N	2015-03-18 17:22:11.061576	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
453	3	\N	\N	\N	2015-03-18 17:22:11.185386	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
440	3	\N	\N	\N	2015-03-18 17:20:17.223398	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
441	3	\N	\N	\N	2015-03-18 17:20:17.49201	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:29:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
452	3	\N	\N	\N	2015-03-18 17:22:11.182176	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
442	3	\N	\N	\N	2015-03-18 17:20:30.666188	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:30:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/forma_pago\\/45"}
443	3	public	cotizaciones_formas_pagos	45	2015-03-18 17:20:30.709219	DELETE	{"id": "45", "cotizacion_id": "99", "forma_pago_id": "1", "tipo_plazo": "entrega", "dias": "1", "porcentaje": "100", "usuario_id": "3", "modificado_en": "2015-03-18 17:19:42", "intervalo": "{"dias":""}"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:30:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/forma_pago\\/45"}
454	3	\N	\N	\N	2015-03-18 17:22:11.247742	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
455	3	\N	\N	\N	2015-03-18 17:22:11.484816	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
456	3	\N	\N	\N	2015-03-18 17:22:13.551901	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
457	3	\N	\N	\N	2015-03-18 17:22:14.431433	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
458	3	\N	\N	\N	2015-03-18 17:22:14.544071	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
459	3	\N	\N	\N	2015-03-18 17:22:16.403338	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
460	3	public	cotizaciones_recursos	20	2015-03-18 17:22:16.47059	INSERT	{}	{"id": "20", "cotizacion_id": "99", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:22:16.47059", "orden": "1"}	\N
461	3	\N	\N	\N	2015-03-18 17:22:16.555476	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
472	3	\N	\N	\N	2015-03-18 17:22:53.243571	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:32:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
445	3	\N	\N	\N	2015-03-18 17:22:05.934213	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
446	3	\N	\N	\N	2015-03-18 17:22:07.367395	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
447	3	\N	\N	\N	2015-03-18 17:22:07.487512	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
448	3	\N	\N	\N	2015-03-18 17:22:10.908308	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
449	3	public	productos_cotizaciones	49	2015-03-18 17:22:10.963382	INSERT	{}	{"id": "49", "cotizacion_id": "99", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-18 17:22:10.963382", "cantidad": "1", "precio_total": "500"}	\N
450	3	\N	\N	\N	2015-03-18 17:22:11.050125	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
476	3	\N	\N	\N	2015-03-18 17:23:03.866463	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:32:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
462	3	\N	\N	\N	2015-03-18 17:22:16.558969	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
464	3	\N	\N	\N	2015-03-18 17:22:16.690746	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
475	3	\N	\N	\N	2015-03-18 17:22:53.412927	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:32:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
463	3	\N	\N	\N	2015-03-18 17:22:16.680881	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
465	3	\N	\N	\N	2015-03-18 17:22:16.761132	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
466	3	\N	\N	\N	2015-03-18 17:22:16.959299	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
474	3	\N	\N	\N	2015-03-18 17:22:53.412558	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:32:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
467	3	\N	\N	\N	2015-03-18 17:22:22.112806	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:31:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/forma_pago"}
468	3	public	cotizaciones_formas_pagos	46	2015-03-18 17:22:22.153933	INSERT	{}	{"id": "46", "cotizacion_id": "99", "forma_pago_id": "1", "tipo_plazo": "inicio", "dias": "1", "porcentaje": "100", "usuario_id": "3", "modificado_en": "2015-03-18 17:31:58", "intervalo": "{"dias":""}"}	\N
469	3	\N	\N	\N	2015-03-18 17:22:28.457156	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:32:04","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
470	3	public	cotizaciones	99	2015-03-18 17:22:28.513952	UPDATE	{"id": "99", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:05:38.001757", "fecha_expiracion": "2015-03-28 00:00:00", "descuento": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:05:38.001757", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "sdasdasdasda", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": "false"}	{"id": "99", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:05:38.001757", "fecha_expiracion": "2015-03-28 00:00:00", "descuento": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:05:38.001757", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "sdasdasdasda", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": "true"}	\N
471	3	\N	\N	\N	2015-03-18 17:22:28.611572	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:32:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
473	3	\N	\N	\N	2015-03-18 17:22:53.405608	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:32:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
477	3	\N	\N	\N	2015-03-18 17:24:46.969496	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
478	3	\N	\N	\N	2015-03-18 17:25:22.300225	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
479	3	public	productos_cotizaciones	49	2015-03-18 17:25:22.373509	DELETE	{"id": "49", "cotizacion_id": "99", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-18 17:22:10.963382", "cantidad": "1", "precio_total": "500"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
480	3	public	cotizaciones_recursos	20	2015-03-18 17:25:22.430766	DELETE	{"id": "20", "cotizacion_id": "99", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:22:16.47059", "orden": "1"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
481	3	public	cotizaciones_formas_pagos	46	2015-03-18 17:25:22.471971	DELETE	{"id": "46", "cotizacion_id": "99", "forma_pago_id": "1", "tipo_plazo": "inicio", "dias": "1", "porcentaje": "100", "usuario_id": "3", "modificado_en": "2015-03-18 17:31:58", "intervalo": "{"dias":""}"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
482	3	public	cotizaciones	99	2015-03-18 17:25:22.517811	DELETE	{"id": "99", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:05:38.001757", "fecha_expiracion": "2015-03-28 00:00:00", "descuento": "1", "usuario_id": "3", "modificado_en": "2015-03-18 17:05:38.001757", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "sdasdasdasda", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": "true"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
483	3	public	cotizaciones	100	2015-03-18 17:25:22.574374	INSERT	{}	{"id": "100", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:25:22.574374", "fecha_expiracion": "2015-03-28 17:25:22.574374", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-18 17:25:22.574374", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	\N
484	3	\N	\N	\N	2015-03-18 17:25:22.685194	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
485	3	\N	\N	\N	2015-03-18 17:25:22.708672	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
486	3	\N	\N	\N	2015-03-18 17:25:22.856104	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
491	3	\N	\N	\N	2015-03-18 17:50:46.805427	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
492	3	\N	\N	\N	2015-03-18 17:50:47.013175	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
494	3	\N	\N	\N	2015-03-18 17:50:50.983146	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
497	3	\N	\N	\N	2015-03-18 17:50:56.073055	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
499	3	\N	\N	\N	2015-03-18 17:50:56.219424	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
500	3	\N	\N	\N	2015-03-18 17:50:58.584568	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/store_client"}
502	3	\N	\N	\N	2015-03-18 17:50:58.692597	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
503	3	\N	\N	\N	2015-03-18 17:51:00.437354	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
487	3	\N	\N	\N	2015-03-18 17:25:22.866551	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
488	3	\N	\N	\N	2015-03-18 17:25:22.945856	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
506	3	\N	\N	\N	2015-03-18 17:51:15.961865	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
508	3	\N	\N	\N	2015-03-18 17:51:16.266886	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
509	3	\N	\N	\N	2015-03-18 17:51:17.525339	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
489	3	\N	\N	\N	2015-03-18 17:25:23.159962	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 17:34:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
490	3	\N	\N	\N	2015-03-18 17:50:46.734346	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
493	3	\N	\N	\N	2015-03-18 17:50:50.982562	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/store_client"}
495	3	\N	\N	\N	2015-03-18 17:50:51.180323	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
496	3	\N	\N	\N	2015-03-18 17:50:53.748816	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
498	3	\N	\N	\N	2015-03-18 17:50:56.073653	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
501	3	\N	\N	\N	2015-03-18 17:50:58.585302	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
504	3	\N	\N	\N	2015-03-18 17:51:10.701992	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/delete_contact"}
505	3	public	personas_contactos	2	2015-03-18 17:51:10.747962	DELETE	{"id": "2", "nombre": "jeisy", "apellido": "palacios", "cedula": "V-18164380", "direccion": "hjghgfhgjhghj", "telefono_1": "(0000) 000-0000", "telefono_2": "(0000) 000-0000", "correo_electronico_1": "jetox21@gmail.com", "correo_electronico_2": "jetox21@gmail.com", "cliente_id": "10", "usuario_id": "1", "modificado_en": "2015-01-14 00:00:00", "horario_id": "", "cargo_id": "1", "es_interno": "false"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/delete_contact"}
507	3	\N	\N	\N	2015-03-18 17:51:15.968025	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-18 18:00:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/store_client"}
510	0	\N	\N	\N	2015-03-19 09:22:23.023028	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 09:32:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
511	0	\N	\N	\N	2015-03-19 09:22:23.187104	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 09:32:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
512	0	\N	\N	\N	2015-03-19 09:22:23.486363	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 09:32:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
513	0	\N	\N	\N	2015-03-19 09:25:30.572355	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 09:35:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
514	3	\N	\N	\N	2015-03-19 09:25:31.078002	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 09:35:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
515	3	\N	\N	\N	2015-03-19 09:25:55.437689	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 09:35:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
516	3	\N	\N	\N	2015-03-19 09:25:55.43709	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 09:35:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
517	3	\N	\N	\N	2015-03-19 09:25:55.626289	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 09:35:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
518	3	\N	\N	\N	2015-03-19 11:11:14.295781	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:20:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
519	3	\N	\N	\N	2015-03-19 11:11:14.29715	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:20:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
520	3	\N	\N	\N	2015-03-19 11:11:14.520188	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:20:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
521	3	\N	\N	\N	2015-03-19 11:11:18.758756	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:20:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
522	3	\N	\N	\N	2015-03-19 11:11:18.826456	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:20:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
523	3	\N	\N	\N	2015-03-19 11:11:19.422264	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:20:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
524	3	\N	\N	\N	2015-03-19 11:11:23.605895	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
525	3	\N	\N	\N	2015-03-19 11:11:25.674609	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
526	3	\N	\N	\N	2015-03-19 11:11:29.023192	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
527	3	\N	\N	\N	2015-03-19 11:11:29.15554	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
528	3	\N	\N	\N	2015-03-19 11:11:29.256821	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
529	3	\N	\N	\N	2015-03-19 11:11:29.364135	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
530	3	\N	\N	\N	2015-03-19 11:11:29.474913	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
531	3	\N	\N	\N	2015-03-19 11:11:29.47615	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
532	3	\N	\N	\N	2015-03-19 11:11:34.505046	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
533	0	public	cotizaciones	100	2015-03-19 11:11:34.635267	DELETE	{"id": "100", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-18 17:25:22.574374", "fecha_expiracion": "2015-03-28 17:25:22.574374", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-18 17:25:22.574374", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	\N
534	3	public	cotizaciones	101	2015-03-19 11:11:34.705901	INSERT	{}	{"id": "101", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 11:11:34.705901", "fecha_expiracion": "2015-03-29 11:11:34.705901", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:11:34.705901", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	\N
535	3	\N	\N	\N	2015-03-19 11:11:34.804259	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
536	3	\N	\N	\N	2015-03-19 11:11:34.992615	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
537	3	\N	\N	\N	2015-03-19 11:11:35.152216	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
538	3	\N	\N	\N	2015-03-19 11:11:35.19043	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
539	3	\N	\N	\N	2015-03-19 11:11:35.292758	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
540	3	\N	\N	\N	2015-03-19 11:11:35.568961	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:21:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
541	3	\N	\N	\N	2015-03-19 11:13:00.029398	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
542	3	\N	\N	\N	2015-03-19 11:13:02.241247	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
543	3	\N	\N	\N	2015-03-19 11:13:02.372857	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
544	3	\N	\N	\N	2015-03-19 11:13:05.277589	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
545	3	public	productos_cotizaciones	50	2015-03-19 11:13:05.420358	INSERT	{}	{"id": "50", "cotizacion_id": "101", "producto_id": "1", "precio": "150000", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:13:05.420358", "cantidad": "1", "precio_total": "150000"}	\N
546	3	\N	\N	\N	2015-03-19 11:13:05.537126	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
547	3	\N	\N	\N	2015-03-19 11:13:05.698523	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
548	3	\N	\N	\N	2015-03-19 11:13:05.876669	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
549	3	\N	\N	\N	2015-03-19 11:13:06.008522	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
550	3	\N	\N	\N	2015-03-19 11:13:06.026563	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
551	3	\N	\N	\N	2015-03-19 11:13:06.188827	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:22:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
552	3	\N	\N	\N	2015-03-19 11:13:33.207452	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:23:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
553	3	\N	\N	\N	2015-03-19 11:13:33.239823	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:23:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_producto"}
554	3	\N	\N	\N	2015-03-19 11:13:33.24056	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:23:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
579	3	\N	\N	\N	2015-03-19 11:14:40.182029	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
556	3	\N	\N	\N	2015-03-19 11:13:33.298493	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:23:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
572	3	\N	\N	\N	2015-03-19 11:14:39.95397	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
581	3	\N	\N	\N	2015-03-19 11:15:02.996827	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
582	3	\N	\N	\N	2015-03-19 11:15:04.890519	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
555	3	public	productos_cotizaciones	50	2015-03-19 11:13:33.262311	DELETE	{"id": "50", "cotizacion_id": "101", "producto_id": "1", "precio": "150000", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:13:05.420358", "cantidad": "1", "precio_total": "150000"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:23:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_producto"}
558	3	\N	\N	\N	2015-03-19 11:13:33.428294	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:23:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
559	3	\N	\N	\N	2015-03-19 11:13:33.810361	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:23:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
573	3	\N	\N	\N	2015-03-19 11:14:39.95567	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_producto"}
576	0	public	productos_cotizaciones	51	2015-03-19 11:14:40.016454	DELETE	{"id": "51", "cotizacion_id": "101", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:14:29.1847", "cantidad": "1", "precio_total": "500"}	{}	\N
577	3	\N	\N	\N	2015-03-19 11:14:40.151194	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
600	0	\N	\N	\N	2015-03-19 11:42:18.543216	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 11:51:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
557	3	\N	\N	\N	2015-03-19 11:13:33.426953	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:23:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
560	3	\N	\N	\N	2015-03-19 11:13:33.821038	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:23:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
561	3	\N	\N	\N	2015-03-19 11:14:25.114328	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:04","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
562	3	\N	\N	\N	2015-03-19 11:14:26.870273	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
563	3	\N	\N	\N	2015-03-19 11:14:26.977999	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
564	3	\N	\N	\N	2015-03-19 11:14:29.154162	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
565	3	public	productos_cotizaciones	51	2015-03-19 11:14:29.1847	INSERT	{}	{"id": "51", "cotizacion_id": "101", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:14:29.1847", "cantidad": "1", "precio_total": "500"}	\N
566	3	\N	\N	\N	2015-03-19 11:14:29.282056	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
567	3	\N	\N	\N	2015-03-19 11:14:29.299023	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
568	3	\N	\N	\N	2015-03-19 11:14:29.408587	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
569	3	\N	\N	\N	2015-03-19 11:14:29.423702	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
570	3	\N	\N	\N	2015-03-19 11:14:29.488111	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
571	3	\N	\N	\N	2015-03-19 11:14:29.730418	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
574	3	\N	\N	\N	2015-03-19 11:14:39.964461	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
575	3	\N	\N	\N	2015-03-19 11:14:39.9883	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
578	3	\N	\N	\N	2015-03-19 11:14:40.15925	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
580	3	\N	\N	\N	2015-03-19 11:14:40.41459	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
583	3	\N	\N	\N	2015-03-19 11:15:14.103823	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
584	3	public	productos_cotizaciones	52	2015-03-19 11:15:14.140924	INSERT	{}	{"id": "52", "cotizacion_id": "101", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:15:14.140924", "cantidad": "1", "precio_total": "500"}	\N
585	3	\N	\N	\N	2015-03-19 11:15:14.254325	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
586	3	\N	\N	\N	2015-03-19 11:15:14.277758	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
587	3	\N	\N	\N	2015-03-19 11:15:14.422531	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
588	3	\N	\N	\N	2015-03-19 11:15:14.433651	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
589	3	\N	\N	\N	2015-03-19 11:15:14.699994	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
590	3	\N	\N	\N	2015-03-19 11:15:14.742382	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
591	3	\N	\N	\N	2015-03-19 11:15:16.372122	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
592	3	\N	\N	\N	2015-03-19 11:15:16.409104	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
593	3	\N	\N	\N	2015-03-19 11:15:16.420912	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
594	3	\N	\N	\N	2015-03-19 11:15:16.421562	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_producto"}
612	3	\N	\N	\N	2015-03-19 11:42:31.089877	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
595	3	public	productos_cotizaciones	52	2015-03-19 11:15:16.581186	DELETE	{"id": "52", "cotizacion_id": "101", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:15:14.140924", "cantidad": "1", "precio_total": "500"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_producto"}
598	3	\N	\N	\N	2015-03-19 11:15:16.811074	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
603	3	\N	\N	\N	2015-03-19 11:42:23.944722	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
605	3	\N	\N	\N	2015-03-19 11:42:26.137155	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
607	3	\N	\N	\N	2015-03-19 11:42:27.199079	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
608	3	\N	\N	\N	2015-03-19 11:42:28.797388	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
609	3	\N	\N	\N	2015-03-19 11:42:30.822447	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
616	3	\N	\N	\N	2015-03-19 11:42:32.837776	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
618	3	\N	\N	\N	2015-03-19 11:42:32.985144	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
596	3	\N	\N	\N	2015-03-19 11:15:16.736528	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
597	3	\N	\N	\N	2015-03-19 11:15:16.743886	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
614	3	\N	\N	\N	2015-03-19 11:42:31.106609	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
617	3	\N	\N	\N	2015-03-19 11:42:32.981992	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
619	3	\N	\N	\N	2015-03-19 11:42:33.068027	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
599	3	\N	\N	\N	2015-03-19 11:15:17.06251	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:24:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
601	0	\N	\N	\N	2015-03-19 11:42:18.676925	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 11:51:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
602	0	\N	\N	\N	2015-03-19 11:42:23.465562	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 11:52:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
604	3	\N	\N	\N	2015-03-19 11:42:26.136525	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
606	3	\N	\N	\N	2015-03-19 11:42:26.321389	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
610	3	\N	\N	\N	2015-03-19 11:42:30.839916	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
611	3	\N	\N	\N	2015-03-19 11:42:31.002018	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
615	3	\N	\N	\N	2015-03-19 11:42:32.817896	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
620	3	\N	\N	\N	2015-03-19 11:42:33.349086	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
613	3	\N	\N	\N	2015-03-19 11:42:31.090563	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
621	3	\N	\N	\N	2015-03-19 11:43:13.295434	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
622	3	\N	\N	\N	2015-03-19 11:43:13.364768	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
623	3	\N	\N	\N	2015-03-19 11:43:13.566096	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
624	3	\N	\N	\N	2015-03-19 11:43:13.567903	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
625	3	\N	\N	\N	2015-03-19 11:43:13.680566	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
626	3	\N	\N	\N	2015-03-19 11:43:13.976973	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:52:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
627	3	\N	\N	\N	2015-03-19 11:45:25.429076	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
628	3	\N	\N	\N	2015-03-19 11:45:28.139377	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
629	3	\N	\N	\N	2015-03-19 11:45:28.184193	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
630	3	\N	\N	\N	2015-03-19 11:45:29.106803	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
631	3	public	productos_cotizaciones	53	2015-03-19 11:45:29.12759	INSERT	{}	{"id": "53", "cotizacion_id": "101", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:45:29.12759", "cantidad": "1", "precio_total": "500"}	\N
632	3	\N	\N	\N	2015-03-19 11:45:29.20904	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
633	3	\N	\N	\N	2015-03-19 11:45:29.226677	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
634	3	\N	\N	\N	2015-03-19 11:45:29.357175	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
635	3	\N	\N	\N	2015-03-19 11:45:29.360038	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
636	3	\N	\N	\N	2015-03-19 11:45:29.420721	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
637	3	\N	\N	\N	2015-03-19 11:45:29.699446	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
638	3	\N	\N	\N	2015-03-19 11:45:33.65593	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
639	3	\N	\N	\N	2015-03-19 11:45:35.961244	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
640	3	\N	\N	\N	2015-03-19 11:45:35.962497	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
641	3	\N	\N	\N	2015-03-19 11:45:37.289102	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
642	3	public	cotizaciones_recursos	21	2015-03-19 11:45:37.423463	INSERT	{}	{"id": "21", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 11:45:37.423463", "orden": "1"}	\N
643	3	\N	\N	\N	2015-03-19 11:45:37.529563	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
644	3	\N	\N	\N	2015-03-19 11:45:37.535352	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
645	3	\N	\N	\N	2015-03-19 11:45:37.73593	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
646	3	\N	\N	\N	2015-03-19 11:45:37.767422	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
647	3	\N	\N	\N	2015-03-19 11:45:37.835614	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
648	3	\N	\N	\N	2015-03-19 11:45:38.063122	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:55:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
649	3	\N	\N	\N	2015-03-19 11:46:36.460967	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:56:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
650	3	\N	\N	\N	2015-03-19 11:46:36.473671	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:56:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
651	3	\N	\N	\N	2015-03-19 11:46:36.477374	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:56:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
652	3	\N	\N	\N	2015-03-19 11:46:36.524545	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:56:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
653	3	public	cotizaciones_recursos	21	2015-03-19 11:46:36.519273	DELETE	{"id": "21", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 11:45:37.423463", "orden": "1"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:56:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
655	3	\N	\N	\N	2015-03-19 11:46:36.646967	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:56:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
656	3	\N	\N	\N	2015-03-19 11:46:36.695657	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:56:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
657	3	\N	\N	\N	2015-03-19 11:46:36.929096	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:56:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
674	0	\N	\N	\N	2015-03-19 15:10:09.548119	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 15:19:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
675	0	\N	\N	\N	2015-03-19 15:10:10.371397	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 15:19:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
676	0	\N	\N	\N	2015-03-19 15:10:14.295399	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 15:19:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
677	3	\N	\N	\N	2015-03-19 15:10:16.130814	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:19:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
679	3	\N	\N	\N	2015-03-19 15:10:18.197184	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:19:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
681	3	\N	\N	\N	2015-03-19 15:10:19.207513	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:19:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
682	3	\N	\N	\N	2015-03-19 15:10:20.300437	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
683	3	\N	\N	\N	2015-03-19 15:10:21.957811	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
689	3	\N	\N	\N	2015-03-19 15:10:29.265609	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
691	3	\N	\N	\N	2015-03-19 15:10:30.818744	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
693	3	\N	\N	\N	2015-03-19 15:10:30.905974	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
694	3	\N	\N	\N	2015-03-19 15:10:31.141276	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
695	3	\N	\N	\N	2015-03-19 15:10:33.709302	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
696	3	\N	\N	\N	2015-03-19 15:10:34.507766	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
697	3	\N	\N	\N	2015-03-19 15:10:34.597774	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
698	3	\N	\N	\N	2015-03-19 15:10:34.728069	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
699	3	\N	\N	\N	2015-03-19 15:10:36.71168	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
700	3	\N	\N	\N	2015-03-19 15:10:36.712354	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
701	3	\N	\N	\N	2015-03-19 15:10:37.284718	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
702	3	\N	\N	\N	2015-03-19 15:10:41.237084	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
654	3	\N	\N	\N	2015-03-19 11:46:36.61812	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 11:56:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
703	3	\N	\N	\N	2015-03-19 15:10:47.224851	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
709	3	\N	\N	\N	2015-03-19 15:10:49.484588	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
658	3	\N	\N	\N	2015-03-19 13:41:50.348676	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
660	3	\N	\N	\N	2015-03-19 13:41:51.749901	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
661	3	\N	\N	\N	2015-03-19 13:41:53.488591	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
662	3	\N	\N	\N	2015-03-19 13:41:53.97111	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
665	3	\N	\N	\N	2015-03-19 13:41:54.231218	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
666	3	\N	\N	\N	2015-03-19 13:41:56.933982	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
667	3	public	cotizaciones_recursos	22	2015-03-19 13:41:57.023318	INSERT	{}	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 13:41:57.023318", "orden": "1"}	\N
668	3	\N	\N	\N	2015-03-19 13:41:57.140828	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
659	3	\N	\N	\N	2015-03-19 13:41:51.747952	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
663	3	\N	\N	\N	2015-03-19 13:41:54.028071	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
664	3	\N	\N	\N	2015-03-19 13:41:54.153515	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
669	3	\N	\N	\N	2015-03-19 13:41:57.165098	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
671	3	\N	\N	\N	2015-03-19 13:41:57.345381	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
670	3	\N	\N	\N	2015-03-19 13:41:57.343603	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
688	3	\N	\N	\N	2015-03-19 15:10:22.227455	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
672	3	\N	\N	\N	2015-03-19 13:41:57.409182	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
673	3	\N	\N	\N	2015-03-19 13:41:57.709986	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 13:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
678	3	\N	\N	\N	2015-03-19 15:10:18.195896	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:19:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
686	3	\N	\N	\N	2015-03-19 15:10:22.226404	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
687	3	\N	\N	\N	2015-03-19 15:10:22.227021	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
690	3	\N	\N	\N	2015-03-19 15:10:29.307198	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
692	3	\N	\N	\N	2015-03-19 15:10:30.828917	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
704	3	\N	\N	\N	2015-03-19 15:10:47.678529	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
705	3	\N	\N	\N	2015-03-19 15:10:48.114448	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
706	3	\N	\N	\N	2015-03-19 15:10:48.886865	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
707	3	\N	\N	\N	2015-03-19 15:10:49.017033	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
708	3	\N	\N	\N	2015-03-19 15:10:49.445146	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
680	3	\N	\N	\N	2015-03-19 15:10:18.371569	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:19:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
684	3	\N	\N	\N	2015-03-19 15:10:22.004803	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
685	3	\N	\N	\N	2015-03-19 15:10:22.092037	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
710	3	\N	\N	\N	2015-03-19 15:10:49.64511	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
711	3	\N	\N	\N	2015-03-19 15:10:55.489967	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:20:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
712	3	public	cotizaciones_recursos	23	2015-03-19 15:10:55.543257	INSERT	{}	{"id": "23", "cotizacion_id": "101", "recurso_id": "3", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:20:35", "orden": "2"}	\N
713	3	\N	\N	\N	2015-03-19 15:11:51.492496	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
714	3	public	cotizaciones_recursos	24	2015-03-19 15:11:51.545455	INSERT	{}	{"id": "24", "cotizacion_id": "101", "recurso_id": "3", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:21:31", "orden": "3"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
715	3	\N	\N	\N	2015-03-19 15:11:51.631254	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
716	3	\N	\N	\N	2015-03-19 15:11:51.647356	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
717	3	\N	\N	\N	2015-03-19 15:11:51.907778	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
718	3	\N	\N	\N	2015-03-19 15:11:51.909611	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
719	3	\N	\N	\N	2015-03-19 15:11:51.952818	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
720	3	\N	\N	\N	2015-03-19 15:11:52.109049	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
721	3	\N	\N	\N	2015-03-19 15:12:20.050616	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
722	3	\N	\N	\N	2015-03-19 15:12:20.053629	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
723	3	\N	\N	\N	2015-03-19 15:12:20.054314	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
724	3	\N	\N	\N	2015-03-19 15:12:21.48005	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:21:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
725	3	public	cotizaciones_recursos	24	2015-03-19 15:12:23.36847	DELETE	{"id": "24", "cotizacion_id": "101", "recurso_id": "3", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:21:31", "orden": "3"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
726	3	\N	\N	\N	2015-03-19 15:12:39.282707	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
727	3	\N	\N	\N	2015-03-19 15:12:39.285692	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
728	3	\N	\N	\N	2015-03-19 15:12:39.329683	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
729	3	\N	\N	\N	2015-03-19 15:12:39.330225	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
730	3	\N	\N	\N	2015-03-19 15:12:45.387037	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
731	3	\N	\N	\N	2015-03-19 15:12:50.847748	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
732	3	\N	\N	\N	2015-03-19 15:12:52.199609	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
733	3	\N	\N	\N	2015-03-19 15:12:52.573103	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
740	3	\N	\N	\N	2015-03-19 15:13:16.593272	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
743	3	\N	\N	\N	2015-03-19 15:13:16.763668	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
734	3	\N	\N	\N	2015-03-19 15:12:52.677059	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
746	3	\N	\N	\N	2015-03-19 15:18:33.479797	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:28:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
735	3	\N	\N	\N	2015-03-19 15:12:52.932395	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
767	3	\N	\N	\N	2015-03-19 15:24:26.857608	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
768	3	\N	\N	\N	2015-03-19 15:24:29.42937	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
769	3	public	cotizaciones_recursos	26	2015-03-19 15:24:29.484279	INSERT	{}	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:34:09", "orden": "3"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
770	3	\N	\N	\N	2015-03-19 15:24:29.60471	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
736	3	\N	\N	\N	2015-03-19 15:12:52.991377	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
737	3	\N	\N	\N	2015-03-19 15:12:55.096476	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
738	3	\N	\N	\N	2015-03-19 15:13:07.468293	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
739	3	public	cotizaciones_recursos	25	2015-03-19 15:13:09.701297	INSERT	{}	{"id": "25", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:22:49", "orden": "3"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
741	3	\N	\N	\N	2015-03-19 15:13:16.593929	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
745	3	\N	\N	\N	2015-03-19 15:13:17.047503	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
751	3	\N	\N	\N	2015-03-19 15:18:34.515796	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:28:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
742	3	\N	\N	\N	2015-03-19 15:13:16.761416	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
747	3	\N	\N	\N	2015-03-19 15:18:33.815233	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:28:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
750	3	\N	\N	\N	2015-03-19 15:18:34.210626	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:28:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
758	3	\N	\N	\N	2015-03-19 15:24:04.835681	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:33:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
761	3	\N	\N	\N	2015-03-19 15:24:05.009226	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:33:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
744	3	\N	\N	\N	2015-03-19 15:13:16.811915	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:22:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
752	3	\N	\N	\N	2015-03-19 15:18:47.830616	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:28:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
753	3	public	cotizaciones_recursos	23	2015-03-19 15:18:47.850762	UPDATE	{"id": "23", "cotizacion_id": "101", "recurso_id": "3", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:20:35", "orden": "2"}	{"id": "23", "cotizacion_id": "101", "recurso_id": "3", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:20:35", "orden": "3"}	\N
754	3	public	cotizaciones_recursos	25	2015-03-19 15:18:47.850762	UPDATE	{"id": "25", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:22:49", "orden": "3"}	{"id": "25", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:22:49", "orden": "2"}	\N
755	3	\N	\N	\N	2015-03-19 15:18:48.281403	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:28:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
757	3	\N	\N	\N	2015-03-19 15:24:04.814208	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:33:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
759	3	public	cotizaciones_recursos	23	2015-03-19 15:24:04.837258	DELETE	{"id": "23", "cotizacion_id": "101", "recurso_id": "3", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:20:35", "orden": "3"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:33:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
760	3	\N	\N	\N	2015-03-19 15:24:04.988088	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:33:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
762	3	\N	\N	\N	2015-03-19 15:24:05.082128	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:33:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
763	3	\N	\N	\N	2015-03-19 15:24:05.396464	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:33:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
748	3	\N	\N	\N	2015-03-19 15:18:34.183715	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:28:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
765	3	\N	\N	\N	2015-03-19 15:24:23.460002	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
766	3	\N	\N	\N	2015-03-19 15:24:26.856993	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
771	3	\N	\N	\N	2015-03-19 15:24:29.646188	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
773	3	\N	\N	\N	2015-03-19 15:24:29.83217	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
749	3	\N	\N	\N	2015-03-19 15:18:34.196757	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:28:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
772	3	\N	\N	\N	2015-03-19 15:24:29.812657	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
756	3	\N	\N	\N	2015-03-19 15:24:04.778723	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:33:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
764	3	\N	\N	\N	2015-03-19 15:24:05.537891	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:33:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
774	3	\N	\N	\N	2015-03-19 15:24:29.875008	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
775	3	\N	\N	\N	2015-03-19 15:24:30.181891	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
776	3	\N	\N	\N	2015-03-19 15:24:40.362195	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
777	3	public	cotizaciones_recursos	25	2015-03-19 15:24:40.413483	UPDATE	{"id": "25", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:22:49", "orden": "2"}	{"id": "25", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:22:49", "orden": "3"}	\N
779	3	\N	\N	\N	2015-03-19 15:24:40.615357	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:34:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
780	3	\N	\N	\N	2015-03-19 15:29:23.722591	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
781	3	\N	\N	\N	2015-03-19 15:29:23.727538	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
782	3	\N	\N	\N	2015-03-19 15:29:24.153829	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:04","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
783	3	\N	\N	\N	2015-03-19 15:29:24.193948	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:04","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
784	3	\N	\N	\N	2015-03-19 15:29:24.194865	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:04","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
785	3	\N	\N	\N	2015-03-19 15:29:24.520892	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:04","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
786	3	\N	\N	\N	2015-03-19 15:30:02.42891	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
787	3	\N	\N	\N	2015-03-19 15:30:02.455103	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
788	3	\N	\N	\N	2015-03-19 15:30:02.483284	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
789	3	\N	\N	\N	2015-03-19 15:30:02.62415	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
790	3	\N	\N	\N	2015-03-19 15:30:02.716635	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
791	3	\N	\N	\N	2015-03-19 15:30:02.73391	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
793	3	\N	\N	\N	2015-03-19 15:30:02.749302	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
803	3	\N	\N	\N	2015-03-19 15:36:16.225464	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:45:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
817	3	\N	\N	\N	2015-03-19 16:26:08.354483	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:35:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
819	3	\N	\N	\N	2015-03-19 16:26:09.069837	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:35:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
792	3	public	cotizaciones_recursos	25	2015-03-19 15:30:02.709295	DELETE	{"id": "25", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:22:49", "orden": "3"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/eliminar_recurso"}
806	3	\N	\N	\N	2015-03-19 15:36:49.323954	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:46:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
808	3	\N	\N	\N	2015-03-19 15:36:49.492681	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:46:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
859	3	\N	\N	\N	2015-03-19 17:22:42.079587	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:32:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
860	3	public	cotizaciones	104	2015-03-19 17:22:42.318094	DELETE	{"id": "104", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:30:50", "fecha_expiracion": "2015-03-29 17:30:50", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:30:50", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:32:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
861	3	public	cotizaciones	105	2015-03-19 17:22:42.545305	INSERT	{}	{"id": "105", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:32:22", "fecha_expiracion": "2015-03-29 17:32:22", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:32:22", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:32:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
794	3	\N	\N	\N	2015-03-19 15:30:03.013198	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:39:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
854	3	\N	\N	\N	2015-03-19 17:21:09.561158	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:30:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
855	3	public	cotizaciones	103	2015-03-19 17:21:09.667953	DELETE	{"id": "103", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:29:59", "fecha_expiracion": "2015-03-29 17:29:59", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:29:59", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:30:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
856	3	public	cotizaciones	104	2015-03-19 17:21:09.735057	INSERT	{}	{"id": "104", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:30:50", "fecha_expiracion": "2015-03-29 17:30:50", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:30:50", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:30:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
857	3	\N	\N	\N	2015-03-19 17:21:09.867044	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:30:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
795	3	\N	\N	\N	2015-03-19 15:32:17.014763	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:41:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
798	3	\N	\N	\N	2015-03-19 15:32:17.192921	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:41:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
799	3	\N	\N	\N	2015-03-19 15:33:18.697458	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:42:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
802	3	\N	\N	\N	2015-03-19 15:33:18.86634	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:42:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
809	3	\N	\N	\N	2015-03-19 15:37:57.318561	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:47:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
778	3	public	cotizaciones_recursos	26	2015-03-19 15:24:40.413483	UPDATE	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:34:09", "orden": "3"}	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:34:09", "orden": "2"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:47:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
796	3	public	cotizaciones_recursos	26	2015-03-19 15:32:17.058338	UPDATE	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:34:09", "orden": "2"}	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:34:09", "orden": "1"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:47:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
801	3	public	cotizaciones_recursos	26	2015-03-19 15:33:18.740129	UPDATE	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:34:09", "orden": "1"}	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:34:09", "orden": "2"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:47:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
810	3	public	cotizaciones_recursos	26	2015-03-19 15:37:57.377272	UPDATE	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:34:09", "orden": "2"}	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:47:37", "orden": "1"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:47:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
797	3	public	cotizaciones_recursos	22	2015-03-19 15:32:17.058338	UPDATE	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 13:41:57.023318", "orden": "1"}	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 13:41:57.023318", "orden": "2"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:47:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
800	3	public	cotizaciones_recursos	22	2015-03-19 15:33:18.740129	UPDATE	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 13:41:57.023318", "orden": "2"}	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 13:41:57.023318", "orden": "1"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:47:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
811	3	public	cotizaciones_recursos	22	2015-03-19 15:37:57.377272	UPDATE	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 13:41:57.023318", "orden": "1"}	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:47:37", "orden": "2"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:47:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
843	3	\N	\N	\N	2015-03-19 17:19:59.866157	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
805	3	\N	\N	\N	2015-03-19 15:36:16.39396	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:45:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
813	3	\N	\N	\N	2015-03-19 15:38:25.892838	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:48:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
814	3	public	cotizaciones_recursos	26	2015-03-19 15:38:25.93998	UPDATE	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:47:37", "orden": "1"}	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:48:06", "orden": "2"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:48:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
815	3	public	cotizaciones_recursos	22	2015-03-19 15:38:25.93998	UPDATE	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:47:37", "orden": "2"}	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:48:06", "orden": "1"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:48:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso_orden"}
816	3	\N	\N	\N	2015-03-19 15:38:26.070148	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:48:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
812	3	\N	\N	\N	2015-03-19 15:37:57.516761	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 15:47:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
818	3	\N	\N	\N	2015-03-19 16:26:08.882382	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:35:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
820	3	\N	\N	\N	2015-03-19 16:26:09.07081	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:35:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
821	3	\N	\N	\N	2015-03-19 16:26:09.186578	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:35:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
822	3	\N	\N	\N	2015-03-19 16:26:09.496188	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:35:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
823	3	\N	\N	\N	2015-03-19 16:46:14.833142	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:55:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
824	3	\N	\N	\N	2015-03-19 16:46:14.931702	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:55:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
825	3	\N	\N	\N	2015-03-19 16:46:15.21316	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:55:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
826	3	\N	\N	\N	2015-03-19 16:46:15.28384	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:55:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
828	3	\N	\N	\N	2015-03-19 16:46:15.632701	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:55:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
829	0	\N	\N	\N	2015-03-19 17:19:47.411769	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 17:29:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
835	3	\N	\N	\N	2015-03-19 17:19:51.892285	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
836	3	\N	\N	\N	2015-03-19 17:19:52.058969	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
837	3	\N	\N	\N	2015-03-19 17:19:52.912967	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
838	3	\N	\N	\N	2015-03-19 17:19:54.552093	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
839	3	\N	\N	\N	2015-03-19 17:19:59.6418	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
840	3	\N	\N	\N	2015-03-19 17:19:59.669198	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
841	3	\N	\N	\N	2015-03-19 17:19:59.784457	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
842	3	\N	\N	\N	2015-03-19 17:19:59.835454	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
845	3	\N	\N	\N	2015-03-19 17:20:03.637954	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
846	3	public	productos_cotizaciones	53	2015-03-19 17:20:03.704409	DELETE	{"id": "53", "cotizacion_id": "101", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:45:29.12759", "cantidad": "1", "precio_total": "500"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
848	0	public	cotizaciones_recursos	22	2015-03-19 17:20:03.755262	DELETE	{"id": "22", "cotizacion_id": "101", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:48:06", "orden": "1"}	{}	\N
847	3	public	cotizaciones_recursos	26	2015-03-19 17:20:03.755262	DELETE	{"id": "26", "cotizacion_id": "101", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-19 15:48:06", "orden": "2"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
849	3	public	cotizaciones	101	2015-03-19 17:20:03.819132	DELETE	{"id": "101", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 11:11:34.705901", "fecha_expiracion": "2015-03-29 11:11:34.705901", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 11:11:34.705901", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
827	3	\N	\N	\N	2015-03-19 16:46:15.325615	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 16:55:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
830	0	\N	\N	\N	2015-03-19 17:19:47.423127	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 17:29:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
831	0	\N	\N	\N	2015-03-19 17:19:47.535117	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 17:29:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
832	0	\N	\N	\N	2015-03-19 17:19:49.504381	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 17:29:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
833	3	\N	\N	\N	2015-03-19 17:19:49.876044	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
834	3	\N	\N	\N	2015-03-19 17:19:51.887419	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
844	3	\N	\N	\N	2015-03-19 17:19:59.869181	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
850	3	public	cotizaciones	102	2015-03-19 17:20:03.872231	INSERT	{}	{"id": "102", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:29:44", "fecha_expiracion": "2015-03-29 17:29:44", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:29:44", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
851	3	\N	\N	\N	2015-03-19 17:20:18.756777	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
852	3	public	cotizaciones	102	2015-03-19 17:20:18.861717	DELETE	{"id": "102", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:29:44", "fecha_expiracion": "2015-03-29 17:29:44", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:29:44", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
853	3	public	cotizaciones	103	2015-03-19 17:20:18.928605	INSERT	{}	{"id": "103", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:29:59", "fecha_expiracion": "2015-03-29 17:29:59", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:29:59", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:29:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
858	3	\N	\N	\N	2015-03-19 17:21:09.882138	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:30:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
862	3	\N	\N	\N	2015-03-19 17:23:51.39983	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:33:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
863	0	public	cotizaciones	105	2015-03-19 17:23:51.474293	DELETE	{"id": "105", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:32:22", "fecha_expiracion": "2015-03-29 17:32:22", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:32:22", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	\N
864	3	public	cotizaciones	106	2015-03-19 17:23:51.523582	INSERT	{}	{"id": "106", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:33:31", "fecha_expiracion": "2015-03-29 17:33:31", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:33:31", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:33:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
865	3	\N	\N	\N	2015-03-19 17:24:06.989466	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:33:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
866	0	public	cotizaciones	106	2015-03-19 17:24:07.050818	DELETE	{"id": "106", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:33:31", "fecha_expiracion": "2015-03-29 17:33:31", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:33:31", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	\N
867	3	public	cotizaciones	107	2015-03-19 17:24:07.088474	INSERT	{}	{"id": "107", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:33:47", "fecha_expiracion": "2015-03-29 17:33:47", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:33:47", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:33:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
868	3	\N	\N	\N	2015-03-19 17:42:56.512948	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
869	3	\N	\N	\N	2015-03-19 17:42:56.517863	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
870	3	\N	\N	\N	2015-03-19 17:42:57.3369	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
871	3	\N	\N	\N	2015-03-19 17:42:57.400432	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
872	3	\N	\N	\N	2015-03-19 17:42:57.410827	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
873	3	\N	\N	\N	2015-03-19 17:42:57.41804	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
874	3	\N	\N	\N	2015-03-19 17:42:59.897553	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
875	3	\N	\N	\N	2015-03-19 17:43:00.085124	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
876	3	\N	\N	\N	2015-03-19 17:43:00.097307	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
877	3	\N	\N	\N	2015-03-19 17:43:00.217384	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
878	3	\N	\N	\N	2015-03-19 17:43:11.38939	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
879	3	\N	\N	\N	2015-03-19 17:43:11.397846	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
898	3	\N	\N	\N	2015-03-19 17:46:44.55679	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:56:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/logout"}
880	3	\N	\N	\N	2015-03-19 17:43:11.589	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
881	3	\N	\N	\N	2015-03-19 17:43:12.066229	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
882	3	\N	\N	\N	2015-03-19 17:43:13.499724	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
883	3	\N	\N	\N	2015-03-19 17:43:16.035782	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
895	3	\N	\N	\N	2015-03-19 17:43:18.999583	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
884	3	\N	\N	\N	2015-03-19 17:43:16.050328	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
885	3	\N	\N	\N	2015-03-19 17:43:16.196052	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
889	3	\N	\N	\N	2015-03-19 17:43:18.62636	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
890	0	public	cotizaciones	107	2015-03-19 17:43:18.695403	DELETE	{"id": "107", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:33:47", "fecha_expiracion": "2015-03-29 17:33:47", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:33:47", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	\N
891	3	public	cotizaciones	108	2015-03-19 17:43:18.732485	INSERT	{}	{"id": "108", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:52:59", "fecha_expiracion": "2015-03-29 17:52:59", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:52:59", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
892	3	\N	\N	\N	2015-03-19 17:43:18.841701	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
900	0	\N	\N	\N	2015-03-19 17:46:45.717176	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 17:56:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
886	3	\N	\N	\N	2015-03-19 17:43:16.245713	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
887	3	\N	\N	\N	2015-03-19 17:43:16.251199	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
888	3	\N	\N	\N	2015-03-19 17:43:16.358472	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
893	3	\N	\N	\N	2015-03-19 17:43:18.857279	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
894	3	\N	\N	\N	2015-03-19 17:43:18.998875	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
896	3	\N	\N	\N	2015-03-19 17:43:19.117954	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
897	3	\N	\N	\N	2015-03-19 17:43:19.39065	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-19 17:52:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
899	0	\N	\N	\N	2015-03-19 17:46:45.498867	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-19 17:56:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
901	0	\N	\N	\N	2015-03-20 09:00:17.132859	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 09:10:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
902	0	\N	\N	\N	2015-03-20 09:00:21.882187	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 09:10:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
903	3	\N	\N	\N	2015-03-20 09:00:22.549871	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:10:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
904	3	\N	\N	\N	2015-03-20 09:03:06.227447	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:12:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
905	3	\N	\N	\N	2015-03-20 09:03:06.228786	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:12:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
906	3	\N	\N	\N	2015-03-20 09:03:06.762824	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:12:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
907	3	\N	\N	\N	2015-03-20 09:04:43.45726	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:14:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
908	3	\N	\N	\N	2015-03-20 09:04:44.71799	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:14:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
909	3	\N	\N	\N	2015-03-20 09:04:47.191058	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:14:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
910	3	\N	\N	\N	2015-03-20 09:04:47.365324	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:14:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
911	3	\N	\N	\N	2015-03-20 09:04:47.571137	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:14:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
912	3	\N	\N	\N	2015-03-20 09:04:47.644197	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:14:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
913	3	\N	\N	\N	2015-03-20 09:04:47.65101	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:14:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
914	3	\N	\N	\N	2015-03-20 09:04:47.788982	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:14:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
915	3	\N	\N	\N	2015-03-20 09:07:16.42359	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:17:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
916	0	public	cotizaciones	108	2015-03-20 09:07:16.531433	DELETE	{"id": "108", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-19 17:52:59", "fecha_expiracion": "2015-03-29 17:52:59", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-19 17:52:59", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	\N
919	3	\N	\N	\N	2015-03-20 09:07:16.692908	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:17:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
920	3	\N	\N	\N	2015-03-20 09:07:16.861566	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:17:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
921	3	\N	\N	\N	2015-03-20 09:07:16.968045	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:17:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
917	3	public	cotizaciones	109	2015-03-20 09:07:16.571722	INSERT	{}	{"id": "109", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:17:00", "fecha_expiracion": "2015-03-30 09:17:00", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:17:00", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:17:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
918	3	\N	\N	\N	2015-03-20 09:07:16.68464	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:17:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
922	3	\N	\N	\N	2015-03-20 09:07:16.982716	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:17:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
923	3	\N	\N	\N	2015-03-20 09:07:17.259549	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:17:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
929	3	\N	\N	\N	2015-03-20 09:21:35.976918	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:31:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
933	3	\N	\N	\N	2015-03-20 09:22:24.535612	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
935	3	\N	\N	\N	2015-03-20 09:22:24.792239	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
954	3	\N	\N	\N	2015-03-20 09:22:44.379263	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
924	3	\N	\N	\N	2015-03-20 09:21:22.512644	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:31:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
942	3	\N	\N	\N	2015-03-20 09:22:30.851501	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
952	3	\N	\N	\N	2015-03-20 09:22:40.285851	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
925	3	\N	\N	\N	2015-03-20 09:21:22.513522	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:31:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
932	3	\N	\N	\N	2015-03-20 09:22:24.3694	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
939	3	\N	\N	\N	2015-03-20 09:22:30.596182	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
940	3	\N	\N	\N	2015-03-20 09:22:30.723366	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
944	3	\N	\N	\N	2015-03-20 09:22:33.636874	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
945	0	public	cotizaciones	109	2015-03-20 09:22:33.726364	DELETE	{"id": "109", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:17:00", "fecha_expiracion": "2015-03-30 09:17:00", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:17:00", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	\N
946	3	public	cotizaciones	110	2015-03-20 09:22:33.842331	INSERT	{}	{"id": "110", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:32:17", "fecha_expiracion": "2015-03-30 09:32:17", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:32:17", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
947	3	\N	\N	\N	2015-03-20 09:22:34.001778	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
926	3	\N	\N	\N	2015-03-20 09:21:22.52053	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:31:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
927	3	\N	\N	\N	2015-03-20 09:21:22.630532	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:31:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
943	3	\N	\N	\N	2015-03-20 09:22:30.852201	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
949	3	\N	\N	\N	2015-03-20 09:22:36.285823	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
955	3	\N	\N	\N	2015-03-20 09:22:48.718414	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
957	3	\N	\N	\N	2015-03-20 09:22:52.960683	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
959	3	\N	\N	\N	2015-03-20 09:22:56.169401	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
928	3	\N	\N	\N	2015-03-20 09:21:35.756819	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:31:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
934	3	\N	\N	\N	2015-03-20 09:22:24.53667	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
936	3	\N	\N	\N	2015-03-20 09:22:26.648055	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
937	3	\N	\N	\N	2015-03-20 09:22:27.933641	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
938	3	\N	\N	\N	2015-03-20 09:22:30.583037	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
948	3	\N	\N	\N	2015-03-20 09:22:34.006476	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
951	3	\N	\N	\N	2015-03-20 09:22:36.716642	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
930	3	\N	\N	\N	2015-03-20 09:21:35.977636	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:31:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
931	3	\N	\N	\N	2015-03-20 09:21:35.983044	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:31:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
941	3	\N	\N	\N	2015-03-20 09:22:30.818276	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
950	3	\N	\N	\N	2015-03-20 09:22:36.442351	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
953	3	\N	\N	\N	2015-03-20 09:22:40.90831	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
956	3	\N	\N	\N	2015-03-20 09:22:49.849005	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
958	3	\N	\N	\N	2015-03-20 09:22:53.210339	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:32:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
960	3	\N	\N	\N	2015-03-20 09:23:17.188244	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:33:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
961	3	\N	\N	\N	2015-03-20 09:23:22.454759	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:33:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
962	3	\N	\N	\N	2015-03-20 09:23:33.587856	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:33:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
963	3	\N	\N	\N	2015-03-20 09:23:36.753801	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:33:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
964	3	\N	\N	\N	2015-03-20 09:23:41.559019	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:33:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
965	3	\N	\N	\N	2015-03-20 09:24:04.205519	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:33:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
966	3	\N	\N	\N	2015-03-20 09:24:06.494997	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:33:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
967	3	\N	\N	\N	2015-03-20 09:24:12.483105	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:33:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
968	3	\N	\N	\N	2015-03-20 09:27:11.077931	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:36:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
969	3	public	cotizaciones	110	2015-03-20 09:27:18.415506	DELETE	{"id": "110", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:32:17", "fecha_expiracion": "2015-03-30 09:32:17", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:32:17", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:36:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
970	3	public	cotizaciones	111	2015-03-20 09:27:21.111224	INSERT	{}	{"id": "111", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:37:04", "fecha_expiracion": "2015-03-30 09:37:04", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:37:04", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:36:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
971	3	\N	\N	\N	2015-03-20 09:36:16.359253	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:46:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
972	3	public	cotizaciones	111	2015-03-20 09:36:16.472184	DELETE	{"id": "111", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:37:04", "fecha_expiracion": "2015-03-30 09:37:04", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:37:04", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:46:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
973	3	public	cotizaciones	112	2015-03-20 09:36:16.563907	INSERT	{}	{"id": "112", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:46:00", "fecha_expiracion": "2015-03-30 09:46:00", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:46:00", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:46:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
989	3	\N	\N	\N	2015-03-20 09:39:01.94955	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
990	3	\N	\N	\N	2015-03-20 09:39:02.14641	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
991	3	\N	\N	\N	2015-03-20 09:39:03.924751	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
974	3	\N	\N	\N	2015-03-20 09:37:18.128458	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:47:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
975	3	public	cotizaciones	112	2015-03-20 09:37:18.231378	DELETE	{"id": "112", "codigo": "AST - 2015 - 00038", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:46:00", "fecha_expiracion": "2015-03-30 09:46:00", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:46:00", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:47:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
976	3	\N	\N	\N	2015-03-20 09:37:18.33351	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:47:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
977	3	\N	\N	\N	2015-03-20 09:37:18.356048	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:47:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
978	3	\N	\N	\N	2015-03-20 09:38:51.561144	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
979	3	\N	\N	\N	2015-03-20 09:38:51.586237	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
980	3	\N	\N	\N	2015-03-20 09:38:51.67254	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
981	3	public	cotizaciones	113	2015-03-20 09:38:51.814313	INSERT	{}	{"id": "113", "codigo": "AST - 2015 - 00042", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:48:35", "fecha_expiracion": "2015-03-30 09:48:35", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:48:35", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
982	3	\N	\N	\N	2015-03-20 09:38:51.927152	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
983	3	\N	\N	\N	2015-03-20 09:38:51.941966	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
984	3	\N	\N	\N	2015-03-20 09:38:52.128981	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
985	3	\N	\N	\N	2015-03-20 09:38:52.134868	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
986	3	\N	\N	\N	2015-03-20 09:38:52.237651	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
987	3	\N	\N	\N	2015-03-20 09:38:52.65412	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
988	3	\N	\N	\N	2015-03-20 09:39:01.945061	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
994	3	\N	\N	\N	2015-03-20 09:39:09.057855	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
995	3	\N	\N	\N	2015-03-20 09:39:09.164796	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
996	3	\N	\N	\N	2015-03-20 09:39:09.215445	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
999	3	\N	\N	\N	2015-03-20 09:39:14.049197	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1000	0	public	cotizaciones	113	2015-03-20 09:39:14.177116	DELETE	{"id": "113", "codigo": "AST - 2015 - 00042", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:48:35", "fecha_expiracion": "2015-03-30 09:48:35", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:48:35", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	\N
1016	3	\N	\N	\N	2015-03-20 09:41:12.466552	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1019	3	\N	\N	\N	2015-03-20 09:41:27.17219	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:51:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
1020	3	\N	\N	\N	2015-03-20 09:41:27.172634	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:51:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1034	3	\N	\N	\N	2015-03-20 09:44:17.27765	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1041	3	\N	\N	\N	2015-03-20 09:44:25.366502	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
992	3	\N	\N	\N	2015-03-20 09:39:06.071186	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
993	3	\N	\N	\N	2015-03-20 09:39:09.036437	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1002	3	\N	\N	\N	2015-03-20 09:39:14.270598	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1012	3	\N	\N	\N	2015-03-20 09:41:12.100143	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1015	3	\N	\N	\N	2015-03-20 09:41:12.42736	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1049	3	\N	\N	\N	2015-03-20 09:44:35.625181	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1050	0	public	cotizaciones	115	2015-03-20 09:44:35.85897	DELETE	{"id": "115", "codigo": "AST - 2015 - 00044", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:54:00", "fecha_expiracion": "2015-03-30 09:54:00", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:54:00", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	\N
1051	3	\N	\N	\N	2015-03-20 09:44:36.005034	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1065	3	\N	\N	\N	2015-03-20 09:48:53.9291	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1066	3	\N	\N	\N	2015-03-20 09:48:56.274584	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
1068	3	\N	\N	\N	2015-03-20 09:48:59.456895	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
1069	3	public	productos_cotizaciones	54	2015-03-20 09:48:59.528497	INSERT	{}	{"id": "54", "cotizacion_id": "116", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:58:43", "cantidad": "1", "precio_total": "500"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
1070	3	\N	\N	\N	2015-03-20 09:48:59.654353	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1081	3	\N	\N	\N	2015-03-20 09:49:06.091578	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1083	3	\N	\N	\N	2015-03-20 09:49:06.41286	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
997	3	\N	\N	\N	2015-03-20 09:39:09.220158	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1007	3	\N	\N	\N	2015-03-20 09:41:06.223988	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1008	3	\N	\N	\N	2015-03-20 09:41:11.120811	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1014	3	\N	\N	\N	2015-03-20 09:41:12.426738	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1033	3	\N	\N	\N	2015-03-20 09:44:16.863545	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1035	3	\N	\N	\N	2015-03-20 09:44:17.278425	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1072	3	\N	\N	\N	2015-03-20 09:48:59.807716	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
998	3	\N	\N	\N	2015-03-20 09:39:09.244507	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1029	3	\N	\N	\N	2015-03-20 09:44:16.335486	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
1030	3	\N	\N	\N	2015-03-20 09:44:16.468732	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1031	3	public	cotizaciones	115	2015-03-20 09:44:16.598961	INSERT	{}	{"id": "115", "codigo": "AST - 2015 - 00044", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:54:00", "fecha_expiracion": "2015-03-30 09:54:00", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:54:00", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1032	3	\N	\N	\N	2015-03-20 09:44:16.844913	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1043	3	\N	\N	\N	2015-03-20 09:44:25.441769	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1044	3	\N	\N	\N	2015-03-20 09:44:25.689111	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1045	3	\N	\N	\N	2015-03-20 09:44:29.436583	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1084	3	\N	\N	\N	2015-03-20 09:49:06.427749	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1001	3	\N	\N	\N	2015-03-20 09:39:14.262452	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:48:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1017	3	\N	\N	\N	2015-03-20 09:41:12.850538	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1003	3	\N	\N	\N	2015-03-20 09:40:55.981101	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1004	3	\N	\N	\N	2015-03-20 09:40:59.315879	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1005	3	\N	\N	\N	2015-03-20 09:41:02.412748	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1006	3	\N	\N	\N	2015-03-20 09:41:06.134751	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1009	3	\N	\N	\N	2015-03-20 09:41:11.124619	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
1010	3	\N	\N	\N	2015-03-20 09:41:11.573807	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1011	3	public	cotizaciones	114	2015-03-20 09:41:11.750128	INSERT	{}	{"id": "114", "codigo": "AST - 2015 - 00043", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:50:55", "fecha_expiracion": "2015-03-30 09:50:55", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:50:55", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1013	3	\N	\N	\N	2015-03-20 09:41:12.100767	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:50:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1053	3	\N	\N	\N	2015-03-20 09:46:40.049236	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1054	3	\N	\N	\N	2015-03-20 09:46:42.871822	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1067	3	\N	\N	\N	2015-03-20 09:48:56.277526	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
1071	3	\N	\N	\N	2015-03-20 09:48:59.66063	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1073	3	\N	\N	\N	2015-03-20 09:48:59.808404	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1018	3	\N	\N	\N	2015-03-20 09:41:27.158381	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:51:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1022	3	\N	\N	\N	2015-03-20 09:41:30.831162	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:51:14","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1023	0	public	cotizaciones	114	2015-03-20 09:41:31.093653	DELETE	{"id": "114", "codigo": "AST - 2015 - 00043", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:50:55", "fecha_expiracion": "2015-03-30 09:50:55", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:50:55", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	\N
1024	3	\N	\N	\N	2015-03-20 09:41:31.311386	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:51:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1038	3	\N	\N	\N	2015-03-20 09:44:23.232935	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1039	3	\N	\N	\N	2015-03-20 09:44:25.149669	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1046	3	\N	\N	\N	2015-03-20 09:44:29.453428	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1061	3	\N	\N	\N	2015-03-20 09:46:50.132272	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1021	3	\N	\N	\N	2015-03-20 09:41:27.189863	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:51:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1025	3	\N	\N	\N	2015-03-20 09:41:31.374191	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:51:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1026	3	\N	\N	\N	2015-03-20 09:44:10.180082	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:53:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1027	3	\N	\N	\N	2015-03-20 09:44:12.964607	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:53:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1028	3	\N	\N	\N	2015-03-20 09:44:16.33485	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1036	3	\N	\N	\N	2015-03-20 09:44:17.367286	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1037	3	\N	\N	\N	2015-03-20 09:44:17.545648	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1040	3	\N	\N	\N	2015-03-20 09:44:25.155798	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1042	3	\N	\N	\N	2015-03-20 09:44:25.406792	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1048	3	\N	\N	\N	2015-03-20 09:44:29.473332	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
1052	3	\N	\N	\N	2015-03-20 09:44:36.009494	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1063	3	\N	\N	\N	2015-03-20 09:46:50.179374	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1082	3	\N	\N	\N	2015-03-20 09:49:06.405933	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1085	3	\N	\N	\N	2015-03-20 09:49:06.573213	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1047	3	\N	\N	\N	2015-03-20 09:44:29.472588	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:54:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1056	3	\N	\N	\N	2015-03-20 09:46:49.524139	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
1057	3	\N	\N	\N	2015-03-20 09:46:49.684889	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1058	3	public	cotizaciones	116	2015-03-20 09:46:49.832119	INSERT	{}	{"id": "116", "codigo": "AST - 2015 - 00045", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:56:33", "fecha_expiracion": "2015-03-30 09:56:33", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:56:33", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1059	3	\N	\N	\N	2015-03-20 09:46:49.956047	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1064	3	\N	\N	\N	2015-03-20 09:46:50.522943	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1055	3	\N	\N	\N	2015-03-20 09:46:49.521439	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1060	3	\N	\N	\N	2015-03-20 09:46:49.97263	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1062	3	\N	\N	\N	2015-03-20 09:46:50.13292	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1074	3	\N	\N	\N	2015-03-20 09:48:59.952282	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1075	3	\N	\N	\N	2015-03-20 09:49:00.116744	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1076	3	\N	\N	\N	2015-03-20 09:49:02.159516	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1077	3	\N	\N	\N	2015-03-20 09:49:03.268957	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
1078	3	\N	\N	\N	2015-03-20 09:49:05.813167	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
1079	3	public	cotizaciones_recursos	27	2015-03-20 09:49:05.937209	INSERT	{}	{"id": "27", "cotizacion_id": "116", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-20 09:58:49", "orden": "1"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
1080	3	\N	\N	\N	2015-03-20 09:49:06.054034	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 09:58:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1086	3	\N	\N	\N	2015-03-20 10:11:27.043924	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:21:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1087	3	\N	\N	\N	2015-03-20 10:14:43.926791	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:24:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1088	3	\N	\N	\N	2015-03-20 10:14:44.182412	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:24:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1089	3	\N	\N	\N	2015-03-20 10:15:38.073444	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1090	3	\N	\N	\N	2015-03-20 10:15:38.229693	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1091	3	\N	\N	\N	2015-03-20 10:15:38.412261	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1092	3	\N	\N	\N	2015-03-20 10:15:38.41769	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1093	3	\N	\N	\N	2015-03-20 10:15:38.543782	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1094	3	\N	\N	\N	2015-03-20 10:15:38.884508	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1095	3	\N	\N	\N	2015-03-20 10:16:05.31859	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1096	3	\N	\N	\N	2015-03-20 10:16:07.441652	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
1097	3	\N	\N	\N	2015-03-20 10:16:07.575477	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
1098	3	\N	\N	\N	2015-03-20 10:16:07.778801	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
1099	3	\N	\N	\N	2015-03-20 10:16:11.797717	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
1100	3	public	cotizaciones_recursos	28	2015-03-20 10:16:11.858978	INSERT	{}	{"id": "28", "cotizacion_id": "116", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-20 10:25:55", "orden": "2"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
1101	3	\N	\N	\N	2015-03-20 10:16:11.986376	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1102	3	\N	\N	\N	2015-03-20 10:16:11.989355	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1103	3	\N	\N	\N	2015-03-20 10:16:12.15078	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1115	3	\N	\N	\N	2015-03-20 10:32:09.157134	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:41:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1122	3	\N	\N	\N	2015-03-20 10:46:14.838923	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:55:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1124	3	\N	\N	\N	2015-03-20 10:46:15.201492	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:55:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
1125	3	\N	\N	\N	2015-03-20 10:46:19.840564	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:56:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1104	3	\N	\N	\N	2015-03-20 10:16:12.189397	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1105	3	\N	\N	\N	2015-03-20 10:16:12.207492	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1106	3	\N	\N	\N	2015-03-20 10:16:12.45462	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:25:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1107	3	\N	\N	\N	2015-03-20 10:24:03.353248	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:33:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1108	3	\N	\N	\N	2015-03-20 10:24:03.409086	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:33:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1109	3	\N	\N	\N	2015-03-20 10:24:03.658047	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:33:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1110	3	\N	\N	\N	2015-03-20 10:24:03.670207	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:33:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1111	3	\N	\N	\N	2015-03-20 10:24:03.803621	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:33:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1112	3	\N	\N	\N	2015-03-20 10:24:04.132319	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:33:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1113	3	\N	\N	\N	2015-03-20 10:31:58.046543	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:41:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1130	3	\N	\N	\N	2015-03-20 10:51:18.933039	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:01:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/edit_client"}
1131	3	public	clientes	15	2015-03-20 10:51:18.979227	UPDATE	{"id": "15", "nombre": "dasda", "tipo": "E", "dni": "123123123", "razon_social": "asdadasd", "direccion_fiscal": "sdasdasdas", "direccion_correspondencia": "dasdasdasda", "es_direccion_fiscal": "false", "telefonos": "(1231) 231-2312", "fax": "(2334) 234-23422", "correo_electronico": "dasda@sdasda.com", "sector_id": "8", "sitio_web": "http://www.google.com", "es_prospecto": "true", "usuario_id": "3", "modificado_en": "2015-01-26 00:00:00"}	{"id": "15", "nombre": "dasda", "tipo": "E", "dni": "123123123", "razon_social": "asdadasd", "direccion_fiscal": "sdasdasdas", "direccion_correspondencia": "dasdasdasda", "es_direccion_fiscal": "false", "telefonos": "(1231) 231-2312", "fax": "(2334) 234-23422", "correo_electronico": "dasda@sdasda.com", "sector_id": "8", "sitio_web": "http://www.google.com", "es_prospecto": "true", "usuario_id": "3", "modificado_en": "2015-01-26 00:00:00"}	\N
1132	3	\N	\N	\N	2015-03-20 10:51:19.083278	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:01:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1133	3	\N	\N	\N	2015-03-20 10:51:35.278344	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:01:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/edit_contact"}
1114	3	\N	\N	\N	2015-03-20 10:31:58.823813	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:41:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1118	3	\N	\N	\N	2015-03-20 10:32:13.229225	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:41:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1119	3	\N	\N	\N	2015-03-20 10:32:17.333773	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:42:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1127	3	\N	\N	\N	2015-03-20 10:51:12.159569	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:00:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/store_client"}
1129	3	\N	\N	\N	2015-03-20 10:51:12.369764	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:00:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1143	3	\N	\N	\N	2015-03-20 10:55:06.713482	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:04:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1116	3	\N	\N	\N	2015-03-20 10:32:09.195772	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:41:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1117	3	\N	\N	\N	2015-03-20 10:32:09.196786	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:41:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1120	3	\N	\N	\N	2015-03-20 10:32:17.361898	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:42:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
1121	3	\N	\N	\N	2015-03-20 10:32:19.844217	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:42:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
1123	3	\N	\N	\N	2015-03-20 10:46:15.014769	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:55:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
1126	3	\N	\N	\N	2015-03-20 10:46:19.93743	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 10:56:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/store_client"}
1128	3	\N	\N	\N	2015-03-20 10:51:12.211885	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:00:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1135	3	\N	\N	\N	2015-03-20 10:54:16.90376	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:04:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1136	3	\N	\N	\N	2015-03-20 10:54:16.976692	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:04:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/store_client"}
1137	3	\N	\N	\N	2015-03-20 10:54:17.24082	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:04:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1138	3	\N	\N	\N	2015-03-20 10:54:18.645734	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:04:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1139	3	\N	\N	\N	2015-03-20 10:54:33.903167	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:04:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/edit_contact"}
1140	3	public	personas_contactos	29	2015-03-20 10:54:33.948003	INSERT	{}	{"id": "29", "nombre": "dasdad", "apellido": "asdasdasd", "cedula": "", "direccion": "asdasdasdasd", "telefono_1": "(1231) 231-31233", "telefono_2": "", "correo_electronico_1": "", "correo_electronico_2": "", "cliente_id": "15", "usuario_id": "3", "modificado_en": "2015-03-20 11:04:17", "horario_id": "1", "cargo_id": "1", "es_interno": "true"}	\N
1141	3	\N	\N	\N	2015-03-20 10:54:34.109996	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:04:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/store_client"}
1142	3	\N	\N	\N	2015-03-20 10:55:06.712869	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:04:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
1144	3	\N	\N	\N	2015-03-20 10:55:06.92746	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:04:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
1145	3	\N	\N	\N	2015-03-20 10:55:37.202963	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:05:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1146	3	\N	\N	\N	2015-03-20 10:55:37.259178	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:05:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
1147	3	\N	\N	\N	2015-03-20 10:55:37.496659	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:05:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
1148	0	\N	\N	\N	2015-03-20 11:01:24.4465	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 11:11:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1149	0	\N	\N	\N	2015-03-20 11:01:24.46306	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 11:11:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
1150	0	\N	\N	\N	2015-03-20 11:01:24.586873	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 11:11:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1151	0	\N	\N	\N	2015-03-20 11:01:29.102168	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 11:11:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
1152	3	\N	\N	\N	2015-03-20 11:01:29.482159	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1153	3	\N	\N	\N	2015-03-20 11:01:33.232959	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
1154	3	\N	\N	\N	2015-03-20 11:01:33.233971	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1155	3	\N	\N	\N	2015-03-20 11:01:33.422118	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
1156	3	\N	\N	\N	2015-03-20 11:01:36.050286	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/store_client"}
1157	3	\N	\N	\N	2015-03-20 11:01:36.051203	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1158	3	\N	\N	\N	2015-03-20 11:01:36.242842	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1159	3	\N	\N	\N	2015-03-20 11:01:41.164982	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1160	3	\N	\N	\N	2015-03-20 11:02:09.362511	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/sectors"}
1161	3	\N	\N	\N	2015-03-20 11:02:09.434546	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1162	3	\N	\N	\N	2015-03-20 11:02:09.584874	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/sector\\/sectors"}
1163	3	\N	\N	\N	2015-03-20 11:02:10.390317	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1164	3	\N	\N	\N	2015-03-20 11:02:10.418831	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/positions"}
1165	3	\N	\N	\N	2015-03-20 11:02:10.589058	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/position\\/positions"}
1166	3	\N	\N	\N	2015-03-20 11:02:14.13325	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/position\\/get"}
1167	3	\N	\N	\N	2015-03-20 11:02:14.175572	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:11:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1168	3	\N	\N	\N	2015-03-20 11:02:23.055869	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:12:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/position\\/edit"}
1169	0	public	cargos	1	2015-03-20 11:02:23.117331	UPDATE	{"id": "1", "descripcion": "Cargo3", "usuario_id": "1", "modificado_en": "2015-01-23 16:49:59.295944"}	{"id": "1", "descripcion": "Cargo de prueba", "usuario_id": "0", "modificado_en": "2015-01-23 16:49:59.295944"}	\N
1170	3	\N	\N	\N	2015-03-20 11:02:23.194666	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:12:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1171	3	\N	\N	\N	2015-03-20 11:02:23.199222	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:12:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/positions"}
1172	3	\N	\N	\N	2015-03-20 11:02:23.319064	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 11:12:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/position\\/positions"}
1173	3	\N	\N	\N	2015-03-20 12:12:41.565975	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1174	3	\N	\N	\N	2015-03-20 12:12:43.532976	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/positions"}
1175	3	\N	\N	\N	2015-03-20 12:12:43.768957	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/position\\/positions"}
1176	3	\N	\N	\N	2015-03-20 12:12:46.306077	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1177	3	\N	\N	\N	2015-03-20 12:12:46.314035	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
1178	3	\N	\N	\N	2015-03-20 12:12:46.602611	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
1179	3	\N	\N	\N	2015-03-20 12:12:50.131944	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1180	3	\N	\N	\N	2015-03-20 12:12:50.348368	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/listar_contactos"}
1181	3	\N	\N	\N	2015-03-20 12:12:52.449511	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
1182	3	\N	\N	\N	2015-03-20 12:12:52.464313	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1183	3	\N	\N	\N	2015-03-20 12:12:52.696495	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
1184	3	\N	\N	\N	2015-03-20 12:13:14.365118	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/positions"}
1186	3	\N	\N	\N	2015-03-20 12:13:14.536194	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/position\\/positions"}
1188	3	\N	\N	\N	2015-03-20 12:13:14.900606	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/sectors"}
1189	3	\N	\N	\N	2015-03-20 12:13:15.036372	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/sector\\/sectors"}
1185	3	\N	\N	\N	2015-03-20 12:13:14.38129	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1187	3	\N	\N	\N	2015-03-20 12:13:14.898214	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:22:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1190	3	\N	\N	\N	2015-03-20 12:13:18.54959	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1192	3	\N	\N	\N	2015-03-20 12:13:20.917673	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/sector\\/edit"}
1193	0	public	sectores	16	2015-03-20 12:13:20.941785	UPDATE	{"id": "16", "nombre": "Sector 1", "usuario_id": "0", "modificado_en": "2015-01-29 12:39:01.613838"}	{"id": "16", "nombre": "asdasdasda", "usuario_id": "0", "modificado_en": "2015-01-29 12:39:01.613838"}	\N
1195	3	\N	\N	\N	2015-03-20 12:13:21.015265	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1198	3	\N	\N	\N	2015-03-20 12:13:24.284527	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1200	3	\N	\N	\N	2015-03-20 12:13:27.602325	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1203	3	\N	\N	\N	2015-03-20 12:13:30.817045	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1206	3	\N	\N	\N	2015-03-20 12:13:33.20414	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/edit"}
1207	3	public	productos_historico_precio	17	2015-03-20 12:13:33.2531	INSERT	{}	{"id": "17", "producto_id": "1", "precio_anterior": "150000", "precio_nuevo": "150000", "usuario_id": "3", "modificado_en": "2015-03-20 12:13:33.2531"}	\N
1208	3	public	productos	1	2015-03-20 12:13:33.2531	UPDATE	{"id": "1", "codigo": "00001", "nombre": "AST", "descripcion": "Prueba4 a", "precio": "150000", "tipo_producto_id": "4", "requisitos": "Prueba", "activo": "true", "usuario_id": "3", "modificado_en": "2015-01-12 09:41:26.828969"}	{"id": "1", "codigo": "00001", "nombre": "AST", "descripcion": "Prueba4 a", "precio": "150000", "tipo_producto_id": "4", "requisitos": "Prueba", "activo": "true", "usuario_id": "3", "modificado_en": "2015-01-12 09:41:26.828969"}	\N
1222	3	\N	\N	\N	2015-03-20 12:17:07.450582	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1191	3	\N	\N	\N	2015-03-20 12:13:18.550298	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/sector\\/get"}
1194	3	\N	\N	\N	2015-03-20 12:13:21.014348	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/sectors"}
1196	3	\N	\N	\N	2015-03-20 12:13:21.153977	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/sector\\/sectors"}
1197	3	\N	\N	\N	2015-03-20 12:13:24.283828	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/schedules"}
1199	3	\N	\N	\N	2015-03-20 12:13:24.500456	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/schedule\\/schedules"}
1201	3	\N	\N	\N	2015-03-20 12:13:27.60303	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1202	3	\N	\N	\N	2015-03-20 12:13:27.83271	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1204	3	\N	\N	\N	2015-03-20 12:13:30.855261	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1205	3	\N	\N	\N	2015-03-20 12:13:30.952895	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1209	3	\N	\N	\N	2015-03-20 12:13:38.838674	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:23:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/edit"}
1210	3	public	productos_historico_precio	18	2015-03-20 12:13:38.88621	INSERT	{}	{"id": "18", "producto_id": "1", "precio_anterior": "150000", "precio_nuevo": "150000", "usuario_id": "3", "modificado_en": "2015-03-20 12:13:38.88621"}	\N
1211	3	public	productos	1	2015-03-20 12:13:38.88621	UPDATE	{"id": "1", "codigo": "00001", "nombre": "AST", "descripcion": "Prueba4 a", "precio": "150000", "tipo_producto_id": "4", "requisitos": "Prueba", "activo": "true", "usuario_id": "3", "modificado_en": "2015-01-12 09:41:26.828969"}	{"id": "1", "codigo": "00001", "nombre": "AST", "descripcion": "Prueba4 a", "precio": "150000", "tipo_producto_id": "4", "requisitos": "Prueba", "activo": "true", "usuario_id": "3", "modificado_en": "2015-01-12 09:41:26.828969"}	\N
1227	3	\N	\N	\N	2015-03-20 12:17:20.391515	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:27:04","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1228	3	public	recursos	6	2015-03-20 12:17:20.441305	UPDATE	{"id": "6", "descripcion": "asdasda", "codigo": "00012", "usuario_id": "3", "modificado_en": "2015-02-20 09:12:28", "informacion_adicional": ""}	{"id": "6", "descripcion": "LAPTOP", "codigo": "00012", "usuario_id": "3", "modificado_en": "2015-03-20 12:27:04", "informacion_adicional": "null"}	\N
1229	3	\N	\N	\N	2015-03-20 12:17:20.521121	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:27:04","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1212	3	\N	\N	\N	2015-03-20 12:16:54.004317	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1213	3	\N	\N	\N	2015-03-20 12:16:54.067773	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/productTypes"}
1214	3	\N	\N	\N	2015-03-20 12:16:54.296574	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1215	3	\N	\N	\N	2015-03-20 12:16:56.512897	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1216	3	\N	\N	\N	2015-03-20 12:16:56.514603	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/get"}
1217	3	\N	\N	\N	2015-03-20 12:17:00.167956	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/edit"}
1219	3	\N	\N	\N	2015-03-20 12:17:00.296648	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1220	3	\N	\N	\N	2015-03-20 12:17:00.297727	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/productTypes"}
1221	3	\N	\N	\N	2015-03-20 12:17:00.494609	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1225	3	\N	\N	\N	2015-03-20 12:17:14.127245	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/get"}
1218	3	public	tipos_productos	5	2015-03-20 12:17:00.213647	UPDATE	{"id": "5", "descripcion": "asdasda", "usuario_id": "3", "modificado_en": "2015-03-02 12:59:29.095402"}	{"id": "5", "descripcion": "65345763573567", "usuario_id": "3", "modificado_en": "2015-03-02 12:59:29.095402"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/edit"}
1223	3	\N	\N	\N	2015-03-20 12:17:07.463193	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1224	3	\N	\N	\N	2015-03-20 12:17:07.638042	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1226	3	\N	\N	\N	2015-03-20 12:17:14.135122	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:26:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1230	3	\N	\N	\N	2015-03-20 12:17:20.659596	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:27:04","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1231	3	\N	\N	\N	2015-03-20 12:17:20.858009	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:27:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1232	3	\N	\N	\N	2015-03-20 12:18:38.682339	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1233	3	\N	\N	\N	2015-03-20 12:18:44.390217	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/add"}
1234	3	public	recursos	9	2015-03-20 12:18:44.460803	INSERT	{}	{"id": "9", "descripcion": "agadfgdfgs", "codigo": "0987654", "usuario_id": "3", "modificado_en": "2015-03-20 12:28:28", "informacion_adicional": "[]"}	\N
1235	3	\N	\N	\N	2015-03-20 12:18:44.5577	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1236	3	\N	\N	\N	2015-03-20 12:18:44.572622	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1237	3	\N	\N	\N	2015-03-20 12:18:44.842882	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1238	3	\N	\N	\N	2015-03-20 12:18:54.660992	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/get"}
1239	3	\N	\N	\N	2015-03-20 12:18:54.676161	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1240	3	\N	\N	\N	2015-03-20 12:18:57.815909	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1241	3	public	recursos	9	2015-03-20 12:18:57.852276	UPDATE	{"id": "9", "descripcion": "agadfgdfgs", "codigo": "0987654", "usuario_id": "3", "modificado_en": "2015-03-20 12:28:28", "informacion_adicional": "[]"}	{"id": "9", "descripcion": "agadfgdfgsddddd", "codigo": "0987654", "usuario_id": "3", "modificado_en": "2015-03-20 12:28:42", "informacion_adicional": "[]"}	\N
1242	3	\N	\N	\N	2015-03-20 12:18:57.961932	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1243	3	\N	\N	\N	2015-03-20 12:18:57.962659	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1244	3	\N	\N	\N	2015-03-20 12:18:58.145662	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1245	3	\N	\N	\N	2015-03-20 12:19:04.629183	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1246	3	\N	\N	\N	2015-03-20 12:19:04.63018	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1247	3	\N	\N	\N	2015-03-20 12:19:04.946431	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1248	3	\N	\N	\N	2015-03-20 12:19:05.929032	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1249	3	\N	\N	\N	2015-03-20 12:19:05.929784	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:28:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1250	3	\N	\N	\N	2015-03-20 12:19:22.251791	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:29:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1251	3	\N	\N	\N	2015-03-20 12:19:56.300493	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:29:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1252	3	\N	\N	\N	2015-03-20 12:20:25.490629	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:30:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1253	3	\N	\N	\N	2015-03-20 12:21:08.832138	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_user"}
1254	3	\N	\N	\N	2015-03-20 12:21:08.833225	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1255	3	\N	\N	\N	2015-03-20 12:21:09.027238	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/user_controller\\/list_user"}
1256	3	\N	\N	\N	2015-03-20 12:21:13.583618	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:30:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1257	3	\N	\N	\N	2015-03-20 12:21:13.584389	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:30:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1258	3	\N	\N	\N	2015-03-20 12:21:13.750541	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:30:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1259	3	\N	\N	\N	2015-03-20 12:21:35.346248	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1261	3	\N	\N	\N	2015-03-20 12:21:38.490579	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1265	3	\N	\N	\N	2015-03-20 12:21:42.189958	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1268	3	\N	\N	\N	2015-03-20 12:21:47.416035	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1271	3	\N	\N	\N	2015-03-20 12:21:50.929088	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1273	3	\N	\N	\N	2015-03-20 12:21:56.967763	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1274	3	\N	\N	\N	2015-03-20 12:21:57.154001	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1276	3	\N	\N	\N	2015-03-20 12:22:01.486769	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_user"}
1277	3	\N	\N	\N	2015-03-20 12:22:01.638089	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/user_controller\\/list_user"}
1290	0	\N	\N	\N	2015-03-20 13:08:14.831981	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 13:17:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1291	0	\N	\N	\N	2015-03-20 13:08:15.848107	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 13:18:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1313	3	\N	\N	\N	2015-03-20 13:18:08.844827	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:27:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1317	3	\N	\N	\N	2015-03-20 13:19:06.063945	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:28:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1328	3	\N	\N	\N	2015-03-20 13:24:28.912045	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:34:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1346	3	\N	\N	\N	2015-03-20 13:29:56.080442	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:39:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1260	3	\N	\N	\N	2015-03-20 12:21:35.351747	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/group_operation"}
1262	3	\N	\N	\N	2015-03-20 12:21:38.4956	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1263	3	\N	\N	\N	2015-03-20 12:21:38.915123	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1264	3	\N	\N	\N	2015-03-20 12:21:42.189238	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_user"}
1266	3	\N	\N	\N	2015-03-20 12:21:42.367358	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/user_controller\\/list_user"}
1267	3	\N	\N	\N	2015-03-20 12:21:47.415342	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1269	3	\N	\N	\N	2015-03-20 12:21:47.595421	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1270	3	\N	\N	\N	2015-03-20 12:21:50.927223	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1272	3	\N	\N	\N	2015-03-20 12:21:56.962253	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1275	3	\N	\N	\N	2015-03-20 12:22:01.485974	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:31:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1295	3	\N	\N	\N	2015-03-20 13:08:51.44236	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1298	3	\N	\N	\N	2015-03-20 13:08:54.324183	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1308	3	\N	\N	\N	2015-03-20 13:14:50.203557	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:24:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1309	3	\N	\N	\N	2015-03-20 13:15:46.610861	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:25:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1278	3	\N	\N	\N	2015-03-20 12:22:55.358434	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:32:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/user_controller\\/store"}
1279	3	\N	\N	\N	2015-03-20 12:22:55.364429	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:32:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1292	0	\N	\N	\N	2015-03-20 13:08:25.341661	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 13:18:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
1293	3	\N	\N	\N	2015-03-20 13:08:28.525719	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
1294	3	\N	\N	\N	2015-03-20 13:08:31.892367	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1307	3	\N	\N	\N	2015-03-20 13:14:32.9573	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:24:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1280	3	\N	\N	\N	2015-03-20 12:23:35.941788	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:33:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/create_user"}
1282	3	\N	\N	\N	2015-03-20 12:23:36.391119	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:33:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1296	3	\N	\N	\N	2015-03-20 13:08:51.452222	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_client"}
1297	3	\N	\N	\N	2015-03-20 13:08:51.636541	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/list_client"}
1299	3	\N	\N	\N	2015-03-20 13:08:54.329842	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/clientes\\/cliente\\/store_client"}
1300	3	\N	\N	\N	2015-03-20 13:08:54.480058	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1302	3	\N	\N	\N	2015-03-20 13:09:00.437771	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1303	3	\N	\N	\N	2015-03-20 13:09:00.616384	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1305	3	\N	\N	\N	2015-03-20 13:09:08.362063	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1281	3	\N	\N	\N	2015-03-20 12:23:36.376944	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:33:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_user"}
1283	3	\N	\N	\N	2015-03-20 12:23:36.517094	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 12:33:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/user_controller\\/list_user"}
1343	3	\N	\N	\N	2015-03-20 13:27:40.486404	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1345	3	\N	\N	\N	2015-03-20 13:27:40.641771	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1365	3	\N	\N	\N	2015-03-20 13:35:37.347137	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:45:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1366	3	\N	\N	\N	2015-03-20 13:35:37.434933	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:45:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1284	3	\N	\N	\N	2015-03-20 12:56:34.304716	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:06:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/productTypes"}
1286	3	\N	\N	\N	2015-03-20 12:56:34.54793	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:06:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1287	3	\N	\N	\N	2015-03-20 12:56:35.035261	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:06:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1289	3	\N	\N	\N	2015-03-20 12:56:35.217365	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:06:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1311	3	\N	\N	\N	2015-03-20 13:15:58.717533	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:25:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1315	3	\N	\N	\N	2015-03-20 13:18:54.434064	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:28:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1319	3	\N	\N	\N	2015-03-20 13:19:38.125117	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:29:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1321	3	\N	\N	\N	2015-03-20 13:21:06.34507	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:30:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1285	3	\N	\N	\N	2015-03-20 12:56:34.309303	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:06:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1288	3	\N	\N	\N	2015-03-20 12:56:35.044914	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:06:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1306	3	\N	\N	\N	2015-03-20 13:14:32.94804	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:24:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1322	3	\N	\N	\N	2015-03-20 13:21:06.346742	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:30:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1347	3	\N	\N	\N	2015-03-20 13:29:56.094645	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:39:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1348	3	\N	\N	\N	2015-03-20 13:29:56.348923	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:39:40","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1301	3	\N	\N	\N	2015-03-20 13:09:00.429552	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1304	3	\N	\N	\N	2015-03-20 13:09:08.340026	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:18:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1310	3	\N	\N	\N	2015-03-20 13:15:46.671859	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:25:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1312	3	\N	\N	\N	2015-03-20 13:18:08.838076	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:27:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1314	3	\N	\N	\N	2015-03-20 13:18:18.501509	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:28:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1316	3	\N	\N	\N	2015-03-20 13:18:54.567287	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:28:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1318	3	\N	\N	\N	2015-03-20 13:19:38.124351	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:29:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1320	3	\N	\N	\N	2015-03-20 13:19:50.280717	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:29:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1323	3	\N	\N	\N	2015-03-20 13:21:31.362568	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:31:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1324	3	\N	\N	\N	2015-03-20 13:22:08.95997	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:31:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1325	3	\N	\N	\N	2015-03-20 13:22:09.027455	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:31:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1326	3	\N	\N	\N	2015-03-20 13:22:18.732316	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:32:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1327	3	\N	\N	\N	2015-03-20 13:24:28.891337	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:34:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1329	3	\N	\N	\N	2015-03-20 13:24:37.812776	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:34:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1330	3	\N	\N	\N	2015-03-20 13:27:04.824982	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:36:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1331	3	\N	\N	\N	2015-03-20 13:27:04.973687	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:36:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1332	3	\N	\N	\N	2015-03-20 13:27:16.715312	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/add"}
1333	3	public	productos	6	2015-03-20 13:27:16.763667	INSERT	{}	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "1.23123e+13", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	\N
1334	3	\N	\N	\N	2015-03-20 13:27:16.886901	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1335	3	\N	\N	\N	2015-03-20 13:27:17.011568	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1336	3	\N	\N	\N	2015-03-20 13:27:17.165967	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1337	3	\N	\N	\N	2015-03-20 13:27:34.230847	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1338	3	\N	\N	\N	2015-03-20 13:27:34.329416	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1339	3	\N	\N	\N	2015-03-20 13:27:34.400106	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1340	3	\N	\N	\N	2015-03-20 13:27:40.337844	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/edit"}
1341	3	public	productos_historico_precio	19	2015-03-20 13:27:40.375851	INSERT	{}	{"id": "19", "producto_id": "6", "precio_anterior": "12312312283136", "precio_nuevo": "1231231.25", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:40.375851"}	\N
1342	3	public	productos	6	2015-03-20 13:27:40.375851	UPDATE	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "1.23123e+13", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "1.23123e+06", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	\N
1344	3	\N	\N	\N	2015-03-20 13:27:40.487186	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:37:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1349	3	\N	\N	\N	2015-03-20 13:30:59.997891	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:40:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/edit"}
1350	3	public	productos_historico_precio	20	2015-03-20 13:31:00.061561	INSERT	{}	{"id": "20", "producto_id": "6", "precio_anterior": "1231231.25", "precio_nuevo": "123333312", "usuario_id": "3", "modificado_en": "2015-03-20 13:31:00.061561"}	\N
1351	3	public	productos	6	2015-03-20 13:31:00.061561	UPDATE	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "1.23123e+06", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "1.23333e+08", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	\N
1353	3	\N	\N	\N	2015-03-20 13:31:00.15168	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:40:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1358	3	\N	\N	\N	2015-03-20 13:31:29.165669	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:41:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/edit"}
1359	3	public	productos_historico_precio	21	2015-03-20 13:31:29.201211	INSERT	{}	{"id": "21", "producto_id": "6", "precio_anterior": "123333312", "precio_nuevo": "12223.330078125", "usuario_id": "3", "modificado_en": "2015-03-20 13:31:29.201211"}	\N
1360	3	public	productos	6	2015-03-20 13:31:29.201211	UPDATE	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "1.23333e+08", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "12223.3", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	\N
1362	3	\N	\N	\N	2015-03-20 13:31:29.278799	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:41:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1370	3	\N	\N	\N	2015-03-20 13:36:04.432055	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:45:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1372	3	\N	\N	\N	2015-03-20 13:36:08.74389	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:45:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1352	3	\N	\N	\N	2015-03-20 13:31:00.1496	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:40:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1354	3	\N	\N	\N	2015-03-20 13:31:00.292621	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:40:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1355	3	\N	\N	\N	2015-03-20 13:31:18.978441	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:41:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1357	3	\N	\N	\N	2015-03-20 13:31:19.067656	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:41:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1356	3	\N	\N	\N	2015-03-20 13:31:18.979311	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:41:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1364	3	\N	\N	\N	2015-03-20 13:35:37.346378	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:45:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1367	3	\N	\N	\N	2015-03-20 13:35:45.654075	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:45:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1369	3	\N	\N	\N	2015-03-20 13:35:45.834952	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:45:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1361	3	\N	\N	\N	2015-03-20 13:31:29.278031	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:41:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1363	3	\N	\N	\N	2015-03-20 13:31:29.44461	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:41:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1368	3	\N	\N	\N	2015-03-20 13:35:45.654826	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:45:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1371	3	\N	\N	\N	2015-03-20 13:36:05.137026	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:45:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1373	3	\N	\N	\N	2015-03-20 13:37:22.570407	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:47:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1374	3	\N	\N	\N	2015-03-20 13:37:22.81439	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:47:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1375	3	\N	\N	\N	2015-03-20 13:37:22.995015	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 13:47:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1376	3	\N	\N	\N	2015-03-20 13:55:34.636376	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:05:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1377	3	\N	\N	\N	2015-03-20 13:55:34.726074	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:05:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1378	3	\N	\N	\N	2015-03-20 13:55:34.819963	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:05:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1379	3	\N	\N	\N	2015-03-20 13:56:09.187487	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:05:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1380	3	\N	\N	\N	2015-03-20 13:56:09.269403	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:05:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1381	3	\N	\N	\N	2015-03-20 13:56:09.370187	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:05:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1382	3	\N	\N	\N	2015-03-20 13:57:02.563889	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:06:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1383	3	\N	\N	\N	2015-03-20 13:57:02.6623	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:06:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1384	3	\N	\N	\N	2015-03-20 13:57:02.781118	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:06:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1385	3	\N	\N	\N	2015-03-20 13:58:41.314737	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1386	3	\N	\N	\N	2015-03-20 13:58:41.317767	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1387	3	\N	\N	\N	2015-03-20 13:58:41.67519	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1388	3	\N	\N	\N	2015-03-20 13:58:44.705986	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/edit"}
1389	3	public	productos_historico_precio	22	2015-03-20 13:58:44.759724	INSERT	{}	{"id": "22", "producto_id": "6", "precio_anterior": "12223.330078125", "precio_nuevo": "12223.2998046875", "usuario_id": "3", "modificado_en": "2015-03-20 13:58:44.759724"}	\N
1390	3	public	productos	6	2015-03-20 13:58:44.759724	UPDATE	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "12223.3", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "12223.3", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	\N
1391	3	\N	\N	\N	2015-03-20 13:58:44.853765	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1392	3	\N	\N	\N	2015-03-20 13:58:44.856502	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1393	3	\N	\N	\N	2015-03-20 13:58:45.04641	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1394	3	\N	\N	\N	2015-03-20 13:58:52.328219	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1395	3	\N	\N	\N	2015-03-20 13:58:52.328935	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1396	3	\N	\N	\N	2015-03-20 13:58:52.424293	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1397	3	\N	\N	\N	2015-03-20 13:58:57.302309	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/edit"}
1398	3	public	productos_historico_precio	23	2015-03-20 13:58:57.353519	INSERT	{}	{"id": "23", "producto_id": "6", "precio_anterior": "12223.2998046875", "precio_nuevo": "12223.2998046875", "usuario_id": "3", "modificado_en": "2015-03-20 13:58:57.353519"}	\N
1399	3	public	productos	6	2015-03-20 13:58:57.353519	UPDATE	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "12223.3", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "12223.3", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	\N
1400	3	\N	\N	\N	2015-03-20 13:58:57.514964	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1401	3	\N	\N	\N	2015-03-20 13:58:57.600362	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1402	3	\N	\N	\N	2015-03-20 13:58:57.766416	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:08:41","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1433	3	\N	\N	\N	2015-03-20 14:02:31.329871	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:12:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1434	3	public	productos_cotizaciones	54	2015-03-20 14:02:37.999532	DELETE	{"id": "54", "cotizacion_id": "116", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:58:43", "cantidad": "1", "precio_total": "500"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:12:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1436	0	public	cotizaciones_recursos	28	2015-03-20 14:02:50.482716	DELETE	{"id": "28", "cotizacion_id": "116", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-20 10:25:55", "orden": "2"}	{}	\N
1435	3	public	cotizaciones_recursos	27	2015-03-20 14:02:50.482716	DELETE	{"id": "27", "cotizacion_id": "116", "recurso_id": "2", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-20 09:58:49", "orden": "1"}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:12:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1437	3	public	cotizaciones	116	2015-03-20 14:02:57.833958	DELETE	{"id": "116", "codigo": "AST - 2015 - 00045", "cliente_id": "10", "fecha_creacion": "2015-03-20 09:56:33", "fecha_expiracion": "2015-03-30 09:56:33", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 09:56:33", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:12:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1438	3	\N	\N	\N	2015-03-20 14:02:59.928217	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:12:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1441	3	\N	\N	\N	2015-03-20 14:09:27.15764	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1448	3	\N	\N	\N	2015-03-20 14:09:40.388411	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1453	3	\N	\N	\N	2015-03-20 14:18:19.071053	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:28:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1456	3	\N	\N	\N	2015-03-20 14:18:34.287958	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:28:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1460	3	\N	\N	\N	2015-03-20 14:25:46.934757	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
1462	3	\N	\N	\N	2015-03-20 14:25:47.37848	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1463	3	public	cotizaciones	117	2015-03-20 14:25:47.887717	INSERT	{}	{"id": "117", "codigo": "AST - 2015 - 00046", "cliente_id": "10", "fecha_creacion": "2015-03-20 14:35:32", "fecha_expiracion": "2015-03-30 14:35:32", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 14:35:32", "con_impuestos": "true", "porcentaje_impuesto": "12", "es_terminada": "false", "subtotal": "", "total_impuesto": "", "total_descuento": "", "total": "", "motivo_descuento": "", "fue_enviada": "false", "aprobada": "false", "costo_hora_adicional": "5000", "subtotal_con_descuento": "", "promocion": ""}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/crear"}
1464	3	\N	\N	\N	2015-03-20 14:25:48.170592	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1512	3	\N	\N	\N	2015-03-20 15:15:07.833936	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1403	3	\N	\N	\N	2015-03-20 13:59:33.207903	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/productTypes"}
1405	3	\N	\N	\N	2015-03-20 13:59:33.36918	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1406	3	\N	\N	\N	2015-03-20 13:59:36.736539	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/get"}
1410	3	\N	\N	\N	2015-03-20 13:59:40.045626	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/productTypes"}
1412	3	\N	\N	\N	2015-03-20 13:59:40.228696	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1413	3	\N	\N	\N	2015-03-20 13:59:43.641732	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1415	3	\N	\N	\N	2015-03-20 13:59:43.808068	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1425	3	\N	\N	\N	2015-03-20 14:01:24.63479	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:11:07","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1428	3	\N	\N	\N	2015-03-20 14:01:40.424089	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:11:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1446	3	\N	\N	\N	2015-03-20 14:09:34.475104	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
1458	3	\N	\N	\N	2015-03-20 14:25:35.042031	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1459	3	\N	\N	\N	2015-03-20 14:25:37.677135	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1478	3	\N	\N	\N	2015-03-20 14:26:11.462658	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1498	0	\N	\N	\N	2015-03-20 15:11:58.55238	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 15:21:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1499	0	\N	\N	\N	2015-03-20 15:11:58.696353	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 15:21:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1517	3	\N	\N	\N	2015-03-20 15:16:26.487127	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:26:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1518	3	\N	\N	\N	2015-03-20 15:16:26.784881	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:26:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1404	3	\N	\N	\N	2015-03-20 13:59:33.217568	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:17","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1407	3	\N	\N	\N	2015-03-20 13:59:36.737327	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1408	3	\N	\N	\N	2015-03-20 13:59:39.923806	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/edit"}
1411	3	\N	\N	\N	2015-03-20 13:59:40.046395	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1414	3	\N	\N	\N	2015-03-20 13:59:43.642426	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1416	3	\N	\N	\N	2015-03-20 13:59:52.218916	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1417	3	\N	\N	\N	2015-03-20 13:59:58.144458	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/add"}
1418	3	public	recursos	10	2015-03-20 14:00:05.131703	INSERT	{}	{"id": "10", "descripcion": "eqasdasdas", "codigo": "q212", "usuario_id": "3", "modificado_en": "2015-03-20 14:09:49", "informacion_adicional": "[]"}	\N
1419	3	\N	\N	\N	2015-03-20 14:00:09.509992	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1420	3	\N	\N	\N	2015-03-20 14:00:10.768814	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:09:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1421	3	\N	\N	\N	2015-03-20 14:00:21.061986	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:10:05","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1422	3	\N	\N	\N	2015-03-20 14:00:53.079993	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:10:36","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
1423	3	\N	\N	\N	2015-03-20 14:00:54.831592	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:10:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1424	3	\N	\N	\N	2015-03-20 14:00:59.159615	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:10:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
1426	3	\N	\N	\N	2015-03-20 14:01:26.99772	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:11:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1427	3	\N	\N	\N	2015-03-20 14:01:35.350553	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:11:19","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
1430	3	\N	\N	\N	2015-03-20 14:01:43.861779	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:11:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1431	3	\N	\N	\N	2015-03-20 14:01:44.940045	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:11:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1432	3	\N	\N	\N	2015-03-20 14:01:45.250132	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:11:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
1439	3	\N	\N	\N	2015-03-20 14:03:00.215024	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:12:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1440	3	\N	\N	\N	2015-03-20 14:09:27.08881	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1442	3	\N	\N	\N	2015-03-20 14:09:27.158502	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
1443	3	\N	\N	\N	2015-03-20 14:09:27.164	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1444	3	\N	\N	\N	2015-03-20 14:09:34.33852	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1447	3	\N	\N	\N	2015-03-20 14:09:34.476305	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1449	3	\N	\N	\N	2015-03-20 14:09:40.391687	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
1450	3	\N	\N	\N	2015-03-20 14:09:40.582309	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
1451	3	\N	\N	\N	2015-03-20 14:09:44.111715	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1452	3	\N	\N	\N	2015-03-20 14:09:53.14169	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1429	3	\N	\N	\N	2015-03-20 14:01:42.97914	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:11:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1461	3	\N	\N	\N	2015-03-20 14:25:46.935536	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1465	3	\N	\N	\N	2015-03-20 14:25:48.181186	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1466	3	\N	\N	\N	2015-03-20 14:25:48.636535	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1468	3	\N	\N	\N	2015-03-20 14:25:48.787307	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1483	3	\N	\N	\N	2015-03-20 14:26:49.674147	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1485	3	\N	\N	\N	2015-03-20 14:26:53.006728	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1513	3	\N	\N	\N	2015-03-20 15:16:26.215714	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:26:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1516	3	\N	\N	\N	2015-03-20 15:16:26.424972	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:26:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1522	3	\N	\N	\N	2015-03-20 15:18:37.27298	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1445	3	\N	\N	\N	2015-03-20 14:09:34.470733	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:19:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1454	3	\N	\N	\N	2015-03-20 14:18:25.360183	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:28:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1455	3	\N	\N	\N	2015-03-20 14:18:25.769593	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:28:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1470	3	\N	\N	\N	2015-03-20 14:26:01.550211	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
1471	3	\N	\N	\N	2015-03-20 14:26:01.742059	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
1482	3	\N	\N	\N	2015-03-20 14:26:49.670327	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
1502	3	\N	\N	\N	2015-03-20 15:14:53.528352	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1533	3	\N	\N	\N	2015-03-20 15:18:55.260253	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1457	3	\N	\N	\N	2015-03-20 14:21:10.096225	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:30:54","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1467	3	\N	\N	\N	2015-03-20 14:25:48.638393	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1469	3	\N	\N	\N	2015-03-20 14:25:49.226856	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1472	3	\N	\N	\N	2015-03-20 14:26:03.45651	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1473	3	\N	\N	\N	2015-03-20 14:26:04.327548	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1474	3	\N	\N	\N	2015-03-20 14:26:06.31495	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1475	3	\N	\N	\N	2015-03-20 14:26:11.0616	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1488	3	\N	\N	\N	2015-03-20 14:26:58.948727	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1494	3	\N	\N	\N	2015-03-20 14:27:01.909872	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1496	3	\N	\N	\N	2015-03-20 14:27:02.06873	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1503	3	\N	\N	\N	2015-03-20 15:14:53.578709	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
1504	3	\N	\N	\N	2015-03-20 15:14:53.871361	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:38","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
1505	3	\N	\N	\N	2015-03-20 15:14:58.768714	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1506	3	\N	\N	\N	2015-03-20 15:15:01.589695	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1507	3	\N	\N	\N	2015-03-20 15:15:06.443751	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1515	3	\N	\N	\N	2015-03-20 15:16:26.424248	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:26:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1530	3	\N	\N	\N	2015-03-20 15:18:55.058855	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
1531	3	\N	\N	\N	2015-03-20 15:18:55.165139	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1476	3	\N	\N	\N	2015-03-20 14:26:11.085153	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
1477	3	\N	\N	\N	2015-03-20 14:26:11.461034	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1500	0	\N	\N	\N	2015-03-20 15:12:13.49911	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 15:21:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/login"}
1501	3	\N	\N	\N	2015-03-20 15:12:14.014422	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:21:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1511	3	\N	\N	\N	2015-03-20 15:15:07.827682	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
1521	3	\N	\N	\N	2015-03-20 15:18:28.876278	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1526	3	\N	\N	\N	2015-03-20 15:18:47.211433	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar"}
1527	3	\N	\N	\N	2015-03-20 15:18:50.042801	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1528	3	\N	\N	\N	2015-03-20 15:18:53.131963	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/clientes_referencia"}
1529	3	\N	\N	\N	2015-03-20 15:18:55.040356	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1479	3	\N	\N	\N	2015-03-20 14:26:11.519237	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
1489	3	\N	\N	\N	2015-03-20 14:26:58.949451	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1490	3	\N	\N	\N	2015-03-20 14:26:59.36724	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/get"}
1491	3	\N	\N	\N	2015-03-20 14:27:01.774356	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/edit"}
1492	3	public	productos_historico_precio	24	2015-03-20 14:27:01.812932	INSERT	{}	{"id": "24", "producto_id": "6", "precio_anterior": "12223.2998046875", "precio_nuevo": "12223.2998046875", "usuario_id": "3", "modificado_en": "2015-03-20 14:27:01.812932"}	\N
1493	3	public	productos	6	2015-03-20 14:27:01.812932	UPDATE	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "12223.3", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	{"id": "6", "codigo": "dasdasdasd", "nombre": "asdasdasd", "descripcion": "asdasdas", "precio": "12223.3", "tipo_producto_id": "3", "requisitos": "dasdasdasdasd", "activo": "true", "usuario_id": "3", "modificado_en": "2015-03-20 13:27:16.763667"}	\N
1495	3	\N	\N	\N	2015-03-20 14:27:01.91062	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1497	0	\N	\N	\N	2015-03-20 15:11:58.546061	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 15:21:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/productTypes"}
1514	3	\N	\N	\N	2015-03-20 15:16:26.220645	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:26:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1480	3	\N	\N	\N	2015-03-20 14:26:11.520172	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:35:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1481	3	\N	\N	\N	2015-03-20 14:26:49.529057	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1484	3	\N	\N	\N	2015-03-20 14:26:49.807999	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1486	3	\N	\N	\N	2015-03-20 14:26:53.02582	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/products"}
1487	3	\N	\N	\N	2015-03-20 14:26:53.187334	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 14:36:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/product\\/list"}
1508	3	\N	\N	\N	2015-03-20 15:15:06.771484	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/cotizacion_pendiente"}
1509	3	\N	\N	\N	2015-03-20 15:15:07.815629	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1510	3	\N	\N	\N	2015-03-20 15:15:07.817434	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:24:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1519	3	\N	\N	\N	2015-03-20 15:18:28.775596	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1520	3	\N	\N	\N	2015-03-20 15:18:28.785089	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:13","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1523	3	\N	\N	\N	2015-03-20 15:18:37.95579	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:22","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1525	3	\N	\N	\N	2015-03-20 15:18:46.931672	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/cotizacion"}
1532	3	\N	\N	\N	2015-03-20 15:18:55.242007	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1524	3	\N	\N	\N	2015-03-20 15:18:46.931015	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1534	3	\N	\N	\N	2015-03-20 15:18:55.261126	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:28:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos"}
1535	3	\N	\N	\N	2015-03-20 15:20:44.397666	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1536	3	\N	\N	\N	2015-03-20 15:20:44.411932	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1537	3	\N	\N	\N	2015-03-20 15:20:44.625502	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1538	3	\N	\N	\N	2015-03-20 15:20:44.627073	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1539	3	\N	\N	\N	2015-03-20 15:20:44.758872	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1540	3	\N	\N	\N	2015-03-20 15:20:45.135477	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1541	3	\N	\N	\N	2015-03-20 15:20:47.646221	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1542	3	\N	\N	\N	2015-03-20 15:20:49.472564	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
1543	3	\N	\N	\N	2015-03-20 15:20:52.137837	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/productos_referencia"}
1544	3	\N	\N	\N	2015-03-20 15:20:57.641202	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
1545	3	public	productos_cotizaciones	55	2015-03-20 15:20:57.729695	INSERT	{}	{"id": "55", "cotizacion_id": "117", "producto_id": "5", "precio": "500", "descuento": "0", "usuario_id": "3", "modificado_en": "2015-03-20 15:30:42", "cantidad": "1", "precio_total": "500"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/producto"}
1546	3	\N	\N	\N	2015-03-20 15:20:57.830464	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1547	3	\N	\N	\N	2015-03-20 15:20:57.839165	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1548	3	\N	\N	\N	2015-03-20 15:20:57.999028	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1549	3	\N	\N	\N	2015-03-20 15:20:58.039129	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1550	3	\N	\N	\N	2015-03-20 15:20:58.239638	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1551	3	\N	\N	\N	2015-03-20 15:20:58.34666	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:42","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1552	3	\N	\N	\N	2015-03-20 15:21:00.56924	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:44","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1553	3	\N	\N	\N	2015-03-20 15:21:01.514235	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
1554	3	\N	\N	\N	2015-03-20 15:21:01.585279	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
1555	3	\N	\N	\N	2015-03-20 15:21:05.642155	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:50","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos_referencia"}
1556	3	\N	\N	\N	2015-03-20 15:21:08.988364	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
1557	3	public	cotizaciones_recursos	29	2015-03-20 15:21:09.24847	INSERT	{}	{"id": "29", "cotizacion_id": "117", "recurso_id": "1", "cantidad": "1", "usuario_id": "3", "modificado_en": "2015-03-20 15:30:53", "orden": "1"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recurso"}
1558	3	\N	\N	\N	2015-03-20 15:21:09.36859	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion"}
1559	3	\N	\N	\N	2015-03-20 15:21:09.371178	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1563	3	\N	\N	\N	2015-03-20 15:21:10.633221	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/listar_detalle"}
1572	3	\N	\N	\N	2015-03-20 16:05:21.772441	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1589	3	\N	\N	\N	2015-03-20 16:10:03.299865	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/create_group"}
1590	0	acl	groups	4	2015-03-20 16:10:03.38113	INSERT	{}	{"id": "4", "name": "Invitado", "description": "Usuarios sin permisos", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:10:03.38113"}	\N
1591	3	\N	\N	\N	2015-03-20 16:10:03.490915	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1560	3	\N	\N	\N	2015-03-20 15:21:09.501354	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/formas_pagos\\/forma_pago\\/all"}
1564	3	\N	\N	\N	2015-03-20 15:21:31.060721	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:31:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/forma_pago"}
1565	3	public	cotizaciones_formas_pagos	47	2015-03-20 15:21:31.097515	INSERT	{}	{"id": "47", "cotizacion_id": "117", "forma_pago_id": "1", "tipo_plazo": "entrega", "dias": "1", "porcentaje": "100", "usuario_id": "3", "modificado_en": "2015-03-20 15:31:15", "intervalo": "{"dias":""}"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:31:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/forma_pago"}
1594	3	\N	\N	\N	2015-03-20 16:10:14.151562	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1625	3	\N	\N	\N	2015-03-20 16:26:50.303431	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:36:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1644	3	\N	\N	\N	2015-03-20 16:28:35.874606	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1646	3	\N	\N	\N	2015-03-20 16:28:36.090667	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1652	3	\N	\N	\N	2015-03-20 16:29:24.021262	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:39:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1653	3	\N	\N	\N	2015-03-20 16:29:24.143561	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:39:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1561	3	\N	\N	\N	2015-03-20 15:21:09.502061	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/formas_pagos"}
1569	3	\N	\N	\N	2015-03-20 15:59:37.182556	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:09:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1573	3	\N	\N	\N	2015-03-20 16:05:21.773521	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1582	3	\N	\N	\N	2015-03-20 16:05:42.497305	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_user"}
1583	3	\N	\N	\N	2015-03-20 16:05:42.767746	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:27","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/user_controller\\/list_user"}
1611	3	\N	\N	\N	2015-03-20 16:23:37.022162	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:33:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1613	3	\N	\N	\N	2015-03-20 16:23:38.693275	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:33:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_group"}
1614	0	acl	groups	3	2015-03-20 16:23:38.718454	UPDATE	{"id": "3", "name": "Prueba", "description": "Rol de prueba de usuario", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:08:36.031739"}	{"id": "3", "name": "Prueba", "description": "Rol de prueba de usuario", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:08:36.031739"}	\N
1615	3	\N	\N	\N	2015-03-20 16:23:38.81374	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:33:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1627	3	\N	\N	\N	2015-03-20 16:27:01.250039	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:36:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/create_group"}
1628	3	\N	\N	\N	2015-03-20 16:27:01.339196	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:36:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1647	3	\N	\N	\N	2015-03-20 16:29:15.443327	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1663	3	\N	\N	\N	2015-03-20 16:32:54.495496	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:42:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1666	3	\N	\N	\N	2015-03-20 16:32:55.392371	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:42:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1680	3	\N	\N	\N	2015-03-20 16:39:10.456513	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:48:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1687	3	\N	\N	\N	2015-03-20 16:41:50.424724	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:51:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/get"}
1692	3	\N	\N	\N	2015-03-20 16:41:52.900738	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1693	3	\N	\N	\N	2015-03-20 16:41:53.03471	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1562	3	\N	\N	\N	2015-03-20 15:21:09.567117	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:30:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/cotizaciones\\/cotizacion\\/recursos"}
1566	3	\N	\N	\N	2015-03-20 15:21:44.335217	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:31:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1567	3	\N	\N	\N	2015-03-20 15:21:44.33627	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:31:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1568	3	\N	\N	\N	2015-03-20 15:21:44.932565	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 15:31:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1570	3	\N	\N	\N	2015-03-20 15:59:37.183407	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:09:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1571	3	\N	\N	\N	2015-03-20 15:59:37.462258	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:09:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1574	3	\N	\N	\N	2015-03-20 16:05:27.862842	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_user"}
1575	3	\N	\N	\N	2015-03-20 16:05:27.958231	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1576	3	\N	\N	\N	2015-03-20 16:05:28.109772	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/user_controller\\/list_user"}
1577	3	\N	\N	\N	2015-03-20 16:05:31.130059	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1578	3	\N	\N	\N	2015-03-20 16:05:31.130782	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/user_controller\\/store"}
1579	3	\N	\N	\N	2015-03-20 16:05:41.983335	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_user"}
1580	3	acl	users	8	2015-03-20 16:05:42.15421	UPDATE	{"id": "8", "ip_address": "127.0.0.1", "username": "uprueba", "password": "$2y$08$J6SnbpTrNL7MSPzh7vwkmOB.BImIEiCdpHSTFD4ePmWaVrf1vx1Pi", "salt": "", "email": "uprueba@ast.com.ve", "activation_code": "", "forgotten_password_code": "", "forgotten_password_time": "", "remember_code": "", "created_on": "1426089944", "last_login": "", "active": "1", "first_name": "Usuario", "last_name": "Prueba", "direccion": "alsdkjaalkslkdakl", "phone": "(1293) 812-30911", "cedula": "V-12345678", "usuario_id": "0", "modificado_en": "2015-03-20 15:53:49.238739"}	{"id": "8", "ip_address": "127.0.0.1", "username": "uprueba", "password": "$2y$08$J6SnbpTrNL7MSPzh7vwkmOB.BImIEiCdpHSTFD4ePmWaVrf1vx1Pi", "salt": "", "email": "uprueba@ast.com.ve", "activation_code": "", "forgotten_password_code": "", "forgotten_password_time": "", "remember_code": "", "created_on": "1426089944", "last_login": "", "active": "1", "first_name": "Usuario", "last_name": "Prueba", "direccion": "Dirección de prueba", "phone": "(1293) 812-30911", "cedula": "V-12345678", "usuario_id": "3", "modificado_en": "2015-03-20 16:15:26"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_user"}
1581	3	\N	\N	\N	2015-03-20 16:05:42.482562	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:15:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1584	3	\N	\N	\N	2015-03-20 16:09:40.980177	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1585	3	\N	\N	\N	2015-03-20 16:09:40.981122	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1586	3	\N	\N	\N	2015-03-20 16:09:41.208936	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1587	3	\N	\N	\N	2015-03-20 16:09:43.598201	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1588	3	\N	\N	\N	2015-03-20 16:09:43.602065	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1592	3	\N	\N	\N	2015-03-20 16:10:03.509605	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1593	3	\N	\N	\N	2015-03-20 16:10:03.645322	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1595	3	\N	\N	\N	2015-03-20 16:10:14.152678	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:19:58","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1596	3	\N	\N	\N	2015-03-20 16:10:19.104498	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:20:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_group"}
1598	3	\N	\N	\N	2015-03-20 16:10:19.301652	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:20:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1635	3	\N	\N	\N	2015-03-20 16:27:40.796126	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:37:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1636	3	\N	\N	\N	2015-03-20 16:27:41.081856	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:37:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1599	3	\N	\N	\N	2015-03-20 16:10:19.310705	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:20:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1600	3	\N	\N	\N	2015-03-20 16:10:19.450917	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:20:03","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1619	3	\N	\N	\N	2015-03-20 16:26:02.160294	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:35:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1623	3	\N	\N	\N	2015-03-20 16:26:03.768416	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:35:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1624	3	\N	\N	\N	2015-03-20 16:26:03.912888	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:35:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1660	3	\N	\N	\N	2015-03-20 16:30:41.996393	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:40:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1662	3	\N	\N	\N	2015-03-20 16:30:42.164866	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:40:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1669	3	\N	\N	\N	2015-03-20 16:33:03.144208	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:42:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1671	3	\N	\N	\N	2015-03-20 16:33:03.294972	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:42:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1702	3	\N	\N	\N	2015-03-20 16:46:49.212074	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/get"}
1706	3	\N	\N	\N	2015-03-20 16:46:52.866992	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:56:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1707	3	\N	\N	\N	2015-03-20 16:46:53.012651	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:56:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1601	3	\N	\N	\N	2015-03-20 16:14:07.270448	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:23:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1603	3	\N	\N	\N	2015-03-20 16:14:07.536109	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:23:52","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1629	3	\N	\N	\N	2015-03-20 16:27:01.367351	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:36:45","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1630	3	\N	\N	\N	2015-03-20 16:27:01.511377	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:36:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1633	3	\N	\N	\N	2015-03-20 16:27:40.712673	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:37:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/create_group"}
1634	3	\N	\N	\N	2015-03-20 16:27:40.794775	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:37:25","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1649	3	\N	\N	\N	2015-03-20 16:29:23.871536	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:39:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/create_group"}
1650	0	acl	groups	5	2015-03-20 16:29:23.921408	INSERT	{}	{"id": "5", "name": "Otro grupo", "description": "Otro grupo", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:29:23.921408"}	\N
1651	3	\N	\N	\N	2015-03-20 16:29:23.995622	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:39:08","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1664	3	\N	\N	\N	2015-03-20 16:32:54.515688	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:42:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1665	3	\N	\N	\N	2015-03-20 16:32:54.711996	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:42:39","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1602	3	\N	\N	\N	2015-03-20 16:14:07.271397	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:23:51","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1638	3	\N	\N	\N	2015-03-20 16:28:24.756921	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1639	3	\N	\N	\N	2015-03-20 16:28:26.199964	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1641	3	\N	\N	\N	2015-03-20 16:28:26.357674	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1642	3	\N	\N	\N	2015-03-20 16:28:27.81473	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1648	3	\N	\N	\N	2015-03-20 16:29:15.449122	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:39:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1604	3	\N	\N	\N	2015-03-20 16:17:58.532882	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:27:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1606	3	\N	\N	\N	2015-03-20 16:18:02.36273	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:27:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_group"}
1609	3	\N	\N	\N	2015-03-20 16:18:02.531801	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:27:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1607	0	acl	groups	4	2015-03-20 16:18:02.404103	UPDATE	{"id": "4", "name": "Invitado 2", "description": "Usuarios sin permisos", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:10:03.38113"}	{"id": "4", "name": "Invitado", "description": "Usuarios sin permisos", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:10:03.38113"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:35:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_group"}
1631	3	\N	\N	\N	2015-03-20 16:27:30.747475	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:37:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1645	3	\N	\N	\N	2015-03-20 16:28:35.876848	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1657	3	\N	\N	\N	2015-03-20 16:30:36.080021	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:40:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1678	3	\N	\N	\N	2015-03-20 16:37:45.440805	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:47:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1605	3	\N	\N	\N	2015-03-20 16:17:58.537024	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:27:43","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1608	3	\N	\N	\N	2015-03-20 16:18:02.53112	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:27:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1610	3	\N	\N	\N	2015-03-20 16:18:02.684224	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:27:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1612	3	\N	\N	\N	2015-03-20 16:23:37.02301	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:33:21","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1616	3	\N	\N	\N	2015-03-20 16:23:38.81797	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:33:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1617	3	\N	\N	\N	2015-03-20 16:23:38.96974	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:33:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1626	3	\N	\N	\N	2015-03-20 16:26:50.304115	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:36:34","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1655	3	\N	\N	\N	2015-03-20 16:30:26.277506	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:40:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1675	3	\N	\N	\N	2015-03-20 16:37:38.880779	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:47:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1676	3	\N	\N	\N	2015-03-20 16:37:39.167048	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:47:23","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1681	3	\N	\N	\N	2015-03-20 16:39:10.476236	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:48:55","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/get"}
1618	3	\N	\N	\N	2015-03-20 16:26:02.151139	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:35:46","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1620	3	\N	\N	\N	2015-03-20 16:26:03.641006	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:35:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_group"}
1621	3	acl	groups	4	2015-03-20 16:26:03.67124	UPDATE	{"id": "4", "name": "Invitado", "description": "Usuarios sin permisos", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:10:03.38113"}	{"id": "4", "name": "Invitado", "description": "Usuarios sin permisos", "_disabled": "false", "usuario_id": "3", "modificado_en": "2015-03-20 16:35:48"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:35:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_group"}
1597	0	acl	groups	4	2015-03-20 16:10:19.147925	UPDATE	{"id": "4", "name": "Invitado", "description": "Usuarios sin permisos", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:10:03.38113"}	{"id": "4", "name": "Invitado 2", "description": "Usuarios sin permisos", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:10:03.38113"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:35:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_group"}
1622	3	\N	\N	\N	2015-03-20 16:26:03.758601	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:35:48","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1632	3	\N	\N	\N	2015-03-20 16:27:30.911239	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:37:15","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1637	3	\N	\N	\N	2015-03-20 16:28:24.750769	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1640	3	\N	\N	\N	2015-03-20 16:28:26.212553	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1643	3	\N	\N	\N	2015-03-20 16:28:27.815438	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:38:12","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1654	3	\N	\N	\N	2015-03-20 16:30:26.253655	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:40:10","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1656	3	\N	\N	\N	2015-03-20 16:30:26.533326	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:40:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1658	3	\N	\N	\N	2015-03-20 16:30:36.080794	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:40:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1659	3	\N	\N	\N	2015-03-20 16:30:41.853315	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:40:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/create_group"}
1661	3	\N	\N	\N	2015-03-20 16:30:41.997154	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:40:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1667	3	\N	\N	\N	2015-03-20 16:33:03.013739	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:42:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/add"}
1668	3	public	recursos	11	2015-03-20 16:33:03.057521	INSERT	{}	{"id": "11", "descripcion": "asdasdasdasdasdas", "codigo": "asdasda", "usuario_id": "3", "modificado_en": "2015-03-20 16:42:47", "informacion_adicional": "[]"}	\N
1670	3	\N	\N	\N	2015-03-20 16:33:03.144954	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:42:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1677	3	\N	\N	\N	2015-03-20 16:37:45.408137	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:47:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1679	3	\N	\N	\N	2015-03-20 16:37:45.657321	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:47:30","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1682	3	\N	\N	\N	2015-03-20 16:39:16.566955	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:49:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1683	3	public	recursos	11	2015-03-20 16:39:16.605194	UPDATE	{"id": "11", "descripcion": "asdasdasdasdasdas", "codigo": "asdasda", "usuario_id": "3", "modificado_en": "2015-03-20 16:42:47", "informacion_adicional": "[]"}	{"id": "11", "descripcion": "nose", "codigo": "nose", "usuario_id": "3", "modificado_en": "2015-03-20 16:49:01", "informacion_adicional": "[]"}	\N
1685	3	\N	\N	\N	2015-03-20 16:39:16.69403	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:49:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1694	3	\N	\N	\N	2015-03-20 16:45:44.568186	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:55:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1695	3	\N	\N	\N	2015-03-20 16:45:44.589505	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:55:29","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/get"}
1696	3	\N	\N	\N	2015-03-20 16:45:47.295433	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:55:31","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1697	3	public	recursos	11	2015-03-20 16:45:47.335887	UPDATE	{"id": "11", "descripcion": "nose3", "codigo": "nose3", "usuario_id": "3", "modificado_en": "2015-03-20 16:51:37", "informacion_adicional": "[]"}	{"id": "11", "descripcion": "nose3", "codigo": "nose3", "usuario_id": "3", "modificado_en": "2015-03-20 16:55:31", "informacion_adicional": "[]"}	\N
1684	3	\N	\N	\N	2015-03-20 16:39:16.67987	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:49:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1686	3	\N	\N	\N	2015-03-20 16:39:16.822842	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:49:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1688	3	\N	\N	\N	2015-03-20 16:41:50.487749	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:51:35","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1689	3	\N	\N	\N	2015-03-20 16:41:52.784015	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1690	3	public	recursos	11	2015-03-20 16:41:52.811859	UPDATE	{"id": "11", "descripcion": "nose", "codigo": "nose", "usuario_id": "3", "modificado_en": "2015-03-20 16:49:01", "informacion_adicional": "[]"}	{"id": "11", "descripcion": "nose3", "codigo": "nose3", "usuario_id": "3", "modificado_en": "2015-03-20 16:51:37", "informacion_adicional": "[]"}	\N
1691	3	\N	\N	\N	2015-03-20 16:41:52.88615	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:51:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1701	3	\N	\N	\N	2015-03-20 16:46:49.20249	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:56:33","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1703	3	\N	\N	\N	2015-03-20 16:46:52.763476	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:56:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1705	3	\N	\N	\N	2015-03-20 16:46:52.855916	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:56:37","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1704	3	public	recursos	10	2015-03-20 16:46:52.788631	UPDATE	{"id": "10", "descripcion": "eqasdasdas", "codigo": "q212", "usuario_id": "3", "modificado_en": "2015-03-20 14:09:49", "informacion_adicional": "[]"}	{"id": "10", "descripcion": "eqasdasdas", "codigo": "q212", "usuario_id": "3", "modificado_en": "2015-03-20 16:56:37", "informacion_adicional": "[]"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:04:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1698	3	\N	\N	\N	2015-03-20 16:45:47.420245	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:55:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1700	3	\N	\N	\N	2015-03-20 16:45:47.599689	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:55:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1699	3	\N	\N	\N	2015-03-20 16:45:47.421187	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:55:32","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1708	3	\N	\N	\N	2015-03-20 16:48:02.737692	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:57:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1709	3	\N	\N	\N	2015-03-20 16:48:02.738777	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:57:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1710	3	\N	\N	\N	2015-03-20 16:48:03.008816	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 16:57:47","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1711	3	\N	\N	\N	2015-03-20 16:51:31.472388	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:16","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1712	3	\N	\N	\N	2015-03-20 16:51:33.606237	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1713	3	\N	\N	\N	2015-03-20 16:51:33.619123	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1714	3	\N	\N	\N	2015-03-20 16:51:33.753464	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:18","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1715	3	\N	\N	\N	2015-03-20 16:51:35.75293	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1716	3	\N	\N	\N	2015-03-20 16:51:35.756836	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:20","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/get"}
1717	3	\N	\N	\N	2015-03-20 16:51:39.412635	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1719	3	\N	\N	\N	2015-03-20 16:51:39.490585	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1720	3	\N	\N	\N	2015-03-20 16:51:39.828023	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1721	3	\N	\N	\N	2015-03-20 16:51:39.877023	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:01:24","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1722	3	\N	\N	\N	2015-03-20 16:54:15.487441	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:04:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1723	3	\N	\N	\N	2015-03-20 16:54:15.499602	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:04:00","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/get"}
1724	3	\N	\N	\N	2015-03-20 16:54:21.972513	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:04:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1725	3	public	recursos	10	2015-03-20 16:54:22.006051	UPDATE	{"id": "10", "descripcion": "eqasdasdas", "codigo": "asdadadasdasdas", "usuario_id": "3", "modificado_en": "2015-03-20 17:01:24", "informacion_adicional": "[]"}	{"id": "10", "descripcion": "asdasdasdasdasdasdasdasd", "codigo": "11111111111", "usuario_id": "3", "modificado_en": "2015-03-20 17:04:06", "informacion_adicional": "[]"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:04:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1718	3	public	recursos	10	2015-03-20 16:51:39.431307	UPDATE	{"id": "10", "descripcion": "eqasdasdas", "codigo": "q212", "usuario_id": "3", "modificado_en": "2015-03-20 16:56:37", "informacion_adicional": "[]"}	{"id": "10", "descripcion": "eqasdasdas", "codigo": "asdadadasdasdas", "usuario_id": "3", "modificado_en": "2015-03-20 17:01:24", "informacion_adicional": "[]"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:04:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/edit"}
1726	3	\N	\N	\N	2015-03-20 16:54:22.118308	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:04:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1727	3	\N	\N	\N	2015-03-20 16:54:22.127386	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:04:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/recurso"}
1728	3	\N	\N	\N	2015-03-20 16:54:22.273801	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:04:06","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/recursos\\/recurso\\/listar"}
1729	3	\N	\N	\N	2015-03-20 16:58:14.582961	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:07:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1730	3	\N	\N	\N	2015-03-20 16:58:14.584011	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:07:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_user"}
1731	3	\N	\N	\N	2015-03-20 16:58:14.780118	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:07:59","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/user_controller\\/list_user"}
1742	3	\N	\N	\N	2015-03-20 16:59:05.189105	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1746	3	\N	\N	\N	2015-03-20 16:59:08.60331	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/get"}
1749	3	\N	\N	\N	2015-03-20 16:59:12.511241	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/productTypes"}
1732	3	\N	\N	\N	2015-03-20 16:58:17.242182	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1734	3	\N	\N	\N	2015-03-20 16:58:17.400221	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:02","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1733	3	\N	\N	\N	2015-03-20 16:58:17.24301	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:01","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1735	3	\N	\N	\N	2015-03-20 16:58:24.974115	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1737	3	\N	\N	\N	2015-03-20 16:58:26.469474	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_group"}
1738	3	acl	groups	5	2015-03-20 16:58:26.493074	UPDATE	{"id": "5", "name": "Otro grupo", "description": "Otro grupo", "_disabled": "false", "usuario_id": "0", "modificado_en": "2015-03-20 16:29:23.921408"}	{"id": "5", "name": "Otro grupo", "description": "Otro grupo", "_disabled": "false", "usuario_id": "3", "modificado_en": "2015-03-20 17:08:11"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/edit_group"}
1740	3	\N	\N	\N	2015-03-20 16:58:26.586666	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1736	3	\N	\N	\N	2015-03-20 16:58:24.975094	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:09","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/store"}
1739	3	\N	\N	\N	2015-03-20 16:58:26.582296	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/list_group"}
1741	3	\N	\N	\N	2015-03-20 16:58:26.719411	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:11","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/administracion\\/group_controller\\/list_group"}
1743	3	\N	\N	\N	2015-03-20 16:59:05.189944	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/productTypes"}
1744	3	\N	\N	\N	2015-03-20 16:59:05.337735	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:49","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1745	3	\N	\N	\N	2015-03-20 16:59:08.59346	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:53","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1747	3	\N	\N	\N	2015-03-20 16:59:12.382011	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/edit"}
1748	3	public	tipos_productos	5	2015-03-20 16:59:12.40527	UPDATE	{"id": "5", "descripcion": "asdadsasdasdasda", "usuario_id": "3", "modificado_en": "2015-03-02 12:59:29.095402"}	{"id": "5", "descripcion": "prueba", "usuario_id": "3", "modificado_en": "2015-03-20 17:08:57"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/edit"}
1409	3	public	tipos_productos	5	2015-03-20 13:59:39.968199	UPDATE	{"id": "5", "descripcion": "65345763573567", "usuario_id": "3", "modificado_en": "2015-03-02 12:59:29.095402"}	{"id": "5", "descripcion": "asdadsasdasdasda", "usuario_id": "3", "modificado_en": "2015-03-02 12:59:29.095402"}	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:56","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/edit"}
1750	3	\N	\N	\N	2015-03-20 16:59:12.511963	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1751	3	\N	\N	\N	2015-03-20 16:59:12.625625	ACCESS	\N	\N	{"id_user":"3","ip":"127.0.0.1","access_time":"2015-03-20 17:08:57","os":"Linux","browser":"Chrome","_version":"41.0.2272.89","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/products\\/type\\/types"}
1752	0	\N	\N	\N	2015-03-20 17:11:42.28547	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 17:21:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.101","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
1753	0	\N	\N	\N	2015-03-20 17:11:42.28723	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 17:21:26","os":"Linux","browser":"Chrome","_version":"41.0.2272.101","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/has_permissions\\/productTypes"}
1754	0	\N	\N	\N	2015-03-20 17:11:44.504816	ACCESS	\N	\N	{"id_user":0,"ip":"127.0.0.1","access_time":"2015-03-20 17:21:28","os":"Linux","browser":"Chrome","_version":"41.0.2272.101","url":"http:\\/\\/localhost2\\/cotizaciones_ast\\/auth_cotizaciones\\/store"}
\.


--
-- TOC entry 2616 (class 0 OID 0)
-- Dependencies: 223
-- Name: auditorias_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('auditorias_id_seq', 1754, true);


--
-- TOC entry 2309 (class 0 OID 285405)
-- Dependencies: 211 2323
-- Data for Name: cargos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cargos (id, descripcion, usuario_id, modificado_en) FROM stdin;
1	Cargo de prueba	0	2015-01-23 16:49:59.295944
\.


--
-- TOC entry 2617 (class 0 OID 0)
-- Dependencies: 210
-- Name: cargos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cargos_id_seq', 30, true);


--
-- TOC entry 2266 (class 0 OID 283888)
-- Dependencies: 168 2323
-- Data for Name: categorias_detalles_minutas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY categorias_detalles_minutas (id, descripcion) FROM stdin;
\.


--
-- TOC entry 2618 (class 0 OID 0)
-- Dependencies: 169
-- Name: categorias_detalles_minutas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('categorias_detalles_minutas_id_seq', 1, false);


--
-- TOC entry 2268 (class 0 OID 283893)
-- Dependencies: 170 2323
-- Data for Name: clientes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY clientes (id, nombre, tipo, dni, razon_social, direccion_fiscal, direccion_correspondencia, es_direccion_fiscal, telefonos, fax, correo_electronico, sector_id, sitio_web, es_prospecto, usuario_id, modificado_en) FROM stdin;
10	AST	J	3000000	AST C.A.	dasdasda	sfsdfs	t	(0212) 555-5555	(0212) 555-5555	asda@asda.com.ve	6	http://ast.com.ve	t	1	2015-01-06 11:59:08.447581
23	\N	V	12354678	Empresa de prueba	askdjhalvbalkjbvdl	\N	f	(1234) 876-354223546	\N	\N	3	\N	t	3	2015-03-12 00:00:00
24	\N	V	987654321	HOLA MUNDO	asdfasdfasfdaf	\N	f	(1234) 567-8069	\N	\N	3	\N	t	3	2015-03-12 00:00:00
25	\N	V	98765432	HOLA MUNDO	dsfdjsfagsds	\N	f	(1239) 870-68574	\N	\N	3	\N	t	3	2015-03-12 00:00:00
26	\N	V	88888888	Prueba 2015-03-17 5:36	asdasdasdas	\N	f	(2139) 875-75364	\N	\N	3	\N	t	3	2015-03-17 00:00:00
27	\N	V	99999999	Prueba 2015-03-17 5:38	asasdasdasdas	\N	f	(2345) 698-75433	\N	\N	3	\N	t	3	2015-03-17 00:00:00
15	dasda	E	123123123	asdadasd	sdasdasdas	dasdasdasda	f	(1231) 231-2312	(2334) 234-23422	dasda@sdasda.com	8	http://www.google.com	t	3	2015-01-26 00:00:00
11	prueba validate	V	71234567	prueba validate	direccion fizcal	lkjkljlkjl	f	(0000) 000-0000	(0000) 000-0000	jetox21@gmail.com	6	wwww.google.com	t	1	2015-01-15 00:00:00
12	gffd	V	18888888	gfdsgdsg	gfds	fdsgfdsg	f	(3242) 432-4324	(3243) 243-2432	jetox21@gmail.com	6	www.rrrrr.com	t	1	2015-01-19 00:00:00
13	dfsadfdsa	V	00000000	dsfadsf	dsfdsaf	dfsasd	f	(3453) 214-2332	(3243) 214-3214	jetox21@gmail.com	3	www.gffhgf	t	1	2015-01-19 00:00:00
14	soluciones ast c.a.	J	293726280	soluciones ast c.a.	boleita sur	\N	f	(0212) 234-2627	(0212) 235-1491	ccherres@ast.com	3	www.dasdas.com	t	1	2015-01-19 00:00:00
16	sdfsdfs	V	12312312	dsfsdfs	sdasda	\N	f	(1231) 231-23121	\N	asdada@dasda.cm	8		t	3	2015-01-26 00:00:00
17	asdasdasd	V	12343567	dasdasdas	1asdasdasdasda	\N	f	(1231) 231-23123	\N	\N	8	\N	t	3	2015-01-27 00:00:00
18	\N	V	09876543	asdasdasd	sdasdasdasdas	\N	f	(2312) 312-31233	\N	\N	6	\N	t	3	2015-01-27 00:00:00
19	\N	V	46797555	sdasdasda	asdasdasdasd	\N	f	(1231) 312-31231	\N	\N	6	\N	t	3	2015-01-28 00:00:00
20	\N	V	32346245	sdasdasda	asdadadasa	\N	f	(2312) 312-12313	\N	\N	6	\N	t	3	2015-01-28 00:00:00
21	\N	J	123123131	asdasda	asdasdads	asda	f	(1231) 231-23121	(1231) 231-23122	\N	3	\N	t	3	2015-01-30 00:00:00
\.


--
-- TOC entry 2619 (class 0 OID 0)
-- Dependencies: 171
-- Name: clientes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('clientes_id_seq', 27, true);


--
-- TOC entry 2270 (class 0 OID 283902)
-- Dependencies: 172 2323
-- Data for Name: compromisos_pagos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY compromisos_pagos (id, codigo, factura_id, cantidad_cuotas, frecuencia_pago_id, fecha_inicio, usuario_id, modificado_en) FROM stdin;
\.


--
-- TOC entry 2620 (class 0 OID 0)
-- Dependencies: 173
-- Name: compromisos_pagos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('compromisos_pagos_id_seq', 1, false);


--
-- TOC entry 2272 (class 0 OID 283910)
-- Dependencies: 174 2323
-- Data for Name: configuraciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY configuraciones (id, clave, valor) FROM stdin;
1	IVA	12
3	costo_hora_adicional	5000
4	dias_vigencia_estandar_cotizacion	10
\.


--
-- TOC entry 2621 (class 0 OID 0)
-- Dependencies: 175
-- Name: configuraciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('configuraciones_id_seq', 4, true);


--
-- TOC entry 2274 (class 0 OID 283915)
-- Dependencies: 176 2323
-- Data for Name: cotizaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cotizaciones (id, codigo, cliente_id, fecha_creacion, fecha_expiracion, descuento, usuario_id, modificado_en, con_impuestos, porcentaje_impuesto, es_terminada, subtotal, total_impuesto, total_descuento, total, motivo_descuento, fue_enviada, aprobada, costo_hora_adicional, subtotal_con_descuento, promocion) FROM stdin;
4	0000000004	10	2015-01-02 09:47:46.935141	2016-01-01 09:47:46.935141	0	1	2015-01-12 09:47:46.935141	f	0	t	\N	\N	\N	\N	\N	f	f	0	\N	\N
10	0000000010	10	2015-01-15 17:35:57.014001	2015-01-15 00:00:00	0	1	2015-01-15 17:35:57.014001	t	12	t	\N	\N	\N	\N	\N	f	f	0	\N	\N
48	0000000048	10	2015-01-21 17:31:19.145264	2015-01-21 00:00:00	0	1	2015-01-21 17:31:19.145264	t	12	t	1000	120	0	1120	\N	f	f	0	\N	\N
49	0000000049	10	2015-01-22 11:40:37.697399	2015-01-22 00:00:00	0.0199999996	1	2015-01-22 11:40:37.697399	t	12	t	100004590	12000550.8000000007	22401.0299999999988	111982739.769999996	\N	f	f	0	\N	\N
50	0000000050	10	2015-01-23 17:00:47.714164	2015-01-23 17:00:47.714164	0	1	2015-01-23 17:00:47.714164	t	12	t	2000	240	0	2240	\N	f	f	0	\N	\N
51	0000000051	10	2015-01-23 17:07:17.727896	2015-01-23 17:07:17.727896	0	3	2015-01-23 17:07:17.727896	t	12	t	2000	240	0	2240	\N	f	f	0	\N	\N
52	0000000052	10	2015-01-26 10:38:08.920837	2015-01-26 10:38:08.920837	0	3	2015-01-26 10:38:08.920837	t	12	t	200000	24000	0	224000	\N	f	f	0	\N	\N
53	0000000053	14	2015-01-29 12:08:31.527881	2015-01-29 12:08:31.527881	0	3	2015-01-29 12:08:31.527881	t	12	f	\N	\N	\N	\N	\N	f	f	0	\N	\N
80	AST - 2015 - 00026	10	2015-02-02 17:39:32.145536	2015-02-02 17:39:32.145536	0	3	2015-02-02 17:39:32.145536	t	12	t	200000	24000	0	224000	\N	f	f	0	\N	\N
59	AST201502020010	10	2015-02-02 15:56:10.714803	2015-02-02 15:56:10.714803	0	3	2015-02-02 15:56:10.714803	t	12	t	20000	2400	0	22400	\N	f	f	0	\N	\N
82	AST - 2015 - 00027	10	2015-02-02 17:43:55.337799	2015-02-02 17:43:55.337799	0	3	2015-02-02 17:43:55.337799	t	12	t	20000	2400	0	22400	\N	t	f	0	\N	\N
84	AST - 2015 - 00028	10	2015-02-02 17:44:48.546207	2015-02-02 17:44:48.546207	0	3	2015-02-02 17:44:48.546207	t	12	t	20000	2400	0	22400	\N	f	f	0	\N	\N
85	AST - 2015 - 00029	10	2015-02-03 10:21:36.791973	2015-02-03 00:00:00	10	3	2015-02-03 10:21:36.791973	t	12	t	20000	2400	2000	20400	Porque es cliente fijo	f	f	0	\N	\N
86	AST - 2015 - 00030	10	2015-02-10 16:42:07.212102	2015-06-30 00:00:00	1	3	2015-02-10 16:42:07.212102	t	12	t	200000	24000	2000	222000	Descuento de prueba	f	f	0	\N	\N
87	AST - 2015 - 00031	11	2015-02-10 16:44:43.850303	2015-02-10 00:00:00	0	3	2015-02-10 16:44:43.850303	t	12	t	300000	36000	0	336000		f	f	0	\N	\N
88	AST - 2015 - 00032	10	2015-02-12 14:54:23.860308	2015-02-27 00:00:00	10	3	2015-02-12 14:54:23.860308	t	12	t	500	60	50	510	Descuento de muestra para la cotización	f	f	0	\N	\N
96	AST - 2015 - 00039	15	2015-03-02 10:54:53.961439	2015-03-12 10:54:53.961439	0	3	2015-03-02 10:54:53.961439	t	12	f	\N	\N	\N	\N	\N	f	f	5000	\N	\N
97	AST - 2015 - 00040	18	2015-03-02 10:55:20.722114	2015-03-12 10:55:20.722114	0	3	2015-03-02 10:55:20.722114	t	12	f	\N	\N	\N	\N	\N	f	f	5000	\N	\N
98	AST - 2015 - 00041	21	2015-03-02 11:10:22.997948	2015-03-12 11:10:22.997948	0	3	2015-03-02 11:10:22.997948	t	12	f	\N	\N	\N	\N	\N	f	f	5000	\N	\N
90	AST - 2015 - 00033	10	2015-02-18 16:57:00.451954	2015-02-18 00:00:00	0	3	2015-02-18 16:57:00.451954	t	12	t	150000	18000	0	168000		f	t	15000	\N	\N
3	0000000003	10	2015-01-11 09:41:35.807256	2016-01-10 00:00:00	10	1	2015-01-12 09:41:35.807256	t	12	t	\N	\N	\N	\N	\N	f	t	0	\N	\N
91	AST - 2015 - 00034	10	2015-02-18 17:24:49.981015	2015-02-27 00:00:00	10	3	2015-02-18 17:24:49.981015	t	12	t	650000	78000	65000	663000		t	t	15000	\N	\N
92	AST - 2015 - 00035	10	2015-02-20 12:01:47.046011	2015-02-20 00:00:00	10	3	2015-02-20 12:01:47.046011	t	12	t	150000	18000	15000	153000		t	f	15000	135000	\N
93	AST - 2015 - 00036	10	2015-02-24 08:58:05.505905	2015-02-24 00:00:00	0.0799999982	3	2015-02-24 08:58:05.505905	t	12	t	150230	18027.5999999999985	120.180000000000007	168137.420000000013	Por pana	f	f	5000	150109.820000000007	\N
94	AST - 2015 - 00037	10	2015-02-26 12:08:26.999298	2015-03-08 12:08:26.999298	0	3	2015-02-26 12:08:26.999298	t	12	t	150000	18000	0	168000	\N	f	f	5000	150000	\N
117	AST - 2015 - 00046	10	2015-03-20 14:35:32	2015-03-30 14:35:32	0	3	2015-03-20 14:35:32	t	12	f	\N	\N	\N	\N	\N	f	f	5000	\N	\N
\.


--
-- TOC entry 2320 (class 0 OID 299356)
-- Dependencies: 222 2323
-- Data for Name: cotizaciones_formas_pagos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cotizaciones_formas_pagos (id, cotizacion_id, forma_pago_id, tipo_plazo, dias, porcentaje, usuario_id, modificado_en, intervalo) FROM stdin;
13	93	1	entrega	1	50	1	2015-02-27 15:01:23.577147	\N
18	93	1	entrega	1	40	1	2015-02-27 15:01:23.577147	\N
21	93	1	dias	7	10	1	2015-02-27 15:01:23.577147	\N
29	94	1	inicio	1	99	1	2015-02-27 15:01:23.577147	\N
30	94	2	dias	1	1	1	2015-02-27 15:01:23.577147	\N
47	117	1	entrega	1	100	3	2015-03-20 15:31:15	{"dias":""}
\.


--
-- TOC entry 2622 (class 0 OID 0)
-- Dependencies: 221
-- Name: cotizaciones_formas_pagos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cotizaciones_formas_pagos_id_seq', 47, true);


--
-- TOC entry 2623 (class 0 OID 0)
-- Dependencies: 177
-- Name: cotizaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cotizaciones_id_seq', 117, true);


--
-- TOC entry 2315 (class 0 OID 286513)
-- Dependencies: 217 2323
-- Data for Name: cotizaciones_recursos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY cotizaciones_recursos (id, cotizacion_id, recurso_id, cantidad, usuario_id, modificado_en, orden) FROM stdin;
1	85	1	1	3	2015-02-04 16:49:20.31906	\N
5	92	1	1	3	2015-02-20 12:01:55.53702	\N
7	92	2	3	3	2015-02-23 13:49:31.107926	\N
8	93	1	1	3	2015-02-25 10:17:18.183969	\N
9	94	2	1	3	2015-02-26 12:08:40.56626	\N
29	117	1	1	3	2015-03-20 15:30:53	1
\.


--
-- TOC entry 2624 (class 0 OID 0)
-- Dependencies: 216
-- Name: cotizaciones_recursos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('cotizaciones_recursos_id_seq', 29, true);


--
-- TOC entry 2276 (class 0 OID 283923)
-- Dependencies: 178 2323
-- Data for Name: facturas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY facturas (id, cliente_id, codigo, fecha_creacion, con_impuestos, porcentaje_impuesto, descuento, usuario_id, modificado_en) FROM stdin;
1	10	0000000001	2015-01-12 17:08:35.717116	f	0	0	3	2015-01-12 17:08:35.717116
2	10	0000000002	2015-01-12 17:08:42.029869	f	0	0	3	2015-01-12 17:08:42.029869
3	10	0000000003	2015-01-12 17:11:28.590966	f	0	0	3	2015-01-12 17:11:28.590966
4	10	0000000004	2015-01-16 15:50:03.183433	f	0	0	3	2015-01-16 15:50:03.183433
5	10	0000000005	2015-01-16 16:47:26.496665	f	0	0	3	2015-01-16 16:47:26.496665
6	10	0000000006	2015-01-22 15:36:55.562118	f	0	0	3	2015-01-22 15:36:55.562118
\.


--
-- TOC entry 2625 (class 0 OID 0)
-- Dependencies: 179
-- Name: facturas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('facturas_id_seq', 6, true);


--
-- TOC entry 2318 (class 0 OID 299331)
-- Dependencies: 220 2323
-- Data for Name: formas_pagos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY formas_pagos (id, descripcion, usuario_id, modificado_en) FROM stdin;
2	Crédito	1	2015-02-27 14:59:30.386326
3	Nota de pagos	3	2015-02-27 15:47:16
1	Contado	3	2015-02-27 15:51:48
4	asdasdasdasda	3	2015-03-02 13:09:31
\.


--
-- TOC entry 2626 (class 0 OID 0)
-- Dependencies: 219
-- Name: formas_pagos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('formas_pagos_id_seq', 4, true);


--
-- TOC entry 2278 (class 0 OID 283931)
-- Dependencies: 180 2323
-- Data for Name: frecuencias_pagos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY frecuencias_pagos (id, dias, descripcion, usuario_id, modificado_en) FROM stdin;
\.


--
-- TOC entry 2627 (class 0 OID 0)
-- Dependencies: 181
-- Name: frecuencias_pagos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('frecuencias_pagos_id_seq', 1, false);


--
-- TOC entry 2311 (class 0 OID 285453)
-- Dependencies: 213 2323
-- Data for Name: horarios; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY horarios (id, descripcion, usuario_id, modificado_en) FROM stdin;
1	Horario2	0	2015-01-23 18:07:07.449974
\.


--
-- TOC entry 2628 (class 0 OID 0)
-- Dependencies: 212
-- Name: horarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('horarios_id_seq', 1, true);


--
-- TOC entry 2280 (class 0 OID 283936)
-- Dependencies: 182 2323
-- Data for Name: minutas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY minutas (id, cliente_id, descripcion, fecha_creacion, usuario_id, modificado_en) FROM stdin;
\.


--
-- TOC entry 2281 (class 0 OID 283942)
-- Dependencies: 183 2323
-- Data for Name: minutas_detalles; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY minutas_detalles (id, minuta_id, acuerdo, producto_id, categoria_id, fecha_respuesta, fecha_notificacion, administrativo, tecnico, realizado, usuario_id, modificado_en) FROM stdin;
\.


--
-- TOC entry 2629 (class 0 OID 0)
-- Dependencies: 184
-- Name: minutas_detalles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('minutas_detalles_id_seq', 1, false);


--
-- TOC entry 2630 (class 0 OID 0)
-- Dependencies: 185
-- Name: minutas_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('minutas_id_seq', 1, false);


--
-- TOC entry 2284 (class 0 OID 283952)
-- Dependencies: 186 2323
-- Data for Name: pagos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY pagos (id, compromiso_pago_id, fecha_pago, comprobante_pago, fecha_inicia_periodo, fecha_fin_periodo, es_pago, es_vencido, fecha_vencimiento, fecha_notificacion_vencimiento, usuario_id, modificado_en) FROM stdin;
\.


--
-- TOC entry 2631 (class 0 OID 0)
-- Dependencies: 187
-- Name: pagos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('pagos_id_seq', 1, false);


--
-- TOC entry 2286 (class 0 OID 283958)
-- Dependencies: 188 2323
-- Data for Name: participantes; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY participantes (id, persona_contacto_id, minuta_id, usuario_id, modificado_en) FROM stdin;
\.


--
-- TOC entry 2632 (class 0 OID 0)
-- Dependencies: 189
-- Name: participantes_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('participantes_id_seq', 1, false);


--
-- TOC entry 2288 (class 0 OID 283963)
-- Dependencies: 190 2323
-- Data for Name: personas_contactos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY personas_contactos (id, nombre, apellido, cedula, direccion, telefono_1, telefono_2, correo_electronico_1, correo_electronico_2, cliente_id, usuario_id, modificado_en, horario_id, cargo_id, es_interno) FROM stdin;
19	asdasda	asdasdasd	\N	dasdasda	(1231) 231-23121	(1231) 123-12312	\N	\N	21	3	2015-01-30 00:00:00	1	\N	\N
20	asdadasdasdaas	asdasdasdasda		asdasdasdasda	(2131) 231-31312		asdasda@asdasda.com	asdasda@asda.com	13	3	2015-01-30 00:00:00	1	1	f
27	sdgfadgasdfgsdfgs	dfgsdfgsfgsdfg	\N	sadfasdfasdfasd	(2321) 343-14525	\N		\N	27	3	2015-03-17 00:00:00	\N	1	\N
29	dasdad	asdasdasd	\N	asdasdasdasd	(1231) 231-31233	\N	\N	\N	15	3	2015-03-20 11:04:17	1	1	t
5	carol	cherres	V-14574644	la cancelaria	(0212) 566-8876	(0212) 456-7865	ccherres@ast.com	ccherres@ast.com	14	1	2015-01-19 00:00:00	\N	\N	\N
12	dadasdas	\N	\N	wqewdasdasd	(1231) 231-23121	\N	\N	\N	19	3	2015-01-28 00:00:00	\N	\N	t
13	asasdasasd	\N	\N	asdadasdasdasda	(1231) 231-23122	\N	\N	\N	20	3	2015-01-28 00:00:00	\N	\N	\N
8	dasdasda			sdasdasda	(1231) 231-23123				17	3	2015-01-27 00:00:00	1	1	t
9	dasdasd	\N	\N	sdasdasdas	(1231) 231-2	(1231) 231-231	asdasda@asda.com	\N	17	3	2015-01-27 00:00:00	1	1	f
10	asdasdasda			asdasdasdasdas	(1231) 231-23122				18	3	2015-01-27 00:00:00	1	\N	f
11	dsasdadas	\N	\N	sdasdasdasda	(1231) 231-23122	(2312) 313-1312	\N	\N	18	3	2015-01-28 00:00:00	1	\N	f
21	Eduardo	Villegas		AST	(0212) 555-5555		evillegas@ast.com.ve		10	3	2015-02-13 00:00:00	1	1	t
7	Darwin	Serrano		dklasjdalksdalksda	(1232) 353-4123		dserrano@ast.com.ve		10	3	2015-01-27 00:00:00	1	1	f
22	José	Rodríguez		AST	(0212) 555-5555		jrodriguez@ast.com.ve		10	3	2015-02-13 00:00:00	1	1	t
23	Elizabeth	García		AST	(0212) 555-5555		egarcia@ast.com.ve		10	3	2015-02-13 00:00:00	1	1	t
1	jefferson Arturo	lara molina	V-17042979	los tequess	(0414) 466-4081	(0414) 466-4081	jetox21@gmail.com	jetox21@gmail.com	10	1	2015-01-14 00:00:00	\N	\N	f
25	asdas			asdasd	(2131) 231-23121				10	3	2015-02-13 00:00:00	\N	\N	f
\.


--
-- TOC entry 2633 (class 0 OID 0)
-- Dependencies: 191
-- Name: personas_contactos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('personas_contactos_id_seq', 29, true);


--
-- TOC entry 2290 (class 0 OID 283971)
-- Dependencies: 192 2323
-- Data for Name: productos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productos (id, codigo, nombre, descripcion, precio, tipo_producto_id, requisitos, activo, usuario_id, modificado_en) FROM stdin;
4	dfsfsd	dsfds	xczxczxc	4343.24023	1	xzcxzcxzcz	t	1	2015-01-19 16:57:59.512694
2	0002	jhjkhjhkjhkj	FDGFDSgfdsgdgfdsgfds	230	1	jhfkjdahkjfhdakjf	t	1	2015-01-14 13:47:38.359681
5	hdesarrollo	Hora Desarrollo	Costo de la hora de desarrollo	500	4	Especificaciones del producto	t	3	2015-02-13 10:31:26.860124
3	324324	fdsgfdgfdsg	dfdsafdsa	3333	2	dsfdsafdsafdsa	t	3	2015-01-15 15:14:47.741711
1	00001	AST	Prueba4 a	150000	4	Prueba	t	3	2015-01-12 09:41:26.828969
6	dasdasdasd	asdasdasd	asdasdas	12223.2998	3	dasdasdasdasd	t	3	2015-03-20 13:27:16.763667
\.


--
-- TOC entry 2291 (class 0 OID 283978)
-- Dependencies: 193 2323
-- Data for Name: productos_cotizaciones; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productos_cotizaciones (id, cotizacion_id, producto_id, precio, descuento, usuario_id, modificado_en, cantidad, precio_total) FROM stdin;
2	3	1	100000	0	3	2015-01-12 09:42:02.414244	1	\N
3	4	1	100000	0	3	2015-01-12 09:48:23.510003	1	\N
4	10	1	1000	0	3	2015-01-19 16:52:07.300895	1	\N
5	10	1	1000	0	3	2015-01-19 16:54:08.421338	1	\N
6	10	1	1000	0	3	2015-01-19 16:54:25.038382	1	\N
10	48	1	1000	0	3	2015-01-22 11:27:02.870217	1	\N
13	49	2	10	0	3	2015-01-22 16:10:54.063777	10000000	100000000
14	49	1	1000	0	3	2015-01-22 16:45:04.535907	1	1000
15	49	2	10	0	3	2015-01-22 16:57:42.711568	1	10
16	49	2	10	0	3	2015-01-22 17:28:20.528117	50	500
17	49	2	10	0	3	2015-01-22 17:29:11.099266	50	500
18	49	2	10	0	3	2015-01-22 17:30:44.199929	50	500
19	49	2	12	0	3	2015-01-22 17:33:10.378174	50	600
20	49	2	125	0	3	2015-01-23 10:03:16.381603	10	1250
21	49	2	230	0	3	2015-01-23 11:19:39.68136	1	230
22	50	1	2000	0	1	2015-01-23 17:00:57.859706	1	2000
23	51	1	2000	0	3	2015-01-23 17:07:22.621596	1	2000
24	52	1	20000	0	3	2015-01-26 10:43:43.773009	10	200000
26	59	1	20000	0	3	2015-02-02 16:56:08.212155	1	20000
29	80	1	20000	0	3	2015-02-02 17:39:43.55419	10	200000
31	82	1	20000	0	3	2015-02-02 17:44:05.894466	1	20000
32	84	1	20000	0	3	2015-02-02 17:44:58.890715	1	20000
33	85	1	20000	0	3	2015-02-03 10:21:47.409178	1	20000
34	86	1	20000	0	3	2015-02-10 16:42:18.983703	10	200000
35	87	1	20000	0	3	2015-02-10 16:44:52.772355	15	300000
36	88	5	500	0	3	2015-02-13 10:32:29.635553	1	500
38	90	1	150000	0	3	2015-02-18 16:57:47.879529	1	150000
39	91	1	150000	0	3	2015-02-20 11:47:54.319358	1	150000
40	91	5	500	0	3	2015-02-20 11:48:28.537174	1000	500000
41	92	1	150000	0	3	2015-02-20 12:09:23.367695	1	150000
42	93	1	150000	0	3	2015-02-25 09:32:59.446351	1	150000
43	93	2	230	0	3	2015-02-25 14:16:43.806125	1	230
44	94	1	150000	0	3	2015-02-26 12:08:34.009262	1	150000
55	117	5	500	0	3	2015-03-20 15:30:42	1	500
\.


--
-- TOC entry 2634 (class 0 OID 0)
-- Dependencies: 194
-- Name: productos_cotizaciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('productos_cotizaciones_id_seq', 55, true);


--
-- TOC entry 2635 (class 0 OID 0)
-- Dependencies: 196
-- Name: productos_factura_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('productos_factura_id_seq', 1, true);


--
-- TOC entry 2293 (class 0 OID 283983)
-- Dependencies: 195 2323
-- Data for Name: productos_facturas; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productos_facturas (id, factura_id, producto_id, fecha_adquisicion, fecha_activacion, fecha_vencimiento, precio, descuento, activo, usuario_id, modificado_en, cantidad) FROM stdin;
1	6	3	\N	\N	\N	3333	0	t	3	2015-01-22 15:37:05.421129	1
\.


--
-- TOC entry 2295 (class 0 OID 283988)
-- Dependencies: 197 2323
-- Data for Name: productos_facturas_estados; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productos_facturas_estados (id, estado, producto_factura_id, cliente_producto_id, usuario_id, modificado_en) FROM stdin;
\.


--
-- TOC entry 2636 (class 0 OID 0)
-- Dependencies: 198
-- Name: productos_facturas_estados_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('productos_facturas_estados_id_seq', 1, false);


--
-- TOC entry 2307 (class 0 OID 285286)
-- Dependencies: 209 2323
-- Data for Name: productos_historico_precio; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY productos_historico_precio (id, producto_id, precio_anterior, precio_nuevo, usuario_id, modificado_en) FROM stdin;
1	1	1000	10	1	2015-01-23 11:15:04.07024
2	1	10	2000	1	2015-01-23 11:16:00.107926
3	2	125	230	1	2015-01-23 11:19:39.274537
4	1	2000	20000	1	2015-01-26 10:43:43.593203
5	5	50	500	3	2015-02-13 10:32:04.682874
6	1	20000	150000	1	2015-02-18 11:26:25.289212
7	1	150000	150000	3	2015-02-18 17:17:38.168139
8	1	150000	150000	3	2015-02-18 17:17:52.29411
9	1	150000	150000	3	2015-02-18 17:18:05.763398
10	1	150000	150000	3	2015-02-18 17:20:01.111125
11	1	150000	150000	3	2015-02-18 17:20:09.435947
12	1	150000	150000	3	2015-02-18 17:20:58.336325
13	1	150000	150000	3	2015-02-18 17:21:40.292825
14	1	150000	150000	3	2015-03-06 12:38:18.348828
15	5	500	500	3	2015-03-06 12:38:39.277253
16	3	3333	3333	3	2015-03-06 12:38:59.053823
17	1	150000	150000	3	2015-03-20 12:13:33.2531
18	1	150000	150000	3	2015-03-20 12:13:38.88621
19	6	12312312283136	1231231.25	3	2015-03-20 13:27:40.375851
20	6	1231231.25	123333312	3	2015-03-20 13:31:00.061561
21	6	123333312	12223.330078125	3	2015-03-20 13:31:29.201211
22	6	12223.330078125	12223.2998046875	3	2015-03-20 13:58:44.759724
23	6	12223.2998046875	12223.2998046875	3	2015-03-20 13:58:57.353519
24	6	12223.2998046875	12223.2998046875	3	2015-03-20 14:27:01.812932
\.


--
-- TOC entry 2637 (class 0 OID 0)
-- Dependencies: 208
-- Name: productos_historico_precio_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('productos_historico_precio_id_seq', 24, true);


--
-- TOC entry 2638 (class 0 OID 0)
-- Dependencies: 199
-- Name: productos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('productos_id_seq', 6, true);


--
-- TOC entry 2313 (class 0 OID 286403)
-- Dependencies: 215 2323
-- Data for Name: recursos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY recursos (id, descripcion, codigo, usuario_id, modificado_en, informacion_adicional) FROM stdin;
4	PC	0004	1	2015-02-04 15:13:41.575727	\N
2	Desarrollador Medio	0002	3	2015-03-09 16:20:30	[{"descripcion":"<p>Amplia experiencia en el manejo de proyectos, manejo de tiempo, entregables, con la facilidad de trabajar bajo presi\\u00f3n, y fuertes conocimientos en dise\\u00f1o, an\\u00e1lisis y desarrollo de aplicaciones web.","titulo":"Perfil"},{"titulo":"Descripci\\u00f3n","descripcion":"<p>Planificar y dirigir todas las programaciones y actividades asociadas al departamento de sistemas. Administrar la actividad de todo el personal. Reportar a la gerencia el estado de los proyectos y el manejo de los recursos.<\\/p>"},{"titulo":"Funciones y Responsabilidades","descripcion":"<ul><li>Recibir y evaluar requerimientos de programaci\\u00f3n, planes y agenda de los proyectos.<\\/li><li>Relacionar los tiempos de los proyectos en funci\\u00f3n a las necesidades de los usuarios y a los recursos disponibles.<\\/li><li>Establecer est\\u00e1ndares de programaci\\u00f3n y documentaci\\u00f3n.<\\/li><li>Supervisar dise\\u00f1o,codificaci\\u00f3n de programas, integraci\\u00f3n y testeo de c\\u00f3digo.<\\/li><\\/ul>"}]
3	Desarrollador Junior	0003	3	2015-03-09 16:24:32	[{"descripcion":"<p>Amplia experiencia en el manejo de proyectos, manejo de tiempo, entregables, con la facilidad de trabajar bajo presi\\u00f3n, y fuertes conocimientos en dise\\u00f1o, an\\u00e1lisis y desarrollo de aplicaciones web.<\\/p>","titulo":"Perfil"},{"titulo":"Descripci\\u00f3n","descripcion":"<p>Planificar y dirigir todas las programaciones y actividades asociadas al departamento de sistemas. Administrar la actividad de todo el personal. Reportar a la gerencia el estado de los proyectos y el manejo de los recursos.<\\/p>"},{"titulo":"Funciones y Responsabilidades","descripcion":"<ul><li>Recibir y evaluar requerimientos de programaci\\u00f3n, planes y agenda de los proyectos.<\\/li><li>Relacionar los tiempos de los proyectos en funci\\u00f3n a las necesidades de los usuarios y a los recursos disponibles.<\\/li><li>Establecer est\\u00e1ndares de programaci\\u00f3n y documentaci\\u00f3n.<\\/li><li>Supervisar dise\\u00f1o,codificaci\\u00f3n de programas, integraci\\u00f3n y testeo de c\\u00f3digo.<\\/li><\\/ul>"}]
5	Servidor	0005	3	2015-02-19 12:26:23.501015	\N
6	LAPTOP	00012	3	2015-03-20 12:27:04	null
7	asdasdasd	1000000	3	2015-03-02 13:09:00	[{"titulo":"asdasdasdasda","descripcion":"<p>asdasdasdasda<\\/p>"}]
1	Desarrollador Senior	0001	3	2015-03-09 16:47:01	[{"descripcion":"<p>Amplia experiencia en el manejo de proyectos, manejo de tiempo, entregables, con la facilidad de trabajar bajo presi\\u00f3n, y fuertes conocimientos en dise\\u00f1o, an\\u00e1lisis y desarrollo de aplicaciones web.<\\/p>","titulo":"Perfil"},{"titulo":"Descripci\\u00f3n","descripcion":"<p>Planificar y dirigir todas las programaciones y actividades asociadas al departamento de sistemas. Administrar la actividad de todo el personal. Reportar a la gerencia el estado de los proyectos y el manejo de los recursos.<\\/p>"},{"titulo":"Funciones y Responsabilidades","descripcion":"<ul><li>Recibir y evaluar requerimientos de programaci\\u00f3n, planes y agenda de los proyectos.<\\/li><li>Relacionar los tiempos de los proyectos en funci\\u00f3n a las necesidades de los usuarios y a los recursos disponibles.<\\/li><li>Establecer est\\u00e1ndares de programaci\\u00f3n y documentaci\\u00f3n.<\\/li><li>Supervisar dise\\u00f1o,codificaci\\u00f3n de programas, integraci\\u00f3n y testeo de c\\u00f3digo.<\\/li><\\/ul>"}]
8	asdasdasdasda	RJVU85	3	2015-03-17 11:59:51	[]
9	agadfgdfgsddddd	0987654	3	2015-03-20 12:28:42	[]
11	nose3	nose3	3	2015-03-20 16:55:31	[]
10	asdasdasdasdasdasdasdasd	11111111111	3	2015-03-20 17:04:06	[]
\.


--
-- TOC entry 2639 (class 0 OID 0)
-- Dependencies: 214
-- Name: recursos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('recursos_id_seq', 11, true);


--
-- TOC entry 2298 (class 0 OID 283995)
-- Dependencies: 200 2323
-- Data for Name: sectores; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY sectores (id, nombre, usuario_id, modificado_en) FROM stdin;
3	Financiero	0	2015-01-06 11:57:52.75042
6	Darwin	0	2015-01-06 00:00:00
8	Goku	0	2015-01-23 17:36:02.804361
12	Sector 1	0	2015-01-29 12:38:33.944615
14	Sector 1	0	2015-01-29 12:38:43.59799
15	Sector 1	0	2015-01-29 12:38:57.850574
17	Sector 1	0	2015-01-29 12:39:08.131262
18	Sector 1	0	2015-01-29 12:39:13.524358
19	Sector 1	0	2015-01-29 12:39:20.26876
20	Sector 1	0	2015-01-29 12:39:30.072514
21	Sector 1	0	2015-01-29 12:39:47.0264
22	Sector 1	0	2015-01-29 12:39:51.351598
23	Sector 1	0	2015-01-29 12:39:54.130727
24	Sector 1	0	2015-01-29 12:39:56.556263
25	Sector 1	0	2015-01-29 12:39:58.608122
26	Sector 1	0	2015-01-29 12:40:00.358994
27	Sector 1	0	2015-01-29 12:40:02.541151
28	Sector 1	0	2015-01-29 12:40:04.862933
29	Sector 1	0	2015-01-29 12:40:07.188086
30	Sector 1	0	2015-01-29 12:40:09.573938
31	Sector 1	0	2015-01-29 12:40:12.365984
32	Sector 1	0	2015-01-29 12:40:14.758671
33	Sector 1	0	2015-01-29 12:40:17.329744
9	Otro Sector	0	2015-01-29 12:38:19.275338
10	Sector editado	0	2015-01-29 12:38:27.147871
11	Sin Borrar Cache	0	2015-01-29 12:38:30.847218
13	Borrando Cache	0	2015-01-29 12:38:37.385684
35	Nuevo	0	2015-01-30 10:53:19.438052
34	Actualizo Cache	0	2015-01-29 12:40:20.698212
36	otro sector	0	2015-03-17 14:57:01.385788
16	asdasdasda	0	2015-01-29 12:39:01.613838
\.


--
-- TOC entry 2640 (class 0 OID 0)
-- Dependencies: 201
-- Name: sectores_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('sectores_id_seq', 36, true);


--
-- TOC entry 2641 (class 0 OID 0)
-- Dependencies: 218
-- Name: seq_codigo_cotizacion; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('seq_codigo_cotizacion', 46, true);


--
-- TOC entry 2300 (class 0 OID 284000)
-- Dependencies: 202 2323
-- Data for Name: tipos_productos; Type: TABLE DATA; Schema: public; Owner: postgres
--

COPY tipos_productos (id, descripcion, usuario_id, modificado_en) FROM stdin;
1	Desarrollo	1	2015-01-12 09:40:48.125655
2	Servicio	1	2015-01-12 11:03:46.35334
3	prueba4	1	2015-01-23 14:41:22.267881
4	Session	1	2015-01-23 15:46:59.598114
5	prueba	3	2015-03-20 17:08:57
\.


--
-- TOC entry 2642 (class 0 OID 0)
-- Dependencies: 203
-- Name: tipos_productos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('tipos_productos_id_seq', 5, true);


SET search_path = acl, pg_catalog;

--
-- TOC entry 2177 (class 2606 OID 284206)
-- Dependencies: 204 204 2324
-- Name: ci_sessions_pkey; Type: CONSTRAINT; Schema: acl; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY ci_sessions
    ADD CONSTRAINT ci_sessions_pkey PRIMARY KEY (session_id);


--
-- TOC entry 2133 (class 2606 OID 284030)
-- Dependencies: 162 162 2324
-- Name: groups_pkey; Type: CONSTRAINT; Schema: acl; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY groups
    ADD CONSTRAINT groups_pkey PRIMARY KEY (id);


--
-- TOC entry 2184 (class 2606 OID 284278)
-- Dependencies: 207 207 207 2324
-- Name: pk_group_operation; Type: CONSTRAINT; Schema: acl; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY group_operation
    ADD CONSTRAINT pk_group_operation PRIMARY KEY (id_operation, id_group);


--
-- TOC entry 2180 (class 2606 OID 284257)
-- Dependencies: 206 206 2324
-- Name: pk_operation; Type: CONSTRAINT; Schema: acl; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY operation
    ADD CONSTRAINT pk_operation PRIMARY KEY (id);


--
-- TOC entry 2137 (class 2606 OID 284036)
-- Dependencies: 165 165 165 2324
-- Name: uc_users_groups; Type: CONSTRAINT; Schema: acl; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_groups
    ADD CONSTRAINT uc_users_groups UNIQUE (user_id, group_id);


--
-- TOC entry 2182 (class 2606 OID 284259)
-- Dependencies: 206 206 2324
-- Name: unique_url; Type: CONSTRAINT; Schema: acl; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY operation
    ADD CONSTRAINT unique_url UNIQUE (url);


--
-- TOC entry 2139 (class 2606 OID 284038)
-- Dependencies: 165 165 2324
-- Name: users_groups_pkey; Type: CONSTRAINT; Schema: acl; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users_groups
    ADD CONSTRAINT users_groups_pkey PRIMARY KEY (id);


--
-- TOC entry 2135 (class 2606 OID 284040)
-- Dependencies: 164 164 2324
-- Name: users_pkey; Type: CONSTRAINT; Schema: acl; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


SET search_path = public, pg_catalog;

--
-- TOC entry 2198 (class 2606 OID 299361)
-- Dependencies: 222 222 2324
-- Name: cotizaciones_formas_pagos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cotizaciones_formas_pagos
    ADD CONSTRAINT cotizaciones_formas_pagos_pkey PRIMARY KEY (id);


--
-- TOC entry 2194 (class 2606 OID 286518)
-- Dependencies: 217 217 2324
-- Name: cotizaciones_recursos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cotizaciones_recursos
    ADD CONSTRAINT cotizaciones_recursos_pkey PRIMARY KEY (id);


--
-- TOC entry 2196 (class 2606 OID 299336)
-- Dependencies: 220 220 2324
-- Name: formas_pagos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY formas_pagos
    ADD CONSTRAINT formas_pagos_pkey PRIMARY KEY (id);


--
-- TOC entry 2201 (class 2606 OID 335924)
-- Dependencies: 224 224 2324
-- Name: pk_auditorias_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY auditorias
    ADD CONSTRAINT pk_auditorias_id PRIMARY KEY (id);


--
-- TOC entry 2188 (class 2606 OID 285411)
-- Dependencies: 211 211 2324
-- Name: pk_cargos_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cargos
    ADD CONSTRAINT pk_cargos_id PRIMARY KEY (id);


--
-- TOC entry 2141 (class 2606 OID 284044)
-- Dependencies: 168 168 2324
-- Name: pk_categorias_detalles_minutas_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY categorias_detalles_minutas
    ADD CONSTRAINT pk_categorias_detalles_minutas_id PRIMARY KEY (id);


--
-- TOC entry 2143 (class 2606 OID 284046)
-- Dependencies: 170 170 2324
-- Name: pk_clientes_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY clientes
    ADD CONSTRAINT pk_clientes_id PRIMARY KEY (id);


--
-- TOC entry 2145 (class 2606 OID 284048)
-- Dependencies: 172 172 2324
-- Name: pk_compromisos_pagos_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY compromisos_pagos
    ADD CONSTRAINT pk_compromisos_pagos_id PRIMARY KEY (id);


--
-- TOC entry 2147 (class 2606 OID 284050)
-- Dependencies: 174 174 2324
-- Name: pk_configuraciones_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY configuraciones
    ADD CONSTRAINT pk_configuraciones_id PRIMARY KEY (id);


--
-- TOC entry 2149 (class 2606 OID 284052)
-- Dependencies: 176 176 2324
-- Name: pk_cotizaciones_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY cotizaciones
    ADD CONSTRAINT pk_cotizaciones_id PRIMARY KEY (id);


--
-- TOC entry 2151 (class 2606 OID 284054)
-- Dependencies: 178 178 2324
-- Name: pk_facturas_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY facturas
    ADD CONSTRAINT pk_facturas_id PRIMARY KEY (id);


--
-- TOC entry 2153 (class 2606 OID 284056)
-- Dependencies: 180 180 2324
-- Name: pk_frecuencias_pagos_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY frecuencias_pagos
    ADD CONSTRAINT pk_frecuencias_pagos_id PRIMARY KEY (id);


--
-- TOC entry 2190 (class 2606 OID 285459)
-- Dependencies: 213 213 2324
-- Name: pk_horarios_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY horarios
    ADD CONSTRAINT pk_horarios_id PRIMARY KEY (id);


--
-- TOC entry 2157 (class 2606 OID 284058)
-- Dependencies: 183 183 2324
-- Name: pk_minutas_detalles_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY minutas_detalles
    ADD CONSTRAINT pk_minutas_detalles_id PRIMARY KEY (id);


--
-- TOC entry 2155 (class 2606 OID 284060)
-- Dependencies: 182 182 2324
-- Name: pk_minutas_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY minutas
    ADD CONSTRAINT pk_minutas_id PRIMARY KEY (id);


--
-- TOC entry 2159 (class 2606 OID 284062)
-- Dependencies: 186 186 2324
-- Name: pk_pagos_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY pagos
    ADD CONSTRAINT pk_pagos_id PRIMARY KEY (id);


--
-- TOC entry 2161 (class 2606 OID 284064)
-- Dependencies: 188 188 2324
-- Name: pk_participantes_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY participantes
    ADD CONSTRAINT pk_participantes_id PRIMARY KEY (id);


--
-- TOC entry 2163 (class 2606 OID 284066)
-- Dependencies: 190 190 2324
-- Name: pk_personas_contactos_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY personas_contactos
    ADD CONSTRAINT pk_personas_contactos_id PRIMARY KEY (id);


--
-- TOC entry 2167 (class 2606 OID 284068)
-- Dependencies: 193 193 2324
-- Name: pk_productos_cotizaciones_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productos_cotizaciones
    ADD CONSTRAINT pk_productos_cotizaciones_id PRIMARY KEY (id);


--
-- TOC entry 2169 (class 2606 OID 284070)
-- Dependencies: 195 195 2324
-- Name: pk_productos_factura_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productos_facturas
    ADD CONSTRAINT pk_productos_factura_id PRIMARY KEY (id);


--
-- TOC entry 2171 (class 2606 OID 284072)
-- Dependencies: 197 197 2324
-- Name: pk_productos_facturas_estados_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productos_facturas_estados
    ADD CONSTRAINT pk_productos_facturas_estados_id PRIMARY KEY (id);


--
-- TOC entry 2186 (class 2606 OID 285291)
-- Dependencies: 209 209 2324
-- Name: pk_productos_historico_precio_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productos_historico_precio
    ADD CONSTRAINT pk_productos_historico_precio_id PRIMARY KEY (id);


--
-- TOC entry 2165 (class 2606 OID 284074)
-- Dependencies: 192 192 2324
-- Name: pk_productos_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY productos
    ADD CONSTRAINT pk_productos_id PRIMARY KEY (id);


--
-- TOC entry 2173 (class 2606 OID 284076)
-- Dependencies: 200 200 2324
-- Name: pk_sectores_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY sectores
    ADD CONSTRAINT pk_sectores_id PRIMARY KEY (id);


--
-- TOC entry 2175 (class 2606 OID 284078)
-- Dependencies: 202 202 2324
-- Name: pk_tipos_productos_id; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY tipos_productos
    ADD CONSTRAINT pk_tipos_productos_id PRIMARY KEY (id);


--
-- TOC entry 2192 (class 2606 OID 286421)
-- Dependencies: 215 215 2324
-- Name: recursos_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres; Tablespace: 
--

ALTER TABLE ONLY recursos
    ADD CONSTRAINT recursos_pkey PRIMARY KEY (id);


SET search_path = acl, pg_catalog;

--
-- TOC entry 2178 (class 1259 OID 284207)
-- Dependencies: 204 2324
-- Name: last_activity_idx; Type: INDEX; Schema: acl; Owner: postgres; Tablespace: 
--

CREATE INDEX last_activity_idx ON ci_sessions USING btree (last_activity);


SET search_path = public, pg_catalog;

--
-- TOC entry 2199 (class 1259 OID 338670)
-- Dependencies: 224 224 224 224 224 2324
-- Name: ind_update_auditoria; Type: INDEX; Schema: public; Owner: postgres; Tablespace: 
--

CREATE INDEX ind_update_auditoria ON auditorias USING btree (esquema, tabla, registro_id, transaccion, info_adicional NULLS FIRST);


SET search_path = acl, pg_catalog;

--
-- TOC entry 2234 (class 2620 OID 339906)
-- Dependencies: 239 164 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: acl; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON users FOR EACH ROW EXECUTE PROCEDURE public.ft_auditoria();


--
-- TOC entry 2233 (class 2620 OID 339962)
-- Dependencies: 162 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: acl; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON groups FOR EACH ROW EXECUTE PROCEDURE public.ft_auditoria();


--
-- TOC entry 2252 (class 2620 OID 285002)
-- Dependencies: 237 206 2324
-- Name: trigger_add_operation_to_admin; Type: TRIGGER; Schema: acl; Owner: postgres
--

CREATE TRIGGER trigger_add_operation_to_admin AFTER INSERT ON operation FOR EACH ROW EXECUTE PROCEDURE add_operation_to_admin();


SET search_path = public, pg_catalog;

--
-- TOC entry 2245 (class 2620 OID 285308)
-- Dependencies: 238 192 192 2324
-- Name: t_actualizar_historico_precio; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_actualizar_historico_precio BEFORE UPDATE OF precio ON productos FOR EACH ROW EXECUTE PROCEDURE ft_actualizar_historico_precio();


--
-- TOC entry 2254 (class 2620 OID 334417)
-- Dependencies: 211 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON cargos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2235 (class 2620 OID 335979)
-- Dependencies: 170 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON clientes FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2236 (class 2620 OID 335980)
-- Dependencies: 239 172 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON compromisos_pagos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2237 (class 2620 OID 335981)
-- Dependencies: 239 176 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON cotizaciones FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2259 (class 2620 OID 335982)
-- Dependencies: 239 222 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON cotizaciones_formas_pagos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2257 (class 2620 OID 335983)
-- Dependencies: 217 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON cotizaciones_recursos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2238 (class 2620 OID 335984)
-- Dependencies: 178 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON facturas FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2258 (class 2620 OID 335985)
-- Dependencies: 239 220 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON formas_pagos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2239 (class 2620 OID 335986)
-- Dependencies: 239 180 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON frecuencias_pagos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2255 (class 2620 OID 335987)
-- Dependencies: 239 213 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON horarios FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2240 (class 2620 OID 335988)
-- Dependencies: 182 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON minutas FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2241 (class 2620 OID 335989)
-- Dependencies: 183 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON minutas_detalles FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2242 (class 2620 OID 335990)
-- Dependencies: 239 186 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON pagos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2243 (class 2620 OID 335991)
-- Dependencies: 239 188 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON participantes FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2244 (class 2620 OID 335992)
-- Dependencies: 239 190 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON personas_contactos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2246 (class 2620 OID 335993)
-- Dependencies: 239 192 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON productos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2247 (class 2620 OID 335994)
-- Dependencies: 239 193 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON productos_cotizaciones FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2248 (class 2620 OID 335995)
-- Dependencies: 195 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON productos_facturas FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2249 (class 2620 OID 335996)
-- Dependencies: 239 197 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON productos_facturas_estados FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2253 (class 2620 OID 335997)
-- Dependencies: 239 209 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON productos_historico_precio FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2256 (class 2620 OID 335998)
-- Dependencies: 215 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON recursos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2250 (class 2620 OID 335999)
-- Dependencies: 239 200 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON sectores FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


--
-- TOC entry 2251 (class 2620 OID 336000)
-- Dependencies: 202 239 2324
-- Name: t_auditoria; Type: TRIGGER; Schema: public; Owner: postgres
--

CREATE TRIGGER t_auditoria BEFORE INSERT OR DELETE OR UPDATE ON tipos_productos FOR EACH ROW EXECUTE PROCEDURE ft_auditoria();


SET search_path = acl, pg_catalog;

--
-- TOC entry 2226 (class 2606 OID 284279)
-- Dependencies: 162 207 2132 2324
-- Name: fk_gro_ope_group; Type: FK CONSTRAINT; Schema: acl; Owner: postgres
--

ALTER TABLE ONLY group_operation
    ADD CONSTRAINT fk_gro_ope_group FOREIGN KEY (id_group) REFERENCES groups(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2227 (class 2606 OID 284284)
-- Dependencies: 207 206 2179 2324
-- Name: fk_gro_ope_operation; Type: FK CONSTRAINT; Schema: acl; Owner: postgres
--

ALTER TABLE ONLY group_operation
    ADD CONSTRAINT fk_gro_ope_operation FOREIGN KEY (id_operation) REFERENCES operation(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2225 (class 2606 OID 284260)
-- Dependencies: 206 206 2179 2324
-- Name: fk_ope_operation; Type: FK CONSTRAINT; Schema: acl; Owner: postgres
--

ALTER TABLE ONLY operation
    ADD CONSTRAINT fk_ope_operation FOREIGN KEY (id_operation) REFERENCES operation(id) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- TOC entry 2202 (class 2606 OID 284089)
-- Dependencies: 162 165 2132 2324
-- Name: users_groups_group_id_fkey; Type: FK CONSTRAINT; Schema: acl; Owner: postgres
--

ALTER TABLE ONLY users_groups
    ADD CONSTRAINT users_groups_group_id_fkey FOREIGN KEY (group_id) REFERENCES groups(id);


--
-- TOC entry 2203 (class 2606 OID 284094)
-- Dependencies: 164 165 2134 2324
-- Name: users_groups_user_id_fkey; Type: FK CONSTRAINT; Schema: acl; Owner: postgres
--

ALTER TABLE ONLY users_groups
    ADD CONSTRAINT users_groups_user_id_fkey FOREIGN KEY (user_id) REFERENCES users(id);


SET search_path = public, pg_catalog;

--
-- TOC entry 2210 (class 2606 OID 284099)
-- Dependencies: 183 168 2140 2324
-- Name: categorias_detalles_minutas_minutas_detalles_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY minutas_detalles
    ADD CONSTRAINT categorias_detalles_minutas_minutas_detalles_fk FOREIGN KEY (categoria_id) REFERENCES categorias_detalles_minutas(id);


--
-- TOC entry 2207 (class 2606 OID 284104)
-- Dependencies: 176 170 2142 2324
-- Name: clientes_cotizaciones_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cotizaciones
    ADD CONSTRAINT clientes_cotizaciones_fk FOREIGN KEY (cliente_id) REFERENCES clientes(id) ON UPDATE CASCADE;


--
-- TOC entry 2208 (class 2606 OID 284109)
-- Dependencies: 178 170 2142 2324
-- Name: clientes_facturas_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY facturas
    ADD CONSTRAINT clientes_facturas_fk FOREIGN KEY (cliente_id) REFERENCES clientes(id);


--
-- TOC entry 2209 (class 2606 OID 284114)
-- Dependencies: 2142 170 182 2324
-- Name: clientes_minutas_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY minutas
    ADD CONSTRAINT clientes_minutas_fk FOREIGN KEY (cliente_id) REFERENCES clientes(id) ON UPDATE CASCADE;


--
-- TOC entry 2216 (class 2606 OID 284119)
-- Dependencies: 190 170 2142 2324
-- Name: clientes_personas_contactos_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personas_contactos
    ADD CONSTRAINT clientes_personas_contactos_fk FOREIGN KEY (cliente_id) REFERENCES clientes(id);


--
-- TOC entry 2213 (class 2606 OID 284124)
-- Dependencies: 186 172 2144 2324
-- Name: compromisos_pagos_pagos_realizados_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY pagos
    ADD CONSTRAINT compromisos_pagos_pagos_realizados_fk FOREIGN KEY (compromiso_pago_id) REFERENCES compromisos_pagos(id);


--
-- TOC entry 2231 (class 2606 OID 299362)
-- Dependencies: 222 176 2148 2324
-- Name: cotizaciones_formas_pagos_cotizacion_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cotizaciones_formas_pagos
    ADD CONSTRAINT cotizaciones_formas_pagos_cotizacion_id_fkey FOREIGN KEY (cotizacion_id) REFERENCES cotizaciones(id) ON UPDATE CASCADE;


--
-- TOC entry 2232 (class 2606 OID 299367)
-- Dependencies: 220 2195 222 2324
-- Name: cotizaciones_formas_pagos_forma_pago_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cotizaciones_formas_pagos
    ADD CONSTRAINT cotizaciones_formas_pagos_forma_pago_id_fkey FOREIGN KEY (forma_pago_id) REFERENCES formas_pagos(id) ON UPDATE CASCADE;


--
-- TOC entry 2220 (class 2606 OID 284129)
-- Dependencies: 193 176 2148 2324
-- Name: cotizaciones_productos_cotizaciones_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_cotizaciones
    ADD CONSTRAINT cotizaciones_productos_cotizaciones_fk FOREIGN KEY (cotizacion_id) REFERENCES cotizaciones(id) ON UPDATE CASCADE;


--
-- TOC entry 2229 (class 2606 OID 286519)
-- Dependencies: 217 176 2148 2324
-- Name: cotizaciones_recursos_cotizacion_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cotizaciones_recursos
    ADD CONSTRAINT cotizaciones_recursos_cotizacion_id_fkey FOREIGN KEY (cotizacion_id) REFERENCES cotizaciones(id) ON UPDATE CASCADE;


--
-- TOC entry 2230 (class 2606 OID 286524)
-- Dependencies: 217 215 2191 2324
-- Name: cotizaciones_recursos_recurso_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY cotizaciones_recursos
    ADD CONSTRAINT cotizaciones_recursos_recurso_id_fkey FOREIGN KEY (recurso_id) REFERENCES recursos(id) ON UPDATE CASCADE;


--
-- TOC entry 2205 (class 2606 OID 284134)
-- Dependencies: 172 178 2150 2324
-- Name: facturas_compromisos_pagos_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compromisos_pagos
    ADD CONSTRAINT facturas_compromisos_pagos_fk FOREIGN KEY (factura_id) REFERENCES facturas(id);


--
-- TOC entry 2222 (class 2606 OID 284139)
-- Dependencies: 195 178 2150 2324
-- Name: facturas_productos_factura_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_facturas
    ADD CONSTRAINT facturas_productos_factura_fk FOREIGN KEY (factura_id) REFERENCES facturas(id);


--
-- TOC entry 2206 (class 2606 OID 284144)
-- Dependencies: 172 180 2152 2324
-- Name: frecuencias_pagos_compromisos_pagos_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY compromisos_pagos
    ADD CONSTRAINT frecuencias_pagos_compromisos_pagos_fk FOREIGN KEY (frecuencia_pago_id) REFERENCES frecuencias_pagos(id);


--
-- TOC entry 2211 (class 2606 OID 284149)
-- Dependencies: 183 182 2154 2324
-- Name: minutas_minutas_acuerdos_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY minutas_detalles
    ADD CONSTRAINT minutas_minutas_acuerdos_fk FOREIGN KEY (minuta_id) REFERENCES minutas(id) ON UPDATE CASCADE;


--
-- TOC entry 2214 (class 2606 OID 284154)
-- Dependencies: 188 182 2154 2324
-- Name: minutas_participantes_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participantes
    ADD CONSTRAINT minutas_participantes_fk FOREIGN KEY (minuta_id) REFERENCES minutas(id);


--
-- TOC entry 2218 (class 2606 OID 285849)
-- Dependencies: 190 211 2187 2324
-- Name: personas_contactos_cargo_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personas_contactos
    ADD CONSTRAINT personas_contactos_cargo_id_fkey FOREIGN KEY (cargo_id) REFERENCES cargos(id) ON UPDATE CASCADE;


--
-- TOC entry 2217 (class 2606 OID 285841)
-- Dependencies: 190 213 2189 2324
-- Name: personas_contactos_horario_id_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY personas_contactos
    ADD CONSTRAINT personas_contactos_horario_id_fkey FOREIGN KEY (horario_id) REFERENCES horarios(id) ON UPDATE CASCADE;


--
-- TOC entry 2215 (class 2606 OID 284159)
-- Dependencies: 188 190 2162 2324
-- Name: personas_contactos_participantes_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY participantes
    ADD CONSTRAINT personas_contactos_participantes_fk FOREIGN KEY (persona_contacto_id) REFERENCES personas_contactos(id);


--
-- TOC entry 2224 (class 2606 OID 284164)
-- Dependencies: 197 195 2168 2324
-- Name: productos_factura_productos_facturas_estados_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_facturas_estados
    ADD CONSTRAINT productos_factura_productos_facturas_estados_fk FOREIGN KEY (id) REFERENCES productos_facturas(id);


--
-- TOC entry 2212 (class 2606 OID 284169)
-- Dependencies: 183 192 2164 2324
-- Name: productos_minutas_acuerdos_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY minutas_detalles
    ADD CONSTRAINT productos_minutas_acuerdos_fk FOREIGN KEY (producto_id) REFERENCES productos(id) ON UPDATE CASCADE;


--
-- TOC entry 2221 (class 2606 OID 284174)
-- Dependencies: 193 192 2164 2324
-- Name: productos_productos_cotizaciones_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_cotizaciones
    ADD CONSTRAINT productos_productos_cotizaciones_fk FOREIGN KEY (producto_id) REFERENCES productos(id) ON UPDATE CASCADE;


--
-- TOC entry 2223 (class 2606 OID 284179)
-- Dependencies: 195 192 2164 2324
-- Name: productos_productos_factura_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_facturas
    ADD CONSTRAINT productos_productos_factura_fk FOREIGN KEY (producto_id) REFERENCES productos(id);


--
-- TOC entry 2228 (class 2606 OID 285292)
-- Dependencies: 209 192 2164 2324
-- Name: productos_productos_historico_precio_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos_historico_precio
    ADD CONSTRAINT productos_productos_historico_precio_fk FOREIGN KEY (producto_id) REFERENCES productos(id) ON UPDATE CASCADE;


--
-- TOC entry 2204 (class 2606 OID 284184)
-- Dependencies: 170 200 2172 2324
-- Name: sectores_clientes_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY clientes
    ADD CONSTRAINT sectores_clientes_fk FOREIGN KEY (sector_id) REFERENCES sectores(id);


--
-- TOC entry 2219 (class 2606 OID 284189)
-- Dependencies: 192 202 2174 2324
-- Name: tipos_productos_productos_fk; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY productos
    ADD CONSTRAINT tipos_productos_productos_fk FOREIGN KEY (tipo_producto_id) REFERENCES tipos_productos(id);


--
-- TOC entry 2329 (class 0 OID 0)
-- Dependencies: 7
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


SET search_path = acl, pg_catalog;

--
-- TOC entry 2336 (class 0 OID 0)
-- Dependencies: 207
-- Name: group_operation; Type: ACL; Schema: acl; Owner: postgres
--

REVOKE ALL ON TABLE group_operation FROM PUBLIC;
REVOKE ALL ON TABLE group_operation FROM postgres;
GRANT ALL ON TABLE group_operation TO postgres;


--
-- TOC entry 2355 (class 0 OID 0)
-- Dependencies: 206
-- Name: operation; Type: ACL; Schema: acl; Owner: postgres
--

REVOKE ALL ON TABLE operation FROM PUBLIC;
REVOKE ALL ON TABLE operation FROM postgres;
GRANT ALL ON TABLE operation TO postgres;


-- Completed on 2015-03-21 10:10:41 VET

--
-- PostgreSQL database dump complete
--

