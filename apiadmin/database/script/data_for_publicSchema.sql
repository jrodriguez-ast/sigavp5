--
-- PostgreSQL database dump
--

--
-- Data for Name: cargos; Type: TABLE DATA; Schema: public; Owner: astcom_cotiz
--

INSERT INTO cargos (id, descripcion, usuario_id, modificado_en) VALUES
(1	,'Abogado',1,'2015-01-26 15:16:26.890392'),
(2	,'Abogado especialista',1,'2015-01-26 16:23:24.193713'),
(3	,'Abogado especialista sectorial',1,'2015-01-28 09:29:41.480142'),
(4	,'Administrador',1,'2015-01-28 09:30:18.571092'),
(5	,'Administrador de Redes y Recursos de Informática',1,'2015-01-28 09:31:17.483311'),
(6	,'Almacenista',1,'2015-01-28 09:31:50.401634'),
(7	,'Analista',1,'2015-01-28 09:32:56.461686'),
(8  ,'Analista de Presupuesto',1,'2015-01-28 09:33:27.771799'),
(9	,'Analista de Nómina',1,'2015-01-28 09:36:41.168431'),
(10,'Analista de organización y sistemas',1,'2015-01-28 09:37:41.062351'),
(11,	'Analista de Recursos Humanos',1,'2015-01-28 09:39:21.135171'),
(12,	'Analista Financiero',1,'2015-01-28 09:40:01.541824'),
(13,	'Archivista',1,'2015-01-28 09:43:06.001626'),
(14,	'Arquitecto',1,'2015-01-28 09:43:35.971653'),
(15,	'Asistente Administrativo',1,'2015-01-28 09:44:10.164484'),
(16,	'Asistente de Contabilidad',1,'2015-01-28 09:45:01.104092'),
(17,	'Asistente de Costos de Obras',1,'2015-01-28 09:45:21.411246'),
(18,	'Asistente de Farmacia',1,'2015-01-28 09:45:21.81401'),
(19,	'Asistente de Laboratorio',1,'2015-01-28 09:50:25.511268'),
(20,	'Asistente de Nómina',1,'2015-01-28 09:52:34.283617'),
(21,	'Asistente de Preescolar',1,'2015-01-28 09:54:26.066822'),
(22,	'Asistente de Presupuesto',1,'2015-01-28 09:58:54.145712'),
(23,	'Asistente de Publicidad y Mercadeo',1,'2015-01-28 10:00:37.955786'),
(24,	'Asistente de Recursos Humanos',1,'2015-01-28 10:04:48.62564'),
(25,	'Asistente Financiero',1,'2015-01-28 10:07:18.440188'),
(26,	'Auditor',1,'2015-01-28 10:07:27.015206'),
(27,	'Auxiliar de Archivo',1,'2015-01-28 10:08:06.577153'),
(28,	'Auxiliar de Laboratorio',1,'2015-01-28 10:08:33.257916'),
(29,	'Bioanalista',1,'2015-01-28 10:09:48.727798'),
(30,	'Cajero',1,'2015-01-28 10:10:17.311563'),
(31,	'Contabilista',1,'2015-01-28 10:12:50.894813'),
(32,	'Contador',1,'2015-01-28 10:13:06.096414'),
(33,	'Coordinador',1,'2015-01-28 10:15:00.915254'),
(34,	'Director Educativo',1,'2015-01-28 10:19:55.747999'),
(35,	'Diseñador Gráfico',1,'2015-01-28 10:21:47.746306'),
(36,	'Docente',1,'2015-01-28 10:26:30.352268'),
(37,	'Enfermera',1,'2015-01-28 10:30:16.439302'),
(38,	'Enfermera Instrumentista',1,'2015-01-28 10:31:00.580134'),
(39,	'Entrenador Deportivo',1,'2015-01-28 10:39:21.435957'),
(40,	'Especialista de Sistemas de Informática',1,'2015-01-28 10:39:45.669876'),
(41,	'Especialista en Información',10,'2015-01-28 10:40:50.57795'),
(42,	'Facilitador',1,'2015-01-28 10:51:32.605342'),
(43,	'Farmacéutico',1,'2015-01-28 10:53:31.486072'),
(44,	'Fisioterapeuta',1,'2015-01-28 10:55:29.505208'),
(45,	'Ingeniero',1,'2015-01-28 10:58:23.782338'),
(46,	'Inspector',1,'2015-01-28 10:58:48.351093'),
(47,	'Inspector de Obras',1,'2015-01-28 10:59:02.803786'),
(48,	'Maquetista',1,'2015-01-28 11:09:33.467553'),
(49,	'Mecánico Dental',1,'2015-01-28 11:12:13.723995'),
(50,	'Médico Especialista',1,'2015-01-28 11:15:17.26177'),
(51,	'Médico General',1,'2015-01-28 11:15:39.383202'),
(52,	'Odontólogo',1,'2015-01-28 11:16:33.141591'),
(53,	'Oficinista',1,'2015-01-28 11:17:26.54427'),
(54,	'Periodista',1,'2015-01-28 11:18:09.596655'),
(55,	'Programador de Sistemas',1,'2015-01-28 11:18:43.840682'),
(56,	'Promotor',1,'2015-01-28 11:19:39.15341'),
(57,	'Psicólogo',1,'2015-01-28 11:22:43.260568'),
(58,	'Psicopedagogo',1,'2015-01-28 11:23:05.081521'),
(59,	'Recepcionista',1,'2015-01-28 11:23:45.704537'),
(60,	'Secretaria',1,'2015-01-28 11:24:49.060365'),
(61,	'Supervisor',1,'2015-01-28 11:28:02.139871'),
(62,	'Técnico',1,'2015-01-28 11:29:58.133721'),
(63,	'Trabajador Social',1,'2015-01-28 11:30:16.158172'),
(64,	'Traductor',1,'2015-01-28 11:32:06.640137');


--
-- Name: cargos_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astcom_cotiz
--

SELECT pg_catalog.setval('cargos_id_seq', 64, true);

--
-- Data for Name: sectores; Type: TABLE DATA; Schema: public; Owner: astcom_cotiz
--

INSERT INTO sectores (id, nombre, usuario_id, modificado_en) VALUES
(3,'Gastronomía y Turismo',1, '2015-01-06 11:57:52.75042'),
(6,'Gerencia',1,'2015-01-06 00:00:00'),
(7,'Famaceutico',1, '2015-01-26 14:15:11.032928'),
(8,'Administración',1, '2015-01-26 15:17:07.116292'),
(9,'Comunicación',1, '2015-01-26 16:30:23.743046'),
(10,'Comercio Exterior',1, '2015-01-28 09:03:53.626997'),
(11,'Diseño',1, '2015-01-28 09:06:40.577002'),
(12,'Educación',1, '2015-01-28 09:06:40.628053'),
(13,'Finanzas',1, '2015-01-28 09:06:40.873122'),
(14,'Ingeniería',1, '2015-01-28 09:09:25.722705'),
(15,'Legales',1, '2015-01-28 09:10:18.130441'),
(16,'Marketing',1, '2015-01-28 09:10:40.203486'),
(17,'Logística',1, '2015-01-28 09:11:03.038804'),
(18,'Medicina',1, '2015-01-28 09:11:24.557694'),
(19,'Minería',1, '2015-01-28 09:11:46.77804'),
(20,'Oficios',1, '2015-01-28 09:12:01.826325'),
(21,'Producción',1, '2015-01-28 09:12:14.772986'),
(22,'RRHH',1, '2015-01-28 09:12:27.259016'),
(23,'Tecnología',1, '2015-01-28 09:12:39.236037');

SELECT pg_catalog.setval('sectores_id_seq', 23, true);


--- Confiraciones
insert into configuraciones (id, clave, valor) values
(1,	'IVA',	12),
(2,	'dias_vigencia_estandar_cotizacion',	10),
(3,	'costo_hora_adicional',	10000);

--
-- Name: configuraciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astcom_cotiz
--

SELECT pg_catalog.setval('configuraciones_id_seq', 3, true);


--
-- Name: configuraciones_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astcom_cotiz
--

SELECT pg_catalog.setval('configuraciones_id_seq', 1, true);

--
-- Data for Name: horarios; Type: TABLE DATA; Schema: public; Owner: astcom_cotiz
--

INSERT INTO horarios (id, descripcion, usuario_id, modificado_en) VALUES
(1,	'Todo el día',1	,'2015-01-26 15:20:10.180474'),
(3,	'Toda la noche',1,'2015-01-28 13:32:05.061359'),
(5,	'Solo en la Mañana',1,'2015-01-28 13:42:38.238434'),
(7,	'Solo fines de Semana',1,'2015-01-28 13:54:06.562056'),
(4,	'Solo en la Tarde',1,'2015-01-28 13:33:03.278806'),
(2,	'Medio día',1,'2015-01-26 15:31:09.257665'),
(6,	'Horario de Oficina',1,'2015-01-28 13:45:09.291225'),
(8,	'De lunes a viernes',1,'2015-01-28 14:15:02.456523');


--
-- Name: horarios_id_seq; Type: SEQUENCE SET; Schema: public; Owner: astcom_cotiz
--

SELECT pg_catalog.setval('horarios_id_seq', 8, true);

--
-- Data for Name: tipos_productos; Type: TABLE DATA; Schema: public; Owner: astcom_cotiz
--

INSERT INTO tipos_productos (id, descripcion, usuario_id, modificado_en) VALUES
(1	,'Desarrollo',1	,'2015-01-12 09:40:48.125655'),
(2	,'Servicio',1,'2015-01-12 11:03:46.35334'),
(3	,'Otro',3,'2015-01-26 14:23:51.921607');