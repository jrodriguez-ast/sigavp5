-- Table: acl.groups

-- DROP TABLE acl.groups;

CREATE TABLE acl.groups
(
  id serial NOT NULL,
  name character varying(20) NOT NULL,
  description character varying(100) NOT NULL,
  CONSTRAINT groups_pkey PRIMARY KEY (id),
  CONSTRAINT check_id CHECK (id >= 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE acl.groups
  OWNER TO postgres;


-- Table: acl.operaciones

-- DROP TABLE acl.operaciones;

CREATE TABLE acl.operaciones
(
  id serial NOT NULL,
  url character varying(300) NOT NULL,
  nombre character varying NOT NULL,
  data character varying(300),
  value character varying,
  estatus bit(1) NOT NULL,
  CONSTRAINT id_operaciones PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE acl.operaciones
  OWNER TO postgres;

  -- Table: acl.operaciones_roles

-- DROP TABLE acl.operaciones_roles;

CREATE TABLE acl.operaciones_roles
(
  id serial NOT NULL,
  operacion_id integer,
  rol_id integer,
  CONSTRAINT id_operaciones_rol PRIMARY KEY (id),
  CONSTRAINT fk_operacion_oprol FOREIGN KEY (operacion_id)
      REFERENCES acl.operaciones (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT fk_rol_oprol FOREIGN KEY (rol_id)
      REFERENCES acl.groups (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
ALTER TABLE acl.operaciones_roles
  OWNER TO postgres;



  -- Table: acl.users

-- DROP TABLE acl.users;

CREATE TABLE acl.users
(
  id serial NOT NULL,
  ip_address character varying(15),
  username character varying(100) NOT NULL,
  password character varying(255) NOT NULL,
  salt character varying(255),
  email character varying(100) NOT NULL,
  activation_code character varying(40),
  forgotten_password_code character varying(40),
  forgotten_password_time integer,
  remember_code character varying(40),
  created_on integer NOT NULL,
  last_login integer,
  active integer,
  first_name character varying(50),
  last_name character varying(50),
  direccion character varying(300),
  phone character varying(20),
  cedula integer NOT NULL,
  rif character varying,
  i18n character varying(125),
  CONSTRAINT users_pkey PRIMARY KEY (id),
  CONSTRAINT check_active CHECK (active >= 0),
  CONSTRAINT check_id CHECK (id >= 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE acl.users
  OWNER TO postgres;


-- Table: acl.users_groups

-- DROP TABLE acl.users_groups;

CREATE TABLE acl.users_groups
(
  id serial NOT NULL,
  user_id integer NOT NULL,
  group_id integer NOT NULL,
  CONSTRAINT users_groups_pkey PRIMARY KEY (id),
  CONSTRAINT users_groups_group_id_fkey FOREIGN KEY (group_id)
      REFERENCES acl.groups (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT users_groups_user_id_fkey FOREIGN KEY (user_id)
      REFERENCES acl.users (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT uc_users_groups UNIQUE (user_id, group_id),
  CONSTRAINT users_groups_check_group_id CHECK (group_id >= 0),
  CONSTRAINT users_groups_check_id CHECK (id >= 0),
  CONSTRAINT users_groups_check_user_id CHECK (user_id >= 0)
)
WITH (
  OIDS=FALSE
);
ALTER TABLE acl.users_groups
  OWNER TO postgres;

-- Table: acl.operation

-- DROP TABLE acl.operation;

CREATE TABLE acl.operation
(
  id serial NOT NULL, -- Identificador del registro
  _name character varying(150) NOT NULL, -- Nombre visible de la operacion, se utiliza en la etiqueta del boton o en el tooltip del icono
  url text, -- URL con la accion de la operacion
  _order integer, -- Numero que define el orden de las operaciones
  visible bit(1) DEFAULT B'1'::"bit", -- Define si la operacion es visible o no
  icon character varying(50) DEFAULT NULL::character varying, -- Ruta del icono de la operacion
  help text, -- Ayuda del campo, es usada en la documentacion
  tooltip text, -- Ayuda del campo, es usada en el tooltip
  id_operation integer, -- Identificador de la operacion|
  chk_render_on character varying(25) NOT NULL DEFAULT 'sidebar'::character varying, -- Identificador de la categoria que define el lugar donde se muestra la operacion
  chk_visual_type character varying(25) NOT NULL DEFAULT 'link'::character varying, -- Identificador de la categoria que define el formato visual como se mostrara la operacion
  chk_target_on character varying(25) NOT NULL DEFAULT 'content'::character varying, -- Indica donde se abrira la operacion luego de hacer click, si en window o content
  descripcion character varying, -- contiene la descripcion de los procesos realizados por la operacion
  imagen character varying,
  CONSTRAINT pk_operation PRIMARY KEY (id),
  CONSTRAINT fk_ope_operation FOREIGN KEY (id_operation)
      REFERENCES acl.operation (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT unique_url UNIQUE (url),
  CONSTRAINT chk_render_on CHECK (chk_render_on::text = ANY (ARRAY['grid'::character varying::text, 'sidebar'::character varying::text, 'topbar'::character varying::text, 'actionbar'::character varying::text])),
  CONSTRAINT chk_target_on CHECK (chk_target_on::text = ANY (ARRAY['content'::character varying::text, 'tab'::character varying::text, 'tab_content'::character varying::text, 'window'::character varying::text, 'window2'::character varying::text])),
  CONSTRAINT chk_visual_type CHECK (chk_visual_type::text = ANY (ARRAY['menu'::character varying::text, 'submenu'::character varying::text, 'button'::character varying::text, 'link'::character varying::text]))
)
WITH (
  OIDS=FALSE
);
ALTER TABLE acl.operation
  OWNER TO postgres;
GRANT ALL ON TABLE acl.operation TO postgres;
COMMENT ON TABLE acl.operation
  IS 'Entidad que contiene las operaciones del sistema';
COMMENT ON COLUMN acl.operation.id IS 'Identificador del registro';
COMMENT ON COLUMN acl.operation._name IS 'Nombre visible de la operacion, se utiliza en la etiqueta del boton o en el tooltip del icono';
COMMENT ON COLUMN acl.operation.url IS 'URL con la accion de la operacion';
COMMENT ON COLUMN acl.operation._order IS 'Numero que define el orden de las operaciones';
COMMENT ON COLUMN acl.operation.visible IS 'Define si la operacion es visible o no';
COMMENT ON COLUMN acl.operation.icon IS 'Ruta del icono de la operacion';
COMMENT ON COLUMN acl.operation.help IS 'Ayuda del campo, es usada en la documentacion';
COMMENT ON COLUMN acl.operation.tooltip IS 'Ayuda del campo, es usada en el tooltip';
COMMENT ON COLUMN acl.operation.id_operation IS 'Identificador de la operacion|';
COMMENT ON COLUMN acl.operation.chk_render_on IS 'Identificador de la categoria que define el lugar donde se muestra la operacion';
COMMENT ON COLUMN acl.operation.chk_visual_type IS 'Identificador de la categoria que define el formato visual como se mostrara la operacion';
COMMENT ON COLUMN acl.operation.chk_target_on IS 'Indica donde se abrira la operacion luego de hacer click, si en window o content';
COMMENT ON COLUMN acl.operation.descripcion IS 'contiene la descripcion de los procesos realizados por la operacion';


-- Table: acl.group_operation

-- DROP TABLE acl.group_operation;

CREATE TABLE acl.group_operation
(
  _date date DEFAULT now(), -- Fecha de creacion de la asignacion de la operacion al grupo
  id_operation integer NOT NULL, -- Identificador de la operacion
  id_group integer NOT NULL, -- Identificador del grupo
  CONSTRAINT pk_group_operation PRIMARY KEY (id_operation, id_group),
  CONSTRAINT fk_gro_ope_group FOREIGN KEY (id_group)
      REFERENCES acl.groups (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT,
  CONSTRAINT fk_gro_ope_operation FOREIGN KEY (id_operation)
      REFERENCES acl.operation (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE RESTRICT
)
WITH (
  OIDS=FALSE
);
ALTER TABLE acl.group_operation
  OWNER TO postgres;
GRANT ALL ON TABLE acl.group_operation TO postgres;
COMMENT ON TABLE acl.group_operation
  IS 'Entidad que contiene las operaciones asignadas a los grupos de usuarios del sistema';
COMMENT ON COLUMN acl.group_operation._date IS 'Fecha de creacion de la asignacion de la operacion al grupo';
COMMENT ON COLUMN acl.group_operation.id_operation IS 'Identificador de la operacion';
COMMENT ON COLUMN acl.group_operation.id_group IS 'Identificador del grupo';
