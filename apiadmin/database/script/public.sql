
CREATE SEQUENCE public.auditorias_id_seq;

CREATE TABLE public.auditorias (
                id INTEGER NOT NULL DEFAULT nextval('public.auditorias_id_seq'),
                usuario_id INTEGER NOT NULL,
                tabla VARCHAR NOT NULL,
                fecha TIMESTAMP DEFAULT NOW() NOT NULL,
                transaccion VARCHAR NOT NULL,
                datos_previos VARCHAR NOT NULL,
                datos_actuales VARCHAR NOT NULL,
                CONSTRAINT pk_auditorias_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.auditorias IS 'Almacena la trama de lo sucedido con los registros';
COMMENT ON COLUMN public.auditorias.id IS 'Identificador de la auditoría';
COMMENT ON COLUMN public.auditorias.usuario_id IS 'Usuario que realiza la transacción';
COMMENT ON COLUMN public.auditorias.tabla IS 'tabla sobre la cual se realiza una transacción';
COMMENT ON COLUMN public.auditorias.fecha IS 'Fecha y hora en la que se realiza la transacción';
COMMENT ON COLUMN public.auditorias.transaccion IS 'Indica qué transacción se realizó sobre el registro INSERT, UPDATE, DELETE';
COMMENT ON COLUMN public.auditorias.datos_previos IS 'Estructura de los datos antes de ser modificados. JSON con formato campo: valor';
COMMENT ON COLUMN public.auditorias.datos_actuales IS 'Estructura de los datos final luego de ser modificados. JSON con formato campo: valor';


ALTER SEQUENCE public.auditorias_id_seq OWNED BY public.auditorias.id;

CREATE SEQUENCE public.configuraciones_id_seq;

CREATE TABLE public.configuraciones (
                id INTEGER NOT NULL DEFAULT nextval('public.configuraciones_id_seq'),
                clave VARCHAR(50) NOT NULL,
                valor VARCHAR(50) NOT NULL,
                CONSTRAINT pk_configuraciones_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.configuraciones IS 'Configuracion del sistema';
COMMENT ON COLUMN public.configuraciones.id IS 'Identificador de la configuración';
COMMENT ON COLUMN public.configuraciones.clave IS 'Nombre o clave que representa la configuración';
COMMENT ON COLUMN public.configuraciones.valor IS 'Valor asignado a la configuración';


ALTER SEQUENCE public.configuraciones_id_seq OWNED BY public.configuraciones.id;

CREATE SEQUENCE public.categorias_detalles_minutas_id_seq;

CREATE TABLE public.categorias_detalles_minutas (
                id INTEGER NOT NULL DEFAULT nextval('public.categorias_detalles_minutas_id_seq'),
                descripcion VARCHAR(50) NOT NULL,
                CONSTRAINT pk_categorias_detalles_minutas_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.categorias_detalles_minutas IS 'Categorías de los ítems detallados en la minuta';
COMMENT ON COLUMN public.categorias_detalles_minutas.id IS 'Identificador de la categoría';
COMMENT ON COLUMN public.categorias_detalles_minutas.descripcion IS 'Descripcion de la categoría del item de la minuta';


ALTER SEQUENCE public.categorias_detalles_minutas_id_seq OWNED BY public.categorias_detalles_minutas.id;

CREATE SEQUENCE public.tipos_productos_id_seq;

CREATE TABLE public.tipos_productos (
                id INTEGER NOT NULL DEFAULT nextval('public.tipos_productos_id_seq'),
                descripcion VARCHAR(50) NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_tipos_productos_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.tipos_productos IS 'Almacena la tipificación de los productos';
COMMENT ON COLUMN public.tipos_productos.id IS 'Identificador del tipo de producto';
COMMENT ON COLUMN public.tipos_productos.descripcion IS 'Descripción del tipo de producto';
COMMENT ON COLUMN public.tipos_productos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.tipos_productos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.tipos_productos_id_seq OWNED BY public.tipos_productos.id;

CREATE SEQUENCE public.sectores_id_seq;

CREATE TABLE public.sectores (
                id INTEGER NOT NULL DEFAULT nextval('public.sectores_id_seq'),
                nombre VARCHAR(50) NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_sectores_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.sectores IS 'Sectores para clasificar el tipo de actividad que realiza el cliente';
COMMENT ON COLUMN public.sectores.id IS 'Identificador del sector';
COMMENT ON COLUMN public.sectores.nombre IS 'Nombre del sector';
COMMENT ON COLUMN public.sectores.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.sectores.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.sectores_id_seq OWNED BY public.sectores.id;

CREATE SEQUENCE public.frecuencias_pagos_id_seq;

CREATE TABLE public.frecuencias_pagos (
                id INTEGER NOT NULL DEFAULT nextval('public.frecuencias_pagos_id_seq'),
                dias INTEGER NOT NULL,
                descripcion VARCHAR(50) NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_frecuencias_pagos_id PRIMARY KEY (id)
);
COMMENT ON COLUMN public.frecuencias_pagos.id IS 'Identificador de la frecuencia de pago';
COMMENT ON COLUMN public.frecuencias_pagos.dias IS 'ndica la frecuencia en días con la que realizará los pagos, 7 semanal, 15 quincenal, 30 mensual, 90 trimestral. Se representa por un número de días.';
COMMENT ON COLUMN public.frecuencias_pagos.descripcion IS 'Nombre descriptivo para la frecuencia de pago';
COMMENT ON COLUMN public.frecuencias_pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.frecuencias_pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.frecuencias_pagos_id_seq OWNED BY public.frecuencias_pagos.id;

CREATE SEQUENCE public.productos_id_seq;

CREATE TABLE public.productos (
                id INTEGER NOT NULL DEFAULT nextval('public.productos_id_seq'),
                codigo VARCHAR(25) NOT NULL,
                nombre VARCHAR(255) NOT NULL,
                descripcion VARCHAR(1024) NOT NULL,
                precio REAL NOT NULL,
                tipo_producto_id INTEGER NOT NULL,
                requisitos VARCHAR NOT NULL,
                activo BOOLEAN DEFAULT TRUE NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_productos_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.productos IS 'Almacena los productos a cotizar';
COMMENT ON COLUMN public.productos.id IS 'Identificador del producto';
COMMENT ON COLUMN public.productos.codigo IS 'Código del producto';
COMMENT ON COLUMN public.productos.nombre IS 'Nombre del producto';
COMMENT ON COLUMN public.productos.descripcion IS 'Descripción del producto';
COMMENT ON COLUMN public.productos.precio IS 'Precio del producto';
COMMENT ON COLUMN public.productos.tipo_producto_id IS 'Identificador del tipo de producto';
COMMENT ON COLUMN public.productos.requisitos IS 'Requisitos para la adquisición del producto';
COMMENT ON COLUMN public.productos.activo IS 'Indica si el producto se encuentra activo o no';
COMMENT ON COLUMN public.productos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.productos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.productos_id_seq OWNED BY public.productos.id;

CREATE SEQUENCE public.clientes_id_seq;

CREATE TABLE public.clientes (
                id INTEGER NOT NULL DEFAULT nextval('public.clientes_id_seq'),
                nombre VARCHAR(100) NOT NULL,
                tipo CHAR NOT NULL,
                dni VARCHAR(20) NOT NULL,
                razon_social VARCHAR(255) NOT NULL,
                direccion_fiscal VARCHAR(1024) NOT NULL,
                direccion_correspondencia VARCHAR(1024),
                es_direccion_fiscal BOOLEAN NOT NULL,
                telefonos VARCHAR(50) NOT NULL,
                fax VARCHAR(50) NOT NULL,
                correo_electronico VARCHAR(255) NOT NULL,
                sector_id INTEGER NOT NULL,
                sitio_web VARCHAR(255),
                es_prospecto BOOLEAN DEFAULT TRUE NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_clientes_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.clientes IS 'Tabla para almacenar los datos de clientes';
COMMENT ON COLUMN public.clientes.id IS 'Identificador del cliente';
COMMENT ON COLUMN public.clientes.nombre IS 'Nombre del cliente';
COMMENT ON COLUMN public.clientes.tipo IS 'Indica el tipo de cliente, almacena 
V, E, J, G';
COMMENT ON COLUMN public.clientes.dni IS 'DNI Identificación del cliente';
COMMENT ON COLUMN public.clientes.razon_social IS 'Razón Social';
COMMENT ON COLUMN public.clientes.direccion_fiscal IS 'Dirección Fiscal';
COMMENT ON COLUMN public.clientes.direccion_correspondencia IS 'Dirección de correspondencia';
COMMENT ON COLUMN public.clientes.es_direccion_fiscal IS 'Indica si la dirección de correspondencia es igual a la dirección fiscal';
COMMENT ON COLUMN public.clientes.telefonos IS 'Teléfonos del cliente';
COMMENT ON COLUMN public.clientes.fax IS 'Fax del cliente';
COMMENT ON COLUMN public.clientes.correo_electronico IS 'Correo Electrónico del cliente';
COMMENT ON COLUMN public.clientes.sector_id IS 'Sector económico del cliente';
COMMENT ON COLUMN public.clientes.sitio_web IS 'Sitio Web del cliente';
COMMENT ON COLUMN public.clientes.es_prospecto IS 'Indica si un cliente es prospecto. Deja de ser prospecto al adquirir un producto';
COMMENT ON COLUMN public.clientes.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.clientes.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.clientes_id_seq OWNED BY public.clientes.id;

CREATE SEQUENCE public.personas_contactos_id_seq;

CREATE TABLE public.personas_contactos (
                id INTEGER NOT NULL DEFAULT nextval('public.personas_contactos_id_seq'),
                nombre VARCHAR(50) NOT NULL,
                apellido VARCHAR(20),
                cedula VARCHAR(15) NOT NULL,
                direccion VARCHAR(1024) NOT NULL,
                telefono_1 VARCHAR(20) NOT NULL,
                telefono_2 VARCHAR NOT NULL,
                correo_electronico_1 VARCHAR NOT NULL,
                correo_electronico_2 VARCHAR NOT NULL,
                horario_contacto VARCHAR NOT NULL,
                cliente_id INTEGER NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_personas_contactos_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.personas_contactos IS 'Personas de contacto';
COMMENT ON COLUMN public.personas_contactos.id IS 'Identificador del contacto';
COMMENT ON COLUMN public.personas_contactos.nombre IS 'Nombre de la persona de contacto';
COMMENT ON COLUMN public.personas_contactos.apellido IS 'Apellido de la persona de contacto';
COMMENT ON COLUMN public.personas_contactos.cedula IS 'Cédula del contacto';
COMMENT ON COLUMN public.personas_contactos.direccion IS 'Dirección de la persona de contacto';
COMMENT ON COLUMN public.personas_contactos.telefono_1 IS 'Teléfono de contacto principal';
COMMENT ON COLUMN public.personas_contactos.telefono_2 IS 'Teléfono de contacto secundario';
COMMENT ON COLUMN public.personas_contactos.correo_electronico_1 IS 'Correo electrónico de contacto principal';
COMMENT ON COLUMN public.personas_contactos.correo_electronico_2 IS 'Correo electrónico de contacto secundario';
COMMENT ON COLUMN public.personas_contactos.horario_contacto IS 'Horario de contacto permitido por la persona';
COMMENT ON COLUMN public.personas_contactos.cliente_id IS 'Identificador del cliente al que pertenece el contacto';
COMMENT ON COLUMN public.personas_contactos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.personas_contactos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.personas_contactos_id_seq OWNED BY public.personas_contactos.id;

CREATE SEQUENCE public.facturas_id_seq;

CREATE TABLE public.facturas (
                id INTEGER NOT NULL DEFAULT nextval('public.facturas_id_seq'),
                cliente_id INTEGER NOT NULL,
                codigo VARCHAR NOT NULL,
                fecha_creacion TIMESTAMP NOT NULL,
                con_impuestos BOOLEAN NOT NULL,
                porcentaje_impuesto REAL NOT NULL,
                descuento REAL NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_facturas_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.facturas IS 'Almacena los datos generales de la factura';
COMMENT ON COLUMN public.facturas.id IS 'Identificador de la factura';
COMMENT ON COLUMN public.facturas.cliente_id IS 'Identificador del cliente';
COMMENT ON COLUMN public.facturas.codigo IS 'Código';
COMMENT ON COLUMN public.facturas.fecha_creacion IS 'Fecha de creación de la cotización';
COMMENT ON COLUMN public.facturas.con_impuestos IS 'Indica si la cotización incluye o no los impuestos como el IVA';
COMMENT ON COLUMN public.facturas.porcentaje_impuesto IS 'Indica el valor en porcentaje del impuesto a calcular';
COMMENT ON COLUMN public.facturas.descuento IS 'Descuento sobre la cotización que otorga el usuario que genera la cotización';
COMMENT ON COLUMN public.facturas.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.facturas.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.facturas_id_seq OWNED BY public.facturas.id;

CREATE SEQUENCE public.compromisos_pagos_id_seq;

CREATE TABLE public.compromisos_pagos (
                id INTEGER NOT NULL DEFAULT nextval('public.compromisos_pagos_id_seq'),
                codigo VARCHAR NOT NULL,
                factura_id INTEGER NOT NULL,
                cantidad_cuotas INTEGER NOT NULL,
                frecuencia_pago_id INTEGER NOT NULL,
                fecha_inicio TIMESTAMP NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_compromisos_pagos_id PRIMARY KEY (id)
);
COMMENT ON COLUMN public.compromisos_pagos.id IS 'Identificador del compromiso de pago';
COMMENT ON COLUMN public.compromisos_pagos.codigo IS 'Código';
COMMENT ON COLUMN public.compromisos_pagos.factura_id IS 'Identificador de la factura';
COMMENT ON COLUMN public.compromisos_pagos.cantidad_cuotas IS 'Cantidad cuotas en la que acuerda el cliente realizar el pago';
COMMENT ON COLUMN public.compromisos_pagos.frecuencia_pago_id IS 'Indica la frecuencia con la que realizará los pagos, 7 semanal, 15 quincenal, 30 mensual, 90 trimestral. Se representa por un número de días.';
COMMENT ON COLUMN public.compromisos_pagos.fecha_inicio IS 'Fecha en que acuerdan las partes inciar con los pagos.';
COMMENT ON COLUMN public.compromisos_pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.compromisos_pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.compromisos_pagos_id_seq OWNED BY public.compromisos_pagos.id;

CREATE SEQUENCE public.pagos_id_seq;

CREATE TABLE public.pagos (
                id INTEGER NOT NULL DEFAULT nextval('public.pagos_id_seq'),
                compromiso_pago_id INTEGER NOT NULL,
                fecha_pago TIMESTAMP NOT NULL,
                comprobante_pago VARCHAR(50) NOT NULL,
                fecha_inicia_periodo TIMESTAMP NOT NULL,
                fecha_fin_periodo TIMESTAMP NOT NULL,
                es_pago BOOLEAN DEFAULT FALSE NOT NULL,
                es_vencido BOOLEAN,
                fecha_vencimiento TIMESTAMP,
                fecha_notificacion_vencimiento TIMESTAMP,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_pagos_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.pagos IS 'Almacena los pagos sobre los compromisos adquiridos';
COMMENT ON COLUMN public.pagos.id IS 'Identificador del compromiso de pago';
COMMENT ON COLUMN public.pagos.compromiso_pago_id IS 'Identificador del compromiso de pago';
COMMENT ON COLUMN public.pagos.fecha_pago IS 'Fecha de pago';
COMMENT ON COLUMN public.pagos.comprobante_pago IS 'Código del comprobante del pago realizado';
COMMENT ON COLUMN public.pagos.fecha_inicia_periodo IS 'Fecha en la que inicia el período del pago';
COMMENT ON COLUMN public.pagos.fecha_fin_periodo IS 'Fecha en que finaliza el período del pago';
COMMENT ON COLUMN public.pagos.es_pago IS 'Indica si el pago fue realizado.';
COMMENT ON COLUMN public.pagos.es_vencido IS 'Indica si el pago se encuentra vencido';
COMMENT ON COLUMN public.pagos.fecha_vencimiento IS 'Indica la fecha en que el pago venció,';
COMMENT ON COLUMN public.pagos.fecha_notificacion_vencimiento IS 'Fecha en que se envió la notificación de vencimiento de acuerdo de pago';
COMMENT ON COLUMN public.pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.pagos_id_seq OWNED BY public.pagos.id;

CREATE SEQUENCE public.productos_factura_id_seq;

CREATE TABLE public.productos_factura (
                id INTEGER NOT NULL DEFAULT nextval('public.productos_factura_id_seq'),
                factura_id INTEGER NOT NULL,
                producto_id INTEGER NOT NULL,
                fecha_adquisicion TIMESTAMP NOT NULL,
                fecha_activacion TIMESTAMP,
                fecha_vencimiento TIMESTAMP NOT NULL,
                precio REAL NOT NULL,
                descuento REAL NOT NULL,
                activo BOOLEAN NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_productos_factura_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.productos_factura IS 'Productos facturados';
COMMENT ON COLUMN public.productos_factura.id IS 'Identificador del registro';
COMMENT ON COLUMN public.productos_factura.factura_id IS 'Identificador de la factura';
COMMENT ON COLUMN public.productos_factura.fecha_adquisicion IS 'Fecha en la que el producto fue adquirido';
COMMENT ON COLUMN public.productos_factura.fecha_activacion IS 'Fecha en la que se activa el producto y a partir del cual se contabiliza el tiempo en que estará activo.';
COMMENT ON COLUMN public.productos_factura.fecha_vencimiento IS 'Fecha en la que vence el producto adquirido según el contrato';
COMMENT ON COLUMN public.productos_factura.precio IS 'Precio extablecido para el producto en el momento en que se crea la cotización';
COMMENT ON COLUMN public.productos_factura.descuento IS 'Porcentaje de descuento sobre el precio del producto, otorgado por el iusuario que genera la cotización';
COMMENT ON COLUMN public.productos_factura.activo IS 'Indica si el producto se encuentra activo o no';
COMMENT ON COLUMN public.productos_factura.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.productos_factura.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.productos_factura_id_seq OWNED BY public.productos_factura.id;

CREATE SEQUENCE public.productos_facturas_estados_id_seq;

CREATE TABLE public.productos_facturas_estados (
                id INTEGER NOT NULL DEFAULT nextval('public.productos_facturas_estados_id_seq'),
                estado VARCHAR(10) NOT NULL,
                producto_factura_id INTEGER NOT NULL,
                cliente_producto_id INTEGER NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_productos_facturas_estados_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.productos_facturas_estados IS 'Almacena el histórico de estados que tiene un producto facturado para un cliente';
COMMENT ON COLUMN public.productos_facturas_estados.id IS 'Identificador del estado';
COMMENT ON COLUMN public.productos_facturas_estados.estado IS 'Estado en que se encuetra el producto con respecto al cliente. Se registra un histórico y por aplicación se dará siempre el último estado.';
COMMENT ON COLUMN public.productos_facturas_estados.cliente_producto_id IS 'Identificador de la relación clientes_productos';
COMMENT ON COLUMN public.productos_facturas_estados.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.productos_facturas_estados.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.productos_facturas_estados_id_seq OWNED BY public.productos_facturas_estados.id;

CREATE SEQUENCE public.minutas_id_seq;

CREATE TABLE public.minutas (
                id INTEGER NOT NULL DEFAULT nextval('public.minutas_id_seq'),
                cliente_id INTEGER NOT NULL,
                descripcion VARCHAR NOT NULL,
                fecha_creacion TIMESTAMP NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_minutas_id PRIMARY KEY (id)
);
COMMENT ON COLUMN public.minutas.id IS 'Identificador del la minuta';
COMMENT ON COLUMN public.minutas.cliente_id IS 'Identificador del cliente';
COMMENT ON COLUMN public.minutas.descripcion IS 'Descripción de la minuta, puede almacenar puntos a tratar';
COMMENT ON COLUMN public.minutas.fecha_creacion IS 'Fecha de creación de la cotización';
COMMENT ON COLUMN public.minutas.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.minutas.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.minutas_id_seq OWNED BY public.minutas.id;

CREATE SEQUENCE public.participantes_id_seq;

CREATE TABLE public.participantes (
                id INTEGER NOT NULL DEFAULT nextval('public.participantes_id_seq'),
                persona_contacto_id INTEGER NOT NULL,
                minuta_id INTEGER NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_participantes_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.participantes IS 'Participantes de la reunión registrada en minuta';
COMMENT ON COLUMN public.participantes.id IS 'Identificador del participante';
COMMENT ON COLUMN public.participantes.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.participantes.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.participantes_id_seq OWNED BY public.participantes.id;

CREATE SEQUENCE public.minutas_detalles_id_seq;

CREATE TABLE public.minutas_detalles (
                id INTEGER NOT NULL DEFAULT nextval('public.minutas_detalles_id_seq'),
                minuta_id INTEGER NOT NULL,
                acuerdo VARCHAR NOT NULL,
                producto_id INTEGER NOT NULL,
                categoria_id INTEGER NOT NULL,
                fecha_respuesta TIMESTAMP NOT NULL,
                fecha_notificacion TIMESTAMP,
                administrativo BOOLEAN NOT NULL,
                tecnico BOOLEAN NOT NULL,
                realizado BOOLEAN NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_minutas_detalles_id PRIMARY KEY (id)
);
COMMENT ON TABLE public.minutas_detalles IS 'Tabla donde se crean los ítems de la reunión';
COMMENT ON COLUMN public.minutas_detalles.id IS 'Identificador del ítem de acuerdos en la reunión';
COMMENT ON COLUMN public.minutas_detalles.minuta_id IS 'Identificador de la minuta';
COMMENT ON COLUMN public.minutas_detalles.acuerdo IS 'Descripción del acuerdo establacido en la reunión';
COMMENT ON COLUMN public.minutas_detalles.producto_id IS 'Identificador del producto al que se llegó adquirir en el cuerdo';
COMMENT ON COLUMN public.minutas_detalles.categoria_id IS 'Categoría del ítem de la minuta';
COMMENT ON COLUMN public.minutas_detalles.fecha_respuesta IS 'Fecha en que se dará respuesta sobre el acuerdo';
COMMENT ON COLUMN public.minutas_detalles.fecha_notificacion IS 'Fecha en que se envió la notificación de acuerdo por vencer';
COMMENT ON COLUMN public.minutas_detalles.administrativo IS 'Indica si el ítem de es de tipo administrativo';
COMMENT ON COLUMN public.minutas_detalles.tecnico IS 'Indica si el item es de tipo técnico';
COMMENT ON COLUMN public.minutas_detalles.realizado IS 'Indica si el ítem levantado en la minuta se realizó o no.';
COMMENT ON COLUMN public.minutas_detalles.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.minutas_detalles.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.minutas_detalles_id_seq OWNED BY public.minutas_detalles.id;

CREATE SEQUENCE public.cotizaciones_id_seq;

CREATE TABLE public.cotizaciones (
                id INTEGER NOT NULL DEFAULT nextval('public.cotizaciones_id_seq'),
                codigo VARCHAR NOT NULL,
                cliente_id INTEGER NOT NULL,
                fecha_creacion TIMESTAMP NOT NULL,
                fecha_expiracion TIMESTAMP NOT NULL,
                descuento REAL NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_cotizaciones_id PRIMARY KEY (id)
);
COMMENT ON COLUMN public.cotizaciones.id IS 'Identificador de la cotización';
COMMENT ON COLUMN public.cotizaciones.codigo IS 'Código';
COMMENT ON COLUMN public.cotizaciones.cliente_id IS 'Identificador del cliente';
COMMENT ON COLUMN public.cotizaciones.fecha_creacion IS 'Fecha de creación de la cotización';
COMMENT ON COLUMN public.cotizaciones.fecha_expiracion IS 'Fecha en que expira la cotización';
COMMENT ON COLUMN public.cotizaciones.descuento IS 'Descuento sobre la cotización que otorga el usuario que genera la cotización';
COMMENT ON COLUMN public.cotizaciones.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.cotizaciones.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.cotizaciones_id_seq OWNED BY public.cotizaciones.id;

CREATE SEQUENCE public.productos_cotizaciones_id_seq;

CREATE TABLE public.productos_cotizaciones (
                id INTEGER NOT NULL DEFAULT nextval('public.productos_cotizaciones_id_seq'),
                cotizacion_id INTEGER NOT NULL,
                producto_id INTEGER NOT NULL,
                precio REAL NOT NULL,
                descuento REAL NOT NULL,
                usuario_id INTEGER NOT NULL,
                modificado_en TIMESTAMP NOT NULL,
                CONSTRAINT pk_productos_cotizaciones_id PRIMARY KEY (id)
);
COMMENT ON COLUMN public.productos_cotizaciones.id IS 'Identificador del ítem';
COMMENT ON COLUMN public.productos_cotizaciones.cotizacion_id IS 'Identificador de la cotización, clave for{anea proveniente de la tabla cotizaciones';
COMMENT ON COLUMN public.productos_cotizaciones.producto_id IS 'Identificador del producto, campo foráneo de la tabla productos';
COMMENT ON COLUMN public.productos_cotizaciones.precio IS 'Precio extablecido para el producto en el momento en que se crea la cotización';
COMMENT ON COLUMN public.productos_cotizaciones.descuento IS 'Porcentaje de descuento sobre el precio del producto, otorgado por el iusuario que genera la cotización';
COMMENT ON COLUMN public.productos_cotizaciones.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN public.productos_cotizaciones.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


ALTER SEQUENCE public.productos_cotizaciones_id_seq OWNED BY public.productos_cotizaciones.id;

ALTER TABLE public.minutas_detalles ADD CONSTRAINT categorias_detalles_minutas_minutas_detalles_fk
FOREIGN KEY (categoria_id)
REFERENCES public.categorias_detalles_minutas (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.productos ADD CONSTRAINT tipos_productos_productos_fk
FOREIGN KEY (tipo_producto_id)
REFERENCES public.tipos_productos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.clientes ADD CONSTRAINT sectores_clientes_fk
FOREIGN KEY (sector_id)
REFERENCES public.sectores (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compromisos_pagos ADD CONSTRAINT frecuencias_pagos_compromisos_pagos_fk
FOREIGN KEY (frecuencia_pago_id)
REFERENCES public.frecuencias_pagos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.productos_cotizaciones ADD CONSTRAINT productos_productos_cotizaciones_fk
FOREIGN KEY (producto_id)
REFERENCES public.productos (id)
ON DELETE NO ACTION
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.minutas_detalles ADD CONSTRAINT productos_minutas_acuerdos_fk
FOREIGN KEY (producto_id)
REFERENCES public.productos (id)
ON DELETE NO ACTION
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.productos_factura ADD CONSTRAINT productos_productos_factura_fk
FOREIGN KEY (producto_id)
REFERENCES public.productos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.cotizaciones ADD CONSTRAINT clientes_cotizaciones_fk
FOREIGN KEY (cliente_id)
REFERENCES public.clientes (id)
ON DELETE NO ACTION
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.minutas ADD CONSTRAINT clientes_minutas_fk
FOREIGN KEY (cliente_id)
REFERENCES public.clientes (id)
ON DELETE NO ACTION
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.facturas ADD CONSTRAINT clientes_facturas_fk
FOREIGN KEY (cliente_id)
REFERENCES public.clientes (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.personas_contactos ADD CONSTRAINT clientes_personas_contactos_fk
FOREIGN KEY (cliente_id)
REFERENCES public.clientes (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.participantes ADD CONSTRAINT personas_contactos_participantes_fk
FOREIGN KEY (persona_contacto_id)
REFERENCES public.personas_contactos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.productos_factura ADD CONSTRAINT facturas_productos_factura_fk
FOREIGN KEY (factura_id)
REFERENCES public.facturas (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.compromisos_pagos ADD CONSTRAINT facturas_compromisos_pagos_fk
FOREIGN KEY (factura_id)
REFERENCES public.facturas (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.pagos ADD CONSTRAINT compromisos_pagos_pagos_realizados_fk
FOREIGN KEY (compromiso_pago_id)
REFERENCES public.compromisos_pagos (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.productos_facturas_estados ADD CONSTRAINT productos_factura_productos_facturas_estados_fk
FOREIGN KEY (id)
REFERENCES public.productos_factura (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.minutas_detalles ADD CONSTRAINT minutas_minutas_acuerdos_fk
FOREIGN KEY (minuta_id)
REFERENCES public.minutas (id)
ON DELETE NO ACTION
ON UPDATE CASCADE
NOT DEFERRABLE;

ALTER TABLE public.participantes ADD CONSTRAINT minutas_participantes_fk
FOREIGN KEY (minuta_id)
REFERENCES public.minutas (id)
ON DELETE NO ACTION
ON UPDATE NO ACTION
NOT DEFERRABLE;

ALTER TABLE public.productos_cotizaciones ADD CONSTRAINT cotizaciones_productos_cotizaciones_fk
FOREIGN KEY (cotizacion_id)
REFERENCES public.cotizaciones (id)
ON DELETE NO ACTION
ON UPDATE CASCADE
NOT DEFERRABLE;