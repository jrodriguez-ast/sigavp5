DROP TABLE auditorias;

CREATE TABLE auditorias
(
  id serial NOT NULL, -- Identificador de la auditoría
  usuario_id integer NOT NULL, -- Usuario que realiza la transacción
  esquema character varying, -- Nombre del esquema de bd al que pertence la tabla que es modificada
  tabla character varying, -- tabla sobre la cual se realiza una transacción
  registro_id bigint,
  fecha timestamp without time zone NOT NULL DEFAULT now(), -- Fecha y hora en la que se realiza la transacción
  transaccion character varying NOT NULL, -- Indica qué transacción se realizó sobre el registro INSERT, UPDATE, DELETE
  datos_previos character varying, -- Estructura de los datos antes de ser modificados. JSON con formato campo: valor
  datos_actuales character varying, -- Estructura de los datos final luego de ser modificados. JSON con formato campo: valor
  info_adicional character varying, -- Datos adicionales de conexión o cualquiera que sea necesario para la auditoría
  CONSTRAINT pk_auditorias_id PRIMARY KEY (id)
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE auditorias
  IS 'Almacena la trama de lo sucedido con los registros';
COMMENT ON COLUMN auditorias.id IS 'Identificador de la auditoría';
COMMENT ON COLUMN auditorias.usuario_id IS 'Usuario que realiza la transacción';
COMMENT ON COLUMN auditorias.esquema IS 'Nombre del esquema de bd al que pertence la tabla que es modificada';
COMMENT ON COLUMN auditorias.tabla IS 'tabla sobre la cual se realiza una transacción';
COMMENT ON COLUMN auditorias.fecha IS 'Fecha y hora en la que se realiza la transacción';
COMMENT ON COLUMN auditorias.transaccion IS 'Indica qué transacción se realizó sobre el registro INSERT, UPDATE, DELETE';
COMMENT ON COLUMN auditorias.datos_previos IS 'Estructura de los datos antes de ser modificados. JSON con formato campo: valor';
COMMENT ON COLUMN auditorias.datos_actuales IS 'Estructura de los datos final luego de ser modificados. JSON con formato campo: valor';
COMMENT ON COLUMN auditorias.info_adicional IS 'Datos adicionales de conexión o cualquiera que sea necesario para la auditoría';


CREATE INDEX ind_update_auditoria
   ON auditorias USING btree (
   esquema ASC NULLS LAST,
   tabla ASC NULLS LAST,
   registro_id ASC NULLS LAST,
   transaccion ASC NULLS LAST,
   info_adicional ASC NULLS FIRST);

-- Function: ft_auditoria()

-- DROP FUNCTION ft_auditoria();

-- Function: ft_auditoria()

-- DROP FUNCTION ft_auditoria();

CREATE OR REPLACE FUNCTION ft_auditoria()
  RETURNS trigger AS
$BODY$
DECLARE
	ri 			RECORD;
	t 			TEXT;
	sql_auditoria		TEXT;
	old_data		TEXT := '';
	new_data		TEXT := '';
	usuario_id		INTEGER;
	auditoria_id		BIGINT;
	registro_id     BIGINT;
BEGIN
	IF TG_OP = 'INSERT' OR TG_OP = 'UPDATE' THEN
		usuario_id := NEW.usuario_id;
		registro_id := NEW.id;
		FOR ri IN
			SELECT ordinal_position, column_name, data_type
			FROM information_schema.columns
			WHERE table_schema = quote_ident(TG_TABLE_SCHEMA)
			AND table_name = quote_ident(TG_TABLE_NAME)
			ORDER BY ordinal_position
		LOOP
			EXECUTE 'SELECT ($1).' || ri.column_name || '::text' INTO t USING NEW;
			IF (new_data != '') THEN
				new_data := new_data::text || ', ' ;
			END IF;
			new_data := new_data::text || '"'|| ri.column_name::text ||'": "'|| COALESCE(t::text, '') ||'"';
		END LOOP;
	END IF;

	IF TG_OP = 'DELETE' OR TG_OP = 'UPDATE' THEN
		IF TG_OP = 'DELETE' THEN
			usuario_id := 0;
			registro_id := OLD.id;
		END IF;

		FOR ri IN
			SELECT ordinal_position, column_name, data_type
			FROM information_schema.columns
			WHERE table_schema = quote_ident(TG_TABLE_SCHEMA)
			AND table_name = quote_ident(TG_TABLE_NAME)
			ORDER BY ordinal_position
		LOOP
			EXECUTE 'SELECT ($1).' || ri.column_name || '::text' INTO t USING OLD;
			IF old_data != '' THEN
				old_data := old_data::text || ', ' ;
			END IF;
			old_data := old_data::text || '"'|| ri.column_name::text ||'": "'|| COALESCE(t::text, '') ||'"';
		END LOOP;
	END IF;


	--raise notice 'new_data = %', new_data;
	new_data := '{'|| new_data ||'}';
	old_data := '{'|| old_data ||'}';

	-- Por problemas con framework Codeigniter, la tabla de auditoria no maneja una secuencia para el id
	-- ya que Codeigniter pide LASTVAL en el método insert_id y al disparar un insert dentro de un trigger
	-- se reemplaza el valor de LASTVAL y es erroneo el id dado por Codeigniter.

	-- En este punto se construye el id para la tabla auditorias
	-- SELECT COALESCE(MAX(id), 0) INTO auditoria_id FROM auditorias;

	INSERT INTO auditorias(registro_id, usuario_id, esquema, tabla, fecha, transaccion, datos_previos, datos_actuales)
		VALUES (registro_id, usuario_id, TG_TABLE_SCHEMA, TG_TABLE_NAME, NOW(), TG_OP, old_data, new_data);

    IF TG_OP = 'DELETE' THEN
      RETURN OLD;
    END IF;
    RETURN NEW;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON cargos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();

CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON clientes
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();

CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON compromisos_pagos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();

CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON cotizaciones
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();



CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON cotizaciones_formas_pagos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON cotizaciones_recursos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON facturas
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON formas_pagos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON frecuencias_pagos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON horarios
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON minutas
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON minutas_detalles
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON pagos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON participantes
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON personas_contactos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON productos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON productos_cotizaciones
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON productos_facturas
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON productos_facturas_estados
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON productos_historico_precio
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON recursos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON sectores
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON tipos_productos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();




ALTER TABLE acl.users ADD COLUMN usuario_id integer NOT NULL DEFAULT 0;
ALTER TABLE acl.users ADD COLUMN modificado_en timestamp without time zone NOT NULL DEFAULT now();

COMMENT ON COLUMN acl.users.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN acl.users.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON acl.users
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();



  ALTER TABLE acl.groups ADD COLUMN usuario_id integer NOT NULL DEFAULT 0;
ALTER TABLE acl.groups ADD COLUMN modificado_en timestamp without time zone NOT NULL DEFAULT now();

COMMENT ON COLUMN acl.groups.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN acl.groups.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON acl.groups
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();