ALTER TABLE cotizaciones_recursos
  ADD COLUMN orden smallint;
COMMENT ON COLUMN cotizaciones_recursos.orden IS 'Indica el orden en que se mostraran los recursos en la cotización';
