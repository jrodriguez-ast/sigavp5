ALTER TABLE cotizaciones_formas_pagos
  ADD COLUMN intervalo character varying;
COMMENT ON COLUMN cotizaciones_formas_pagos.intervalo IS 'Almacena condiciones de pagos por intervalos';
ALTER TABLE cotizaciones
   ADD COLUMN promocion boolean;
COMMENT ON COLUMN cotizaciones.promocion
  IS 'Indica que la cotización es una promoción que se está haciendo';

