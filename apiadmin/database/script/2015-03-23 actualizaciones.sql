UPDATE acl.operation
   SET _name='Grupos'
 WHERE id = 1404;

INSERT INTO acl.operation(
            id, _name, url, _order, visible, icon, help, tooltip, id_operation,
            chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen)
VALUES(
 '1410','Auditoria','system_audit',1,'1','glyphicon glyphicon-user','','',1400,'sidebar','submenu','content',
 'Consulta para auditar el sistema','');

INSERT INTO acl.operation(
  id, _name, url, _order, visible, icon, help, tooltip, id_operation,
  chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen)
VALUES ('1411','Ver','system_audit_view',1,'1','glyphicon glyphicon-zoom-in',NULL,NULL,1410,'grid','link','content',NULL,NULL);


INSERT INTO configuraciones(
            clave, valor)
    VALUES ('emails_cotizacion_aprobada', 'dserrano.ast@gmail.com, dserrano@ast.com.ve');