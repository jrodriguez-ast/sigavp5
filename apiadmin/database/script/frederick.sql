--CAMBIOS DEL 02-03-2015
INSERT INTO acl.operation(
            id, _name, url, _order, visible, icon, help, tooltip, id_operation,
            chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen)
    VALUES
('1404','Grupos deUsuarios','list_group',1,'1','glyphicon glyphicon-user','','',1400,'sidebar','submenu','content','Listado para la gestion de grupo de usuarios',''),
('1405','Nuevo Grupo','create_group',1,'1','glyphicon glyphicon-plus','','',1404,'actionbar','link','content','Permite la creacion de un nuevo grupo de usuario',''),
('1406','Editar Grupo','edit_group',1,'1','glyphicon glyphicon-pencil','','',1404,'grid','link','content','Permite  editar los datos de los grupos de usuarios',''),
('1407','Operaciones de Grupo','group_operation',1,'1','glyphicon glyphicon-list','','',1404,'grid','link','content','Permite  editar los datos de los grupos de usuarios',''),
('1408','Operaciones de Grupo','io_operation',1,'1','glyphicon glyphicon-check','','',1404,'grid','link','content','Permite activar o desactivar usuarios asociados al grupo','');

ALTER TABLE acl.groups
  ADD COLUMN _disabled boolean NOT NULL DEFAULT false;
COMMENT ON COLUMN acl.groups._disabled IS 'Permite desactivar o activar el grupo';

--CAMBIOS DEL 05-03-2015
ALTER TABLE acl.operation DROP CONSTRAINT chk_visual_type;
ALTER TABLE acl.operation
  ADD CONSTRAINT chk_visual_type CHECK (chk_visual_type::text = ANY (ARRAY['menu'::character varying::text, 'submenu'::character varying::text, 'method_js'::character varying::text, 'link'::character varying::text]));

INSERT INTO acl.operation(
            id, _name, url, _order, visible, icon, help, tooltip, id_operation,
            chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen)
    VALUES (
'1409','Activacion-Desactivacion de Usuarios','io_user',1,'1','glyphicon glyphicon-check','','',1401,'grid','method_js','content','Permite activar o desactivar usuarios',''

    );