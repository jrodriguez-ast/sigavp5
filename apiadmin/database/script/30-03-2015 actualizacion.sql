INSERT INTO acl.operation(
  id, _name, url, _order, visible, icon, help, tooltip, id_operation,
  chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen)
VALUES (
  '1302','Generar Factura','cotizacion_aprobada',1,'1','glyphicon glyphicon-saved','','',1301,'actionbar','link','content','',''
);

INSERT INTO acl.operation(
  id, _name, url, _order, visible, icon, help, tooltip, id_operation,
  chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen)
VALUES (
  '1303','Ver Cotización Facturar','cotizacion_facturar_ver',1,'1','glyphicon glyphicon-zoom-in','','',1302,'grid','link','content','',''
);


ALTER TABLE cotizaciones
ADD COLUMN por_facturar boolean NOT NULL DEFAULT FALSE;
COMMENT ON COLUMN cotizaciones.por_facturar IS 'Indica que la cotización ya generó el proceso de facturación, con este estado la cotización no se muestra en cotizaciones y queda como histórico. ';


INSERT INTO acl.operation(
            id, _name, url, _order, visible, icon, help, tooltip, id_operation,
            chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen)
    VALUES
    ('1311','Pendientes','factura_pendiente',1,'1',NULL,NULL,NULL,1300,'sidebar','submenu','content',NULL,NULL),
('1312','Editar Factura','factura_editar',1,'1','glyphicon glyphicon-pencil',NULL,NULL,1311,'grid','link','content',NULL,NULL);

ALTER TABLE clientes
  DROP COLUMN nombre;


INSERT INTO acl.operation(
            id, _name, url, _order, visible, icon, help, tooltip, id_operation,
            chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen)
    VALUES
    ('1304','Ver Factura','factura_ver',1,'1','glyphicon glyphicon-zoom-in',NULL,NULL,1301,'grid','link','content',NULL,NULL);
