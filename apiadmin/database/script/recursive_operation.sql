
WITH RECURSIVE operations AS
	(
		SELECT id, _name, url, _order, visible, icon, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on
		FROM acl.operation
		WHERE id=1000 --url = 'admin/listar_medios'
	    UNION ALL
		SELECT parent_operation.id, parent_operation._name, parent_operation.url, parent_operation._order, parent_operation.visible, parent_operation.icon, parent_operation.tooltip, parent_operation.id_operation, parent_operation.chk_render_on, parent_operation.chk_visual_type, parent_operation.chk_target_on
		FROM acl.operation AS parent_operation
			INNER JOIN operations AS ops ON (ops.id = parent_operation.id_operation)
	)

SELECT DISTINCT * FROM operations ORDER BY id;
