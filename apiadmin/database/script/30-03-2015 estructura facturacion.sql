DROP TRIGGER t_auditoria ON productos_facturas_estados;
DROP TRIGGER t_auditoria ON productos_facturas;
DROP TRIGGER t_auditoria ON facturas;
DROP TRIGGER t_auditoria ON compromisos_pagos;
DROP TRIGGER t_auditoria ON pagos;
DROP TABLE pagos;
DROP TABLE compromisos_pagos;
DROP TABLE productos_facturas_estados;
DROP TABLE productos_facturas;
DROP TABLE facturas;

-- Table: facturas

-- DROP TABLE facturas;

CREATE TABLE facturas
(
  id serial NOT NULL,
  codigo character varying,
  cliente_id integer NOT NULL,
  fecha_creacion timestamp without time zone NOT NULL,
  descuento double precision NOT NULL,
  usuario_id integer NOT NULL,
  modificado_en timestamp without time zone NOT NULL,
  con_impuestos boolean NOT NULL,
  porcentaje_impuesto double precision,
  es_terminada boolean NOT NULL DEFAULT false,
  subtotal double precision,
  total_impuesto double precision,
  total_descuento double precision,
  total double precision,
  motivo_descuento character varying(255),
  fue_enviada boolean NOT NULL DEFAULT false,
  aprobada boolean DEFAULT false,
  costo_hora_adicional double precision DEFAULT 0,
  subtotal_con_descuento double precision,
  promocion boolean,
  cotizacion_id integer,
  CONSTRAINT pk_facturas_id PRIMARY KEY (id),
  CONSTRAINT clientes_facturas_fk FOREIGN KEY (cliente_id)
      REFERENCES clientes (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON COLUMN facturas.id IS 'Identificador de la factura';
COMMENT ON COLUMN facturas.codigo IS 'Código';
COMMENT ON COLUMN facturas.cliente_id IS 'Identificador del cliente';
COMMENT ON COLUMN facturas.fecha_creacion IS 'Fecha de creación de la factura';
COMMENT ON COLUMN facturas.descuento IS 'Descuento sobre la factura que otorga el usuario que genera la factura';
COMMENT ON COLUMN facturas.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN facturas.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';
COMMENT ON COLUMN facturas.con_impuestos IS 'Indica si la factura incluye o no los impuestos como el IVA';
COMMENT ON COLUMN facturas.porcentaje_impuesto IS 'Indica el valor en porcentaje del impuesto a calcular';
COMMENT ON COLUMN facturas.es_terminada IS 'Indica si la factura fue culmidad, es decir, al finalizar el proceso la factura se almacena con éxito.';
COMMENT ON COLUMN facturas.subtotal IS 'SubTotal de la factura';
COMMENT ON COLUMN facturas.total_impuesto IS 'Total calculado a aplicar por razón de impuestos';
COMMENT ON COLUMN facturas.total_descuento IS 'Total calculado del porcentaje de descuento aplicado.';
COMMENT ON COLUMN facturas.total IS 'Total de la factura.';
COMMENT ON COLUMN facturas.motivo_descuento IS 'Motivo por el cual se le dio un descuento en la factura';
COMMENT ON COLUMN facturas.fue_enviada IS 'Indica si la factura fue enviada por correo o no';
COMMENT ON COLUMN facturas.aprobada IS 'Indica si la factura fue aprobada para su facturación';
COMMENT ON COLUMN facturas.costo_hora_adicional IS 'Costo de la hora adicional por requerimientos no contemplados';
COMMENT ON COLUMN facturas.subtotal_con_descuento IS 'Subtotal menos el descuento realizado';
COMMENT ON COLUMN facturas.promocion IS 'Indica que la factura es una promoción que se está haciendo';
COMMENT ON COLUMN facturas.cotizacion_id IS 'Identificador de la cotización desde donde nace la factura';


-- Trigger: t_auditoria on facturas

-- DROP TRIGGER t_auditoria ON facturas;

CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON facturas
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();

-- Table: productos_facturas

-- DROP TABLE productos_facturas;

CREATE TABLE productos_facturas
(
  id serial,
  factura_id integer NOT NULL,
  producto_id integer NOT NULL,
  fecha_adquisicion timestamp without time zone,
  fecha_activacion timestamp without time zone,
  fecha_vencimiento timestamp without time zone,
  precio double precision NOT NULL,
  descuento double precision NOT NULL,
  activo boolean NOT NULL DEFAULT true,
  usuario_id integer NOT NULL,
  modificado_en timestamp without time zone NOT NULL,
  cantidad smallint NOT NULL DEFAULT 1,
  precio_total double precision,
  CONSTRAINT pk_productos_factura_id PRIMARY KEY (id),
  CONSTRAINT facturas_productos_factura_fk FOREIGN KEY (factura_id)
      REFERENCES facturas (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT productos_productos_factura_fk FOREIGN KEY (producto_id)
      REFERENCES productos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE productos_facturas
  IS 'Productos facturados';
COMMENT ON COLUMN productos_facturas.id IS 'Identificador del registro';
COMMENT ON COLUMN productos_facturas.factura_id IS 'Identificador de la factura';
COMMENT ON COLUMN productos_facturas.fecha_adquisicion IS 'Fecha en la que el producto fue adquirido';
COMMENT ON COLUMN productos_facturas.fecha_activacion IS 'Fecha en la que se activa el producto y a partir del cual se contabiliza el tiempo en que estará activo.';
COMMENT ON COLUMN productos_facturas.fecha_vencimiento IS 'Fecha en la que vence el producto adquirido según el contrato';
COMMENT ON COLUMN productos_facturas.precio IS 'Precio extablecido para el producto en el momento en que se crea la cotización';
COMMENT ON COLUMN productos_facturas.descuento IS 'Porcentaje de descuento sobre el precio del producto, otorgado por el iusuario que genera la cotización';
COMMENT ON COLUMN productos_facturas.activo IS 'Indica si el producto se encuentra activo o no';
COMMENT ON COLUMN productos_facturas.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN productos_facturas.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';
COMMENT ON COLUMN productos_facturas.cantidad IS 'Cantidad de productos a adquirir';


-- Trigger: t_auditoria on productos_facturas

-- DROP TRIGGER t_auditoria ON productos_facturas;

CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON productos_facturas
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();

-- Table: productos_facturas_estados

-- DROP TABLE productos_facturas_estados;

CREATE TABLE productos_facturas_estados
(
  id serial NOT NULL,
  estado character varying(10) NOT NULL,
  producto_factura_id integer NOT NULL,
  cliente_producto_id integer NOT NULL,
  usuario_id integer NOT NULL,
  modificado_en timestamp without time zone NOT NULL,
  CONSTRAINT pk_productos_facturas_estados_id PRIMARY KEY (id),
  CONSTRAINT productos_factura_productos_facturas_estados_fk FOREIGN KEY (id)
      REFERENCES productos_facturas (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE productos_facturas_estados
  IS 'Almacena el histórico de estados que tiene un producto facturado para un cliente';
COMMENT ON COLUMN productos_facturas_estados.id IS 'Identificador del estado';
COMMENT ON COLUMN productos_facturas_estados.estado IS 'Estado en que se encuetra el producto con respecto al cliente. Se registra un histórico y por aplicación se dará siempre el último estado.';
COMMENT ON COLUMN productos_facturas_estados.cliente_producto_id IS 'Identificador de la relación clientes_productos';
COMMENT ON COLUMN productos_facturas_estados.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN productos_facturas_estados.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


-- Trigger: t_auditoria on productos_facturas_estados

-- DROP TRIGGER t_auditoria ON productos_facturas_estados;

CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON productos_facturas_estados
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();


-- Table: compromisos_pagos

-- DROP TABLE compromisos_pagos;

CREATE TABLE compromisos_pagos
(
  id serial NOT NULL, -- Identificador del compromiso de pago
  codigo character varying NOT NULL, -- Código
  factura_id integer NOT NULL, -- Identificador de la factura
  cantidad_cuotas integer NOT NULL, -- Cantidad cuotas en la que acuerda el cliente realizar el pago
  frecuencia_pago_id integer NOT NULL, -- Indica la frecuencia con la que realizará los pagos, 7 semanal, 15 quincenal, 30 mensual, 90 trimestral. Se representa por un número de días.
  fecha_inicio timestamp without time zone NOT NULL, -- Fecha en que acuerdan las partes inciar con los pagos.
  usuario_id integer NOT NULL, -- Identificador del usuario que inserta, modifica o elimina el registro
  modificado_en timestamp without time zone NOT NULL, -- Fecha en que se insertó, modificó o eliminó el registro
  CONSTRAINT pk_compromisos_pagos_id PRIMARY KEY (id),
  CONSTRAINT facturas_compromisos_pagos_fk FOREIGN KEY (factura_id)
      REFERENCES facturas (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION,
  CONSTRAINT frecuencias_pagos_compromisos_pagos_fk FOREIGN KEY (frecuencia_pago_id)
      REFERENCES frecuencias_pagos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON COLUMN compromisos_pagos.id IS 'Identificador del compromiso de pago';
COMMENT ON COLUMN compromisos_pagos.codigo IS 'Código';
COMMENT ON COLUMN compromisos_pagos.factura_id IS 'Identificador de la factura';
COMMENT ON COLUMN compromisos_pagos.cantidad_cuotas IS 'Cantidad cuotas en la que acuerda el cliente realizar el pago';
COMMENT ON COLUMN compromisos_pagos.frecuencia_pago_id IS 'Indica la frecuencia con la que realizará los pagos, 7 semanal, 15 quincenal, 30 mensual, 90 trimestral. Se representa por un número de días.';
COMMENT ON COLUMN compromisos_pagos.fecha_inicio IS 'Fecha en que acuerdan las partes inciar con los pagos.';
COMMENT ON COLUMN compromisos_pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN compromisos_pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


-- Trigger: t_auditoria on compromisos_pagos

-- DROP TRIGGER t_auditoria ON compromisos_pagos;

CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON compromisos_pagos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();


-- Table: pagos

-- DROP TABLE pagos;

CREATE TABLE pagos
(
  id serial NOT NULL, -- Identificador del compromiso de pago
  compromiso_pago_id integer NOT NULL, -- Identificador del compromiso de pago
  fecha_pago timestamp without time zone NOT NULL, -- Fecha de pago
  comprobante_pago character varying(50) NOT NULL, -- Código del comprobante del pago realizado
  fecha_inicia_periodo timestamp without time zone NOT NULL, -- Fecha en la que inicia el período del pago
  fecha_fin_periodo timestamp without time zone NOT NULL, -- Fecha en que finaliza el período del pago
  es_pago boolean NOT NULL DEFAULT false, -- Indica si el pago fue realizado.
  es_vencido boolean, -- Indica si el pago se encuentra vencido
  fecha_vencimiento timestamp without time zone, -- Indica la fecha en que el pago venció,
  fecha_notificacion_vencimiento timestamp without time zone, -- Fecha en que se envió la notificación de vencimiento de acuerdo de pago
  usuario_id integer NOT NULL, -- Identificador del usuario que inserta, modifica o elimina el registro
  modificado_en timestamp without time zone NOT NULL, -- Fecha en que se insertó, modificó o eliminó el registro
  CONSTRAINT pk_pagos_id PRIMARY KEY (id),
  CONSTRAINT compromisos_pagos_pagos_realizados_fk FOREIGN KEY (compromiso_pago_id)
      REFERENCES compromisos_pagos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);
COMMENT ON TABLE pagos
  IS 'Almacena los pagos sobre los compromisos adquiridos';
COMMENT ON COLUMN pagos.id IS 'Identificador del compromiso de pago';
COMMENT ON COLUMN pagos.compromiso_pago_id IS 'Identificador del compromiso de pago';
COMMENT ON COLUMN pagos.fecha_pago IS 'Fecha de pago';
COMMENT ON COLUMN pagos.comprobante_pago IS 'Código del comprobante del pago realizado';
COMMENT ON COLUMN pagos.fecha_inicia_periodo IS 'Fecha en la que inicia el período del pago';
COMMENT ON COLUMN pagos.fecha_fin_periodo IS 'Fecha en que finaliza el período del pago';
COMMENT ON COLUMN pagos.es_pago IS 'Indica si el pago fue realizado.';
COMMENT ON COLUMN pagos.es_vencido IS 'Indica si el pago se encuentra vencido';
COMMENT ON COLUMN pagos.fecha_vencimiento IS 'Indica la fecha en que el pago venció,';
COMMENT ON COLUMN pagos.fecha_notificacion_vencimiento IS 'Fecha en que se envió la notificación de vencimiento de acuerdo de pago';
COMMENT ON COLUMN pagos.usuario_id IS 'Identificador del usuario que inserta, modifica o elimina el registro';
COMMENT ON COLUMN pagos.modificado_en IS 'Fecha en que se insertó, modificó o eliminó el registro';


-- Trigger: t_auditoria on pagos

-- DROP TRIGGER t_auditoria ON pagos;

CREATE TRIGGER t_auditoria
  BEFORE INSERT OR UPDATE OR DELETE
  ON pagos
  FOR EACH ROW
  EXECUTE PROCEDURE ft_auditoria();

