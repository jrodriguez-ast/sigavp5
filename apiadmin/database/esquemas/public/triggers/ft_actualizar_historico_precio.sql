CREATE FUNCTION ft_actualizar_historico_precio() RETURNS trigger
AS $$
    BEGIN

        INSERT INTO productos_historico_precio (
            producto_id,
            precio_anterior,
            precio_nuevo,
            usuario_id,
            modificado_en
        )
        VALUES (
            NEW.id,
            OLD.precio,
            NEW.precio,
            NEW.usuario_id,
            NOW()
        );

        RETURN NEW;
    END;
$$ LANGUAGE plpgsql;

COMMENT ON FUNCTION ft_actualizar_historico_precio() IS '
Inserta un registro por cada actualización que se realiza en el precio de un producto.
Mantiene en el histórico el precio anterior, el precio nuevo, fecha de cambio y quien lo realizó.

Autor: Darwin Serrano
Version: V1.0 23/01/2015
';


CREATE TRIGGER t_actualizar_historico_precio BEFORE UPDATE OF precio
ON productos
FOR EACH ROW
EXECUTE PROCEDURE ft_actualizar_historico_precio();