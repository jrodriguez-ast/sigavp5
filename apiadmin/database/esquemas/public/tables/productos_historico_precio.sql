CREATE TABLE productos_historico_precio
(
  id serial NOT NULL,
  producto_id integer NOT NULL,
  precio_anterior double precision,
  precio_nuevo double precision,
  usuario_id integer NOT NULL,
  modificado_en timestamp NOT NULL,
  CONSTRAINT pk_productos_historico_precio_id PRIMARY KEY (id),
  CONSTRAINT productos_productos_historico_precio_fk FOREIGN KEY (producto_id)
      REFERENCES productos (id) MATCH SIMPLE
      ON UPDATE CASCADE ON DELETE NO ACTION
);

COMMENT ON TABLE productos_historico_precio IS 'Almacena un histórico del cambio de precios en para los productos.';
COMMENT ON COLUMN productos_historico_precio.id IS 'Identificador del registro';
COMMENT ON COLUMN productos_historico_precio.producto_id IS 'Identificador del producto al que se actualiza el precio';
COMMENT ON COLUMN productos_historico_precio.precio_anterior IS 'Precio que tenía el producto antes de ser actualizado';
COMMENT ON COLUMN productos_historico_precio.precio_nuevo IS 'Precio al que se actualiza el producto.';
COMMENT ON COLUMN productos_historico_precio.usuario_id IS 'Usuario que ejecutó la acción';
COMMENT ON COLUMN productos_historico_precio.modificado_en IS 'Fecha Hora en que se ejecutó la acción';

