INFORMACIÓN IMPORTANTE A TOMAR EN CUENTA AL DESARROLLAR

- Se recomienda extender los modelos de la clase MY_Model, ya que esta clase contiene métodos que facilita el trabajo
  con los datos, y permite hacer uso del ActiveRecord de codeigniter, ella extiende Model_Audit que debe ser utilizada
  en los modelos que se quiere realizar auditorías.

- Al usar el método $this->db->insert_id() es obligatorio pasar los parámetros tabla y columna que tiene asignada
  la secuencia que genera el id. Esto es necesario porque insert_id por defecto usa el método LASTVAL de postgres
  (obtiene el último valor generado por la última secuencia en usarse en la sesión de postgres)
  para obtener el id que se generó para el registro, y este método en postgres toma otro valor al ejecutarse el trigger
  de auditoría que inserta un registro en la tabla public.auditorias.

- Para implementar la auditoría en los modelos, se deben extender los modelos de la clase Model_Audit y agregar
  los observer before_delete y after_delete. Tomen en cuenta que la clase Model_Audit extiende de MY_Model.