<?php

/**
 * Created by PhpStorm.
 * User: darwin
 * Date: 18/03/15
 * Time: 10:45 AM
 *
 * Esta librería extiende de MY_Model con la finalidad de apoyar el proceso de auditoria.
 *
 * La intención es aprovechar los eventos before_* y after_* de la librería MY_Model para registrar el recorrido
 * de una operación.
 *
 * Este trabaja en conjunto con el Hook Audit.php, el Model auditoria_model y la librería MY_Model.php.
 *
 * En base de datos se encuentra un trigger, que se dispara por cada tabla, que se encarga de registrar los estados de
 * los datos al realizar una transacción sobre ellos.
 */
class AST_Audit_Model extends MY_Model
{
    public $a_before_create = array();
    public $a_after_create = array('audit_after_create');
    public $a_before_update = array();
    public $a_after_update = array('audit_after_update');
    public $a_before_get = array();
    public $a_after_get = array();
    public $a_before_delete = array('audit_before_delete');
    public $a_after_delete = array('audit_after_delete');


    /**
     * Almacena el registro a eliminar haciendo uso del método audit_before_delete
     * @var integer
     */
    private $_last_transaction_id = null;

    /**
     * Se reescribe el método trigger para asignar los métodos de la auditoría
     *
     * @param $event
     * @param bool $data
     * @param bool $last
     * @return bool|mixed
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 19/03/15
     */
    public function trigger($event, $data = FALSE, $last = TRUE)
    {
        $a_event = 'a_' . $event;
        if (isset($this->{$a_event}) && is_array($this->{$a_event})) {
            if (isset($this->$event) && is_array($this->$event)) {
                $this->{$event} = array_merge($this->{$event}, $this->{$a_event});
            }
        }

        if (isset($this->$event) && is_array($this->$event)) {
            foreach ($this->$event as $method) {
                if (strpos($method, '(')) {
                    preg_match('/([a-zA-Z0-9\_\-]+)(\(([a-zA-Z0-9\_\-\., ]+)\))?/', $method, $matches);

                    $method = $matches[1];
                    $this->callback_parameters = explode(',', $matches[3]);
                }

                $data = call_user_func_array(array($this, $method), array($data, $last));
            }
        }

        return $data;
    }

    /**
     * Se reescribe método update para agregar al evento after_update
     * @param $primary_value
     * @param $data
     * @param bool $skip_validation
     * @return bool
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 19/03/15
     */
    public function update($primary_value, $data, $skip_validation = FALSE)
    {
        $data = $this->trigger('before_update', $data);

        if ($skip_validation === FALSE) {
            $data = $this->validate($data);
        }

        if ($data !== FALSE) {
            $result = $this->_database->where($this->primary_key, $primary_value)
                ->set($data)
                ->update($this->_table);

            $this->trigger('after_update', array($data, $result, $primary_value));

            return $result;
        } else {
            return FALSE;
        }
    }

    /**
     * Este método se debe ejecutar antes del método delete de la clase MY_Model
     * Almacena el id del registro a eliminar en el atributo $_record_delete_id
     *
     * Trabaja en junto con el método audit_after_delete para poder llevar a cabo la actualización del registro
     * en auditoría.
     *
     * Para ejecutar el método se debe asociar al evento before_delete en la clase donde se extienda
     * la clase AST_Audit_Model
     *
     * @param integer $where
     *
     * @return int
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 19/03/15
     */
    public function audit_before_delete($where)
    {
        if (!is_array($where)) {
            $this->_last_transaction_id = $where;
        } else {
            $query = $this->get_by($where[0]);
            if (isset($query) && !empty($query)) {
                $this->_last_transaction_id = $query->id;
            }
        }
        return $where;
    }

    /**
     * Este método debe ser ejecutado después del método delete de la clase MY_Model.
     * Permite actualizar el registro de auditoría que se creó al eliminar un registro con el id del usuario
     * y los datos de conexión al sistema
     *
     * Trabaja en conjunto con el método audit_before_delete, requerido para obtener el id del registro a eliminar
     * antes de que sea eliminado.
     *
     * Para ejecutar el método se debe asociar al evento after_delete en la clase donde se extienda
     * la clase AST_Audit_Model
     *
     * @param boolean $status
     * @return bool
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 19/03/15
     */
    public function audit_after_delete($status)
    {
        if (!is_null($this->_last_transaction_id)) {
            $this->auditoria_model->set_delete_user($this->_last_transaction_id, $this->session->userdata('user_id'), $this->_table);
        }

        return $status;
    }

    /**
     * Actualizar el registro insertado con los datos de conexión en la aplicación.
     * @param $insert_id
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 19/03/15
     */
    public function audit_after_create($insert_id)
    {
        $this->auditoria_model->update_transaction($insert_id, $this->_table);

        return $insert_id;
    }


    /**
     * Al realizar un update, actualiza el registro de la auditoría con los datos de la conexión en la aplicación
     * @param $data
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 19/03/15
     */
    public function audit_after_update($data)
    {
        if (isset($data[2])) {
            $id = $data[2];
            $this->auditoria_model->update_transaction($id, $this->_table, 'UPDATE');
        }

        return $data;
    }
}