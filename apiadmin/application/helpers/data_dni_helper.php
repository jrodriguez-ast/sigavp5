<?php

if (!function_exists('verificaRif')) {

    function verificaRif($rif)
    {
        $response_json = array('code_result' => '', 'response' => false);
        $msj['-1'] = "No Hay Soporte a Curl";
        $msj['0'] = "No Hay Conexión a Internet";
        $msj['1'] = "Existe RIF Consultado";
        $msj['450'] = "Formato de RIF Invalido";
        $msj['452'] = "RIF no Existe";
        if (!empty($rif)):
            if (function_exists('curl_init')) {// Comprobamos si hay soporte para cURL
                $url = "http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif=$rif";
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_TIMEOUT, 30);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                $resultado = curl_exec($ch);
                if ($resultado) {
                    try {
                        if (substr($resultado, 0, 1) != '<')
                            throw new Exception($resultado);
                        $xml = simplexml_load_string($resultado);
                        if (!is_bool($xml)) {
                            $elements = $xml->children('rif');
                            $seniat = array();
                            $response_json['code_result'] = 1;
                            foreach ($elements as $indice => $node) {
                                $index = strtolower($node->getName());
                                $response_json[$index] = (string)$node;
                            }

                        }
                    } catch (Exception $e) {
                        $result = explode(' ', $resultado, 2);
                        $response_json['code_result'] = (int)$result[0];
                    }
                } else {
                    $response_json['code_result'] = 0;//No hay conexion a internet
                }
            } else {
                $response_json['code_result'] = -1;//No hay soporte a curl_php

            }
            if ($response_json['code_result'] == 1):
                $response_json['response'] = true;
            endif;
            $response_json['mensaje'] = $msj[$response_json['code_result']];
        else:
            $response_json['mensaje'] = 'Debe Verificar el RIF';
        endif;

        return $response_json;
    }

    if (!function_exists('verificaCedula')) {

        function verificaCedula($ci)
        {
            $response=array();
            $url="http://www.cne.gov.ve/web/registro_electoral/ce.php?nacionalidad=V&cedula=$ci";
            $ch = curl_init();
            curl_setopt ($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true); // almacene en una variable
            curl_setopt($ch, CURLOPT_HEADER, true);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

            $xxx1 = curl_exec($ch);
            curl_close($ch);
            $xxx = explode("</strong>", $xxx1);
// Imprimo la pantalla Completa del sistema divido en arreglo
// print_r($xxx);
            $ciDatos = explode("<font", $xxx[2]);

            $response['cedula']=$ciDatos[0];

            $datos = explode(" ", $xxx[3]);
            $response['apellidos'] = $datos[3].' '.$datos[4];
            $response['nombres'] = $datos[5].' '.$datos[6];

// Imprimir los datos Asociado a la cedula consultada en la CNE
            //echo "<strong>C&eacute;dula:</strong>".$cedula;
            //echo "<br/>Apellidos: </strong>".$apellidos;
            //echo "<br/><strong>Nombres: </strong>".$nombres;

            /* Nota: si quieren saber la información completa del ciudadano sobre su centro de votación
             * descomenta la linea 14 print_r($xxx); y alli te mostrara la pantalla completa, debes tomar
             * el arreglo con el indice asociado y asi muestra la informacion necesaria, como por ejemplo
             * Centro de votación y Dirección.
             */

            return $response;
        }
    }
}