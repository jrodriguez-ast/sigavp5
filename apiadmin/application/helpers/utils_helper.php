<?php
/**
 * Created by PhpStorm.
 * User: darwin
 * Date: 18/02/15
 * Time: 09:36 AM
 */


/**
 * Verifica la existencia del directorio indicado, de no existir crea el directorio y da permisos 775.
 *
 * La creación del directorio es recursiva, por lo que se le pueden enviar una ruta de la siguiente forma
 * dir1/dir2/dir3
 *
 * @param $path
 * @return bool true si existe el directorio o si fue creado con éxito
 */
if (!function_exists('verificar_directorio')) {

    function verificar_directorio($path)
    {
        if (!file_exists($path)) {
            if (!mkdir($path, 0777, true)) {
                return false;
            }
        }
        return true;
    }

}