<?php

class Group_models extends AST_Audit_Model
{

    protected $_table = 'acl.groups';
    public $_table_group_operation = 'acl.group_operation';
    public $_table_operation = 'acl.operation';

    public $before_create = array('prepare');
    public $before_update = array('prepare');

    public function __construct()
    {
        parent::__construct();
    }

    protected function prepare($data)
    {
        $data['usuario_id'] = $this->session->userdata('user_id');
        $data['modificado_en'] = date('Y-m-d H:i:s');
        return $data;
    }

    /**
     * Provee los datos de los grupos
     *
     * @access public
     * @param integer $id Identificador del grupo
     * @return array
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 27/02/15 08:50 AM
     */
    public function get_groups($id = null)
    {
        if ($id):
            $this->db->select('*', FALSE)
                ->from("{$this->_table}", "group")
                ->where("id", $id)
                ->limit(1);
            $query = $this->db->get();

            return $query->result_array();
        else:
            return $this->get_all();
        endif;

    }

    /**
     * Provee los datos para el listado de Grupos de Usuarios
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 26/02/15 02:44 PM
     */
    public function get_datatable_list($searchs)
    {
        foreach ($searchs['columns'] as $key => $row) {
            if ($key == 0 or preg_match('/_status/i',$row['name'])) {
                continue;
            }
            $this->datatables->or_like($row['name'], $searchs['search']['value'],'both');
        }

        $this->datatables
            ->select('groups.*')
            ->select("(CASE WHEN groups._disabled THEN 'INACTIVO' ELSE 'ACTIVO' END) as _status", FALSE)
            ->from("$this->_table AS groups");
        return $this->datatables->generate();
    }

    /**
     * Crea o actualiza los grupos de usuarios
     *
     * @access public
     * @param integer $id Identificador del grupo
     * @param array $data Datos a procesar
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 27/02/15 10:45 AM
     */
    public function group_management($id = null, $data = null)
    {
        $exist_group = 0;
        $response = array(
            'msg' => '',
            'success' => false
        );
        if (!$data):
            return false;
        else:
            $this->db->select('name');
            $this->db->from($this->_table);
            $this->db->where('name', $data['name']);
            $query = $this->db->get();
            $exist_group = $query->num_rows();

            $this->db->trans_start();
            if ($id):
                $this->update($id, $data);
            else:
                if ($exist_group > 0):
                    return $response = array(
                        'msg' => 'El grupo que que desea registrar ya existe!',
                        'success' => false
                    );
                else:
                    $this->insert($data);
                endif;

            endif;
            $this->db->trans_complete();
            return $response = array(
                'msg' => '',
                'success' => $this->db->trans_status()
            );

        endif;

    }

    /**
     * Provee los datos de la entidad acl.aperation ordenados en forma jerarquica
     *
     * @access  public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @author  Eliezer Pe#a <pe.eliezer@gmail.com>
     * @version V 1.0 11/12/13 10:32 AM
     */
    function menu_operation($with_session_user = FALSE)
    {
        //$user_id = $this->session->userdata('user_id');
        $query_string = "
                                WITH RECURSIVE q AS
                                    (
                                        SELECT
                                                distinct h,
                                                1 AS level,
                                                ARRAY[h.id] AS breadcrumb
                                        FROM
						acl.users_groups as user_g
						INNER JOIN acl.groups as g ON user_g.group_id=g.id
						INNER JOIN acl.group_operation g_ope ON g.id=g_ope.id_group
						INNER JOIN acl.operation h ON g_ope.id_operation=h.id


                                        WHERE

                                                h.id_operation IS NULL ";
        //$query_string.=($with_session_user ? 'AND user_g.user_id=' . (int) $user_id : '');

        $query_string .= "       UNION ALL
                                        SELECT
                                                distinct hi,
                                                q.level + 1 AS level,
                                                breadcrumb || hi.id
                                        FROM
                                                q
                                                JOIN acl.operation hi ON hi.id_operation = (q.h).id
                                                INNER JOIN acl.group_operation g_ope ON hi.id=g_ope.id_operation
                                                INNER JOIN acl.users_groups as user_g ON g_ope.id_group=user_g.group_id ";
        //$query_string.=($with_session_user ? 'AND user_g.user_id=' . (int) $user_id : '');

        $query_string .= ")
                SELECT
                        REPEAT('', level) || (q.h).id as id,
                        REPEAT('', level) || (q.h).id as value,
                        COALESCE((q.h).id_operation, 0) as id_operation,
                        (CASE WHEN (q.h).id_operation IS NULL THEN
                                                    upper ((q.h)._name)
                                              ELSE
                                                    (q.h)._name
                                              END) as label,
                        level,
                        breadcrumb::VARCHAR AS path
                FROM
                        q
                ORDER BY
                        breadcrumb";
        $query = $this->db->query($query_string);
        //var_dump($query);
        //echo $this->db->last_query(); die;
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;
    }

    /**
     * Descripción
     *
     * @access public
     * @param
     * @return bool
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 02/03/15 01:05 PM
     */
    function io_groups($id = NULL)
    {

        if (!$id)
            return FALSE;
        $this->db->select();
        $this->db->from("$this->_table");
        $this->db->where('id', $id);

        $query = $this->db->get();
        return ($query->num_rows() > 0) ? $query->result_array() : FALSE;

    }

    /**
     * @param null $id
     * @return bool
     */
    function proccess_io_groups($id = NULL)
    {

        if (!$id)
            return FALSE;
        $this->db->select("$this->_table._disabled");
        $this->db->from("$this->_table");
        $this->db->where('id', $id);

        $query = $this->db->get();
        $data = $query->row_array();

        if ($data['_disabled'] == 't') {
            $data = array('_disabled' => 'f');
        } elseif ($data['_disabled'] == 'f') {
            $data = array('_disabled' => 't');
        }

        $this->update($id, $data);
        return true;

    }

    /**
     * Dado el identificador del grupo provee todas las operaciones asociadas
     * @access  public
     * @param   integer $id Identificador del grupo
     * @return mixed
     * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 11/12/13 10:55 AM
     */
    function get_group_operation($id = NULL)
    {

        if (!$id):
            return FALSE;
        endif;

        //tables
        $t_group_operation = 'acl.group_operation';

        //Select
        $this->db->select("$t_group_operation.id_operation");

        //From and join
        $this->db->from("$t_group_operation");
        $this->db->where("$t_group_operation.id_group", $id);

        $query = $this->db->get();
        $response = array();
        if ($query->num_rows() > 0):
            foreach ($query->result_array() as $key => $value):
                $response[] = (int)$value['id_operation'];
            endforeach;
            return $response;
        else:
            return FALSE;
        endif;
    }

    /**
     * Retorna listado de grupos de usuarios
     * @return mixed
     */
    public function get_list()
    {
        $this->db->select("id, name", false);
        return $this->order_by('name')->as_array()->get_all();
    }
}
