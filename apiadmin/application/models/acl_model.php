<?php

if (!defined ('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package        CodeIgniter
 * @subpackage            Models
 * @category        Models
 * @author        Nohemi Rojas<nohemir@gmail.com>
 * @since               V 1.0
 */

/**
 * CodeIgniter Acl_model Class
 *
 */
class Acl_model extends CI_Model
{

    /**
     * Nombre de la tabla operations
     *
     * @access private
     * @var string
     */
    private $tb_ope = 'acl.operation';

    /**
     * Nombre de la tabla users_groups
     *
     * @access private
     * @var string
     */
    private $tb_use_gro = 'acl.users_groups';

    /**
     * Nombre de la tabla groups
     *
     * @access private
     * @var string
     */
    private $tb_gro = 'acl.groups';

    /**
     * Nombre de la tabla groups
     *
     * @access private
     * @var string
     */
    private $tb_gro_ope = 'acl.group_operation';

    /**
     * Nombre de la tabla de usuarios
     *
     * @access private
     * @var integer
     */
    private $tb_user = 'acl.users';

    /**
     * Constructor
     *
     * @access    public
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function __construct ()
    {
        parent::__construct ();
    }

    /**
     * Obtiene el rol de un determinado usuario
     *
     * @access    public
     * @param   integer $user_id identificador del usuario
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function getRole ($user_id)
    {

        $this->db->select ('g.name AS role_name, group_id AS role_id');
        $this->db->from ("$this->tb_use_gro AS ug");
        $this->db->join ("$this->tb_gro AS g", "g.id = ug.group_id", 'left');
        $this->db->where ('user_id', $user_id);
        $query = $this->db->get ();
        return ($query->num_rows () > 0) ? $query->row_array () : FALSE;
    }

    /**
     * Obtiene los permisos de un determinado rol
     *
     * @access    public
     * @param   integer $role_id identificador del rol
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author  Nohemi Rojas, Jose Rodriguez
     * @version V-1.1 30/11/12 15:16
     */
    public function getPermissions ($role_id)
    {
        //selects
        $this->db->select ('ope.id');
        $this->db->select ('ope._name');
        $this->db->select ('ope.url');
        $this->db->select ('ope.id_operation');
        $this->db->select ('ope.chk_render_on AS render_on');
        $this->db->select ('ope.chk_visual_type AS visual_type');
        $this->db->select ('ope.icon');
        $this->db->select ('ope.chk_target_on AS target_on');
        $this->db->select ('ope._order AS order');

        //From and inners
        $this->db->from ($this->tb_gro_ope);
        $this->db->join ($this->tb_ope . ' AS ope', "ope.id = {$this->tb_gro_ope}.id_operation");
        $this->db->join ($this->tb_gro . ' AS gro', "gro.id = {$this->tb_gro_ope}.id_group");

        //Wheres
        $this->db->where ('ope.visible', '1');
        $this->db->where ('gro.id', $role_id);

        //Orders
        $this->db->order_by ('ope.id_operation', 'ASC');
        $this->db->order_by ('ope._order', 'ASC');
        $this->db->order_by ('ope.id', 'ASC');

        $query = $this->db->get();

        if (!$query->num_rows () > 0)
            return FALSE;

        $results = $query->result_array ();
        $results_format = array();

        //formatea el arreglo, para que cada operacion sea la clave
        foreach ($results as $value) {
            $results_format[$value['id']] = $value;
        }
        return $results_format;
    }

    /**
     * Obtiene los permisos de un determinado rol
     *
     * @access    public
     * @param   integer $user_id identificador del usuario
     * @return  mixed Array de los valores de los registros, false en caso de que no encuentre
     * @author  Jefferson Lara
     * @version V-1.0 12/01/15 09:46
     */
    public function get_info_user ()
    {
        $return = array();

        $id = $this->session->userdata ('user_id');

        if (!empty($id)):

            $this->db->select ('usu.username');
            $this->db->select ('first_name');
            $this->db->select ('last_name');
            $this->db->select ('email');
            $this->db->select ('cliente_id');
            $this->db->select ('group_id');
            $this->db->select ('name');
            $this->db->from ($this->tb_user . ' AS usu');
            $this->db->join($this->tb_use_gro . ' AS usu_gru', 'usu_gru.user_id = usu.id');
            $this->db->join($this->tb_gro . ' AS gru', 'gru.id = usu_gru.group_id');
            $this->db->where ('usu.id', $id);

            $query = $this->db->get ();

            if ($query->num_rows () > 0):
                foreach ($query->result () as $valor):
                    $return['username'] = $valor->username;
                    $return['first_name'] = $valor->first_name;
                    $return['last_name'] = $valor->last_name;
                    $return['email'] = $valor->email;
                    $return['cliente_id'] = $valor->cliente_id;
                    $return['group_id'] = $valor->group_id;
                    $return['name'] = $valor->name;
                endforeach;
                $return['user_id'] = $id;
            endif;

        endif;

        return $return;
    }

    /**
     * Obtiene los permisos de una determinada url
     *
     * @access    public
     * @param   integer $user_id identificador del operacion
     * @return  mixed Array
     * @author  Jefferson Lara
     * @version V-1.0 12/01/15 09:46
     */
    public function get_operation ($id_operation)
    {

        $return = array();
        $permissions = $this->session->userdata ('permissions');

        $this->db->order_by ('_order', 'ASC');
        $query = $this->db->get_where ($this->tb_ope, array('id_operation' => $id_operation));

        if ($query->num_rows () > 0) {
            foreach ($query->result_array () as $key => $row):
                if (array_key_exists ($row['id'], $permissions))
                    array_push($return, $row);
            endforeach;
        }

        return $return;
    }

}

// END Acl_model class
/* End of file acl_model.php */
/* Location: application/modules/auth/model/acl_model.php */
