<?php
class Sectores extends CI_Model {

    function __construct(){
        parent::__construct();

    }

    /**
     *
     * Devuelve la data registrada en la tabal de sectores o actividad economica
     *
     * @author Jefferspn Lara
     * @access public
     * @version V-1.0 <jetox21@gmail.com>
     * @return array()
     *
     */
    public function get_sectores()
    {   $response=array();
        $query=$this->db->get('sectores');

        if ($query->num_rows()>0) {
            foreach($query->result() as $row):
                $response[]=array(
                    'id'=>$row->id,
                    'name'=>$row->nombre
                );
            endforeach;

        }

        return $response;
    }
} 