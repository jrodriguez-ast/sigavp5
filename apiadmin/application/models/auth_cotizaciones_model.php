<?php

class Auth_cotizaciones_model extends CI_Model {

    protected $_table = 'acl.ci_sessions';
    public $_table_users = 'acl.users';

    public function __construct(){
        parent::__construct();
    }

    /**
     * Provee los datos de los grupos
     *
     * @access public
     * @param integer $id    Identificador del grupo
     * @return array
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 27/02/15 08:50 AM
     */
    public function get_session($id=null){
        if($id):
            $this->db->select('*', FALSE)
                ->where("id", $id)
                ->from($this->_table_users);
            $query = $this->db->get();
            $data_user=$query->result_array();
            if($data_user):
                $this->db->select('session_id', FALSE)
                    ->like("user_data",'"'.$data_user[0]['email'].'"')
                    ->from($this->_table);
                $query = $this->db->get();
                $data_user_session=$query->result_array();
                return $this->destroy_session($data_user_session);
            endif;
        endif;

    }

    public function destroy_session($session_id=null){
        if(!empty($session_id)):
            foreach($session_id as $key=>$value):
                $this->db->where('session_id', $value['session_id']);
                $this->db->delete($this->_table);
            endforeach;
            return true;
        endif;

    }
}
