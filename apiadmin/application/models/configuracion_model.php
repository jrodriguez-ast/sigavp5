<?php

/**
 * Created by PhpStorm.
 * User: darwin
 * Date: 19/03/15
 * Time: 04:32 PM
 */
class Configuracion_model extends AST_Audit_Model
{
    protected $_table = 'public.configuraciones';

    public function get_porcentaje_impuesto()
    {
        $query = $this->db->get_where('configuraciones', array('clave' => 'IVA'), 1);
        $record = $query->row();

        return $record->valor;
    }

    public function get_costo_hora_adicional()
    {
        $query = $this->db->get_where('configuraciones', array('clave' => 'costo_hora_adicional'), 1);
        $record = $query->row();

        return $record->valor;
    }

    public function get_dias_vigencia_estandar_cotizacion()
    {
        $query = $this->db->get_where('configuraciones', array('clave' => 'dias_vigencia_estandar_cotizacion'), 1);
        $record = $query->row();

        return $record->valor;
    }

    public function get_emails_cotizacion_aprobada()
    {
        $result = $this->get_by(array("clave" => "emails_cotizacion_aprobada"));
        if($result){
            return $result->valor;
        } else {
            return null;
        }

    }

}