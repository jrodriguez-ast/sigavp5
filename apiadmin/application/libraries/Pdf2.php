<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once APPPATH . '/third_party/vendor/tecnick.com/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{

    function __construct()
    {
        parent::__construct();
    }

    var $show_footer = false;
    var $logo_background = '';

    public function logo_background($logo)
    {
        $this->logo_background = $logo;
    }

    //Page header
    public function Header()
    {
        if (true):
            // get the current page break margin
            $bMargin = $this->getBreakMargin();
            // get current auto-page-break mode
            $auto_page_break = $this->AutoPageBreak;
            // disable auto-page-break
            $this->SetAutoPageBreak(false, 0);
            // set bacground image
            if (!is_null($this->logo_background)) {
                $img_file = $this->logo_background;
                $this->Image($img_file, 0, 0, 210, 297, '', '', '', false, 300, '', false, false, 0);
            }
            // restore auto-page-break status
            $this->SetAutoPageBreak($auto_page_break, $bMargin);
            // set the starting point for the page content
            $this->setPageMark();
        endif;
    }

    public function show_footer($validate)
    {
        $this->show_footer = $validate;
    }

    public function Footer($text = '')
    {
        $CI = &get_instance();
        $CI->config->load('view_engine');
        //if ($this->show_footer):
        if (true):
            $this->SetFontSize(7);

            $text = $CI->config->item('app_dir');
        endif;
        $cur_y = $this->y;
        $this->SetTextColorArray($this->footer_text_color);
        //set style for cell border
        $line_width = (0.85 / $this->k);
        $this->SetLineStyle(array('width' => $line_width, 'cap' => 'butt', 'join' => 'miter', 'dash' => 0, 'color' => $this->footer_line_color));
        //print document barcode
        $barcode = $this->getBarcode();
        if (!empty($barcode)) {
            $this->Ln($line_width);
            $barcode_width = round(($this->w - $this->original_lMargin - $this->original_rMargin) / 3);
            $style = array(
                'position' => $this->rtl ? 'R' : 'L',
                'align' => $this->rtl ? 'R' : 'L',
                'stretch' => false,
                'fitwidth' => true,
                'cellfitalign' => '',
                'border' => false,
                'padding' => 0,
                'fgcolor' => array(0, 0, 0),
                'bgcolor' => false,
                'text' => false
            );
            $this->write1DBarcode($barcode, 'C128', '', $cur_y + $line_width, '', (($this->footer_margin / 3) - $line_width), 0.3, $style, '');
        }
        $w_page = isset($this->l['w_page']) ? $this->l['w_page'] . ' ' : '';
        if (empty($this->pagegroups)) {
            $pagenumtxt = $w_page . $this->getAliasNumPage() . ' / ' . $this->getAliasNbPages();
        } else {
            $pagenumtxt = $w_page . $this->getPageNumGroupAlias() . ' / ' . $this->getPageGroupAlias();
        }
        $this->SetY($cur_y);
        //Print page number
        if ($this->getRTL()) {
            $this->SetX($this->original_rMargin);
            $this->Cell(50, 5, 'hola1', 'T', 0, 'M');
            $this->Cell(0, 0, $pagenumtxt, 'T', 0, 'L');
        } else {

            $this->SetX($this->original_lMargin);
            /*
           <?php echo $this->lang->line('PdfCot_Nota_Line_1'); ?>
                           &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_2'); ?><br/>
                           &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_3'); ?><br/>
                           &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_4'); ?><br/>
                           &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_5'); ?><br/>
                           &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_6'); ?><br/>
                           &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_7'); ?><br/>
                           &nbsp;<?php echo $this->lang->line('PdfCot_Nota_Line_8'); ?>
                           */
            $this->Cell(170, 0, $text, 'T', 0, 'R');
            $this->Cell(20, 0, $this->getAliasRightShift() . $pagenumtxt, 'T', 0, 'R');
        }
    }


    public function LoadData($file)
    {
        // Read file lines
        $lines = file($file);
        $data = array();
        foreach ($lines as $line) {
            $data[] = explode(';', chop($line));
        }
        return $data;
    }

    public function ColoredTable($header, $data, $info = array(), $tipo = '')
    {//funcion para generar la tabla del reporte inteligente

        switch ($tipo) {
            case 'cotizacion':
                $this->_printCotizacion($header, $data, $info);
                break;
            case 'backup':
                $this->_printCotizacionBackup($header, $data, $info);
                break;
            case 'instrumento':
                $this->_printCotizacionInstrumento($header, $data, $info);
                break;
            case 'cotizacion_sustituida':
                $this->_printCotizacionCotizacionSustituida($header, $data, $info);
                break;
            case 'despacho_materiales':
                $this->_printDespacho($header, $data, $info);
                break;
            case 'despacho_materiales_preview':
                $this->_printDespacho($header, $data, $info);
                break;
            case 'despacho_backup':
                $this->_printDespachoBackup($header, $data, $info);
                break;
            case 'despacho_backup_preview':
                $this->_printDespachoBackup($header, $data, $info);
                break;
            case 'despacho_almacen':
                $this->_printDespachoAlmacen($header, $data, $info);
                break;
            case 'despacho_instrumento':
                $this->_printDespachoInstrumento($header, $data, $info);
                break;
            case 'despacho_instrumento_preview':
                $this->_printDespachoInstrumento($header, $data, $info);
                break;
            case 'devolucion_materiales':
                $this->_printDevolucion($header, $data, $info);
                break;
            case 'devolucion_backup':
                $this->_printDevolucionBackup($header, $data, $info);
                break;
            case 'devolucion_almacen':
                $this->_printDevolucionAlmacen($header, $data, $info);
                break;
            case 'devolucion_instrumento':
                $this->_printDevolucionInstrumento($header, $data, $info);
                break;
            case 'pre-factura':
                $this->_printPreFactura($header, $data, $info);
                break;
            case 'cotizacion_free':
                $this->_printCotizacionFree($header, $data, $info);
                break;
            case 'despacho_materiales_free':
                $this->_printDespachoFree($header, $data, $info);
                break;
            default:
                $this->_printCotizacion($header, $data, $info);
                break;
        }

        return;


    }

    protected function _printDespachoHeader($header, $data, $info)
    {

        $CI = &get_instance();

        $i = $this->MultiCell(65, 0, '<b>' . $CI->lang->line('PdfCot_Paciente') . '</b>', 'LTR', 'L', false, 0, '', '', false, 0, true);
        $i = $this->MultiCell(30, 0, '<b>' . $CI->lang->line('PdfCot_Rif') . '</b>', 'RT', 'L', false, 0, '', '', false, 0, true);

        $this->Cell(45, 0, $CI->lang->line('PdfDes_NroDes'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['codigo_orden'], 0, 0, 'R', 0);
        $this->Ln();

        $this->Cell(65, 0, $info['cliente'], "LR", 0, 'r', 0);
        $this->Cell(30, 0, $info['rif'], "R", 0, 'r', 0);
        $this->Cell(45, 0, $CI->lang->line('PdfCot_NroCot'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['nro_cotizacion'], 0, 0, 'R', 0);
        $this->Ln();

        $i = $this->MultiCell(95, 8, '<b>' . $CI->lang->line('PdfCot_Seg') . ':</b>' . $info['seguro'], "LRT", 'L', false, 0, '', '', false, 0, true);
        $i = $this->MultiCell(45, 8, $CI->lang->line('PdfCot_FecEmi') . '<br/>' . $CI->lang->line('PdfCot_Vend'), "R", 'R', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(55, 8, $info['fecha_emision'] . '<br/>' . $info['vendedor'], 0, 'R', false, 0, '', '', true, 0, true);
        $this->Ln();

        $i = $this->MultiCell(95, 12, '<b>' . $CI->lang->line('PdfCot_Dir_Client') . ':</b>' . $info['direccion'], $border = "TLR", $align = 'J', $fill = false, $ln = 0, $x = '', $y = '', $reseth = false, 0, $ishtml = true);
        $i = $this->MultiCell(45, 12, $CI->lang->line('PdfCot_ForPag'), "R", 'R', false, 0, '', '', true);
        $i = $this->MultiCell(55, 12, $info['forma_pago'], 0, 'R', false, 0, '', '', true);
        $this->Ln();

        $i = $this->MultiCell(95, $i, '<b>' . $CI->lang->line('PdfCot_Tlf') . ':</b>' . $info['tlf'], "TLRB", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(45, $i, $CI->lang->line('PdfCot_TiempEsp'), "R", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(55, $i, $info['tiempo_entrega'], 0, 'R', false, 0, '', '', false, 0, true);
        $this->Ln();

        $this->Cell(95, 0, '', 0, 0, 'r', 0);
        $this->Cell(45, 0, $CI->lang->line('PdfCot_MedTratant'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['dr_tratante'], 0, 0, 'R', 0);
        $this->Ln();
    }


    protected function _printDevolucionHeader($header, $data, $info)
    {

        $CI = &get_instance();

        $i = $this->MultiCell(65, 0, '<b>' . $CI->lang->line('PdfCot_Paciente') . '</b>', 'LTR', 'L', false, 0, '', '', false, 0, true);
        $i = $this->MultiCell(30, 0, '<b>' . $CI->lang->line('PdfCot_Rif') . '</b>', 'RT', 'L', false, 0, '', '', false, 0, true);
        $this->Cell(45, 0, $CI->lang->line('PdfDev_NroDev'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['codigo_orden'], 0, 0, 'R', 0);
        $this->Ln();


        $this->MultiCell(65, 8, $info['cliente'], "LR", 'LR', false, 0, '', '', false, 0, true);
        $this->MultiCell(30, 8, $info['rif'], "R", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(45, 8, $CI->lang->line('PdfDes_NroDes') . '<br/>' . $CI->lang->line('PdfCot_NroCot'), "R", 'R', false, 0, '', '', true, 0, true);
        $this->MultiCell(55, 8, $info['despacho'] . '<br/>' . $info['nro_cotizacion'], 0, 'R', false, 0, '', '', true, 0, true);

        $this->Ln();

        $i = $this->MultiCell(95, 8, '<b>' . $CI->lang->line('PdfCot_Seg') . ':</b>' . $info['seguro'], "LRT", 'L', false, 0, '', '', false, 0, true);
        $i = $this->MultiCell(45, 8, $CI->lang->line('PdfCot_FecEmi') . '<br/>' . $CI->lang->line('PdfCot_Vend'), "R", 'R', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(55, 8, $info['fecha_emision'] . '<br/>' . $info['vendedor'], 0, 'R', false, 0, '', '', true, 0, true);
        $this->Ln();

        $i = $this->MultiCell(95, 12, '<b>' . $CI->lang->line('PdfCot_Dir_Client') . ':</b>' . $info['direccion'], $border = "TLR", $align = 'J', $fill = false, $ln = 0, $x = '', $y = '', $reseth = false, 0, $ishtml = true);
        $i = $this->MultiCell(45, 12, $CI->lang->line('PdfCot_ForPag'), "R", 'R', false, 0, '', '', true);
        $i = $this->MultiCell(55, 12, $info['forma_pago'], 0, 'R', false, 0, '', '', true);
        $this->Ln();

        $i = $this->MultiCell(95, $i, '<b>' . $CI->lang->line('PdfCot_Tlf') . ':</b>' . $info['tlf'], "TLRB", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(45, $i, $CI->lang->line('PdfCot_TiempEsp'), "R", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(55, $i, $info['tiempo_entrega'], 0, 'R', false, 0, '', '', false, 0, true);
        $this->Ln();

        $this->Cell(95, 0, '', 0, 0, 'r', 0);
        $this->Cell(45, 0, $CI->lang->line('PdfCot_MedTratant'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['dr_tratante'], 0, 0, 'R', 0);
        $this->Ln();

    }

    protected function _printPreFacturaHeader($header, $data, $info)
    {

        $CI = &get_instance();

        $i = $this->MultiCell(65, 0, '<b>' . $CI->lang->line('PdfCot_Paciente') . '</b>', 'LTR', 'L', false, 0, '', '', false, 0, true);
        $i = $this->MultiCell(30, 0, '<b>' . $CI->lang->line('PdfCot_Rif') . '</b>', 'RT', 'L', false, 0, '', '', false, 0, true);
        $this->Cell(45, 0, $CI->lang->line('PdfPre_NroFac'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['nro_pre_factura'], 0, 0, 'R', 0);
        $this->Ln();


        $this->Cell(65, 0, $info['cliente'], "LR", 0, 'r', 0);
        $this->Cell(30, 0, $info['rif'], "R", 0, 'r', 0);
        $this->Cell(45, 0, $CI->lang->line('PdfDev_NroDev'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['nro_devolucion'], 0, 0, 'R', 0);
        $this->Ln();
        $this->Cell(65, 0, "", "LR", 0, 'r', 0);
        $this->Cell(30, 0, "", "R", 0, 'r', 0);
        $this->Cell(45, 0, $CI->lang->line('PdfDes_NroDes'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['nro_despacho'], 0, 0, 'R', 0);
        $this->Ln();

        $i = $this->MultiCell(95, 8, '<b>' . $CI->lang->line('PdfCot_Seg') . ':</b>' . $info['seguro'], "LRT", 'L', false, 0, '', '', false, 0, true);
        $i = $this->MultiCell(45, 8, $CI->lang->line('PdfCot_NroCot') . '<br/>' . $CI->lang->line('PdfCot_FecEmi'), "R", 'R', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(55, 8, $info['nro_cotizacion'] . '<br/>' . $info['fecha_emision'], 0, 'R', false, 0, '', '', true, 0, true);
        $this->Ln();

        $i = $this->MultiCell(95, 12, '<b>' . $CI->lang->line('PdfCot_Dir_Client') . ':</b>' . $info['direccion'], $border = "TLR", $align = 'J', $fill = false, $ln = 0, $x = '', $y = '', $reseth = false, 0, $ishtml = true);
        $i = $this->MultiCell(45, 12, $CI->lang->line('PdfCot_ForPag'), "R", 'R', false, 0, '', '', true);
        $i = $this->MultiCell(55, 12, $info['forma_pago'], 0, 'R', false, 0, '', '', true);
        $this->Ln();

        $i = $this->MultiCell(95, $i, '<b>' . $CI->lang->line('PdfCot_Tlf') . ':</b>' . $info['tlf'], "TLRB", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(45, $i, $CI->lang->line('PdfCot_Vend'), "R", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(55, $i, $info['vendedor'], 0, 'R', false, 0, '', '', false, 0, true);
        $this->Ln();


        $this->Cell(95, 0, '', 0, 0, 'r', 0);
        $this->Cell(45, 0, $CI->lang->line('PdfCot_TiempEsp'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['tiempo_entrega'], 0, 0, 'R', 0);
        $this->Ln();
        $this->Cell(95, 0, '', 0, 0, 'r', 0);
        $this->Cell(45, 0, $CI->lang->line('PdfCot_MedTratant'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['dr_tratante'], 0, 0, 'R', 0);
        $this->Ln();
    }

    protected function _printCotizacionHeader($header, $data, $info)
    {

        $CI = &get_instance();

        $this->MultiCell(65, 0, '<b>' . $CI->lang->line('PdfCot_Paciente') . '</b>', 'LTR', 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(30, 0, '<b>' . $CI->lang->line('PdfCot_Rif') . '</b>', 'RT', 'L', false, 0, '', '', false, 0, true);
        $this->Cell(45, 0, $CI->lang->line('PdfCot_NroCot'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['nro_cotizacion'], 0, 0, 'R', 0);
        $this->Ln();

        $this->Cell(65, 0, $info['cliente'], "LR", 0, 'r', 0);
        $this->Cell(30, 0, $info['rif'], "R", 0, 'r', 0);
        $this->Cell(45, 0, $CI->lang->line('PdfCot_FecEmi'), "R", 0, 'R', 0);
        $this->Cell(55, 0, $info['fecha_emision'], 0, 0, 'R', 0);
        $this->Ln();

        $i = $this->MultiCell(95, 8, '<b>' . $CI->lang->line('PdfCot_Seg') . ':</b>' . $info['seguro'], "LRT", 'L', false, 0, '', '', false, 0, true);
        $i = $this->MultiCell(45, 8, $CI->lang->line('PdfCot_Vend') . '<br/>' . $CI->lang->line('PdfCot_MedTratant'), "R", 'R', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(55, 8, $info['vendedor'] . '<br/>' . $info['dr_tratante'], 0, 'R', false, 0, '', '', true, 0, true);
        $this->Ln();

        $i = $this->MultiCell(95, 12, '<b>' . $CI->lang->line('PdfCot_Dir_Client') . ':</b>' . $info['direccion'], $border = "TLR", $align = 'J', $fill = false, $ln = 0, $x = '', $y = '', $reseth = false, 0, $ishtml = true);
        $i = $this->MultiCell(45, 12, $CI->lang->line('PdfCot_ForPag'), "R", 'R', false, 0, '', '', true);
        $i = $this->MultiCell(55, 12, $info['forma_pago'], 0, 'R', false, 0, '', '', true);
        $this->Ln();

        $i = $this->MultiCell(95, $i, '<b>' . $CI->lang->line('PdfCot_Tlf') . ':</b>' . $info['tlf'], "TLRB", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(45, $i, $CI->lang->line('PdfCot_TiempEsp'), "R", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(55, $i, $info['tiempo_entrega'], 0, 'R', false, 0, '', '', false, 0, true);
        $this->Ln();

    }

    protected function _printCotizacionFreeHeader($header, $data, $info)
    {

        $CI = &get_instance();


        $i = $this->MultiCell(55, 8, '<b>' . $CI->lang->line('PdfCot_Client') . '</b>' . $info['cliente'], $border = "TLR", $align = 'J', $fill = false, $ln = 0, $x = '', $y = '', $reseth = false, 0, $ishtml = true);
        $this->MultiCell(40, 8, '<b>' . $CI->lang->line('free_PdfOrd_ced_Rif') . ':</b>' . $info['ced_rif'] . '</b>', 'LTR', 'L', false, 0, '', '', false, 0, true);
        $i = $this->MultiCell(45, 8, $CI->lang->line('PdfDes_NroDes') . '<br/>' . $CI->lang->line('PdfCot_NroCot'), "R", 'R', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(55, 8, $info['codigo_orden'] . '<br/>' . $info['nro_cotizacion'], 0, 'R', false, 0, '', '', true, 0, true);
        $this->Ln();

        $i = $this->MultiCell(95, 12, '<b>' . $CI->lang->line('PdfCot_Dir_Client') . ':</b>' . $info['direccion'], $border = "TLR", $align = 'J', $fill = false, $ln = 0, $x = '', $y = '', $reseth = false, 0, $ishtml = true);
        $i = $this->MultiCell(45, 12, $CI->lang->line('PdfCot_FecEmi') . '<br/>' . $CI->lang->line('PdfCot_Vend'), "R", 'R', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(55, 12, $info['fecha_emision'] . '<br/>' . $info['vendedor'], 0, 'R', false, 0, '', '', true, 0, true);
        $this->Ln();

        $i = $this->MultiCell(95, $i, '<b>' . $CI->lang->line('PdfCot_Tlf') . ':</b>' . $info['tlf'], "TLRB", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(45, $i, $CI->lang->line('PdfCot_TiempEsp'), "R", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(55, $i, $info['tiempo_entrega'], 0, 'R', false, 0, '', '', false, 0, true);
        $this->Ln();
        $this->Ln();

    }

    protected function _printDespachoFreeHeader($header, $data, $info)
    {

        $CI = &get_instance();


        $i = $this->MultiCell(95, 8, '<b>' . $CI->lang->line('PdfCot_Client') . ':</b>' . $info['cliente'], $border = "TLR", $align = 'J', $fill = false, $ln = 0, $x = '', $y = '', $reseth = false, 0, $ishtml = true);
        $i = $this->MultiCell(45, 8, $CI->lang->line('PdfCot_NroCot') . '<br/>' . $CI->lang->line('PdfCot_FecEmi'), "R", 'R', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(55, 8, $info['nro_cotizacion'] . '<br/>' . $info['fecha_emision'], 0, 'R', false, 0, '', '', true, 0, true);
        $this->Ln();

        $i = $this->MultiCell(95, 12, '<b>' . $CI->lang->line('PdfCot_Dir_Client') . ':</b>' . $info['direccion'], $border = "TLR", $align = 'J', $fill = false, $ln = 0, $x = '', $y = '', $reseth = false, 0, $ishtml = true);
        $i = $this->MultiCell(45, 12, $CI->lang->line('PdfCot_Vend') . '<br/>' . $CI->lang->line('PdfCot_TiempEsp'), "R", 'R', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(55, 12, $info['vendedor'] . '<br/>' . $info['tiempo_entrega'], 0, 'R', false, 0, '', '', true, 0, true);
        $this->Ln();

        $i = $this->MultiCell(95, $i, '<b>' . $CI->lang->line('PdfCot_Tlf') . ':</b>' . $info['tlf'], "TLRB", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(45, $i, "", "R", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(55, $i, "", 0, 'R', false, 0, '', '', false, 0, true);
        $this->Ln();
        $this->Ln();

    }

    protected function _printCotizacion($header, $data, $info)
    {
        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);

        $n_page = 12;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);
            /*
                * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(100, 0, '<h1><b>COTIZACIÓN</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(12);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printCotizacionHeader($header, $data, $info);
            $this->Ln(2);
            //--

            $fill = 0;
            $w = array(15, 30, 10, 80, 30, 30);
            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;

            $count_m = 1;

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[2], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'T', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[4], 'L', 0, 'L', $fill, '');
                $this->Cell($w[5], $heightCell, $row[5], 'RL', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    $this->Ln();
                    $this->_printImportante();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[5], $heightCell, "", 'RL', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }

        $this->MultiCell(60, 6, '<h5><b><u>' . $CI->lang->line('PdfCot_Valid_Line_1') . '</u></b></h5>', "LT", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(60, 4, '<h5><b><u>' . $CI->lang->line('PdfCot_Valid_Line_2') . '</u></b></h5>', "RT", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(40, 4, $CI->lang->line('PdfCot_MontExent'), "LRT", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(35, 4, $info['monto_exento'], "LRT", 'R', false, 0, '', '', false, 0, true);
        $this->Ln();

        $this->MultiCell(60, 6, '<h5><b><u>' . $CI->lang->line('PdfCot_EmitCheq_Line_1') . '</u></b></h5>', "L", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(60, 4, '<h5><b><u>' . $CI->lang->line('PdfCot_EmitCheq_Line_2') . '</u></b></h5>', "R", 'L', false, 0, '', '', false, 0, true);
        $this->MultiCell(40, 4, $CI->lang->line('PdfCot_MontGrav'), "LR", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(35, 4, $info['monto_gravable'], "LR", 'R', false, 0, '', '', false, 0, true);
        $this->Ln();

        $this->MultiCell(120, 0, "", "LB", 'L', false, 0, '', '', false, 0);
        $this->MultiCell(40, 0, $info['porcentaje_descuento'] . $CI->lang->line('PdfCot_Desc'), "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(35, 0, $info['calculo_descuento'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $this->MultiCell(120, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $this->MultiCell(40, 0, $info['p_recargo'] . ' ' . $CI->lang->line('PdfCot_Recarg'), "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(35, 0, $info['recargo'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $this->MultiCell(120, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $this->MultiCell(40, 0, $CI->lang->line('PdfCot_Iva') . ' ' . $info['porcentaje_iva'] . ' % ', "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(35, 0, $info['calculo_iva'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $this->MultiCell(120, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $this->MultiCell(40, 0, '<b>' . $CI->lang->line('PdfCot_NetTotal') . '</b>', "LRBT", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(35, 0, $info['neto'], "LRBT", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $this->Ln();
        $this->_printImportante();
    }

    protected function _printCotizacionFree($header, $data, $info)
    {
        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);

        $n_page = 12;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);
            /*
                * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(100, 0, '<h1><b>COTIZACIÓN</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(12);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printCotizacionFreeHeader($header, $data, $info);
            $this->Ln(2);
            //--

            $fill = 0;
            $w = array(15, 30, 10, 80, 30, 30);
            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;

            $count_m = 1;


            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[2], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'T', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[4], 'L', 0, 'L', $fill, '');
                $this->Cell($w[5], $heightCell, $row[5], 'RL', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    $this->Ln();
                    $this->_printImportante();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[5], $heightCell, "", 'RL', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }

        $this->MultiCell(60, 0, $CI->lang->line('PdfCot_Valid_Line_1'), "LT", 'L', false, 0, '', '', true, 0, true);
        $this->MultiCell(65, 0, $CI->lang->line('PdfCot_Valid_Line_2'), "RT", 'L', false, 0, '', '', true, 0, true);
        $this->MultiCell(40, 0, $CI->lang->line('PdfCot_MontExent'), "LRT", 'R', false, 0, '', '', false);
        $this->MultiCell(30, 0, $info['monto_exento'], "LRT", 'R', false, 0, '', '', false);
        $this->Ln();

        $this->MultiCell(60, 0, $CI->lang->line('PdfCot_EmitCheq_Line_1'), "LB", 'L', false, 0, '', '', true, 0);
        $this->MultiCell(65, 0, $CI->lang->line('PdfCot_EmitCheq_Line_2'), "RB", 'L', false, 0, '', '', true, 0);
        $this->MultiCell(40, 0, $CI->lang->line('PdfCot_MontGrav'), "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(30, 0, $info['monto_gravable'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();

        $this->MultiCell(125, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $this->MultiCell(40, 0, $info['porcentaje_descuento'] . $CI->lang->line('PdfCot_Desc'), "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(30, 0, $info['calculo_descuento'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $this->MultiCell(125, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $this->MultiCell(40, 0, $info['p_recargo'] . ' ' . $CI->lang->line('PdfCot_Recarg'), "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(30, 0, $info['recargo'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $this->MultiCell(125, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $this->MultiCell(40, 0, $CI->lang->line('PdfCot_Iva') . ' ' . $info['porcentaje_iva'] . ' % ', "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(30, 0, $info['calculo_iva'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $this->MultiCell(125, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $this->MultiCell(40, 0, '<b>' . $CI->lang->line('PdfCot_NetTotal') . '</b>', "LRB", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(30, 0, $info['neto'], "LRB", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $this->Ln();
        $this->_printImportante();
    }

    /**
     *
     */
    protected function _printImportante()
    {
        $CI = &get_instance();
        /*
        * Script para colocar las advertencias
        */
        $this->SetFont('freemono', '', 5);
        $this->MultiCell(190, 0, $CI->lang->line('PdfCot_Nota_Line_1'), 0, 'L', false, 0, '', '', true, 0, true);
        $this->Ln();
        $this->MultiCell(190, 0, $CI->lang->line('PdfCot_Nota_Line_2'), 0, 'L', false, 0, '', '', true, 0, true);
        $this->Ln();
        $this->MultiCell(190, 0, $CI->lang->line('PdfCot_Nota_Line_3'), 0, 'L', false, 0, '', '', true, 0, true);
        $this->Ln();
        $this->MultiCell(190, 0, $CI->lang->line('PdfCot_Nota_Line_4'), 0, 'L', false, 0, '', '', true, 0, true);
        $this->Ln();
        $this->MultiCell(190, 0, $CI->lang->line('PdfCot_Nota_Line_5'), 0, 'L', false, 0, '', '', true, 0, true);
        $this->Ln();
        $this->MultiCell(190, 0, $CI->lang->line('PdfCot_Nota_Line_6'), 0, 'L', false, 0, '', '', true, 0, true);
        $this->Ln();
        $this->MultiCell(190, 0, $CI->lang->line('PdfCot_Nota_Line_7'), 0, 'L', false, 0, '', '', true, 0, true);
        $this->Ln();
        $this->MultiCell(190, 0, $CI->lang->line('PdfCot_Nota_Line_8'), 0, 'L', false, 0, '', '', true, 0, true);
        $this->Ln();
        //-------------------
    }

    protected function _printPreFactura($header, $data, $info)
    {
        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>PRE-FACTURA</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(12);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printPreFacturaHeader($header, $data, $info);
            $this->Ln(2);
            //--

            $fill = 0;
            $w = array(15, 30, 10, 80, 30, 30);
            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;


            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[2], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'T', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[4], 'L', 0, 'L', $fill, '');
                $this->Cell($w[5], $heightCell, $row[5], 'RL', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();

                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($i = 0; $i < $r_data; $i++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[5], $heightCell, "", 'RL', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }

        $i = $this->MultiCell(125, 0, "", "T", 'L', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(40, 0, $CI->lang->line('PdfCot_MontExent'), "LRT", 'R', false, 0, '', '', false);
        $i = $this->MultiCell(30, 0, $info['monto_exento'], "LRT", 'R', false, 0, '', '', false);
        $this->Ln();

        $i = $this->MultiCell(125, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $i = $this->MultiCell(40, 0, $CI->lang->line('PdfCot_MontGrav'), "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(30, 0, $info['monto_gravable'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();

        $i = $this->MultiCell(125, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $i = $this->MultiCell(40, 0, $info['porcentaje_descuento'] . $CI->lang->line('PdfCot_Desc'), "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(30, 0, $info['calculo_descuento'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $i = $this->MultiCell(125, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $i = $this->MultiCell(40, 0, $info['p_recargo'] . ' ' . $CI->lang->line('PdfCot_Recarg'), "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(30, 0, $info['recargo'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $i = $this->MultiCell(125, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $i = $this->MultiCell(40, 0, $CI->lang->line('PdfCot_Iva') . ' ' . $info['porcentaje_iva'] . ' % ', "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(30, 0, $info['calculo_iva'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $i = $this->MultiCell(125, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $i = $this->MultiCell(40, 0, '<b>' . $CI->lang->line('PdfCot_NetTotal') . '</b>', "LRB", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(30, 0, $info['neto'], "LRB", 'R', false, 0, '', '', false, 0);
        $this->Ln();


    }

    protected function _printCotizacionBackup($header, $data, $info)
    {
        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);

        $n_page = 12;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);
            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>BACKUP</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(12);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printCotizacionHeader($header, $data, $info);
            $this->Ln(2);
            //--

            $fill = 0;
            $w = array(15, 30, 10, 80, 30, 30);
            $num_headers = count($header);


            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;

            $count_m = 1;

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[2], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'T', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[4], 'L', 0, 'L', $fill, '');
                $this->Cell($w[5], $heightCell, $row[5], 'RL', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[5], $heightCell, "", 'RL', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }

        }
        $i = $this->MultiCell(120, 0, "", "T", 'L', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(40, 0, $CI->lang->line('PdfCot_MontSubTotal_backup'), "LRT", 'R', false, 0, '', '', false);
        $i = $this->MultiCell(35, 0, $info['monto_sub_total'], "LRT", 'R', false, 0, '', '', false);
        $this->Ln();

        $i = $this->MultiCell(120, 0, "", 0, 'L', false, 0, '', '', true, 0, true);
        $i = $this->MultiCell(40, 0, $CI->lang->line('PdfCot_Iva_backup') . ' ' . $CI->config->item('valor_iva') . ' % ', "LR", 'R', false, 0, '', '', false, 0);
        $this->MultiCell(35, 0, $info['calculo_iva'], "LR", 'R', false, 0, '', '', false, 0);
        $this->Ln();
        $i = $this->MultiCell(120, 0, "", 0, 'L', false, 0, '', '', true, 0);
        $i = $this->MultiCell(40, 0, '<b>' . $CI->lang->line('PdfCot_MontTotal_backup') . '</b>', "LRB", 'R', false, 0, '', '', false, 0, true);
        $this->MultiCell(35, 0, $info['monto_total'], "LRB", 'R', false, 0, '', '', false, 0);
        $this->Ln();
    }

    protected function _printCotizacionInstrumento($header, $data, $info)
    {
        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 12;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>INSTRUMENTAL</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(12);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printCotizacionHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(15, 30, 140, 10);
            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[2], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[3], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }

        $i = $this->MultiCell(195, 0, "", "T", 'L', false, 0, '', '', true, 0, true);
        $this->Ln();
    }

    /**
     *
     * @param unknown $header
     * @param unknown $data
     * @param unknown $info
     */
    protected function _printCotizacionCotizacionSustituida($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 16;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(100, 0, '<h1><b>Nota de Entrega de Material Sustituidos</b></h1>' . $pages, 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printCotizacionHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(15, 30, 140, 10);
            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {

                if ($row[6] != $tipo) {
                    $this->MultiCell($w[0] + $w[1], 1, "<b>" . $row[6] . "</b>", 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->MultiCell($w[2], 1, "---", 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->MultiCell($w[3], 1, "---", 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->Ln();
                    $tipo = $row[6];
                }
                $this->Cell($w[0], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[2], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[3], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, 0, "", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }

                $count_m++;

            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }

        $this->Cell(40, 4, $CI->lang->line('PdfCot_Client'), 'TLR', 0, 'L', $fill, '');
        $this->Cell(85, 4, $info['cliente'], 'TR', 0, 'L', $fill, '');
        $this->Cell(60, 4, "Total Articulos", "T", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "T", 0, 'L', $fill, '');
        $this->Ln();

        $this->MultiCell(40, 8, $CI->lang->line('PdfOrd_Clinica'), 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(85, 8, $info['centro_salud'], 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Fecha_Cirugia'), 'LR', 0, 'L', $fill, '');
        $this->Cell(85, 4, $info['fecha_cirugia'], 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Instrumentista'), 'BLR', 0, 'L', $fill, '');
        $this->Cell(85, 4, $info['hora_cirugia'], 'BR', 0, 'L', $fill, '');
        $this->Ln();

        $this->Ln();
    }

    protected function _printDespacho($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>ORDEN DEDESPACHO(Materiales)</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printDespachoHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(25, 25, 40, 95, 10);

            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[6], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }

        }

        $this->Cell(40, 4, $CI->lang->line('PdfCot_Client'), 'TLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['cliente'], 'TR', 0, 'L', $fill, '');
        $this->Cell(80, 4, "Materiales Despachados", "T", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "T", 0, 'L', $fill, '');
        $this->Ln();

        $this->MultiCell(40, 8, $CI->lang->line('PdfOrd_Clinica'), 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 8, $info['centro_salud'], 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(30, 8, "Recibi conforme:", 0, 0, 'R', $fill, '');

        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Fecha_Cirugia'), 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['fecha_cirugia'], 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Hora_Cirugia'), 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['hora_cirugia'], 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Instrumentista'), 'BLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['instrumentista'], 'BR', 0, 'L', $fill, '');
        $this->Cell(30, 4, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(60, 4, "", "B", 'L', false, 0, '', '', true, 0, true);
        $this->Ln();

        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Nombre de Recibido", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();
        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Fecha/Hora:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();
        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Firma:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();

        $this->Ln();
    }

    protected function _printDespachoFree($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);

            $this->MultiCell(95, 0, '<h1><b>' . $CI->lang->line('free_PdfOrd_Title_Mat') . '</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printCotizacionFreeHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(25, 25, 40, 95, 10);

            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[6], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }

        }

        $this->Cell(40, 4, $CI->lang->line('PdfCot_Client'), 'TLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['cliente'], 'TR', 0, 'L', $fill, '');
        $this->Cell(80, 4, "Materiales Despachados", "T", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "T", 0, 'L', $fill, '');
        $this->Ln();

        $this->MultiCell(40, 8, "", 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 8, "", 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(30, 8, "Recibi conforme:", 0, 0, 'R', $fill, '');

        $this->Ln();

        $this->Cell(40, 4, "", 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, "", 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, "", 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, "", 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, "", 'BLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, "", 'BR', 0, 'L', $fill, '');
        $this->Cell(30, 4, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(60, 4, "", "B", 'L', false, 0, '', '', true, 0, true);
        $this->Ln();

        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Nombre de Recibido", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();
        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Fecha/Hora:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();
        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Firma:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();

        $this->Ln();
    }

    protected function _printDevolucion($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>ORDEN DEVOLUCION(Materiales)</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printDevolucionHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(25, 25, 40, 95, 10);

            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[4], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }

        $this->Cell(105, 4, "", 'T', 0, 'L', $fill, '');
        $this->Cell(80, 4, "Total Materiales Devueltos", "TRBL", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "TRB", 0, 'L', $fill, '');
        $this->Ln();
    }

    protected function _printDevolucionAlmacen($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>ORDEN DEVOLUCION (Almacen) CONSOLIDADO</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printDevolucionHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(25, 25, 40, 95, 10);

            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {
                if (!($row[5] == $tipo)) {
                    $this->MultiCell($w[0] + $w[1], 1, "<b>" . $row[5] . "</b>", 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->MultiCell($w[2], 1, "---", 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->MultiCell($w[3], 1, "---", 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->MultiCell($w[4], 1, "---", 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->Ln();
                    $tipo = $row[5];
                }
                $this->Cell($w[0], $heightCell, $row[4], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }
                $count_m++;
            }

            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }
        $this->Cell(105, 4, "", 'T', 0, 'L', $fill, '');
        $this->Cell(80, 4, "Total Articulos Devueltos", "TRBL", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "TRB", 0, 'L', $fill, '');
        $this->Ln();
    }

    protected function _printDevolucionInstrumento($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>ORDEN DEVOLUCION(Instrumental)</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printDevolucionHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(25, 25, 40, 95, 10);

            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[4], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }
        $this->Cell(105, 4, "", 'T', 0, 'L', $fill, '');
        $this->Cell(80, 4, "Total Instumentos Devueltos", "TRBL", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "TRB", 0, 'L', $fill, '');
        $this->Ln();
    }

    protected function _printDevolucionBackup($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>ORDEN DEVOLUCION(Backup)</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printDevolucionHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(25, 25, 40, 95, 10);

            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[4], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }
        $this->Cell(105, 4, "", 'T', 0, 'L', $fill, '');
        $this->Cell(80, 4, "Total Backup Devuelto", "TRBL", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "TRB", 0, 'L', $fill, '');
        $this->Ln();
    }

    protected function _printDespachoBackup($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(100, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>ORDEN DEDESPACHO(Backup)</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printDespachoHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(25, 25, 40, 95, 10);

            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[6], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }

        $this->Cell(40, 4, $CI->lang->line('PdfCot_Client'), 'TLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['cliente'], 'TR', 0, 'L', $fill, '');
        $this->Cell(80, 4, "Backup Despachados", "T", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "T", 0, 'L', $fill, '');
        $this->Ln();

        $this->MultiCell(40, 8, $CI->lang->line('PdfOrd_Clinica'), 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 8, $info['centro_salud'], 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(30, 8, "Recibi conforme:", 0, 0, 'R', $fill, '');

        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Fecha_Cirugia'), 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['fecha_cirugia'], 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Hora_Cirugia'), 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['hora_cirugia'], 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Instrumentista'), 'BLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['instrumentista'], 'BR', 0, 'L', $fill, '');
        $this->Cell(30, 4, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(60, 4, "", "B", 'L', false, 0, '', '', true, 0, true);
        $this->Ln();

        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Nombre de Recibido", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();
        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Fecha/Hora:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();
        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Firma:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();

    }

    protected function _printDespachoInstrumento($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(100, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>ORDEN DEDESPACHO(Instrumental)</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printDespachoHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(25, 25, 40, 95, 10);

            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {
                $this->Cell($w[0], $heightCell, $row[6], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();
                unset($data[$key]);

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }

        $this->Cell(40, 4, $CI->lang->line('PdfCot_Client'), 'TLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['cliente'], 'TR', 0, 'L', $fill, '');
        $this->Cell(80, 4, "Backup Despachados", "T", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "T", 0, 'L', $fill, '');
        $this->Ln();

        $this->MultiCell(40, 8, $CI->lang->line('PdfOrd_Clinica'), 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 8, $info['centro_salud'], 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(30, 8, "Recibi conforme:", 0, 0, 'R', $fill, '');

        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Fecha_Cirugia'), 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['fecha_cirugia'], 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Hora_Cirugia'), 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['hora_cirugia'], 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Instrumentista'), 'BLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['instrumentista'], 'BR', 0, 'L', $fill, '');
        $this->Cell(30, 4, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(60, 4, "", "B", 'L', false, 0, '', '', true, 0, true);
        $this->Ln();

        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Nombre de Recibido", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();
        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Fecha/Hora:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();
        $this->MultiCell(130, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 0, "Firma:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();

    }

    protected function _printDespachoAlmacen($header, $data, $info)
    {

        $CI = &get_instance();
        $CI->config->load('view_engine');
        $this->SetFillColor(255, 255, 255);
        $n_page = 14;
        $n_data = count($data);
        $pages = $n_data / $n_page;
        $pages = ($pages < 1 ? 1 : ceil($pages));

        for ($i = 0; $i < $pages; $i++) {
            $n_data = count($data);
            $this->SetFont('freemono', '', 8);
            $this->AddPage("P", 'LETTER');
            //Espacio para mostrar el logo en el background
            $this->Ln(16);

            /*
            * Titulo del PDF en el Header
            */
            $this->Cell(95, 0, '', 0, 0, 'r', 0);
            $this->MultiCell(95, 0, '<h1><b>ORDEN DEDESPACHO(ALMACEN)CONSOLIDADO</b></h1>', 0, 'C', false, 0, '', '', false, 0, true);
            $this->Ln(14);
            //----

            /*
            * imprimir el header que es el mismo para todos los archisvos de cotizacion
            */
            $this->_printDespachoHeader($header, $data, $info);
            $this->Ln(2);
            //--


            // Header
            $fill = 0;
            $w = array(25, 25, 40, 95, 10);

            $num_headers = count($header);
            for ($j = 0; $j < $num_headers; ++$j) {
                $this->Cell($w[$j], 7, $header[$j], 1, 0, 'C', 0);
            }
            $this->Ln();

            $heightCell = 8;
            $count_m = 1;
            $tipo = "";

            foreach ($data as $key => $row) {
                if (!($row[7] == $tipo)) {
                    $this->MultiCell($w[0] + $w[1], 1, "<b>" . $row[7] . "</b>", 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->MultiCell($w[2], 1, "---", 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->MultiCell($w[3], 1, "---", 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->MultiCell($w[4], 1, "---", 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                    $this->Ln();
                    $tipo = $row[7];
                }
                $this->Cell($w[0], $heightCell, $row[6], 'L', 0, 'L', $fill, '');
                $this->Cell($w[1], $heightCell, $row[0], 'L', 0, 'L', $fill, '');
                $this->Cell($w[2], $heightCell, $row[1], 'L', 0, 'L', $fill, '');
                $this->MultiCell($w[3], $heightCell, $row[3], 'L', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = false, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
                $this->Cell($w[4], $heightCell, $row[2], 'LR', 0, 'L', $fill, '');
                $this->Ln();

                if ($count_m == $n_page && (($i + 1) != $pages)) {
                    $this->Cell(195, $heightCell, " ", 'T', 0, 'L', $fill, '');
                    $this->Ln();
                    break;
                }
                $count_m++;
            }


            if ($n_data >= $n_page) {

            } else {
                $r_data = $n_page - $n_data;
                for ($j = 0; $j < $r_data; $j++) {

                    $this->Cell($w[0], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[1], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[2], $heightCell, "", 'L', 0, 'L', $fill, '');
                    $this->Cell($w[3], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Cell($w[4], $heightCell, "", 'LR', 0, 'L', $fill, '');
                    $this->Ln();

                }
            }
        }
        $this->Cell(40, 4, $CI->lang->line('PdfCot_Client'), 'TLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['cliente'], 'TR', 0, 'L', $fill, '');
        $this->Cell(80, 4, "Total Articulos", "T", 0, 'R', $fill, '');
        $this->Cell(10, 4, $info['suma'], "T", 0, 'L', $fill, '');
        $this->Ln();

        $this->MultiCell(40, 8, $CI->lang->line('PdfOrd_Clinica'), 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->MultiCell(65, 8, $info['centro_salud'], 'LR', 'L', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(30, 8, "", 0, 0, 'R', $fill, '');

        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Fecha_Cirugia'), 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['fecha_cirugia'], 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Hora_Cirugia'), 'LR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['hora_cirugia'], 'R', 0, 'L', $fill, '');
        $this->Ln();

        $this->Cell(40, 4, $CI->lang->line('PdfOrd_Instrumentista'), 'BLR', 0, 'L', $fill, '');
        $this->Cell(65, 4, $info['instrumentista'], 'BR', 0, 'L', $fill, '');
        $this->Cell(10, 4, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(35, 4, "", "B", 'L', false, 0, '', '', true, 0, true);
        $this->Cell(10, 4, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(35, 4, "", "B", 'L', false, 0, '', '', true, 0, true);
        $this->Ln();

        $this->MultiCell(105, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(5, 0, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(45, 0, "Nombre Transportista", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(5, 0, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(35, 0, "Nombre de Recibido", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();

        $this->MultiCell(105, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(10, 0, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(35, 0, "Fecha/Hora:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(10, 0, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(35, 0, "Fecha/Hora:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Ln();

        $this->MultiCell(105, 0, "", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(10, 0, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(35, 0, "Firma:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);
        $this->Cell(10, 0, "", 0, 0, 'L', $fill, '');
        $this->MultiCell(35, 0, "Firma:", 0, 'C', $fill, $ln = 0, $x = '', $y = '', $reseth = true, $stretch = 0, $ishtml = true, $autopadding = false, $heightCell, $valign = 'M', $fitcell = false);

        $this->Ln();

    }
//Fin de la clase
}

/* End of file Pdf2.php */
/* Location: ./application/libraries/Pdf2.php */
