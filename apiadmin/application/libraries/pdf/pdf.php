<?php
/**
 * Clase para generar el pdf de la cotización
 * User: darwin
 * Date: 05/02/15
 * Time: 02:59 PM
 */
require_once 'application/third_party/vendor/tecnick.com/tcpdf/tcpdf.php';

class Pdf extends TCPDF
{
    // Url donde se encuentran las imágenes a usar en el pdf
    private $_url_images = '/images/';

    // Url donde se encuentra la librería
    private $_url_lib = '';

    /**
     * Título del documento
     * @var string
     */
    private $_title = '';

    /**
     * Cliente al que va dirigido el documento
     * @var string
     */
    private $_client = '';

    /**
     * Empresa que genera el documento
     * @var string
     */
    private $_company = '';

    /**
     * Indíca si el documento es válido para su proceso o no.
     * @var bool
     */
    private $_no_valid = false;

    private $_print_on_first_page = false;

    function __construct()
    {
        parent::__construct();

        $this->_title = 'Sin Título';
        $this->_url_lib = dirname(__FILE__);

        $this->SetMargins(20, 30);

    }


    /**
     * Agrega un título a la cotización
     * @param $text
     */
    public function setTitle($text)
    {
        $this->_title = $text;
    }

    /**
     * Define el cliente al que va dirigida la cotización
     * @param $text
     */
    public function setClient($text)
    {
        $this->_client = $text;
    }


    /**
     * Define la compañía que genera la cotización
     * @param $text
     */
    public function setCompany($text)
    {
        $this->_company = $text;
    }

    /**
     * Método que genera la cabecera de cada hoja
     */
    public function Header()
    {
        $this->waterMark();
        $this->noValid();

        // Este Header se agrega a las páginas que no son la portada
        if ($this->PageNo() != 1) {
            // Logo
            $image_file = $this->_url_lib . $this->_url_images . 'logo.png';

            // Logo
            $this->Image($image_file, 10, 5, 86);
            $this->headerColor();

            $this->SetY(10);
            // Movernos a la derecha
            $this->Cell(30);
            // Título
            $this->Cell(0, 10, $this->_title, 0, 15, 'L');
            // Salto de línea
            $this->Ln(10);
        }
    }

    /**
     * Método que genera el pie de página de todas las páginas del documento
     */
    public function Footer()
    {
        // Este Footer se agrega a las páginas que no son la portada
        if ($this->PageNo() != 1) {
            // Logo
            $image_file = $this->_url_lib . $this->_url_images . 'footer.png';

            // Posición: a 1,5 cm del final
            $this->SetY(-18);

            // Logo
            $this->Image($image_file, 15, $this->GetY() - 3, 0, 21);
            // Times italic 8
            $this->SetFont('Times', 'I', 8);
            // Número de página
            $this->Cell(183, 10, 'Página ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, false, 'R', 0, '', 0, false, 'T', 'T');
        }

    }

    /**
     * Define el color de fondo y letras para el header
     */
    private function headerColor()
    {
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(79, 175, 75);
        // Times bold 15
        $this->SetFont('Times', 'I', 15);
    }

    /**
     * Define el color de fondo y letras para el cuerpo del documento
     */
    private function bodyColor()
    {
        $this->SetFillColor(255, 255, 255);
        $this->SetTextColor(0, 0, 0);
        $this->SetFont('Times', '', 12);
    }

    /**
     * Define color de fondo y letras para el título de la sección
     */
    private function titleSectionColor()
    {
        $this->SetFillColor(200, 220, 255);
        $this->SetTextColor(0, 0, 0);
        // Times 12
        $this->SetFont('Times', 'BI', 12);
    }

    /**
     * Agrega la página de portada del documento
     */
    public function addTitlePage()
    {

        $this->AddPage();

        $image_file = $this->_url_lib . $this->_url_images . 'logo_portada.png';
        $this->Image($image_file, 40, 70, 150);

        $this->noValid();

        $this->Ln(150);
        $this->headerColor();
        $this->SetFont('Times', 'BI', 28);
        $this->Cell(0, 0, $this->_title, 0, 1, 'R', false);
        $this->SetFont('Times', 'BI', 18);
        $this->Cell(0, 20, "Cliente: {$this->_client}", 0, 0, 'R', false);

        $this->SetY(-30);
        $this->Cell(0, 2, "{$this->_company}", 0, 0, 'R', false);


    }

    /**
     * Agrega una marca de agua a la página
     */
    private function waterMark()
    {
        if ($this->_print_on_first_page || $this->PageNo() != 1) {
            $image_file = $this->_url_lib . $this->_url_images . 'marca_agua.png';
            $this->Image($image_file, 10, 90, 180);
        }
    }

    public function setWaterMarkOnFirstPage($print = false){
        $this->_print_on_first_page = $print;
    }

    /**
     * Indíca que el documento no es válido para su ejecución
     */
    private function noValid()
    {
        if ($this->_no_valid) {
            $image_file = $this->_url_lib . $this->_url_images . 'no_valido.png';
            $this->Image($image_file, 0, 10);
        }
    }

    /**
     * Crea una sección en el documento y Arrega un título para ésta
     * @param $title
     */
    public function newSection($title = null)
    {
        // Agrega una nueva página para la sección
        $this->AddPage();
        if (!is_null($title)) {
            //  Agrega el título para la sección
            $this->addTitle($title);
        }

    }

    /**
     * Agrega un título para la sección
     * @param $title
     */
    public function addTitle($title, $bookmark = true)
    {
        // Color de fondo
        $this->titleSectionColor();

        // Se agrega un título para la sección
        $this->Cell(0, 6, $title, 0, 1, 'L', true);
        // Salto de línea
        $this->Ln(5);

        if ($bookmark) {
            $this->Bookmark($title, 0, 0, '', 'B', array(0, 64, 128));
        }
    }

    /**
     * Agrega el contenido de la sección
     * @param mixed $content Recibe el valor que hace referencia al tipo de contenido enviado.
     * @param string $type Los valores permitidos para type son
     * <ul>
     *  <li>file: Dado un archivo txt vacia el contenido en la sección</li>
     *  <li>html: Dada una estructura html simple la agrega a la sección</li>
     * </ul>
     */
    public function sectionBody($content, $type = 'file')
    {
        $this->bodyColor();
        switch (strtolower($type)) {
            case 'file':
                $this->WriteFile($content);
                break;
            case 'html':
                $this->WriteHTML($content);
                break;
        }
    }

    /**
     * Incrusta el contenido de un fichero en el documento
     * @param $file Ruta o ubicación del fichero
     */
    private function WriteFile($file)
    {
        // Leemos el fichero
        $txt = file_get_contents($file);
        // Imprimimos el texto justificado
        $this->MultiCell(0, 5, $txt);
        // Salto de línea
        $this->Ln();
    }

    /**
     * @param $flag boolean Indía si el documento debe imprimir la marca de agua No Válido
     */
    public function setNoValid($flag)
    {
        $this->_no_valid = $flag;
    }

    /**
     * Agrega una tabla de contenido al documento
     */
    public function addTableOfContent()
    {
        $this->addTOCPage();

        // write the TOC title
        $this->addTitle('Tabla de Contenido', false);

        $this->bodyColor();

        // add a simple Table Of Content at first page
        // (check the example n. 59 for the HTML version)
        $this->addTOC(2, 'Times', '.', 'INDEX', '', array(0, 0, 0));

        // end of TOC page
        $this->endTOCPage();
    }

}