<?php

if (!defined ('BASEPATH'))
    exit('No direct script access allowed');
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP 5.1.6 or newer
 *
 * @package        CodeIgniter
 * @subpackage            Libraries
 * @category        Libraries
 * @author        Nohemi Rojas<nohemir@gmail.com>
 * @version             V 1.0
 */

/**
 * CodeIgniter Acl Class
 *
 * This class is Access Control List
 */
class Acl
{

    /**
     * Nombre de usuario
     *
     * @access private
     * @var string
     */
    private $username;

    /**
     * Identificador de usuario
     *
     * @access private
     * @var integer
     */
    private $user_id;

    /**
     * Identificador del rol
     *
     * @access private
     * @var integer
     */
    private $role_id;

    /**
     * Nombre del rol
     *
     * @access private
     * @var string
     */
    private $role_name;

    /**
     * Instancia de CodeIgniter
     *
     * @access private
     * @var string
     */
    private $CI;

    /**
     * Nombre del usuario en session
     *
     * @access private
     * @var string
     */
    private $last_name;

    /**
     * Apellido del usuario en session
     *
     * @access private
     * @var string
     */
    private $first_name;
    /**
     * Id del cliente que se le asigno al usuario
     *
     * @access private
     * @var string
     */
    private $cliente_id;
    /**
     * Id del grupo al que pertenece el usuario
     *
     * @access private
     * @var string
     */
    private $group_id;
    /**
     * Nombre del grupo al que pertenece el usuario
     *
     * @access private
     * @var string
     */
    private $name;

    /**
     * Constructor
     *
     * @access    public
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function __construct ()
    {
        $this->CI = &get_instance ();
        $this->username = '';
        $this->user_id = '';
        $this->role_id = '';
        $this->role_name = '';

    }

    /**
     * Inicializa la clase, obteniendo el rol, creando las operaciones
     *
     * @access    public
     * @param array|string $user usuario actual en session
     * @param bool $redirect
     * @return bool
     * @author  Nohemi Rojas, Jefferson Lara
     * @version V-1.2 13/01/15 10:37 AM
     */
    public function init ($user = '', $redirect = true)
    {

        $this->CI->load->model ('acl_model');
        if (empty($user))

            $user = $this->CI->acl_model->get_info_user ();

        $this->username = $user['username'];
        $this->user_id = $user['user_id'];
        $this->first_name = $user['first_name'];
        $this->last_name = $user['last_name'];
        $this->email = $user['email'];
        $this->cliente_id = $user['cliente_id'];
        $this->group_id = $user['group_id'];
        $this->name = $user['name'];

        if ($this->hasRole ()) {
            $this->setPermissions ();
            $this->setMenuSidebar ();
            $this->setSession ();
            /*if($redirect)
            	$this->homePage();*/
            return true;
        } else {
            $message = 'user_no_role';
            /*if($redirect)
            	redirect("auth/logout/$message", 'refresh');*/

            return false;
        }
    }

    /**
     * Redirección a la pagina principal
     *
     * @access    public
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function homePage ()
    {
        redirect ('home', 'refresh');
    }

    /**
     * Verifica si el usuario que esta en session tiene un rol asociado
     *
     * @access    private
     * @author  Nohemi Rojas
     * @return boolean
     * @version V-1.0 21/11/12 02:13 PM
     */
    private function hasRole ()
    {
        $role = $this->CI->acl_model->getRole ($this->user_id);
        if (!$role)
            return FALSE;

        $this->setRole ($role);
        return TRUE;
    }

    /**
     * Realiza un set de los valores del rol que se encuentra en session
     *
     * @access    private
     * @param   array $role arreglo con los datos del rol
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    private function setRole ($role)
    {
        $this->role_name = $role['role_name'];
        $this->role_id = $role['role_id'];
    }

    /**
     * Realiza un set en session de los valores del usuario y su rol
     *
     * @access    private
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    private function setSession ()
    {
        $this->CI->session->set_userdata ('username', $this->username);
        $this->CI->session->set_userdata ('user_id', $this->user_id);
        $this->CI->session->set_userdata ('role_id', $this->role_id);
        $this->CI->session->set_userdata ('role_name', $this->role_name);
        $this->CI->session->set_userdata ('user_first_name', $this->first_name);
        $this->CI->session->set_userdata ('user_last_name', $this->last_name);
        $this->CI->session->set_userdata ('user_email', $this->email);
        $this->CI->session->set_userdata ('cliente_id', $this->cliente_id);
        $this->CI->session->set_userdata ('group_id', $this->group_id);
        $this->CI->session->set_userdata ('name', $this->name);
    }

    /**
     * Obtiene los permisos asociados a un rol para el usuario
     *
     * @access    private
     * @author  Nohemi Rojas
     * @version V-1.0 21/11/12 02:13 PM
     */
    private function setPermissions ()
    {
        $permissions = $this->CI->acl_model->getPermissions ($this->role_id);
        $this->CI->session->set_userdata ('permissions', $permissions);
    }

    /**
     * Verifica si el usuario en session tiene permiso de acceder a un determinado método
     *
     * @access    public
     * @param string $module Nombre del modulo
     * @param string $controller Nombre del controlador
     * @param string $method Nombre del método
     * @return mixed Identificador de la operación en caso de que exista, sino false
     * @author  Nohemi Rojas, Jose Rodriguez
     * @version V-1.0 21/11/12 02:13 PM
     */
    public function hasPermissions ($module, $controller, $method)
    {

        //Construcción de la url que entiende los permisos
        $operation_url = strtolower ($module . '/' . $controller) . '/' . $method;

        $permissions = $this->CI->session->userdata ('permissions');

        //---Inicio Excepciones

        if ($operation_url == 'home/home/index' || $operation_url == 'home/home/about' ||
            $operation_url == 'home/home/help' || $operation_url == 'home/home/profile'
        )

            return TRUE;

        $patter = '/^EXC_/';
        if (preg_match ($patter, $method))
            return TRUE;

        //---Fin Excepciones
        //Función que verifica si la url que es esta pasando se encuentra en los permisos del usuario

        $arr_permission = array();

        foreach ($permissions as $permission) {

            if ($permission['url'] == $operation_url) {
                $arr_permission['operation_url'] = $permission['url'];
                $arr_permission['operation_id'] = $permission['id'];
                $arr_permission['operation_title'] = $permission['_name'];
                $arr_permission['target_on'] = $permission['target_on'] ? $permission['target_on'] : 'content';

                break;
            }
        }
        return (isset($arr_permission['operation_id'])) ? $arr_permission : FALSE;
    }

    /**
     * Realiza un set del menu con las operaciones que el usuario puede realizar
     *
     * @access    private
     * @author  Nohemi Rojas, Jefferson Lara
     * @version V-1.2 13/01/15 10:37 AM
     */
    private function setMenuSidebar ()
    {
        $permissions = $this->CI->session->userdata ('permissions');
        $menu = array();
        if ($permissions) {
            foreach ($permissions as $permission) {
                if ($permission['render_on'] == "sidebar" and $permission['visual_type'] == "menu") {
                    $menu[$permission['id']] = array(
                        'label' => $permission['_name'],
                        'icon' => $permission['icon']
                    );
                }
            }

            foreach ($permissions as $permission) {
                if ($permission['render_on'] == "sidebar" and $permission['visual_type'] == "submenu") {
                    $menu[$permission['id_operation']]['submenu'][$permission['id']] = array('label' => $permission['_name'], 'url' => $permission['url'],);
                }
            }
        }

        $this->CI->session->set_userdata ('sidebar', $menu);
    }

}

// END Acl class
/* End of file Acl.php */
/* Location: application/libraries/Acl.php */
