<?php

/**
 * Hook que se encarga de registrar los diferentes accesos a la aplicación.
 * User: darwin
 * Date: 17/03/15
 * Time: 11:20 AM
 */
class Audit
{
    private $CI;

    public function register_access()
    {
        $this->CI = &get_instance();
        $access_data = $this->CI->auditoria_model->get_access_user_data();
        if (!preg_match('/has_permissions|store/', $access_data['url'])) {
            $record = array(
                "usuario_id" => $access_data['id_user'],
                "transaccion" => 'ACCESS',
                "info_adicional" => json_encode($access_data)
            );
            $this->CI->auditoria_model->insert_record($record);
        }
    }
}