<?php

/**
 * Clase para manejar la seguridad de los controladores
 * User: darwin
 * Date: 03/02/15
 * Time: 04:51 PM
 */
class Security
{

    private $CI;

    public function __construct()
    {
        $this->CI = &get_instance();
        $this->CI->load->library('ion_auth');
    }

    /**
     * Verifica si hay sesión, de no existir no deja ejecutar otra acción
     * sólo permite el paso del controlador auth_cotizaciones y al método login
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 03/02/15
     */
    public function check_login()
    {
        $segmento = $this->CI->uri->segment(1);
        if (!$this->CI->ion_auth->logged_in()
            && ($this->CI->uri->segment(2) != 'login'
            && $segmento != 'auth_cotizaciones')
            && $segmento != 'auth'
            && !empty($segmento)
        ) {
            $return = array(
                "success" => false,
                "messages" => 'Usted no posee session activa..!!'
            );

            print json_encode($return);
            die();
        }


    }
}