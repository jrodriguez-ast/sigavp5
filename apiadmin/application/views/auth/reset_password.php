<!DOCTYPE html>
<html lang="es" ng-app="app">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>SIGAVP</title>

    <!----------------Styles---------------------->
    <link rel="stylesheet" type="text/css" href="../../../app/js/vendor/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="../../../app/css/app.css">
    <link rel="stylesheet" type="text/css" href="../../../app/js/vendor/rdash-ui/dist/css/rdash.min.css">
    <link rel="stylesheet" type="text/css" href="../../../app/css/rdash-ast.css">
</head>
<body>
<div class="container">
    <div class="page-header">
        <h3>SIGAVP</h3>
        <h4>Sistema de Gestión Publicitaria de AVP</h4>
    </div>
    <div class="panel-body">
        <h3><?php echo lang('reset_password_heading'); ?></h3>
        <?php if ($message) : ?>
            <div id="infoMessage" class="alert alert-danger"><?php echo $message; ?></div>
        <?php endif; ?>
        <?php echo form_open('auth/reset_password/' . $code); ?>

        <div class="form-group">
            <label for="new_password" class="control-label">
                <?php echo sprintf(lang('reset_password_new_password_label'), $min_password_length); ?>
            </label>
            <?php echo form_input($new_password, '', 'class="form-control"'); ?>
        </div>

        <div class="form-group">
            <?php echo lang('reset_password_new_password_confirm_label', 'new_password_confirm'); ?> <br/>
            <?php echo form_input($new_password_confirm, '', 'class="form-control"'); ?>
        </div>

        <?php echo form_input($user_id); ?>
        <?php echo form_hidden($csrf); ?>

        <p><?php echo form_submit('submit', lang('reset_password_submit_btn'), 'class="btn btn-primary"'); ?></p>

        <?php echo form_close(); ?>
    </div>
</div>
</body>
</html>