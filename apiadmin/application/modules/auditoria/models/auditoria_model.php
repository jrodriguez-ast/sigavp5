<?php

/**
 * Modelo para administrar las transacciones de auditoría que se realizan desde la aplicación.
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 18/03/15
 */
class Auditoria_model extends My_Model
{
    protected $_table = 'acl.auditorias';

    public $access_user_data = null;

    public function __construct()
    {
        parent::__construct();
        $this->access_user_data = json_encode($this->get_access_user_data());
    }

    /**
     * Obtiene los datos de conexión al sistema
     * @return array
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 18/03/15
     */
    public function get_access_user_data()
    {
        $this->load->library('user_agent');
        $uri = $this->uri->uri_string();
        $id_user = $this->session->userdata('user_id');
        if (empty($id_user)) {
            $id_user = 0;
        }

        return array(
            'id_user' => $id_user,
            'ip' => $this->input->ip_address(),
            'access_time' => date('Y-m-d H:i:s'),
            'os' => $this->agent->platform(),
            'browser' => $this->agent->browser(),
            '_version' => $this->agent->version(),
            'url' => base_url($uri),
        );
    }

    /**
     * Inserta un registro en la tabla de auditoría
     * @param $data
     */
    public function insert_record($data)
    {
        $this->insert($data);
    }

    /**
     * Al eliminar un registro busca el registro de auditoría que se creó y actualiza el registro con el usuario de la sesión
     *
     * @param integer $record_id
     * @param integer $user_id
     * @param string $table
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 18/03/15
     */
    public function set_delete_user($record_id, $user_id, $table)
    {
        $schema = 'public';
        $schema_table = explode('.', $table);

        if (count($schema_table) > 1) {
            $schema = $schema_table[0];
            $table = $schema_table[1];
        }

        // Obtiene el registro que se insertó en la tabla de auditoría
        $query = $this->get_by(array(
            "registro_id" => $record_id,
            "tabla" => $table,
            "esquema" => $schema,
            "transaccion" => "DELETE",
            "info_adicional IS NULL"
        ));

        // Crea la estructura para el registro que se va a actualizar
        $data = array("usuario_id" => $user_id);

        // Almacena la información adicional
        $data["info_adicional"] = $this->access_user_data;

        // Actualiza en auditoría la información eliminada con el id del usuario que realizó la transacción
        // y los datos adicionales de la conexión.
        $this->update($query->id, $data);
    }

    /**
     * Actualiza los registros de auditoría referentes a transacciones de INSERT o UPDATE con la información
     * de conexión de la aplicación
     *
     * @param $record_id
     * @param $table
     * @param string $transaction
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 19/03/15
     */
    public function update_transaction($record_id, $table, $transaction = 'INSERT')
    {
        $schema = 'public';

        $schema_table = explode('.', $table);

        if (count($schema_table) > 1) {
            $schema = $schema_table[0];
            $table = $schema_table[1];
        }

        $where = array(
            "esquema" => $schema,
            "tabla" => $table,
            "registro_id" => $record_id,
            "transaccion" => $transaction,
            "info_adicional IS NULL"
        );

        $data = array(
            "info_adicional" => $this->access_user_data
        );

        $this->update_by($where, $data);
    }


    /**
     * Genera listado de datos de auditoría, permite filtros de fechas, usuario y transacción
     *
     * @param $fecha_inicio
     * @param $fecha_fin
     * @param null $usuario_id
     * @param null $transaccion
     * @return mixed
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 25/03/15
     */
    public function dt_get_listado($fecha_inicio, $fecha_fin, $filtros = array())
    {
        $filtersOnWhere = array(
            "user" => array(),
            "transaction" => array()
        );

        $this->datatables
            ->select("aud.id, " .
                "to_char(aud.fecha, 'DD/MM/YYYY HH12:MI:SS AM') as fecha, " .
                "CASE transaccion WHEN 'ACCESS' THEN 'Acceso' " .
                "WHEN 'INSERT' THEN 'Inserción de datos' " .
                "WHEN 'UPDATE' THEN 'Actualización de datos' " .
                "WHEN 'DELETE' THEN 'Borrado de datos' END as transaccion, " .
                "COALESCE(esquema || '.' || tabla, 'N/A') as tabla_transaccion, " .
                "gro.name as nombre_grupo", FALSE)
            ->from("{$this->_table} aud")
            ->join('acl.users usu', 'aud.usuario_id = usu.id')
            ->select("usu.first_name || ' ' || usu.last_name as nombre_usuario")
            ->join('acl.users_groups ugro', 'usu.id = ugro.user_id')
            ->join('acl.groups gro', 'ugro.group_id = gro.id')
            ->where("aud.fecha >= '{$fecha_inicio}'::timestamp")
            ->where("aud.fecha < ('{$fecha_fin}'::timestamp + INTERVAL '1 day')");

        if (!empty($filtros)) {
            foreach ($filtros as $filtro) {
                if ($filtro["type"]["id"] == "user") {
                    $where = "usu.id";
                } else if ($filtro["type"]["id"] == "transaction") {
                    $where = "aud.transaccion";
                } else if ($filtro["type"]["id"] == "group") {
                    $where = "ugro.group_id";
                }

                if ($filtro["compare"]["id"] == "equal") {
                    $where .= " = ";
                } else {
                    $where .= " <> ";
                }

                $where .= "'{$filtro["value"]["id"]}'";


                $filtersOnWhere[$filtro["type"]["id"]][] = $where;
            }

            foreach ($filtersOnWhere as $filters) {
                $nFilter = '';
                $sw = false;
                foreach ($filters as $filter) {
                    if (!$sw) {
                        $nFilter = $filter;
                    } else {
                        $nFilter .= " OR {$filter}";
                    }
                    $sw = true;
                }

                if (!empty($nFilter)) {
                    $this->datatables->where("({$nFilter})");
                }
            }
        }

        $data = $this->datatables->generate();
//        echo $this->db->last_query();
//        die();
        return $data;
    }

    public function get_transaccion($id)
    {
        $query = $this->db
            ->select("aud.id, aud.usuario_id, aud.esquema, aud.tabla, aud.registro_id, " .
                "to_char(aud.fecha, 'DD/MM/YYYY HH12:MI:SS AM') as fecha, " .
                "CASE transaccion WHEN 'ACCESS' THEN 'Acceso' " .
                "WHEN 'INSERT' THEN 'Inserción de datos' " .
                "WHEN 'UPDATE' THEN 'Actualización de datos' " .
                "WHEN 'DELETE' THEN 'Borrado de datos' END as descripcion_transaccion," .
                "aud.transaccion, aud.datos_previos, aud.datos_actuales, aud.info_adicional", false)
            ->from("{$this->_table} aud")
            ->join("acl.users usu", "aud.usuario_id = usu.id", 'INNER')
            ->select("usu.first_name || ' ' || usu.last_name as nombre_usuario")
            ->where("aud.id", $id)
            ->get();

        $result = $query->row_array();
        $result['datos_previos'] = json_decode($result['datos_previos'], true);
        $result['datos_actuales'] = json_decode($result['datos_actuales'], true);
        $result['info_adicional'] = json_decode($result['info_adicional'], true);
        return $result;
    }
}