<?php

/**
 * Created by PhpStorm.
 * User: darwin
 * Date: 25/03/15
 * Time: 11:09 AM
 */
class Auditoria extends REST_Controller
{
    public function dt_lista_post()
    {
        $fecha_inicio = $this->post('dateStart');
        $fecha_fin = $this->post('dateEnd');

        $filtros = ( !$this->post('filters') ) ? array() : json_decode($this->post('filters'), true);

        $data = json_decode($this->auditoria_model->dt_get_listado($fecha_inicio, $fecha_fin, $filtros), true);
        $this->response($data, 200);
    }

    public function transaccion_get()
    {
        $id = $this->get('id');
        $transaccion = $this->auditoria_model->get_transaccion($id);
        $this->response($transaccion, 200);
    }
}