<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Clase que integra la capa de negocios con la capa de presentacion de
 * cargos.
 *
 * @author  Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Soluciones A.S.T
 * @subpackage Clientes
 * @category Controller
 * @since   Version 1.0 08/01/15 21:05:32
 */
class Position extends REST_Controller {

	function __construct(){
        parent::__construct();
        $this->load->model('position_model', 'model');
    }

    /**
     * Obtiene toda la información de los cargos.
     * @return json
     * @author Jose Rodriguez
     * @version V-1.0 09/01/15 10:29:49
     */
    public function positions_get()
    {
        $positions = $this->model->get_all();
        $this->response($positions,200);
    }

    /**
     * Obtiene el listado de cargos en formao datatables
     * @author Jose Rodriguez
     * @version V-1.0 07/01/2015
     */
    public function positions_post()
    {
        $positions = $this->model->get_datatable_list();
        $positions = json_decode($positions,TRUE);
        $this->response($positions,200);
    }

        public function positions_delete()
    {
        $id =$this->uri->segment(4); 
        $response=$this->model->destroy($id);

        $this->response($response,200);
    }

    /**
     * Agrega un Cargo desde un formulario dado.
     * @author Jose Rodriguez
     * @version V-1.0 22/01/15 09:39:52
     */
    public function add_post()
    {
        $position = $this->post();
        $rs = $this->model->add($position);
        $this->response($rs, 201);
    }

    /**
     * Obtiene un Cargo, dado si ID
     * @param Integer $id ID de Cargo
     * @return JSON
     * @author Jose Rodriguez
     * @version V-1.0 08/01/15 12:20:48
     */
    public function get_get()
    {
        $id = $this->get('id');
        $response = array();
        $status = 200;

        if (empty($id)) {
        exit($id);
            throw new Exception("Valor requerido.", 1000);
            $response = array('status'=>FALSE, 'error'=>'Unknown method.');
            $status = 404;
        } elseif (!is_numeric($id)) {
            throw new Exception("Valor debe ser entero.", 1001);
            $response = array('status'=>FALSE, 'error'=>'Unknown method.');
            $status = 404;
        }

        $response = $this->model->get($id);
        $this->response($response, $status);
    }

    /**
     * Actualiza la informacion de un Cargo, conocido su ID
     * @return JSON
     * @version V-1.0 09/01/15 12:22:31
     * @author Jose Rodriguez
     */
    public function edit_post()
    {
        $position = $this->post();
        $rs = $this->model->update($position);
        $this->response($rs, 201);
    }

}
/* End of file position.php */
/* Location: ./application/modules/clientes/controllers/position.php */