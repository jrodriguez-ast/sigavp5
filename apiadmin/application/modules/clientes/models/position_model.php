<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * Clase que maneja los datos y validaciones de Cargos.
 *
 * @author  Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Soluciones A.S.T
 * @subpackage Clientes
 * @category Model
 * @since   Version 1.0 07/01/15 10:00:43
 */
class Position_model extends AST_Audit_Model {
	protected $_table = 'cargos';
    public $before_create = array( 'prepare' );
    public $before_update = array( 'prepare' );

    public $after_create = array( 'delete_cache' );
    public $after_update = array( 'delete_cache' );

    public $protected_attributes = array('id','modificado_en');

    public $validate = array(
        array( 'field' => 'descripcion',
               'label' => 'Descripción',
               'rules' => 'required' ),
    );

    function __construct(){
        parent::__construct();
    }

    /**
     * Dado un arreglo de producto, devuelve el mismo arreglo con los campos
     * automaticos de sistema para la inserción de Cargos.
     * @param  array
     * @return array
     * @author Jose Rodriguez <josearodriueze@gmail.com>
     * @version V-1.0 08/01/15 15:42:36
     */
    protected function prepare($data)
    {
      //  $data['modificado_en'] = date('Y-m-d H:i:s');
        $data['usuario_id'] = $this->session->userdata('user_id');
        return $data;
    }

    /**
     * Callback que borra la cache del modelo
     *
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.0 29/01/15 12:32:16
     */
    protected function delete_cache() {
        $this->db->cache_delete('clientes','position');
    }

    /**
     * Metodo para devolver la data para el listado de Cargos.
     *
     * @access public
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.0 14/01/15 11:40:57
     */
    public function get_datatable_list(){
        $this->datatables
            ->select('position.id')
            ->select('position.descripcion')
            ->from('cargos AS position');
            //->where('prod.activo' => TRUE);
        return $this->datatables->generate();
    }

    /**
     * Dado un arreglo de Cargo, realiza la insercion en BD de la data
     * de producto y retorna:
     *      Exito -> Un entero con el ID del Producto.
     *      Fracaso -> un String con las Validaciones faltantes.
     *
     * @param array
     * @return array mixed
     * @version V-1.0 08/01/15 17:37:09
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     */
    public function add($object)
    {
        if(empty($object)){
            $rs['error'] = TRUE;
            $rs['msg'] = 'Produto es Vacio';
            throw new Exception("Dato Vacio", 1000);
        }

        $rs['error'] = FALSE;
        $rs['msg'] = $this->insert($object);
        if (empty($rs['msg'])) {
            $rs['error'] = TRUE;
            $rs['msg'] = validation_errors();
        }
        return $rs;
    }

    /**
     * [Override] Obtiene un cargo dado un ID
     *
     * @param  integer
     * @return array
     * @version V-1.0 09/01/15 12:32:12
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @see https://github.com/jamierumbelow/codeigniter-base-model#synopsis
     */
    public function get($id)
    {
        if (empty($id)){
            throw new Exception("Dato Vacio", 1000);
        }
        return parent::get($id);
    }

    /**
     * [Override] Actualiza un cargo dado un ID
     *
     * @param  array
     * @return array
     * @version V-1.0 09/01/15 12:32:12
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @see https://github.com/jamierumbelow/codeigniter-base-model#synopsis
     */
    public function update($product)
    {
        if(empty($product) && empty($product['id'])){
            $rs['error'] = TRUE;
            $rs['msg'] = 'Produto es Vacio';
            log_message('error','Producto es vacio., Exception:1000, product_model->update:142');
            throw new Exception("Dato Vacio", 1000);
        } else {
            $rs['error'] = FALSE;
            $rs['msg'] = parent::update($product['id'], $product);
            if (empty($rs['msg'])) {
                $rs['error'] = TRUE;
                $rs['msg'] = validation_errors();
            }
        }
        return $rs;
    }

    public function destroy($id)
    {

        $result2=$this->db
        ->select('cargo_id')
        ->from('contactos_clientes')
        ->where('cargo_id', $id)
        ->limit(1)->get();

        if ($result2->num_rows() === 1) {

            $response=['status' => 'fail', 'code' => 404, 'message' => 'Este cargo tiene registros asociados.'];

        }else if ($result2->num_rows() === 0) {
            $this->db->where('id',$id);
            $result=$this->db->delete('cargos');

            if ($result) {
                $response=['status' => 'ok', 'code' => 204, 'message' => 'Cargo eliminado exitosamente.'];
            }else{
                $response=['status' => 'fail', 'code' => 404, 'message' => 'Este cargo no pudo ser eliminado.'];
            }
        }

        return $response;

        /*return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Este cargo no pudo ser eliminado.'], 200);*/
    }

}
/* End of file position_model.php */
/* Location: ./application/modules/clientes/models/position_model.php */