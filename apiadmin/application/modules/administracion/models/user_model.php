<?php

class User_model extends AST_Audit_Model
{

    protected $_table = 'acl.users';
    public $_table_user_group = 'acl.users_groups';
    public $primary_key = 'id';


    public $before_create = array('prepare');
    public $before_update = array('prepare');


    /*public $validate = array(
        array( 'field' => 'codigo',
            'label' => 'Código',
            'rules' => 'required|alpha_dash'),//|is_unique[users.email]'),

        array( 'field' => 'nombre',
            'label' => 'Nombre',
            'rules' => 'required' ),

        array( 'field' => 'descripcion',
            'label' => 'Descripción',
            'rules' => 'required' ),

        array( 'field' => 'precio',
            'label' => 'Precio',
            'rules' => 'required|decimal'),

        array( 'field' => 'tipo_producto_id',
            'label' => 'Tipo de Producto',
            'rules' => 'required|is_natural_no_zero'),

        array( 'field' => 'requisitos',
            'label' => 'Requisitos',
            'rules' => 'required'),
    );*/

    function __construct()
    {
        parent::__construct();
    }

    protected function prepare($data)
    {
        $data['usuario_id'] = $this->session->userdata('user_id');
        $data['modificado_en'] = date('Y-m-d H:i:s');
        return $data;
    }

    /**
     * Metodo para devolver la data para el listado de usuarios
     *
     * @access public
     * @author Jefferson Lara <jetox21@@gmail.com>
     * @version V-1.0 06/02/2015
     */
    public function get_datatable_list($searchs)
    {
        foreach ($searchs['columns'] as $key => $row) {
            if ($key == 0 or preg_match('/active/i',$row['name'])) {
                continue;
            }
            $this->datatables->or_like($row['name'], $searchs['search']['value'],'both');
        }

        $this->datatables
            ->select('u.username')
            ->select('u.email')
            ->select('u.first_name')
            ->select('u.last_name')
            ->select('u.last_name')
            ->select('u.cedula')
            ->select('u.direccion')
            ->select('u.phone')
            ->select('u.id')
            ->select(" (CASE WHEN u.active='1' THEN 'Activo' ELSE 'Inactivo' END) as active ", true)
            ->from('acl.users AS u');
        return $this->datatables->generate();
//        $this->datatables->generate();
//        echo $this->datatables->last_query();
    }

    public function get_data_user($id)
    {
//        return $this->get($id);

        $this->db->select($this->_table . ".*", false);
        $this->db->select($this->_table_user_group . '.group_id as group', false);
        $this->db->from($this->_table . ' LEFT');
        $this->db->join($this->_table_user_group, $this->_table . '.id=' . $this->_table_user_group . '.user_id');
        $this->db->where($this->_table . '.id', $id);
        $query = $this->db->get();
        $return_user = ($query->num_rows() > 0) ? $query->result_array() : FALSE;
        if ($return_user)
            $return_user = $return_user[0];
        return $return_user;
    }

    /**
     *
     */
    public function edit_user($id_user, $data)
    {
        if (!$data['cliente_id']) {
            unset($data['cliente_id']);
        }
        $usuario = $this->update($id_user, $data);
        $return_user = $usuario ? TRUE : FALSE;
        return $return_user;
    }

    public function get_list()
    {
        $this->db->select("id, first_name || ' ' || last_name as name", false);
        return $this->order_by('first_name')->as_array()->get_all();
    }
} 