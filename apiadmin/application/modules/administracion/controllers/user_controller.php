<?php

/**
 * Created by PhpStorm.
 * User: jetox
 * Date: 07/02/15
 * Time: 08:59 AM
 */
class User_controller extends REST_Controller
{
    private $return = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model("user_model");
        $this->load->model("group_models");
        $this->load->library(array('ion_auth'));
        $this->load->model("ion_auth_model");
    }

    public function store_get()
    {

        $id = $this->get('id');
        $data['groups'] = $this->group_models->get_groups();
        if ($id != 'undefined'):
            $data['info_user'] = $this->user_model->get_data_user($id);

            if ($data['info_user']):
                $data['group_selected'] = $this->group_models->get_groups($data['info_user']['group']);
                $data['group_selected'] = $data['group_selected'][0];
            endif;
        endif;

        $this->response($data, 200);
    }

    public function list_user_get()
    {
        $data = $this->user_model->get_datatable_list($this->input->get());
        $response = json_decode($data, true);
        $this->response($response, 200);
    }

    public function data_user_get()
    {
        $id = $this->get('id');
        $data_user = $this->user_model->get_data_user($id);
        if ($data_user):
            $this->return['success'] = true;
            $this->return['messages'] = 'Usuario desactivado Exitosamente!';
            $this->return['data'] = $data_user;
        endif;
        $this->response($this->return, 200);
    }

    public function io_user_get()
    {

        $id = $this->get('id');
        if (empty($id)):
            $this->return['success'] = false;
            $this->return['messages'] = 'Identificador no recibido';
        else:
            $data_user = $this->user_model->get_data_user($id);
            if ($data_user):
                if ((int)$data_user->active == 1):
                    $response = $this->ion_auth->deactivate($id);
                    if ($response):
                        $this->return['success'] = true;
                        $this->return['messages'] = 'Usuario desactivado Exitosamente!';
                    else:
                        $this->return['success'] = false;
                        $this->return['messages'] = 'No se pudo desactivar el usuario!';
                    endif;

                else:
                    $response = $this->ion_auth->activate($id);
                    if ($response):
                        $this->return['success'] = true;
                        $this->return['messages'] = 'Usuario activado Exitosamente!';
                    else:
                        $this->return['success'] = false;
                        $this->return['messages'] = 'No se pudo activar el usuario!';
                    endif;
                endif;

            else:
                $this->return['success'] = false;
                $this->return['messages'] = 'Error al procesar la accion';
            endif;

        endif;
        $this->response($this->return, 200);
    }


    /**
     * Retorna el listado de usuarios del sistema
     *
     * @author Darwin Serrano <darwinserrano@gmail.com>
     * @version V1.0 24/03/15
     */
    public function users_get()
    {
        $data = $this->user_model->get_list();
        $this->response($data, 200);
    }
}