<?php

/**
 * Created by PhpStorm.
 * User: frederick
 * Date: 26/02/15
 * Time: 02:26 PM
 */
class Group_controller extends REST_Controller
{
    private $return = array();

    public function __construct()
    {
        parent::__construct();
        $this->load->model("group_models");
    }

    /**
     * Provee los datos de los grupos de usuarios
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 27/02/15 02:30 PM
     */
    public function store_get()
    {
        $id = $this->get('id');
        if ($id != 'undefined')
            $data['groups'] = $this->group_models->get_groups($id);
        $this->response($data, 200);
    }

    /**
     * Provee el listado de Gestion de Grupos de Usuarios
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 26/02/15 02:48 PM
     */
    public function list_group_get()
    {
        $data = $this->group_models->get_datatable_list($this->input->get());
        $response = json_decode($data, true);
        $this->response($response, 200);
    }

    /**
     * Provee las operaciones de los grupos de usuarios
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 27/02/15 02:33 PM
     */

    public function group_operation_get()
    {

        $array_post['id'] = $this->get('id');
        if ($array_post['id']):
            $this->session->set_userdata('id_group_operation', $array_post['id']);
            $id = $array_post['id'];
            $group = $this->group_models->get_groups($id);
            $groupOperationsId = $this->group_models->get_group_operation($id);
            $menu = $this->group_models->menu_operation();
            $data_filter = $this->filter_operation($menu, $groupOperationsId);

            $out['roles'] = $this->buildTree(($data_filter ? $data_filter : $menu), 'id_operation', 'id');
            $out['id'] = $id;
            $out['grupo'] = $group;
            $this->response($out, 200);
        endif;
        $this->response(null, 200);
    }

    /**
     * Ordena de forma jerarquica las operaciones existente
     *
     * @access private
     * @param array $flat Arreglo de operaciones
     * @param array $pidKey Identificador Padre
     * @param array $idKey Identificador Principal
     * @return array Jerarquia
     * @return
     */
    private function buildTree($flat, $pidKey, $idKey = null)
    {
        $grouped = array();
        foreach ($flat as $sub) {
            $grouped[$sub[$pidKey]][] = $sub;
        }

        $fnBuilder = function ($siblings) use (&$fnBuilder, $grouped, $idKey) {
            foreach ($siblings as $k => $sibling) {
                $id = $sibling[$idKey];
                if (isset($grouped[$id])) {
                    $sibling['children'] = $fnBuilder($grouped[$id]);
                }
                $siblings[$k] = $sibling;
            }

            return $siblings;
        };

        $tree = $fnBuilder($grouped[0]);

        return $tree;
    }

    /**
     * Filtra las operaciones asociadas al grupo
     *
     * @access private
     * @param array $menu Universo de operaciones existentes
     * @param array $groupOperationsId Identificadores de Filtro de Grupos
     * @return array Data filtrada
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 03/03/15 11:04 AM
     */
    private function filter_operation($menu = array(), $groupOperationsId = array())
    {
        $output_filter = array();
        if (!empty($menu)):
            foreach ($menu as $key => $value):
                $output_filter[$key] = $value;
                if ($groupOperationsId):
                    if (in_array((int)$value['id'], $groupOperationsId)):
                        $output_filter[$key]['selected'] = true;
                    endif;
                endif;

            endforeach;
        endif;
        return $output_filter;
    }

    /**
     * Info previa para desactivar/activar grupo
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 02/03/15 12:05 PM
     */
    public function io_operation_get()
    {
        $id = $this->get('id');
        if ($id != 'undefined')

            $this->response($this->group_models->io_groups($id), 200);
    }

    /**
     * Permite desactivar/activar un grupo y los usuarios asociados
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 03/03/15 11:03 AM
     */
    public function proccess_io_operation_get()
    {
        $id = $this->get('id');
        if ($id != 'undefined')
            if ($this->group_models->proccess_io_groups($id)) {

                $this->return['success'] = true;
                $this->return['messages'] = 'Cambios aplicados Exitosamente!';

            }
        $this->response($this->return, 200);
    }

    /**
     * Permite agregar y eliminar operaciones asociadas a un grupo
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 03/03/15 11:29 AM
     */
    public function add_remove_operation_post()
    {
        $out = array();
        if ($this->post()):
            $id = $this->post('id');
            foreach ($this->post('roles') as $datos2):
                if ($datos2['selected']):
                    $out[$datos2['id']] = $datos2['id'];
                endif;
                if (isset($datos2['children'])):
                    foreach ($datos2['children'] as $datos3):
                        if ($datos3['selected']):
                            $out[$datos2['id']] = $datos2['id'];
                            $out[$datos3['id']] = $datos3['id'];
                        endif;
                        if (isset($datos3['children'])):
                            foreach ($datos3['children'] as $dato):
                                if ($dato['selected']):
                                    $out[$datos2['id']] = $datos2['id'];
                                    $out[$datos3['id']] = $datos3['id'];
                                    $out[] = $dato['id'];
                                endif;
                            endforeach;
                        endif;
                    endforeach;
                endif;
            endforeach;
            print_r($out); die;
            $success = $this->group_models->insert($out, $id);
            if ($success):
                $this->return['success'] = true;
                $this->return['messages'] = 'Cambios aplicados Exitosamente!';
            else:
                $this->return['success'] = false;
                $this->return['messages'] = 'Error!';
            endif;
        endif;
        $this->response($this->return, 200);

    }


    private function recorro($matriz)
    {
        $data = array();
        foreach ($matriz as $key => $value) {
            if (is_array($value)) {
                $this->recorro($value);
            } else {
                $data[] = $value;
            }


        }
        return $data;
    }

    public function all_group_get()
    {
        $this->response($this->group_models->get_list(), 200);
    }
}