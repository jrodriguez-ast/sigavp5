<?php

/**
 * Controlador para el manejo del inicio y usuarios de la aplicación
 *
 * @package Cotizaciones_ast
 * @since    V-1.0 12/01/2015
 * @author Jefferson Lara
 *
 */
class Auth_cotizaciones extends REST_Controller
{
    private $return = array();

    function __construct()
    {
        parent::__construct();
        $this->load->database();
        $this->load->library(array('ion_auth', 'form_validation'));
        $this->load->helper(array('url', 'language'));
        $this->load->library('acl');
        $this->form_validation->set_error_delimiters(
            $this->config->item('error_start_delimiter', 'ion_auth'),
            $this->config->item('error_end_delimiter', 'ion_auth')
        );

        $this->lang->load('auth');
    }

    /**
     * Método que verifica si existe session activa
     *
     * @access public
     * @author Jefferson Lara <jetox21@gmail.com>
     * @version V-1.0 12/01/15
     */
    public function store_get()
    {
        if (!$this->ion_auth->logged_in()) {

            $this->return['success'] = false;
            $this->return['messages'] = 'Usted no posee session activa..!!';

        } else {

            $this->return['success'] = true;
            $this->return['messages'] = '';

            $this->return['first_name'] = $this->session->userdata('user_first_name');
            $this->return['last_name'] = $this->session->userdata('user_last_name');
            $this->return['user_name'] = $this->session->userdata('username');
            $this->return['cliente_id'] = $this->session->userdata('cliente_id');
            $this->return['group_id'] = $this->session->userdata('group_id');
            $this->return['name'] = $this->session->userdata('name');


            //armamos los datos de session
            if (!$this->acl->init()) {
                $this->return['success'] = false;
                $this->return['messages'] = 'El usuario no posee ningun rol asociado<br />Contacto con el administrador de sistema..!!';
                $this->ion_auth->logout();

            } else {
                $this->return['menu'] = $this->session->userdata('sidebar');
            }


        }
        $this->response($this->return, 200);
    }

    /**
     * Método para ejecutar la autenticación de usuario y poder dar ingreso al sistema
     *
     * @access public
     * @author Jefferson Lara <jetox21@gmail.com>
     * @version V-1.0 12/01/15
     */
    public function login_post()
    {
        if (!$_POST) {
            $_POST["identity"] = $this->post("identity");
            $_POST["password"] = $this->post("password");
        }

        //validate form input
        $this->form_validation->set_rules('identity', 'Identity', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == true) {

            if ($this->ion_auth->login($this->input->post('identity'), $this->input->post('password'), false)) {

                $this->return['success'] = true;
                $this->return['messages'] = 'Estas logueado';

                $this->return['first_name'] = $this->session->userdata('user_first_name');
                $this->return['last_name'] = $this->session->userdata('user_last_name');
                $this->return['user_name'] = $this->session->userdata('username');
                $this->return['cliente_id'] = $this->session->userdata('cliente_id');
                $this->return['group_id'] = $this->session->userdata('group_id');
                $this->return['name'] = $this->session->userdata('name');


                //armamos los datos de session
                if (!$this->acl->init()) {
                    $this->return['success'] = false;
                    $this->return['messages'] = 'El usuario no posee ningun rol asociado<br />Contacto con el administrador de sistema..!!';
                    $this->ion_auth->logout();

                } else {
                    $this->return['menu'] = $this->session->userdata('sidebar');
                }


            } else {

                $this->return['success'] = false;
                $this->return['messages'] = 'Datos de acceso incorrectos..!!';
            }


        } else {

            $this->return['success'] = false;
            $this->return['messages'] = $this->form_validation->set_value('identity');
        }

        $this->response($this->return, 200);
    }

    /**
     * Método que verifica si el usuario tiene permiso para la url que intenta ingresar y devuelve los mismos
     *
     * @access public
     * @return int
     * @author Jefferson Lara <jetox21@gmail.com>
     * @version V-1.0 13/01/15
     */
    public function has_permissions_get()
    {
        $operation_url = $this->get('operation');
        $this->return['logged'] = true;

        if (!$this->ion_auth->logged_in()) {

            $this->return['success'] = false;
            $this->return['logged'] = false;
            $this->return['messages'] = 'La session ha expirado..!!';

        } else {

            $this->return['success'] = true;

            $id = $this->hasPermissions($operation_url);

            if (!empty($id)) {

                $this->load->model('acl_model');
                $data = $this->acl_model->get_operation($id);
                $this->return['permission'] = $data;

            } else {

                $this->return['success'] = false;
                $this->return['messages'] = 'Usted no posee permisos para esta operation...!!';
            }
        }

        $this->response($this->return, 200);
    }

    /**
     * Método que verifica si el usuario tiene permisos para esa url y devuelve el id de la operación si posee permisos
     *
     * @access private
     * @param   string $operation_url contiene la url a buscar en los permisos
     * @author Jefferson Lara <jetox21@gmail.com>
     * @version V-1.0 13/01/15
     * @return integer
     */
    private function hasPermissions($operation_url)
    {
        $return = null;
        $permissions = $this->session->userdata('permissions');
        if (!empty($permissions)):
            foreach ($permissions as $permission) {
                if ($permission['url'] == $operation_url) {
                    $return = $permission['id'];
                    break;
                }
            }
        endif;
        return $return;
    }

    /**
     * Método que verifica cierre de la session de usuario
     *
     * @access public
     * @author Jefferson Lara <jetox21@gmail.com>
     * @version V-1.0 15/01/15
     */
    function logout_get()
    {
        $this->return['success'] = false;
        $this->return['messages'] = 'No se pudo cerrar la session..!!';

        if ($this->ion_auth->logout()):
            $this->return['success'] = true;
            $this->return['messages'] = '';
        endif;

        $this->response($this->return, 200);
    }

    /**
     *
     */
    function create_user_post()
    {
        if (!$_POST) {
            $_POST["first_name"] = $this->post("first_name");
            $_POST["last_name"] = $this->post("last_name");
            $_POST["email"] = $this->post("email");
            $_POST["email_secundario"] = $this->post("email_secundario");
            $_POST["phone"] = $this->post("phone");
            $_POST["cedula"] = $this->post("cedula");
            $_POST["direccion"] = $this->post("direccion");
            $_POST["group"] = $this->post("group");
            $_POST["cliente_id"] = $this->post("cliente_id");
        }

        $tables = $this->config->item('tables', 'ion_auth');
        //validate form input
        $this->form_validation->set_rules('first_name', 'Nombres', 'required');
        $this->form_validation->set_rules('last_name', 'Apellidos', 'required');
        $this->form_validation->set_rules('email', 'Correo Electrónico', 'required|valid_email');
        $this->form_validation->set_rules('email_secundario', 'Correo Electrónico Secundario', 'required|valid_email');
        $this->form_validation->set_rules('phone', 'Teléfono', 'required');
        $this->form_validation->set_rules('cedula', 'Cédula', 'required');
        $this->form_validation->set_rules('group', 'Rol de Usuario', 'required');
        //   $this->form_validation->set_rules('cliente_id', 'Cliente');


        if ($this->form_validation->run() == true) {

            $email = strtolower($this->input->post('email'));
            $part = explode('@', $email);
            $username = strtolower(trim($part[0]));
            $password = strtolower(trim($part[0]));
            $address = $this->input->post('direccion');
            $additional_data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'cedula' => $this->input->post('cedula'),
                'phone' => $this->input->post('phone'),
                'cliente_id' => $this->input->post('cliente_id'),
                'direccion' => $address,
                'email_secundario' => $this->post("email_secundario")
            );
            $group = $this->input->post('group');
        }

        if ($this->form_validation->run() == true && $this->ion_auth->register($username, $password, $email, $additional_data, array($group))) {
            //check to see if we are creating the user
            //redirect them back to the admin page
            //$this->session->set_flashdata('message', $this->ion_auth->messages());
            //redirect("auth", 'refresh');
            $this->return['success'] = true;
            $this->return['messages'] = $this->ion_auth->messages();
        } else {
            //display the create user form
            //set the flash data error message if there is one
            //$this->data['message'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->return['messages'] = (validation_errors() ? validation_errors() : ($this->ion_auth->errors() ? $this->ion_auth->errors() : $this->session->flashdata('message')));
            $this->return['success'] = false;
        }

        $this->response($this->return, 200);
    }

    /**
     *
     */
    function edit_user_post()
    {
        $id = $this->post('id');
        if (!$_POST) {
            $_POST["first_name"] = $this->post("first_name");
            $_POST["last_name"] = $this->post("last_name");
            $_POST["email"] = $this->post("email");
            $_POST["email_secundario"] = $this->post("email_secundario");
            $_POST["phone"] = $this->post("phone");
            $_POST["cedula"] = $this->post("cedula");
            $_POST["direccion"] = $this->post("direccion");
            $_POST["group"] = $this->post("group");
            $_POST["cliente_id"] = $this->post("cliente_id");
        }
        //$tables = $this->config->item('tables', 'ion_auth');
        //validate form input
        $this->form_validation->set_rules('first_name', 'Nombres', 'required');
        $this->form_validation->set_rules('last_name', 'Apellidos', 'required');
        //$this->form_validation->set_rules('email', 'Correo Electronico', 'required|valid_email|is_unique[' . $tables['users'] . '.email]');
        $this->form_validation->set_rules('phone', 'Telefono', 'required');
        $this->form_validation->set_rules('cedula', 'Cedula', 'required');
        $this->form_validation->set_rules('group', 'Rol de Usuario', 'required');
        //   $this->form_validation->set_rules('cliente_id', 'Cliente');


        if ($this->form_validation->run() == true) {
            if ($this->ion_auth->is_admin()) {
                $user_groups = $this->ion_auth->get_users_groups($id)->result();

                $this->ion_auth->remove_from_group('', $id);
                $this->ion_auth->add_to_group($this->input->post('group'), $id);

            }
            $data = array(
                'first_name' => $this->input->post('first_name'),
                'last_name' => $this->input->post('last_name'),
                'cedula' => $this->input->post('cedula'),
                'email_secundario' => $this->input->post('email_secundario'),
                'phone' => $this->input->post('phone'),
                'direccion' => $this->input->post('direccion'),
                'email' => $this->input->post('email'),
                'cliente_id' => $this->input->post('cliente_id'),

            );
            $this->load->model('administracion/user_model');
            //check to see if we are updating the user
            if ($this->user_model->edit_user($id, $data)) {
                //redirect them back to the admin page if admin, or to the base url if non admin
                //$this->session->set_flashdata('message', $this->ion_auth->messages() );
                $this->return['success'] = true;
                $this->return['messages'] = 'Usuario Editado Exitosamente..!!';
                $this->destroy_session_user($id);

            } else {
                //redirect them back to the admin page if admin, or to the base url if non admin
                //$this->session->set_flashdata('message', $this->ion_auth->errors() );
                $this->return['success'] = false;
                $this->return['messages'] = 'Ocurrió algo inesperado al intentar editar el usuario!!';

            }

        } else {
            $this->return['messages'] = validation_errors();
            $this->return['success'] = false;
        }
        $this->response($this->return, 200);

    }

    /**
     * Creación de Grupos de Usuarios
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 27/02/15 09:54 AM
     */
    function create_group_post()
    {

        if (!$_POST) {
            $_POST["name"] = $this->post("name");
            $_POST["description"] = $this->post("description");
        }

        //validate form input
        $this->form_validation->set_rules('name', 'Nombre', 'required');
        $this->form_validation->set_rules('description', 'Descripción', 'required');


        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description')
            );
            $this->load->model('group_models');
            $create_group = $this->group_models->group_management($id = null, $data);
            if ($create_group['success']) {
                $this->return['success'] = true;
                $this->return['messages'] = 'Grupo Creado Exitosamente..!!';
            } else {
                $this->return['success'] = false;
                $this->return['messages'] = (empty($create_group['msg']) ? 'Ocurrió algo inesperado al intentar editar el Grupo!!' : $create_group['msg']);

            }

        } else {
            $this->return['messages'] = validation_errors();
            $this->return['success'] = false;
        }
        $this->response($this->return, 200);
    }

    /**
     * Edición de Grupos de Usuarios
     *
     * @access public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 27/02/15 10:44 AM
     */
    function edit_group_post()
    {
        $id = $this->post('id');

        if (!$_POST) {
            $_POST["name"] = $this->post("name");
            $_POST["description"] = $this->post("description");
        }
        $this->form_validation->set_rules('name', 'Nombre', 'required');
        $this->form_validation->set_rules('description', 'Descripción', 'required');


        if ($this->form_validation->run() == true) {
            $data = array(
                'name' => $this->input->post('name'),
                'description' => $this->input->post('description')
            );
            $this->load->model('group_models');
            $edit_group = $this->group_models->group_management($id, $data);
            if ($edit_group['success']) {
                $this->return['success'] = true;
                $this->return['messages'] = 'Grupo Editado Exitosamente..!!';

            } else {
                $this->return['success'] = false;
                $this->return['messages'] = (empty($edit_group['msg']) ? 'Ocurrió algo inesperado al intentar editar el Grupo!!' : $edit_group['msg']);

            }

        } else {
            $this->return['messages'] = validation_errors();
            $this->return['success'] = false;
        }

        $this->response($this->return, 200);

    }


    //change password
    function change_password_post()
    {
        $_POST['old'] = $this->post('actual');
        $_POST['new'] = $this->post('nueva');
        $_POST['new_confirm'] = $this->post('confirmacion');

        $response = array(
            "success" => true,
            "message" => ''
        );

        $this->form_validation->set_rules('old', $this->lang->line('change_password_validation_old_password_label'), 'required');
        $this->form_validation->set_rules('new', $this->lang->line('change_password_validation_new_password_label'), 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[new_confirm]');
        $this->form_validation->set_rules('new_confirm', $this->lang->line('change_password_validation_new_password_confirm_label'), 'required');

        if (!$this->ion_auth->logged_in()) {
            $response = array(
                "success" => false,
                "message" => 'Usted no posee session activa..!!'
            );
            $this->response($response, 401);
        }


        if ($this->form_validation->run() == false) {
            $response = array(
                "success" => false,
                "message" => str_replace(array("<p>", "</p>"), array("", ""), validation_errors())
            );
        } else {
            $identity = $this->session->userdata('identity');

            $change = $this->ion_auth->change_password($identity, $this->input->post('old'), $this->input->post('new'));
            if ($change) {
                //if the password was successfully changed
                $response['message'] = str_replace(array("<p>", "</p>"), array("", ""), $this->ion_auth->messages());
            } else {
                $response = array(
                    "success" => false,
                    "message" => str_replace(array("<p>", "</p>"), array("", ""), $this->ion_auth->errors())
                );
            }
        }

        $this->response($response, 200);
    }

    /**
     * Destruye la sesión de un usuario especifico
     *
     * @access public
     * @param integer $id Identificador del usuario
     * @return boolean
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 06/03/15 03:27 PM
     */

    public function destroy_session_user($id = null)
    {
        if (empty($id))
            return false;
        $this->load->model("auth_cotizaciones_model");
        $data = $this->auth_cotizaciones_model->get_session($id);
        return $data;
    }

    /**
     *
     */
    public function forgot_password_post()
    {
        $_POST['email'] = $this->post('email');

        $response = array(
            "status" => true,
            "message" => ""
        );

        try {
            //setting validation rules by checking wheather identity is username or email
            if ($this->config->item('identity', 'ion_auth') == 'username') {
                $this->form_validation->set_rules('email', $this->lang->line('forgot_password_username_identity_label'), 'required');
            } else {
                $this->form_validation->set_rules('email', $this->lang->line('forgot_password_validation_email_label'), 'required|valid_email');
            }


            if ($this->form_validation->run() == false) {
                //setup the input
                $this->data['email'] = array('name' => 'email',
                    'id' => 'email',
                );

                if ($this->config->item('identity', 'ion_auth') == 'username') {
                    $this->data['identity_label'] = $this->lang->line('forgot_password_username_identity_label');
                } else {
                    $this->data['identity_label'] = $this->lang->line('forgot_password_email_identity_label');
                }

                //set any errors and display the form

                throw new Exception((validation_errors()) ? validation_errors() : $this->session->flashdata('message'));

            } else {
                // get identity from username or email
                if ($this->config->item('identity', 'ion_auth') == 'username') {
                    $identity = $this->ion_auth->where('username', strtolower($this->input->post('email')))->users()->row();
                }

                if (empty($identity)) {
                    $identity = $this->ion_auth->where('email', strtolower($this->input->post('email')))->users()->row();
                }


                if (empty($identity)) {

                    if ($this->config->item('identity', 'ion_auth') == 'username') {
                        $this->ion_auth->set_message('forgot_password_username_not_found');
                    } else {
                        $this->ion_auth->set_message('forgot_password_email_not_found');
                    }

                    throw new Exception($this->ion_auth->messages());
                }

                //run the forgotten password method to email an activation code to the user
                $forgotten = $this->ion_auth->forgotten_password($identity->{$this->config->item('identity', 'ion_auth')});

                if ($forgotten) {
                    //if there were no errors
                    $response['message'] = $this->ion_auth->messages();
                } else {
                    throw new Exception($this->ion_auth->errors());
                }
            }
        } catch (Exception $e) {
            $response['message'] = str_replace(array("<p>", "</p>"), array("", ""), $e->getMessage());
            $response['status'] = false;
        }

        $response['message'] = str_replace(array("<p>", "</p>"), array("", ""), $response['message']);
        $this->response($response, 200);
    }
}