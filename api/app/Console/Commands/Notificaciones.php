<?php namespace App\Console\Commands;

use App\Models\Medio;
use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Notificacion;

class Notificaciones extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'notificaciones:crear';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Comando que llama a la(s) funcion(es) del modelo Notificacion para realizar el registro en la tabla notificaciones.';

    /**
     * Create a new command instance.
     *
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $notificacion = new Notificacion();
        $this->info(json_encode($notificacion->facturaVencida()));
	}



}
