<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Models\Medio;

class ActualizarDatos extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'datos:actualizar';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Actualiza los datos de los proveedores consultados en la web';

    /**
     * Create a new command instance.
     *
     */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function fire()
	{
        $medio = new Medio();
        $medio->updateDatos();
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
//	protected function getArguments()
//	{
//		return [
//			['example', InputArgument::REQUIRED, 'An example argument.'],
//		];
//	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
//	protected function getOptions()
//	{
//		return [
//			['example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null],
//		];
//	}

}
