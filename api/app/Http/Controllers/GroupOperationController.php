<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Acl\Operation;
use App\Models\Acl\Group;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

class GroupOperationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $operation = Operation::with('children')
            ->whereHas('children', function ($q) {
                return $q;
            })->get();
        return response()->json(['status' => 'ok', 'data' => $operation], 200);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function group_operation(Request $request)
    {
        $grupo_id = $request->input('id');
        $group = Group::find($grupo_id);
        if ($group):
            $operationsIDs = $group->groupOperationsIDs($grupo_id);
            $menu = Operation::operationsMenu();
            // print_r($menu);die;
            $data_filter = $this->filter_operation($menu, $operationsIDs);
            //print_r($data_filter); die;
            //$out['roles'] = $this->buildTree($menu, 'id_operation', 'id');
            $out['roles'] = $this->buildTree(($data_filter ? $data_filter : $menu), 'id_operation', 'id');
            $out['id'] = $grupo_id;
            $out['grupo'] = [$group];
            return response()->json($out, 200);
        endif;
    }

    /**
     * @param Request $request
     */
    public function add_remove_operation_post(Request $request)
    {
        $return['success'] = false;
        $return['messages'] = 'Error!';

        if ($request->all()):
            $id = $request->input('id');
            $operaciones = $request->input('roles');
            $operacionesArray = array();
            Operation::getOperationsArray($operaciones, $operacionesArray, $id);
            $grupo = Group::find($id);
            //$grupo->operation()->detach();
            $grupo->operation()->sync($operacionesArray);
            if (true):
                $return['success'] = true;
                $return['messages'] = 'Cambios aplicados Exitosamente!';
            endif;
        endif;

        return response()->json($return, 200);
    }

    /**
     * @param $flat
     * @param $pidKey
     * @param null $idKey
     * @return mixed
     */
    private function buildTree($flat, $pidKey, $idKey = null)
    {
        $grouped = array();
        foreach ($flat as $sub) {
            $grouped[$sub[$pidKey]][] = $sub;
        }

        $fnBuilder = function ($siblings) use (&$fnBuilder, $grouped, $idKey) {
            foreach ($siblings as $k => $sibling) {
                $id = $sibling[$idKey];
                if (isset($grouped[$id])) {
                    $sibling['children'] = $fnBuilder($grouped[$id]);
                }
                $siblings[$k] = $sibling;
            }
            return $siblings;
        };

        $tree = $fnBuilder($grouped[0]);

        return $tree;
    }

    /**
     * @param array $menu
     * @param array $groupOperationsId
     * @return array
     */
    private function filter_operation($menu = array(), $groupOperationsId = array())
    {
        $output_filter = array();
        if (!empty($menu)):
            foreach ($menu as $key => $value):
                $output_filter[$key] = $value;
                if ($groupOperationsId):
                    if (in_array((int)$value['id'], $groupOperationsId)):
                        $output_filter[$key]['selected'] = true;
                    else:
                        $output_filter[$key]['selected'] = false;
                    endif;
                endif;

            endforeach;
        endif;
        return $output_filter;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
