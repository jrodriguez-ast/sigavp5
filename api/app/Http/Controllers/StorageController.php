<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

class StorageController extends Controller
{

    public function save(Request $request)
    {   //Respuesta default
        $data = array('success' => false);

        //obtenemos el campo file definido en el formulario
        $file = $request->file('file');

        //obtenemos el nombre del archivo
        $nombre = str_random() . '.' . $file->getClientOriginalExtension();

        //indicamos que queremos guardar un nuevo archivo en el disco local/tmp
        if (\Storage::disk('tmp')->put($nombre, \File::get($file))):
            $data = array(
                'success' => true,
                'file_name' => $nombre,
                'message' => 'Archivo agregado exitosamente'
            );
        endif;
        return Response($data, 200);
    }
}
