<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Acl\User;
use App\Models\Cargo;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;

class CargosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json(Cargo::all(), 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            [
                'descripcion' => 'required',
                'username' => 'required',

            ]);
        if ($validator->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $validator->errors()])], 422);
        }
        $user = User::where('username', $request->header('username'))->first(['id']);
        $nuevoCargo = array(
            'descripcion' => $request->input('descripcion'),
            'usuario_id' => $user->id,
        );
        $nuevoCargo = Cargo::create($nuevoCargo);
        return response()->json(['status' => 'ok', 'data' => $nuevoCargo], 201, array('Location' => 'localhost/laravel/api/public/Cargo/' . $nuevoCargo->id, 'Content-Type' => 'application/json'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $Cargo = Cargo::find($id);
        if (!$Cargo) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Cargo con ese código.'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $Cargo], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $Cargo = Cargo::find($id);
        if (!$Cargo) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Cargo con ese código.'])], 404);
        }
        $descripcion = $request->input('descripcion');


        if ($request->method() === 'PATCH') {

            $bandera = false;

            if ($descripcion) {
                $Cargo->$descripcion = $descripcion;
                $bandera = true;
            }


            if ($bandera) {
                $Cargo->save();
                return response()->json(['status' => 'ok', 'data' => $Cargo], 200, array('Location' => 'localhost/laravel/api/public/Cargos/' . $Cargo->id));
            } else {
                return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato de Cargo.'])], 304);
            }
        }
        if (!$descripcion) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Faltan valores para completar el procesamiento.'])], 422);
        }
        $Cargo->descripcion = $descripcion;
        $Cargo->save();
        return response()->json(['status' => 'ok', 'data' => $Cargo], 200, array('Location' => 'localhost/laravel/api/public/Cargos/' . $Cargo->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $Cargo = Cargo::find($id);
        if (!$Cargo) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Cargo con ese código.'])], 404);
        }
        $Cargo->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el Cargo correctamente.'], 200);
    }

}
