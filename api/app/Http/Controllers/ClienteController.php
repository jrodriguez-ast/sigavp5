<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Response;

class ClienteController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json(['status' => 'ok', 'data' => Cliente::all()], 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'razon_social' => 'required',
            'rif' => 'required',
            'fecha_fundacion' => 'required',
            'direccion_fiscal' => 'required',
            'servicios' => 'required',
            'otros_canales' => 'required',
            'telefonos' => 'required',
            'correos_electronicos' => 'required',
        ]);
        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
        }
        $nuevoCliente = Cliente::create($request->all());
        return response()->json(['status' => 'ok', 'data' => $nuevoCliente], 201, array('Location' => 'localhost/laravel/api/public/cliente/' . $nuevoCliente->id, 'Content-Type' => 'application/json'));

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id);
        if (!$cliente) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $cliente], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $razon_social = $request->input('razon_social');
        $rif = $request->input('rif');
        $fecha_fundacion = $request->input('fecha_fundacion');
        $direccion_fiscal = $request->input('direccion_fiscal');
        $servicios = $request->input('servicios');
        $otros_canales = $request->input('otros_canales');
        $telefonos = $request->input('telefonos');
        $correos_electronicos = $request->input('correos_electronicos');

        if ($request->method() === 'PATCH') {

            $bandera = false;

            if ($razon_social) {
                $cliente->razon_social = $razon_social;
                $bandera = true;
            }

            if ($rif) {
                $cliente->rif = $rif;
                $bandera = true;
            }


            if ($fecha_fundacion) {
                $cliente->fecha_fundacion = $fecha_fundacion;
                $bandera = true;
            }
            if ($otros_canales) {
                $cliente->otros_canales = $otros_canales;
                $bandera = true;
            }

            if ($direccion_fiscal) {
                $cliente->direccion_fiscal = $direccion_fiscal;
                $bandera = true;
            }

            if ($servicios) {
                $cliente->servicios = $servicios;
                $bandera = true;
            }


            if ($telefonos) {
                $cliente->telefonos = $telefonos;
                $bandera = true;
            }

            if ($correos_electronicos) {
                $cliente->correos_electronicos = $correos_electronicos;
                $bandera = true;
            }
            if ($bandera) {
                $cliente->save();
                return response()->json(['status' => 'ok', 'data' => $cliente], 200, array('Location' => 'localhost/laravel/api/public/clientes/' . $cliente->id));
            } else {
                return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato de cliente.'])], 304);
            }
        }
        if (!$razon_social || !$rif || !$fecha_fundacion || !$direccion_fiscal || !$servicios || !$otros_canales || !$telefonos || !$correos_electronicos) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Faltan valores para completar el procesamiento.'])], 422);
        }
        $cliente->razon_social = $razon_social;
        $cliente->rif = $rif;
        $cliente->fecha_fundacion = $fecha_fundacion;
        $cliente->direccion_fiscal = $direccion_fiscal;
        $cliente->servicios = $servicios;
        $cliente->telefonos = $telefonos;
        $cliente->correos_electronicos = $correos_electronicos;
        $cliente->save();
        return response()->json(['status' => 'ok', 'data' => $cliente], 200, array('Location' => 'localhost/laravel/api/public/clientes/' . $cliente->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $cliente = Cliente::find($id);
        $flag=false;

        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un cliente con ese código.'], 200);
        }

        if ($cliente->cotizaciones()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este cliente tiene balances asociados.'], 200);
        }

        if ($cliente->publicaciones()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este cliente tiene balances asociados.'], 200);
        }

        if ($cliente->contratos()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este cliente tiene balances asociados.'], 200);
        }

        if ($cliente->balance()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este cliente tiene balances asociados.'], 200);
        }

        if ($cliente->nota()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este cliente tiene balances asociados.'], 200);
        }

        if ($flag) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este cliente tiene balances asociados.'], 200);
        }

        $cliente->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el cliente correctamente.'], 200);
    }

}
