<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Contrato;
use App\Models\NotasCredito;
use App\Models\RecargoGestion;
use App\Models\Servicio;
use App\Models\Utilities\GenericConfiguration;
use App\Models\Negociaciones;
use App\Models\Factura;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\URL;
use DateTime; //Es necesario agregarlo aquí por ser una clase nativa del lenguaje
use DateInterval;
use App\Models\Publicacion;
use Illuminate\Support\Facades\DB;
use PDO;

class PdfController extends Controller
{

    public function header (Request $request)
    {
        return view ('pdf.header', $request->all ());
    }

    public function footer (Request $request)
    {
        return view ('pdf.footer', $request->all ());
    }

    /**
     * Construcción Genérica del esqueleto de PDFs, el parámetro $modelo define a quien pertenece el pdf, el $modelo_id
     * indica el identificador a consultar.
     *
     * @param string $modelo
     * @param int $modelo_id
     * @param bool $invalid
     * @author  Jose Rodriguez
     * @version V-1.0 22-07-2015
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function _print ($modelo, $modelo_id, $invalid = false)
    {
        if (empty($modelo) || empty($modelo_id) || !is_string ($modelo) || !is_numeric ($modelo_id)) {
            return response ()->json (['status' => 'fail', 'errors' => array([
                'code' => 422, 'message' => 'Parámetros Inválidos.'])
            ], 422);
        }

        $data = [];
        // Data debe contener las siguientes posiciones:
        // header_data => [
        //    'titulo' => 'Titulo de documento',
        //    'to' => 'A quien va dirigido el documento',
        //    'fecha' => 'Fecha cuando se realizo el documento',
        //    'code' => 'Código generado para el documento',
        //    'code_name' => 'Nombre que tendrá la etiqueta que acompaña el nombre',
        //    'subject' => 'Negociación Registrada'
        // ]
        // footer_data => [
        //    'firmante' => 'Nombre de la persona que firma el documento',
        //    'cargo' => 'Cargo de la persona que firma el documento',
        //    'iniciales' => 'Iniciales del Firmante "/" Iniciales del usuario quien creo el modelo.'
        // ]
        // view_data => []
        // view_path = ''
        // file_name = ''
        $modelo = strtoupper ($modelo);
        if ($modelo == 'NEGOCIACION') {
            $data = $this->negociacion ($modelo_id);
        } elseif ($modelo == 'PUBLICACION') {
            $data = $this->orden_publicacion ($modelo_id);
        } elseif ($modelo == 'TARIFARIO') {
            $data = $this->tarifario ($modelo_id);
        } elseif ($modelo == 'CONTRATO') {
            $data = $this->contrato ($modelo_id);
        } elseif ($modelo == 'FACTURACION') {
             //return $this->facturacion ($modelo_id);
            $data = $this->facturacion ($modelo_id);
        } elseif ($modelo == 'FACTURACIONCUOTA') {
             //return $this->facturacionCuota ($modelo_id);
            $data = $this->facturacionCuota ($modelo_id);
        } elseif ($modelo == 'NOTACREDITO') {
            // return $this->notaCredito ($modelo_id);
            $data = $this->notaCredito ($modelo_id);
        }
        // Coloca AQui tu condición de MODELO
        // Copia la siguiente linea.
        // elseif ($modelo == 'NEGOCIACION') { $data = $this->negociacion($modelo_id);}
        else {
            return response ()->json (['status' => 'fail', 'errors' => array([
                'code' => 422, 'message' => 'modelo inválido.'])
            ], 422);
        }

        if (!is_array ($data)) {
            return $data;
        }

        $data['header_data']['avp_rif'] = GenericConfiguration::where ('_label', 'avp_rif')->pluck ('value');
        $data['header_data']['avp_direccion'] = GenericConfiguration::where ('_label', 'avp_direccion')->pluck ('value');
        $data['view_data']['no_valido'] = $invalid;

        // Construcción del PDF
        $pdf = App::make ('snappy.pdf.wrapper');
        $pdf->setOption ('margin-top', '65mm');
        $pdf->setOption ('margin-bottom', '30mm');
        $pdf->setOption ('load-error-handling', 'ignore');
        if (!empty($data['footer_data']))
        $pdf->setOption ('header-html', URL::to ('/') . '/pdf/header?' . http_build_query ($data['header_data']));

        // Si no tiene Footer.
        if (!empty($data['footer_data']))
            $pdf->setOption ('footer-html', URL::to ('/') . '/pdf/footer?' . http_build_query ($data['footer_data']));

        $pdf->setPaper ('a4');
        $pdf->loadView ($data['view_path'], $data['view_data']);
        return $pdf->download ($data['file_name']);
    }

    /**
     * Recibe una cadena de texto y retorna solo sus iniciales en minúsculas (L) o mayúsculas (U) de acuerdo al
     * parámetro $case.
     *
     * @param        $name
     * @param string $case
     * @author  Jose Rodriguez
     * @version V-1.0 22-07-2015
     * @return string
     */
    public function getInitials ($name, $case = 'L')
    {
        if (empty($name)) {
            return $name;
        }

        $initials = $name;
        $splitted_name = explode (' ', trim ($name));
        if (!empty($splitted_name)) {
            $initials = '';
            foreach ($splitted_name as $row)
                $initials .= substr (trim ($row), 0, 1);
        }
        return ($case == 'U') ? strtoupper ($initials) : strtolower ($initials);
    }

    /**
     * Método que arma la data de negociación para la impresión del PDF.
     *
     * @param Integer $negociacion_id
     * @author  Jose Rodriguez
     * @version V-1.0 22-07-2015
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    private function negociacion ($negociacion_id)
    {
        $data = [];

        $negociacion = Negociaciones::with ([
            'servicios.servicio',

            'condiciones.servicio',
            'condiciones.condicion',

            'condicionDetalles.servicio',
            'condicionDetalles.condicion',
            'condicionDetalles.detalle',

            'usuario',
            'medio'
        ])->find ($negociacion_id);

        if (!$negociacion) {
            return response ()->json (['status' => 'fail', 'errors' => array([
                'code' => 404, 'message' => 'No se encuentra la negociación con ese código.'])
            ], 404);
        }

        $data['header_data'] = [
            'titulo' => GenericConfiguration::where ('_label', 'negociacion_pdf_titulo')->pluck ('value'),
            'to' => $negociacion->medio->razon_social,
            'fecha' => $negociacion->fecha_negociacion,
            'code' => $negociacion->codigo,
            'code_name' => GenericConfiguration::where ('_label', 'negociacion_code_name')->pluck ('value'),
            'subject' => GenericConfiguration::where ('_label', 'negociacion_subject')->pluck ('value')
        ];

        $data['footer_data'] = [
            'firmante' => $negociacion->firmante_nombre,
            'cargo' => $negociacion->firmante_cargo,
            'iniciales' => $this->getInitials ($negociacion->firmante_nombre, 'U') . '/' . $this->getInitials ("{$negociacion['user']['first_name']} {$negociacion['user']['last_name']}")
        ];
        $data['view_data'] = ['negociacion' => $negociacion];
        $data['view_path'] = 'pdf.negociacion';
        $data['file_name'] = "AVP_Negociacion_N_{$negociacion->codigo}.pdf";
        return $data;
    }

    /**
     * Método que genera el PDF de la factura
     *
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param Integer $idfactura
     * @return array
     */
    private function facturacion ($idfactura)
    {
        $factura = Factura::with ([
            'publicacion.cotizacion',
            'publicacion.version.servicio',
            'publicacion.version.versioncondicion.detallecondicion',
            'publicacion.version.versioncondicion.condicion',
            'publicacion.cliente.contactos'
        ])->find ($idfactura)->toArray ();
       // dd($factura);

        foreach ($factura['publicacion'] as $key => $publicacion) {
            $id_publicacion = $publicacion['id'];
            DB::setFetchMode(PDO::FETCH_ASSOC);
            $query = "SELECT DISTINCT
                    version_nombre,
                    MIN(version_publicacion_fecha_inicio) as fecha_inicio,
                    MAX(version_publicacion_fecha_fin) as fecha_fin
                  FROM
                    ordenes_publicacion_versiones opv

                  INNER JOIN

                    ordenes_publicacion op ON  op.id = opv.orden_publicacion_id

                  WHERE

                    op.id = '$id_publicacion'

                  GROUP BY
                    version_nombre";
            $fechas = DB::select(DB::raw($query));
            foreach($fechas as $fecha){
                $fecha_inicio = $fecha['fecha_inicio'];
                $fecha_final = $fecha['fecha_fin'];
                $fecha_inicial = new DateTime($fecha['fecha_inicio']);
                $fecha_fin = new DateTime($fecha['fecha_fin']);
                $diasporpublicacion = Carbon::createFromTimestamp (strtotime (($fecha_final)))->diffInDays (Carbon::createFromTimestamp (strtotime ($fecha_inicio)));
                $diasporpublicacion++;

                foreach ($publicacion['version'] as $version) {

                    foreach ($factura['publicacion'][$key]['version'] as $posicion => $versiones) {
                        $factura['publicacion'][$key]['version'][$posicion]['diasporpublicacion'] = $diasporpublicacion;
                        $factura['publicacion'][$key]['version'][$posicion]['fecha_inicial'] = $fecha_inicial;
                        $factura['publicacion'][$key]['version'][$posicion]['fecha_final'] = $fecha_fin;
                    }

                }
            }
        }

        //  dd($factura);
        $total = 0.00;
        $iva = 0.00;
        $subtotal = 0.00;
        $count = 0;
        $porcentaje_iva = 0;
        foreach ($factura['publicacion'] as $key =>$publicacion) {

            $iva = $iva+$publicacion['total_impuesto'];
            $porcentaje_iva = $publicacion['porcentaje_impuesto'];
            foreach ($publicacion['version'] as $version) {
                $subtotal = $subtotal + $version['monto_con_avp'];
                $total = $total + ($version['monto_con_avp'] + $iva);

                $count++;
            }
        }
       // dd($subtotal);
        $totalfactura=  $factura['total_factura'];

//         dd($factura);

        $fecha = new DateTime($factura['created_at']);
        $fecha_creacion = $fecha->format ("d-m-Y");

        $fecha->add (new DateInterval('P' . GenericConfiguration::where ("_label", "factura_validez")->pluck ('value') . 'D'));
        $vencimiento = $fecha->format ("d-m-Y");
        $direccion = $factura['publicacion'][0]['cliente']['direccion_fiscal'];
        $cod_cliente = $factura['publicacion'][0]['cliente']['codigo'];
        $rif = $factura['publicacion'][0]['cliente']['rif'];
        $telefonos = json_decode ($factura['publicacion'][0]['cliente']['telefonos']);
        $correo_electronico = json_decode ($factura['publicacion'][0]['cliente']['correos_electronicos']);
        $numero_factura = $factura['codigo'];
        $nota = GenericConfiguration::where ("_label", "nota_vencimiento_factura")->pluck ('value');
        $code_name = GenericConfiguration::where ("_label", "factura_code_name")->pluck ('value');
        $subject = GenericConfiguration::where ("_label", "factura_subject")->pluck ('value');
        $cliente = $factura['publicacion'][0]['cliente']['razon_social'];
        $nombre_cheque = GenericConfiguration::where ("_label", "nombre_cheque")->pluck ('value');
        $numero_cuenta = GenericConfiguration::where ("_label", "numero_cuenta")->pluck ('value');
        $monto_letras = $this->valorEnLetras($totalfactura);


        $data = [
            'view_data' => [
                'factura' => $factura,
                'subtotal' => $subtotal,
                'total' => $total,
                'iva' => $iva,
                'porcentaje_iva' => $porcentaje_iva,
                'fecha_creacion' => $fecha_creacion,
                'numero_factura' => $numero_factura,
                'cliente' => $cliente,
                'direccion' => $direccion,
                'rif' => $rif,
                'cod_cliente' => $cod_cliente,
                'correo_electronico' => $correo_electronico,
                'telefonos' => $telefonos,
                'monto_letras' => $monto_letras,
                'nombre_cheque' => $nombre_cheque,
                'numero_cuenta' => $numero_cuenta,
                'vencimiento'=> $vencimiento,
            ],
            'view_path' => 'pdf.facturacion',
            'file_name' => 'factura_' . $factura['codigo'] . '.pdf',
        ];

        return $data;
//        $pdf = App::make('dompdf.wrapper');
//        $pdf->loadView($data['view_path'],$data);
//        return @$pdf->setPaper ('a4')
//            ->setWarnings (false)
//            ->download ($data['file_name']);
    }

    /**
     * Método que genera el PDF de la Orden de Publicación
     *
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @param Integer $id_orden
     * @return array
     */
    private function orden_publicacion ($id_orden)
    {
        $publicacion = Publicacion::data_for_publicacion ($id_orden)->toArray ();
     //   dd($publicacion['version']);
        //$publicacion['versiones'] = $publicacion['versiones']->toArray ();
        $totalconiva = 0;
        $rotacion = 0;
        $fecha_inicio = 0;
        $fecha_final = 0;
        $dias = 0;
        $diasporpublicacion = 0;
        $id_publicacion = $publicacion['id'];
        DB::setFetchMode(PDO::FETCH_ASSOC);
        $query = "SELECT DISTINCT
                    version_nombre,
                    MIN(version_publicacion_fecha_inicio) as fecha_inicio,
                    MAX(version_publicacion_fecha_fin) as fecha_fin
                  FROM
                    ordenes_publicacion_versiones opv

                  INNER JOIN

                    ordenes_publicacion op ON  op.id = opv.orden_publicacion_id

                  WHERE

                    op.id = '$id_publicacion'

                  GROUP BY
                    version_nombre";
        $fechas = DB::select(DB::raw($query));

        foreach($fechas as $fecha) {
            $fecha_inicio = $fecha['fecha_inicio'];
            $fecha_final = $fecha['fecha_fin'];
            $fecha_ini = new DateTime($fecha['fecha_inicio']);
            $fecha_fin = new DateTime($fecha['fecha_fin']);
            $diasporpublicacion = Carbon::createFromTimestamp (strtotime (($fecha_final)))->diffInDays (Carbon::createFromTimestamp (strtotime ($fecha_inicio)));
            $diasporpublicacion++;
            $dias += 1 + Carbon::createFromTimestamp (strtotime (($fecha_final)))->diffInDays (Carbon::createFromTimestamp (strtotime ($fecha_inicio)));

            foreach ($publicacion['version'] as $key => $version) {
                $totalconiva += $version['subtotal'] + ($version['subtotal'] * $publicacion['porcentaje_impuesto'] / 100);
                $rotacion += $version['version_apariciones'];
                $publicacion['version'][$key]['diasporpublicacion'] = $diasporpublicacion;
                $publicacion['version'][$key]['fecha_inicial'] = $fecha_ini;
                $publicacion['version'][$key]['fecha_final'] = $fecha_fin;
                //dd($publicacion['versiones']);
            }
        }
//dd($publicacion);
        $total = 0.00;
        $subtotal = 0.00;
        $fecha = new DateTime($publicacion['fecha_creacion']);
        $fecha_creacion = $fecha->format ("d-m-Y");
        $data = [
            'header_data' => [
                'titulo' => 'Orden de Publicacion',
                'to' => $publicacion['medio']['razon_social'],
                'fecha' => $fecha_creacion,
                'code' => $publicacion['codigo'],
                'code_name' => GenericConfiguration::where ("_label", "publicacion_code_name")->pluck ('value'),
                'subject' => GenericConfiguration::where ("_label", "publicacion_subject")->pluck ('value'),
                'cliente' => $publicacion['cliente']['razon_social'],
                'rifcli' => $publicacion['cliente']['rif'],
                'rifmed' => $publicacion['medio']['rif'],
            ],
            'footer_data' => [
                'firmante' => $publicacion['firmante_nombre'],
                'cargo' => $publicacion['firmante_cargo'],
                'iniciales' => $this->getInitials ($publicacion['firmante_nombre'], 'U'),
            ],
            'view_data' => [
                'publicacion' => $publicacion,
                'subtotal' => $subtotal,
                'total' => $total,
                'totalconiva' => $totalconiva,
                'diasporpublicacion' => $diasporpublicacion,
            ],
            'view_path' => 'pdf.orden_publicacion',
            'file_name' => 'orden_publicacion_' . $publicacion['codigo'] . '.pdf',
        ];
        return $data;
    }

    /**
     * @param Integer $contrato_id
     * @return array
     */
    private function contrato ($contrato_id)
    {
        $contrato = Contrato::with ([
            'cliente',
            'cuotas',
            'tipo',
            'pautas',
            'pautas.tipo'
        ])->find ($contrato_id)->toArray();

        //dd($contrato);
        $fecha = new DateTime($contrato['created_at']);
        $fecha_creacion = $fecha->format ("d-m-Y");
        $tipo_contrato = $contrato['tipo']['_label'];
        $codigo_contrato = $contrato['codigo'];

        //datos del cliente
        $cliente = $contrato['cliente']['razon_social'];

        $data = [
            'header_data' => [
                'titulo' => GenericConfiguration::where ("_label", "contrato_titulo")->pluck ('value'),
                'to' => $cliente,
                'fecha' => $fecha_creacion,
                'code' => $contrato['codigo'],
                'code_name' => GenericConfiguration::where ("_label", "contrato_code_name")->pluck ('value'),
                'subject' => GenericConfiguration::where ("_label", "contrato_subject")->pluck ('value'),
            ],
            'footer_data' => [
                'firmante' => $contrato['firmante_nombre'],
                'cargo' => $contrato['firmante_cargo'],
                'iniciales' => $this->getInitials ($contrato['firmante_nombre'], 'U'),
            ],
            'view_data' => [
                'tipo_contrato' => $tipo_contrato,
                'contrato' => $contrato,
                'cliente_id' => $contrato['cliente']['razon_social'],
                'rif' => $contrato['cliente']['rif'],
                'tarifa' => $contrato['total'],
                'cuota' => $contrato['cuota'],
                'codigo_contrato' => $codigo_contrato,
                'created_at' => $fecha_creacion,
            ],
            'view_path' => 'pdf.contrato',
            'file_name' => 'contrato' . $contrato['codigo'] . '.pdf',
        ];
        //dd($data);
        return $data;
    }

    /**
     * Crea el Tarifario del servicio indicado.
     *
     * @param Integer $servicio_id
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    private function tarifario ($servicio_id)
    {
        $data = [];
        $servicio = Servicio::with ([
            'tipoPublicacion' => function ($q) {
                $q->addSelect ('id', 'nombre');
            },
            'condiciones' => function ($q) {
                $q->orderBy ('es_base', 'desc');
            },
            'condiciones.detalles',
            'medio' => function ($q) {
                $q->addSelect ('id', 'nombre_comercial', 'razon_social', 'rif');
            }
        ])->find ($servicio_id);
        if (!$servicio) {
            return response ()->json (['status' => 'fail', 'errors' => array([
                'code' => 404, 'message' => 'No se encuentra la negociación con ese código.'])
            ], 404);
        }

        $data['header_data'] = [
            'titulo' => GenericConfiguration::where ('_label', 'tarifario_titulo')->pluck ('value'),
            'to' => $servicio->medio->razon_social,
            'fecha' => Date ('d-m-Y'),
        ];

        $data['view_data'] = ['servicio' => $servicio];
        $data['view_path'] = 'pdf.tarifario';
        $data['file_name'] = "Tarifario_Medio_{$servicio->medio->razon_social}_Servicio_{$servicio->nombre}.pdf";
        return $data;
    }

    /**
     * Método que genera el PDF de la factura por cuota
     *
     * @author Maykol Purica <cebs923@gmail.com>
     * @param Integer $idfactura
     * @return array
     */
    private function facturacionCuota ($idfactura)
    {
        $factura = Factura::with ([
            'cuota.contrato.cliente'
        ])->find ($idfactura)->toArray ();
        //dd($factura);

        $total = 0.00;
        $iva = 0.00;
        $porcentaje = 0.00;
        $nombre_cheque = GenericConfiguration::where ("_label", "nombre_cheque")->pluck ('value');
        $numero_cuenta = GenericConfiguration::where ("_label", "numero_cuenta")->pluck ('value');
        foreach ($factura['cuota'] as $key => $cuota){
            $total += $total + $factura['cuota'][$key]['monto'];
            $iva += $iva + $factura['cuota'][$key]['monto_iva'];
        }

        //$porcentaje_avp= RecargoGestion::first('id')->orderBy('des')->pluck ('porcentaje');
        // dd($porcentaje_avp
        $porcentaje_avp= 10;
        $porcentaje = ($porcentaje_avp * $total) / 100;

        $monto_total= ($total + $porcentaje )+$iva;
        //Monto en letras
        $monto_letras = $this->valorEnLetras($monto_total);

        //datos de la factura

        $fecha_creacion = $factura['fecha_generada'];
        $numero_factura= $factura['codigo'];

        //datos del cliente
        $cliente = $factura['cuota'][0]['contrato']['cliente']['razon_social'];
        $direccion= $factura['cuota'][0]['contrato']['cliente']['direccion_fiscal'];
        $rif= $factura['cuota'][0]['contrato']['cliente']['rif'];
        $cod_cliente= $factura['cuota'][0]['contrato']['cliente']['codigo'];
        $correo_electronico = json_decode ($factura['cuota'][0]['contrato']['cliente']['correos_electronicos']);
        $telefonos = json_decode ($factura['cuota'][0]['contrato']['cliente']['telefonos']);

        $data = [
            'view_data' => [
                'factura' => $factura,
                'fecha_creacion' => $fecha_creacion,
                'numero_factura' => $numero_factura,
                'cliente' => $cliente,
                'direccion' => $direccion,
                'rif' => $rif,
                'total' => $total,
                'iva' => $iva,
                'cod_cliente' => $cod_cliente,
                'correo_electronico' => $correo_electronico,
                'telefonos' => $telefonos,
                'porcentaje' => $porcentaje,
                'monto_letras' => $monto_letras,
                'nombre_cheque' => $nombre_cheque,
                'numero_cuenta' => $numero_cuenta,

            ],
            'view_path' => 'pdf.facturacion_cuota',
            'file_name' => 'factura_' . $factura['codigo'] . '.pdf',
        ];

       return $data;
//        $pdf = App::make('dompdf.wrapper');
//        $pdf->loadView($data['view_path'],$data);
//        return @$pdf->setPaper ('a4')
//            ->setWarnings (false)
//            ->download ($data['file_name']);
    }

    private function notaCredito ($idnota)
    {
        $nota = NotasCredito::with ([
            'factura',
            'factura.facturaContratoCuotas.contrato.cliente',
            'factura.facturaOrdenPublicacion'
        ])->find ($idnota)->toArray ();
        //dd($nota);

        $total = 0.00;
        $nombre_cheque = GenericConfiguration::where ("_label", "nombre_cheque")->pluck ('value');
        $numero_cuenta = GenericConfiguration::where ("_label", "numero_cuenta")->pluck ('value');

         $total += $nota['monto'];
         $codigo_nota= $nota['codigo'];
         $fecha= $nota['created_at'];

        //$porcentaje_avp= RecargoGestion::first('id')->orderBy('des')->pluck ('porcentaje');
        // dd($porcentaje_avp
//        $porcentaje_avp= 10;
//        $porcentaje = ($porcentaje_avp * $total) / 100;
        //Monto en letras
        $monto_letras = $this->valorEnLetras($total);

        //datos de la factura

        $fecha_creacion = $nota['factura']['fecha_generada'];
        $numero_factura= $nota['factura']['codigo'];

        //datos del cliente
        if($nota['factura']['factura_contrato_cuotas'] != null) {


            $cliente = $nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['razon_social'];

            $direccion = $nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['direccion_fiscal'];
            $rif = $nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['rif'];
            $cod_cliente = $nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['codigo'];
            $correo_electronico = json_decode ($nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['correos_electronicos']);
            $telefonos = json_decode ($nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['telefonos']);
        }else{

            $cliente = $nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['razon_social'];
            $direccion = $nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['direccion_fiscal'];
            $rif = $nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['rif'];
            $cod_cliente = $nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['codigo'];
            $correo_electronico = json_decode ($nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['correos_electronicos']);
            $telefonos = json_decode ($nota['factura']['factura_contrato_cuotas'][0]['contrato']['cliente']['telefonos']);
        }
        $data = [
            'view_data' => [
                'nota' => $nota,
                'fecha_creacion' => $fecha_creacion,
                'numero_factura' => $numero_factura,
                'cliente' => $cliente,
                'direccion' => $direccion,
                'rif' => $rif,
                'total' => $total,
                'cod_cliente' => $cod_cliente,
                'correo_electronico' => $correo_electronico,
                'telefonos' => $telefonos,
                'monto_letras' => $monto_letras,
                'nombre_cheque' => $nombre_cheque,
                'numero_cuenta' => $numero_cuenta,
                'codigo_nota' => $codigo_nota,
                'fecha' => $fecha,

            ],
            'view_path' => 'pdf.nota_credito',
            'file_name' => 'nota_' . $nota['codigo'] . '.pdf',
        ];

        return $data;
//        $pdf = App::make('dompdf.wrapper');
//        $pdf->loadView($data['view_path'],$data);
//        return @$pdf->setPaper ('a4')
//            ->setWarnings (false)
//            ->download ($data['file_name']);
    }



function unidades($u)
{
    if ($u==0)  {$ru = " ";}
    elseif ($u==1)  {$ru = "Un ";}
    elseif ($u==2)  {$ru = "Dos ";}
    elseif ($u==3)  {$ru = "Tres ";}
    elseif ($u==4)  {$ru = "Cuatro ";}
    elseif ($u==5)  {$ru = "Cinco ";}
    elseif ($u==6)  {$ru = "Seis ";}
    elseif ($u==7)  {$ru = "Siete ";}
    elseif ($u==8)  {$ru = "Ocho ";}
    elseif ($u==9)  {$ru = "Nueve ";}
    elseif ($u==10) {$ru = "Diez ";}

    elseif ($u==11) {$ru = "Once ";}
    elseif ($u==12) {$ru = "Doce ";}
    elseif ($u==13) {$ru = "Trece ";}
    elseif ($u==14) {$ru = "Catorce ";}
    elseif ($u==15) {$ru = "Quince ";}
    elseif ($u==16) {$ru = "Dieciseis ";}
    elseif ($u==17) {$ru = "Decisiete ";}
    elseif ($u==18) {$ru = "Dieciocho ";}
    elseif ($u==19) {$ru = "Diecinueve ";}
    elseif ($u==20) {$ru = "Veinte ";}

    elseif ($u==21) {$ru = "Veintiun ";}
    elseif ($u==22) {$ru = "Veintidos ";}
    elseif ($u==23) {$ru = "Veintitres ";}
    elseif ($u==24) {$ru = "Veinticuatro ";}
    elseif ($u==25) {$ru = "Veinticinco ";}
    elseif ($u==26) {$ru = "Veintiseis ";}
    elseif ($u==27) {$ru = "Veintisiente ";}
    elseif ($u==28) {$ru = "Veintiocho ";}
    elseif ($u==29) {$ru = "Veintinueve ";}
    elseif ($u==30) {$ru = "Treinta ";}

    elseif ($u==31) {$ru = "Treinta y un ";}
    elseif ($u==32) {$ru = "Treinta y dos ";}
    elseif ($u==33) {$ru = "Treinta y tres ";}
    elseif ($u==34) {$ru = "Treinta y cuatro ";}
    elseif ($u==35) {$ru = "Treinta y cinco ";}
    elseif ($u==36) {$ru = "Treinta y seis ";}
    elseif ($u==37) {$ru = "Treinta y siete ";}
    elseif ($u==38) {$ru = "Treinta y ocho ";}
    elseif ($u==39) {$ru = "Treinta y nueve ";}
    elseif ($u==40) {$ru = "Cuarenta ";}

    elseif ($u==41) {$ru = "Cuarenta y un ";}
    elseif ($u==42) {$ru = "Cuarenta y dos ";}
    elseif ($u==43) {$ru = "Cuarenta y tres ";}
    elseif ($u==44) {$ru = "Cuarenta y cuatro ";}
    elseif ($u==45) {$ru = "Cuarenta y cinco ";}
    elseif ($u==46) {$ru = "Cuarenta y seis ";}
    elseif ($u==47) {$ru = "Cuarenta y siete ";}
    elseif ($u==48) {$ru = "Cuarenta y ocho ";}
    elseif ($u==49) {$ru = "Cuarenta y nueve ";}
    elseif ($u==50) {$ru = "Cincuenta ";}

    elseif ($u==51) {$ru = "Cincuenta y un ";}
    elseif ($u==52) {$ru = "Cincuenta y dos ";}
    elseif ($u==53) {$ru = "Cincuenta y tres ";}
    elseif ($u==54) {$ru = "Cincuenta y cuatro ";}
    elseif ($u==55) {$ru = "Cincuenta y cinco ";}
    elseif ($u==56) {$ru = "Cincuenta y seis ";}
    elseif ($u==57) {$ru = "Cincuenta y siete ";}
    elseif ($u==58) {$ru = "Cincuenta y ocho ";}
    elseif ($u==59) {$ru = "Cincuenta y nueve ";}
    elseif ($u==60) {$ru = "Sesenta ";}

    elseif ($u==61) {$ru = "Sesenta y un ";}
    elseif ($u==62) {$ru = "Sesenta y dos ";}
    elseif ($u==63) {$ru = "Sesenta y tres ";}
    elseif ($u==64) {$ru = "Sesenta y cuatro ";}
    elseif ($u==65) {$ru = "Sesenta y cinco ";}
    elseif ($u==66) {$ru = "Sesenta y seis ";}
    elseif ($u==67) {$ru = "Sesenta y siete ";}
    elseif ($u==68) {$ru = "Sesenta y ocho ";}
    elseif ($u==69) {$ru = "Sesenta y nueve ";}
    elseif ($u==70) {$ru = "Setenta ";}

    elseif ($u==71) {$ru = "Setenta y un ";}
    elseif ($u==72) {$ru = "Setenta y dos ";}
    elseif ($u==73) {$ru = "Setenta y tres ";}
    elseif ($u==74) {$ru = "Setenta y cuatro ";}
    elseif ($u==75) {$ru = "Setenta y cinco ";}
    elseif ($u==76) {$ru = "Setenta y seis ";}
    elseif ($u==77) {$ru = "Setenta y siete ";}
    elseif ($u==78) {$ru = "Setenta y ocho ";}
    elseif ($u==79) {$ru = "Setenta y nueve ";}
    elseif ($u==80) {$ru = "Ochenta ";}

    elseif ($u==81) {$ru = "Ochenta y un ";}
    elseif ($u==82) {$ru = "Ochenta y dos ";}
    elseif ($u==83) {$ru = "Ochenta y tres ";}
    elseif ($u==84) {$ru = "Ochenta y cuatro ";}
    elseif ($u==85) {$ru = "Ochenta y cinco ";}
    elseif ($u==86) {$ru = "Ochenta y seis ";}
    elseif ($u==87) {$ru = "Ochenta y siete ";}
    elseif ($u==88) {$ru = "Ochenta y ocho ";}
    elseif ($u==89) {$ru = "Ochenta y nueve ";}
    elseif ($u==90) {$ru = "Noventa ";}

    elseif ($u==91) {$ru = "Noventa y un ";}
    elseif ($u==92) {$ru = "Noventa y dos ";}
    elseif ($u==93) {$ru = "Noventa y tres ";}
    elseif ($u==94) {$ru = "Noventa y cuatro ";}
    elseif ($u==95) {$ru = "Noventa y cinco ";}
    elseif ($u==96) {$ru = "Noventa y seis ";}
    elseif ($u==97) {$ru = "Noventa y siete ";}
    elseif ($u==98) {$ru = "Noventa y ocho ";}
    else            {$ru = "Noventa y nueve ";}
    return $ru; //Retornar el resultado
}

function decenas($d)
{
    if ($d==0)  {$rd = "";}
    elseif ($d==1)  {$rd = "Ciento ";}
    elseif ($d==2)  {$rd = "Doscientos ";}
    elseif ($d==3)  {$rd = "Trescientos ";}
    elseif ($d==4)  {$rd = "Cuatrocientos ";}
    elseif ($d==5)  {$rd = "Quinientos ";}
    elseif ($d==6)  {$rd = "Seiscientos ";}
    elseif ($d==7)  {$rd = "Setecientos ";}
    elseif ($d==8)  {$rd = "Ochocientos ";}
    else            {$rd = "Novecientos ";}
    return $rd; //Retornar el resultado
}
    function valorEnLetras($x)
    {
        if ($x<0) { $signo = "menos ";}
        else      { $signo = "";}
        $x = abs ($x);
        $C1 = $x;

        $G6 = floor($x/(1000000));  // 7 y mas

        $E7 = floor($x/(100000));
        $G7 = $E7-$G6*10;   // 6

        $E8 = floor($x/1000);
        $G8 = $E8-$E7*100;   // 5 y 4

        $E9 = floor($x/100);
        $G9 = $E9-$E8*10;  //  3

        $E10 = floor($x);
        $G10 = $E10-$E9*100;  // 2 y 1


        $G11 = round(($x-$E10)*100,0);  // Decimales
//////////////////////

        $H6 = $this->unidades($G6);

        if($G7==1 AND $G8==0) { $H7 = "Cien "; }
        else {    $H7 = $this->decenas($G7); }

        $H8 = $this->unidades($G8);

        if($G9==1 AND $G10==0) { $H9 = "Cien "; }
        else {    $H9 = $this->decenas($G9); }

        $H10 = $this->unidades($G10);

        if($G11 < 10) { $H11 = "0".$G11; }
        else { $H11 = $G11; }

/////////////////////////////
        if($G6==0) { $I6=" "; }
        elseif($G6==1) { $I6="Millón "; }
        else { $I6="Millones "; }

        if ($G8==0 AND $G7==0) { $I8=" "; }
        else { $I8="Mil "; }

        $I10 = "Bolivares Con ";
        $I11 = "/100 Centimos ";

        $C3 = $signo.$H6.$I6.$H7.$H8.$I8.$H9.$H10.$I10.$H11.$I11;

        return $C3; //Retornar el resultado

    }

}
