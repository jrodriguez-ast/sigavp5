<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

// Necesitaremos el modelo Fabricante para ciertas tareas.
use App\GroupModel;
use App\OperationModel;
use App\UserGroupModel;
use App\UserModel;

class PermisoController extends Controller {

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {


       // return response()->json(['status'=>'ok','data'=>GroupModel::all()], 200);

        $users = UserModel::where('username', '=', 'administrator')->get();
        $user = json_decode($users);
        $objuser = $user[0];
        $id_user = $objuser->id;

         $userGroup=UserModel::find($id_user);

         $grupo_user = $userGroup->group;

        $grupo_user = json_decode($grupo_user);

        $objusergrp = $grupo_user[0];
         $objusergrp->id;
        $grupo=GroupModel::find($objusergrp->id);

         $arr = $grupo->operation;
        //var_dump(json_decode($arr));
        foreach ($arr as $operacion) {
            print_r($operacion->accion);
        }


        //return response()->json(['status'=>'ok','data'=>$arr], 200);
        //$aviones = Fabricante::paginate(2);

        //return $aviones;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
        return "Se muestra formulario para crear un fabricante.";

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
        // return "Se muestra Fabricante con id: $id";
        // Buscamos un fabricante por el id.
        $fabricante=Fabricante::find($id);

        // Si no existe ese fabricante devolvemos un error.
        if (!$fabricante)
        {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['errors'=>array(['code'=>404,'message'=>'No se encuentra un fabricante con ese código.'])],404);
        }

        return response()->json(['status'=>'ok','data'=>$fabricante],200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
        return "Se muestra formulario para editar Fabricante con id: $id";
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}