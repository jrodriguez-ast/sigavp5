<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Consulta;

/**
 * Class RifController
 * @package App\Http\Controllers
 */
class RifController extends Controller
{

    /**
     * @param $rif
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show($rif)
    {
        $consulta = new Consulta();
        $response['seniat'] = $consulta->getSeniat(str_replace("-", "", $rif));
        $response['rnc'] = $consulta->getDataRNC(str_replace("-", "", $rif));
        $response['solvencia'] = $consulta->getSolvencia($rif);
        $response['status'] = 'ok';
        return response()->json($response);
    }

    /**
     * @param $rif
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getRNC($rif)
    {
        $consulta = new Consulta();
        $response['rnc'] = $consulta->getDataRNC(str_replace("-", "", $rif));
        return response()->json($response);
    }
}