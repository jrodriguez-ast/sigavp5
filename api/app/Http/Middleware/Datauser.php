<?php
namespace App\Http\Middleware;

//use Illuminate\Routing;
use App\Http\Controllers;
use App\Models\Acl\User;
use Closure;

//

//use Route;

class Datauser
{
    public function handle($request, Closure $next)
    {
        $headers = $request->header();
        if (!empty($headers['username'][0])) {
            $username = $headers['username'][0];
            $users = User::where('username', '=', $username)->get();
            $user = json_decode($users);
            $objuser = $user[0];
            $id_user = $objuser->id;
            session(['userId' => $id_user]);
        }

        return $next($request);


    }
}