<?php
namespace App\Http\Middleware;
//use Illuminate\Routing;
use Illuminate\Http\Request;
use App\Http\Controllers;
//
use App\GroupModel;
use App\OperationModel;
use App\UserGroupModel;
use App\UserModel;
//use Route;
use Closure;

/**
 * Class Permisos. El Middleware captura el nombre de usuario desde la cabecera de la petición
 * Luego de esto obtiene las acciones a las que tiene permiso
 * Si el usuario con sesión iniciada no tiene permiso para ejecutar la acción actual entonces retorna
 * un mensaje de error
 * @package App\Http\Middleware
 * @author Maykol Purica
 * TODO Queda pendiente re-factorizar este código para hacerlo mas elegante haciendo un uso mas eficiente del ORM Eloquent
 */
class Permisos
{
    public function handle($request, Closure $next)
    {
        $ruta_accion = $request->route()->getAction();
        $headers = $request->header();
        if(!empty($headers['username'][0]))
        {
        $username = $headers['username'][0];
        }else {
            return "No ha iniciado sesion";
        }
        $users = UserModel::where('username', '=', $username)->get();
        $user = json_decode($users);
        $objuser = $user[0];
        $id_user = $objuser->id;
        $userGroup=UserModel::find($id_user);
        $grupo_user = $userGroup->group;
        $grupo_user = json_decode($grupo_user);
        $objusergrp = $grupo_user[0];
        $objusergrp->id;
        $grupo=GroupModel::find($objusergrp->id);
        $arr = $grupo->operation;
        foreach ($arr as $operacion) {
            if($operacion->accion != null) {
                $operaciones[] = $operacion->accion;
            }
        }
        if (in_array($ruta_accion['as'], $operaciones)) {
            return $next($request);
        }else{
            echo "No tiene permisos";
        }

    }
}