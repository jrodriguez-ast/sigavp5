<?php namespace App\Http\Middleware;


use App\Models\Acl\Auditoria;
use Closure;


class AuditoriaMiddleware
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Registra la entrada solo para operaciones de tipo GET, ya que las demás opciones de mueven por base de datos.
        if ($request->method() == 'GET') {
            $auditoria = new Auditoria();
            $auditoria->registrarAcceso();
        }
        return $next($request);
    }

}
