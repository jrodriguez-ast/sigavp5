<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
//Guardar archivos en el servidor.
Route::resource('storage/create', 'StorageController@save');

//Rutas para los cargos
Route::resource('cargos', 'CargosController');

//Consulta de rif
Route::get('rif/show/{rif}', 'RifController@show');
Route::get('rif/rnc/{rif}', 'RifController@getRNC');

// Gestión de PDFs
Route::group(['prefix' => 'pdf'], function () {
    Route::get('header', 'PdfController@header');
    Route::get('footer', 'PdfController@footer');
    Route::get('print/{modelo}/{modelo_id}', 'PdfController@_print');
});

Route::resource('operation', 'GroupOperationController');//group_operation
Route::get('group_operation', 'GroupOperationController@group_operation');
Route::post('add_remove_operation', 'GroupOperationController@add_remove_operation_post');