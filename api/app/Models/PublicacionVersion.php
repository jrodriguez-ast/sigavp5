<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicacionVersion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.ordenes_publicacion_versiones';

    protected $fillable = [
        "orden_publicacion_id",
        "servicio_id",
        'version_nombre',
        'version_file',
        'version_publicacion_fecha',
        'version_publicacion_fecha_inicio',
        'version_publicacion_fecha_fin',
        'tipo_publicacion_name',
        'tipo_publicacion_id',
        'version_segundos',
        'version_apariciones',
        'usuario_id',
        'monto_servicio',
        'medio_id'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'created_at', 'updated_at'];


    /**
     * Relación con modelo publicación
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function publicacion()
    {
        return $this->belongsTo('App\Models\Publicacion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function versioncondicion()
    {
        return $this->hasMany('App\Models\PublicacionVersionCondicion', 'ordenes_publicacion_versiones_id');
    }

    /**
     * Relación con modelo Servicio
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function servicio()
    {
        return $this->belongsTo('App\Models\Servicio');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medio()
    {
        return $this->belongsTo('App\Models\Medio');
    }

    /**
     * Provee los datos de una version especifica
     *
     * @access public
     * @param integer $version_id Identificador de la version
     *
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:25 AM
     */
    public static function  version_and_condicion($version_id)
    {
        $InstPublicacionVersion = new PublicacionVersion();

        $PublicacionVersion = $InstPublicacionVersion->with([
            'versioncondicion',
            'versioncondicion.condicion',
            'versioncondicion.detallecondicion'
        ])->find($version_id);
        return $PublicacionVersion;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function incidencias()
    {
        return $this->hasMany('App\Models\PublicacionIncidencias', 'version_id');
    }

}