<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TechoConsumo extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "public.techo_consumo";

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        'id',
        'techo',
        //'relacion',
        'relacion_id',
        'created_at',
        //'deleted_at',
        'updated_at',
        'activo'
    ];

    /**
     * Trae la relación con techo de consumo de acuerdo a $relacionado
     *
     * @param $id
     * @param $relacion
     * @return mixed
     */
    private function getAll($id, $relacion)
    {
        return $this->where(['relacion' => $relacion, 'relacion_id' => $id])->orderBy('created_at','desc')->get();
    }

    /**
     * Relación con Cliente
     * @param $id
     * @return mixed
     */
    public function cliente($id)
    {
        return $this->getAll($id, 'clientes');
    }

    /**
     * Relación con Proveedor (Medio)
     * @param $id
     * @return mixed
     */
    public function proveedor($id)
    {
        return $this->getAll($id, 'medios');
    }

    /**
     * Relación con Medio (Servicio)
     * @param $id
     * @return mixed
     */
    public function medio($id)
    {
        return $this->getAll($id, 'servicios');
    }

    /**
     * @return array
     */
    public function getDates()
    {
        return [];
    }

}



