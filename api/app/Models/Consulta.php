<?php

namespace App\Models;

use Goutte\Client;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Consulta
 * @package App\Models
 */
class Consulta extends Model
{
    var $response;

    /**
     * @param $rif
     * @return array $this->response
     */
    public function getDataRNC($rif)
    {
        $client = new Client();
        $this->response = [
            'rif' => '',
            'razon_social' => '',
            'estatus' => '',
            'nivel' => '',
            'contacto' => '',
            'telefonos' => '',
        ];

        try {
            $crawler = $client->request(
                'GET',
                "http://rncenlinea.snc.gob.ve/reportes/resultado_busqueda?p=1&rif={$rif}&search=RIF"
            );
            $configurationRows = $crawler->filter('html table table tr');
            $configurationRows->each(function ($configurationRow, $index) {
                if (intval($index) == 1 && count($configurationRow->filter('td')) > 2) {
                    $this->response = [
                        'rif' => trim($configurationRow->filter('td')->eq(0)->text()),
                        'razon_social' => $configurationRow->filter('td')->eq(1)->text(),
                        'estatus' => $configurationRow->filter('td')->eq(2)->text(),
                        'nivel' => $configurationRow->filter('td')->eq(3)->text(),
                        'contacto' => $configurationRow->filter('td')->eq(4)->text(),
                        'telefonos' => $configurationRow->filter('td')->eq(5)->text(),
                    ];
                }
            });
        } catch (\Exception $e) {
            $this->response = [
                'rif' => '',
                'razon_social' => '',
                'estatus' => '',
                'nivel' => '',
                'contacto' => '',
                'telefonos' => '',
                'status' => 'fail',
            ];
        }

        if ($this->response['rif'] != null) {
            $this->response['status'] = 'ok';
        }
        return $this->response;
    }

    /**
     * @param $rif
     * @return array|null
     */
    public function getSolvencia($rif)
    {
        $client = new Client();
        try {
            $crawler = $client->request(
                'POST',
                'http://app.gestion.minpptrass.gob.ve/gestion/mod_herramientas/reporte_ultima_solvencia.php',
                ['btnAceptar' => 'Buscar', 'rif' => $rif]
            );

            $configurationRows = $crawler->filter('html table');

            $this->response = null;
            $configurationRows->each(function ($configurationRow, $index) {
                if (intval($index) == 1) {
                    $configurationRow2 = $configurationRow->filter('tr');
                    $configurationRow2->each(function ($configurationRow3, $index2) {

                        if (intval($index2) == 3) {
                            if (!empty($configurationRow3)) {
                                $this->response = array(
                                    'rif' => $configurationRow3->filter('td')->eq(0)->text(),
                                    'razon_social' => $configurationRow3->filter('td')->eq(1)->text(),
                                    'estatus' => $configurationRow3->filter('td')->eq(2)->text(),
                                    'ultima_solicitud' => $configurationRow3->filter('td')->eq(3)->text(),
                                    'status' => 'ok'
                                );
                            }

                            if ($this->response['rif'] == '' || $this->response['razon_social'] == '' || $this->response['estatus'] == '' || $this->response['ultima_solicitud'] == '') {
                                $this->response['status'] = 'fail';
                            }
                        }
                    });

                }
            });
        } catch (\Exception $e) {
            $this->response = array(
                'rif' => '',
                'razon_social' => '',
                'estatus' => '',
                'ultima_solicitud' => '',
                'status' => 'fail',

            );
        }
        return $this->response;
    }

    /**
     * @param $rif
     * @return array|mixed
     */
    public function getSeniat($rif)
    {
        $this->response = [
            'nombre' => '',
            'agenteretencioniva' => '',
            'contribuyenteiva' => '',
            'tasa' => '',
            'status' => 'fail'
        ];

        try {
            $url = "http://contribuyente.seniat.gob.ve/getContribuyente/getrif?rif={$rif}";
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_TIMEOUT, 60*2);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            $result = curl_exec($ch);

            if ($result) {
                try {
                    if (substr($result, 0, 1) != '<') {
                        // Contribuyente no registrado.
                        $this->response['code_result'] = 452;
                        $this->response['message'] = 'El contribuyente no esta registrado.';
                        return $this->response;
                    }

                    $xml = simplexml_load_string($result);
                    if (!is_bool($xml)) {
                        $elements = $xml->children('rif');
                        $this->response = [];
                        foreach ($elements as $key => $node) {
                            $index = strtolower($node->getName());
                            $this->response[$index] = (string)$node;
                        }
                        $this->response['code_result'] = 1;
                        $this->response['message'] = 'Consulta satisfactoria';
                        $this->response['status'] = 'ok';
                    }
                } catch (Exception $e) {
                    $exception = explode(' ', $result, 2);
                    $responseJson['code_result'] = (int)$exception[0];
                }
            } else {
                // No hay conexión a internet
                $this->response['code_result'] = 0;
                $this->response['message'] = 'Sin Conexión a Internet';
            }

            $this->response = $this->processSeniatData($this->response);
        } catch (\Exception $e) {
            $this->response = [
                'nombre' => '',
                'agenteretencioniva' => '',
                'contribuyenteiva' => '',
                'tasa' => '',
                'status' => 'fail'
            ];
        }
        return $this->response;
    }

    /**
     * Elimina la información que viene luego del paréntesis de los servicios de SENIAT.
     *
     * @param $data
     * @return mixed
     * @author  Jose Rodriguez
     */
    private function processSeniatData($data)
    {
        $data['nombre'] = substr($data['nombre'], 0, strrpos($data['nombre'], '('));
        $data['nombre'] = trim($data['nombre']);
        return $data;
    }

}
