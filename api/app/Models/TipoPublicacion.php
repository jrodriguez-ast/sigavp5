<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class TipoPublicacion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'tipos_publicaciones';

    protected $fillable = [
        "nombre",
        "descripcion",
        'usuario_id',
        'id'
    ];

    protected $hidden = [ 'created_at', 'updated_at'];

    /**
     * Relación con Condición
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function condiciones()
    {
        return $this->hasMany('App\Models\Condicion');
    }


    /**
     * Relación con modelo Medio
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function medios()
    {
        return $this->hasMany('App\Models\Medio');
    }

    /**
     * Relación con modelo TipoIncidencia
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tiposIncidencias()
    {
        return $this->hasMany('App\Models\TipoIncidencia');
    }

    /**
     * Relación con modelo TipoIncidencia
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function genericConfiguration()
    {
        return $this->hasMany('App\Models\Utilities\GenericConfiguration');
    }
    public static function listado_for_medios($medio_id)
    {
        return DB::select("
                SELECT tipo.id,tipo.nombre
                FROM medios as m
                INNER JOIN servicios as s ON s.medio_id = m.id
                INNER JOIN tipos_publicaciones as tipo ON s.tipo_publicacion_id = tipo.id
                WHERE m.id=:id
                GROUP BY  tipo.id, tipo.nombre
                ORDER BY tipo.nombre ASC;
        ",
            ['id' => $medio_id]
        );

    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function servicios()
    {
        return $this->hasMany('App\Models\Servicio','tipo_publicacion_id');
    }

    public function contratoPauta(){
        return $this->hasMany('App\Models\ContratoPauta','tipo_publicacion_id');
    }

}
