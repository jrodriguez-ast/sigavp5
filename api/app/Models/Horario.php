<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Horario extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'horarios';

    protected $fillable = [
        "descripcion"
    ];

    protected $hidden = [
        "usuario_id",
        "created_at",
        "updated_at"
    ];


    /**
     * Relación con el modelo ContactoCliente
     */
    public function contactosClientes()
    {
        $this->hasMany('App\Models\ContactoCliente');
    }

    /**
     * Relación con el modelo ContactoMedio
     */
    public function contactosMedios()
    {
        $this->hasMany('App\Models\ContactoMedio');
    }

}
