<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CotizacionOrdenServicio extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'cotizaciones_orden_servicio';

    protected $fillable = [
        "cliente_id",
        "cotizacion_id",
        "file",
        "via_recepcion_id",
        "fecha_envio",
        "observaciones",
        "usuario_id"
    ];

    protected $hidden = [];


    /**
     * Relación con modelo Cotización
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cotizacion()
    {
        return $this->belongsTo('App\Models\Cotizacion');
    }

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }

    public function getDates()
    {
        return array();
    }

    /**
     * Relación con el modelo RecepciónOrdenServicio
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function viaRecepcionOrdenServicio()
    {
        return $this->belongsTo ('App\Models\RecepcionOrdenServicio', 'via_recepcion_id');
    }
}
