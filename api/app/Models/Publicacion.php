<?php namespace App\Models;

use App\Models\Utilities\GenericConfiguration;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Publicacion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.ordenes_publicacion';

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        'id',
        'codigo',
        'orden_publicacion_id',
        'fecha_creacion',
        'fecha_expiracion',
        'descuento',
        'con_impuestos',
        'porcentaje_impuesto',
        'es_terminada',
        'subtotal',
        'total_impuesto',
        'total_descuento',
        'total',
        'fue_enviada',
        'motivo_descuento',
        'aprobada',
        'subtotal_con_descuento',
        'por_facturar',
        'cliente_id',
        'porcentaje_avp',
        'medio_id',
        'factura_id',
        'firmante_nombre',
        'firmante_cargo'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'created_at', 'updated_at'];

    protected $casts = [
        'descuento' => 'integer',
        'con_impuestos' => 'boolean',
        'porcentaje_impuesto' => 'float',
        'es_terminada' => 'boolean',
        'subtotal' => 'float',
        'total_impuesto' => 'float',
        'total_descuento' => 'float',
        'total' => 'float',
        'fue_enviada' => 'boolean',
        'aprobada' => 'boolean',
        'subtotal_con_descuento' => 'float',
        'por_facturar' => 'boolean',
        'porcentaje_avp' => 'float'
    ];

    /**
     * Relación con modelo publicación_estatus_estatus
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function publicacion_estatus_estatus()
    {
        return $this->hasMany('App\Models\PublicacionEstatusEstatus', 'orden_publicacion_id');
    }

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente');
    }

    /**
     * Relación con modelo Medio
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medio()
    {
        return $this->belongsTo('App\Models\Medio', 'medio_id');
    }

    /**
     * Relación con el modelo PublicaciónVersion
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function version()
    {
        return $this->hasMany('App\Models\PublicacionVersion', 'orden_publicacion_id');
    }

    /**
     * Relación con el modelo PublicaciónVersion
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function incidencias()
    {
        return $this->hasMany('App\Models\PublicacionIncidencias', 'orden_publicacion_id');
    }

    /**
     * Relación con modelo Cotización
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cotizacion()
    {
        return $this->belongsTo('App\Models\Cotizacion', 'cotizacion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function factura()
    {
        return $this->belongsTo('App\Models\Factura', 'factura_id');
    }

    /**
     * @return mixed
     */
    public function status()
    {
        return $this->belongsToMany(
            'App\Models\PublicacionEstatus',
            'ordenes_publicacion_estatus_estatus',
            'orden_publicacion_id',
            'orden_publicacion_estatus_id'
        )->latest();
    }

    /**
     * Provee los datos de la publicación y sus datos asociados
     *
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|Model|\Illuminate\Support\Collection|null|static
     */
    public static function  data_for_publicacion($id)
    {
        $InstPublicacion = new Publicacion();

        $publicacion = $InstPublicacion->with(array(
            'cliente',
            //'user',
            'cotizacion',
            'cliente.contactos',
            'cliente.contactos.cargo',
            'version',
            //'version.medio',
            'version.incidencias',
            'version.servicio.tipoPublicacion',
            'version.versioncondicion.condicion',
            'version.versioncondicion.detallecondicion',
            'medio',
            'medio.contactos',
            'incidencias.version',
            'incidencias.tipo_incidencia',
            'incidencias.user'
        ))->find($id);

        if ($publicacion) {
            $publicacion['all_estatus'] = Publicacion::getStatuses($id);
            $publicacion['current_estatus'] = Publicacion::getStatuses($id, TRUE);
            $publicacion['versiones'] = [];

            $sub_total_versiones = 0;
            if (!empty($publicacion['version'])):
                foreach ($publicacion['version'] as $key => &$version):
                    $calculo_apariciones_segundos = 0;

                    //Se obtienen las condiciones agrupadas de acuerdo a cada version
                    if (!empty($version['condiciones'])):
                        $version['condiciones'] = json_decode(json_encode(str_getcsv(trim($version['condiciones'], '{}'))), true);
                    endif;
                    //Fix Jose Rodriguez
                    //$valor_con_recargos = (new PublicacionVersionCondicion())->calcularValorConRecargos($version['ordenes_publicacion_versiones_id']);
                    $valor_con_recargos = (new PublicacionVersionCondicion())->calcularValorConRecargos($version['id']);
                    $apariciones = (empty($version['version_apariciones'])) ? 1 : $version['version_apariciones'];
                    $segundos = (empty($version['version_segundos'])) ? 1 : $version['version_segundos'];
                    $calculo_apariciones_segundos = ($valor_con_recargos * $segundos * $apariciones);
                    //Se obtiene la tarifa base de las condiciones pertenecientes a una version
                    //y se aplica el calculo de acuerdo a las apariciones y los segundos
                    //if (!empty($value['tarifa_base'])):
                    //    $calculo_apariciones_segundos = ($value['tarifa_base'] * $value['version_segundos'] * $value['version_apariciones']);
                    //endif;
                    //Fin Fix Jose Rodriguez

                    //Se le aplica formato a la fecha de publicación
                    if (!empty($version['version_publicacion_fecha'])):
                        $date = date_create($version['version_publicacion_fecha']);
                        $version['version_publicacion_fecha'] = date_format($date, 'd/m/Y');
                    else:
                        $version['version_publicacion_fecha'] = 'Sin Información';
                    endif;

                    $version['subtotal'] = $calculo_apariciones_segundos;
                    $finicio = $version['version_publicacion_fecha_inicio'];
                    $ffin = $version['version_publicacion_fecha_fin'];

                    //Aquí se obtiene los dias entre las fecha de publicación
                    //para aplicar al subtotal por versiones.
                    if (!empty($finicio) && !empty($ffin)):
                        $datetime1 = date_create($finicio);
                        $datetime2 = date_create($ffin);
                        $interval = date_diff($datetime1, $datetime2);
                        $version['subtotal'] = $version['subtotal'] * (1 + (Integer)$interval->format('%a'));
                    endif;
                    $sub_total_versiones += $version['subtotal'];
                endforeach;
                $publicacion['msubtotal'] = $sub_total_versiones;
            endif;

            $date_expiration = date_create($publicacion['fecha_expiracion']);
            $publicacion['fecha_expiracion'] = date_format($date_expiration, 'd/m/Y');

            $date_create = date_create($publicacion['fecha_creacion']);
            $publicacion['fecha_creacion_label'] = date_format($date_create, 'd/m/Y');

            if (sizeof($publicacion['incidencias']) > 0):
                foreach ($publicacion['incidencias'] as $key => $version):
                    $date = date_create($version['created_at']);
                    $publicacion['incidencias'][$key]['created_at'] = date_format($date, 'd/m/Y h:i A');
                endforeach;
            endif;
        }
        $configuracion = GenericConfiguration::where(array('_label' => 'cotizacion_validez'))->get();

        $validez_dias = ($configuracion ? $configuracion[0]['value'] : '0');

        $publicacion['dias_validez'] = $validez_dias;
        return $publicacion;
    }


    /**
     * Provee el calculo de la publicación
     *
     * @param null $publicacion_id
     * @return bool
     */
    public static function calculate_publicacion($publicacion_id = null)
    {
        if (empty($publicacion_id))
            return false;

        $InstPublicacion = new Publicacion();

        //Se obtienen los datos de la cotización
        $getSubtotal = $InstPublicacion->data_for_publicacion($publicacion_id);

        //Iva
        $p_iva = Iva::where('activo', true)->firstOrFail();
        //Impuesto AVP
        $p_avp = RecargoGestion::where('activo', true)->firstOrFail();

        //Obtengo el subtotal
        $subtotal = (!empty($getSubtotal['msubtotal']) ? $getSubtotal['msubtotal'] : 0.00);
        //Iva
        $porcentaje_impuesto = ($p_iva ? $p_iva->iva : 0.00);
        //Impuesto AVP
        $porcentaje_avp = ($p_avp ? $p_avp->porcentaje : 0.00);

        //Aplico impuesto AVP al subtotal
        $impuesto_avp = ($subtotal * $porcentaje_avp / 100);

        //Subtotal con el recargo AVP aplicado
        $subtotal_con_avp = $subtotal + $impuesto_avp;

        //Aplico impuesto IVA al subtotal ya con recargo AVP incluido
        $impuesto_iva = ($subtotal_con_avp * $porcentaje_impuesto / 100);

        //Total a pagar
        $total = $subtotal_con_avp + $impuesto_iva;

        //Datos de salida
        $response = array(
            'subtotal' => $subtotal_con_avp,
            'porcentaje_impuesto' => $porcentaje_impuesto,
            'total_impuesto' => $impuesto_iva,
            'porcentaje_avp' => $porcentaje_avp,
            'total_impuesto_avp' => $impuesto_avp,
            'total' => $total
        );

        //Actualizo la cotización con los datos actuales
        $publicacion = $InstPublicacion->find($publicacion_id);

        $publicacion->subtotal = $response['subtotal'];
        $publicacion->porcentaje_impuesto = $response['porcentaje_impuesto'];
        $publicacion->total_impuesto = $response['total_impuesto'];
        $publicacion->porcentaje_avp = $response['porcentaje_avp'];
        $publicacion->total_impuesto_avp = $response['total_impuesto_avp'];
        $publicacion->total = $response['total'];
        $publicacion->usuario_id = session()->get('userId');
        return $publicacion->save();
    }

    /**
     * Permite cambiar el estatus de la publicación
     *
     * @param integer $publicacion_id
     * @param null $estatus
     * @param null $usuario_id
     * @return bool|static
     */
    public static function change_estatus($publicacion_id = null, $estatus = null, $usuario_id = null)
    {
        if (empty($estatus) && empty($publicacion_id))
            return false;

        $insert = false;

        $id_estatus = PublicacionEstatus::where(array('estatus' => $estatus))->pluck('id');
        if ($id_estatus):
            $data_for_insert = array(
                'orden_publicacion_id' => $publicacion_id,
                'orden_publicacion_estatus_id' => $id_estatus,
                'usuario_id' => $usuario_id
            );
            $insert = PublicacionEstatusEstatus::create($data_for_insert);

        endif;
        return $insert;
    }

    /**
     * Devuelve los status de la publicación.
     * Si $current es TRUE, devuelve el estatus actual.
     * @param integer $publicacion_id
     * @param bool $current
     * @return mixed
     * @author Jose Rodriguez
     */
    private static function getStatuses($publicacion_id, $current = FALSE)
    {
        $statusQuery = DB::table('ordenes_publicacion_estatus_estatus AS e_e')
            ->join('ordenes_publicacion_estatus AS e', 'e_e.orden_publicacion_estatus_id', '=', 'e.id')
            ->join('acl.users AS u', 'e_e.usuario_id', '=', 'u.id')
            ->orderBy('id', 'desc')
            ->select([
                'e_e.*',
                'e.estatus',
                'e.descripcion',
                DB::raw("(u.first_name || ' ' || u.last_name) AS creator")
            ]);

        if ($current) {
            $estatus_id = DB::table('ordenes_publicacion_estatus_estatus')
                ->where('orden_publicacion_id', $publicacion_id)
                ->max('id');
            return $statusQuery->where('e_e.id', $estatus_id)->get();
        }

        return $statusQuery->where('e_e.orden_publicacion_id', $publicacion_id)->get();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function facturaOrdenPublicacion()
    {
        return $this->belongsToMany('App\Models\Factura', 'factura_orden_publicacion', 'orden_id', 'factura_id');
    }
}