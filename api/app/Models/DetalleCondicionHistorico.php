<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleCondicionHistorico extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'condiciones_detalles_historico';

    protected $hidden = [
        'updated_at',
        'deleted_at'];

    /**
     * Relación con medio
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medio()
    {
        return $this->belongsTo('App\Models\Medio', 'medio_id');
    }

    /**
     * Relación con servicio
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function servicio()
    {
        return $this->belongsTo('App\Models\Servicio', 'servicio_id');
    }

    /**
     * Relación con condición
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function condicion()
    {
        return $this->belongsTo('App\Models\Condicion', 'condicion_id');
    }

    /**
     * Relación con detalle condición
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
     public function detalle_condicion()
    {
        return $this->belongsTo('App\Models\DetalleCondicion', 'condicion_detalle_id');
    }
    /**
     * Relación con User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }

    public function getDates()
    {
        return array();
    }
}