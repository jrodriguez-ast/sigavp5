<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author  Maykol Purica <puricamaykol@gmail.com>
 *          Class Contrato
 * @package App\Models
 */
class Contrato extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'contratos';
    public $timestamps = false;

    protected $fillable = [
        "cliente_id",
        "es_postpago",
        "tarifa",
        "regimen_pago_id",
        "cuota",
        "cerrado",
        "activo",
        'firmante_nombre',
        'firmante_cargo',
        'codigo',
        "usuario_id",
        "tipo_contrato_id",
        "contratos_id",
        "created_at",
        "techo_consumo",
        "descripcion",
        "fecha_creacion",
        'fecha_inicio',
        'fecha_fin',
        'por_publicacion',
        'monto_iva',
        'balancear'
    ];

    protected $hidden = [
        "updated_at",
        "deleted_at",
    ];

    protected $appends = ['total'];

    /**
     * Relación con modelo de Cliente
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balance()
    {
        return $this->hasMany('App\Models\Balance');
    }

    /**
     * Esta función se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacío entonces ninguno lo sera
     * @return array
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function getDates()
    {
        return array();
    }

    /**
     * Relación con el modelo Regimen de pago
     *
     * @return \Illuminate\Database\Eloquent\Relations\belongsTo
     */
    public function regimen()
    {
        return $this->belongsTo('App\Models\Regimenpago','regimen_pago_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipo()
    {
        return $this->belongsTo('App\Models\TipoContrato', 'tipo_contrato_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo('App\Models\Contrato', 'contratos_id');
    }

    /**
     * @return mixed
     */
    public function children()
    {
        return $this->hasMany('App\Models\Contrato', 'contratos_id')->orderBy('id', 'desc');
    }
    /**
     * @author Carlos Blanco <cebs923@@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cuotas()
    {
        return $this->hasMany('App\Models\ContratoCuotas');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function pautas()
    {
        return $this->hasMany('App\Models\ContratoPauta');
    }

    /**
     * Movimiento de Balance por Tipo de Publicación
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balancePorTipoPublicacion()
    {
        return $this->hasMany('App\Models\BalancePorTipoPublicacion', 'contrato_id');
    }

    /**
     * Campo mutado para obtener el total a gastar en tipo de publicación
     * @return mixed
     */
    public function getTotalAttribute(){
        return $this->tarifa + $this->monto_iva;
    }
}