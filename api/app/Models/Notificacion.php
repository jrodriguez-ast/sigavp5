<?php

namespace App\Models;

use App\Models\Utilities\GenericConfiguration;
use DateInterval;
use DateTime;
use Illuminate\Database\Eloquent\Model;


//class Notification extends Model
class Notificacion extends Model
{
    protected $table = 'notifications';

    protected $fillable = [
        'chekk',
        'tit',
        'des',
        'lin',
        'titu_icon',
        'tipo_not_ico',
        'ids',
    ];
    protected $hidden = [
        'created_at',
        'updated_at',
    ];
    public $timestamps = false;

    public function facturaVencida()
    {
        $facturas = Factura::with('publicacion.cliente')->where('restante', '>', 0)->get()->toArray();
        $facturas_array = [
            'facturas_por_vencer' => array(),
            'facturas_vencidas' => array(),

        ];

        foreach ($facturas as $factura):

            $fecha = new DateTime($factura['created_at']);
            $hoy = new DateTime();
            $strhoy = $hoy->format('d-m-Y');
            //$vencimiento = $fecha->add(new DateInterval('P' . GenericConfiguration::where("_label", "factura_validez")->pluck('value') . 'D'));
            $strvence = $fecha->format('d-m-Y');
            $fecha->sub(new DateInterval('P5D'));
            $stralert = $fecha->format('d-m-Y');
            if (strtotime($strhoy) >= strtotime($stralert) || strtotime($strhoy) < strtotime($strvence)):
                $notificacion = [
                    'tit' => 'Factura por vencer',
                    'des' => 'La factura de código ' . $factura['codigo'] . ' está a punto de vencerse.',
                    'lin' => 'admin.detalle_cliente.facturas_detail',
                    'titu_icon' => 'Factura por vencer',
                    'tipo_not_ico' => 'fa fa-calculator',
                    'ids' => json_encode([
                        'id' => $factura['publicacion'][0]['cliente']['id'],
                        'facturaID' => $factura['id'],
                    ])
                ];
                $facturas_array['facturas_por_vencer'][] = $this->create($notificacion);
            endif;
            if (strtotime($strhoy) >= strtotime($strvence)):
                $notificacion = [
                    'tit' => 'Factura vencida',
                    'des' => 'La factura de código ' . $factura['codigo'] . ' está vencida.',
                    'lin' => 'admin.detalle_cliente.facturas_detail',
                    'titu_icon' => 'Factura vencida',
                    'tipo_not_ico' => 'fa fa-calculator',
                    'ids' => json_encode([
                        'id' => $factura['publicacion'][0]['cliente']['id'],
                        'facturaID' => $factura['id'],
                    ])
                ];
                $facturas_array['facturas_vencidas'][] = $this->create($notificacion);
            endif;
        endforeach;
        return $facturas_array;
    }

}