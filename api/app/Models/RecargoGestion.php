<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecargoGestion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "recargo_gestion";

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable =
        [
            'id',
            'porcentaje',
            'fecha',
            'activo',
            'usuario_id',

        ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at', 'updated_at','deleted_at'];
}