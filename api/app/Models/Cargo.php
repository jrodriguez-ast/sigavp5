<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Cargo extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'cargos';

    protected $fillable = [
        "descripcion"
    ];

    protected $hidden = [
        "usuario_id",
        "created_at",
        "updated_at"
    ];


    /**
     * Relación con el modelo ContactoCliente
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contactosClientes()
    {
        return $this->hasMany('App\Models\ContactoCliente');
    }

    /**
     * Relación con el modelo ContactoMedio
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contactosMedios()
    {
        return $this->hasMany('App\Models\ContactoMedio');
    }
}
