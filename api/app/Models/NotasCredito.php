<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NotasCredito extends Model {

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'public.notas_credito';
    /**
     * Campos que pueden ser llenados
     * @type array
     */
    protected $fillable = [
        'codigo',
        'id',
        'nombre_firmante',
        'cargo_firmante',
        'usuario_id',
        'monto',
        'n_control',
        'descripcion',
        'created_at',
        'factura_id',
        'estatus',
        'fecha_motorizado',
        'fecha_cliente',
        'cliente_id',
    ];
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [

        'updated_at',
        'deleted_at'
    ];

    /**
     * Esta función se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacío entonces ninguno lo sera
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return array
     */
    public function getDates()
    {
        return array();
    }

    /**
     * Relación con Factura.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function factura()
    {
        return $this->belongsTo('App\Models\Factura');
    }

     /**
     * Relación con Cliente.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente');
    }
}
