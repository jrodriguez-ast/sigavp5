<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MedioCondicion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'medios_condiciones';

    protected $fillable = [
        "nombre",
        "monto",
        "operacion"
    ];

    protected $hidden = ['usuario_id', 'created_at', 'updated_at'];

    /**
     * Relación con DetalleCondición
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function detalleCondicion()
    {
        return $this->belongsTo('App\Models\DetalleCondicion');
    }

    /**
     * Relación con Servicio
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function servicio()
    {
        return $this->belongsTo('App\Models\Servicio');
    }
}
