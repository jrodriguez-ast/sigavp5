<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContratoPauta extends Model {

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'contrato_pauta';

    public $timestamps = false;

    protected $fillable = [
        'contrato_id',
        'tipo_publicacion_id',
        'fecha',
        'monto',
        'monto_iva'
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    protected $appends = ['total'];

    /**
     * Esta función se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacío entonces ninguno lo sera
     * @return array
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function getDates()
    {
        return array();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contrato()
    {
        return $this->belongsTo('App\Models\Contrato', 'contrato_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipo(){
        return $this->belongsTo('App\Models\TipoPublicacion', 'tipo_publicacion_id');
    }

    /**
     * Campo mutado para obtener el total a gastar en tipo de publicación
     * @return mixed
     */
    public function getTotalAttribute(){
        return $this->monto + $this->monto_iva;
    }

}
