<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Condicion extends Model
{
    use SoftDeletes;

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'condiciones';

    protected $fillable = [
        'nombre',
        'usuario_id',
        'es_porcentaje',
        'es_base',
        'deleted_at',
        'servicio_id'];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [
        'created_at',
        'updated_at'];

    /**
     * Relación con DetalleCondición
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detalles()
    {
        return $this->hasMany('App\Models\DetalleCondicion');
    }

    /**
     * Relación con TipoPublicación
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function servicio()
    {
        return $this->belongsTo('App\Models\Servicio','servicio_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function negociacion_condicion()
    {
        return $this->hasMany('App\Models\NegociacionesCondiciones', 'condicion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function negociacion_condicion_detalle()
    {
        return $this->hasMany('App\Models\NegociacionesCondicionesDetalles', 'condicion_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function publicaciones()
    {
        return $this->hasMany('App\Models\PublicacionVersionCondicion', 'condicion_id');
    }

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }
}