<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class RecepcionOrdenServicio extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'recepcion_orden_servicio';

    protected $fillable = [
        "nombre",
        "usuario_id"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

    /**
     * Relación con el modelo CotizaciónOrdenServicio
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cotizacionOrden ()
    {
        return $this->hasMany ('App\Models\CotizacionOrdenServicio', 'via_recepcion_id');
    }
}
