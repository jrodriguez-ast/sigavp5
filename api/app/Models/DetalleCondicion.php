<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleCondicion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'condiciones_detalles';

    protected $fillable = [
        "nombre",
        'tarifa',
        'usuario_id',
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'];

    /**
     * Relación con Condición
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function condicion()
    {
        return $this->belongsTo('App\Models\Condicion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function negociaciones_detalles()
    {
        return $this->hasMany('App\Models\NegociacionesCondicionesDetalles', 'condicion_detalle_id');
    }

    /**
     * Relación con Cotizaciones
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cotizacion_servicio_detalles()
    {
        return $this->hasMany('App\Models\CotizacionServicioCondicion', 'detalle_condicion_id');
    }

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }
}