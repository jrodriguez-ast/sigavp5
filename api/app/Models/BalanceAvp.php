<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalanceAvp extends Model {

	/**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'balances_avp';

    protected $fillable = [
        'medio_id','usuario_id','negociacion_id','saldo','debito',
        'cargo','techo_consumo','observaciones','created_at'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['updated_at', 'deleted_at'];
    /**
     * Relación con modelo de Medio
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medio()
    {
        return $this->belongsTo('App\Models\Medio');
    }

    /**
     * Relación con Usuario.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Acl\User');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function negociacion()
    {
        return $this->belongsTo('App\Models\Negociaciones');
    }
    /**
     * Esta función se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacío entonces ninguno lo sera
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return array
     */
    public function getDates()
    {
        return array();
    }
}
