<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoMedio extends Model {

    protected $table = 'tipo_medio';

    protected $fillable = [
        "nombre",
        "id"
    ];

//    protected $hidden = ['id'];

    /**
     * Relación con Condición
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function medio()
    {
        return $this->hasMany('App\Models\Medio');
    }

}
