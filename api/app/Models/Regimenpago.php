<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Regimenpago extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'regimen_pago';

    protected $fillable = [
        'id',
        'nombre',
//        'usuario_id'
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * Relación con el modelo contrato
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contrato()
    {
        return $this->hasMany('App\Models\Contrato', 'regimen_pago_id');
    }
}
