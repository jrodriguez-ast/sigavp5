<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicacionEstatusEstatus extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.ordenes_publicacion_estatus_estatus';

    protected $fillable = [
        "orden_publicacion_id",
        "orden_publicacion_estatus_id",
        'usuario_id'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'created_at', 'updated_at'];

    /**
     * Relación con modelo publicación
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function publicacion()
    {
        return $this->belongsTo('App\Models\Publicacion');
    }
    /**
     * Relación con modelo publicación estatus.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function publicacion_estatus()
    {
        return $this->belongsTo('App\Models\PublicacionEstatus','orden_publicacion_estatus_id');
    }


}