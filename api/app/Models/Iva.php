<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Iva extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "iva";

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable =
        [
            'id',
            'iva',
            'fecha',
            'activo',
            'usuario_id',
        ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at', 'updated_at','deleted_at'];
}