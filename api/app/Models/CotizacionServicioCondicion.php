<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CotizacionServicioCondicion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'cotizaciones_servicios_condiciones';

    protected $fillable = [
        "cotizaciones_servicios_id",
        "detalle_condicion_id",
        'es_porcentaje',
        'tarifa',
        'usuario_id',
        'condicion_id',
        'es_base',
        'data_extra'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'created_at', 'updated_at'];


    /**
     * Relación con modelo Cotización
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cotizacion_servicio()
    {
        return $this->belongsTo('App\Models\CotizacionServicio', 'cotizaciones_servicios_id');
    }

    /**
     * Relación con modelo Servicio
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    /*    public function w()
        {
            return $this->belongsTo('App\Models\Servicio');
        }*/

    /**
     * Relación con Condiciones Detalles
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function condicion_detalle()
    {
        return $this->belongsTo('App\Models\DetalleCondicion', 'detalle_condicion_id');
    }

    /**
     * @param $cotizaciones_servicios_id
     * @param bool $tiempo_incluido
     * @return float|int
     */
    public function calcularValorConRecargos($cotizaciones_servicios_id, $tiempo_incluido = false)
    {
        $valor = 0;
        $descuento_servicio = 1;
        $cotiVerConDetalles = $this
            ->with([
                'cotizacion_servicio.servicio.negociacion_servicio' => function ($q) {
                    $q->where('estado', true);
                }
            ])
            ->where('cotizaciones_servicios_id', $cotizaciones_servicios_id)
            ->orderBy('es_base', 'desc')
            ->orderBy('es_porcentaje', 'desc')
            ->get();

        foreach ($cotiVerConDetalles as $version_detalle) {
            //Condición
            $negociacion_condicion = NegociacionesCondiciones
                ::where('estado', true)
                ->where('condicion_id', $version_detalle['condicion_id'])
                ->first();

            if (!empty($negociacion_condicion)) {
                if ($negociacion_condicion['exonerado']) {
                    continue;
                }
            }

            // Servicio
            $negociacion_servicio = $version_detalle['cotizacion_servicio']['servicio']['negociacion_servicio'];
            if (!empty($negociacion_servicio) && !$negociacion_servicio->isEmpty()) {
                $negociacion_servicio = $negociacion_servicio[0];
                if ($negociacion_servicio['exonerado']) {
                    return 0;
                }
                if ($descuento_servicio == 1 && !empty($negociacion_servicio['monto'])) {
                    $descuento_servicio = (100 - $negociacion_servicio['monto']) / 100;
                }
            }

            //Detalle de Condición
            $negociacion_detalle = NegociacionesCondicionesDetalles//::find(1);
            ::where('condicion_detalle_id', $version_detalle['detalle_condicion_id'])
                ->where('estado', true)
                ->first();

            // Existe Negociación de Detalle de Condición
            if (!empty($negociacion_detalle)) {
                // Condición Exonerada
                if ($negociacion_detalle['exonerado']) {
                    continue;
                }

                if ($negociacion_detalle['sustituye']) {
                    // Tarifa Base
                    if ($version_detalle['es_base']) {
                        $valor = $negociacion_detalle['monto'];
                        continue;
                    }

                    // Cuando es Recargo
                    if ($negociacion_detalle['es_porcentaje']) {
                        $valor += ($valor * $negociacion_detalle['monto']) / 100;
                    } else {
                        // Caso que es aplicado en Bs
                        $valor += $negociacion_detalle['monto'];
                    }
                } else {
                    // No sustituyo
                    if ($negociacion_detalle['es_porcentaje']) {
                        $valor += ($valor * ($version_detalle['tarifa'] - $negociacion_detalle['monto'])) / 100;
                    } else {
                        // Caso que es aplicado en Bs
                        $valor += ($version_detalle['tarifa'] - $negociacion_detalle['monto']);
                    }
                }
                continue;
            }

            // No existe Negociación de Detalle de Condición.
            // Tarifa Base
            if ($version_detalle['es_base']) {
                $valor = $version_detalle['tarifa'];
                continue;
            }

            // Cuando es Recargo
            if ($version_detalle['es_porcentaje']) {
                $valor += ($valor * $version_detalle['tarifa']) / 100;
            } else {
                // Caso que es aplicado en Bs
                $valor += $version_detalle['tarifa'];
            }
        }

        if (!$tiempo_incluido)
            return $valor * $descuento_servicio;


        $cotiVer = CotizacionServicio::where('id', $cotizaciones_servicios_id)->get();
        $cotiVer = $cotiVer[0];

        $apariciones = (empty($cotiVer['version_apariciones'])) ? 1 : $cotiVer['version_apariciones'];
        $segundos = (empty($cotiVer['version_segundos'])) ? 1 : $cotiVer['version_segundos'];
        $calculo_apariciones_segundos = (($valor * $descuento_servicio) * $segundos * $apariciones);

        $finicio = $cotiVer['version_publicacion_fecha_inicio'];
        $ffin = $cotiVer['version_publicacion_fecha_fin'];

        //Aquí se obtiene los dias entre las fecha de publicación
        //para aplicar al subtotal por versiones.
        if (!empty($finicio) && !empty($ffin)):
            $datetime1 = date_create($finicio);
            $datetime2 = date_create($ffin);

            $interval = date_diff($datetime1, $datetime2);

            return array(
                'con_tiempo' => $calculo_apariciones_segundos * (1 + (Integer)$interval->format('%a')),
                // Se incluye el tiempo xq lo que se desea es calcular el precio por dia.
                'sin_tiempo' => $calculo_apariciones_segundos
            );

        endif;
    }
}