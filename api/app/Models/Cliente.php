<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Cliente extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "clientes";

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        'razon_social',
        'rif',
        'direccion_fiscal',
        'servicios',
        'otros_canales',
        'telefonos',
        'correos_electronicos',
        'sitio_web',
        'codigo',
        'abreviatura',
        'nombre_comercial',
        'img_cliente',
        'differentiation_chain',
        'usuario_id',
        'techo_consumo'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at', 'updated_at'];


    /**
     * Relación con modelo de Cotización
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cotizaciones()
    {
        return $this->hasMany('App\Models\Cotizacion');
    }

    /**
     * Relación con modelo de Publicación
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function publicaciones()
    {
        return $this->hasMany('App\Models\Publicacion');
    }

    /**
     * Relación con modelo de ContactoCliente
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contactos()
    {
        return $this->hasMany('App\Models\ContactoCliente');
    }

    /**
     * Relación con modelo de Contrato
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contratos()
    {
        return $this->hasMany('App\Models\Contrato');
    }

    /**
     * @return mixed
     */
    public function tipoContratos()
    {
        $tipoContrato = TipoContrato::where('nombre', 'contrato')->pluck('id');
        return $this->hasMany('App\Models\Contrato')
            ->where('tipo_contrato_id', '=', $tipoContrato);
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balance()
    {
        return $this->hasMany('App\Models\Balance');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function publicacion()
    {
        return $this->hasMany('App\Models\Publicacion');
    }

    /**
     * Relación con modelo de NotasCredito
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function nota()
    {
        return $this->hasMany('App\Models\NotasCredito');
    }

    /**
     * @param $id
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getData($id)
    {
        return $this
            ->with([
                'cotizaciones' => function ($q) {
                    $q->where('es_terminada', false);
                }
            ])
            ->Where('id', $id)
            ->get();
    }

    /**
     * @param $texto
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function getReferencia($texto)
    {
        return $this
            ->with([
                'cotizaciones' => function ($q) {
                    $q->where('es_terminada', false);
                }
            ])
            ->where('razon_social', 'ilike', "%{$texto}%")
            ->orWhere('rif', 'ilike', "%{$texto}%")
            ->get();
    }

    /**
     * Obtiene las facturas asociadas a las publicaciones de todos los clientes
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @return mixed
     */
    public static function factura_clientes_todas()
    {
        return DB::select(DB::raw('

        SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at, CLI.codigo as cliente_codigo, CLI.razon_social, FACTC.created_at
              FROM factura_contrato_cuotas  FACONC

              INNER JOIN factura_cliente FACTC ON FACONC.factura_id = FACTC .id
              LEFT JOIN contrato_cuotas CONC ON FACTC.id = CONC.factura_id
              INNER JOIN contratos CON ON FACONC.contrato_id = CON.id
              INNER JOIN clientes CLI ON CON.cliente_id = CLI.id
            UNION
            SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at, CLI.codigo as cliente_codigo, CLI.razon_social, FACTC.created_at
              FROM factura_cliente FACTC

	     LEFT JOIN factura_orden_publicacion FAORDP ON FAORDP.factura_id = FACTC.id
	      LEFT JOIN ordenes_publicacion ORDP ON ORDP.factura_id = FACTC.id
	      INNER JOIN clientes CLI ON ORDP.cliente_id = CLI.id


            '));
    }
//            SELECT A.id, A.created_at, A.codigo, A.updated_at,
//                  B.codigo as cliente_codigo, B.razon_social
//
//              FROM factura_cliente A, clientes B, ordenes_publicacion C
//            WHERE
//            C.factura_id = A.id AND
//            C.cliente_id = B.id
//            GROUP BY A.id, A.created_at, A.codigo, A.updated_at, B.codigo , B.razon_social ORDER BY A.id
    /**
     * Obtiene las facturas asociadas a las publicaciones y cuotas del cliente
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $idcliente
     * @return mixed
     */
    public static function factura($idcliente)
    {
        return DB::select(DB::raw("SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at
              FROM factura_contrato_cuotas  FACONC

              INNER JOIN factura_cliente FACTC ON FACONC.factura_id = FACTC .id
              LEFT JOIN contrato_cuotas CONC ON FACTC.id = CONC.factura_id
              INNER JOIN contratos CON ON FACONC.contrato_id = CON.id

            WHERE

            CON.cliente_id = $idcliente

            UNION
            SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at
              FROM factura_cliente FACTC

	       LEFT JOIN factura_orden_publicacion FAORDP ON FAORDP.factura_id = FACTC.id
	      LEFT JOIN ordenes_publicacion ORDP ON ORDP.factura_id = FACTC.id

           -- WHERE

            --ORDP.cliente_id =
            "));

    }

    /**
     * Convierte el nombre comercial en mayúsculas.
     * @param $value
     */
    public function setNombreComercialAttribute($value)
    {
        $this->attributes['nombre_comercial'] = strtoupper($value);
    }

    /**
     * Convierte la abreviatura en mayúsculas.
     * @param $value
     */
    public function setAbreviaturaAttribute($value)
    {
        $this->attributes['abreviatura'] = strtoupper($value);
    }

    /**
     * Convierte la razon social en mayusculas.
     * @param $value
     */
    public function setRazonSocialAttribute($value)
    {
        $this->attributes['razon_social'] = strtoupper($value);
    }

    /**
     * Convierte la direccion fiscal en mayusculas.
     * @param $value
     */
    public function setDireccionFiscalAttribute($value)
    {
        $this->attributes['direccion_fiscal'] = strtoupper($value);
    }

    /**
     * Actualiza el Techo de consumo del cliente.
     * @param $cliente_id
     * @param $request
     * @return bool
     */
    public function updateTechoConsumo($cliente_id, $request)
    {
        $cliente = $this->find($cliente_id);
        $cliente->techo_consumo = $request->techo;
        return $cliente->save();
    }
}