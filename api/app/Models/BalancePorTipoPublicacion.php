<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class BalancePorTipoPublicacion extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'balance_x_tipo_publicacion';

    protected $fillable = [
        'id',
        'balance_id',
        'contrato_id',
        'tipo_publicacion_id',
        'usuario_id',
        'monto',
        'cargo',
        'debito',
        'unidad',
        'unidad_nombre'
    ];

    protected $hidden = [
        "updated_at",
        "deleted_at",
    ];

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contrato()
    {
        return $this->belongsTo('App\Models\Contrato');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return array
     * Esta función se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacío entonces ninguno lo sera
     */
    public function getDates()
    {
        return array();
    }

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function balance()
    {
        return $this->belongsTo('App\Models\Balance');
    }

    /**
     * Relación con modelo TipoPublicación
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoPublicacion ()
    {
        return $this->belongsTo ('App\Models\TipoPublicacion', 'tipo_publicacion_id');
    }

}
