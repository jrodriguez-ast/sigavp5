<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MediosMedios extends Model {

    protected $table = 'medios_medios';

    protected $fillable = [
        "medio_padre_id",
        "medio_hijo_id",
        'usuario_id',
    ];

//    protected $hidden = ['id'];

    /**
     * Relación con Medio Padre
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function medioPadre()
    {
        return $this->belongsTo('App\Models\Medio');
    }
    /**
     * Relación con Medio Hijo.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
   /* public function medioHijo()
    {
        return $this->belongsTo('App\Models\Medio');
    }*/

}