<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Servicio extends Model
{
    use SoftDeletes;
    private $servicioPadre = '';
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'servicios';

    protected $fillable = [
        "nombre",
        "descripcion",
        "tipo_publicacion_id",
        "costo",
        "otros_detalles",
        "usuario_id",
        "tipo_medio",
        "id",
        "es_cadena",
    ];

    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'];

    /**
     * Relación con modelo Medio
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medio ()
    {
        return $this->belongsTo ('App\Models\Medio', 'medio_id');
    }

    /**
     * Relación con modelo TipoPublicación
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoPublicacion ()
    {
        return $this->belongsTo ('App\Models\TipoPublicacion', 'tipo_publicacion_id');
    }

    /**
     * Relación con modelo TipoPublicación
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function condiciones ()
    {
        return $this->hasMany ('App\Models\Condicion')->withTrashed ();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function negociacion_servicio ()
    {
        return $this->hasMany ('\App\Models\NegociacionesServicios', 'servicio_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function servicio_servicio ()
    {
        return $this->hasMany ('\App\Models\ServiciosServicios', 'servicios_hijo_id');
    }

    /**
     * @return $this
     */
    public function children()
    {
        return $this->belongsToMany('App\Models\Servicio', 'servicios_servicios', 'servicios_padre_id', 'servicios_hijo_id')
            ->withPivot(['id']);
    }

    /**
     * @return $this
     */
    public function parent()
    {
        return $this->belongsToMany('App\Models\Servicio', 'servicios_servicios', 'servicios_hijo_id', 'servicios_padre_id')
            ->withPivot(['id']);
    }

    /**
     * function que crea las tarifas bases y no bases por defecto
     * @param $service
     */
    public function crearTarifa ($service)
    {
        //Creación de las condiciones
        $condiciones = [

            //Creación de las condiciones para tipo impreso
            4 => [
                [
                    'nombre' => 'Tamaño',
                    'descripcion' => 'Tarifa para el tamaño',
                    'usuario_id' => $service['usuario_id'],
                    'es_base' => 1,
                    'es_porcentaje' => 0,
                    'detalles' => [
                        [
                            'nombre' => '1 página',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                            'principal' => 1
                        ],
                        [
                            'nombre' => '1/2 página vertical',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ],
                        [
                            'nombre' => '1/2 página horizontal',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ],
                        [
                            'nombre' => '1/4 página',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ],
                    ]
                ],
                [
                    'nombre' => 'Ubicación',
                    'descripcion' => 'Tarifa para la ubicación',
                    'usuario_id' => $service['usuario_id'],
                    'es_base' => 0,
                    'es_porcentaje' => 1,
                    'detalles' => [
                        [
                            'nombre' => 'Determinada',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ],
                        [
                            'nombre' => 'Indeterminada',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ]
                    ]
                ],
                [
                    'nombre' => 'Par o Impar',
                    'descripcion' => 'Tarifa para par e impar',
                    'usuario_id' => $service['usuario_id'],
                    'es_base' => 0,
                    'es_porcentaje' => 1,
                    'detalles' => [
                        [
                            'nombre' => 'Par',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ],
                        [
                            'nombre' => 'Impar',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ]
                    ]
                ]
            ],
            //Creación de las condiciones para televisivo
            2 => [
                [
                    'nombre' => 'Segundo',
                    'descripcion' => 'Tarifa para los segundos',
                    'usuario_id' => $service['usuario_id'],
                    'es_base' => 1,
                    'es_porcentaje' => 0,
                    'detalles' => [
                        [
                            'nombre' => '1 Segundo',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                            'principal' => 1
                        ]
                    ]
                ],
                [
                    'nombre' => 'Horario',
                    'descripcion' => 'Tarifa para el horario',
                    'usuario_id' => $service['usuario_id'],
                    'es_base' => 0,
                    'es_porcentaje' => 1,
                    'detalles' => [
                        [
                            'nombre' => 'Rotativo',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ],
                        [
                            'nombre' => 'Selectivo',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ]
                    ]
                ]
            ],
            //Creación de las condiciones para radial
            3 => [
                [
                    'nombre' => 'Segundo',
                    'descripcion' => 'Tarifa para los segundos',
                    'usuario_id' => $service['usuario_id'],
                    'es_base' => 1,
                    'es_porcentaje' => 0,
                    'detalles' => [
                        [
                            'nombre' => '1 Segundo',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                            'principal' => 1
                        ]
                    ]
                ],
                [
                    'nombre' => 'Horario',
                    'descripcion' => 'Tarifa para el horario',
                    'usuario_id' => $service['usuario_id'],
                    'es_base' => 0,
                    'es_porcentaje' => 1,
                    'detalles' => [
                        [
                            'nombre' => 'Rotativo',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ],
                        [
                            'nombre' => 'Selectivo',
                            'tarifa' => 0,
                            'usuario_id' => $service['usuario_id'],
                        ]
                    ]
                ]
            ],
            5 => [],
            1 => []
        ];
        foreach ($condiciones[$service['tipo_publicacion_id']] as $condicion) {
            // si la tarifa no tiene detalles solo creamos la tarifa
            if (!isset($condicion['detalles'])) {
                $service->condiciones ()->create ($condicion);
                continue;
            }

            // Si la tarifa viene con condiciones, creamos las tarifas y las condiciones
            $detalles = $condicion['detalles'];
            unset($condicion['detalles']);
            $service->condiciones ()->create ($condicion)->detalles ()->createMany ($detalles);
        }
    }


    /**
     * @param $hijo
     */
    public function crearTarifaMedio ($hijo)
    {
        //Creación de las condiciones
        $condiciones = [
            //Creación de las condiciones para tipo impreso
            4 => [
                [
                    'nombre' => 'Tamaño',
                    'descripcion' => 'Tarifa para el tamaño',
                    'usuario_id' => $hijo['usuario_id'],
                    'es_base' => 1,
                    'es_porcentaje' => 0,
                    'detalles' => [
                        [
                            'nombre' => '1 página',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                            'principal' => 1
                        ],
                        [
                            'nombre' => '1/2 página vertical',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ],
                        [
                            'nombre' => '1/2 página horizontal',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ],
                        [
                            'nombre' => '1/4 página',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ],
                    ]
                ],
                [
                    'nombre' => 'Ubicación',
                    'descripcion' => 'Tarifa para la ubicación',
                    'usuario_id' => $hijo['usuario_id'],
                    'es_base' => 0,
                    'es_porcentaje' => 1,
                    'detalles' => [
                        [
                            'nombre' => 'Determinada',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ],
                        [
                            'nombre' => 'Indeterminada',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ]
                    ]
                ],
                [
                    'nombre' => 'Par o Impar',
                    'descripcion' => 'Tarifa para par e impar',
                    'usuario_id' => $hijo['usuario_id'],
                    'es_base' => 0,
                    'es_porcentaje' => 1,
                    'detalles' => [
                        [
                            'nombre' => 'Par',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ],
                        [
                            'nombre' => 'Impar',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ]
                    ]
                ]
            ],
            //Creación de las condiciones para televisivo
            2 => [
                [
                    'nombre' => 'Segundo',
                    'descripcion' => 'Tarifa para los segundos',
                    'usuario_id' => $hijo['usuario_id'],
                    'es_base' => 1,
                    'es_porcentaje' => 0,
                    'detalles' => [
                        [
                            'nombre' => '1 Segundo',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                            'principal' => 1
                        ]
                    ]
                ],
                [
                    'nombre' => 'Horario',
                    'descripcion' => 'Tarifa para el horario',
                    'usuario_id' => $hijo['usuario_id'],
                    'es_base' => 0,
                    'es_porcentaje' => 1,
                    'detalles' => [
                        [
                            'nombre' => 'Rotativo',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ],
                        [
                            'nombre' => 'Selectivo',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ]
                    ]
                ]
            ],
            //Creación de las condiciones para radial
            3 => [
                [
                    'nombre' => 'Segundo',
                    'descripcion' => 'Tarifa para los segundos',
                    'usuario_id' => $hijo['usuario_id'],
                    'es_base' => 1,
                    'es_porcentaje' => 0,
                    'detalles' => [
                        [
                            'nombre' => '1 Segundo',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                            'principal' => 1
                        ]
                    ]
                ],
                [
                    'nombre' => 'Horario',
                    'descripcion' => 'Tarifa para el horario',
                    'usuario_id' => $hijo['usuario_id'],
                    'es_base' => 0,
                    'es_porcentaje' => 1,
                    'detalles' => [
                        [
                            'nombre' => 'Rotativo',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ],
                        [
                            'nombre' => 'Selectivo',
                            'tarifa' => 0,
                            'usuario_id' => $hijo['usuario_id'],
                        ]
                    ]
                ]
            ]
        ];

        foreach ($condiciones[$hijo['tipo_publicacion_id']] as $condicion) {
            if (!isset($condicion['detalles'])) {
                $hijo->condiciones ()->create ($condicion);
                continue;
            }

            // Si la tarifa viene con condiciones, creamos las tarifas y las condiciones
            $detalles = $condicion['detalles'];
            unset($condicion['detalles']);
            $hijo->condiciones ()->create ($condicion)->detalles ()->createMany ($detalles);
        }
    }

    public function servicio_padre ()
    {
        return $this->hasMany ('\App\Models\ServiciosServicios', 'servicios_padre_id');
    }

    public function getChildren ($servicio_id)
    {
        $this->servicioPadre = $servicio_id;
        $childrenServices = Servicio::select (
            'nombre',
            'public.servicios_servicios.servicios_hijo_id',
            'public.servicios_servicios.servicios_padre_id as id_padre',
            'descripcion',
            'public.servicios.tipo_publicacion_id'
        )
            ->leftJoin ('public.servicios_servicios',
                'public.servicios_servicios.servicios_hijo_id', '=', 'public.servicios.id')
//                    ->has('servicio_servicio')->get();
            ->whereHas ('servicio_servicio', function ($q) {
                $q->where ('servicios_padre_id', '=', $this->servicioPadre);
                // $q->where('servicios_hijo_id', '=', $this->servicioHijo);

            })->get ();
        return $childrenServices;
    }

    /**
     * Convierte la nombre fiscal en mayúsculas.
     * @param $value
     */
    public function setNombreAttribute($value)
    {
        $this->attributes['nombre'] = strtoupper($value);
    }
}

