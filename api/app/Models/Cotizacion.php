<?php namespace App\Models;

use App\Models\Utilities\GenericConfiguration;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Cotizacion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.cotizaciones';

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        'id',
        'codigo',
        'fecha_creacion',
        'fecha_expiracion',
        'descuento',
        'con_impuestos',
        'porcentaje_impuesto',
        'es_terminada',
        'subtotal',
        'total_impuesto',
        'total_descuento',
        'total',
        'fue_enviada',
        'motivo_descuento',
        'aprobada',
        'subtotal_con_descuento',
        'por_facturar',
        'cliente_id',
        'porcentaje_avp',
        'firmante_nombre',
        'firmante_cargo',
        'usuario_id',
        'subtotal_sin_avp',
        'contrato_asociado'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at', 'updated_at'];

    protected $casts = [
        'descuento' => 'integer',
        'con_impuestos' => 'boolean',
        'porcentaje_impuesto' => 'float',
        'es_terminada' => 'boolean',
        'subtotal' => 'float',
        'total_impuesto' => 'float',
        'total_descuento' => 'float',
        'total' => 'float',
        'fue_enviada' => 'boolean',
        'aprobada' => 'boolean',
        'subtotal_con_descuento' => 'float',
        'por_facturar' => 'boolean',
        'porcentaje_avp' => 'float'
    ];

    /**
     * Calcula los montos de una cotización con
     * respecto a las versiones asociadas existentes
     *
     * @access public
     * @param null $cotizacion_id Identificador de la cotización
     * @param null $seeder_user
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:25 AM
     */
    public static function calculate_quote($cotizacion_id = null, $seeder_user = NULL)
    {
        if (empty($cotizacion_id))
            return false;
        $InstCotizacion = new Cotizacion();

        //Se obtienen los datos de la cotización
        $getSubtotal = $InstCotizacion->data_for_quote($cotizacion_id);

        //Iva
        $p_iva = Iva::where('activo', true)->firstOrFail();
        //Impuesto AVP
        $p_avp = RecargoGestion::where('activo', true)->firstOrFail();

        //Obtengo el subtotal
        $subtotal = (!empty($getSubtotal['msubtotal']) ? $getSubtotal['msubtotal'] : 0.00);
        //$subtotal = mt_rand (1.9*10, 7000.999*10) / 10;
        //Iva
        $porcentaje_impuesto = ($p_iva ? $p_iva->iva : 0.00);
        //Impuesto AVP
        $porcentaje_avp = ($p_avp ? $p_avp->porcentaje : 0.00);

        //Aplico impuesto AVP al subtotal
        $impuesto_avp = ($subtotal * $porcentaje_avp / 100);

        //Subtotal con el recargo AVP aplicado
        $subtotal_con_avp = $subtotal + $impuesto_avp;

        //Aplico impuesto IVA al subtotal ya con recargo AVP incluido
        $impuesto_iva = ($subtotal_con_avp * $porcentaje_impuesto / 100);

        //Total a pagar
        $total = $subtotal_con_avp + $impuesto_iva;

        //Datos de salida
        $response = array(
            'subtotal' => $subtotal_con_avp,
            'subtotal_sin_avp' => $subtotal,
            'porcentaje_impuesto' => $porcentaje_impuesto,
            'total_impuesto' => $impuesto_iva,
            'porcentaje_avp' => $porcentaje_avp,
            'total_impuesto_avp' => $impuesto_avp,
            'total' => $total
        );

        //Actualizo la cotizacion con los datos actuales
        $cotizacion = $InstCotizacion->find($cotizacion_id);
        $cotizacion->subtotal_sin_avp = $response['subtotal_sin_avp'];
        $cotizacion->subtotal = $response['subtotal'];
        $cotizacion->porcentaje_impuesto = $response['porcentaje_impuesto'];
        $cotizacion->total_impuesto = $response['total_impuesto'];
        $cotizacion->porcentaje_avp = $response['porcentaje_avp'];
        $cotizacion->total_impuesto_avp = $response['total_impuesto_avp'];
        $cotizacion->total = $response['total'];
        $cotizacion->usuario_id = (empty($seeder_user)) ? session()->get('userId') : $seeder_user;
        $cotizacion->create_code = false;
        return $cotizacion->save();

    }

    /**
     * Provee los datos de la cotización y sus entidades dependientes
     *
     * @access public
     * @param Int $id Identificador de la cotización
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:25 AM
     */
    public static function data_for_quote($id)
    {
        $InstCotizacion = new Cotizacion();

        $cotizacion = $InstCotizacion->with([
            'cliente' => function ($q) {
                $q->select([
                    'id',
                    'razon_social',
                    'rif',
                    'direccion_fiscal',
                    'correos_electronicos',
                    'telefonos',
                    'codigo'
                ]);
            },
            'user',
            'cliente.contactos',
            'cliente.contactos.cargo',
            'orden_servicio',
            'orden_servicio.user',
            'orden_servicio.viaRecepcionOrdenServicio',
            'contrato.tipo',
            'contrato.cuotas',
            'contrato.pautas.tipo'
        ])->find($id);

        if (!empty($cotizacion->contrato)) {
            $cotizacion['contrato']['balance'] =
                $cotizacion->contrato->balance()->latest()->select('contrato_saldo')->first();

            $contrato['contrato']['balance']['porTipoPublicacion'] = BalancePorTipoPublicacion::
            where('contrato_id', $cotizacion->contrato->id)
                ->groupBy('contrato_id')
                ->groupBy('tipo_publicacion_id')
                ->select(['contrato_id', 'tipo_publicacion_id', DB::raw('MIN(monto)')])
                ->get();
        }

        if (!empty($cotizacion)) {
            $cotizacion['versiones'] = array();
            if (!empty($cotizacion['orden_servicio'])) {
                foreach ($cotizacion['orden_servicio'] as $key => $value):
                    $date = date_create($value['fecha_envio']);
                    $cotizacion['orden_servicio'][$key]['file_exist'] = false;
                    if (Storage::exists("cotizacion/orden_servicio/" . trim($value['file']))) {
                        $cotizacion['orden_servicio'][$key]['file_exist'] = true;
                    }
                    $cotizacion['orden_servicio'][$key]['fecha_envio'] = date_format($date, 'd/m/Y');
                    $cotizacion['orden_servicio'][$key]['file'] = url() . "/storage/cotizacion/orden_servicio/" . $value['file'];

                endforeach;
            }
            if ($cotizacion['cliente']) {
                $cotizacion['cliente']['contrato_valido'] = false;
                $cotizacion['cliente']['contrato_tarifa'] = array();


                $cotizacion['cliente']['contrato_tarifa'] = DB::select(DB::raw("
                    SELECT tarifa FROM contratos
                    WHERE id =( SELECT max(id) FROM contratos WHERE cliente_id={$cotizacion['cliente']['id']} AND activo=TRUE )"
                ));

                if (!empty($cotizacion['cliente']['contrato_tarifa'])) {
                    $cotizacion['cliente']['contrato_valido'] = true;
                }
            }


            $cotizacionesServicios = CotizacionServicio
                ::leftJoin('public.servicios', 'public.cotizaciones_servicios.servicio_id', '=', 'public.servicios.id')
                ->leftJoin('public.medios', 'public.servicios.medio_id', '=', 'public.medios.id')
                ->leftJoin('public.tipos_publicaciones', 'public.servicios.tipo_publicacion_id', '=', 'public.tipos_publicaciones.id')
                ->where(array('cotizacion_id' => $cotizacion->id))
                ->select(array(
                    DB::raw("
                    (
                            WITH RECURSIVE sum_monto_total (monto_total) AS (

                            SELECT
                            (
                            CASE WHEN base
                            THEN tarifa
                            WHEN es_porcentaje
                            THEN round(tarifa_base::numeric * tarifa/100::numeric,2)
                            ELSE tarifa
                            END ) as monto_total


                            FROM (SELECT
                            cs.id,
                            cd.nombre as condiciones_detalles_nombre,
                            csc.id as detalle_cotizacion,
                            false as base,
                            csc.es_porcentaje,

                            csc.tarifa,
                            (
                            select

                            cotizaciones_servicios_condiciones.tarifa
                            from cotizaciones_servicios_condiciones

                            where
                            cotizaciones_servicios_condiciones.cotizaciones_servicios_id = cs.id AND
                            cotizaciones_servicios_condiciones.condicion_id =
                            (
                            select
                            condiciones.id
                            from
                            cotizaciones_servicios_condiciones
                            inner join cotizaciones_servicios on cotizaciones_servicios_condiciones.cotizaciones_servicios_id = cotizaciones_servicios.id
                            inner join servicios on cotizaciones_servicios.servicio_id = servicios.id
                            inner join condiciones on condiciones.servicio_id=servicios.id
                            where
                            condiciones.es_base= '1'
                            and cotizaciones_servicios.servicio_id = cs.servicio_id
                            group by condiciones.id)

                            group by cotizaciones_servicios_condiciones.detalle_condicion_id,cotizaciones_servicios_condiciones.tarifa

                            limit 1
                             ) as tarifa_base

                            FROM
                            cotizaciones cot
                            INNER JOIN cotizaciones_servicios cs ON cot.id = cs.cotizacion_id
                            INNER JOIN cotizaciones_servicios_condiciones csc ON cs.id = csc.cotizaciones_servicios_id
                            INNER JOIN condiciones_detalles cd ON cd.id = csc.detalle_condicion_id
                            INNER JOIN condiciones c ON c.id = cd.condicion_id
                            INNER JOIN servicios s ON c.servicio_id = s.id
                            INNER JOIN medios m ON s.medio_id = m.id

                            WHERE
                             c.es_base<>'1'

                            UNION
                            SELECT
                            cs.id,
                            cd.nombre as condiciones_detalles_nombre,
                            csc.id as detalle_cotizacion,
                            true as base,
                            csc.es_porcentaje,

                            csc.tarifa,
                            (
                            select

                            cotizaciones_servicios_condiciones.tarifa
                            from cotizaciones_servicios_condiciones

                            where
                            cotizaciones_servicios_condiciones.cotizaciones_servicios_id = cs.id AND
                            cotizaciones_servicios_condiciones.condicion_id =
                            (
                            select
                            condiciones.id
                            from
                            cotizaciones_servicios_condiciones
                            inner join cotizaciones_servicios on cotizaciones_servicios_condiciones.cotizaciones_servicios_id = cotizaciones_servicios.id
                            inner join servicios on cotizaciones_servicios.servicio_id = servicios.id
                            inner join condiciones on condiciones.servicio_id=servicios.id
                            where
                            condiciones.es_base= '1'
                            and cotizaciones_servicios.servicio_id = cs.servicio_id
                            group by condiciones.id)

                            group by cotizaciones_servicios_condiciones.detalle_condicion_id,cotizaciones_servicios_condiciones.tarifa

                            limit 1
                             ) as tarifa_base


                            FROM
                            cotizaciones cot
                            INNER JOIN cotizaciones_servicios cs ON cot.id = cs.cotizacion_id
                            INNER JOIN cotizaciones_servicios_condiciones csc ON cs.id = csc.cotizaciones_servicios_id
                            INNER JOIN condiciones_detalles cd ON cd.id = csc.detalle_condicion_id
                            INNER JOIN condiciones c ON c.id = cd.condicion_id
                            INNER JOIN servicios s ON c.servicio_id = s.id
                            INNER JOIN medios m ON s.medio_id = m.id

                            WHERE

                             c.es_base='1'

                            ) AS vista_detalles
                            WHERE
                            id=public.cotizaciones_servicios.id
                            GROUP BY

                            id,
                            condiciones_detalles_nombre,
                            detalle_cotizacion,
                            base,
                            es_porcentaje,
                            tarifa,
                            tarifa_base
                                                    )
                                        SELECT
                                            sum(monto_total)
                                        FROM sum_monto_total

                             ) as subtotal,
                             (
                             select csc.tarifa
                                from cotizaciones_servicios_condiciones as csc
                            where
                                csc.cotizaciones_servicios_id = public.cotizaciones_servicios.id AND
                            csc.es_base= '1'
                            limit 1

                             )as tarifa_base
                             "),
                    DB::raw("
                            ARRAY(
                            SELECT
                            condiciones_detalles_nombre || '(' || label || ')'
                            FROM (
                            SELECT
                            cs.id,
                            cd.nombre as condiciones_detalles_nombre,
                            (case when csc.es_base then 'Base' else 'Recargo' end) as label
                            FROM
                            cotizaciones cot
                            INNER JOIN cotizaciones_servicios cs ON cot.id = cs.cotizacion_id
                            INNER JOIN cotizaciones_servicios_condiciones csc ON cs.id = csc.cotizaciones_servicios_id
                            INNER JOIN condiciones_detalles cd ON cd.id = csc.detalle_condicion_id
                            INNER JOIN condiciones c ON c.id = cd.condicion_id
                            INNER JOIN servicios s ON c.servicio_id = s.id
                            INNER JOIN medios m ON s.medio_id = m.id
                            ) AS vista_detalles
                            WHERE
                            id=public.cotizaciones_servicios.id
                            GROUP BY
                            id,
                            condiciones_detalles_nombre,
                            label
                 ) as condiciones"),
                    'public.medios.id as medio_id',
                    'cotizaciones_servicios.id as id',
                    'public.tipos_publicaciones.nombre as tipo_publicacion',
                    'cotizacion_id',
                    'servicio_id',
                    'cotizaciones_servicios.id as cotizaciones_servicios_id',
                    'public.servicios.nombre as servicio',
                    'version_nombre',
                    'aprobada',
                    'version_file',
                    'version_publicacion_fecha',
                    'public.medios.razon_social as razon_social',
                    'public.medios.nombre_comercial as nombre_comercial',
                    'cotizaciones_servicios.version_publicacion_fecha_inicio',
                    'cotizaciones_servicios.version_publicacion_fecha_fin',
                    'cotizaciones_servicios.version_segundos',
                    'cotizaciones_servicios.version_apariciones',
                    'cotizaciones_servicios.tipo_publicacion_name',
                    'cotizaciones_servicios.tipo_publicacion_id',
                ))->get();


            //Variable para sumar al monto de las condiciones por versiones con respecto
            //a los dias entre las fecha inicio y fecha fin
            $sub_total_versiones = 0.00;
            if (!empty($cotizacionesServicios)):
//                dd($cotizacionesServicios);
                $cotizacion['versiones'] = $cotizacionesServicios;
                foreach ($cotizacion['versiones'] as $key => &$cotiVersion):
                    $calculo_apariciones_segundos = 0.00;
                    $cotiVersion['fecha_unica'] = false;
                    //Se obtienen las condiciones agrupadas de acuerdo a cada versiones
                    if (!empty($cotiVersion['condiciones'])):
                        $cotiVersion['condiciones'] = json_decode(json_encode(str_getcsv(trim($cotiVersion['condiciones'], '{}'))), true);
                    endif;

                    // Fix Jose Rodriguez
                    $valor_con_recargos = (new CotizacionServicioCondicion())->calcularValorConRecargos($cotiVersion['cotizaciones_servicios_id']);
                    $apariciones = (empty($cotiVersion['version_apariciones'])) ? 1 : $cotiVersion['version_apariciones'];
                    $segundos = (empty($cotiVersion['version_segundos'])) ? 1 : $cotiVersion['version_segundos'];
                    $calculo_apariciones_segundos = ($valor_con_recargos * $segundos * $apariciones);

                    //Se obtiene la tarifa base de las condiciones pertenecientes a una version
                    //y se aplica el calculo de acuerdo a las apariciones y los segundos
//                    if (!empty($cotiVersion['tarifa_base'])):
//                        $calculo_apariciones_segundos = ($cotiVersion['tarifa_base'] * $cotiVersion['version_segundos'] * $cotiVersion['version_apariciones']);
//                    endif;
                    // Fin Fix Jose Rodriguez


                    //Se le aplica formato a la fecha de publicación
                    if (!empty($cotiVersion['version_publicacion_fecha'])):
                        $date = date_create($cotiVersion['version_publicacion_fecha']);
                        $cotiVersion['version_publicacion_fecha'] = date_format($date, 'd/m/Y');
                    else:
                        $cotiVersion['version_publicacion_fecha'] = 'Sin Información';
                    endif;

                    $cotiVersion['subtotal'] = $calculo_apariciones_segundos;
                    $finicio = $cotiVersion['version_publicacion_fecha_inicio'];
                    $ffin = $cotiVersion['version_publicacion_fecha_fin'];

                    //Aquí se obtiene los dias entre las fecha de publicación
                    //para aplicar al subtotal por versiones.
                    if (!empty($finicio) && !empty($ffin)):
                        $datetime1 = date_create($finicio);
                        $datetime2 = date_create($ffin);

                        $interval = date_diff($datetime1, $datetime2);
                        if ($interval->days == 0)
                            $cotiVersion['fecha_unica'] = true;
                        /** @var Integer $interval */
                        $cotiVersion['subtotal'] = $cotiVersion['subtotal'] * (1 + (Integer)$interval->format('%a'));

                    endif;
                    $sub_total_versiones += $cotiVersion['subtotal'];
                endforeach;
                $cotizacion['msubtotal'] = $sub_total_versiones;
            endif;
        }
        $configuracion = GenericConfiguration::where(array('_label' => 'cotizacion_validez'))->get();

        $validez_dias = ($configuracion ? $configuracion[0]['value'] : '0');
        $date_expiration = date_create($cotizacion['fecha_expiracion']);
        $cotizacion['fechaa_expiracion'] = date_format($date_expiration, 'd-m-Y');
        $cotizacion['fecha_expiracion'] = date_format($date_expiration, 'Y-m-d');

        $date_create = date_create($cotizacion['fecha_creacion']);
        $cotizacion['fecha_creacion_label'] = date_format($date_create, 'd-m-Y');
        $cotizacion['dias_validez'] = $validez_dias;

        return $cotizacion;
    }

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }

    /**
     * Relación con modelo Cliente
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente');
    }

    /**
     * Relación con el modelo CotizacionServicio
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function detalles()
    {
        return $this->hasMany('App\Models\CotizacionServicio');
    }

    /**
     * Relación con el modelo CotizacionOrdenServicio
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orden_servicio()
    {
        return $this->hasMany('App\Models\CotizacionOrdenServicio');
    }

    /**
     * Permite obtener uno o todos los registros de la entidad GenericConfiguration
     *
     * @access public
     * @param null $where Filtro en la búsqueda
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:25 AM
     */
    public function get_configuration_generic($where = null)
    {
        $out = array();
        if (!empty($where)):
            $data = GenericConfiguration::where($where)->get();
        else:
            $data = GenericConfiguration::all();
        endif;
        if (!empty($data)):
            foreach ($data as $key => $value):
                $out[$value['_label']] = $value;
            endforeach;
        endif;

        return $out;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function orden_publicacion()
    {
        return $this->hasMany('App\Models\Publicacion');
    }

    /**
     * Relación con modelo de Contrato
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contrato()
    {
        return $this->belongsTo('App\Models\Contrato');
    }

    /**
     * Verifica el Saldo en contrato en función del monto total a aprobar.
     * @param $cotizacion_id
     * @param $versiones
     * @return bool
     */
    public static function montoAprobado($cotizacion_id, $versiones)
    {
        $cotizacion = Cotizacion::find($cotizacion_id);
        $subtotal_versiones = 0;
        foreach ($versiones as $version) {
            $subtotal_version = 0;
            $dias = 1;
            // Validamos que no sean versiones previamente aprobadas y
            // que tmp_aprobada (Aprobadas en Vista), sea true.
            if (!$version['tmp_aprobada'] || $version['aprobada']) {
                continue;
            }

            $valor_con_recargos = (new CotizacionServicioCondicion())->calcularValorConRecargos($version['cotizaciones_servicios_id']);
            $apariciones = (empty($version['version_apariciones'])) ? 1 : $version['version_apariciones'];
            $segundos = (empty($version['version_segundos'])) ? 1 : $version['version_segundos'];
            $subtotal_version = ($valor_con_recargos * $segundos * $apariciones);

            if (!$version['fecha_unica']) {
                $dias = Carbon::createFromTimestamp(strtotime($version['version_publicacion_fecha_fin']))
                    ->diffInDays(Carbon::createFromTimestamp(strtotime($version['version_publicacion_fecha_inicio'])));
            }

            $subtotal_version *= $dias;
            $subtotal_versiones += $subtotal_version;
        }

        $subtotal_versiones *= (1 + $cotizacion->porcentaje_avp / 100);
        $subtotal_versiones *= (1 + $cotizacion->porcentaje_impuesto / 100);
        $total = round($subtotal_versiones, 2);

        $balance_contrato = Balance::where('contrato_id', $cotizacion->contrato_id)->latest()->first(['saldo']);
        if ($total > $balance_contrato->saldo) {
            return false;
        }
        return true;
    }

}
