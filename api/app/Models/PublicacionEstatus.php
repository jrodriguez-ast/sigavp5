<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicacionEstatus extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.ordenes_publicacion_estatus';

    protected $fillable = [
        "descripcion",
        "estatus",
        'usuario_id'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'created_at', 'updated_at'];

    /**
     * Relación con modelo publicación
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function publicacion_estatus_estatus()
    {
        return $this->hasMany('App\Models\PublicacionEstatusEstatus','orden_publicacion_id');
    }


}