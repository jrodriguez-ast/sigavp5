<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicacionIncidencias extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.ordenes_publicacion_incidencias';

    protected $fillable = [
        "orden_publicacion_id",
        "tipo_incidencia_id",
        'version_id',
        'monto',
        'precio',
        'exonerado',
        'usuario_id',
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'updated_at'];

    /**
     * Relación con modelo publicación
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function publicacion()
    {
        return $this->belongsTo('App\Models\Publicacion');
    }

    /**
     * Relación con modelo tipo incidencia
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipo_incidencia()
    {
        return $this->belongsTo('App\Models\TipoIncidencia', 'tipo_incidencia_id');
    }

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }


    /**
     * Se aplica para evitar que las fechas devueltas sean
     * formateadas por la librería carbon de laravel
     *
     * @return array
     */
    public function getDates()
    {
        return array();
    }
    /**
     * Relación con modelo version
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function version()
    {
        return $this->belongsTo('App\Models\PublicacionVersion', 'version_id');
    }

}