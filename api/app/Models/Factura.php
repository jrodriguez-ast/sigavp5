<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Factura extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $casts = [ 'codigo' => 'string', ];
    protected $table = 'public.factura_cliente';
    /**
     * Campos que pueden ser llenados
     * @type array
     */
    protected $fillable = [
        'created_at',
        'codigo',
        'id',
        'updated_at',
        'nombre_firmante',
        'cargo_firmante',
        'fecha_motorizado',
        'fecha_cliente',
        'fecha_cancelada',
        'fecha_generada',
        'usuario_id',
        'total_factura',
        'restante',
        'n_control',
        'tipo_factura',
        'fecha_anulacion',
        'estatus_factura',
    ];

    /**
     * Relación de Factura con Publicación
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function publicacion()
    {
        return $this->hasMany('App\Models\Publicacion');
    }
    /**
     * Esta función se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacío entonces ninguno lo sera
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return array
     */
    public function getDates()
    {
        return array();
    }

    /**
     * Relación con Pagos.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTomany
     */
    public function pagos()
    {
        return $this->belongsToMany('App\Models\Pagos','public.fact_pago', 'factura_id','pago_id');
    }

    /**
     * @author Carlos Blanco <cebs923@@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cuota()
    {
        return $this->hasMany('App\Models\ContratoCuotas');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function facturaContratoCuotas()
    {
        return $this->belongsToMany('App\Models\ContratoCuotas','factura_contrato_cuotas', 'factura_id', 'cuota_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function facturaOrdenPublicacion()
    {
        return $this->belongsToMany('App\Models\Publicacion','factura_orden_publicacion', 'factura_id', 'orden_id');
    }
    /**
     * @author Carlos Blanco <cebs923@@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function nota()
    {
        return $this->hasMany('App\Models\NotasCredito');
    }
}
