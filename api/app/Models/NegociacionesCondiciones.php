<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NegociacionesCondiciones extends Model {

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'negociaciones_condiciones';

    protected $fillable = ['id','negociacion_id','servicio_id','medio_id','condicion_id','usuario_id', 'monto',
        'estado', 'fecha','es_porcentaje','exonerado'];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Relación con Negociación
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function negociacion(){
        return $this->belongsTo('App\Models\Negociaciones');
    }

    /**
     * Relación con Servicio.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function servicio()
    {
        return $this->belongsTo('App\Models\Servicio','servicio_id');
    }

    /**
     * Relación con Servicio.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function condicion()
    {
        return $this->belongsTo('App\Models\Condicion','condicion_id');
    }

    /**
     * Relación con Usuario.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Acl\User');
    }

}
