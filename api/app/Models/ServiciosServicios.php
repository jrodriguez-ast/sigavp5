<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiciosServicios extends Model {

    protected $table = 'servicios_servicios';

    protected $fillable = [
        "servicios_padre_id",
        "servicios_hijo_id",
    ];

//    protected $hidden = ['id'];

    /**
     * Relación con Servicio Padre
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function servicioPadre()
    {
        return $this->belongsTo('App\Models\Servicio');
    }
    /**
     * Relación con Servicios Hijos
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function hijos()
    {
        return $this->belongsTo('App\Models\Servicio', 'servicios_hijo_id');
    }

}