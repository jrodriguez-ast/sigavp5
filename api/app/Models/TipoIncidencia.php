<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoIncidencia extends Model {

	protected $table = 'tipos_incidencias';

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        'nombre',
        'tipo_publicacion_id',
        'usuario_id',
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [ 'created_at', 'updated_at'];


    /**
     * Relación con modelo TipoPublicación
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function tipoPublicacion()
    {
        return $this->belongsTo('App\Models\TipoPublicacion');
    }
}
