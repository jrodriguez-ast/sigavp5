<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Municipio extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'municipio';

    protected $fillable = [
        "nombre",
        "estado_id",
        "usuario_id"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];


    /**
     * Relación con el modelo Estado
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function estado ()
    {
        return $this->belongsTo ('App\Models\Estado');
    }


}
