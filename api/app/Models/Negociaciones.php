<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Negociaciones extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'negociaciones';

    protected $fillable = [
        'id', 'usuario_id', 'medio_id', 'fecha_negociacion',
        'tarifa', 'techo_consumo', 'cerrado', 'observaciones', 'codigo',
        'firmante_nombre', 'firmante_cargo'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'created_at', 'updated_at', 'deleted_at'];

    /**
     * Relación con Medio
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medio()
    {
        return $this->belongsTo('App\Models\Medio');
    }

    /**
     * Relación con Usuario.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Acl\User');
    }

    /**
     * Relación con Servicios
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function servicios()
    {
        return $this->hasMany('App\Models\NegociacionesServicios', 'negociacion_id');
    }

    /**
     * Relación con Condiciones
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function condiciones()
    {
        return $this->hasMany('App\Models\NegociacionesCondiciones', 'negociacion_id');
    }

    /**
     * Relación con DetalleCondición
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function condicionDetalles()
    {
        return $this->hasMany('App\Models\NegociacionesCondicionesDetalles', 'negociacion_id');
    }

    /**
     * Relación con Balance.
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balanceAVP()
    {
        return $this->hasMany('\App\Models\BalanceAvp', 'negociacion_id');
    }

}

