<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PublicacionVersionCondicion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.ordenes_publicacion_versiones_condiciones';

    protected $fillable = [
        "ordenes_publicacion_versiones_id",
        "detalle_condicion_id",
        'es_porcentaje',
        'tarifa',
        'usuario_id',
        'condicion_id',
        'es_base',
        'data_extra'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'created_at', 'updated_at'];

    /**
     * Relación con modelo publicación version
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function version()
    {
        return $this->belongsTo('App\Models\PublicacionVersion', 'ordenes_publicacion_versiones_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function condicion()
    {
        return $this->belongsTo('App\Models\Condicion');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function detallecondicion()
    {
        return $this->belongsTo('App\Models\DetalleCondicion', 'detalle_condicion_id');
    }

    /**
     * @param $publicacion_servicio_id
     * @return float|int
     */
    public function calcularValorConRecargos($publicacion_servicio_id)
    {
        $valor = 0;
        $descuento_servicio = 1;
        $cotiVerConDetalles = $this
            ->with([
                'version.servicio.negociacion_servicio' => function($q){
                    $q->where('estado',true);
                }
            ])
            ->where('ordenes_publicacion_versiones_id', $publicacion_servicio_id)
            ->orderBy('es_base', 'desc')
            ->orderBy('es_porcentaje', 'desc')
            ->get();
        foreach($cotiVerConDetalles as $version_detalle) {
            //Condición
            $negociacion_condicion = NegociacionesCondiciones
                ::where('estado',true)
                ->where('condicion_id',$version_detalle['condicion_id'])
                ->first();

            if(!empty($negociacion_condicion)){
                if ($negociacion_condicion['exonerado']) {
                    continue;
                }
            }

            // Servicio
            $negociacion_servicio = $version_detalle['version']['servicio']['negociacion_servicio'];
            if(!empty($negociacion_servicio) && !$negociacion_servicio->isEmpty()) {
                $negociacion_servicio = $negociacion_servicio[0];
                if ($negociacion_servicio['exonerado']) {
                    return 0;
                }
                if ($descuento_servicio == 1 && !empty($negociacion_servicio['monto'])) {
                    $descuento_servicio = (100 - $negociacion_servicio['monto']) / 100;
                }
            }

            //Detalle de Condición
            $negociacion_detalle = NegociacionesCondicionesDetalles //::find(1);
                ::where('condicion_detalle_id',$version_detalle['detalle_condicion_id'])
                ->where('estado',true)
                ->first();

            // Existe Negociación de Detalle de Condición
            if(!empty($negociacion_detalle)) {
                // Condición Exonerada
                if($negociacion_detalle['exonerado']){
                    continue;
                }

                if($negociacion_detalle['sustituye']) {
                    // Tarifa Base
                    if($version_detalle['es_base']){
                        $valor = $negociacion_detalle['monto'];
                        continue;
                    }

                    // Cuando es Recargo
                    if($negociacion_detalle['es_porcentaje']){
                        $valor += ($valor * $negociacion_detalle['monto'])/100;
                    } else{
                        // Caso que es aplicado en Bs
                        $valor += $negociacion_detalle['monto'];
                    }
                } else {
                    // No sustituyo
                    if($negociacion_detalle['es_porcentaje']) {
                        $valor += ($valor * ($version_detalle['tarifa'] - $negociacion_detalle['monto']))/100;
                    } else {
                        // Caso que es aplicado en Bs
                        $valor += ($version_detalle['tarifa'] - $negociacion_detalle['monto']);
                    }
                }
                continue;
            }

            // No existe Negociación de Detalle de Condición.
            // Tarifa Base
            if($version_detalle['es_base']){
                $valor = $version_detalle['tarifa'];
                continue;
            }

            // Cuando es Recargo
            if($version_detalle['es_porcentaje']){
                $valor += ($valor * $version_detalle['tarifa'])/100;
            } else{
                // Caso que es aplicado en Bs
                $valor += $version_detalle['tarifa'];
            }
        }
        return $valor * $descuento_servicio;
    }
}