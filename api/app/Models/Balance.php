<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author  Maykol Purica <puricamaykol@gmail.com>
 *          Class Balance
 * @package App\Models
 */
class Balance extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'balances';

    protected $fillable = [
        'id',
        'codigo',
        'cliente_id', // Relacionado
        'contrato_id', // Relacionado
        'addedum_id', // Relacionado
        'factura_id', // Relacionado
        'publicacion_id', // Relacionado
        'incidencia_id',
        'usuario_id', // Relacionado
        'saldo',
        'cargo',
        'debito',
        'deuda',
        'contrato_saldo',
        'contrato_deuda',
        'contrato_cuota',
        'descripcion',
        'observaciones',
        'versiones_ids',
        'created_at',
        'nota_credito_id'
    ];

    protected $hidden = [
        "updated_at",
        "deleted_at",
    ];

    /**
     * Relación con modelo de Contrato
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cliente()
    {
        return $this->belongsTo('App\Models\Cliente');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contrato()
    {
        return $this->belongsTo('App\Models\Contrato');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function addedum()
    {
        return $this->belongsTo('App\Models\Contrato', 'addedum_id');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return array
     * Esta función se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacío entonces ninguno lo sera
     */
    public function getDates()
    {
        return array();
    }

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }

    /**
     * Movimiento de Balance por Tipo de Publicación
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balancePorTipoPublicacion()
    {
        return $this->hasMany('App\Models\BalancePorTipoPublicacion', 'balance_id');
    }

    /**
     * Relación con modelo Publicaciones
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function publicacion()
    {
        return $this->belongsTo('App\Models\Publicacion', 'usuario_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function factura()
    {
        return $this->belongsTo('App\Models\Factura', 'factura_id');
    }

}
