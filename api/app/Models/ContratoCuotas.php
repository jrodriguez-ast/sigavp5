<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author  Carlos Blanco<cebs923@@gmail.com>
 * @package App\Models
 */
class ContratoCuotas extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'contrato_cuotas';

    public $timestamps = false;

    protected $fillable = [
        "id",
        "monto",
        "contrato_id",
        "fecha",
        "factura_id",
        "monto_iva",
        'numero_cuota',
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];

    /**
     * Esta función se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacío entonces ninguno lo sera
     * @return array
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function getDates()
    {
        return array();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function factura()
    {
        return $this->belongsTo('App\Models\Factura', 'factura_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function contrato()
    {
        return $this->belongsTo('App\Models\Contrato', 'contrato_id');
    }
    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function facturaContratoCuotas()
    {
        return $this->belongsToMany('App\Models\Factura','factura_contrato_cuotas', 'cuota_id', 'factura_id');
    }
}