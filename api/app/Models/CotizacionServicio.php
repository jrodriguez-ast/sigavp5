<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CotizacionServicio extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'cotizaciones_servicios';

    protected $fillable = [
        "cotizacion_id",
        "servicio_id",
        'version_nombre',
        'version_file',
        'version_publicacion_fecha',
        'version_publicacion_fecha_inicio',
        'version_publicacion_fecha_fin',
        'tipo_publicacion_name',
        'tipo_publicacion_id',
        'version_segundos',
        'version_apariciones',
        'usuario_id',
        'monto_servicio',
        'medio_id',
        'valor_con_recargo',
        'aprobada'
    ];
    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'created_at', 'updated_at'];


    /**
     * Relación con modelo Cotización
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cotizacion()
    {
        return $this->belongsTo('App\Models\Cotizacion');
    }

    /**
     * Relación con modelo Servicio
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function servicio()
    {
        return $this->belongsTo('App\Models\Servicio');
    }

    /**
     * Relación con el modelo CotizaciónServicioCondición
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cotizacion_servicio_condicion()
    {
        return $this->hasMany('App\Models\CotizacionServicioCondicion','cotizaciones_servicios_id');
    }


}
