<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class NegociacionesServicios extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'negociaciones_servicios';

    protected $fillable = [
        'id', 'monto', 'estado', 'fecha', 'es_porcentaje', 'usuario_id', 'medio_id', 'negociacion_id', 'servicio_id',
        'exonerado'
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at', 'updated_at', 'deleted_at'];

    /**
     * Relación con Negociación
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function negociacion()
    {
        return $this->belongsTo('App\Models\Negociaciones', 'negociacion_id');
    }

    /**
     * Relación con Servicio.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function servicio()
    {
        return $this->belongsTo('App\Models\Servicio', 'servicio_id');
    }

    /**
     * Relación con Usuario.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Acl\User');
    }

}