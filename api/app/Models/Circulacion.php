<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Circulacion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'circulacion';

    protected $fillable = [
        "nombre",
        "usuario_id"
    ];

    protected $hidden = [
        "created_at",
        "updated_at"
    ];

}
