<?php namespace App\Models;

use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Medio extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = "medios";

    // Atributos que se pueden asignar de manera masiva.
    protected $fillable =
        [
            'razon_social',
            'rif',
            'fecha_fundacion',
            'direccion_fiscal',
            'telefonos',
            'correos_electronicos',
            'otros_detalles',
            'sitio_web',
            'rnc',
            'solvencia',
            'seniat',
            'nombre_comercial',
            'tipo_medio',
            'img_medio',
            'usuario_id',
        ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['created_at', 'updated_at'];


    /**
     * Relación con modelo de ContactoMedio
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contactos()
    {
        return $this->hasMany('App\Models\ContactoMedio');
    }

    /**
     * Relación con modelo Servicio
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function servicios()
    {
        return $this->hasMany('App\Models\Servicio');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $tipoPub
     * @return mixed
     * Retorna todos los servicios del tipo especificados que no tengan medio padre
     */
    public function serviciosByTipoPub($tipoPub)
    {
        return $this->hasMany('App\Models\Servicio')
            ->where('tipo_publicacion_id', '=', $tipoPub)
            ->where('es_cadena', '=', false)// ->whereDoesntHave('parent')
            ;
    }

    /**
     * @param $tipoPub
     * @return mixed
     */
    public function CadenasByTipoPub($tipoPub)
    {
        return $this->hasMany('App\Models\Servicio')
            ->where('tipo_publicacion_id', '=', $tipoPub)
            ->where('es_cadena', '=', true)// ->whereDoesntHave('parent')
            ;
    }

    /**
     * Relación con negociaciones
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function negociaciones()
    {
        return $this->hasMany('App\Models\Negociaciones');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function DetalleCondicionHistorico()
    {
        return $this->hasMany('App\Models\DetalleCondicionHistorico', 'medio_id');
    }

    /**
     * @param $tipoPublicacionId
     * @return mixed
     */
    public static function listado($tipoPublicacionId)
    {
        return DB::select("
            WITH RECURSIVE medios_jerarquia (id, razon_social, nivel, path_info) AS (
            (
                SELECT
                    mp.id,
                    razon_social,
                    0 as nivel,
                    razon_social || ' Nivel 1.' as path_info
                FROM medios mp -- mp Medio padre
                LEFT JOIN medios_medios mm -- mm Relación de medios
                        ON mp.id = mm.medio_hijo_id
                WHERE -- Filtra los medios que no tienen padres para obtener la parte más alta de la jerarquía
                        mm.medio_padre_id IS NULL
                ORDER BY razon_social
            )
            UNION
                SELECT
                        mh.id,
                        mh.razon_social,
                        mj.nivel+1 as nivel,
                        -- Se crea un string para ordenar alfabéticamente los niveles
                        mj.path_info || ' ' || mh.razon_social || ' Nivel ' || mj.nivel+1 || '.' as path_info
                FROM medios mh -- mh Medios hijos
                JOIN medios_medios mm2 -- Relación con tabla de muchos a muchos
                        ON mh.id = mm2.medio_hijo_id
                JOIN medios_jerarquia mj -- Se relaciona con el WITH recursivo para obtener la relación con cada padre
                        ON mm2.medio_padre_id = mj.id
            )
            SELECT
                mj.*, mtp.tipo_publicacion_id
            FROM medios_jerarquia mj
            LEFT JOIN medios_tipos_publicaciones mtp ON mj.id = mtp.medio_id
            WHERE deleted_at IS NULL
            AND tipo_publicacion_id = :id
            ORDER BY path_info;
        ",
            ['id' => $tipoPublicacionId]
        );

    }

    /**
     * @param $texto
     * @return mixed
     */
    public function getReferencia($texto)
    {
        return $this
            ->where('razon_social', 'ilike', "%{$texto}%")
            ->orWhere('rif', 'ilike', "%{$texto}%")
            ->get();
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function balance()
    {
        return $this->hasMany('App\Models\BalanceAvp');
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function tipo()
    {
        return $this->belongsTo('App\Models\TipoMedio', 'tipo_medio');
    }

    /**
     * Relación con Condición
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function medioPadre()
    {
        return $this->hasMany('App\Models\MediosMedios', 'medio_hijo_id');
    }

    /**
     * Relación con Condición
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function medioHijo()
    {
        return $this->hasMany('App\Models\MediosMedios', 'medio_padre_id');
    }

    /**
     * Relación con modelo User
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo('App\Models\Acl\User', 'usuario_id');
    }

    /**
     * Convierte el nombre comercial en mayúsculas.
     * @param $value
     */
    public function setNombreComercialAttribute($value)
    {
        $this->attributes['nombre_comercial'] = strtoupper($value);
    }

    /**
     *
     */
    public function updateDatos()
    {
        $proveedores = $this->all()->toArray();

        foreach ($proveedores as $proveedor):
            $consulta = new Consulta();
            $medio = Medio::find($proveedor['id']);
            $rnc = json_encode($consulta->getDataRNC(str_replace("-", "", $proveedor['rif'])));
            $solvencia = json_encode($consulta->getSolvencia($proveedor['rif']));
            $seniat = json_encode($consulta->getSeniat(str_replace("-", "", $proveedor['rif'])));
            $medio->rnc = $rnc;
            $medio->solvencia = $solvencia;
            $medio->seniat = $seniat;
            $medio->save();
            // print_r($rnc);
            // print_r($solvencia);
            //print_r($seniat);
        endforeach;
    }

    /**
     * Convierte la razón social en mayúsculas.
     * @param $value
     */
    public function setRazonSocialAttribute($value)
    {
        $this->attributes['razon_social'] = strtoupper($value);
    }

    /**
     * Convierte la dirección fiscal en mayúsculas.
     * @param $value
     */
    public function setDireccionFiscalAttribute($value)
    {
        $this->attributes['direccion_fiscal'] = strtoupper($value);
    }
}
