<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ContactoMedio extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'contactos_medios';

    protected $fillable = [
        "nombre",
        "apellido",
        "telefonos",
        "correos_electronicos",
        "cargo_id",
        "es_interno",
        "medio_id",
        "tipo",
        "usuario_id",
    ];

    protected $hidden = [

        "created_at",
        "updated_at"
    ];

    /**
     * Relación con el modelo de Cargo
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cargo()
    {
        return $this->belongsTo('App\Models\Cargo');
    }

    /**
     * Relación con el modelo de Horario
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function horario()
    {
        return $this->belongsTo('App\Models\Horario');
    }

    /**
     * Relación con modelo de Cliente
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medio()
    {
        return $this->belongsTo('App\Models\Medio');
    }

}
