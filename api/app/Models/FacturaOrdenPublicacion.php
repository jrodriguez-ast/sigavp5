<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * @author  Carlos Blanco<cebs923@@gmail.com>
 * @package App\Models
 */
class FacturaOrdenPublicacion extends Model {

    /**
     * Nombre de la tabla física en la base de datos
     *
     * @var string
     */
    protected $table = 'factura_orden_publicacion';

    public $timestamps = false;

    protected $fillable = [
        "id",
        "orden_id",
        "factura_id",
    ];

    protected $hidden = [
        "created_at",
        "updated_at",
        "deleted_at",
    ];
    /**
     * Esta función se usa para configurar el formateo de las fecha de Carbon. Todos los timestamps en Laravel son
     * convertidos a un objeto de carbon. En el array que se retorna se especifican los campos a ser convertidos
     * Si se retorna el array vacío entonces ninguno lo sera
     * @return array
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function getDates()
    {
        return array();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */



}
