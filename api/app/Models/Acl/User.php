<?php namespace App\Models\Acl;
//
//use Hash;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract, CanResetPasswordContract
{

    use Authenticatable, CanResetPassword;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'acl.users';

    /**
     * The primary key  used by the model.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['username', 'email', 'password', 'idsecretquestion', 'answer', 'confirmation_code', 'lastlogin'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * Relación de los usuarios y sus grupo
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function group()
    {
        return $this->belongsToMany('App\Models\Acl\Group', 'acl.users_groups', 'user_id', 'group_id');
    }

    /**
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function cotizaciones()
    {
        return $this->hasMany('App\Models\Cotizacion', 'usuario_id');
    }

    /**
     * @param $name
     * @param bool $only_id
     * @return mixed
     */
    public function findByName($name, $only_id = false)
    {
        $fields = ($only_id) ? ['id'] : ['*'];
        return $this->where('username', $name)->first($fields);
    }
}
