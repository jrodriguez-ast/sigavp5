<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DetalleCotizacion extends Model
{
    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'detalles_cotizaciones';

    protected $fillable = [
        "cotizacion_id",
        "medio_condicion_id",
        "monto",
        "descuento",
        "cantidad",
        "operacion",
        "total"
    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = ['usuario_id', 'created_at', 'updated_at'];


    /**
     * Relación con modelo Cotización
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function cotizacion()
    {
        return $this->belongsTo('App\Models\Cotizacion');
    }

    /**
     * Relación con modelo MedioCondición
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function medioCondicion()
    {
        return $this->belongsTo('App\Models\MedioCondicion');
    }

}
