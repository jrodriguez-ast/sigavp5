<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Pagos extends Model
{

    /**
     * Nombre de la tabla física en la base de datos
     * @var string
     */
    protected $table = 'public.pagos';

    protected $fillable = [
        'id',
        'usuario_id',
        'descripcion',
        'monto',
        'formas_pagos',
        'fecha_pago',
        'banco',
        'numero_transaccion'

    ];

    // Aquí ponemos los campos que no queremos que se devuelvan en las consultas.
    protected $hidden = [
        'created_at',
        'updated_at',
        'deleted_at'
    ];


    /**
     * Relación con Usuario.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function usuario()
    {
        return $this->belongsTo('App\Models\Acl\User');
    }
    /**
     * Relación con Factura.
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function factura()
    {
        return $this->belongsToMany('App\Models\Factura','public.fact_pago','pago_id', 'factura_id');
    }
    /**
     * Relación con Factura.
     *@author Maykol Purica
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
     public function contrato()
    {
        return $this->belongsToMany('App\Models\Contrato','public.contrato_pago','pago_id', 'contrato_id');
    }

}
