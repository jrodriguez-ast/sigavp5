<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TipoContrato extends Model {
    protected $table = 'tipo_contrato';
    // Atributos que se pueden asignar de manera masiva.
    protected $fillable = [
        'nombre',
        '_label',
    ];
   // protected $hidden = [ 'created_at', 'updated_at'];


}