<?php
namespace App\Modules\TiposPublicaciones\Http\Controllers;

use App\Http\Controllers\Controller;

// Activamos uso de caché.
use App\Models\Acl\User;
use App\Models\TipoPublicacion;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Response;
use Validator;


class TipoPublicacionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function dtIndex(Request $request)
    {
        // dd($request->all());die;
        $types = TipoPublicacion::select(array("id", "nombre", "descripcion"));
        return Datatables::of($types)->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $tipos = Cache::remember('tipos_publicaciones', 20 / 60, function () {
            // Caché válida durante 20 segundos.
            return TipoPublicacion::all();
        });

        // Con caché.
        return response()->json($tipos, 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $v = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            [
                'nombre' => 'required',
                'descripcion' => 'required',
                'username' => 'required',
            ]
        );
        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
        }

        $user = User::where('username', $request->header('username'))->first(['id']);
        $type = array(
            'nombre' => $request->input('nombre'),
            'descripcion' => $request->input('descripcion'),
            'usuario_id' => $user->id,
        );
        $type = TipoPublicacion::create($type);

        // Más información sobre respuestas en http://jsonapi.org/format/
        // Devolvemos el código HTTP 201 Created – [Creada] Respuesta a un POST que resulta en una creación. Debería ser combinado con un encabezado Location, apuntando a la ubicación del nuevo recurso.
        $response = Response::make(json_encode(array('status' => 'ok', 'data' => $type)), 201)
            ->header('Location', 'http://sigavp/api/public/cotizaciones/tipos_publicaciones/' . $type->id)
            ->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $type = TipoPublicacion::find($id);
        // Si no existe ese fabricante devolvemos un error.
        if (!$type) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra el Tipo de Publicación con ese código.'])], 404);
        }

        return response()->json(array('status' => 'ok', 'data' => $type), 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            [
                'nombre' => 'required',
                'descripcion' => 'required',
                'username' => 'required',
            ]
        );
        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
        }

        // Comprobamos si el fabricante que nos están pasando existe o no.
        $type = TipoPublicacion::find($id);

        // Si no existe ese fabricante devolvemos un error.
        if (!$type) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Tipo de Publicación con ese código.'])], 404);
        }

        $user = User::where('username', $request->header('username'))->first(['id']);

        $type->nombre = $request->input('nombre');
        $type->descripcion = $request->input('descripcion');
        $type->usuario_id = $user->id;

        // Almacenamos en la base de datos el registro.
        $type->save();
        return response()->json(['status' => 'ok', 'data' => $type], 200, array('Location' => 'http://sigavp/api/public/cotizaciones/tipos_publicaciones/' . $type->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $flag=false;
        $msj=null;
        $tipo_publicacion = TipoPublicacion::find($id);
        if (!$tipo_publicacion) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un tipo de publicacion con ese código.'], 404);
        }


        /* Se comentaron estas lineas porque no estaban los id's correspondintes 
        en las tablas para verificar estas relaciones.
         
        No existia la columna.
        if ($tipo_publicacion->condiciones()->count() > 0) {
            $flag=true;
            $msj="Este registro tiene condiciones asociadas.";
        }
        
        No existia la columna. Hay columnas que guardan una serie de info en JSON,
        pero niguna contenia info relacionada al tipo de publicación.
        if ($tipo_publicacion->medios()->count() > 0) {
            $flag=true;
            $msj="Este registro tiene medios asociados.";
        }*/

        if ($tipo_publicacion->tiposIncidencias()->count() > 0) {
            $flag=true;
            $msj="Este registro tiene tipo de incidencias asociadas.";
        }

        if ($tipo_publicacion->genericConfiguration()->count() > 0) {
            $flag=true;
            $msj="Este registro tiene genericConfiguration asociadas.";
        }

        if ($tipo_publicacion->servicios()->count() > 0) {
            $flag=true;
            $msj="Este registro tiene servicios asociadas.";
        }

        if ($tipo_publicacion->contratoPauta()->count() > 0) {
            $flag=true;
            $msj="Este registro pautas de contrato asociadas.";
        }

        if($flag == true){
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => $msj], 200);
        }

        $tipo_publicacion->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el tipo de publicacion correctamente.'], 200);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function darwinIndex()
    {
        $input = Input::all();
        $tiposPublicacion = TipoPublicacion::select();

        foreach ($input as $key => $value) {
            $tiposPublicacion->where($key, 'ilike', "%{$value}%");
        }

        return response()->json($tiposPublicacion->get());
    }


    public function listado_for_medio($medioId)
    {
        return response()->json(TipoPublicacion::listado_for_medios($medioId), 200);

    }

}
