<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */

Route::group(['prefix' => 'tipos_publicaciones'], function () {
    Route::get('listado_for_medio/{medioId}', 'TipoPublicacionController@listado_for_medio');
    Route::get('dt', 'TipoPublicacionController@dtIndex');
    Route::resource('tipo', 'TipoPublicacionController');

});
