<?php
Route::group(['prefix' => 'notificacion'], function() {

            //muestra los registro
            //Route::get('notifica/{todos}', 'MiControllador@mostrarNoti');
                Route::get('notifica', 'NotificacionController@mostrarNoti');

                Route::get('notificatodo', 'NotificacionController@mostrartodaNoti');

            //muestra total de registro
                Route::get('mostrar_num_noti', 'NotificacionController@mostrarNumeroNoti');

            //Modifica el registro para cambiar el estatus
                Route::get('notificacionsave/{id}', 'NotificacionController@notificacionVista');
});