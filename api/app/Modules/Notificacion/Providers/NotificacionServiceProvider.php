<?php
namespace App\Modules\Notificacion\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class NotificacionServiceProvider extends ServiceProvider
{
	/**
	 * Register the Notificacion module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Notificacion\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Notificacion module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('notificacion', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('notificacion', realpath(__DIR__.'/../Resources/Views'));
	}
}
