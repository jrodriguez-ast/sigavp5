<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'cotizaciones'], function () {

    Route::resource('cotizacion', 'CotizacionController');
    Route::resource('cotizacion.servicios', 'CotizacionServiciosController');
    Route::get('cotizacion_servicios_dt', 'CotizacionServiciosController@dtIndex');
    Route::get('dt', 'CotizacionController@dtIndex');
    Route::get('dt_client/{clienteId}', 'CotizacionController@dtClientIndex');
    Route::get('pendiente/{clienteId}', 'CotizacionController@pendiente');
    Route::get('pre_creacion/{clienteId}', 'CotizacionController@pre_creacion');
    Route::put('enviarCotizacion/{cotizacionId}', 'CotizacionController@send_quote_client');
    Route::put('asociarContrato/{cotizacion_id}/{contrato_id}', 'CotizacionController@asociar_contrato');
    Route::post('save_orden_servicio', 'CotizacionOrdenServicioController@store');


    // Condiciones de Pauta y Cotización
    Route::resource('conditions', 'ConditionController');
    Route::get('conditions_dt', 'ConditionController@dtIndex');
    Route::get('{servicioId}/{tipoPublicacionId}/conditions/listado', 'ConditionController@listado');
    Route::get('{servicioId}/{condicionId}/conditions/listado_detalle_condicion', 'ConditionController@listado_detalle_condicion');

    // TIpos Incidencias
    Route::resource('tipos_incidencias', 'TipoIncidenciaController');
    Route::get('tipos_incidencias_dt', 'TipoIncidenciaController@dtIndex');

    //Recargo Gestion
    Route::resource('recargo_gestion', 'RecargoGestionController');
    Route::get('recargo_gestion_dt', 'RecargoGestionController@dtIndex');

    //Iva
    Route::resource('iva', 'IvaController');
    Route::get('iva_dt', 'IvaController@dtIndex');


    Route::get('pdf_pre/{cotizacionId}/{noValido}', 'CotizacionController@pre_visualizar_cotizacion');

});