<?php namespace App\Modules\Cotizaciones\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Cliente;
use App\Models\Contrato;
use App\Models\Cotizacion;
use App\Models\CotizacionServicio;
use App\Models\Iva;
use App\Models\Negociaciones;
use App\Models\NegociacionesCondiciones;
use App\Models\NegociacionesCondicionesDetalles;
use App\Models\NegociacionesServicios;
use App\Models\Notificacion;
use App\Models\RecargoGestion;
use App\Models\Utilities\GenericConfiguration;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use yajra\Datatables\Datatables;


class CotizacionController extends Controller
{
    private $attach_mail = null;
    private $data_email = null;
    private $all_emails = null;
    private $data_cliente = null;
    private $PdfCotizacionTemplate = 'cotizacion/doc/PdfCotizacion';

    public function __construct()
    {
        $this->middleware('data.user');
    }

    /**
     * Permite crear la  cotización
     *
     * @access Public
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:01 AM
     */
    public function store()
    {
        $p_iva = Iva::where('activo', true)->firstOrFail();
        $p_avp = RecargoGestion::where('activo', true)->firstOrFail();
        $dias_validez = GenericConfiguration::where('_label', 'cotizacion_validez')->pluck('value');
        $dias_validez = ($dias_validez ? $dias_validez : 15);

        //Preparación de datos que se insertaran en la creación de la cotización
        $defaultCotizacion = [
            "cliente_id" => Input::get('clienteId'),
            "fecha_creacion" => Carbon::now()->toDateTimeString(),
            "fecha_expiracion" => Carbon::now()->addDay($dias_validez)->toDateTimeString(),
            "descuento" => 0,
            "con_impuestos" => false,
            "porcentaje_impuesto" => (float)$p_iva->iva,
            'porcentaje_avp' => (float)$p_avp->porcentaje,
            'usuario_id' => session()->get('userId'),
            'firmante_nombre' => GenericConfiguration::where('_label', 'cotizacion_firmante_nombre')->pluck('value'),
            'firmante_cargo' => GenericConfiguration::where('_label', 'cotizacion_firmante_cargo')->pluck('value')
        ];

        //Se eliminan las cotizaciones Sin Finalizar asociadas al cliente
        $this->destroy_cotizaciones_pendientes(Input::get('clienteId'));

        //Se crea la cotización nueva
        $cotizacion = Cotizacion::create($defaultCotizacion);
        return response()->json($cotizacion->toArray(), 200);
    }

    /**
     * Permite eliminar cotizaciones pendientes(Sin Finalizar) con respecto a un cliente
     *
     * @access public
     * @param integer $clienteId Identificador del Cliente
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 17/09/15 12:34 PM
     */
    public function destroy_cotizaciones_pendientes($clienteId = null)
    {
        if (empty($clienteId))
            return false;

        $Cliente = Cliente::with([
            'cotizaciones' => function ($q) {
                $q->where('es_terminada', false);
            }
        ])
            ->find($clienteId);
        if ($Cliente):
            if (!empty($Cliente['cotizaciones'])):
                foreach ($Cliente['cotizaciones'] as $key => $value):
                    $this->destroy($value['id']);
                endforeach;
            endif;
            return true;
        endif;
        return false;
    }

    /**
     * Permite eliminar una cotización de forma permanente
     *
     * @access public
     * @param integer $cotizacionId Identificador de la Cotización
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 15/09/15 11:34 AM
     */
    public function destroy($cotizacionId)
    {
        $flag=false;
        $msj=null;
        $Cotizacion = Cotizacion::with([
            'detalles'
        ])->find($cotizacionId);

        if ($Cotizacion) {
            foreach ($Cotizacion['detalles'] as $key => $value):
                $CotizacionServicio = CotizacionServicio::find($value['id']);
                if ($CotizacionServicio) {
                    $CotizacionServicio->cotizacion_servicio_condicion()->delete();
                    $CotizacionServicio->delete();
                }
            endforeach;

            if ($Cotizacion->orden_servicio()->count() > 0) {
                $flag=true;
                $msj="Este registro tiene ordenes de servicios asociadas.";
            }

            if ($Cotizacion->orden_publicacion()->count() > 0) {
                $flag=true;
                $msj="Este registro tiene ordenes de publicaciones asociadas.";
            }

            if ($Cotizacion->contrato()->count() > 0) {
                $flag=true;
                $msj="Este registro pertenece a un contrato.";
            }

            if($flag == true){
                return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => $msj], 200);
            }

            $Cotizacion->orden_servicio()->delete();
            $Cotizacion->delete();
            //return response()->json($Cotizacion, 200);
            return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado la cotizacion correctamente.'], 200);
        }
    }

    /**
     * Muestra los datos de la cotización y aplica los cálculos con
     * respecto a los datos cargados.
     *
     * @access public
     * @param integer $id Identificador de la cotización
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 10:53 AM
     */
    public function show($id)
    {
        Cotizacion::calculate_quote($id);
        $cotizacion = Cotizacion::data_for_quote($id);

        //calculo los dias que hay entre las dos fechas
        foreach ($cotizacion['versiones'] as $key => $version) :
            $fecha_inicio = $version['version_publicacion_fecha_inicio'];
            $fecha_final = $version['version_publicacion_fecha_fin'];
            $diasporpublicacion = Carbon::createFromTimestamp(strtotime(($fecha_final)))->diffInDays(Carbon::createFromTimestamp(strtotime($fecha_inicio)));
            $diasporpublicacion++;
            $cotizacion['versiones'][$key]['diasporpublicacion'] = $diasporpublicacion;
        endforeach;

        return response()->json($cotizacion, 200);
    }

    /**
     * Permite actualizar la cotización
     *
     * @access Public
     * @param Request $request
     * @param integer $id Identificador de la Cotización
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:01 AM
     */
    public function update(Request $request, $id)
    {

        if ($request->has('validar_monto') && $request->get('validar_monto') &&
            !Cotizacion::montoAprobado($id, $request->input('versiones'))
        ) {
            return response()->json([
                'status' => 'fail',
                'errors' => [
                    'code' => 9999,
                    'message' => 'El saldo disponible en contrato no satisface la aprobación de estas publicaciones.'
                ]
            ]);
        }

        $cotizacion = Cotizacion::find($id);
        $cotizacion->porcentaje_impuesto = $request->input('porcentaje_impuesto');
        $cotizacion->usuario_id = session()->get('userId');
        $cotizacion->porcentaje_avp = $request->input('porcentaje_avp');
        $cotizacion->fecha_expiracion = $request->input('fecha_expiracion');
        $cotizacion->es_terminada = ($request->has('es_terminada')) ? $request->input('es_terminada') : false;
        $cotizacion->create_code = (!empty($request->input('create_code')) ? $request->input('create_code') : false);

        // Esto para q se modifique cotización->aprobado solo al momento de aprobar.
        if ($request->has('aprobada') && $request->input('aprobada') == true) {
            $cotizacion->aprobada = true;
        }

        // Asocio id de contrato a cotización.
        //$cotizacion->contrato_id = ($request->has('contrato')) ? $request->input('contrato')['id'] : NULL;

        $con_email = ($request->has('con_email')) ? $request->input('con_email') : false;
        $data_email = ($request->has('data_email')) ? $request->input('data_email') : false;

        if (count($request->versiones) > 0):
            $total_versiones_aprobadas = 0;
            foreach ($request->versiones as $key => $value):
                if (!empty($value['tmp_aprobada']) && $value['tmp_aprobada']):
                    $CotizacionServicio = CotizacionServicio::find($value['id']);
                    $CotizacionServicio->aprobada = $value['tmp_aprobada'];

                    //Se inserta en la entidad CotizaciónServicio
                    //Si la inserción es exitosa de agregan los detalles de la condición
                    if ($CotizacionServicio->save()):
                        $total_versiones_aprobadas++;
                    endif;
                endif;
            endforeach;

            // Esto se lo vamos a delegar al trigger de creación de orden de publicación.
            //$total_versiones_cotizadas = CotizacionServicio::where('cotizacion_id', $id)->count();
            //if (($total_versiones_cotizadas - $total_versiones_aprobadas) < 1) {
            //    $cotizacion->aprobada = true;
            //}
        endif;
        if ($con_email):
            $cotizacion->fue_enviada = (count($this->send_mail_cotizacion($id, $data_email)) > 0 ? true : false);
        endif;

        if ($cotizacion->save()):
            Cotizacion::calculate_quote($id);
            $response = Cotizacion::data_for_quote($id);

            if ($response->codigo != null) {
                //Datos para la notificación
                $datosNotificacion = [
                    'chekk' => 0,
                    'tit' => 'Cotización Creada',
                    'des' => 'Se ha creado la cotización #' . $response['codigo'] . ' exitosamente',
                    'lin' => 'admin.cotizacion_editar',
                    'titu_icon' => 'Cotización Creada',
                    'tipo_not_ico' => 'fa fa-credit-card',
                    'ids' => json_encode([
                        'cotizacion_id' => $id
                    ])
                ];
                Notificacion::create($datosNotificacion);
            }
            return response()->json(array('status' => 'ok', 'data' => $response), 200);
        else:
            return response()->json([
                'status' => 'fail',
                'errors' => [
                    ['code' => 304, 'message' => 'No se ha modificado ningún dato de la cotización.']
                ]
            ], 304);

        endif;
    }

    /**
     * Envía un email a los contactos del cliente con respecto a una cotización
     *
     * @access public
     * @param null $cotizacionId Identificador de la Cotización
     * @param null $data_email Datos pertinentes al envío del email
     * @return bool
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:20 AM
     */
    public function send_mail_cotizacion($cotizacionId = null, $data_email = null)
    {
        if (empty($cotizacionId))
            return false;

        $response = false;
        $cotizacion = Cotizacion::data_for_quote($cotizacionId);

        if (!empty($cotizacion)):
            $pdf = App::make('dompdf.wrapper');
            $iniciales = '';
            $cotizacion['iniciales_firmante'] = $this->get_iniciales_string($cotizacion['firmante_nombre'], 'U');
            if (!empty($cotizacion['user'])):
                $cotizacion['iniciales_usuario'] = $this->get_iniciales_string($cotizacion['user']['first_name'] . " " . $cotizacion['user']['last_name']);
            endif;
            $cotizacion['novalido'] = asset('/css/cotizacion_pdf.css');
            $pdf->loadView($this->PdfCotizacionTemplate, $cotizacion);
            $pdf->setPaper('a4')->save("storage/DOC_$cotizacion->codigo.pdf")->setWarnings(false);

            $public_path = public_path();
            $this->attach_mail = $public_path . '/storage/DOC_' . $cotizacion->codigo . '.pdf';

            $this->data_email = $data_email;
            $this->all_emails = $cotizacion['cliente']->correos_electronicos;
            $this->data_cliente = $cotizacion['cliente'];
            //verificamos si el archivo existe y lo retornamos
            if (Storage::exists("DOC_$cotizacion->codigo.pdf")) {
                $response = Mail::send('EmailCotizacion', array('data' => $cotizacion), function ($message) {
                    $message->from('no-responder@sigavp.ast.com.ve', 'Agencia Venezolana de Publicidad');
                    if (!empty($this->data_email)):
                        if (!empty($this->data_email['todos']) && $this->data_email['todos']):
                            foreach (json_decode($this->all_emails, true) as $key => $value):
                                $message->to($value, $this->data_cliente['razon_social']);
                            endforeach;
                        elseif (!empty($this->data_email['email_send'])):
                            foreach ($this->data_email['email_send'] as $clave => $valor):
                                if ($valor['activo'])
                                    $message->to($valor['mail'], $valor['mail']);
                            endforeach;
                        endif;

                    endif;

                    //Envío con copia tipeada (custom)
                    if (!empty($this->data_email['cc'])):
                        $message->cc($this->data_email['cc']);
                    endif;


                    //Envío de correos a cuentas AVP para cotizaciones  BY MARIANNNE AND FREDERICK
                    $cuentas_correo_avp = GenericConfiguration::where('_label', 'cotizacion_correo')->pluck('value');
                    if ($cuentas_correo_avp):
                        $cuentas = explode(";", $cuentas_correo_avp);
                        foreach ($cuentas as $valor):
                            $message->cc(trim($valor));
                        endforeach;
                    endif;

                    //Envío con copia tipeada (custom)
                    if (!empty($this->data_email['cc'])):
                        $message->cc($this->data_email['cc']);
                    endif;
                    //Envio de a contactos administrativos y publicitarios asociados al cliente
                    if (!empty($this->data_email['contactos'])):
                        foreach ($this->data_email['contactos'] as $clave_c => $valor_c):

                            if (($valor_c['tipo'] == 'administrativo' || $valor_c['tipo'] == 'publicitario') && !empty($valor_c['correos_electronicos'])):
                                foreach ($valor_c['correos_electronicos'] as $clave_c_correos => $valor_c_correos):
                                    if ($valor_c_correos['activo'] && !empty($valor_c_correos['a']))
                                        $message->cc($valor_c_correos['a'], $valor_c['nombre'] . " " . $valor_c['apellido']);
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                    $message->subject('Generada Cotización AVP para ' . $this->data_cliente['razon_social']);
                    $message->attach($this->attach_mail);
                });
            }
        endif;
        return $response;
    }

    /**
     * Dada una cadena de texto obtiene las iniciales de cada palabra
     *
     *
     * @access
     * @param String $name Cadena a procesar
     * @param string $case Establece como sera la salida de la cadena procesada (mayúscula, minúsculas)
     * @return string
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:23 AM
     */
    public function get_iniciales_string($name, $case = 'L')
    {
        if (empty($name)) {
            return $name;
        }

        $initials = $name;
        $splitted_name = explode(' ', trim($name));
        if (!empty($splitted_name)) {
            $initials = '';
            foreach ($splitted_name as $row)
                $initials .= substr(trim($row), 0, 1);
        }
        return ($case == 'U') ? strtoupper($initials) : strtolower($initials);
    }

    /**
     * Provee los datos al datatable
     *
     * @access Public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:01 AM
     */
    public function dtIndex()
    {
        $cotizaciones = Cotizacion::leftJoin('acl.users', 'public.cotizaciones.usuario_id', '=', 'acl.users.id')
            ->leftJoin('public.clientes', 'public.clientes.id', '=', 'public.cotizaciones.cliente_id')
            ->select(array(
                'public.cotizaciones.id',
                'public.cotizaciones.codigo',
                'public.cotizaciones.es_terminada',
                'razon_social',
                'fecha_creacion',
                'fecha_expiracion',
                'aprobada',
                'first_name',
                'last_name'
            ));

        $cotizaciones = new Collection($cotizaciones->get());

        return Datatables::of($cotizaciones)
            ->editColumn('fecha_creacion', function ($data) {
                $date = date_create($data->fecha_creacion);
                return date_format($date, 'd/m/Y');
            })
            ->editColumn('fecha_expiracion', function ($data) {
                $date = date_create($data->fecha_expiracion);
                return date_format($date, 'd/m/Y');
            })
            ->editColumn('usuario', function ($data) {
                return $data->first_name . ' ' . $data->last_name;
            })
            ->editColumn('aprobada_text', '@if($aprobada)
                                Aprobada
                            @else
                                Por Aprobar
                            @endif')
            ->make(true);

    }

    /**
     * Provee los datos de una cotización datatable con respecto a un cliente
     *
     * @param integer $clienteId Identificador del cliente
     * @access Public
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:01 AM
     */
    public function dtClientIndex($clienteId)
    {
        $cotizaciones = Cotizacion::where('public.clientes.id', $clienteId)
            ->leftJoin('acl.users', 'public.cotizaciones.usuario_id', '=', 'acl.users.id')
            ->leftJoin('public.clientes', 'public.clientes.id', '=', 'public.cotizaciones.cliente_id')
            ->select(array(
                'public.cotizaciones.id',
                'public.cotizaciones.codigo',
                'razon_social',
                'fecha_creacion',
                'fecha_expiracion',
                'aprobada',
                'first_name',
                'last_name'
            ));

        return Datatables::of($cotizaciones)
            ->editColumn('fecha_creacion', function ($data) {
                $date = date_create($data->fecha_creacion);
                return date_format($date, 'd/m/Y');
            })
            ->editColumn('fecha_expiracion', function ($data) {
                $date = date_create($data->fecha_expiracion);
                return date_format($date, 'd/m/Y');
            })
            ->editColumn('usuario', function ($data) {
                return $data->first_name . ' ' . $data->last_name;
            })
            ->editColumn('aprobada_text', '@if($aprobada)
                                Aprobada
                            @else
                                Por Aprobar
                            @endif')
            ->make(true);

    }

    /**
     * @param $clienteId
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function pendiente($clienteId)
    {
        $cotizacion = Cotizacion::where('cliente_id', $clienteId)
            ->whereRaw('es_terminada IS FALSE')
            ->select(['id'])
            ->first();

        return response()->json($cotizacion);
    }

    /**
     * Permite previsualizar la cotización
     *
     * @access public
     * @param integer $cotizacionId Identificador de la Cotización
     * @param boolean $noValido Establece si el documento llevara marca de no valido o no
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:01 AM
     */
    public function pre_visualizar_cotizacion($cotizacionId = null, $noValido)
    {

        if (empty($cotizacionId))
            return false;
        $cotizacion = Cotizacion::data_for_quote($cotizacionId);
        if (!empty($cotizacion)):
            $pdf = App::make('dompdf.wrapper');

            $cotizacion['iniciales_firmante'] = $this->get_iniciales_string($cotizacion['firmante_nombre'], 'U');
            if (!empty($cotizacion['user'])):
                $cotizacion['iniciales_usuario'] = $this->get_iniciales_string($cotizacion['user']['first_name'] . " " . $cotizacion['user']['last_name']);
            endif;

            $cotizacion['novalido'] = asset('/css/cotizacion_pdf.css');

            if ($noValido == 'true')
                $cotizacion['novalido'] = asset('/css/cotizacion_pdf_novalido.css');


            $cotizacion['config'] = $this->get_configuration_generic();
            $pdf->loadView($this->PdfCotizacionTemplate, $cotizacion);
            return @$pdf->setPaper('a4')
                ->setWarnings(false)
                ->download("DOC_" . (is_null($cotizacion->codigo) ? Carbon::now()->toDateTimeString() : $cotizacion->codigo) . ".pdf");
        else:
            echo "No se pudo obtener el documento";
        endif;
    }

    /**
     * Obtiene registros específicos o generales en la entidad GenericConfiguration
     *
     * @access  public
     * @param null $where Permite condicionar la consulta
     * @return array
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:27 AM
     */
    public function get_configuration_generic($where = null)
    {
        $out = array();
        if (!empty($where)):
            $data = GenericConfiguration::where($where)->get();
        else:
            $data = GenericConfiguration::all();
        endif;
        if (!empty($data)):
            foreach ($data as $key => $value):
                $out[$value['_label']] = $value;
            endforeach;
        endif;

        return $out;
    }

    /**
     * Provee las negociaciones asociadas a un medio
     *
     * @access public
     *
     * @param integer $medio_id Identificador del medio
     * @param integer $servicio_id Identificador del Servicio
     * @param integer $condicion_id Identificador de la condición
     * @param integer $condicion_detalle_id Identificador del detalle de la condición
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:16 AM
     */
    public function get_negociaciones($medio_id, $servicio_id, $condicion_id, $condicion_detalle_id)
    {

        $response = false;
        //Se verifica si existe negociación
        $negociaciones = Negociaciones::where(array('medio_id' => $medio_id))->get();

        if (!$negociaciones->isEmpty()):

            //Servicios negociados
            $negociaciones_servicios = NegociacionesServicios::where(
                array(
                    'negociacion_id' => $negociaciones[0]['id'],
                    'servicio_id' => $servicio_id
                )
            )->get();

            //Condiciones negociadas
            $negociaciones_condiciones = NegociacionesCondiciones::where(
                array(
                    'negociacion_id' => $negociaciones[0]['id'],
                    'condicion_id' => $condicion_id
                )
            )->get();


            $negociaciones_condiciones_detalle = NegociacionesCondicionesDetalles::where(
                array(
                    'negociacion_id' => $negociaciones[0]['id'],
                    'condicion_detalle_id' => $condicion_detalle_id
                )
            )->get();


            if (!$negociaciones_servicios->isEmpty())://Verifica si tiene servicio
                $response['servicio'] = $negociaciones_servicios[0];
                $response['servicio']['negociacion'] = true;
            elseif (!$negociaciones_condiciones->isEmpty()): //Verifica si hay condicion
                $response['condicion'] = $negociaciones_condiciones[0];
                $response['condicion']['negociacion'] = true;
            elseif (!$negociaciones_condiciones_detalle->isEmpty())://Verifica si hay condicion detalles
                $response = $negociaciones_condiciones_detalle[0];
                $response->tipo_negociacion = 'detalle';
                $response->negociacion = true;

            endif;
        endif;

        return $response;
    }

    /**
     * Envía un email con respecto a una cotización
     *
     * @access public
     * @param Request $request Dependencia para recibir variables
     * @param int $id Identificador de la cotización
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:25 AM
     */
    public function send_quote_client(Request $request, $id)
    {
        $cotizacion = Cotizacion::find($id);
        $data_email = $request->has('data_email') ? $request->input('data_email') : false;
        $cotizacion->fue_enviada = (count($this->send_mail_cotizacion($id, $data_email)) > 0 ? true : false);

        if ($cotizacion->save()):
            $response = Cotizacion::data_for_quote($id);
            return response()->json(array(
                'status' => 'ok',
                'data' => $response
            ), 200);
        else:
            return response()->json(
                array(
                    'status' => 'fail',
                    'errors' => array(
                        [
                            'code' => 304,
                            'message' => 'No se ha modificado ningún dato de la cotización.'
                        ]
                    )
                ), 304);

        endif;
    }

    /**
     * @param $cotizacion_id
     * @param $contrato_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function asociar_contrato($cotizacion_id, $contrato_id)
    {
        try {
            $cotizacion = Cotizacion::findorFail($cotizacion_id);
            Contrato::findorFail($contrato_id);
        } catch (ModelNotFoundException $e) {
            return response()->json([
                'status' => 'fail',
                'errors' => ['message' => 'Cotización o Contrato Invalido.']
            ], 304);
        }

        $cotizacion->contrato_asociado = '1';
        $cotizacion->contrato_id = $contrato_id;
        if (!$cotizacion->save()) {
            // Fallo el Guardado
            return response()->json([
                'status' => 'fail',
                'errors' => ['message' => 'No se ha modificado ningún dato de la cotización.']
            ], 304);
        }

        return response()->json(['status' => 'ok']);
//        return response()->json(['status' => 'ok', 'data' => Cotizacion::data_for_quote($cotizacion_id)]);
    }
}
