<?php
namespace App\Modules\Cotizaciones\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\TipoIncidencia;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Validator;

class TipoIncidenciaController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function dtIndex()
    {
        $condicions = TipoIncidencia::
        join('tipos_publicaciones', 'tipos_incidencias.tipo_publicacion_id', '=', 'tipos_publicaciones.id')
            ->select(['tipos_incidencias.id', 'tipos_incidencias.nombre', DB::raw('tipos_publicaciones.nombre as nombre_tipo, tipos_publicaciones.id as id_tipo')])
            // ->select('tipos_incidencias.id', 'tipos_incidencias.nombre')
            ->get();
        return Datatables::of($condicions)->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $v = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            [
                'nombre' => 'required',
                'tipo_publicacion' => 'required',
                'username' => 'required',
            ]
        );
        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
        }

        $user = User::where('username', $request->header('username'))->first(['id']);

        $type = array(
            'nombre' => $request->input('nombre'),
            'tipo_publicacion_id' => $request->input('tipo_publicacion')['id'],
            'usuario_id' => $user->id,
        );
        $type = TipoIncidencia::create($type);

        // Más información sobre respuestas en http://jsonapi.org/format/
        // Devolvemos el código HTTP 201 Created – [Creada] Respuesta a un POST que resulta en una creación. Debería ser combinado con un encabezado Location, apuntando a la ubicación del nuevo recurso.
        $response = Response::make(json_encode(array('status' => 'ok', 'data' => $type)), 201)
            ->header('Location', 'http://sigavp/api/public/cotizaciones/tipos_incidencias/' . $type->id)
            ->header('Content-Type', 'application/json');
        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        // return "Se muestra Fabricante con id: $id";
        // Buscamos un fabricante por el id.
        $type = TipoIncidencia::find($id);
        $type->TipoPublicacion = $type->tipoPublicacion()->first();

        // Si no existe ese fabricante devolvemos un error.
        if (!$type) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra el tipo de incidencia con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $type], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $v = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            [
                'nombre' => 'required',
                'tipo_publicacion' => 'required',
                'username' => 'required',
            ]
        );
        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
        }

        // Comprobamos si el fabricante que nos están pasando existe o no.
        $type = TipoIncidencia::find($id);

        // Si no existe ese fabricante devolvemos un error.
        if (!$type) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Tipo de Publicación con ese código.'])], 404);
        }

        $user = User::where('username', $request->header('username'))->first(['id']);

        $type->nombre = $request->input('nombre');
        $type->tipo_publicacion_id = $request->input('tipo_publicacion')['id'];
        $type->usuario_id = $user->id;

        // Almacenamos en la base de datos el registro.
        $type->save();
        return response()->json(['status' => 'ok', 'data' => $type], 200, array('Location' => 'http://sigavp/api/public/cotizaciones/tipos_incidencias/' . $type->id));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $tipo_incidencia = TipoIncidencia::find($id);
        if (!$tipo_incidencia) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un tipo de incidencia con ese código.'], 200);
        }

        $tipo_incidencia->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el tipo de incidencia correctamente.'], 200);
    }

}
