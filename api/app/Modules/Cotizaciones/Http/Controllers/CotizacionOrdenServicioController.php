<?php namespace App\Modules\Cotizaciones\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Faker\Provider\File;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Models\CotizacionOrdenServicio;
use App\Models\Cotizacion;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use App\Models\Acl\User;

class CotizacionOrdenServicioController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {

      //Provee el identificador del usuario que esta logeado  en el sistema
        $this->middleware('data.user');
    }

    /**
     * Creación de la Orden de Servicio
     *
     * @access public
     * @param Request $request Dependencia para recibir variables
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 31/08/15 11:25 AM
     */
    public function store(Request $request)
    {
        //obtenemos el campo file definido en el formulario
        $file = $request->file('file');
        $data = array(
            'success' => false
        );
        if (!empty($file)):
            //obtenemos el nombre del archivo
            $nombre = str_random() . '.' . $file->getClientOriginalExtension();

            //indicamos que queremos guardar un nuevo archivo en el disco local/tmp
            if (\Storage::disk('cotizacion_orden_servicio')->put($nombre, \File::get($file))):
                $data = array(
                    'success' => true,
                    'file_name' => $nombre,
                    'message' => 'Archivo agregado exitosamente'
                );
            endif;
        endif;
        $dataCreate = [
            "cliente_id" => Input::get('cliente_id'),
            "cotizacion_id" => Input::get('id'),
            "file" => ($data['success'] ? $data['file_name'] : ''),
            "via_recepcion_id" => Input::get('via_recepcion_id'),
            "observaciones" => Input::get('observaciones'),
            "usuario_id" => session()->get('userId')
        ];
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($dataCreate, $row);
        $insert = CotizacionOrdenServicio::create($row);
        $cotizacion = Cotizacion::data_for_quote(Input::get('id'));

        return response()->json(array('status' => 'ok', 'data' => $cotizacion), 200);
    }

}
