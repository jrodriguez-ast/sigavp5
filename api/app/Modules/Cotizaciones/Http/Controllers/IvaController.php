<?php namespace App\Modules\Cotizaciones\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Iva;
use App\Models\Acl\User;
use Response;
use Validator;
use Datatables;
use Illuminate\Http\Request;

use App\Modules\Cotizaciones\Http\Requests\IvaRequest;

class IvaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $model = Iva::where('activo', true)->firstOrFail();

        return response()->json(['status' => 'ok', 'data' => $model], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param IvaRequest $request
     * @return Response
     */

    public function store(IvaRequest $request)
    {

        //Bloque de actualizacion de activo a falso
        Iva::where('activo', true)->update(array('activo' => false));

        //Fin del bloque de actualizacion

        // Bloque de creacion
        $user = User::where('username', $request->header('username'))->first(['id']);
        $nuevoIva = array(
            'iva' => $request->input('iva'),
            'usuario_id' => $user->id,
            'activo' => true,
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($nuevoIva, $row);
        $nuevoIva = Iva::create($row);

        //Fin bloque de creacion

        //  Bloque de respuestas http
        $response = Response::make(json_encode(array('status' => 'ok', 'data' => $nuevoIva)), 201)
            ->header('Location', 'http://sigavp/api/public/cotizaciones/iva/' . $nuevoIva->id)
            ->header('Content-Type', 'application/json');

        return $response;
        //Fin bloque respuestas http
    }

    /**
     * Display the specified resource.
     * @return Response
     */
    public function show()
    {

        $model = Iva::where('activo', true)->firstOrFail();

        return response()->json(['status' => 'ok', 'data' => $model], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $iva = Iva::find($id);
        if (!$iva) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un IVA con ese código.'])], 404);
        }

        if ($iva->municipio()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este registro tiene otros registros asociados.'], 404);
        }

        $iva->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el IVA correctamente.'], 200);
    }

    public function dtIndex()
    {
        $iva = Iva::all();

        return Datatables::of($iva)->make(true);
    }


}
