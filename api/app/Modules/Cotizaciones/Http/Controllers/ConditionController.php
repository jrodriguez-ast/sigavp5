<?php namespace App\Modules\Cotizaciones\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Validator;

use Response;

use App\Models\Condicion;
use App\Models\DetalleCondicion;
use App\Models\Acl\User;
use Datatables;

class ConditionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function dtIndex()
    {
        $condicions = Condicion::
        join('tipos_publicaciones', 'condiciones.tipo_publicacion_id', '=', 'tipos_publicaciones.id')
            ->select('condiciones.id', 'condiciones.nombre', DB::raw('tipos_publicaciones.nombre as nombre_tipo, tipos_publicaciones.id as id_tipo'))
            ->get();
        return Datatables::of($condicions)->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     * @throws \Exception
     */
    public function store(Request $request)
    {
        $validator = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            [
                'nombre' => 'required', //'required|integer',
                'detalles' => 'required|array',
                'tipo_publicacion_id' => 'required|integer', //'required| alpha',
                'username' => 'required'
            ]
        );

        if ($validator->fails())
            return response()->json(array('errors' => array(['code' => 422, 'message' => $validator->errors()])), 422);


        $user = User::where('username', $request->header('username'))->first(['id']);
        $condicion = array(
            'nombre' => $request->input('nombre'),
            'tipo_publicacion_id' => $request->input('tipo_publicacion_id'),
            'usuario_id' => $user->id
        );

        DB::beginTransaction(); //Start transaction!
        try {
            $user = new User();
            $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
            $row = array_merge($condicion, $row);
            $condicion = Condicion::create($row);
            foreach ($request->input('detalles') as &$detalle) {

                $detalleCondicion = new DetalleCondicion;
                $detalleCondicion->nombre = $detalle['nombre'];
                $detalleCondicion->usuario_id = $user->id;

                $condicion->detalles()->save($detalleCondicion);
            }

            // Más información sobre respuestas en http://jsonapi.org/format/
            // Devolvemos el código HTTP 201 Created – [Creada] Respuesta a un POST
            // que resulta en una creación. Debería ser combinado con un encabezado
            // Location, apuntando a la ubicación del nuevo recurso.
            $response = Response::make(json_encode(array('status' => 'ok', 'data' => $condicion)), 201)
                ->header('Location', 'http://sigavp/api/public/cotizaciones/conditions/' . $condicion->id)
                ->header('Content-Type', 'application/json');
        } catch (\Exception $e) {
            DB::rollback();
            $reponse = response()->json(array('errors' => array(['code' => 422, 'message' => 'Problemas al procesar los datos. Por favor intente de nuevo.'])), 422);
            throw $e;
        }
        DB::commit();

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        // return "Se muestra Fabricante con id: $id";
        // Buscamos un fabricante por el id.
        $condicion = Condicion::find($id);
        $condicion->TipoPublicacion = $condicion->tipoPublicacion()->first();
        $condicion->Detalles = $condicion->detalles()->get();

        // Si no existe ese fabricante devolvemos un error.
        if (!$condicion) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra la Tarifa con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $condicion], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     * @throws \Exception
     */
    public function update(Request $request, $id)
    {

        $condicion = Condicion::find($id);
        if (!$condicion)
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra la Tarifa con ese código.'])], 404);

        $validator = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            [
                'nombre' => 'required', //'required|integer',
                'detalles' => 'required|array',
                'tipo_publicacion_id' => 'required|integer', //'required| alpha',
                'username' => 'required'
            ]
        );

        if ($validator->fails())
            return response()->json(array('errors' => array(['code' => 422, 'message' => $validator->errors()])), 422);


        $user = User::where('username', $request->header('username'))->first(['id']);

        $condicion->nombre = $request->input('nombre');
        $condicion->tipo_publicacion_id = $request->input('tipo_publicacion_id');
        $condicion->usuario_id = $user->id;

        DB::beginTransaction(); //Start transaction!
        try {
            $condicion->save();
            foreach ($request->input('detalles') as &$detalle) {

                // TimeStamp en JavaScript la fecha supera siempre los 12 caracasteres.
                // Por tanto es una nueva condición
                // Caso contrario habalmos de una actualización.
                $detalleCondicion = (strlen($detalle['id']) >= 12)
                    ? $detalleCondicion = new DetalleCondicion
                    : DetalleCondicion::find($detalle['id']);

                $detalleCondicion->nombre = $detalle['nombre'];
                $detalleCondicion->usuario_id = $user->id;

                $condicion->detalles()->save($detalleCondicion);
            }

            $response = response()->json(['status' => 'ok', 'data' => $condicion], 200, array('Location' => 'http://sigavp/api/public/cotizaciones/conditions/' . $condicion->id));
        } catch (\Exception $e) {
            DB::rollback();
            $reponse = response()->json(array('errors' => array(['code' => 422, 'message' => 'Problemas al procesar los datos. Por favor intente de nuevo.'])), 422);
            throw $e;
        }
        DB::commit();

        return $response;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    public function listado($servicioId, $tipoPublicacionId)
    {
        $condiciones = Condicion::where('servicio_id', $servicioId)->get();
        if ($condiciones):
            foreach ($condiciones as $key => $value) {
                if ($value['es_base']):
                    $condiciones[$key]['label'] = 'Base';
                else:
                    $condiciones[$key]['label'] = 'Recargo';
                endif;
            }
        endif;
        return response()->json($condiciones, 200);
    }

    public function listado_detalle_condicion($servicioId, $condicionId)
    {
        $detalleCondiciones = DetalleCondicion::where('condicion_id', $condicionId)->get();

        return response()->json($detalleCondiciones, 200);
    }

}
