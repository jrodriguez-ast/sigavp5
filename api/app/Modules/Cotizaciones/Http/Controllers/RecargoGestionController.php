<?php namespace App\Modules\Cotizaciones\Http\Controllers;

use App\Http\Controllers\Controller;

use App\Models\RecargoGestion;
use App\Models\Acl\User;
use Response;
use Validator;
use Datatables;
use Illuminate\Http\Request;

class RecargoGestionController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */

    public function index()
    {
        $model = RecargoGestion::where('activo', true)->firstOrFail();
        return response()->json(['status' => 'ok', 'data' => $model], 200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param Request $request
     * @return Response
     */

    public function store(Request $request)
    {
        // Bloque Validacion
        $validator = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            [
                'porcentaje' => 'required|numeric',
                'username' => 'required',
//                'fecha',
//                'activo',
            ]
        );

        if ($validator->fails())
            return response()->json(array('errors' => array(['code' => 422, 'message' => $validator->errors()])), 422);
        //Fin Bloque Validacion

        //Bloque de actualizacion de activo a falso
        RecargoGestion::where('activo', true)->update(array('activo' => false));

        //Fin del bloque de actualizacion

        // Bloque de creacion
        $user = User::where('username', $request->header('username'))->first(['id']);
        $nuevoRecargo = array(
            'porcentaje' => $request->input('porcentaje'),
            'usuario_id' => $user->id,
            'activo' => true,
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($nuevoRecargo, $row);
        $nuevoRecargo = RecargoGestion::create($row);
        //Fin bloque de creacion

        // Bloque de respuestas http
        $response = Response::make(json_encode(array('status' => 'ok', 'data' => $nuevoRecargo)), 201)
            ->header('Location', 'http://sigavp/api/public/cotizaciones/recargo_gestion/' . $nuevoRecargo->id)
            ->header('Content-Type', 'application/json');

        return $response;
        //Fin bloque respuestas http
    }

    /**
     * Display the specified resource.
     * @return Response
     */
    public function show()
    {
        //todo: esto debe ir en index
        $model = RecargoGestion::where('activo', true)->firstOrFail();

        return response()->json(['status' => 'ok', 'data' => $model], 200);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $recargo = RecargoGestion::find($id);
        if (!$recargo) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un recargo con ese código.'])], 404);
        }

        $recargo->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el recargo correctamente.'], 200);
    }

    public function dtIndex()
    {
        $recargo = RecargoGestion::all();

        return Datatables::of($recargo)->make(true);
    }


}
