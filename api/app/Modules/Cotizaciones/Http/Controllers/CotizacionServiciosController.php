<?php namespace App\Modules\Cotizaciones\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Cotizacion;
use App\Models\CotizacionServicio;
use App\Models\CotizacionServicioCondicion;
use App\Models\Negociaciones;
use App\Models\NegociacionesCondiciones;
use App\Models\NegociacionesCondicionesDetalles;
use App\Models\NegociacionesServicios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use yajra\Datatables\Datatables;

class CotizacionServiciosController extends Controller
{
    /**
     * Constructor
     */
    public function __construct()
    {
        //Provee el identificador del usuario que esta logueado  en el sistema
        $this->middleware('data.user');
    }

    /**
     * Provee las versiones asociadas a la cotización con sus tarifas asociadas
     *
     * @access public
     * @param Integer $id Identificador de la version asociada a la cotización
     * @return mixed
     * @internal param int $request Dependencia para recibir variables
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 31/08/15 6:11 AM
     */
    public function index($id)
    {
        $cotizacionesServicios = CotizacionServicio
            ::leftJoin('public.servicios', 'public.cotizaciones_servicios.servicio_id', '=', 'public.servicios.id')
            ->leftJoin('public.medios', 'public.servicios.medio_id', '=', 'public.medios.id')
            ->leftJoin('public.tipos_publicaciones', 'public.servicios.tipo_publicacion_id', '=', 'public.tipos_publicaciones.id')
            ->where(array('public.cotizaciones_servicios.id' => $id))
            ->select(array(
                DB::raw("
                    ARRAY(
                        SELECT
	                        condiciones.nombre
                        FROM
	                        cotizaciones_servicios_condiciones
	                        INNER JOIN condiciones_detalles ON condiciones_detalles.id = cotizaciones_servicios_condiciones.detalle_condicion_id
	                        INNER JOIN condiciones ON condiciones.id = condiciones_detalles.condicion_id
	                    WHERE
	                        cotizaciones_servicios_condiciones.cotizaciones_servicios_id = public.cotizaciones_servicios.id
                        GROUP BY
                            condiciones.nombre ) as condiciones"),
                'public.medios.id as medio_id',
                'public.medios.nombre_comercial as nombre_comercial',
                'public.tipos_publicaciones.nombre as tipo_publicacion',
                'public.tipos_publicaciones.id as tipo_publicacion_id',
                'cotizacion_id',
                'servicio_id',
                'cotizaciones_servicios.id as cotizaciones_servicios_id',
                'public.servicios.nombre as servicio',
                'version_nombre',
                'version_file',
                'version_publicacion_fecha',
                'version_publicacion_fecha_inicio',
                'version_publicacion_fecha_fin',
                'version_segundos',
                'version_apariciones',
                'public.medios.razon_social as razon_social'
            ))->get();

        if ($cotizacionesServicios):
            //Verifico si la fecha es igual para establecer fecha unica en el frontEnd
            foreach ($cotizacionesServicios as $key => &$cotiVersion) {
                if (!empty($cotiVersion['version_publicacion_fecha_inicio']) && !empty($cotiVersion['version_publicacion_fecha_fin'])):
                    $datetime1 = date_create($cotiVersion['version_publicacion_fecha_inicio']);
                    $datetime2 = date_create($cotiVersion['version_publicacion_fecha_fin']);
                    $interval = date_diff($datetime1, $datetime2);
                    if ($interval->days == 0)
                        $cotiVersion['fecha_unica'] = true;
                endif;
            }
        endif;
        $cotizacionesServiciosCondiciones = CotizacionServicio::leftJoin('public.cotizaciones_servicios_condiciones', 'public.cotizaciones_servicios_condiciones.cotizaciones_servicios_id', '=', 'public.cotizaciones_servicios.id')
            ->leftJoin('public.condiciones_detalles', 'public.condiciones_detalles.id', '=', 'public.cotizaciones_servicios_condiciones.detalle_condicion_id')
            ->leftJoin('public.condiciones', 'public.condiciones.id', '=', 'public.condiciones_detalles.condicion_id')
            ->where(array('public.cotizaciones_servicios.id' => $id))
            ->select(array(
                'public.condiciones.id as condicion_id',
                'public.condiciones.nombre as condicion_nombre',
                'public.condiciones.es_porcentaje',
                'public.condiciones_detalles.id as condiciones_detalles_id',
                'public.condiciones_detalles.nombre as condiciones_detalles_nombre',
                'public.condiciones_detalles.tarifa',
                'public.condiciones.servicio_id as servicio_id',
                'public.condiciones.es_base'
            ))->get();


        if ($cotizacionesServiciosCondiciones):
            foreach ($cotizacionesServiciosCondiciones as $key => $value) {
                if ($value['es_base']):
                    $cotizacionesServiciosCondiciones[$key]['label'] = 'Base';
                else:
                    $cotizacionesServiciosCondiciones[$key]['label'] = 'Recargo';
                endif;
            }
        endif;
        return response()->json(array("cotizaciones_servicios" => $cotizacionesServicios, "cotizaciones_servicios_condiciones" => $cotizacionesServiciosCondiciones), 200);

    }

    /**
     * Creación de la(s) version(es) asociada(s) a una cotización
     *
     * @access public
     * @param Request $request Dependencia para recibir variables
     * @param Integer $cotizacion_id Identificador de la cotización asociadas
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 31/08/15 7:25 AM
     */
    public function store(Request $request, $cotizacion_id)
    {
        //Id del usuario que esta haciendo la petición
        $currentUserId = session()->get('userId');

        //Numero de fallas al validar datos
        $fails = 0;
        $data = array();

        //Recorro versiones para evaluar inserción masiva
        if (count($request->versiones) > 0):
            foreach ($request->versiones as $key => $value):

                //Conformación de datos
                $data[$key] = array(
                    'cotizacion_id' => (int)$cotizacion_id,
                    'servicio_id' => $request->servicio['id'],
                    'tipo_publicacion_name' => $request->tipoPublicacion['nombre'],
                    'tipo_publicacion_id' => $request->tipoPublicacion['id'],
                    'version_nombre' => (!empty($value['nombre']) ? $value['nombre'] : ''),
                    'version_segundos' => (!empty($value['segundos']) ? $value['segundos'] : 0),
                    'version_apariciones' => (!empty($value['apariciones']) ? $value['apariciones'] : 1),
                    'version_file' => (!empty($value['file_name']) ? $value['file_name'] : ''),
                    'usuario_id' => $currentUserId,
                    'medio_id' => $request->medio['id'],

                );
                $fecha_inicio = $fecha_fin = '';
                if (!empty($value['fecha_unica']) && $value['fecha_unica']) {
                    $fecha_fin = $fecha_inicio = (!empty($value['fecha_inicio']) ? $value['fecha_inicio'] : '');

                } else {
                    $fecha_inicio = (!empty($value['fecha_inicio']) ? $value['fecha_inicio'] : '');
                    $fecha_fin = (!empty($value['fecha_fin']) ? $value['fecha_fin'] : '');
                }
                $data[$key]['version_publicacion_fecha_inicio'] = $fecha_inicio;
                $data[$key]['version_publicacion_fecha_fin'] = $fecha_fin;

                if ($negociaciones = $this->get_negociaciones(
                    $request->medio['id'],
                    $request->servicio['id']

                )
                ):
                    $data[$key] = $this->check_negociacion(null, $negociaciones, $data[$key]);
                endif;
                $validator = [
                    'cotizacion_id' => 'required',
                    'tipo_publicacion_id' => 'required',
                    'servicio_id' => 'required',
                    'version_nombre' => 'required',
                    'usuario_id' => 'required'
                ];

                if ($request->tipoPublicacion['nombre'] == 'Televisivo' || $request->tipoPublicacion['nombre'] == 'Radial') {
                    $validator['version_apariciones'] = 'required';
                    $validator['version_segundos'] = 'required';
                }
                //Datos a evaluar
                $v = Validator::make($data[$key], $validator);
                //Evaluo datos
                if ($v->fails()) {
                    $fails++;
                }
            endforeach;
        endif;

        //dd($request->all());

        if ($fails == 0 && (count($data) > 0)):
            foreach ($data as $key => $value):
                try {

                    //Se inserta en la entidad CotizaciónServicio
                    $CotizacionServicio = CotizacionServicio::create($value);
                    $CotizacionServicioCondicion = new CotizacionServicioCondicion();

                    //Si la inserción es exitosa de agregan los detalles de la condición
                    if ($CotizacionServicio):

                        foreach ($request->add_condiciones as $key => $value2):
                            $data_cotizacion_servicio_condicion = array(
                                'cotizaciones_servicios_id' => $CotizacionServicio->id,
                                'detalle_condicion_id' => $value2['detalle']['id'],
                                'es_porcentaje' => $value2['condicion']['es_porcentaje'],
                                'tarifa' => $value2['detalle']['tarifa'],
                                'usuario_id' => $currentUserId,
                                'condicion_id' => $value2['condicion']['id'],
                                'es_base' => $value2['condicion']['es_base'],
                                'data_extra' => (!empty($value2['detalle']['data_extra']) ? json_encode($value2['detalle']['data_extra']) : null)
                            );

                            if (
                            $negociaciones = $this->get_negociaciones(
                                $request->medio['id'],
                                $request->servicio['id'],
                                $value2['condicion']['id'],
                                $value2['detalle']['id']
                            )
                            ):

                                $data_cotizacion_servicio_condicion = $this->check_negociacion($value, $negociaciones, $data_cotizacion_servicio_condicion);
                            endif;
                            //Se inserta en la entidad CotizaciónServicioCondición
                            $CotizacionServicioCondicion->create($data_cotizacion_servicio_condicion);

                        endforeach;


                        $CotizacionServicio_update = CotizacionServicio::find($CotizacionServicio->id);
                        $monto_version = $CotizacionServicioCondicion->calcularValorConRecargos($CotizacionServicio->id, true);
                        $CotizacionServicio_update->monto_servicio = (empty($monto_version)) ? 0 : $monto_version['con_tiempo'];

                        // Se incluye el tiempo xq lo que se desea es calcular el precio por dia.
                        $CotizacionServicio_update->valor_con_recargo = (empty($monto_version)) ? 0 : $monto_version['sin_tiempo'];

                        $CotizacionServicio_update->save();
                        Cotizacion::calculate_quote($cotizacion_id);

                    endif;

                } catch (Exception $e) {
                    return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Error al procesar los datos!']))], 422);
                }
            endforeach;
            return response()->json(array('status' => 'ok', 'data' => $CotizacionServicioCondicion, 'message' => "Medio - Servicio - Condiciones, agregados exitosamente!"), 200);
        else:
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Error al procesar los datos!']))], 422);
        endif;
    }


    /**
     * Actualización de la(s) version(es) asociada(s) a una cotización
     *
     * @access public
     * @param Request $request Dependencia para recibir variables
     * @param Integer $cotizacion_id Identificador de la cotización asociadas
     * @param Integer $cotizacion_servicio_id Identificador de la version a actualizar
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 31/08/15 6:25 AM
     */
    public function update(Request $request, $cotizacion_id, $cotizacion_servicio_id)
    {
        //Id del usuario que esta haciendo la petición
        $currentUserId = session()->get('userId');
        $CotizacionServicio = CotizacionServicio::find($cotizacion_servicio_id);
        //Numero de fallas al validar datos
        $fails = 0;
        $data = array();

        //Recorro versiones para evaluar inserción masiva
        if (count($request->versiones) > 0):
            foreach ($request->versiones as $key => $value):
                //Conformación de datos
                $data[$key] = array(
                    'cotizacion_id' => (int)$cotizacion_id,
                    'servicio_id' => $request->servicio['id'],
                    'tipo_publicacion_name' => $request->tipoPublicacion['nombre'],
                    'tipo_publicacion_id' => $request->tipoPublicacion['id'],
                    'version_nombre' => $value['nombre'],
                    'version_file' => (!empty($value['file_name']) ? $value['file_name'] : ''),
                    'version_publicacion_fecha' => (!empty($value['fecha']) ? $value['fecha'] : ''),
                    'version_segundos' => (!empty($value['segundos']) ? $value['segundos'] : 0),
                    'version_apariciones' => (!empty($value['apariciones']) ? $value['apariciones'] : 1),
                    'usuario_id' => $currentUserId
                );


                $fecha_inicio = $fecha_fin = '';
                if (!empty($value['fecha_unica']) && $value['fecha_unica']) {
                    $fecha_fin = $fecha_inicio = (!empty($value['fecha_inicio']) ? $value['fecha_inicio'] : '');

                } else {
                    $fecha_inicio = (!empty($value['fecha_inicio']) ? $value['fecha_inicio'] : '');
                    $fecha_fin = (!empty($value['fecha_fin']) ? $value['fecha_fin'] : '');
                }
                $data[$key]['version_publicacion_fecha_inicio'] = $fecha_inicio;
                $data[$key]['version_publicacion_fecha_fin'] = $fecha_fin;
                if ($negociaciones = $this->get_negociaciones(
                    $request->medio['id'],
                    $request->servicio['id']

                )
                ):
                    $data[$key] = $this->check_negociacion(null, $negociaciones, $data[$key]);
                endif;

                $validator = [
                    'cotizacion_id' => 'required',
                    'tipo_publicacion_id' => 'required',
                    'servicio_id' => 'required',
                    'version_nombre' => 'required',
                    'usuario_id' => 'required'
                ];
                if ($request->tipoPublicacion['nombre'] == 'Televisivo' || $request->tipoPublicacion['nombre'] == 'Radial') {
                    $validator['version_apariciones'] = 'required';
                    $validator['version_segundos'] = 'required';
                }
                //Datos a evaluar
                $v = Validator::make($data[$key], $validator);

                //Evaluo datos
                if ($v->fails()) {
                    $fails++;
                }
            endforeach;
        endif;

        if ($fails == 0 && (count($data) > 0)):
            $CotizacionServicioCondicion = CotizacionServicioCondicion::where('cotizaciones_servicios_id', '=', $cotizacion_servicio_id)->delete();
            foreach ($data as $key => $value):
                try {

                    $CotizacionServicioCondicion = new CotizacionServicioCondicion();
                    $CotizacionServicio->cotizacion_id = $value['cotizacion_id'];
                    $CotizacionServicio->servicio_id = $value['servicio_id'];
                    $CotizacionServicio->version_nombre = $value['version_nombre'];
                    $CotizacionServicio->version_file = $value['version_file'];
                    $CotizacionServicio->version_publicacion_fecha = $value['version_publicacion_fecha'];
                    $CotizacionServicio->version_publicacion_fecha_inicio = $value['version_publicacion_fecha_inicio'];
                    $CotizacionServicio->version_publicacion_fecha_fin = $value['version_publicacion_fecha_fin'];
                    $CotizacionServicio->tipo_publicacion_name = $value['tipo_publicacion_name'];
                    $CotizacionServicio->tipo_publicacion_id = $value['tipo_publicacion_id'];
                    $CotizacionServicio->version_segundos = $value['version_segundos'];
                    $CotizacionServicio->version_apariciones = $value['version_apariciones'];


                    //Se inserta en la entidad CotizacionServicio
                    //Si la insercion es exitosa de agregan los detalles de la condicion
                    if ($CotizacionServicio->save()):
                        foreach ($request->add_condiciones as $key => $value):
                            $data_cotizacion_servicio_condicion = array(
                                'cotizaciones_servicios_id' => $CotizacionServicio->id,
                                'detalle_condicion_id' => $value['detalle']['id'],
                                'es_porcentaje' => $value['condicion']['es_porcentaje'],
                                'tarifa' => $value['detalle']['tarifa'],
                                'usuario_id' => $currentUserId,
                                'condicion_id' => $value['condicion']['id'],
                                'es_base' => $value['condicion']['es_base'],
                                'data_extra' => (!empty($value['detalle']['data_extra']) ? json_encode($value['detalle']['data_extra']) : null)
                            );

                            if ($negociaciones = $this->get_negociaciones(
                                $request->medio['id'],
                                $request->servicio['id'],
                                $value['condicion']['id'],
                                $value['detalle']['id']
                            )
                            ):

                                $data_cotizacion_servicio_condicion = $this->check_negociacion($value, $negociaciones, $data_cotizacion_servicio_condicion);
                            endif;
                            //Se inserta en la entidad CotizacionServicioCondicion
                            $CotizacionServicioCondicion->create($data_cotizacion_servicio_condicion);
                        endforeach;
                        $monto_version = $CotizacionServicioCondicion->calcularValorConRecargos($cotizacion_servicio_id, true);
                        $CotizacionServicio->valor_con_recargo = $monto_version['sin_tiempo'];
                        $CotizacionServicio->monto_servicio = $monto_version['con_tiempo'];
                        $CotizacionServicio->save();
                        Cotizacion::calculate_quote($cotizacion_id);
                    endif;

                } catch (Exception $e) {
                    return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Error al1 procesar los datos!']))], 422);
                }
            endforeach;
            return response()->json(array('status' => 'ok', 'data' => $CotizacionServicioCondicion, 'message' => "Medio - Servicio - Condiciones, actualizados exitosamente!"), 200);
        else:
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Error al2 procesar los datos!']))], 422);
        endif;
    }

    /**
     * Método que se ejecuta para enviar los datos al datatable
     *
     * @access public
     * @param Request $request Dependencia para recibir variables
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 31/08/15 6:25 AM
     */
    public function dtIndex(Request $request)
    {
        $cotizacion_id = $request->cotizacionId;
        $cotizacionesServicios = CotizacionServicio::leftJoin('public.servicios', 'public.cotizaciones_servicios.servicio_id', '=', 'public.servicios.id')
            ->leftJoin('public.medios', 'public.servicios.medio_id', '=', 'public.medios.id')->leftJoin('public.tipos_publicaciones', 'public.servicios.tipo_publicacion_id', '=', 'public.tipos_publicaciones.id')
            ->where(array('cotizacion_id' => $cotizacion_id))
            ->with([

                'cotizacion_servicio_condicion'

            ])
            ->select(array(
                'public.medios.id',
                'public.medios.nombre_comercial as nombre_comercial',
                'public.tipos_publicaciones.nombre as tipo_publicacion',
                'cotizacion_id',
                'servicio_id',
                'cotizaciones_servicios.id as cotizaciones_servicios_id',
                'public.servicios.nombre as servicio',
                'version_nombre',
                'version_file',
                'version_publicacion_fecha',
                'version_publicacion_fecha_inicio',
                'version_publicacion_fecha_fin',
                'version_segundos',
                'version_apariciones',
                'public.medios.razon_social as razon_social'
            ));

        return Datatables::of($cotizacionesServicios)
            ->editColumn('condiciones', function ($data) {
                $replace = array("{", "}");
                $data_format = str_replace($replace, "", $data->condiciones);
                return (!empty($data_format) ? $data_format : 'Sin Información');
            })
            ->editColumn('version_publicacion_fecha', function ($data) {
                $date = date_create($data->version_publicacion_fecha);
                return date_format($date, 'd/m/Y');
            })
            ->editColumn('version_publicacion_fecha_inicio', function ($data) {
                $date = date_create($data->version_publicacion_fecha_fin);
                return date_format($date, 'd/m/Y');
            })
            ->editColumn('version_publicacion_fecha_fin', function ($data) {
                $date = date_create($data->version_publicacion_fecha_fin);
                return date_format($date, 'd/m/Y');
            })
            ->make(true);

    }

    /**
     * Provee las negociaciones asociadas a un medio
     *
     * @access public
     * @param null $medio_id
     * @param null $servicio_id
     * @param null $condicion_id
     * @param null $condicion_detalle_id
     * @return mixed
     * @internal param Request $request Dependencia para recibir variables
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 31/08/15 6:00 AM
     */
    public function get_negociaciones($medio_id = null, $servicio_id = null, $condicion_id = null, $condicion_detalle_id = null)
    {

        $response = false;
        if (empty($medio_id))
            return false;
        //Se verifica si existe negociacion
        $negociaciones = Negociaciones::where(array('medio_id' => $medio_id))->get();

        if (!$negociaciones->isEmpty()):
            $response = $negociaciones[0];
            if (!empty($servicio_id)):
                //Servicios negociados
                $negociaciones_servicios = NegociacionesServicios::where(
                    array(
                        'negociacion_id' => $negociaciones[0]['id'],
                        'servicio_id' => $servicio_id
                    )
                )->get();

                if (!$negociaciones_servicios->isEmpty())://Verifica si tiene servicio
                    $response['servicio'] = $negociaciones_servicios[0];
                    $response['servicio']['negociacion'] = true;

                endif;
            endif;

            if (!empty($condicion_id)):
                //Condiciones negociadas
                $negociaciones_condiciones = NegociacionesCondiciones::where(
                    array(
                        'negociacion_id' => $negociaciones[0]['id'],
                        'condicion_id' => $condicion_id
                    )
                )->get();

                if (!$negociaciones_condiciones->isEmpty()): //Verifica si hay condicion
                    $response['condicion'] = $negociaciones_condiciones[0];
                    $response['condicion']['negociacion'] = true;
                endif;
            endif;

            if (!empty($condicion_detalle_id)):
                $negociaciones_condiciones_detalle = NegociacionesCondicionesDetalles::where(
                    array(
                        'negociacion_id' => $negociaciones[0]['id'],
                        'condicion_detalle_id' => $condicion_detalle_id
                    )
                )->get();

                if (!$negociaciones_condiciones_detalle->isEmpty())://Verifica si hay condición detalles
                    $response['detalle'] = $negociaciones_condiciones_detalle[0];
                    $response['detalle']['negociacion'] = true;
                endif;
            endif;
        endif;

        return $response;
    }

    /**
     * Checkea las condiciones y las tarifas de una negociación con respecto
     * a una cotización y sus condiciones asociadas y aplica los cálculos pertinentes
     *
     * @access public
     * @param null $value Datos asociados a la cotización que se le aplicaran las negociaciones pertinentes
     * @param null $negociaciones Negociaciones asociadas al medio
     * @param null $data_cotizacion_servicio_condicion Datos de la version que se le aplicaran los cálculos
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 31/08/15 6:00 AM
     */
    public function check_negociacion($value = null, $negociaciones = null, $data_cotizacion_servicio_condicion = null)
    {


        $fecha_actual = strtotime(date("d-m-Y H:i:00", time()));
        $aplica_servicio = false;
        $poncentaje_servicio = 0.00;
        //Se evalúa si esta activa la negociación o esta vencida
        if (!empty($negociaciones) && strtotime($negociaciones['fecha_fin']) >= $fecha_actual):


            //Cuando es detalle
            if (!empty($negociaciones['detalle'])):
                if ($negociaciones['detalle']['estado']):
                    if ($negociaciones['detalle']['exonerado']):
                        $data_cotizacion_servicio_condicion['tarifa'] = '0.00';
                    else:
                        if ($value['condicion']['es_base']):
                            if (!$negociaciones['detalle']['es_porcentaje'] && $negociaciones['detalle']['sustituyo']):
                                $data_cotizacion_servicio_condicion['tarifa'] = $negociaciones['detalle']['monto'];
                            endif;
                        else:
                            if ($negociaciones['detalle']['sustituye'] && !$negociaciones['detalle']['es_porcentaje']):
                                $data_cotizacion_servicio_condicion['es_porcentaje'] = false;
                                $data_cotizacion_servicio_condicion['tarifa'] = $negociaciones['detalle']['monto'];
                            else:
                                if ($value['condicion']['es_porcentaje'] && $negociaciones['detalle']['es_porcentaje']):
                                    $data_cotizacion_servicio_condicion['es_porcentaje'] = true;
                                    $data_cotizacion_servicio_condicion['tarifa'] = ($data_cotizacion_servicio_condicion['tarifa'] - $negociaciones['detalle']['monto']);
                                elseif (!$value['condicion']['es_porcentaje'] && $negociaciones['detalle']['es_porcentaje']):
                                    $data_cotizacion_servicio_condicion['es_porcentaje'] = false;
                                    $porcentaje_aplicado = ($value['detalle']['tarifa'] * $negociaciones['detalle']['monto'] / 100);
                                    $data_cotizacion_servicio_condicion['tarifa'] = ($value['detalle']['tarifa'] - $porcentaje_aplicado);
                                endif;
                            endif;
                        endif;
                    endif;
                endif;
            endif;

            //Cuando es condición
            if (!empty($negociaciones['condicion'])):
                if ($negociaciones['condicion']['estado']):
                    if ($negociaciones['condicion']['exonerado']):
                        $data_cotizacion_servicio_condicion['tarifa'] = '0.00';
                    endif;
                endif;

            endif;
        endif;

        //Verifico si existe servicio activo para aplicar
        if (!empty($negociaciones['servicio'])):
            if ($negociaciones['servicio']['estado']):
                if ($negociaciones['servicio']['exonerado']):
                    $data_cotizacion_servicio_condicion['tarifa'] = '0.00';
                else:
                    if ($negociaciones['servicio']['es_porcentaje']):
                        $data_cotizacion_servicio_condicion['monto_servicio'] = $negociaciones['servicio']['monto'];
                    endif;
                endif;
            endif;
        endif;

        return $data_cotizacion_servicio_condicion;

    }

    /**
     * Permite eliminar versiones de la cotización de forma permanente
     *
     * @access public
     * @param $cotizacion_id Identificador de la Cotización
     * @param $cotizacion_servicio_id Identificador de la version de la cotización
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 15/09/15 11:34 AM
     */
    public function destroy($cotizacion_id, $cotizacion_servicio_id)
    {
        $CotizacionServicio = CotizacionServicio::find($cotizacion_servicio_id);

        if ($CotizacionServicio) {
            $CotizacionServicio->cotizacion_servicio_condicion()->delete();
            $CotizacionServicio->delete();
            $CalculoCotizacion = Cotizacion::calculate_quote($cotizacion_id);
            $responseCotizacion = Cotizacion::data_for_quote($cotizacion_id);
            return response()->json($responseCotizacion, 200);
        }
    }

}