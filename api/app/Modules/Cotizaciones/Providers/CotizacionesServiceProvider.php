<?php
namespace App\Modules\Cotizaciones\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class CotizacionesServiceProvider extends ServiceProvider
{
    /**
     * Register the Cotizaciones module service provider.
     *
     * @return void
     */
    public function register()
    {
        // This service provider is a convenient place to register your modules
        // services in the IoC container. If you wish, you may make additional
        // methods or service providers to keep the code more focused and granular.
        App::register('App\Modules\Cotizaciones\Providers\RouteServiceProvider');

        $this->registerNamespaces();
    }

    /**
     * Register the Cotizaciones module resource namespaces.
     *
     * @return void
     */
    protected function registerNamespaces()
    {
        Lang::addNamespace('cotizaciones', realpath(__DIR__ . '/../Resources/Lang'));

        View::addNamespace('cotizaciones', realpath(__DIR__ . '/../Resources/Views'));
    }
}
