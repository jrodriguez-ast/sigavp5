<?php
namespace App\Modules\Reportes\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class ReportesServiceProvider extends ServiceProvider
{
	/**
	 * Register the Reportes module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Reportes\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Reportes module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('reportes', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('reportes', realpath(__DIR__.'/../Resources/Views'));
	}
}
