<?php namespace App\Modules\Reportes\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Reporte;
use Illuminate\Http\Request;
use Response;


class ReporteController extends Controller
{
    /**
     * @return Response
     */
    public function index()
    {

        $model = Reporte::all();

        return response()->json(['status' => 'ok', 'data' => $model], 200);
    }

    /**
     * Función que llama al método reporteClientesDetallado del modelo y muestra los datos
     * detallados del cliente
     *
     * @param $id
     * @return Response
     */
    public function reporteclientedetallado($id)
    {
        $reporte = new Reporte();
        $datos = $reporte->reporteClientesDetallado($id);

        return response()->json(['status' => 'ok', 'data' => $datos], 200);
    }

    /**
     * Función que llama al método reporteClienteGeneral del modelo y muestra los datos
     * generales de todos cliente
     *
     * @param Request $request
     * @return Response
     */
    public function reporteclientegeneral(Request $request)
    {
        $reporte = new Reporte('Clientes con o sin publicaciones', 'clientes');
        $reporte->build($reporte->reporteClienteGeneral($request->all()));
    }

    /**
     * Función que llama al método reporteMediosDetallado del modelo y muestra los datos
     * detallados del medio
     *
     * @param $id
     * @return Response
     */
    public function reportemediodetallado($id)
    {
        $reporte = new Reporte();
        $datos = $reporte->reporteMediosDetallado($id);

        return response()->json(['status' => 'ok', 'data' => $datos], 200);
    }

    /**
     * Función que llama al método reporteMedioGeneral del modelo y muestra los datos
     * de todos los medios
     *
     * @return Response
     */
    public function reportemediogeneral()
    {
        $reporte = new Reporte();
        $datos = $reporte->reporteMedioGeneral();

        return response()->json(['status' => 'ok', 'data' => $datos], 200);
    }

    /**
     * Función que llama al método reporteClientesfiltros del modelo y muestra los datos
     * de todos clientes con tipo de publicación, si recibe un id muestra solo los de ese cliente
     *
     * @param Request $request
     * @param null $cliente_id
     * @return Response
     */
    public function reporteTodoslosClientes(Request $request, $cliente_id = null)
    {
        $desde = $request->input('dateStart');
        $hasta = $request->input('dateEnd');

        $reporte = new Reporte('Reporte de publicaciones', 'publicaciones');
        $reporte->build($reporte->reporteClientesfiltros($cliente_id, $desde, $hasta));

    }

    /**
     * Muestra el reporte de cotizaciones vs pautas
     *
     * @param Request $request
     * @return mixed
     */
    public function getNumeroCotizaciones(Request $request)
    {
        $reporte = new Reporte('Reporte de cotizaciones', 'Cotizaciones');
        $reporte->build($reporte->getNumeroCotizaciones($request->all()));
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param \Illuminate\Http\Request $request
     */
    public function processFilter(Request $request)
    {
        switch ($request->input('tipoReporte')):
            case 1:
                $this->reporteTotalFacturado($request, $request->input('cliente_id'));
                break;
            case 2:
                $this->reporteTodoslosClientes($request, $request->input('cliente_id'));
                break;
            case 3:
                $this->getNumeroCotizaciones($request);
                break;
            case 4:
                $this->facturasPagadas($request, $request->input('cliente_id'));
                break;
            case 5:
                $this->montoCobrado($request, $request->input('cliente_id'));
                break;
            case 6:
                $this->incidencias($request, $request->input('cliente_id'));
                break;
            case 7:
                $this->cotizacionesAprobadas($request, $request->input('cliente_id'));
                break;
            case 8:
                $this->pautaIncidencia($request, $request->input('cliente_id'), $request->input('tipo_publicacion_id'));
                break;
            case 9:
                $this->contratadoPagado($request, $request->input('cliente_id'), $request->input('contrato_id'));
                break;
            case 10:
                $this->facturasAnuladasCuota($request, $request->input('cliente_id'));
                break;
            case 11:
                $this->facturasAnuladasPublicacion($request, $request->input('cliente_id'));
                break;
            case 12:
                $this->facturasCuotaConNotaCredito($request, $request->input('cliente_id'));
                break;
            case 13:
                $this->facturasPublicacionConNotaCredito($request, $request->input('cliente_id'));
                break;

            default:
                //Default action
        endswitch;

    }

    /**
     * @param Request $request
     * @param null $cliente_id
     * @return mixed
     */
    public function reporteTotalFacturado(Request $request, $cliente_id = null)
    {
        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $reporte = new Reporte('Reporte Facturas', 'Facturas');
        $datos = $reporte->facturasTotales($cliente_id, $desde, $hasta);
        $total = 0.00;
        foreach ($datos as $key => $value):
            $total = $total + $value['total_factura'];
        endforeach;
        $reporte->build($datos, array('', '', '', '', '', 'Total', $total));
        return response()->json($datos, 200);
    }

    /**
     * @param Request $request
     * @param null $cliente_id
     */
    public function facturasPagadas(Request $request, $cliente_id = null)
    {
        $tipo_publicacion = $request->input('tipo_publicacion_id');
        $reporte = new Reporte('Reporte de facturas pagadas', 'facturas');
        $reporte->build($reporte->facturasPagadas($cliente_id, $tipo_publicacion));
    }

    /**
     * @param Request $request
     * @param null $cliente_id
     */
    public function montoCobrado(Request $request, $cliente_id = null)
    {
        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $reporte = new Reporte('Reporte de pagos', 'clientes_pagos');
        $datos = $reporte->montoCobrado($cliente_id, $desde, $hasta);
        $total = 0;
        foreach ($datos as $key => $value) {
            $total = $total + $value['monto'];
        }
        $reporte->build($datos, array('', '', '', '', 'Total Cobrado', $total));
    }

    /**
     * @param Request $request
     * @param null $cliente_id
     */
    public function incidencias(Request $request, $cliente_id = null)
    {
        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $reporte = new Reporte('Reporte incidencias', 'Incidencias');
        $reporte->build($reporte->incidencias($cliente_id, $desde, $hasta));
    }

    /**
     * @param Request $request
     * @param null $cliente_id
     */
    public function cotizacionesAprobadas(Request $request, $cliente_id = null)
    {
        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $reporte = new Reporte('Reporte de cotizaciones aprobadas', 'cotizacionesA');
        $reporte->build($reporte->cotizacionesAprobadas($cliente_id, $desde, $hasta));
    }

    /**
     * @param Request $request
     * @param null $cliente_id
     * @param null $tipo_publicacion
     */
    public function pautaIncidencia(Request $request, $cliente_id = null, $tipo_publicacion = null)
    {
        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $tipo_publicacion = $request->input('tipo_publicacion_id');
        $medio = $request->input('medio_id');
        $reporte = new Reporte('Reporte de pautas con incidencia', 'pautas');
        $reporte->build($reporte->pautaIncidencia($cliente_id, $medio, $tipo_publicacion, $desde, $hasta));
    }

    /**
     * @param Request $request
     * @param null $cliente_id
     * @param null $contrato_id
     */
    public function contratadoPagado(Request $request, $cliente_id = null, $contrato_id = null)
    {

        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $reporte = new Reporte('Reporte de contrato consumido', 'contratos');
        // dd($contrato_id);
        $datos = $reporte->contratadoPagado($cliente_id, $contrato_id, $desde, $hasta);
        $reporte->build($reporte->contratadoPagado($cliente_id, $contrato_id, $desde, $hasta));

    }

    /**
     * @param Request $request
     * @param null $cliente_id
     */
    public function facturasAnuladasCuota(Request $request, $cliente_id = null)
    {
        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $reporte = new Reporte('Factura Anuladas Cuota', 'facturas');
        $datos = $reporte->facturasAnuladasCuotas($cliente_id, $desde, $hasta);
        $reporte->build($reporte->facturasAnuladasCuotas($cliente_id, $desde, $hasta));

    }

    /**
     * @param Request $request
     * @param null $cliente_id
     */
    public function facturasAnuladasPublicacion(Request $request, $cliente_id = null)
    {
        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $reporte = new Reporte('Factura Anuladas Publicacion', 'facturas');
        $datos = $reporte->facturasAnuladasPublicacion($cliente_id, $desde, $hasta);
        $reporte->build($reporte->facturasAnuladasPublicacion($cliente_id, $desde, $hasta));

    }

    /**
     * @param Request $request
     * @param null $cliente_id
     */
    public function facturasCuotaConNotaCredito(Request $request, $cliente_id = null)
    {
        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $reporte = new Reporte('Facturas Cuota Nota de Credito', 'facturas');
        $datos = $reporte->facturasCuotaConNotaCredito($cliente_id, $desde, $hasta);
        $reporte->build($reporte->facturasCuotaConNotaCredito($cliente_id, $desde, $hasta));

    }

    /**
     * @param Request $request
     * @param null $cliente_id
     */
    public function facturasPublicacionConNotaCredito(Request $request, $cliente_id = null)
    {
        $desde = ($request->input('dateStart') != 'false') ? $request->input('dateStart') : null;
        $hasta = ($request->input('dateEnd') != 'false') ? $request->input('dateEnd') : null;
        $reporte = new Reporte('Facturas Cuota Nota de Credito', 'facturas');
        $datos = $reporte->facturasPublicacionConNotaCredito($cliente_id, $desde, $hasta);
        $reporte->build($reporte->facturasPublicacionConNotaCredito($cliente_id, $desde, $hasta));

    }

}