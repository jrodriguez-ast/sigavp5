<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'reportes'], function () {

    Route::resource('reportes', 'ReporteController');
    Route::get('reporteCliente', 'ReporteController@reporteclientegeneral');
    Route::get('reporteCliente/{cliente_id}', 'ReporteController@reporteclientedetallado');
    Route::get('reporteCliente2/{cliente_id?}', 'ReporteController@reporteTodoslosClientes');
    Route::get('reporteMedio', 'ReporteController@reportemediogeneral');
    Route::get('reporteMedio/{medio_id}', 'ReporteController@reportemediodetallado');

    Route::get('cotizacionesporclientes', 'ReporteController@getNumeroCotizaciones');

    Route::get('facturas', 'ReporteController@reporteTotalFacturado');
    Route::get('generar', 'ReporteController@processFilter');

});