<?php
namespace App\Modules\Clientes\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class ClientesServiceProvider extends ServiceProvider
{
    /**
     * Register the Clientes module service provider.
     *
     * @return void
     */
    public function register()
    {
        // This service provider is a convenient place to register your modules
        // services in the IoC container. If you wish, you may make additional
        // methods or service providers to keep the code more focused and granular.
        App::register('App\Modules\Clientes\Providers\RouteServiceProvider');

        $this->registerNamespaces();
    }

    /**
     * Register the Clientes module resource namespaces.
     *
     * @return void
     */
    protected function registerNamespaces()
    {
        Lang::addNamespace('clientes', realpath(__DIR__ . '/../Resources/Lang'));

        View::addNamespace('clientes', realpath(__DIR__ . '/../Resources/Views'));
    }
}
