<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Acl\User;
use App\Models\Balance;
use App\Models\Cliente;
use App\Models\Contrato;
use Carbon\Carbon;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class ClienteBalanceController extends Controller
{

    /**
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function index($cliente_id)
    {
        $cliente = Cliente::find($cliente_id);

        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $cliente->balance()->orderBy('created_at', 'desc')->get()], 200);
    }

    /**
     * Retorna el balance con los datos del contrato asociado si existiera
     * @param $balance_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function show($balance_id)
    {
        $balance = Balance::with('contrato')->find($balance_id);
        if (!$balance) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una operacion con ese código.']))], 404);
        };
        //Al llamar al atributo se agrega al objeto de balance
        //$balance->contrato;
        return response()->json(array('status' => 'ok', 'data' => $balance, 200));
    }

    /**
     * @param Request $request
     * @param $balance_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function update(Request $request, $balance_id)
    {
        $balance = Balance::find($balance_id);
        if (!$balance) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']))], 404);
        }
        $observaciones = $request->input('observaciones');
        $bandera = false;
        if ($observaciones) {
            $balance->observaciones = $observaciones;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $balance->usuario_id = $user->findByName($request->header('username'), true)->id;
            $balance->save();
            return response()->json(array('status' => 'ok', 'data' => $balance), 200);
        } else {
            return response()->json(array('status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato de cliente.'])), 304);
        }
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $cliente_id
     * @param int $contrato_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function dtIndex(Request $request, $cliente_id, $contrato_id = 0)
    {
        $cliente = Cliente::find($cliente_id);

        if (!$cliente) {
            return response()->json([
                'status' => 'fail',
                'errors' => [
                    'code' => 404,
                    'message' => 'No se encuentra un cliente con ese código.'
                ]
            ], 404);
        }

        $todos = $request->input('todos');
        $tipo = $request->input('tipomovimiento');
        $desde = $request->input('dateStart');
        $hasta = $request->input('dateEnd');
        //$reporte = $request->input('reporte');

        $query = $cliente->balance()->with(['contrato'])->orderBy('created_at', 'desc');

        if (!empty($contrato_id)) {
            $query->where('contrato_id', $contrato_id);
        }

        if ($todos == 'false') {
            $desde = ($desde == 'false') ? Carbon::createFromTimestamp(0) : $desde;
            $hasta = Carbon::createFromFormat('Y-m-d', $hasta)->addDay();

            $query->whereBetween('created_at', [$desde, $hasta]);
            if ($tipo == 'Cargos') {
                $query->where('cargo', '>', 0);
            } elseif ($tipo == 'Débitos') {
                $query->where('debito', '>', 0);
                //} else {

            }
        }

        //$data = $query->get();
        //$data = new Collection($data);
        return Datatables::of($query)
            ->editColumn('acciones', function ($row) {
                $relationships = [];
                $relation = '<a ui-sref="admin.detalle_cliente.detalle_contrato({id:' . $row->cliente_id . ',contratoID:' . $row->contrato_id . '})"
                        class="btn btn-default btn-xs" title="Ver Contrato"><i class="glyphicon glyphicon-duplicate"></i></a>';
                array_push($relationships, $relation);

                if (!empty($row->publicacion_id)) {
                    $relation = '<a ui-sref="admin.editar_publicacion({publicacion_id:' . $row->publicacion_id . '})"
                        class="btn btn-default btn-xs" title="Ver Publicación"><i class="glyphicon glyphicon-tasks"></i></a>';
                    array_push($relationships, $relation);
                }
                if (!empty($row->factura_id)) {
                    $relation = '<a ui-sref="admin.detalle_cliente.facturas_detail({id:' . $row->cliente_id . ',facturaID:' . $row->factura_id . '})"
                        class="btn btn-default btn-xs" title="Ver Factura"><i class="glyphicon glyphicon-list-alt"></i></a>';
                    array_push ($relationships, $relation);
                }
                if (!empty($row->nota_credito_id)) {
                    $relation = '<a ui-sref="admin.detalle_cliente.notas_detail({id:' . $row->cliente_id . ',notaID:' . $row->nota_credito_id . '})"
                        class="btn btn-default btn-xs" title="Ver Nota de credito"><i class="glyphicon glyphicon-credit-card"></i></a>';
                    array_push ($relationships, $relation);
                }
                return implode ('&nbsp;', $relationships);

            })
            //return Datatables::of($data)
            /*->editColumn('created_at', function ($datos) {
                $date = date_create($datos->created_at);
                return date_format($date, 'd/m/Y');
            })*/
            ->make(true);
    }

    /**
     * @param \Illuminate\Http\Request $request
     * @param int $contrato_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function balancexTipodt(Request $request, $contrato_id)
    {

        $contrato = Contrato::find($contrato_id);

        if (!$contrato) {
            return response()->json([
                'status' => 'fail',
                'errors' => [
                    'code' => 404,
                    'message' => 'No se encuentra un contrato con ese código.'
                ]
            ], 404);
        }

        $todos = $request->input('todos');
        $tipo = $request->input('tipomovimiento');
        $desde = $request->input('dateStart');
        $hasta = $request->input('dateEnd');
        $reporte = $request->input('reporte');

        $query = $contrato->balancePorTipoPublicacion()->with (['tipoPublicacion'])->orderBy('created_at', 'desc');


        if ($todos == 'false') {
            $desde = ($desde == 'false') ? Carbon::createFromTimestamp(0) : $desde;
            $hasta = Carbon::createFromFormat('Y-m-d', $hasta)->addDay();

            $query->whereBetween('created_at', [$desde, $hasta]);
            if ($tipo == 'Cargos') {
                $query->where('cargo', '>', 0);
            } elseif ($tipo == 'Débitos') {
                $query->where('debito', '>', 0);
                //} else {

            }
        }
        $data = $query->get();
        return Datatables::of($data)->make(true);

    }

    /**
     * Controlador que provee acceso a los datos del balance.
     * @param int $contrato_id
     * @return mixed
     */
    public function  saldoActualxTipo( $contrato_id)
    {
        $contrato = Contrato::find($contrato_id);

        if (!$contrato) {
            return response()->json([
                'status' => 'fail',
                'errors' => [
                    'code' => 404,
                    'message' => 'No se encuentra un contrato con ese código.'
                ]
            ], 404);
        }

        $data = $contrato->balancePorTipoPublicacion();


            $data->select('Monto');

            $data->select('monto AS monto');
            $data->where(['contrato_id' => $contrato_id]);

        $data = $data->orderBy('id', 'desc')->first();



        return response()->json(['status' => 'ok', 'data' => $data], 200);
    }


    public function pre_visualizar_balance ($balance = null)
    {
        if (empty($balance))
            return false;

        $iniciales = '';
        if (!empty($cotizacion)):
            $pdf = App::make('dompdf.wrapper');
            if (!empty($cotizacion['user'])):
                $iniciales = $this->get_iniciales_string($cotizacion['user']['first_name'] . " " . $cotizacion['user']['last_name']);
            endif;
            $cotizacion['iniciales'] = $iniciales;
            $cotizacion['novalido'] = true;
            $pdf->loadView($this->PdfCotizacionTemplate, $cotizacion);
            return $pdf->setPaper('a4')
                ->setWarnings(false)
                ->download("DOC_$cotizacion->codigo.pdf");
        else:
            echo "No se pudo obtener el documento";
        endif;
    }

}