<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Acl\User;
use App\Models\Cliente;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use yajra\Datatables\Datatables;

class ClienteContactoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param $cliente_id
     * @return Response
     */
    public function index($cliente_id)
    {

        $cliente = Cliente::find($cliente_id);

        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $cliente->contactos()->get()], 200);
    }


    /**
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex($cliente_id)
    {
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $contactos = $cliente->contactos();
        $clientes = Cliente::select(array("id", "rif", "razon_social", "telefonos", "correos_electronicos"));

        return Datatables::of($contactos)->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $cliente_id
     * @return Response
     */
    public function store(Request $request, $cliente_id)
    {
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $v = Validator::make($request->all(), [
            "nombre" => 'required',
            "apellido" => 'required',
            "telefonos" => 'required',
            "correos_electronicos" => 'required',
            "cargo_id" => 'required',
            "es_interno" => 'required',
            // "cliente_id" => 'required',
            // "es_interno" => 'required',
        ]);
        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
        }

        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($request->all(), $row);
//        dd($row);
        $nuevoContacto = $cliente->contactos()->create($row);

        return response()->json(['status' => 'ok', 'data' => $nuevoContacto], 201);

    }


    /**
     * Display the specified resource.
     *
     * @param $cliente_id
     * @param $contacto_id
     * @return Response
     * @internal param int $id
     */
    public function show($cliente_id, $contacto_id)
    {

        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $contacto = $cliente->contactos()->find($contacto_id);
        if (!$contacto) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un contacto con ese código.'])], 404);
        }

        $contacto->cargo;

        return response()->json(['status' => 'ok', 'data' => $contacto], 200);

    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $cliente_id
     * @param $contacto_id
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, $cliente_id, $contacto_id)
    {
        //var_dump($request->all());die;
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $contacto = $cliente->contactos()->find($contacto_id);
        if (!$contacto) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un contacto con ese código.'])], 404);
        }
        $nombre = $request->input('nombre');
        $apellido = $request->input('apellido');
        $telefonos = $request->input('telefonos');
        $correos_electronicos = $request->input('correos_electronicos');
        $cargo = $request->input('cargo_id');
        $es_interno = $request->input('es_interno');
        $clienteid = $request->input('cliente_id');
        $tipo = $request->input('tipo');

        $bandera = false;
        if ($nombre) {
            $contacto->nombre = $nombre;
            $bandera = true;
        }
        if ($apellido) {
            $contacto->apellido = $apellido;
            $bandera = true;
        }
        if ($telefonos) {
            $contacto->telefonos = $telefonos;
            $bandera = true;
        }

        if ($correos_electronicos) {
            $contacto->correos_electronicos = $correos_electronicos;
            $bandera = true;
        }
        if ($cargo) {
            $contacto->cargo_id = $cargo;
            $bandera = true;
        }
        if ($es_interno) {
            $contacto->es_interno = $es_interno;
            $bandera = true;
        }
        if ($clienteid) {
            $contacto->cliente_id = $clienteid;
            $bandera = true;
        }
        if ($tipo) {
            $contacto->tipo = $tipo;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $contacto->usuario_id = $user->findByName($request->header('username'), true)->id;
            $contacto->save();
            return response()->json(['status' => 'ok', 'data' => $contacto], 200);
        } else {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato del contacto.'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $cliente_id
     * @param $contacto_id
     * @return Response
     * @internal param int $id
     */
    public function destroy($cliente_id, $contacto_id)
    {
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un cliente con ese código.'], 404);
        }
        $contacto = $cliente->contactos()->find($contacto_id);
        if (!$contacto) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un contacto con ese código.'], 404);
        }
        $contacto->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el contacto correctamente.'], 200);

    }

}
