<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\NotasCredito;
use App\Models\Pagos;
use App\Models\Factura;
use Illuminate\Database\Eloquent\Collection;

use Illuminate\Http\Request;


use App\Models\Balance;

use Illuminate\Support\Facades\DB;
use yajra\Datatables\Datatables;
use App\Models\Acl\User;
use App\Models\Utilities\GenericConfiguration;


class PagosController extends Controller
{

    /**
     * @param $pago_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Carlos Blanco <cebs923@gmail.com>
     */
    public function index ($pago_id)
    {
        $pago = Pagos::find ($pago_id);

        if (!$pago) {
            return response ()->json (['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una factura con ese código.'])], 404);
        }

        return response ()->json (['status' => 'ok', 'data' => $pago->factura ()->orderBy ('created_at', 'desc')->get ()], 200);
    }

    /**
     * Retorna los datos del pago con la factura
     *
     * @param $factura_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Maykol Purica <puricamaykol@gmail.com>
     */
    public function show ($factura_id)
    {
        $pago = Pago::with ('factura')->find ($factura_id);
        if (!$pago) {
            return response ()->json ([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una operacion con ese código.']))], 404);
        };
        return response ()->json (array('status' => 'ok', 'data' => $pago, 200));
    }

    /**
     * @param Request $request
     * @return static
     */
    public function store (Request $request)
    {
        $nuevoPago = array(
            'monto' => $request->input ('monto'),
            'descripcion' => $request->input ('descripcion'),
            'formas_pagos' => $request->input ('formas_pagos'),
            'fecha_pago' => $request->input ('fecha_pago'),
            'banco' => $request->input ('banco'),
            'numero_transaccion' => $request->input ('numero_transaccion'),
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge ($nuevoPago, $row);

        $pago = Pagos::create ($row);

        $totalmente_pagada = GenericConfiguration::where ("_label", "monto_total")->pluck ('value');
        $Parcialmente_pagada = GenericConfiguration::where ("_label", "abono_del_monto_total")->pluck ('value');
        $totalmente_pagada_nota = GenericConfiguration::where ("_label", "pago_con_nota_de_credito")->pluck ('value');
        $Parcialmente_pagada_nota = GenericConfiguration::where ("_label", "abono_factura_ajuste_nota_credito")->pluck ('value');
        // $nombreFirmante = GenericConfiguration::where ("_label", "negociacion_firmante_nombre")->pluck ('value');

      //     if ($request->input ('contrato') == null):

            $facturas = $request->input ('facturas');

            $facturas_ids = [];
        // se capturan los ids de las facturas
            foreach ($facturas as $afacturar) {
                $facturas_ids[] = $afacturar['id'];

            }

            foreach ($facturas_ids as $facturacancelada)
            {

                $pagoFactura = Factura::with ('nota', 'cuota.contrato', 'facturaOrdenPublicacion.cotizacion')->find ($facturacancelada);
            


                if ($pago->monto >= $pagoFactura->restante) {
                    $pago->monto = $pago->monto - $pagoFactura->restante;
                    $pagoFactura->restante = 0;

                    if ($pagoFactura->nota->isEmpty ()) {

                        if ($pagoFactura->restante == 0) {
                            $pagoFactura->estatus = 'pagada';
                            $pagoFactura->estatus_factura = $totalmente_pagada;
                            $pagoFactura->fecha_cancelada = $request->input ('fecha_pago');
                        } else {
                            $pagoFactura->estatus = 'entregado cliente';
                            $pagoFactura->estatus_factura = $Parcialmente_pagada;
                        }

                    } else {
                        if ($pagoFactura->restante == 0) {
                            $pagoFactura->estatus = 'pagada';
                            $pagoFactura->estatus_factura = $totalmente_pagada_nota;
                            $pagoFactura->fecha_cancelada = $request->input ('fecha_pago');
                        } else {
                            $pagoFactura->estatus = 'entregado cliente';
                            $pagoFactura->estatus_factura = $Parcialmente_pagada_nota;
                        }
                    }
                } elseif ($pago->monto < $pagoFactura->restante) {
                    $pagoFactura->restante = $pagoFactura->restante - $pago->monto;
                    $pago->monto = 0;
                    //dd($pagoFactura->nota);
                    if ($pagoFactura->nota->isEmpty ()) {
                        if ($pagoFactura->restante == 0) {
                            $pagoFactura->estatus = 'pagada';
                            $pagoFactura->estatus_factura = $totalmente_pagada;
                            $pagoFactura->fecha_cancelada = $request->input ('fecha_pago');
                        } else {
                            $pagoFactura->estatus = 'entregado cliente';
                            $pagoFactura->estatus_factura = $Parcialmente_pagada;
                        }


                    } else {

                        if ($pagoFactura->restante == 0) {
                            $pagoFactura->estatus = 'pagada';
                            $pagoFactura->estatus_factura = $totalmente_pagada_nota;
                            $pagoFactura->fecha_cancelada = $request->input ('fecha_pago');
                        } else {

                            $pagoFactura->estatus = 'entregado cliente';
                            $pagoFactura->estatus_factura = $Parcialmente_pagada_nota;
                        }
                    }
                }
                $pagoFactura->save ();

             }
                $pago->factura ()->attach ($facturas_ids);

                 //Guardo en el balance el debito
                if($pagoFactura->tipo_factura == 'publicacion'){

                    //Se hace la colsulta y se traen los datos globales y por contrato
                    //datos globales
                    $where = [ 'cliente_id' => $request->input ('cliente') ];
                    $datos_globales = Balance::where ($where)->latest ()->first(['saldo','deuda']);
                    //datos del contrato
                    $where['contrato_id'] = $pagoFactura['facturaOrdenPublicacion'][0]['cotizacion']['contrato_id'];
                    $datos_contrato = Balance::where ($where)->latest ()->first(['contrato_deuda','contrato_saldo']);

                    $nuevoBalance = [

                        'cliente_id' => $request->input ('cliente'),
                        'contrato_id' => $pagoFactura['facturaOrdenPublicacion'][0]['cotizacion']['contrato_id'],
                        'usuario_id' => 1,//arreglar esta asi por pruebas
                        'deuda' => $datos_globales->deuda - $request->input ('monto'),
                        'contrato_deuda' => $datos_contrato->contrato_deuda - $request->input ('monto'),
                        'contrato_cuota'=> $request->input ('monto'),
                        'contrato_saldo'=> $datos_contrato->contrato_saldo,
                        'saldo' => $datos_globales->saldo,
                        'factura_id' => $pagoFactura['id'],
                        'descripcion' => 'Cobro realizado a la factura #' . $pagoFactura->codigo,
                    ];

                    Balance::create ($nuevoBalance);

                }elseif($pagoFactura->tipo_factura == 'cuota'){

                    $pago->contrato ()->attach ($request->input ('contrato'));

                    //Se hace la colsulta y se traen los datos globales y por contrato
                    //datos globales
                    $where = [ 'cliente_id' => $request->input ('cliente') ];
                    $datos_globales = Balance::where ($where)->latest ()->first(['saldo','deuda']);
                    //datos del contrato
                    $where['contrato_id'] = $pagoFactura['cuota'][0]['contrato_id'];
                    $datos_contrato = Balance::where ($where)->latest ()->first(['contrato_deuda','contrato_saldo']);


                    $nuevoBalance = [

                        'cliente_id' => $request->input ('cliente'),
                        'contrato_id' => $pagoFactura['cuota'][0]['contrato_id'],
                        'usuario_id' => 1,//arreglar esta asi por pruebas
                        'deuda' => $datos_globales->deuda - $request->input ('monto'),
                        'contrato_deuda' => $datos_contrato->contrato_deuda - $request->input ('monto'),
                        'contrato_cuota'=> $request->input ('monto'),
                        'contrato_saldo'=> $datos_contrato->contrato_saldo,
                        'saldo' => $datos_globales->saldo,
                        'factura_id' => $pagoFactura['id'],
                        'descripcion' =>  'Abono realizado al contrato ' . $pagoFactura['cuota'][0]['contrato']['codigo'],
                    ];

                    Balance::create ($nuevoBalance);
                }


           //  else:


            //   endif;

          return $pago;
    }


    /**    Listado de facturas por pagar
     *
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listadofacturaporcobrar ($cliente_id)
    {

        $data = DB::select (DB::raw ("
      SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at, FACTC.restante,FACTC.estatus, FACTC.codigo, FACTC.total_factura
              FROM factura_cliente FACTC

              LEFT JOIN contrato_cuotas CONC ON FACTC.id = CONC.factura_id
              LEFT JOIN contratos CON ON CONC.contrato_id = CON.id

            WHERE
            FACTC.restante > 1 AND
            FACTC.estatus = 'entregado cliente' AND
            CON.cliente_id = $cliente_id

            UNION
            SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at, FACTC.restante, FACTC.estatus, FACTC.codigo,FACTC.total_factura
              FROM factura_cliente FACTC

	      LEFT JOIN ordenes_publicacion ORDP ON ORDP.factura_id = FACTC.id

            WHERE
            FACTC.restante > 1 AND
            FACTC.estatus = 'entregado cliente' AND
            ORDP.cliente_id = $cliente_id"
        ));

        $data = new Collection($data);
        return Datatables::of ($data)
            ->addColumn ('pagar', function ($facturas) {

                $codigo = $facturas->codigo;
                return '<div class="checkbox"><input
                type="checkbox"
                id="chk_' . $facturas->id . '"
                name="chk_' . $facturas->id . '"
                value="' . $facturas->id . '"
                ng-model="checkFactura.itm_' . $facturas->id . '.chckd"
                ng-init="checkFactura.itm_' . $facturas->id . '
                .id = ' . $facturas->id . ';
                 checkFactura.itm_' . $facturas->id . '
                 .chckd = true;
                  checkFactura.itm_' . $facturas->id . '
                 .restante = ' . $facturas->restante . ';
                 checkFactura.itm_' . $facturas->id . '
                 .total_factura = ' . $facturas->total_factura . ';
                 checkFactura.itm_' . $facturas->id . '
                 .codigo = ' . "'" . $codigo . "'" . ';
                  checkFactura.itm_' . $facturas->id . '"
                checked="checked"
                 ng-click=""/></div>';
            })
            ->make (true);

    }

    /**   Listado de facturas ya pagas
     *
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function facturasPagadasIndex ($cliente_id)
    {

        $data = DB::select (DB::raw ("
      SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at, FACTC.restante,FACTC.estatus, FACTC.codigo, FACTC.total_factura
              FROM factura_cliente FACTC

              LEFT JOIN contrato_cuotas CONC ON FACTC.id = CONC.factura_id
              LEFT JOIN contratos CON ON CONC.contrato_id = CON.id

            WHERE
            FACTC.restante = 0 AND
            FACTC.estatus = 'pagada' AND
            CON.cliente_id = $cliente_id

            UNION
            SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at, FACTC.restante, FACTC.estatus, FACTC.codigo,FACTC.total_factura
              FROM factura_cliente FACTC

	      LEFT JOIN ordenes_publicacion ORDP ON ORDP.factura_id = FACTC.id

            WHERE
            FACTC.restante = 0 AND
            FACTC.estatus = 'pagada' AND
            ORDP.cliente_id = $cliente_id"
        ));

//        $data = DB::table ('ordenes_publicacion AS op ')
//            ->join ('clientes AS c', 'op.cliente_id', '=', 'c.id')
//            ->join ('factura_cliente AS fc', 'op.factura_id', '=', 'fc.id')
//            ->select (['fc.id', 'fc.codigo', 'estatus', 'total_factura', 'restante'])
//            ->where ('c.id', $idcliente)
//            ->where ('fc.estatus', 'pagada')
//            ->where ('fc.restante', 0)
//            ->groupBy ('fc.id', 'fc.created_at', 'fc.codigo', 'fc.updated_at')
//            ->get ();


        $data = new Collection($data);
        return Datatables::of ($data)->make (true);
    }

}