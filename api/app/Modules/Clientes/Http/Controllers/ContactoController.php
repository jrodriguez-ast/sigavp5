<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;


use App\Models\ContactoCliente as Contacto;

class ContactoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json(['status' => 'ok', 'data' => Contacto::all()], 200);
    }

    /**
     *
     */
    public function dtIndex()
    {
        $contactos = Contacto::all();//->toArray();

        return Datatables::of($contactos)->make(true);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $contacto = Contacto::find($id);
        if (!$contacto) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        return response()->json(array('status' => 'ok', 'data' => $contacto), 200);
    }


}
