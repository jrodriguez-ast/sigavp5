<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Acl\User;
use App\Models\Contrato;
use App\Models\TipoContrato;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class ContratoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json([
            'status' => 'ok',
            'data' => Contrato::all()
        ], 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $contrato = Contrato::with([
            'cliente',
            'tipo',
            'parent',
            'children',
            'regimen',
            'cuotas',
            'pautas.tipo'
        ])->find($id);
        //$contrato = Contrato::find($id);
        //$contrato->cliente;
        if (!$contrato) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un contrato con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $contrato], 200);
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param \Illuminate\Http\Request $request
     * @param                          $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $id)
    {
        $contrato = Contrato::find($id);
        if (!$contrato) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']))], 404);
        }
        $cerrado = $request->input('cerrado');
        $activo = $request->input('activo');
        $es_postpago = $request->input('es_postpago');
        $regimen_pago = $request->input('regimen_pago_id');
        $tarifa = $request->input('tarifa');
        $cuota = $request->input('cuota');

        $tipoCOntrato = TipoContrato::where('nombre', 'addendum')->pluck('id');
        if ($request->input('tipo_contrato_id')) {
            $tipo_contrato_id = $request->input('tipo_contrato_id');
        }
        if ($request->input('contratos_id') && $request->input('tipo_contrato_id') == $tipoCOntrato) {
            $contratos_id = $request->input('contratos_id');
        } elseif ($request->input('contratos_id') && $request->input('tipo_contrato_id') != $tipoCOntrato) {
            //unset($nuevoContrat['contratos_id']);
            $contratos_id = null;
        }
        if ($request->input('contratos_id') && $request->input('tipo_contrato_id') == $tipoCOntrato && $request->input('regimen_pago_id')) {
            $regimen_pago = null;
        }

        $bandera = false;
        if ($cerrado !== '') {
            $contrato->cerrado = $cerrado;
            $bandera = true;
        }
        if ($activo !== '') {
            $contrato->activo = $activo;
            $bandera = true;
        }
        if ($es_postpago) {
            $contrato->es_postpago = $es_postpago;
            $bandera = true;
        }
        if ($tipo_contrato_id) {
            $contrato->tipo_contrato_id = $tipo_contrato_id;
            $bandera = true;
        }
        if ($regimen_pago) {
            $contrato->regimen_pago_id = $regimen_pago;
            $bandera = true;
        }
        if ($regimen_pago == null) {
            $contrato->regimen_pago_id = null;
            $bandera = true;
        }
        if ($tarifa) {
            $contrato->tarifa = $tarifa;
            $bandera = true;
        }
        if ($cuota) {
            $contrato->cuota = $cuota;
            $bandera = true;
        }
        if ($contratos_id) {
            $contrato->contratos_id = $contratos_id;
            $bandera = true;
        }
        if ($contratos_id == null) {
            $contrato->contratos_id = null;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $contrato->usuario_id = $user->findByName($request->header('username'), true)->id;
            $contrato->save();
            return response()->json(array('status' => 'ok', 'data' => $contrato), 200, array('Location' => 'localhost/laravel/api/public/clientes/cliente/' . $contrato->id));
        } else {
            return response()->json(array('status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato de cliente.'])), 304);
        }
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return mixed
     */
    public function dtIndex()
    {
//        $data = Contrato::with(['cliente', 'tipo', 'parent'])->whereNotIn('tipo_contrato_id', [2])->get();
        $data = Contrato::with(['cliente', 'tipo', 'parent'])->get();
        //Book::with('author.contacts')->get();
        //$data = new Collection($contratos);
        return Datatables::of($data)
            ->editColumn('relacion', function ($data) {
                return (empty($data['parent'])) ? '-' : $data['parent']['codigo'];
            })
            ->make(true);
    }

    public function destroy($id)
    {
        $contrato = Contrato::find($id);
        if (!$contrato) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un contrato con ese código.'], 404);
        }

        $contrato->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el contrato correctamente.'], 200);

    }

}