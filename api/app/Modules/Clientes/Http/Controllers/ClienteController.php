<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Acl\User;
use App\Models\Cliente;
use App\Models\TechoConsumo;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Response;
use yajra\Datatables\Datatables;

class ClienteController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $respuesta = array(
            'status' => 'ok',
            'data' => Cliente::all(),
        );

        return response()->json([$respuesta], 200);
    }

    public function dtIndex()
    {
        $clientes = Cliente::select(array("id", "rif", "razon_social", "telefonos", "correos_electronicos", "codigo"));
        //$clientes = Cliente::all();//->toArray();
        $clientArray = new Collection;
        foreach ($clientes as $key => $value) {
            $clientArray->push([
                'id' => $value['id'],
                'rif' => $value['rif'],
                'razon_social' => $value['razon_social'],
                'telefonos' => $value['telefonos'],
                'correos_electronicos' => $value['correos_electronicos'],
                'codigo' => $value['codigo'],
                'abreviatura' => $value['abreviatura'],
                'nombre_comercial' => $value['nombre_comercial'],
                'img_clientes' => $value['img_clientes'],
                'differentiation_chain' => $value['differentiation_chain'],
            ]);
        }
        return Datatables::of($clientes)
            ->editColumn('telefonos', function ($data) {
                return json_decode($data->telefonos);
            })
            ->editColumn('correos_electronicos', function ($data) {
                return json_decode($data->correos_electronicos);
            })->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $v = Validator::make($request->all(), [
            'razon_social' => 'required',
            'rif' => 'required',
            /*'fecha_fundacion' => 'required',
            'direccion_fiscal' => 'required',
            'servicios' => 'required',
            'otros_canales' => 'required',
            'telefonos' => 'required',
            'correos_electronicos' => 'required',
            'codigo'=> 'required',
            'abreviatura'=> 'required'
            'nombre_comercial'=> 'required'
            'img_clientes'=> 'required'
            'differentiation_chain' => 'required'*/
        ]);
        if ($v->fails()) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()]))], 422);
        }
        /*$file =  $request->file('imglogo');
            $extension = $file->getClientOriginalExtension();
    Storage::disk('local')->put($file->getFilename().'.'.$extension,  File::get($file));die;*/
        if (isset($request->telefonos)):
            $request->telefonos = json_encode($request->telefonos);
        endif;
        if (isset($request->correos_electronicos)):
            $request->correos_electronicos = json_encode($request->correos_electronicos);
        endif;

        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $resultado = array_merge($request->all(), $row);
        $nuevoCliente = Cliente::create($resultado);
        return response()->json(array('status' => 'ok', 'data' => $nuevoCliente), 201);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $cliente = Cliente::find($id);
        if (!$cliente) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']))], 404);
        };
        $cliente_arr = array(
            'razon_social' => $cliente->razon_social,
            'rif' => $cliente->rif,
            'direccion_fiscal' => $cliente->direccion_fiscal,
            //'servicios' => json_decode($cliente->servicios),
            'otros_canales' => json_decode($cliente->otros_canales),
            'telefonos' => json_decode($cliente->telefonos),
            'correos_electronicos' => json_decode($cliente->correos_electronicos),
            'sitio_web' => $cliente->sitio_web,
            'codigo' => $cliente->codigo,
            'abreviatura' => $cliente->abreviatura,
            'nombre_comercial' => $cliente->nombre_comercial,
            'img_cliente' => $cliente->img_cliente,
            'differentiation_chain' => $cliente->differentiation_chain,
            'techo_consumo' => $cliente->techo_consumo
        );
        return response()->json(array('status' => 'ok', 'data' => $cliente_arr), 200);
    }

    /**
     * @param Request $request
     * @param         $id
     * @return mixed
     */
    public function update(Request $request, $id)
    {
        $cliente = Cliente::find($id);
        if (!$cliente) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']))], 404);
        }

        $razon_social = $request->input('razon_social');
        $rif = $request->input('rif');
        $direccion_fiscal = $request->input('direccion_fiscal');
        $servicios = $request->input('servicios');
        $otros_canales = $request->input('otros_canales');
        $telefonos = $request->input('telefonos');
        $correos_electronicos = $request->input('correos_electronicos');
        $sitio_web = $request->input('sitio_web');
        $codigo = $request->input('codigo');
        $abreviatura = $request->input('abreviatura');
        $nombre_comercial = $request->input('nombre_comercial');
        $bandera = false;
        if ($razon_social) {
            $cliente->razon_social = $razon_social;
            $bandera = true;
        }
        if ($rif) {
            $cliente->rif = $rif;
            $bandera = true;
        }
        if ($otros_canales) {
            $cliente->otros_canales = $otros_canales;
            $bandera = true;
        }
        if ($direccion_fiscal) {
            $cliente->direccion_fiscal = $direccion_fiscal;
            $bandera = true;
        }
        if ($servicios) {
            $cliente->servicios = $servicios;
            $bandera = true;
        }
        if ($telefonos) {
            $cliente->telefonos = $telefonos;
            $bandera = true;
        }
        if ($correos_electronicos) {
            $cliente->correos_electronicos = $correos_electronicos;
            $bandera = true;
        }
        if ($sitio_web) {
            $cliente->sitio_web = $sitio_web;
            $bandera = true;
        }
        if ($codigo) {
            $cliente->codigo = $codigo;
            $bandera = true;
        }
        if ($abreviatura) {
            $cliente->abreviatura = $abreviatura;
            $bandera = true;
        }
        if ($nombre_comercial) {
            $cliente->nombre_comercial = $nombre_comercial;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $cliente->usuario_id = $user->findByName($request->header('username'), true)->id;
            $cliente->save();
            return response()->json(array('status' => 'ok', 'data' => $cliente), 200);
        } else {
            return response()->json(array('status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato de cliente.'])), 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($cliente_id)
    {
        $flag=false;
        $msj=null;
        $cliente = Cliente::find($cliente_id);

        if (!$cliente) {
            $flag=true;
            $msj="No se encuentra un cliente con ese codigo.";
        }

        if ($cliente->cotizaciones()->count() > 0) {
            $flag=true;
            $msj="Este cliente tiene cotizaciones asociadas.";
        }

        if ($cliente->publicaciones()->count() > 0) {
            $flag=true;
            $msj="Este cliente tiene publicaciones asociadas.";
        }

        if ($cliente->contratos()->count() > 0) {
            $flag=true;
            $msj="Este cliente tiene contratos asociados.";
        }

        if ($cliente->balance()->count() > 0) {
            $flag=true;
            $msj="Este cliente tiene balances asociadas.";
        }

        if ($cliente->nota()->count() > 0) {
            $flag=true;
            $msj="Este cliente tiene notas asociadas.";
        }

        if($flag == true){
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => $msj], 404);
        }

        $cliente->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el cliente correctamente.'], 200);
    }


    /**
     * Retorna un listado con las coincidencias de clientes
     *
     * @param string $texto
     * @return mixed
     */
    public function referencia($texto = "")
    {
        $clientes = new Cliente();
        return response()->json(['status' => 'ok', 'data' => $clientes->getReferencia($texto)], 200);
    }

    public function data($id = null)
    {
        $clientes = new Cliente();
        return response()->json(['status' => 'ok', 'data' => $clientes->getData($id)], 200);
    }

    /**
     * Controlador que provee acceso a los datos del balance.
     * @param int $cliente_id
     * @param int $contrato_id
     * @return mixed
     */
    public function  saldoActual($cliente_id, $contrato_id = 0)
    {
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json([
                'status' => 'fail',
                'errors' => [
                    'code' => 404,
                    'message' => 'No se encuentra un cliente con ese código.'
                ]
            ], 404);
        }

        $data = $cliente->balance();
        if (empty($contrato_id)) {
            $data->select('saldo', 'deuda');
        } else {
            $data->select('contrato_saldo AS saldo', 'contrato_deuda AS deuda' );
            $data->where(['contrato_id' => $contrato_id]);
        }
        $data = $data->orderBy('id', 'desc')->first();

        //Si el resultado es null se retorna 0 de modo que no haya error en el frontend
        if (empty($data)) {
            $data = 0;
        }

        return response()->json(['status' => 'ok', 'data' => $data], 200);
    }

    /**
     * @param $rif
     * @return mixed
     */
    public function getByRif($rif)
    {
        $cliente = Cliente::where('rif', $rif)->get();
        if (count($cliente) == 0) {
            return response()->json(array('status' => 'fail'), 200);
        }
        return response()->json(array('status' => 'ok', 'data' => $cliente), 200);
    }


    public function guardarImg(Request $request)
    {   //Respuesta default
        $data = array('success' => false);

        //obtenemos el campo file definido en el formulario

        $file = $request->file('file');
        //obtenemos el nombre del archivo
        $nombre = str_random() . '.' . $file->getClientOriginalExtension();


        //indicamos que queremos guardar un nuevo archivo en el disco local/tmp
        if (\Storage::disk('tmp')->put($nombre, \File::get($file))):
            $data = array(
                'success' => true,
                'file_name' => $nombre,
                'message' => 'Archivo agregado exitosamente',
            );
            return Response($data, 200);
        endif;

    }

    public function mover(Request $request, $id)
    {

        $file_name = $request->input('file_name');

        Storage::move('tmp/' . $file_name, 'img_clientes/' . $file_name);

        $cliente = Cliente::find($id);
        if (!$cliente) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']))], 404);
        }

        $cliente->img_cliente = $file_name;

        $cliente->save();
        return response()->json(array('status' => 'ok', 'data' => $cliente), 200);

    }

    /**
     * Obtiene el histórico de techo de consumo de una cliente conocido.
     * @param $cliente_id
     * @return mixed
     */
    public function techo_historial($cliente_id)
    {
        $techo = new TechoConsumo();
        $data = $techo->cliente($cliente_id);
        return response()->json(array('status' => 'ok', 'data' => $data), 200);
    }

    /**
     * @param Request $request
     * @param $cliente_id
     * @return mixed
     */
    public function update_techo(Request $request, $cliente_id)
    {
        $v = Validator::make($request->all(), ['techo' => 'required|numeric']);
        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => ['code' => 422, 'message' => $v->errors()]], 422);
        }

        $data = (new Cliente())->updateTechoConsumo($cliente_id, $request);
        return response()->json(array('status' => 'ok', 'data' => $data), 200);
    }

}
