<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Cliente;


class ClienteFacturarController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $cliente = Cliente::find($idcliente);

        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $cliente->contratos()->get()], 200);
    }

}