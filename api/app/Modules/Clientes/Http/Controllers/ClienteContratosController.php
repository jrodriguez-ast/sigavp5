<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Acl\User;
use App\Models\BalancePorTipoPublicacion;
use App\Models\Cliente;
use App\Models\ContratoCuotas;
use App\Models\ContratoPauta;
use App\Models\TipoContrato;
use App\Models\Utilities\GenericConfiguration;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use yajra\Datatables\Datatables;

//Modelo de Cuotas

/**
 * Controlador que brinda acceso a los contratos relacionados con el cliente.
 * Class ClienteContratosController
 * @package App\Modules\Clientes\Http\Controllers
 */
class ClienteContratosController extends Controller
{

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index($cliente_id)
    {
        $cliente = Cliente::find($cliente_id);

        if (!$cliente) {
            return response()->json([
                'status' => 'fail',
                'errors' => [['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']]
            ], 404);
        }
        return response()->json([
            'status' => 'ok',
            'data' => $cliente->contratos()->get()
        ]);
    }

    /**
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function activosIndex($cliente_id)
    {
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json([
                'status' => 'fail',
                'errors' => [['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']]
            ], 404);
        }

        $contratos = $cliente->contratos()->with([
            'tipo',
            'children',
            'cuotas',
            'pautas.tipo'
        ])
            ->whereNotIn('tipo_contrato_id', [2])
            ->orderBy('id', 'desc')
            ->get();

        // Esta acción a fin de obtener la ultima linea del balance referente al contrato y
        // seteo activo en función del monto restante.
        foreach ($contratos as &$contrato) {
            $contrato['balance'] = $contrato->balance()->latest()->first();
            $contrato['activo'] = $contrato['balance']['contrato_saldo'] > 0;

            if ($contrato['activo']) {
                $contrato['balance']['porTipoPublicacion'] = BalancePorTipoPublicacion::
                where('contrato_id', $contrato->id)
                    ->groupBy('contrato_id')
                    ->groupBy('tipo_publicacion_id')
                    ->select(['contrato_id', 'tipo_publicacion_id', DB::raw('MIN(monto)')])
                    ->get();
            }
        }

        return response()->json([
            'status' => 'ok',
            'data' => $contratos
        ]);
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex($cliente_id)
    {
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json([
                'status' => 'fail',
                'errors' => [
                    ['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']
                ]
            ], 404);
        }
        $contratos = $cliente->contratos()->with(['tipo', 'parent'])->get();
        $data = new Collection($contratos);
        return Datatables::of($data)
            ->editColumn('relacion', function ($data) {
                return (empty($data['parent'])) ? '-' : $data['parent']['codigo'];
            })
            ->make(true);
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param \Illuminate\Http\Request $request
     * @param                          $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, $cliente_id)
    {
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $user = new User();
        $nuevoContrat = [
            'firmante_cargo' => GenericConfiguration::where('_label', 'contrato_firmante_cargo')->pluck('value'),
            'firmante_nombre' => GenericConfiguration::where('_label', 'contrato_firmante_nombre')->pluck('value'),
            "cliente_id" => $cliente_id,
            //"es_postpago" => $request->input('es_postpago'),
            "tarifa" => $request->input('tarifa'),
            "monto_iva" => $request->input('monto_iva'),
            //"cerrado" => $request->input('cerrado'),
            //"activo" => $request->input('activo'),
            "usuario_id" => $user->findByName($request->header('username'), true)->id,
            'fecha_inicio' => $request->input('fecha_inicio'),
            'fecha_fin' => $request->input('fecha_fin'),
            'fecha_creacion' => $request->input('fecha_creacion'),
            'por_publicacion' => $request->input('por_publicacion'),
            'descripcion' => $request->input('descripcion'),
            'codigo' => $request->input('codigo'),
        ];
        $tipoCOntrato = TipoContrato::where('nombre', 'addendum')->pluck('id');
        if ($request->input('tipo_contrato_id')) {
            $nuevoContrat['tipo_contrato_id'] = $request->input('tipo_contrato_id');
        }
        if ($request->input('contratos_id') && $request->input('tipo_contrato_id') == $tipoCOntrato) {
            $nuevoContrat['contratos_id'] = $request->input('contratos_id');
        } elseif ($request->input('contratos_id') && $request->input('tipo_contrato_id') != $tipoCOntrato) {
            unset($nuevoContrat['contratos_id']);
        }

        $nuevoContrato = $cliente->contratos()->create($nuevoContrat);
        $tmp = ['pautas' => null, 'cuotas' => null];

        //if($request->has('pautas')) {
        foreach ($request->input('pautas') as $key => $value) {
            $pautas[] = new ContratoPauta($value);
        }
        $tmp['pautas'] = $nuevoContrato->pautas()->saveMany($pautas);
        //}

        if (!$request->input('por_publicacion')) {
            foreach ($request->input('cuotas') as $key => $value) {
                $cuotas[] = new ContratoCuotas($value);
            }

            $tmp['cuotas'] = $nuevoContrato->cuotas()->saveMany($cuotas);
        }

        $nuevoContrato->balancear = '1';
        $nuevoContrato->save();

        $nuevoContrato['pautas'] = $tmp ['pautas'];
        $nuevoContrato['cuotas'] = $tmp ['cuotas'];
        return response()->json(['status' => 'ok', 'data' => $nuevoContrato], 201, array('Content-Type' => 'application/json'));

    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listTiposContratos()
    {
        return response()->json(['status' => 'ok', 'data' => TipoContrato::all()], 200);

    }

    /**
     * @param $id_cliente
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listTipoContrato($id_cliente)
    {
        $cliente = Cliente::find($id_cliente);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        return response()->json(['status' => 'ok', 'data' => $cliente->tipoContratos()->get()->toArray()], 200);

    }

}
