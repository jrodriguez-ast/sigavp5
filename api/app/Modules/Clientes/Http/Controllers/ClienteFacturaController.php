<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\ContratoCuotas;
use App\Models\Medio;

use Illuminate\Database\Eloquent\Collection;
use yajra\Datatables\Datatables;
use App\Models\Cliente;
use App\Models\Publicacion;
use App\Models\Factura;
use App\Models\Utilities\GenericConfiguration;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

use DateTime;
use App\Models\Acl\User;
use Illuminate\Support\Facades\DB;
use App\Models\FacturaContratoCuotas;
use App\Models\FacturaOrdenPublicacion;
use App\Models\NotasCredito;
use App\Models\Balance;

class ClienteFacturaController extends Controller
{

    /**
     * Método que genera la factura y asocia las publicaciones a la misma
     *
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function facturar (Request $request)
    {
        $nombreFirmante = GenericConfiguration::where ("_label", "negociacion_firmante_nombre")->pluck ('value');
        $cargoFirmante = GenericConfiguration::where ("_label", "negociacion_firmante_cargo")->pluck ('value');

        $factura = Factura::create ([
            'nombre_firmante' => $nombreFirmante,
            'cargo_firmante' => $cargoFirmante,
            $user = new User(),
            $row['usuario_id'] = $user->findByName ($request->header ('username'), true)->id,
        ]);
        $subtotal = 0;
        foreach ($request->input ('factura') as $key => $publicacion):
            $objPublicacion = Publicacion::find ($publicacion['valor']);
            $objPublicacion->factura_id = $factura->id;
            $objPublicacion->por_facturar = false;
            $user = new User();
            $objPublicacion->usuario_id = $user->findByName ($request->header ('username'), true)->id;
            $objPublicacion->save ();
            $subtotal = $subtotal + $publicacion['totalpub'];
            $orden_id = $objPublicacion->id;
            FacturaOrdenPublicacion::create ([
                'factura_id' => $factura->id,
                'orden_id' => $orden_id,
                $user = new User(),
                $row['usuario_id'] = $user->findByName ($request->header ('username'), true)->id,
            ]);
        endforeach;
        //d($subtotal);
        $factura->total_factura = $subtotal;
     //   dd($subtotal);
        $factura->restante = $subtotal;
        $factura->n_control = $request->input ('n_control');
        $factura->tipo_factura = 'publicacion';
        $factura->save ();

        return response ()->json (['status' => 'ok', 'data' => $factura], 200);
    }

    /**
     * Método que genera la factura y asocia la cuota a la misma
     *
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param \Illuminate\Http\Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function facturarcuota (Request $request)
    {
        $nombreFirmante = GenericConfiguration::where ("_label", "negociacion_firmante_nombre")->pluck ('value');
        $cargoFirmante = GenericConfiguration::where ("_label", "negociacion_firmante_cargo")->pluck ('value');

        $factura = Factura::create ([
            'nombre_firmante' => $nombreFirmante,
            'cargo_firmante' => $cargoFirmante,
            $user = new User(),
            $row['usuario_id'] = $user->findByName ($request->header ('username'), true)->id,
        ]);
        $cuota_id = '';
        $total = 0;
        $iva = 0;
        foreach ($request->input ('factura') as $key => $cuota):

            $contrato_cuota = ContratoCuotas::find ($cuota['valor']);
            $contrato_cuota->factura_id = $factura->id;
            $contrato_cuota->save ();
            $total = $total + $cuota['montototal'];
            $iva = $iva + $cuota['montoiva'];
            $cuota_id = $contrato_cuota->id;
            endforeach;
            FacturaContratoCuotas::create ([
                'factura_id' => $factura->id,
                'contrato_id' => $contrato_cuota->contrato_id,
                'cuota_id' => $cuota_id,
                $user = new User(),
                $row['usuario_id'] = $user->findByName ($request->header ('username'), true)->id,
            ]);

        
        $totalconiva = $total + $iva;
        $factura->total_factura = $totalconiva;
        $factura->restante = $totalconiva;
        $factura->n_control = $request->input ('n_control');
        $factura->tipo_factura = 'cuota';
        $factura->save ();


        return response ()->json (['status' => 'ok', 'data' => $factura], 200);
    }


    /**
     * Calcula los totales de todas las publicaciones pendientes por facturar del cliente dado
     *
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     * TODO el campo aprobada puede que cambie
     */
    public function totalFactura ($cliente_id)
    {
        $cliente = Cliente::find ($cliente_id);
        $publicaciones = $cliente->publicacion ()->select (
            'ordenes_publicacion_estatus.estatus',
            'ordenes_publicacion.*',
            'ordenes_publicacion_versiones',
            'ordenes_publicacion_versiones.version_nombre',
            'ordenes_publicacion_versiones.valor_con_recargo',
            'servicios.nombre'
        )->with (['medio', 'version'])
            ->join ('ordenes_publicacion_estatus_estatus', 'ordenes_publicacion_estatus_estatus.orden_publicacion_id', '=', 'ordenes_publicacion.id')
            ->join ('ordenes_publicacion_versiones', 'ordenes_publicacion_versiones.orden_publicacion_id', '=', 'ordenes_publicacion.id')
            ->join ('servicios', 'servicios.id', '=', 'ordenes_publicacion_versiones.servicio_id')
            ->where('ordenes_publicacion.factura_id',null)
            ->join ('ordenes_publicacion_estatus', 'ordenes_publicacion_estatus.id', '=', 'ordenes_publicacion_estatus_estatus.orden_publicacion_estatus_id')
            ->whereRaw ("ordenes_publicacion_versiones.publicado = '1' and ordenes_publicacion_estatus_estatus.orden_publicacion_estatus_id IN (
                               SELECT id
                  FROM ordenes_publicacion_estatus
                  WHERE  estatus ='Publicada'
            )")
            ->get();
        $total = 0.00;
        $subtotal =  0.00;
        $total_impuesto = 0.00;
        $total_impuesto_avp = 0.00;
        $count = 0;
        foreach ($publicaciones as $publicacion) {
            $total = $publicacion['total'];
            $subtotal =  $publicacion['subtotal'];
            $total_impuesto = $publicacion['total_impuesto'];
            $total_impuesto_avp = $publicacion['total_impuesto_avp'];
            $count++;
        }

        $response = [
            'status' => 'ok',
            'sinIva' => $subtotal,
            'conIva' => $total,
           // 'cantidad' => $count,
            'importe' => $subtotal - $total_impuesto_avp,
            'total_impuesto' => $total_impuesto,
            'total_impuesto_avp' => $total_impuesto_avp,
        ];
        return response ()->json ($response, 200);
    }

    /**
     * Listado las facturas del cliente
     *
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function facturas_del_cliente ($cliente_id)
    {
        $data = DB::select (DB::raw ("
      SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at
              FROM factura_cliente FACTC

              LEFT JOIN contrato_cuotas CONC ON FACTC.id = CONC.factura_id
              LEFT JOIN contratos CON ON CONC.contrato_id = CON.id

            WHERE

            CON.cliente_id = $cliente_id

            UNION
            SELECT FACTC.id, FACTC.created_at, FACTC.codigo, FACTC.updated_at
              FROM factura_cliente FACTC

	      LEFT JOIN ordenes_publicacion ORDP ON ORDP.factura_id = FACTC.id

            WHERE

            ORDP.cliente_id = $cliente_id"
        ));

        $data = new Collection($data);
        return Datatables::of ($data)->make (true);

//        $facturasArray = Cliente::factura ($cliente_id);
//
//        //Puesto que el método estático factura() retorna un arreglo se debe crear un objeto Collection a partir de él
//        $facturas = new Collection;
//
//        foreach ($facturasArray as $key => $row):
//            $fecha1 = new DateTime($row->created_at);
//            $row->created_at = $fecha1->format ("d-m-Y");
//            $fecha2 = new DateTime($row->updated_at);
//            $row->updated_at = $fecha2->format ("d-m-Y");
//            $facturas->push ([
//                'id' => $row->id,
//                'created_at' => $row->created_at,
//                'codigo' => $row->codigo,
//                'updated_at' => $row->updated_at,
//            ]);
//        endforeach;
//        return Datatables::of ($facturas)
//            ->make (true);
    }

    /**
     * @param $cliente_id
     * @param $factura_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show ($cliente_id, $factura_id)
    {
        $factura = Factura::with (
            'facturaContratoCuotas',
            'facturaContratoCuotas.contrato',
            'facturaOrdenPublicacion',
            'facturaOrdenPublicacion.medio',
            'facturaOrdenPublicacion.version.servicio',
            'nota'
        )->find ($factura_id)->toArray ();
        $total = 0.00;
        $subtotal = 0.00;
        $iva = 0.00;
        $count = 0;

        // dd ($factura);
        // dd ($factura['factura_orden_publicacion']);
        /**
         * Formateo de las fechas
         */
        $created_at = new DateTime($factura['created_at']);
        $factura['created_at'] = $created_at->format ("d-m-Y");
        $updated_at = new DateTime($factura['updated_at']);
        $factura['updated_at'] = $updated_at->format ("d-m-Y");

        if (!empty($factura['fecha_motorizado'])) {
            $fecha_motorizado = new DateTime($factura['fecha_motorizado']);
            $factura['fecha_motorizado'] = $fecha_motorizado->format ("d-m-Y");
        }
        if (!empty($factura['fecha_generada'])) {
            $fecha_generada = new DateTime($factura['fecha_generada']);
            $factura['fecha_generada'] = $fecha_generada->format ("d-m-Y");
        }
        if (!empty($factura['fecha_cliente'])) {
            $fecha_cliente = new DateTime($factura['fecha_cliente']);
            $factura['fecha_cliente'] = $fecha_cliente->format ("d-m-Y");
        }
        if (!empty($factura['fecha_cancelada'])) {
            $fecha_cancelada = new DateTime($factura['fecha_cancelada']);
            $factura['fecha_cancelada'] = $fecha_cancelada->format ("d-m-Y");
        }

        foreach ($factura['factura_orden_publicacion'] as $key => $publicacion) {
            $fecha = new DateTime($publicacion['fecha_creacion']);
            $factura['factura_orden_publicacion'][$key]['fecha_creacion'] = $fecha->format ("d-m-Y");
        }

//            $fecha = new DateTime($factura['fecha_anulacion']);
//            $factura['fecha_anulacion'] = $fecha->format ("d-m-Y");

        foreach ($factura['factura_orden_publicacion'] as $publicacion) {
            $total = $total + $publicacion['total'];
            $subtotal = $subtotal + $publicacion['subtotal'];
            $iva = $total - $subtotal ;
            $count++;
        }

        // datos para la factura por cuota
        foreach ($factura['factura_contrato_cuotas'] as $key => $cuota) {
            $fecha = new DateTime($cuota['fecha']);
            $factura['factura_contrato_cuotas'][$key]['fecha'] = $fecha->format ("d-m-Y");
        }

        foreach ($factura['factura_contrato_cuotas'] as $key => $cuota) {
            $subtotal = $subtotal + $cuota['monto'];
            $iva = $iva + $cuota['monto_iva'];
            $total = $subtotal + $iva;
            $count++;
        }

        return response ()->json (['status' => 'ok', 'data' => $factura, 'totales' => array('total' => $total, 'subtotal' => $subtotal, 'iva' => $iva)], 200);


    }

    /**
     * @param Request $request
     * @param $factura_id
     * @param $status
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateStatus (Request $request, $factura_id, $status)
    {
        $factura = Factura::find ($factura_id);
        // dd($factura['fecha_motorizado']);

        switch ($status) {

            case "motorizado":
                $factura->fecha_motorizado = $request->input ('fechaMotorizado');
                $factura->estatus = 'entregado al mensajero';
                break;
            case "cliente":
                $factura->fecha_cliente = $request->input ('fechaCliente');
                $factura->estatus = 'entregado cliente';
                break;
            case "cancelada":
                $factura->fecha_cancelada = $request->input ('fechaCancelada');
                $factura->estatus = 'pagada';
                break;
        }
        $factura->save ();
        $factura = Factura::find ($factura_id);

        if (!empty($factura['fecha_motorizado'])) {
            $fecha_motorizado = new DateTime($factura['fecha_motorizado']);
            $factura['fecha_motorizado'] = $fecha_motorizado->format ("d-m-Y");
        }
        if (!empty($factura['fecha_generada'])) {
            $fecha_generada = new DateTime($factura['fecha_generada']);
            $factura['fecha_generada'] = $fecha_generada->format ("d-m-Y");
        }
        if (!empty($factura['fecha_cliente'])) {
            $fecha_cliente = new DateTime($factura['fecha_cliente']);
            $factura['fecha_cliente'] = $fecha_cliente->format ("d-m-Y");
        }
        if (!empty($factura['fecha_cancelada'])) {
            $fecha_cancelada = new DateTime($factura['fecha_cancelada']);
            $factura['fecha_cancelada'] = $fecha_cancelada->format ("d-m-Y");
        }
        return response ()->json (['status' => 'ok', 'data' => $factura], 200);
    }

    /**
     *
     */
    public function excel ()
    {
        Excel::create ('Reporte', function ($excel) {
            $excel->setDescription ('A demonstration to change the file properties');

            //$sheet->setPageMargin(0.25);
            $excel->sheet ('Publicaciones', function ($sheet) {

                $publicaciones = Factura::all ();
                $sheet->prependRow (array(
                    'prepended', 'prepended'
                ));
                $sheet->freezeFirstRow ();
                $sheet->setPageMargin (array(0.25, 0.30, 0.25, 0.30));
                /*$sheet->setStyle(array(
                    'font' => array(
                        'name'      =>  'Calibri',
                        'size'      =>  15,
                        'bold'      =>  true
                    )
                ));*/
                $sheet->cell ('A1:L1', function ($cells) {

                    $cells->setBorder (array(
                        'borders' => array(
                            'top' => array(
                                'style' => 'solid'
                            ),
                        )
                    ));
                    $cells->setBackground ('#D34332');

                });
                $sheet->fromArray ($publicaciones);

            });
            $excel->sheet ('Medios', function ($sheet) {

                $medios = Medio::all ();

                $sheet->fromArray ($medios);

            });
        })->export ('xls');
    }

    /**
     * Listado Genérico de las facturas de los clientes
     *
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexDtAll ()
    {
        $facturasArray = Cliente::factura_clientes_todas ();
        //Puesto que el método estático factura() retorna un arreglo se debe crear un objeto Collection a partir de él

        $facturas = new Collection;

        foreach ($facturasArray as $key => $row):
            $fecha1 = new DateTime($row->created_at);
            $row->created_at = $fecha1->format ("d-m-Y");
            $fecha2 = new DateTime($row->updated_at);
            $row->updated_at = $fecha2->format ("d-m-Y");
            $facturas->push ([
                'id' => $row->id,
                'created_at' => $row->created_at,
                'codigo' => $row->codigo,
                'updated_at' => $row->updated_at,
                'cliente_codigo' => $row->cliente_codigo,
                'razon_social' => $row->razon_social
            ]);
        endforeach;
        return Datatables::of ($facturas)
            ->make (true);
    }

    /**
     * @param $factura_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function clientFactura ($factura_id)
    {
        $factura = Factura::with ('publicacion', 'publicacion.medio', 'cuota', 'cuota.contrato')->find ($factura_id)->toArray ();
        return response ()->json (['status' => 'ok', 'data' => $factura], 200);
    }

    /**
     * Retorna un listado con todas las cuotas que aun no han sido facturadas.
     *
     * @param $cliente_id
     * @param $contrato_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Carlos Blanco <cebs923@gmail.com>
     */
    public function cuotaNoFacturadaIndex ($cliente_id, $contrato_id)
    {

        $data = DB::select (DB::raw ("
         SELECT   C.id, C.monto, C.fecha, contrato_id, cliente_id, C.monto_iva, C.numero_cuota
            FROM contratos A, clientes B, contrato_cuotas C
            WHERE
            A.id  = C.contrato_id AND
            A.cliente_id = b.id AND
            B.id = '$cliente_id' AND
            C.contrato_id = '$contrato_id' AND
            C.factura_id is null
            GROUP BY C.id ,A.id, C.monto, C.fecha, contrato_id
	    ORDER BY A.id
       "
        ));

        $data = new Collection($data);
        return Datatables::of ($data)
            ->addColumn ('facturar', function ($publicacion) {
                return '<div class="checkbox">
                 <input type="checkbox" id="chk_' . $publicacion->id . '"
                 name="chk_' . $publicacion->id . '"
                 value="' . $publicacion->id . '"
                 ng-model="checkFactura.itm_' . $publicacion->id . '.chckd"
                 ng-init="checkFactura.itm_' . $publicacion->id . '
                 .valor = ' . $publicacion->id . ';
                  checkFactura.itm_' . $publicacion->id . '
                  .chckd = false;
                   checkFactura.itm_' . $publicacion->id . '
                   .montoiva = ' . $publicacion->monto_iva . ';
                    checkFactura.itm_' . $publicacion->id . '
                  .montototal = ' . $publicacion->monto . ';"
                 checked="checked"
                 ng-click="sumarPublicaciones()"/></div>';
            })
            ->make (true);
    }

    /**
     * Método que anula la factura por cuota
     *
     * @author Carlos blanco <cebs923@gmail.com>
     * @param $factura_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function anularfacturacuota ($factura_id)
    {
        $estatusanulada = GenericConfiguration::where ("_label", "anulada")->pluck ('value');
        $factura = Factura::with (
            'cuota.contrato.cliente'
        )->find ($factura_id)->toArray ();
        //dd($factura);

        foreach ($factura['cuota'] as $key => $cuota):
            $contrato_cuota = ContratoCuotas::find ($cuota['id']);
            $contrato_cuota->factura_id = null;
            $contrato_cuota->save ();

        endforeach;
        $factura = Factura::find ($factura_id);
        $factura->estatus = $estatusanulada;
        $factura->estatus_factura = $estatusanulada;
        $factura->fecha_anulacion = new DateTime();
        $factura->save ();


        $factura['fecha_anulacion'] = $factura->fecha_anulacion->format ("d-m-Y");
        return response ()->json (['status' => 'ok', 'data' => $factura], 200);

    }

    /**
     * Método que anula la factura por publicación
     *
     * @author Carlos Blanco <cebs923@gmail.com>
     * @param $factura_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function anularfacturapublicacion ($factura_id)
    {
        $estatusanulada = GenericConfiguration::where ("_label", "anulada")->pluck ('value');

        $factura = Factura::with (
            'facturaOrdenPublicacion',
            'facturaOrdenPublicacion.medio',
            'facturaOrdenPublicacion.version.servicio'
        )->find ($factura_id)->toArray ();
        // dd($factura);

        foreach ($factura['factura_orden_publicacion'] as $key => $publicacion):
            $objpublicacion = Publicacion::find ($publicacion['id']);
            $objpublicacion->factura_id = null;
            $objpublicacion->save ();

        endforeach;
        $factura = Factura::find ($factura_id);
        $factura->estatus = $estatusanulada;
        $factura->estatus_factura = $estatusanulada;
        $factura->fecha_anulacion = new DateTime();
        $factura->por_facturar = true;
        $factura->save ();


        $factura['fecha_anulacion'] = $factura->fecha_anulacion->format ("d-m-Y");
        return response ()->json (['status' => 'ok', 'data' => $factura], 200);

    }

    public function crearnotaCredito (Request $request, $factura_id, $cliente_id)
    {

        $nombreFirmante = GenericConfiguration::where ("_label", "negociacion_firmante_nombre")->pluck ('value');
        $cargoFirmante = GenericConfiguration::where ("_label", "negociacion_firmante_cargo")->pluck ('value');
        $estatusMontoTotal = GenericConfiguration::where ("_label", "monto_total_nota_de_crédito")->pluck ('value');
        $estatusModificacionMonto = GenericConfiguration::where ("_label", "modificacion_a_monto")->pluck ('value');

        $nota_credito = NotasCredito::create ([
            'nombre_firmante' => $nombreFirmante,
            'cargo_firmante' => $cargoFirmante,
            'monto' => $request->input ('monto'),
            'n_control' => $request->input ('n_control'),
            'cliente_id' => $cliente_id,
            'descripcion' => $request->input ('descripcion'),
            'factura_id' => $factura_id

        ]);
        $nota_creditos = NotasCredito::find($nota_credito->id);
     //   dd($nota_creditos);
        $factura = Factura::with('cuota.contrato', 'facturaOrdenPublicacion.cotizacion')->find ($factura_id);

//dd($factura);
        if ($factura->total_factura == $nota_credito->monto) {

            $factura->estatus = 'Pagada';
            $factura->fecha_motorizado = new DateTime();
            $factura->fecha_cliente = new DateTime();
            $factura->fecha_cancelada = new DateTime();
            $factura->estatus_factura = $estatusMontoTotal;
            $factura->restante = 0;
            $factura->save ();
        } else {
            $factura->estatus_factura = $estatusModificacionMonto;
            $factura->restante = $factura->total_factura - $nota_credito->monto;
            $factura->save ();
        }

        if($factura->tipo_factura == 'publicacion'){

            //Se hace la colsulta y se traen los datos globales y por contrato
            //datos globales
            $where = [ 'cliente_id' => $cliente_id ];
            $datos_globales = Balance::where ($where)->latest ()->first(['saldo','deuda']);
            //datos del contrato
            $where['contrato_id'] = $factura['facturaOrdenPublicacion'][0]['cotizacion']['contrato_id'];
            $datos_contrato = Balance::where ($where)->latest ()->first(['contrato_deuda','contrato_saldo']);

            if ($factura->total_factura == $nota_credito->monto) {

                $nuevoBalance = [

                    'cliente_id' => $cliente_id,
                    'contrato_id' => $factura['facturaOrdenPublicacion'][0]['cotizacion']['contrato_id'],
                    'usuario_id' => 1,//arreglar esta asi por pruebas
                    'deuda' => $datos_globales->deuda - $request->input ('monto'),
                    'contrato_deuda' => $datos_contrato->contrato_deuda - $request->input ('monto'),
                    'contrato_cuota' => $request->input ('monto'),
                    'contrato_saldo' => $datos_contrato->contrato_saldo,
                    'nota_credito_id' => $nota_creditos->id,
                    'saldo' => $datos_globales->saldo,
                    'factura_id'=> $factura['id'],
                    'descripcion' => 'Factura '.$factura->codigo . ' Pagada por la nota de credito ' . $nota_creditos->codigo,
                ];
            }else{

                $nuevoBalance = [

                    'cliente_id' => $cliente_id,
                    'contrato_id' => $factura['facturaOrdenPublicacion'][0]['cotizacion']['contrato_id'],
                    'usuario_id' => 1,//arreglar esta asi por pruebas
                    'deuda' => $datos_globales->deuda - $request->input ('monto'),
                    'contrato_deuda' => $datos_contrato->contrato_deuda - $request->input ('monto'),
                    'contrato_cuota' => $request->input ('monto'),
                    'contrato_saldo' => $datos_contrato->contrato_saldo,
                    'nota_credito_id' => $nota_creditos->id,
                    'saldo' => $datos_globales->saldo,
                    'factura_id'=> $factura['id'],
                    'descripcion' => 'Factura '.$factura->codigo . ' Ajustata por la nota de credito ' . $nota_creditos->codigo,
                ];

            }

            Balance::create ($nuevoBalance);

        }elseif($factura->tipo_factura == 'cuota'){


            //Se hace la colsulta y se traen los datos globales y por contrato
            //datos globales

            $where = [ 'cliente_id' => $cliente_id ];
            $datos_globales = Balance::where ($where)->latest ()->first(['saldo','deuda']);
            //datos del contrato
            $where['contrato_id'] = $factura['cuota'][0]['contrato_id'];
            $datos_contrato = Balance::where ($where)->latest ()->first(['contrato_deuda','contrato_saldo']);

            if ($factura->total_factura == $nota_credito->monto) {

                $nuevoBalance = [

                    'cliente_id' => $cliente_id,
                    'contrato_id' => $factura['cuota'][0]['contrato_id'],
                    'usuario_id' => 1,//arreglar esta asi por pruebas
                    'deuda' => $datos_globales->deuda - $request->input ('monto'),
                    'contrato_deuda' => $datos_contrato->contrato_deuda - $request->input ('monto'),
                    'contrato_cuota' => $request->input ('monto'),
                    'contrato_saldo' => $datos_contrato->contrato_saldo,
                    'nota_credito_id' => $nota_creditos->id,
                    'saldo' => $datos_globales->saldo,
                    'factura_id'=> $factura['id'],
                    'descripcion' => 'Cuota # '.$factura['cuota'][0]['numero_cuota'].' del contrato ' . $factura['cuota'][0]['contrato']['codigo']. ' Pagada por la nota '.$nota_creditos->codigo,
                ];
            }else{
               // dd(6);
                $nuevoBalance = [

                    'cliente_id' => $cliente_id,
                    'contrato_id' => $factura['cuota'][0]['contrato_id'],
                    'usuario_id' => 1,//arreglar esta asi por pruebas
                    'deuda' => $datos_globales->deuda - $request->input ('monto'),
                    'contrato_deuda' => $datos_contrato->contrato_deuda - $request->input ('monto'),
                    'contrato_cuota' => $request->input ('monto'),
                    'contrato_saldo' => $datos_contrato->contrato_saldo,
                    'nota_credito_id' => $nota_creditos->id,
                    'saldo' => $datos_globales->saldo,
                    'factura_id'=> $factura['id'],
                    'descripcion' => 'Cuota # '.$factura['cuota'][0]['numero_cuota'].' del contrato ' . $factura['cuota'][0]['contrato']['codigo']. ' Ajustada por la nota '.$nota_creditos->codigo,
                ];
            }
            Balance::create ($nuevoBalance);
        }

        $nota_credito['created_at'] = $nota_credito->created_at->format ("d-m-Y");
        return response ()->json (['status' => 'ok', 'data' => $nota_credito], 200);
    }

   public function datos_factura_nota ($cliente_id, $factura_id )
    {
     $factura_con_nota = Factura::with('
     nota
     ')->find($factura_id);

        return response ()->json (['status' => 'ok', 'data' => $factura_con_nota], 200);
    }

    public function noFacturadoIndex ($cliente_id, $contrato_id){

 $cliente = Cliente::find($cliente_id);
        $publicaciones = $cliente->publicacion ()->select (
            'ordenes_publicacion_estatus.estatus',
            'ordenes_publicacion.*',
            'ordenes_publicacion_versiones',
            'ordenes_publicacion_versiones.version_nombre',
            'ordenes_publicacion_versiones.valor_con_recargo',
            'servicios.nombre'
        )->with (['medio', 'version'])
            ->join ('ordenes_publicacion_estatus_estatus', 'ordenes_publicacion_estatus_estatus.orden_publicacion_id', '=', 'ordenes_publicacion.id')
            ->join ('ordenes_publicacion_versiones', 'ordenes_publicacion_versiones.orden_publicacion_id', '=', 'ordenes_publicacion.id')
            ->join ('servicios', 'servicios.id', '=', 'ordenes_publicacion_versiones.servicio_id')
            ->join ('ordenes_publicacion_estatus', 'ordenes_publicacion_estatus.id', '=', 'ordenes_publicacion_estatus_estatus.orden_publicacion_estatus_id')
            ->join ('cotizaciones', 'cotizaciones.id', '=', 'ordenes_publicacion.cotizacion_id')
            ->join ('contratos', 'contratos.id', '=', 'cotizaciones.contrato_id')
            ->join ('clientes', 'clientes.id', '=', 'contratos.cliente_id')
            ->where('contratos.id',$contrato_id)
            ->where('ordenes_publicacion.factura_id',null)
            ->whereRaw ("ordenes_publicacion_versiones.publicado = '1' and ordenes_publicacion_estatus_estatus.orden_publicacion_estatus_id IN (
                               SELECT id
                  FROM ordenes_publicacion_estatus
                  WHERE  estatus ='Publicada'
            )")
            ->get();
 //dd($publicaciones);



        foreach ($publicaciones as $publicacion) {
            $publicacion['importe'] = $publicacion['subtotal'] - $publicacion['total_impuesto_avp'];
            $publicacion['importe_unformated'] = $publicacion['subtotal'] - $publicacion['total_impuesto_avp'];
       //     $publicacion['importe'] =$publicacion['importe'] + $publicacion[''];
            $publicacion['importe'] = number_format ($publicacion['importe'], 2, ',', '.');
        }

        $data = new Collection($publicaciones);
        return Datatables::of ($data)
            ->addColumn ('facturar', function ($publicacion) {
               // dd($publicacion);
                return '<div class="checkbox">
                 <input type="checkbox" id="chk_' . $publicacion->id . '"
                 name="chk_' . $publicacion->id . '"
                 value="' . $publicacion->id . '"
                 ng-model="checkFactura.itm_' . $publicacion->id . '.chckd"
                 ng-init="checkFactura.itm_' . $publicacion->id . '.valor = ' . $publicacion->id . '; checkFactura.itm_' . $publicacion->id . '.chckd = false; checkFactura.itm_' . $publicacion->id . '.totalpub = ' . $publicacion->total . '; checkFactura.itm_' . $publicacion->id . '.subtotalpub = ' . $publicacion->subtotal . '; checkFactura.itm_' . $publicacion->id . '.importe =' . $publicacion->importe_unformated . '; checkFactura.itm_' . $publicacion->id . '.total_impuesto_avp = ' . $publicacion->total_impuesto_avp . '"
                 checked="checked"
                 ng-click="sumarPublicaciones()"/></div>';
            })
            ->make (true);

    }

    /**
     * Retorna un listado con todas las publicaciones que aun no han sido facturadas.
     *
     * @param $cliente_id
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function datos_x_version ($cliente_id)
    {
        $cliente = Cliente::find ($cliente_id);
        $publicaciones = $cliente->publicacion ()->select (
            'ordenes_publicacion_estatus.estatus',
            'ordenes_publicacion.*'
        )->with (['medio', 'version'])
            ->join ('ordenes_publicacion_estatus_estatus', 'ordenes_publicacion_estatus_estatus.orden_publicacion_id', '=', 'ordenes_publicacion.id')
            ->join ('ordenes_publicacion_estatus', 'ordenes_publicacion_estatus.id', '=', 'ordenes_publicacion_estatus_estatus.orden_publicacion_estatus_id')
            ->whereRaw ("ordenes_publicacion.por_facturar = true and ordenes_publicacion_estatus_estatus.orden_publicacion_estatus_id IN (
                               SELECT id
                  FROM ordenes_publicacion_estatus
                  WHERE  estatus IN ('Publicada', 'Aprobada')
            )")
            ->get ();

        foreach ($publicaciones as $publicacion) {
            $publicacion['importe'] = $publicacion['subtotal'] - $publicacion['total_impuesto_avp'];
            $publicacion['importe_unformated'] = $publicacion['subtotal'] - $publicacion['total_impuesto_avp'];
            $publicacion['importe'] = number_format ($publicacion['importe'], 2, ',', '.');
        }

        $data = new Collection($publicaciones);
        return Datatables::of ($data)
            ->addColumn ('facturar', function ($publicacion) {
                return '<div class="checkbox">
                 <input type="checkbox" id="chk_' . $publicacion->id . '"
                 name="chk_' . $publicacion->id . '"
                 value="' . $publicacion->id . '"
                 ng-model="checkFactura.itm_' . $publicacion->id . '.chckd"
                 ng-init="checkFactura.itm_' . $publicacion->id . '.valor = ' . $publicacion->id . '; checkFactura.itm_' . $publicacion->id . '.chckd = true; checkFactura.itm_' . $publicacion->id . '.totalpub = ' . $publicacion->total . '; checkFactura.itm_' . $publicacion->id . '.subtotalpub = ' . $publicacion->subtotal . '; checkFactura.itm_' . $publicacion->id . '.importe =' . $publicacion->importe_unformated . '; checkFactura.itm_' . $publicacion->id . '.total_impuesto_avp = ' . $publicacion->total_impuesto_avp . '"
                 checked="checked"
                 ng-click="sumarPublicaciones()"/></div>';
            })
            ->make (true);

    }

    public function destroy($factura_id)
    {
        $factura = Factura::find($factura_id);
        if (!$factura) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra una factura con ese código.'], 404);
        }
        $factura->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado la factura correctamente.'], 200);
    }

}