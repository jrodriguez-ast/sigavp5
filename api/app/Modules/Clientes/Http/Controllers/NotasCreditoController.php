<?php namespace App\Modules\Clientes\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Acl\User;
use App\Models\Cliente;
use App\Models\NotasCredito;
use App\Models\Utilities\GenericConfiguration;
use DateTime;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;


class NotasCreditoController extends Controller
{

    /**
     * Retorna los datos de las notas de crédito
     * @param $cliente_id
     * @param $nota_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show($cliente_id, $nota_id)
    {

        $notaCredito = NotasCredito::with(
            'factura'
        )->find($nota_id)->toArray();

        $fecha_creacion = new DateTime($notaCredito['created_at']);
        $notaCredito['created_at'] = $fecha_creacion->format("d-m-Y");

        $fecha_motorizado = new DateTime($notaCredito['fecha_motorizado']);
        $notaCredito['fecha_motorizado'] = $fecha_motorizado->format("d-m-Y");

        $fecha_generada = new DateTime($notaCredito['fecha_cliente']);
        $notaCredito['fecha_cliente'] = $fecha_generada->format("d-m-Y");

        return response()->json(['status' => 'ok', 'data' => $notaCredito], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param         $idcliente
     * @return Response
     */
    public function store(Request $request, $idcliente)
    {


        $cliente = Cliente::find($idcliente);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        // dd($cliente);

        $nombreFirmante = GenericConfiguration::where("_label", "negociacion_firmante_nombre")->pluck('value');
        $cargoFirmante = GenericConfiguration::where("_label", "negociacion_firmante_cargo")->pluck('value');

        $nuevaNota = array(
            'descripcion' => $request->input('descripcion'),
            'nombre_firmante' => $nombreFirmante,
            'cargo_firmante' => $cargoFirmante,
            'monto' => $request->input('monto'),
            'n_control' => $request->input('n_control'),
            'cliente_id' => $idcliente,
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $nuevo = array_merge($nuevaNota, $row);

        $nuevoNota = $cliente->nota()->create($nuevo);


        return response()->json(['status' => 'ok', 'data' => $nuevoNota], 201);
    }


    /**
     * Listado de Notas de crédito
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex($cliente_id)
    {
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $notasCredito = $cliente->nota()->get();
        $data = new Collection($notasCredito);
        return Datatables::of($data)->make(true);

    }//

    /**
     * @param Request $request
     * @param $nota_id
     * @param $status
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function updateStatus(Request $request, $nota_id, $status)
    {
        $nota = NotasCredito::find($nota_id);
        // dd($factura['fecha_motorizado']);

        switch ($status) {
            case "motorizado":
                $nota->fecha_motorizado = $request->input('fechaMotorizado');
                $nota->estatus = 'entregado al mensajero';
                break;
            case "cliente":
                $nota->fecha_cliente = $request->input('fechaCliente');
                $nota->estatus = 'entregado cliente';
                break;
        }
        $nota->save();
        $nota = NotasCredito::find($nota_id);

        if (!empty($nota['fecha_motorizado'])) {
            $fecha_motorizado = new DateTime($nota['fecha_motorizado']);
            $nota['fecha_motorizado'] = $fecha_motorizado->format("d-m-Y");
        }
        if (!empty($nota['fecha_cliente'])) {
            $fecha_cliente = new DateTime($nota['fecha_cliente']);
            $nota['fecha_cliente'] = $fecha_cliente->format("d-m-Y");
        }
        return response()->json(['status' => 'ok', 'data' => $nota], 200);
    }
}