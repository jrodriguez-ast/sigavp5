<?php


Route::group(['prefix' => 'clientes'], function () {
    //################################
    // Rutas para el recurso Clientes
    //################################
    Route::get('cliente/dt', [
        'as' => 'clientes.cliente.dtindex', 'uses' => 'ClienteController@dtIndex'
    ]);
    Route::resource('cliente', 'ClienteController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);

    Route::post('guardarLogo', 'ClienteController@guardarImg');

    Route::post('moverLogo/{clientID}', 'ClienteController@mover');
    //################################
    // Rutas para el recurso Contactos
    //################################


    Route::resource('contacto', 'ContactoController', ['only' => ['index', 'show']]);
    //################################
    // Rutas para el recurso anidado clientes.contactos
    //################################

    Route::get('{idcliente}/contacto/dt', [
        'as' => 'cliente.contacto.dtindex', 'uses' => 'ClienteContactoController@dtIndex'
    ]);
    Route::resource('cliente.contacto', 'ClienteContactoController', ['except' => ['edit', 'create']]);

    Route::get('referencia/{texto?}', 'ClienteController@referencia');
    Route::get('data/{id}', 'ClienteController@data');
    //################################
    // Rutas para el recurso Contrato
    //################################
    Route::resource('contratos', 'ContratoController', ['only' => ['index', 'show', 'update']]);

    Route::get('contratoDt', [
        'uses' => 'ContratoController@dtIndex'
    ]);
    Route::resource('cliente.contratos', 'ClienteContratosController', ['only' => ['index', 'show', 'update', 'store']]);

    Route::get('contratoClienteDt/{cliente}', [
        'as' => 'cliente.contratoclientedt', 'uses' => 'ClienteContratosController@dtIndex'
    ]);
    Route::get('contratos_activos/{cliente}', [
        'as' => 'cliente.contratos_activos', 'uses' => 'ClienteContratosController@activosIndex'
    ]);
    Route::get('tipos_contratos', [ 'uses' => 'ClienteContratosController@listTiposContratos']);
    Route::get('list_tipo_contrato/{id_cliente}', [ 'uses' => 'ClienteContratosController@listTipoContrato']);
    //################################
    // Rutas para el recurso BALANCE
    //################################
    Route::resource('cliente.balance', 'ClienteBalanceController', ['only' => ['index']]);

    Route::get('balanceClienteDt/{cliente}/{contrato_id?}', 'ClienteBalanceController@dtIndex');
    Route::get('balancexTipoDt/{cliente}/{contrato_id?}', 'ClienteBalanceController@balancexTipodt');
    Route::get('{cliente}/saldo/{contrato_id?}', 'ClienteController@saldoActual');
    Route::get('{cliente}/saldoxtipo/{contrato_id?}', 'ClienteBalanceController@saldoActualxTipo');
    Route::get('balance/{balance}', [
        'as' => 'cliente.balance', 'uses' => 'ClienteBalanceController@show'
    ]);
    Route::put('balance/{balance}', [
        'as' => 'cliente.balance.update', 'uses' => 'ClienteBalanceController@update'
    ]);

    Route::get('rif/{rif}', 'ClienteController@getByRif');

    //################################
    // Rutas para el recurso FACTURACION
    //################################

    Route::post('cliente/facturar', [
        'as' => 'facturar', 'uses' => 'ClienteFacturaController@facturar'
    ]);
    Route::post('cliente/facturarcuota', [
        'as' => 'facturar', 'uses' => 'ClienteFacturaController@facturarcuota'
    ]);
    Route::get('{cliente}/public_nofact/{contrato?}', [
        'as' => 'cliente.factura', 'uses' => 'ClienteFacturaController@noFacturadoIndex'
    ]);
    Route::get('{cliente}/public_cuotanofact/{contrato_id}', 'ClienteFacturaController@cuotaNoFacturadaIndex');

    Route::get('{cliente}/total', [
        'as' => 'cliente.factura.total', 'uses' => 'ClienteFacturaController@totalFactura'
    ]);
    Route::get('{cliente}/facturas_del_cliente', [
        'as' => 'cliente.factura.facturas_del_cliente', 'uses' => 'ClienteFacturaController@facturas_del_cliente'
    ]);
    Route::get('{cliente}/nota/{nota}','NotasCreditoController@show'
    );
    Route::get('cliente/{cliente}/facturas/{factura}','ClienteFacturaController@datos_factura_nota'
    );
    Route::get('{cliente}/facturas/{factura}', [
        'as'=>'cliente.factura.show','uses' => 'ClienteFacturaController@show'
    ]);
    Route::put('facturas/{factura}/{estado}', [
        'as'=>'cliente.factura.updatestatus','uses' => 'ClienteFacturaController@updateStatus'
    ]);
    Route::put('nota/{nota}/{estado}','NotasCreditoController@updateStatus'
    );
    Route::get('listado_facturas_todas', [
        'as'=>'cliente.factura.indexDtAll','uses' => 'ClienteFacturaController@indexDtAll'
    ]);
    Route::get('getClienteId/{factura}', [
        'as'=>'cliente.factura.clientfactura','uses' => 'ClienteFacturaController@clientFactura'
    ]);
    Route::put('cliente/anularfacturacuota/{factura}', 'ClienteFacturaController@anularfacturacuota'
    );
    Route::put('cliente/crearnotaCredito/{factura}/{cliente}', 'ClienteFacturaController@crearnotaCredito'
    );
    Route::put('cliente/anularfacturapublicacion/{factura}', 'ClienteFacturaController@anularfacturapublicacion'
    );
    //################################
    // Rutas para el recurso excel
    //################################
    Route::get('publicaciones/excel', [
        'as'=>'cliente.publicaciones.excel','uses' => 'ClienteFacturaController@excel'
    ]);
    //################################
    // Rutas para pagos
    //################################
    Route::resource('cliente.pagos', 'PagosController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);
    Route::get('{cliente}/listadofacturaporcobrar', 'PagosController@listadofacturaporcobrar');
    Route::get('{cliente}/factpagadasIndex', 'PagosController@facturasPagadasIndex');
    Route::get('{cliente_id}/techo_historial', 'ClienteController@techo_historial');
    Route::put('{cliente_id}/update_techo', 'ClienteController@update_techo');

    //################################
    // Rutas para nota de credito
    //################################
    Route::resource('cliente.nota_credito', 'NotasCreditoController');
    Route::get('{cliente}/dt', 'NotasCreditoController@dtIndex');

});