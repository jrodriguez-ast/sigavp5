<?php
namespace App\Modules\Medios\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class MediosServiceProvider extends ServiceProvider
{
    /**
     * Register the Medios module service provider.
     *
     * @return void
     */
    public function register()
    {
        // This service provider is a convenient place to register your modules
        // services in the IoC container. If you wish, you may make additional
        // methods or service providers to keep the code more focused and granular.
        App::register('App\Modules\Medios\Providers\RouteServiceProvider');

        $this->registerNamespaces();
    }

    /**
     * Register the Medios module resource namespaces.
     *
     * @return void
     */
    protected function registerNamespaces()
    {
        Lang::addNamespace('medios', realpath(__DIR__ . '/../Resources/Lang'));

        View::addNamespace('medios', realpath(__DIR__ . '/../Resources/Views'));
    }
}
