<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
 */
Route::group(['prefix' => 'medios'], function () {
    // Recursos para Medios
    Route::get('referencia/{texto?}', 'MediosController@referencia');
    Route::get('listado/{tipoPublicacionId}', 'MediosController@listado');
    Route::get('dt', 'MediosController@dtIndex');
    Route::get('rif/{rif}', 'MediosController@getByRif');
    Route::get('{id}/children', 'MediosController@getChildren');
    Route::resource('medios', 'MediosController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);

    Route::post('guardarLogo', 'MediosController@guardarImg');
    Route::post('moverLogo/{clientID}', 'MediosController@mover');

    // Recursos para Contactos relacionados al Medios
    Route::get('{medio}/contactos/dt', 'MediosContactosController@dtIndex');
    Route::resource('medio.contacto', 'MediosContactosController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);

    // Recursos para los servicios del medio.
    Route::get('{medioId}/{tipoPublicacionId}/servicios/listado/{modo?}/{servicioPadre?}/{servicioHijo?}', 'ServicioController@listado');
    Route::get('{medio}/servicios/dt', 'MediosServiciosController@dtServices');
    Route::resource('medio.servicio', 'MediosServiciosController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);

    // Recursos para las condiciones de los servicios pertenecientes a los medios
    Route::get('servicio/{servicio}/hasbase', 'MediosServiciosCondicionesController@hasBaseCondition');
    Route::get('servicio/{servicio}/children', 'MediosServiciosController@getChildren');
    Route::delete('medio/{medio_id}/servicio/{servicio_id}/condiciones/{condicion_id}/{desctivar?}', [
        'uses' => 'MediosServiciosCondicionesController@borrar',
    ]);
    Route::patch('medio/{medio_id}/servicio/{servicio_id}/condiciones/{condicion_id}', 'MediosServiciosCondicionesController@activate');
    Route::resource('medio.servicio.condiciones', 'MediosServiciosCondicionesController', ['only' => ['store', 'show', 'update']]);

    // Recursos para negociaciones del medio
    Route::get('{medio}/negociaciones/dtx', 'MediosNegociacionesController@dtIndex');
    Route::resource('medio.negociaciones', 'MediosNegociacionesController', ['only' => ['index', 'store', 'show', 'destroy', 'update']]);
    Route::get('negociaciones/servicio/{servicio_id}', 'MediosNegociacionesController@getNegociacionServicio');
    Route::get('negociaciones/condicion/{condicion_id}', 'MediosNegociacionesController@getNegociacionCondicion');
    Route::get('negociaciones/tarifa/{tarifa_id}', 'MediosNegociacionesController@getNegociacionTarifa');

    //################################
    // Rutas para el recurso BALANCE
    //################################
    Route::resource('medio.balance', 'MedioBalanceController', ['only' => ['index']]);

    Route::get('balanceMedioDt/{medio}', [
        'as' => 'medio.balanceclientedt', 'uses' => 'MedioBalanceController@dtIndex'
    ]);
    Route::get('historicoTarifasDetallesCondicionMedioDt/{medio}', [
        'as' => 'medio.historicodetallesdt', 'uses' => 'MediosServiciosCondicionesController@dtHistoricoTarifasIndex'
    ]);
    Route::get('{medio}/saldo', [
        'as' => 'medio.saldo', 'uses' => 'MedioBalanceController@saldoActual'
    ]);
    Route::get('balance/{balance}', [
        'as' => 'medio.balance', 'uses' => 'MedioBalanceController@show'
    ]);
    Route::put('balance/{balance}', [
        'as' => 'medio.balance.update', 'uses' => 'MedioBalanceController@update'
    ]);
    //################################
    // Rutas para el Tipo de Medio
    //################################
    Route::get('tipo', [
        'as' => 'medio.tipo', 'uses' => 'TipoMedioController@index'
    ]);
    //################################
    // Cadenas
    //################################
    Route::get('cadenas', [
        'as' => 'medios.cadenas', 'uses' => 'MediosController@listarCadenas'
    ]);
    /*Route::get('{medioID}/{tipoPub}/{servicioID}/servicios', [
        'as' => 'medios.servicios', 'uses' => 'MediosController@getMedioServices'
    ]);*/
    Route::get('{medioID}/{tipoPub}/servicios', [
        'as' => 'medios.servicios', 'uses' => 'MediosController@getMedioServices'
    ]);
    Route::get('{medioID}/{tipoPub}/cadenas', [
        'as' => 'medios.servicios', 'uses' => 'MediosController@getCadenas'
    ]);
    Route::get('service/listchildren/{serviceID}', [
        'as' => 'medios.servicios', 'uses' => 'MediosServiciosController@listChildren'
    ]);
});
