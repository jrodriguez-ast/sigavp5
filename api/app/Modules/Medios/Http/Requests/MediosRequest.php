<?php namespace App\Modules\Medios\Http\Requests;

use App\Modules\Medios\Http\Requests;


class MediosRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'razon_social' => 'required',
            'rif' => 'required',
            //'fecha_fundacion' => 'required',
            'direccion_fiscal' => 'required',
        ];
    }

    /**
     * En la función response se devuelve un error 422 donde dice los campos requeridos según el rules
     * @param array $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return response()->json(array('errors' => array(['code' => 422, 'message' => $errors])), 422);
    }
}

