<?php namespace App\Modules\Medios\Http\Requests;

use App\Http\Requests\Request;

class NegociacionRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = ['fecha' => 'required|date'];

        if ($this->input('con_contrato')) {
            $rules['tarifa'] = 'required|numeric';
            $rules['techo_consumo'] = 'numeric';
        }

        if ($this->input('con_condiciones')) {
            $rules['acuerdos'] = 'required|array';
            foreach ($this->input('acuerdos') as $key => $value) {
                $rules['acuerdos.' . $key . '.to'] = 'required|in:Servicio,Condicion,Detalle';
                $rules['acuerdos.' . $key . '.exonerado'] = 'boolean';

                if ($value['to'] == 'Servicio') {
                    // Sustituye no existe para este comportamiento
                    // $rules['acuerdos.' . $key . '.detalle.sustituye'] = 'required|in:false';

                    $rules['acuerdos.' . $key . '.monto'] = 'required|numeric';
                    $rules['acuerdos.' . $key . '.aplicado_en'] = 'required|boolean|in:1';
                }

                if ($value['to'] == 'Condicion') {
                    // Sustituye no existe para este comportamiento
                    // $rules['acuerdos.' . $key . '.detalle.sustituye'] = 'required|in:false';

                    $rules['acuerdos.' . $key . '.monto'] = 'required|in:0';
                    $rules['acuerdos.' . $key . '.aplicado_en'] = 'required|in:1';
                }

                if ($value['to'] == 'Detalle') {
                    $rules['acuerdos.' . $key . '.detalle.sustituye'] = 'required|boolean';
                    $rules['acuerdos.' . $key . '.monto'] = 'required|numeric';
                    $rules['acuerdos.' . $key . '.aplicado_en'] = 'required|boolean';
                }
            }
        }
        return $rules;
    }

    /**
     * @param array $errors
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function response(array $errors)
    {
        return response()->json(array('errors' => array(['code' => 422, 'message' => $errors])), 422);
    }

}

/*
{
    "acuerdos": [
        {
            "to": "Servicio",
            "servicio": {
            "id": 86,
                "nombre": "Servicio 1"
            },
            "exonerado": true,
            "aplicado_en": 1,
            "monto": 10
        }
    ],
    "fecha":"2015-07-01T04:30:00.000Z",
    "con_condiciones":true,
    "con_contrato":true,
    "observaciones":"Observaciones",
    "tarifa":"12000",
    "techo_consumo":"150000"
}
*/