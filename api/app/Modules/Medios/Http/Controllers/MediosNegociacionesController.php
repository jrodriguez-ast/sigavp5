<?php
namespace App\Modules\Medios\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\Medio;
use App\Models\Negociaciones;
use App\Models\NegociacionesCondiciones;
use App\Models\NegociacionesCondicionesDetalles;
use App\Models\NegociacionesServicios;
use App\Models\Utilities\GenericConfiguration;
use App\Modules\Medios\Http\Requests\NegociacionRequest;
use Datatables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class MediosNegociacionesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index()
    {
        return response()->json(['status' => 'ok', 'data' => Negociaciones::all()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param NegociacionRequest $request
     * @param $medio_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(NegociacionRequest $request, $medio_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }

//        Ejemplo de l,o que viene de la vista.
//        {
//            "acuerdos":[
//                  {
//                      "to":"Servicio",
//                      "servicio":{"id":86,"nombre":"Servicio 1"},
//                      "condition":null, "detalle":null,
//                      "aplicado_en":1, "monto":15
//                  },
//                  {
//                      "to":"Detalle",
//                      "servicio":{"id":86,"nombre":"Servicio 1"},
//                      "condition":{"id":88,"nombre":"Condicion 1","es_base":"1","es_porcentaje":"0"},
//                      "detalle":{"id":44,"nombre":"Valor","sustituye":true,"tarifa":"3893.00"},
//                      "aplicado_en":0,"monto":100
//                  },
//                  {
//                      "to":"Condicion",
//                      "servicio":{"id":83,"nombre":"Diario"},
//                      "condition":{"id":77,"nombre":"C-S83-2","es_base":"0","es_porcentaje":"0"},
//                      "detalle":null, "aplicado_en":1, "exonerado":true, "monto":0
//                  }
//          ],
//        }

        $negociacion = $medio->negociaciones();
        $user = User::where('username', $request->header('username'))->first(['id']);
        $negociacion = DB::transaction(function () use ($request, $negociacion, $user) {

            $new = array(
                'fecha_negociacion' => $request->input('fecha'),
                'usuario_id' => $user->id,
                'tarifa' => 0,
                'techo_consumo' => 0,
                'estado' => false,
                'observaciones' => ($request->exists('observaciones')) ? $request->input('observaciones') : '',
                'firmante_nombre' => GenericConfiguration::where('_label', 'negociacion_firmante_nombre')->pluck('value'),
                'firmante_cargo' => GenericConfiguration::where('_label', 'negociacion_firmante_cargo')->pluck('value')
            );

            if ($request->input('con_contrato')) {
                $new['tarifa'] = $request->input('tarifa');
                //$new['techo_consumo'] = $request->input('techo_consumo');
            }
            $negociacion = $negociacion->create($new);

            if ($request->input('con_condiciones')) {
                // Pasamos el estado a false de todas las negociaciones registradas.
                Negociaciones::where(['estado' => true, 'medio_id' => $negociacion->medio_id])->update(array('estado' => false));
                NegociacionesServicios::where(['estado' => true, 'medio_id' => $negociacion->medio_id])->update(array('estado' => false));
                NegociacionesCondiciones::where(['estado' => true, 'medio_id' => $negociacion->medio_id])->update(array('estado' => false));
                NegociacionesCondicionesDetalles::where(['estado' => true, 'medio_id' => $negociacion->medio_id])->update(array('estado' => false));

                $negociacion->estado = true;
                $negociacion->save();

                $acuerdos = $request->input('acuerdos');
                foreach ($acuerdos as $acuerdo) {
                    switch ($acuerdo['to']) {
                        case 'Servicio':
                            $negociacion->servicios()->create(array(
                                'usuario_id' => $user->id,
                                'medio_id' => $negociacion->medio_id,
                                'monto' => $acuerdo['monto'],
                                'servicio_id' => $acuerdo['servicio']['id'],
                                'exonerado' => (isset($acuerdo['exonerado']) ? $acuerdo['exonerado'] : false),
                                'es_porcentaje' => $acuerdo['aplicado_en'],
                                'estado' => true,
                            ));
                            break;
                        case 'Condicion':
                            $negociacion->condiciones()->create(array(
                                'usuario_id' => $user->id,
                                'medio_id' => $negociacion->medio_id,
                                'servicio_id' => $acuerdo['servicio']['id'],
                                'condicion_id' => $acuerdo['condition']['id'],
                                'monto' => $acuerdo['monto'],
                                'exonerado' => (isset($acuerdo['exonerado']) ? $acuerdo['exonerado'] : false),
                                'es_porcentaje' => $acuerdo['aplicado_en'],
                                'estado' => true,
                            ));
                            break;
                        case 'Detalle':
                            $negociacion->condicionDetalles()->create(array(
                                'usuario_id' => $user->id,
                                'medio_id' => $negociacion->medio_id,
                                'servicio_id' => $acuerdo['servicio']['id'],
                                'condicion_id' => $acuerdo['condition']['id'],
                                'condicion_detalle_id' => $acuerdo['detalle']['id'],
                                'monto' => $acuerdo['monto'],
                                'exonerado' => (isset($acuerdo['exonerado']) ? $acuerdo['exonerado'] : false),
                                'sustituye' => (isset($acuerdo['detalle']['sustituye']) ? $acuerdo['detalle']['sustituye'] : false),
                                'es_porcentaje' => $acuerdo['aplicado_en'],
                                'estado' => true,
                            ));
                            break;

                        default:
                            break;
                    }
                }
            }
            return $negociacion;
        });
        return response()->json(array('status' => 'ok', 'data' => $negociacion), 201, array('Location' => 'localhost/laravel/api/public/Medios/Medio/{id}/Negociaciones/' . $negociacion->id, 'Content-Type' => 'application/json'));

    }

    /**
     * Display the specified resource.
     *
     * @param $medio_id
     * @param  int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function show($medio_id, $id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }

        $negociacion = $medio->negociaciones()->with([
            'servicios.servicio',
            'condiciones.servicio',
            'condiciones.condicion',
            'condicionDetalles.servicio',
            'condicionDetalles.condicion',
            'condicionDetalles.detalle'
        ])->find($id);

        if (!$negociacion) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra la negociación con ese código.'])], 404);
        }

        $negociacion['medio'] = $medio;
        return response()->json(array('status' => 'ok', 'data' => $negociacion), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Devuelve el objeto Datatables.
     *
     * @param Request $request
     * @param $medio_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @version V-1.0
     * @author Jose Rodriguez
     */
    public function dtIndex(Request $request, $medio_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }

        // dd($request->get('search')['value']);
        $negociacion = $medio->negociaciones()->with([
            'servicios.servicio' => function ($query) {
                $query->addSelect(array('id', 'nombre'));
            },
            'condiciones.condicion.servicio' => function ($query) {
                $query->addSelect(array('id', 'nombre'));
            },
            'condicionDetalles.detalle.condicion.servicio' => function ($query) {
                $query->addSelect(['id', 'nombre']);
            }
        ])->select(['id', 'codigo', 'fecha_negociacion', 'estado'])->orderBy('estado', 'desc')->orderBy('codigo', 'desc');

        return Datatables::of($negociacion)
            ->filter(function ($query) use ($request) {
                $query->where('codigo', 'ilike', "%{$request->get('search')['value']}%");
                // $query->orWhere('fecha_negociacion', 'ilike', "'%{$request->get('search')['value']}%'");
            })
            ->make(true);
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $servicio_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getNegociacionServicio($servicio_id)
    {
        $negociacion = NegociacionesServicios::where('servicio_id', '=', $servicio_id)->get();
        return response()->json(array('status' => 'ok', 'data' => $negociacion), 200);
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $condicion_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getNegociacionCondicion($condicion_id)
    {
        $negociacion = NegociacionesCondiciones::where('condicion_id', '=', $condicion_id)->get();
        return response()->json(array('status' => 'ok', 'data' => $negociacion), 200);
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $tarifa_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getNegociacionTarifa($tarifa_id)
    {
        $negociacion = NegociacionesCondicionesDetalles::where('condicion_id', '=', $tarifa_id)->get();
        return response()->json(array('status' => 'ok', 'data' => $negociacion), 200);
    }
}
