<?php
namespace App\Modules\Medios\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\Condicion;
use App\Models\DetalleCondicion;
use App\Models\Medio;
use App\Models\Servicio;
use Carbon\Carbon;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Database\Eloquent\Collection;

use yajra\Datatables\Datatables;

/**
 * Class MediosServiciosCondicionesController
 * @package App\Modules\Medios\Http\Controllers
 */
class MediosServiciosCondicionesController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $medio_id
     * @param $servicio_id
     * @return Response
     * @throws Exception
     */
    public function store(Request $request, $medio_id, $servicio_id)
    {

        // INICIO(1) Valido la existencia de los recursos padres
        $medio = Medio::find($medio_id);

        if (!$medio) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.']))], 404);
        }

        $servicio = $medio->servicios()->find($servicio_id);
        if (!$servicio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Medio con ese código.'])], 404);
        }
        // FIN(1)

        // INICIO(2) - Valido la información recibida por el server
        $rules = array(
            'nombre' => 'required', //'required|integer',
            'es_base' => 'required|integer',
            'es_porcentaje' => 'required|integer',
            'detalles' => 'required|array',
            'deleted_details' => 'array',
            'username' => 'required',
        );

        $detalles = $request->input('detalles');
        foreach ($detalles as $key => $det) {
            $rules['detalles.' . $key . '.nombre'] = 'required';
            $rules['detalles.' . $key . '.tarifa'] = 'required|numeric';
        }

        $validator = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            $rules
        );

        if ($validator->fails()) {
            return response()->json(array('errors' => array(['code' => 422, 'message' => $validator->errors()])), 422);
        }
        // FIN(2) - Valido la información recibida por el server

        $user = User::where('username', $request->header('username'))->first(['id']);
        DB::beginTransaction(); //Start transaction!
        try {
            $message = "Se agrego la Tarifa satisfactoriamente";
            $condicion = new Condicion;
            $condicion->nombre = $request->input('nombre');
            $condicion->es_porcentaje = $request->input('es_porcentaje');
            $condicion->es_base = $request->input('es_base');
            /**
             *Si no existe ninguna condición base asociada al servicio la nueva se setea como base
             */
            if (count($servicio->condiciones()->where('es_base', '=', 1)->get()) == 0):
                $condicion->es_porcentaje = 0;
                $condicion->es_base = 1;
                $message = "La Tarifa fue agregada como base puesto que no había otra registrada";
            endif;
            /**
             *Si ya existe una condición base y la nueva esta marcada como base se envía un mensaje notificando esto
             * y todas las demas se marcan a 0 (es_base = 0)
             */
            if (count($servicio->condiciones()->where('es_base', '=', 1)->get()) > 0 && $request->input('es_base') == 1):
                $condicion_base = $servicio->condiciones()->where('es_base', '=', 1)->get()->first();
                $obj_condicion = Condicion::find($condicion_base['id']);
                $obj_condicion->es_base = 0;
                $obj_condicion->save();
                $message = "La Tarifa base fue cambiada exitosamente";
            endif;
            $condicion->usuario_id = $user->id;
            $servicio->condiciones()->save($condicion);
            foreach ($request->input('detalles') as &$detalle) {
                $detalleCondicion = new DetalleCondicion;
                $detalleCondicion->nombre = $detalle['nombre'];
                $detalleCondicion->tarifa = $detalle['tarifa'];
                $detalleCondicion->usuario_id = $user->id;
                $detalleCondicion->fecha = $detalle['fecha'];

                $condicion->detalles()->save($detalleCondicion);
            }

            // Más información sobre respuestas en http://jsonapi.org/format/
            // Devolvemos el código HTTP 201 Created – [Creada] Respuesta a un POST
            // que resulta en una creación. Debería ser combinado con un encabezado
            // Location, apuntando a la ubicación del nuevo recurso.
            $response = Response::make(json_encode(array('status' => 'ok', 'data' => $condicion, 'message' => $message)), 201)
                ->header('Location', 'http://sigavp/api/public/medios/{medio_id}/serivio/{servicio_id}/condiciones/' . $condicion->id)
                ->header('Content-Type', 'application/json');
            // Borro la cache del servicios asociado.
            Cache::forget($this->getCacheServices($medio_id, $servicio_id));
        } catch (\Exception $e) {
            DB::rollback();
            $response = response()->json(array('errors' => array(['code' => 422, 'message' => 'Problemas al procesar los datos. Por favor intente de nuevo.'])), 422);
            throw $e;
        }
        DB::commit();

        return $response;
    }

    /**
     * Display the specified resource.
     *
     * @param Integer $medio_id Sin Uso
     * @param Integer $servicio_id Sin Uso
     * @param Integer $condicion_id
     * @return Response
     * @internal param int $id
     */
    public function show($medio_id, $servicio_id, $condicion_id)
    {
        // return "Se muestra Fabricante con id: $id";
        // Buscamos la condición por el id y la cacheamos
        /*$condicion = Cache::remember($this->getCacheNameOne($medio_id, $servicio_id, $condicion_id), 10, function () use ($condicion_id) {
            return Condicion::with('detalles')->find($condicion_id);
        });*/
        $condicion = Condicion::with('detalles')->find($condicion_id);
        // Si no existe ese fabricante devolvemos un error.
        if (!$condicion) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra la Tarifa con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $condicion], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $medio_id
     * @param $servicio_id
     * @param $condicion_id
     * @return Response
     * @throws Exception
     * @internal param int $id
     */
    public function update(Request $request, $medio_id, $servicio_id, $condicion_id)
    {
        // INICIO(1) Valido la existencia de los recursos padres
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.']))], 404);
        }

        $servicio = $medio->servicios()->find($servicio_id);
        if (!$servicio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Medio con ese código.'])], 404);
        }
        /*
         * $condicion_base es la condición base actual
         */
        $condicion_base = $servicio->condiciones()->where('es_base', '=', 1)->get()->first();

        $condicion = $servicio->condiciones->find($condicion_id);
        if (!$condicion) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra la Tarifa con ese código.'])], 404);
        }
        // FIN(1)
        //
        // Validaciones
        $rules = array(
            'nombre' => 'required', //'required|integer',
            'detalles' => 'required|array',
            'es_base' => 'required|integer',
            'es_porcentaje' => 'required|integer',
            'deleted_details' => 'array',
            'username' => 'required',
        );

        $detalles = $request->input('detalles');
        foreach ($detalles as $key => $det) {
            $rules['detalles.' . $key . '.nombre'] = 'required';
            $rules['detalles.' . $key . '.tarifa'] = 'required|numeric';
            $rules['detalles.' . $key . '.fecha'] = 'required';
        }

        $validator = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            $rules
        );

        if ($validator->fails()) {
            return response()->json(array('errors' => array(['code' => 422, 'message' => $validator->errors()])), 422);
        }
        // Fin Validaciones

        $user = User::where('username', $request->header('username'))->first(['id']);

        $condicion->nombre = $request->input('nombre');
        $condicion->es_porcentaje = $request->input('es_porcentaje');
        $condicion->es_base = $request->input('es_base');
        $condicion->usuario_id = $user->id;

        DB::beginTransaction(); //Start transaction!
        try {
            $message = 'Sin detalles adicionales';
            $condicion->save();
            foreach ($detalles as &$detalle) {

                // TimeStamp en JavaScript la fecha supera siempre los 12 caracteres.
                // Por tanto es una nueva condición
                // Caso contrario hablamos de una actualización.
                $detalleCondicion = (strlen($detalle['id']) >= 12)
                    ? $detalleCondicion = new DetalleCondicion
                    : DetalleCondicion::find($detalle['id']);
                $detalleCondicion->nombre = $detalle['nombre'];
                $detalleCondicion->tarifa = $detalle['tarifa'];
                $detalleCondicion->fecha = Carbon::createFromFormat('Y-m-d',$detalle['fecha']);
                $detalleCondicion->principal = $detalle['principal'];
                $detalleCondicion->usuario_id = $user->id;
                /**
                 *Si la que se esta editando fue marcada como base y es diferente a la condición base actual, el atributo es_base
                 * de la condición base actual es pasado a 0
                 */
                if ($request->input('es_base') == 1 && $condicion_base['id'] != $condicion->id):
                    $obj_condicion = Condicion::find($condicion_base['id']);
                    $obj_condicion->es_base = 0;
                    $obj_condicion->es_porcentaje = 0;
                    $obj_condicion->save();
                    $message = "La Tarifa base fue cambiada exitosamente";
                endif;

                $condicion->detalles()->save($detalleCondicion);
            }

            // Pendiente de Colocar condición de eliminación segun pauta o cotizaciones.
            DetalleCondicion::destroy($request->input('deleted_details'));

            // Eliminamos la cache de ese registro.
            Cache::forget($this->getCacheNameOne($medio_id, $servicio_id, $condicion_id));
            Cache::forget($this->getCacheServices($medio_id, $servicio_id));

            $response = response()->json(['status' => 'ok', 'data' => $condicion, 'message' => $message], 200);
        } catch (\Exception $e) {
            DB::rollback();
            $reponse = response()->json(array('errors' => array(['code' => 422, 'message' => 'Problemas al procesar los datos. Por favor intente de nuevo.'])), 422);
            throw $e;
        }
        DB::commit();

        return $response;
    }

    /**
     * Obtiene el nombre de la cache de un servicio de un medio especifico.
     *
     * @param Integer $medio_id ID del Medio
     * @param Integer $servicio_id ID del Servicio
     * @param Integer $condicion_id
     * @return String
     * @version V-1.0 19/06/15 18:30:41
     */
    private function getCacheNameOne($medio_id, $servicio_id, $condicion_id)
    {
        return sprintf('medios-%d-servicios-%d-condiciones-%d', $medio_id, $servicio_id, $condicion_id);
    }

    /**
     * Obtiene el nombre de la cache de un servicio de un medio especifico.
     *
     * @param  Integer $medio_id ID del Medio
     * @param  Integer $servicio_id ID del Servicio
     * @return String
     * @version V-1.0 19/06/15 18:30:41
     */
    private function getCacheServices($medio_id, $servicio_id)
    {
        return sprintf('medios-%d-servicios-%d', $medio_id, $servicio_id);
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $servicio_id
     * @return mixed
     * Retorna true si el servicio ya posee una condición base
     */
    public function hasBaseCondition($servicio_id)
    {
        $servicio = Servicio::find($servicio_id);
        if (!$servicio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Medio con ese código.'])], 404);
        }
        $condicion_base = $servicio->condiciones()->where('es_base', '=', 1)->get()->first();
        if ($condicion_base) {
            return response()->json(['status' => 'ok', 'hasbase' => 'true'], 200);
        } else {
            return response()->json(['status' => 'ok', 'hasbase' => 'false'], 200);
        }
    }

    /**
     * Elimina una condición siempre y cuando no este utilizada. En caso de estar utilizada,
     * solicita confirmación para desactivar.
     *
     * @param $medio_id
     * @param $servicio_id
     * @param $condicion_id
     * @param bool $desactivar
     * @return mixed
     */
    public function borrar($medio_id, $servicio_id, $condicion_id, $desactivar = false)
    {
        $condicion = Condicion::with([
            'detalles.cotizacion_servicio_detalles',
            'negociacion_condicion.negociacion',
            'negociacion_condicion_detalle',
            'publicaciones'
        ])->find($condicion_id)->toArray();

        $tieneCotizacionAsociada = false;
        foreach ($condicion['detalles'] as $row) {
            if (isset($row['cotizacion_servicio_detalles']['id'])) {
                $tieneCotizacionAsociada = true;
                break;
            }
        }

        if ($tieneCotizacionAsociada || !empty($condicion['negociacion_condicion']) ||
            !empty($condicion['negociacion_condicion_detalle']) || !empty($condicion['publicaciones'])
        ) {
            if (!$desactivar) {
                return response()->json([
                    'status' => 'fail',
                    'errors' => [
                        'code' => 423,
                        'message' => 'Esta tarifa, se encuentra utilizada en negociaciones, cotizaciones y/o pautas, por tanto no puede ser borrada.'
                    ]
                ], 423);
            }

            $condicion = Condicion::find($condicion_id);
            $condicion->delete();
            $code = 206; // Desactivado
            $msg = 'Se ha desactivado la tarifa correctamente.';
        } else {
            $code = 204; // Borrado.
            $msg = 'Se ha eliminado la tarifa correctamente.';
            $condicion = Condicion::find($condicion_id);
            $condicion->forceDelete();
        }

        Cache::forget($this->getCacheNameOne($medio_id, $servicio_id, $condicion_id));
        Cache::forget($this->getCacheServices($medio_id, $servicio_id));

        return response()->json(['status' => 'ok', 'code' => $code, 'message' => $msg], 200);
    }

    /**
     * Activa una condición desactivada
     * @param $medio_id
     * @param $servicio_id
     * @param $condicion_id
     * @return mixed
     */
    public function activate($medio_id, $servicio_id, $condicion_id)
    {
        $condicion = Condicion::withTrashed()->find($condicion_id);
        if (!$condicion) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra la Tarifa con ese código.'])], 404);
        }

        $condicion->restore();
        return response()->json(['status' => 'ok', 'code' => 200, 'message' => 'Tarifa Activada satisfactoriamente!!!'], 200);
    }

    /**
     * Descripcion
     *
     * @access Public
     * @param Request $request
     * @param $idmedio Identificador del Medio
     * @return array
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 25/09/15 09:44 AM
     */
    public function dtHistoricoTarifasIndex(Request $request, $idmedio)
    {
        $todos = $request->input('todos');
        $tipo = $request->input('servicio');
        $desde = $request->input('dateStart');
        $hasta = $request->input('dateEnd');
        $medio = Medio::find($idmedio);

        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }

        if ($desde == 'false') {
            $oldest = $medio->DetalleCondicionHistorico()
                ->with([
                    'user',
                    'medio',
                    'servicio',
                    'condicion',
                    'detalle_condicion'
                ])
                ->orderBy('fecha', 'asc')->first(array('fecha'))->toArray();
            $desde = $oldest['fecha'];
        }
        $historico = $medio->DetalleCondicionHistorico()
            ->with([
                'user',
                'medio',
                'servicio',
                'condicion',
                'detalle_condicion'
            ])
            ->orderBy('fecha', 'desc')->whereBetween('fecha', [$desde, $hasta])->get();
        if ($tipo != 0):
            $historico = $medio->DetalleCondicionHistorico()
                ->with([
                    'user',
                    'medio',
                    'servicio',
                    'condicion',
                    'detalle_condicion'
                ])
                ->orderBy('fecha', 'desc')
                ->where('servicio_id', '=', $tipo)
                ->whereBetween('fecha', [$desde, $hasta])
                ->get();
        endif;
        if ($todos == 'true') {
            $historico = $medio->DetalleCondicionHistorico()
                ->with([
                    'user',
                    'medio',
                    'servicio',
                    'condicion',
                    'detalle_condicion'
                ])
                ->orderBy('fecha', 'desc')->get();
        }

        $data = new Collection($historico);

        return Datatables::of($data)
            ->editColumn('fecha', function ($datos) {
                $date = date_create($datos->fecha);
                return date_format($date, 'd/m/Y');
            })
            ->editColumn('user_nombre_apellido', function ($datos) {
                return $datos->user['first_name'] . " " . $datos->user['last_name'];
            })
            ->make(true);
    }
}
// 2015-08-04 12:42:10.000000