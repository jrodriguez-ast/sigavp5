<?php namespace App\Modules\Medios\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\BalanceAvp as Balance;
use App\Models\Medio;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class MedioBalanceController extends Controller
{

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $cliente_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index($cliente_id)
    {
        $medio = Medio::find($cliente_id);

        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $medio->balance()->orderBy('created_at', 'desc')->get()], 200);
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param $balance_id
     * @return \Symfony\Component\HttpFoundation\Response
     * Retorna el balance con los datos del contrato asociado si existiera
     */
    public function show($balance_id)
    {
        $balance = Balance::find($balance_id);
        if (!$balance) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una operación con ese código.']))], 404);
        };
        //Al llamar al atributo se agrega al objeto de balance
        //$balance->contrato;
        return response()->json(array('status' => 'ok', 'data' => $balance, 200));
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param \Illuminate\Http\Request $request
     * @param                          $balance_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function update(Request $request, $balance_id)
    {
        $balance = Balance::find($balance_id);
        if (!$balance) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']))], 404);
        }
        $observaciones = $request->input('observaciones');
        $bandera = false;
        if ($observaciones) {
            $balance->observaciones = $observaciones;
            $bandera = true;
        }
        if ($bandera) {
            $balance->save();
            return response()->json(array('status' => 'ok', 'data' => $balance), 200);
        } else {
            return response()->json(array('status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato de cliente.'])), 304);
        }
    }

    /**
     * @author Maykol Purica <puricamaykol@gmail.com>
     * @param \Illuminate\Http\Request $request
     * @param $medio_id
     * @return \Symfony\Component\HttpFoundation\Response
     * @internal param $idcliente
     */
    public function dtIndex(Request $request, $medio_id)
    {
        $todos = $request->input('todos');
        $tipo = $request->input('tipomovimiento');
        $desde = $request->input('dateStart');
        $hasta = $request->input('dateEnd');
        $reporte = $request->input('reporte');
        $medio = Medio::find($medio_id);

        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }

        if ($desde == 'false') {
            $oldest = $medio->balance()->orderBy('created_at', 'asc')->first(array('created_at'))->toArray();
            $desde = $oldest['created_at'];
        }
        $balance = $medio->balance()->orderBy('created_at', 'desc')->whereBetween('created_at', [$desde, $hasta])->get();
        if ($tipo == 'Cargos'):
            $balance = $medio->balance()
                ->orderBy('created_at', 'desc')
                ->where('cargo', '>', 0.00)
                ->whereBetween('created_at', [$desde, $hasta])
                ->get();
        endif;
        if ($tipo == 'Débitos'):
            $balance = $medio->balance()
                ->orderBy('created_at', 'desc')
                ->where('debito', '>', 0.00)
                ->whereBetween('created_at', [$desde, $hasta])
                ->get();
        endif;
        if ($todos == 'true') {
            $balance = $medio->balance()->orderBy('created_at', 'desc')->get();
        }

        if ($reporte == true) {
            print_r($balance->toArray());
            die;
        }
        $data = new Collection($balance);

        return Datatables::of($data)
            ->editColumn('created_at', function ($datos) {
                $date = date_create($datos->created_at);
                return date_format($date, 'd/m/Y');
            })
            ->make(true);
    }

    public function pre_visualizar_balance($balance = null)
    {
        if (empty($balance))
            return false;

        $iniciales = '';
        if (!empty($cotizacion)):
            $pdf = App::make('dompdf.wrapper');
            if (!empty($cotizacion['user'])):
                $iniciales = $this->get_iniciales_string($cotizacion['user']['first_name'] . " " . $cotizacion['user']['last_name']);
            endif;
            $cotizacion['iniciales'] = $iniciales;
            $cotizacion['novalido'] = true;
            $pdf->loadView($this->PdfCotizacionTemplate, $cotizacion);
            return $pdf->setPaper('a4')
                ->setWarnings(false)
                ->download("DOC_$cotizacion->codigo.pdf");
        else:
            echo "No se pudo obtener el documento";
        endif;
    }

    /**
     * @param $medio_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function  saldoActual($medio_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.']))], 404);
        }
        $data = $medio->balance()->orderBy('created_at', 'desc')->first(array('saldo'));
        //Si el resultado es null se retorna 0 de modo que no haya error en el frontend
        if ($data == null) {
            $data = 0;
        }
        return response()->json(['status' => 'ok', 'data' => $data], 200);
    }

}