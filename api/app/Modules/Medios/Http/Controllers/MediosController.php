<?php namespace App\Modules\Medios\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Acl\User;
use App\Models\Medio;
use App\Models\MediosMedios;
use App\Modules\Medios\Http\Requests\MediosRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Response;
use yajra\Datatables\Datatables;

/**
 * Class MediosController
 * @package App\Modules\Medios\Http\Controllers
 */
class MediosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param Request $request
     * @return Response
     */
    public function index(Request $request)
    {
        $contrato_id = ($request->has('contrato_id')) ? $request->get('contrato_id') : NULL;
        // Medio sin considerar las negociaciones.
        $data = Medio::orderBy('razon_social');

        // Medio considerando las negociaciones.
        if ($request->has('negociacion')) { // OJO Balance.
            $data->whereHas('negociaciones', function ($q) {
                $q->where('tarifa','>',0);
            });
        }

        //Medios Considerando Negociaciones y Contrato del Cliente.
        if (!empty($contrato_id)) {
            $data->whereHas('servicios.tipoPublicacion.contratoPauta', function ($q) use ($contrato_id) {
                $q->where('contrato_id', $contrato_id);
            });
        }

        $data = $data->get(['razon_social', 'id']);
        $respuesta = ['status' => 'ok', 'data' => $data];
        return response()->json([$respuesta], 200);
    }

    /**
     * @return mixed
     */
    public function listarCadenas()
    {
        $respuesta = array(
            'status' => 'ok',
            'data' => DB::SELECT(DB::RAW(
                "SELECT m.id, m.razon_social,m.nombre_comercial, m.tipo_medio
      FROM medios m, tipo_medio tm
      WHERE
      m.tipo_medio = tm.id AND
      tm.nombre <> 'MEDIO'"
            ))
        );

        return response()->json([$respuesta], 200);
    }

    public function dtIndex()
    {
        //$clientes = Cliente::select(array("id", "rif", "razon_social", "telefonos", "correos_electronicos"));
        $medios = Medio::all();//->toArray();
        /* $clientArray = new Collection;
         foreach ($clientes as $key => $value) {
             $clientArray->push([
                 'id' => $value['id'],
                 'rif' => $value['rif'],
                 'razon_social' => $value['razon_social'],
                 'telefonos' => $value['telefonos'],
                 'correos_electronicos' => $value['correos_electronicos'],
             ]);
         }*/
        return Datatables::of($medios)
            ->editColumn('telefonos', function ($data) {
                return json_decode($data->telefonos);
            })
            ->editColumn('correos_electronicos', function ($data) {
                return json_decode($data->correos_electronicos);
            })->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param MediosRequest $request
     * @return Response
     */
    public function store(MediosRequest $request)
    {
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($request->all(), $row);

        if(empty($row['fecha_fundacion'])){
            unset($row['fecha_fundacion']);
        }
        //dd($row);
        $nuevoMedio = Medio::create($row);
        if ($request->input('medio_padre') != null):
            $padres = $request->input('medio_padre');
            $hijo = $nuevoMedio->id;
            foreach ($padres as $key => $padre):
                if ($padre['id'] > 0):
                    MediosMedios::create(
                        [
                            'medio_hijo_id' => $hijo,
                            'medio_padre_id' => $padre['id']
                        ]
                    );
                endif;
            endforeach;
        endif;
        return response()->json(array('status' => 'ok', 'data' => $nuevoMedio), 200);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        $medio = Medio::with(['tipo'])->find($id);
        if (!$medio) {
            // Se devuelve un array errors con los errores encontrados y cabecera HTTP 404.
            // En code podríamos indicar un código de error personalizado de nuestra aplicación si lo deseamos.
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.']))], 404);
        };
        if ($padres = $medio->medioPadre()->get()->toArray()):
            foreach ($padres as $key => $padre):
                $padresArray[] = array(
                    'id' => $padre['medio_padre_id']
                );
            endforeach;
        else:
            $padresArray = array();
        endif;
        $medio_arr = array(
            'razon_social' => $medio->razon_social,
            'rif' => $medio->rif,
            'fecha_fundacion' => $medio->fecha_fundacion,
            'direccion_fiscal' => $medio->direccion_fiscal,
            'telefonos' => $medio->telefonos,
            'correos_electronicos' => $medio->correos_electronicos,
            'sitio_web' => $medio->sitio_web,
            'otros_detalles' => json_decode($medio->otros_detalles),
            'nombre_comercial' => $medio->nombre_comercial,
            'rnc' => json_decode($medio->rnc),
            'solvencia' => json_decode($medio->solvencia),
            'seniat' => json_decode($medio->seniat),
            'img_medio' => $medio->img_medio,
            'tipo' => $medio->tipo,
            'padres' => $padresArray,
        );
        return response()->json(array('status' => 'ok', 'data' => $medio_arr), 200);
    }

    /**
     * @param $rif
     * @return mixed
     */
    public function getByRif($rif)
    {
        $medio = Medio::where('rif', $rif)->get();
        if (count($medio) == 0) {
            return response()->json(array('status' => 'fail'), 200);
        }
        return response()->json(array('status' => 'ok', 'data' => $medio), 200);
    }

    /**
     * @param MediosRequest|Request $request
     * @param $id
     * @return mixed
     */
    public function update(MediosRequest $request, $id)
    {
        $medio = Medio::find($id);
        if (!$medio) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.']))], 404);
        }
        $razon_social = $request->input('razon_social');
        $rif = $request->input('rif');
        $fecha_fundacion = $request->input('fecha_fundacion');
        $direccion_fiscal = $request->input('direccion_fiscal');
        $servicios = $request->input('servicios');
        $otros_canales = $request->input('otros_canales');
        $telefonos = $request->input('telefonos');
        $correos_electronicos = $request->input('correos_electronicos');
        $sitio_web = $request->input('sitio_web');
        $otros_detalles = $request->input('otros_detalles');
        $nombre_comercial = $request->input('nombre_comercial');
        $bandera = false;
        if ($razon_social):
            $medio->razon_social = $razon_social;
            $bandera = true;
        endif;
        if ($rif):
            $medio->rif = $rif;
            $bandera = true;
        endif;
        if ($fecha_fundacion):
            $medio->fecha_fundacion = $fecha_fundacion;
            $bandera = true;
        endif;
        if ($otros_canales):
            $medio->otros_canales = $otros_canales;
            $bandera = true;
        endif;
        if ($direccion_fiscal):
            $medio->direccion_fiscal = $direccion_fiscal;
            $bandera = true;
        endif;
        if ($servicios):
            $medio->servicios = $servicios;
            $bandera = true;
        endif;
        if ($telefonos):
            $medio->telefonos = $telefonos;
            $bandera = true;
        endif;
        if ($correos_electronicos):
            $medio->correos_electronicos = $correos_electronicos;
            $bandera = true;
        endif;
        if ($sitio_web):
            $medio->sitio_web = $sitio_web;
            $bandera = true;
        endif;
        if ($otros_detalles):
            $medio->otros_detalles = $otros_detalles;
            $bandera = true;
        endif;
        if ($nombre_comercial):
            $medio->nombre_comercial = $nombre_comercial;
            $bandera = true;
        endif;
        if ($medio->medioPadre()):
            $padres = $request->input('medio_padre');
            $hijo = $medio->id;
            $medio->medioPadre()->delete();
            foreach ($padres as $key => $padre):
                if ($padre['id'] > 0):
                    MediosMedios::create(
                        [
                            'medio_hijo_id' => $hijo,
                            'medio_padre_id' => $padre['id']
                        ]
                    );
                endif;
            endforeach;
        endif;
        if ($bandera) {
            $user = new User();
            $medio->usuario_id = $user->findByName($request->header('username'), true)->id;
            $medio->save();
            return response()->json(array('status' => 'ok', 'data' => $medio), 200);
        } else {
            return response()->json(array('status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato de Medio.'])), 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $medio = Medio::find($id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'], 404);
        }

        if ($medio->negociaciones()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este proveedor tiene negociaciones asociadas.'], 404);
        }

        if ($medio->balance()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este proveedor tiene balances asociados.'], 404);
        }

        $medio->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el Medio correctamente.'], 200);
    }


    /**
     * @param $tipoPublicacionId
     * @return mixed
     */
    public function listado($tipoPublicacionId)
    {
        return response()->json(Medio::listado($tipoPublicacionId), 200);
    }

    /**
     * Consulta toda la información de medios con sus hijos, nietos y bisnietos
     *
     * @param $medio_id
     * @return mixed
     * @author  Jose Rodriguez
     * @version V-1.0
     */
    public function getChildren($medio_id)
    {
        $respuesta = array(
            'status' => 'ok',
            'data' => Medio::with([
                'servicios' => function ($query) {
                    $query->orderBy('servicios.nombre', 'asc');
                },
                'servicios.condiciones' => function ($query) {
                    $query->orderBy('condiciones.nombre', 'asc');
                },
                'servicios.condiciones.detalles' => function ($query) {
                    $query->orderBy('condiciones_detalles.nombre', 'asc');
                },
                'negociaciones' => function ($query) {
                    $query->where('estado', true);
                }
            ])->find($medio_id),
        );
        return response()->json($respuesta, 200);
    }

    /**
     * Consulta genérica como referencia para autocompletar
     *
     * @param $texto
     * @return json
     * @version V-1.0
     */
    public function referencia($texto = "")
    {
        $medios = new Medio();
        return response()->json(['status' => 'ok', 'data' => $medios->getReferencia($texto)], 200);
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function guardarImg(Request $request)
    {   //Respuesta default
        $data = array('success' => false);

        //obtenemos el campo file definido en el formulario

        $file = $request->file('file');
        //obtenemos el nombre del archivo
        $nombre = str_random() . '.' . $file->getClientOriginalExtension();


        //indicamos que queremos guardar un nuevo archivo en el disco local/tmp
        if (\Storage::disk('tmp')->put($nombre, \File::get($file))):
            $data = array(
                'success' => true,
                'file_name' => $nombre,
                'message' => 'Archivo agregado exitosamente',
            );
            return Response($data, 200);
        endif;

    }

    /**
     * @param Request $request
     * @param $id
     * @return mixed
     */
    public function mover(Request $request, $id)
    {

        $file_name = $request->input('file_name');

        Storage::move('tmp/' . $file_name, 'img_medios/' . $file_name);

        $medio = Medio::find($id);
        if (!$medio) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.']))], 404);
        }

        $medio->img_medio = $file_name;

        $medio->save();
        return response()->json(array('status' => 'ok', 'data' => $medio), 200);

    }

    /**
     * @param $id
     * @return mixed
     */
    public function padres($id)
    {
        $medio = Medio::find($id);
        $padres = $medio->medioPadre();
        return response()->json(array('status' => 'ok', 'data' => $padres), 200);
    }

    /**
     * @param $medioId
     * @param $tipoPub
     * @return mixed
     */
    public function getMedioServices($medioId, $tipoPub)
    {
        $medio = Medio::find($medioId);
        $servicio = $medio->serviciosByTipoPub($tipoPub)
            ->get()->toArray();
        return response()->json($servicio, 200);
    }

    /**
     * @param $medioId
     * @param $tipoPub
     * @return mixed
     */
    public function getCadenas($medioId, $tipoPub)
    {
        $medio = Medio::find($medioId);
        $servicio = $medio->CadenasByTipoPub($tipoPub)
            ->get()->toArray();
        return response()->json($servicio, 200);
    }

}
