<?php namespace App\Modules\Medios\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Servicio;

class ServicioController extends Controller
{
    private $servicioPadre;
    private $servicioHijo;

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    /**
     *
     * @param $medioId
     * @param $tipoPublicacionId
     * @param string $modo
     * @param null $servicioPadre
     * @param null $servicioHijo
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function listado($medioId, $tipoPublicacionId, $modo = '', $servicioPadre = null, $servicioHijo = null)
    {
        $this->servicioPadre = $servicioPadre;
        $this->servicioHijo = $servicioHijo;
        switch ($modo) {
            case 'parent':
                $servicios = Servicio::where('medio_id', $medioId)
                    ->where('tipo_publicacion_id', $tipoPublicacionId)
                    ->doesntHave('servicio_servicio')->get();
                break;

            case 'children':
                $servicios = Servicio::select('nombre', 'public.servicios_servicios.servicios_hijo_id as id', 'public.servicios_servicios.servicios_padre_id as id_padre')->where('medio_id', $medioId)
                    ->leftJoin('public.servicios_servicios', 'public.servicios_servicios.servicios_hijo_id', '=', 'public.servicios.id')
                    ->where('tipo_publicacion_id', $tipoPublicacionId)
//                    ->has('servicio_servicio')->get();
                    ->whereHas('servicio_servicio', function ($q) {
                        if (!empty($this->servicioPadre) && $this->servicioPadre != 'false')
                            $q->where('servicios_padre_id', '=', $this->servicioPadre);
                        if (!empty($this->servicioHijo) && $this->servicioHijo != 'false')
                            $q->where('servicios_hijo_id', '=', $this->servicioHijo);

                    })->get();
                break;

            default:
                $servicios = Servicio::where('medio_id', $medioId)
                    ->where('tipo_publicacion_id', $tipoPublicacionId)->get();
        }

        return response()->json($servicios, 200);
    }
}
