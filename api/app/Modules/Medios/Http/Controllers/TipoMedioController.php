<?php namespace App\Modules\Medios\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\TipoMedio as Tipo;

class TipoMedioController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $tiposMedios = Tipo::all();
        return response()->json(['status' => 'ok', 'data' =>$tiposMedios], 200);
    }
	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

}
