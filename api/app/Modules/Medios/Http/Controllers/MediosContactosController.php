<?php namespace App\Modules\Medios\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Request as Requerido;
use App\Models\Medio;

use App\Models\ContactoMedio as Contacto;


use yajra\Datatables\Datatables;
use App\Models\Acl\User;

class MediosContactosController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param $medio_id
     * @return Response
     */
    public function index($medio_id)
    {
        //session(['variable' => 'maykol']);
        $medio = Medio::find($medio_id);

        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }
        //return session()->get('variable', 'default');
        return response()->json(['status' => 'ok', 'data' => $medio->contactos()->get()], 200);
    }

    /**
     * @param $medio_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex($medio_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }
        $contactos = $medio->contactos()->get();
        //$medios = Medio::select(array("id", "rif", "razon_social", "telefonos", "correos_electronicos"));
        //$medios = Medio::all();
        return Datatables::of($contactos)->editColumn('telefonos', function ($data) {
            return json_decode($data->telefonos);
        })
            ->editColumn('correos_electronicos', function ($data) {
                return json_decode($data->correos_electronicos);
            })
            ->editColumn('interno', function ($data) {
                if ($data->es_interno == true) {
                    return "Si";
                } else {
                    return "No";
                }
            })->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $medio_id
     * @return Response
     */
    public function store(Request $request, $medio_id)
    {
        //$medio = Medio::find($medio_id);
        $user = new User();
        $data_user = $user->findByName(Requerido::header('username'), true);
        foreach ($request->all() as $key => $row) {
            //var_dump($row['medio_id']);
            // var_dump($row);die;
            $row['tipo'] = $row['tipo']['id'];
            $row['cargo_id'] = $row['cargo']['id'];
            $row['medio_id'] = $medio_id;
            $row['telefonos'] = json_encode($row['telefonos']);
            $row['correos_electronicos'] = json_encode($row['correos_electronicos']);


            $row['usuario_id'] = (!empty($data_user) ? $data_user->id : 0);

            $nuevocontacto[] = Contacto::create($row);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param $medio_id
     * @param $contacto_id
     * @return Response
     * @internal param int $id
     */
    public function show($medio_id, $contacto_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }
        $contacto = $medio->contactos()->find($contacto_id);
        if (!$contacto) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un contacto con ese código.'])], 404);
        }
        $contacto['telefonos'] = json_decode($contacto['telefonos']);
        $contacto['correos_electronicos'] = json_decode($contacto['correos_electronicos']);
        // return var_dump($contacto);die;

        $contacto->cargo;

        return response()->json(['status' => 'ok', 'data' => $contacto], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $idmedio
     * @param $idcontacto
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, $idmedio, $idcontacto)
    {
        $medio = Medio::find($idmedio);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }
        $contacto = $medio->contactos()->find($idcontacto);
        if (!$contacto) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Contacto con ese código.'])], 404);
        }
        //var_dump($request);die;
        $nombre = $request->input('nombre');
        $apellido = $request->input('apellido');
        $telefonos = $request->input('telefonos');
        $correos_electronicos = $request->input('correos_electronicos');
        $cargo_id = $request->input('cargo_id');
        $tipo = $request->input('tipo');
        $es_interno = $request->input('es_interno');

        $bandera = false;
        if ($nombre) {
            $contacto->nombre = $nombre;
            $bandera = true;
        }
        if ($apellido) {
            $contacto->apellido = $apellido;
            $bandera = true;
        }
        if ($telefonos) {
            $contacto->telefonos = $telefonos;
            $bandera = true;
        }
        if ($correos_electronicos) {
            $contacto->correos_electronicos = $correos_electronicos;
            $bandera = true;
        }
        if ($cargo_id) {
            $contacto->cargo_id = $cargo_id;
            $bandera = true;
        }
        if ($tipo) {
            $contacto->tipo = $tipo;
            $bandera = true;
        }
        if ($es_interno) {
        $contacto->es_interno = $es_interno;
        $bandera = true;
    }
        if ($bandera) {
            $user = new User();
            $contacto->usuario_id = $user->findByName($request->header('username'), true)->id;
            $contacto->save();
            return response()->json(['status' => 'ok', 'data' => $contacto], 200, array('Location' => 'localhost/laravel/api/public/Medios/Medios/' . $medio->id . '/servicios/' . $contacto->id));
        } else {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato del contacto.'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $idmedio
     * @param $idcontacto
     * @return Response
     * @internal param int $id
     */
    public function destroy($idmedio, $idcontacto)
    {
        $medio = Medio::find($idmedio);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }
        $contacto = $medio->contactos()->find($idcontacto);
        if (!$contacto) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un contacto con ese código.'])], 404);
        }
        $contacto->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el contacto correctamente.'], 200);

    }

}
