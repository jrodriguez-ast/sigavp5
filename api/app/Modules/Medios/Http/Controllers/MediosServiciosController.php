<?php
namespace App\Modules\Medios\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\Medio;
use App\Models\Servicio;
use App\Models\TipoMedio;
use App\Models\ServiciosServicios;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\Validator;
use yajra\Datatables\Datatables;
use Illuminate\Support\Facades\DB;

class MediosServiciosController extends Controller
{
    private $servicioPadre;
    /**
     * Display a listing of the resource.
     *
     * @param $medio_id
     * @return Response
     */
    public function index($medio_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $medio->servicios()->get()], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param         $medio_id
     * @return Response
     * @version V-1.1 19/06/15 19:10:11
     * @author  Jose Rodriguez
     *         Maykol Purica
     */
    public function store(Request $request, $medio_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }

        // Si existe el parámetro save y es igual 1,
        // indica que estoy guardando un único registro.
        if ($request->input('save') && $request->input('save') == 1) {
            $rules = array(
                'nombre' => 'required',
                'descripcion' => 'required',
                // 'costo'            => 'required|numeric',
                'username' => 'required',
                'tipo_publicacion' => 'required|integer',
            );

            $v = Validator::make(
                array_merge($request->all(), array('username' => $request->header('username'))),
                $rules
            );

            if ($v->fails()) {
                return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
            }

            $user = User::where('username', $request->header('username'))->first(['id']);
            $service = array(
                'nombre' => $request->input('nombre'),
                // 'costo'               => $request->input('costo'),
                'descripcion' => $request->input('descripcion'),
                'tipo_publicacion_id' => $request->input('tipo_publicacion'),
                'usuario_id' => $user->id,
                'otros_detalles' => json_encode($request->input('otros_detalles')),
                'es_cadena' => $request->input('es_cadena'),
            );
            $service = $medio->servicios()->create($service);
            $servicio = new Servicio();
            $servicio->crearTarifa($service);

            $data['service'] = $service;

            if ($request->input('es_cadena') == true && $request->input('hijos') != null):
                $parentID = $service->id;
                $childrenArray = $request->input('hijos');
                $children = $this->saveChildren($medio, $parentID, $childrenArray, $user->id);
                $data['children'] = $children;
            endif;
            if($request->input('children') &&  $request->input('es_cadena') == true):
                $service->children()->sync($request->input('children'));
            endif;
            if($request->input('parent')):
                $service->parent()->sync([$request->input('parent')]);
            endif;
            return response()->json(['status' => 'ok', 'data' => $data], 201, array('Content-Type' => 'application/json'));
        }

        // *** Realiza el guardado para los servicios que vienen desde el wizard de MEdios.
        //
        $rules = array(
            'username' => 'required',
        );
        foreach ($request->all() as $key => $row) {
            $rules["{$key}.nombre"] = 'required';
            $rules["{$key}.descripcion"] = 'required';
            $rules["{$key}.tipo_publicacion_id"] = 'required|integer';
            // $rules["{$key}.costo"]               = 'required|numeric';
        }

        $v = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            $rules
        );

        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
        }
        //dd($request->all());
        $user = User::where('username', $request->header('username'))->first(['id']);
        foreach ($request->all() as $key => $row) {
            $row['usuario_id'] = $user->id;
            $row['otros_detalles'] = json_encode($row['otros_detalles']);
            $service = $medio->servicios()->create($row);
            $servicio = new Servicio();
            $servicio->crearTarifa($service);

            $nuevoservicio[]['servicio'] = $service;
            if ($row['es_cadena'] == 'true'):
                //$this->saveChildren($parentID, )
                $parentID = $service->id;
                $childrenArray = $row['hijos'];
                $children = $this->saveChildren($medio, $parentID, $childrenArray, $user->id);
                $nuevoservicio[]['children'] = $children;
            endif;


        }

        return response()->json(['status' => 'ok', 'data' => $nuevoservicio], 201, array('Content-Type' => 'application/json'));
    }

    /**
     * Display the specified resource.
     *
     * @param $medio_id
     * @param $servicio_id
     * @return Response
     * @internal param int $id
     */
    public function show($medio_id, $servicio_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }

        $servicios = $medio->servicios()->with([
            'tipoPublicacion' => function ($q) {
                $q->addSelect('id', 'nombre');
            },
            'condiciones' => function ($q) {
                $q->orderBy('deleted_at', 'desc');
                $q->orderBy('es_base', 'desc');
            },
            'condiciones.detalles',
            'parent',
        ])->find($servicio_id);

        if (!$servicios) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un contacto con ese código.'])], 404);
        }
        $servicios->otros_detalles = json_decode($servicios->otros_detalles);
        return response()->json(['status' => 'ok', 'data' => $servicios], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request     parámetros enviados por PUT.
     * @param Integer $medio_id    ID de Medio.
     * @param Integer $servicio_id ID del Servicio.
     * @return Response
     * @author  Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.1 19/06/15 17:53:03
     */
    public function update(Request $request, $medio_id, $servicio_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }
        $servicio = $medio->servicios()->find($servicio_id);
        if (!$servicio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Medio con ese código.'])], 404);
        }

        $v = Validator::make($request->all(), [
            "nombre" => 'required',
            "descripcion" => 'required',
            // "costo"       => 'required|numeric',
        ]);

        if ($v->fails()) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 422, 'message' => $v->errors()])], 422);
        }

        $user = User::where('username', $request->header('username'))->first(['id']);
        $servicio->nombre = $request->input('nombre');
        $servicio->descripcion = $request->input('descripcion');
        $servicio->otros_detalles = json_encode($request->input('otros_detalles'));
        $servicio->usuario_id = $user->id;
        $servicio->es_cadena = $request->input('es_cadena');
        $nuevoservicio[]['servicio'] = $servicio;
        $servicio->save();
        /*if (false || count($request->input('hijos')) > 0 && $request->input('hijos') != null):
            $servicio->servicio_padre()->delete();
            //$this->saveChildren($parentID, )
            $parentID = $servicio->id;
            $childrenArray = $request->input('hijos');
            $children = $this->saveChildren($medio, $parentID, $childrenArray, $user->id);
            $nuevoservicio[]['children'] = $children;
            $servicio->save();
        endif;*/
        if($request->input('children')):
            $servicio->children()->sync($request->input('children'));
        endif;
        if($request->input('parent') && $request->input('es_cadena')==false):
            $servicio->parent()->sync([$request->input('parent')]);
        endif;
        if($request->input('parent') && $request->input('es_cadena')==true):
            $pivot = $servicio->parent()->get()->toArray();
            DB::table('servicios_servicios')->where('id', '=', $pivot[0]['pivot']['id'])->delete();
        endif;
        if($request->input('es_cadena')==false):
            $pivot = $servicio->children()->get()->toArray();
            foreach ($pivot as $row) {
                   DB::table('servicios_servicios')->where('id', '=', $row['pivot']['id'])->delete();
            }
        endif;
        return response()->json(['status' => 'ok', 'data' => $nuevoservicio], 200);
    }

    /**
     * Devuelve los Servicios asociados a un medio dado, en formato data tables.
     *
     * @param  Integer $medio_id ID de Medio
     * @return Datatables
     * @author  Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.1 10/06/15 13:44:02
     */
    public function dtServices($medio_id)
    {
        $medio = Medio::find($medio_id);
        if (!$medio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Proveedor con ese código.'])], 404);
        }

        $data = $medio->servicios()->with([
            'tipoPublicacion' => function ($q) {
                $q->addSelect('nombre', 'id');
            }
        ])->get();
        return Datatables::of($data)
            ->addColumn('hijos', function ($servicio) {
                    return json_encode($servicio->getChildren($servicio->id));
            })
            ->make(true);
    }

    /**
     * @param Medio $medio
     * @param $parentID
     * @param $childrenArray
     * @param $usuario_id
     * @return array
     */
    private function saveChildren(Medio $medio, $parentID, $childrenArray, $usuario_id)
    {
        $hijosArray = array();
        $tipoMedio = TipoMedio::where('nombre', 'MEDIO')->first(['id']);
        //dd($childrenArray);
        foreach ($childrenArray as $key => $child):
            //crear hijo
            $childData = [
                'medio_id' => $medio->id,
                'nombre' => $child['nombre'],
                'tipo_publicacion_id' => $child['selectTipo']['id'],
                'usuario_id' => $usuario_id,
                'descripcion' => $child['descripcion'],
                'tipo_medio' => $tipoMedio->id,
            ];
            $hijo = $medio->servicios()->create($childData);
            $servicio = new Servicio();
            $servicio->crearTarifaMedio($hijo);

            //Asociar padre e hijo
            $relaciones = ServiciosServicios::create([
                "servicios_padre_id" => $parentID,
                "servicios_hijo_id" => $hijo->id,
            ]);
            $hijosArray[] = [
                'hijo' => $hijo,
                'relaciones' => $relaciones];
        endforeach;
        return $hijosArray;
    }

    public function getChildren($servicio_id)
    {
        $this->servicioPadre = $servicio_id;
        $servicio = Servicio::find($servicio_id);
        if (!$servicio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Medio con ese código.'])], 404);
        }
        $childrenServices  = $servicio->getChildren($servicio_id);
        return response()->json(['status' => 'ok', 'data' => $childrenServices], 200);
    }

    public function listChildren($servicio_id)
    {
        $service = Servicio::find($servicio_id);
        $children = $service->children()->get()->toArray();
        return response()->json(['status' => 'ok', 'data' => $children], 200);

    }

}