<?php
namespace App\Modules\Configuracion\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\RedesSociales;
use App\Models\Servicio;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class RedesSocialesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $redes_sociales = RedesSociales::all();

        return response()->json(['status' => 'ok', 'data' => $redes_sociales], 200);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex()
    {
        $redes_sociales = RedesSociales::all();
        return Datatables::of($redes_sociales)->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $nueva_target = array(
            'nombre' => $request->input('nombre'),
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($nueva_target, $row);
        $redes_sociales = RedesSociales::create($row);

        return response()->json(['status' => 'ok', 'data' => $redes_sociales], 201);

    }


    /**
     * Display the specified resource.
     *
     * @param $redsocial_id
     * @return Response
     * @internal param int $id
     */
    public function show($redsocial_id)
    {

        $redes_sociales = RedesSociales::find($redsocial_id);

        if (!$redes_sociales) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una red social con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $redes_sociales], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $redsocial_id
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, $redsocial_id)
    {
        //var_dump($request->all());die;
        $redes_sociales = RedesSociales::find($redsocial_id);
        if (!$redes_sociales) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una red social con ese código.'])], 404);
        }
        $nombre = $request->input('nombre');

        $bandera = false;
        if ($nombre) {
            $redes_sociales->nombre = $nombre;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $redes_sociales->usuario_id = $user->findByName($request->header('username'), true)->id;
            $redes_sociales->save();
            return response()->json(['status' => 'ok', 'data' => $redes_sociales], 200);
        } else {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningúna red social'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @internal param int $id
     */
    public function destroy($id)
    {
        $red_social = RedesSociales::find($id);
        if (!$red_social) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra una red social con ese código.'], 404);
        }

        $medios=Servicio::all()->toArray();
        foreach($medios as $medio){
            $json=$medio['otros_detalles'];
            $array=json_decode($json, true);
            if (isset($array[0]['red_social'])) {
                if(count($array[0]['red_social']) > 0){
                    $id_red_social=$array[0]['red_social'][0]['red']['id'];
                    if ($id_red_social == $id) {
                        return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Esta red social está asociada a un servicio.'], 404);
                    }
                }
            }
        }

        $red_social->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado una red social correctamente.'], 200);

    }

}