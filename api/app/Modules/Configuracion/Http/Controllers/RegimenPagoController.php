<?php
namespace App\Modules\Configuracion\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\Regimenpago;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class RegimenPagoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $regimen_pago = Regimenpago::all();

        return response()->json(['status' => 'ok', 'data' => $regimen_pago], 200);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex()
    {
        $regimen_pago = Regimenpago::all();
        return Datatables::of($regimen_pago)->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $nuevoregimen = array(
            'nombre' => $request->input('nombre'),
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($nuevoregimen, $row);
        $regimen_pago = Regimenpago::create($row);

        return response()->json(['status' => 'ok', 'data' => $regimen_pago], 201);

    }


    /**
     * Display the specified resource.
     *
     * @param $regimen_id
     * @return Response
     * @internal param int $id
     */
    public function show($regimen_id)
    {

        $regimen_pago = Regimenpago::find($regimen_id);

        if (!$regimen_pago) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un regimen con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $regimen_pago], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $regimen_id
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, $regimen_id)
    {
        //var_dump($request->all());die;
        $regimen_pago = Regimenpago::find($regimen_id);
        if (!$regimen_pago) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un regimen con ese código.'])], 404);
        }
        $nombre = $request->input('nombre');

        $bandera = false;
        if ($nombre) {
            $regimen_pago->nombre = $nombre;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $regimen_pago->usuario_id = $user->findByName($request->header('username'), true)->id;
            $regimen_pago->save();
            return response()->json(['status' => 'ok', 'data' => $regimen_pago], 200);
        } else {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún regimen de papo.'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @internal param int $id
     */
    public function destroy($id)
    {
        $regimen = Regimenpago::find($id);
        if (!$regimen) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un regimen con ese código.'], 200);
        }

        if ($regimen->contrato()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este registro tiene contratos asociados.'], 200);
        }

        $regimen->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el regimen correctamente.'], 200);
    }

}