<?php
namespace App\Modules\Configuracion\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\Target;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use App\Models\Servicio;

class TargetController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $target = Target::all();

        return response()->json(['status' => 'ok', 'data' => $target], 200);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex()
    {
        $target = Target::all();
        return Datatables::of($target)->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $nueva_target = array(
            'nombre' => $request->input('nombre'),
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($nueva_target, $row);
        $target = Target::create($row);

        return response()->json(['status' => 'ok', 'data' => $target], 201);

    }


    /**
     * Display the specified resource.
     *
     * @param $target_id
     * @return Response
     * @internal param int $id
     */
    public function show($target_id)
    {

        $target = Target::find($target_id);

        if (!$target) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un target con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $target], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $target_id
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, $target_id)
    {
        //var_dump($request->all());die;
        $target = Target::find($target_id);
        if (!$target) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un target con ese código.'])], 404);
        }
        $nombre = $request->input('nombre');

        $bandera = false;
        if ($nombre) {
            $target->nombre = $nombre;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $target->usuario_id = $user->findByName($request->header('username'), true)->id;
            $target->save();
            return response()->json(['status' => 'ok', 'data' => $target], 200);
        } else {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún target'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @internal param int $id
     */
    public function destroy($id)
    {
        $target = Target::find($id);
        if (!$target) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un target con ese código.'], 404);
        }

        $medios=Servicio::all()->toArray();
        foreach($medios as $medio){
            $json=$medio['otros_detalles'];
            $array=json_decode($json, true);
            if (isset($array[0]['target'])) {
                if(count($array[0]['target']) > 0){
                    if (isset($array[0]['target']['id'])) {
                        $id_target=$array[0]['target']['id'];
                        if ($id_target == $id) {
                            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este target está asociado a un servicio.'], 404);
                        }
                    }
                }
            }
        }

        $target->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el target correctamente.'], 200);
    }


}