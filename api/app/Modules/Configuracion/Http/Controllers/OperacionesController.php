<?php namespace App\Modules\Configuracion\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Models\Acl\Operation;
use Illuminate\Http\Request;
//use yajra\Datatables\Facades\Datatables;
use yajra\Datatables\Datatables;

class OperacionesController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        return response()->json((new Operation())->getOptions(), 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        return response()->json((new Operation())->crear($request->all()), 200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        return response()->json(Operation::find($id), 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $result = (new Operation())->actualizar($id, $request->all());
        return response()->json(['status' => 'ok', 'data' => $result], 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Listado de Operaciones
     * @return mixed
     */
    public function dtIndex()
    {
        $obj = Operation::all();
        return Datatables::of($obj)->make(true);
    }

    /**
     * Devuelve y consulta los iconos disponibles para las operaciones.
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Jose Rodriguez
     */
    public function icons()
    {
        return response()->json((new Operation())->getIcons(), 200);
    }

    /**
     * Devuelve y consulta los iconos disponibles para las operaciones.
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Jose Rodriguez
     */
    public function visuals()
    {
        return response()->json((new Operation())->getVisuals(), 200);
    }

    /**
     * Devuelve y consulta los iconos disponibles para las operaciones.
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Jose Rodriguez
     */
    public function renders()
    {
        return response()->json((new Operation())->getRenders(), 200);
    }

    /**
     * Devuelve y consulta los iconos disponibles para las operaciones.
     * @return \Symfony\Component\HttpFoundation\Response
     * @author Jose Rodriguez
     */
    public function targets()
    {
        return response()->json((new Operation())->getTargets(), 200);
    }

}
