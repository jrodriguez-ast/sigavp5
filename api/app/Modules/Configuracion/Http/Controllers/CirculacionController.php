<?php
namespace App\Modules\Configuracion\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\Circulacion;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;
use App\Models\Servicio;

class CirculacionController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $circulacion = Circulacion::all();

        return response()->json(['status' => 'ok', 'data' => $circulacion], 200);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex()
    {
        $circulacion = Circulacion::all();
        return Datatables::of($circulacion)->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $nueva_circulacion = array(
            'nombre' => $request->input('nombre'),
        );
        //$user = new User();
        $row['usuario_id'] = 1;// $user->findByName($request->header('username'), true)->id;
        $row = array_merge($nueva_circulacion, $row);
        $circulacion = Circulacion::create($row);

        return response()->json(['status' => 'ok', 'data' => $circulacion], 201);

    }


    /**
     * Display the specified resource.
     *
     * @param $circulacion_id
     * @return Response
     * @internal param int $id
     */
    public function show($circulacion_id)
    {

        $circulacion = Circulacion::find($circulacion_id);

        if (!$circulacion) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una circulacion con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $circulacion], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $circulacion_id
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, $circulacion_id)
    {
        //var_dump($request->all());die;
        $circulacion = Circulacion::find($circulacion_id);
        if (!$circulacion) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una circulacion con ese código.'])], 404);
        }
        $nombre = $request->input('nombre');

        $bandera = false;
        if ($nombre) {
            $circulacion->nombre = $nombre;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $circulacion->usuario_id = $user->findByName($request->header('username'), true)->id;
            $circulacion->save();
            return response()->json(['status' => 'ok', 'data' => $circulacion], 200);
        } else {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningúna circulacion'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @internal param int $id
     */
    public function destroy($id)
    {
        $circulacion = Circulacion::find($id);
        if (!$circulacion) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una circulacion con ese código.'])], 404);
        }

        $medios=Servicio::all()->toArray();
        foreach($medios as $medio){
            $json=$medio['otros_detalles'];
            $array=json_decode($json, true);
            if (isset($array[0]['circulacion'])) {
                if(count($array[0]['circulacion']) > 0){
                    if(isset($array[0]['circulacion']['id'])){
                        $id_circulacion=$array[0]['circulacion']['id'];
                        if ($id_circulacion == $id) {
                            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Esta circulación está asociado a un servicio.'], 404);
                        }
                    }
                }
            }
        }

        $circulacion->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado la circulacion correctamente.'], 200);
    }

}