<?php namespace App\Modules\Configuracion\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Utilities\GenericConfiguration;
use Illuminate\Http\Request;
use Datatables;
use Response;
use App\Models\Acl\User;

class GenericConfigurationController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $model = GenericConfiguration::find(1);

        return response()->json(['status' => 'ok', 'data' => $model], 200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {

        $configuracion = GenericConfiguration::with('tipoPublicacion')->find($id);

        if (!$configuracion) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una configuracion con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $configuracion], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param  int $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $editconfi = GenericConfiguration::find($id);
        if (!$editconfi) {
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra ninguna configuracion.']))], 404);
        }
        $value = $request->input('value');
        $description = $request->input('description');
        $tipo_publicacion_id = $request->input('tipo_publicacion_id');
        $validacion = $request->input('validacion');
        $bandera = false;
        if ($value) {
            $editconfi->value = $value;
            $bandera = true;
        }
        if ($description) {
            $editconfi->description = $description;
            $bandera = true;
        }
        if ($tipo_publicacion_id) {
            $editconfi->tipo_publicacion_id = $tipo_publicacion_id;
            $bandera = true;
        }
        if ($validacion) {
            $editconfi->validacion = $validacion;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $editconfi->usuario_id = $user->findByName($request->header('username'), true)->id;
            $editconfi->save();
            return response()->json(array('status' => 'ok', 'data' => $editconfi), 200);
        } else {
            return response()->json(array('status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato del correo.'])), 304);
        }

    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }


    public function dtIndex()
    {
        $configuracion = GenericConfiguration::all();
        return Datatables::of($configuracion)->make(true);
    }
}
