<?php
namespace App\Modules\Configuracion\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\RecepcionOrdenServicio;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class RecepcionOrdenServicioController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $recepcionOrden = RecepcionOrdenServicio::all();

        return response()->json(['status' => 'ok', 'data' => $recepcionOrden], 200);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex()
    {
        $recepcionOrden = RecepcionOrdenServicio::all();
        return Datatables::of($recepcionOrden)->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $nuevarecepcion= array(
            'nombre' => $request->input('nombre'),
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($nuevarecepcion, $row);
        $recepcionorden = RecepcionOrdenServicio::create($row);

        return response()->json(['status' => 'ok', 'data' => $recepcionorden], 201);

    }


    /**
     * Display the specified resource.
     *
     * @param $recepcion_id
     * @return Response
     * @internal param int $id
     */
    public function show($recepcion_id)
    {

        $regimen_pago = RecepcionOrdenServicio::find($recepcion_id);

        if (!$regimen_pago) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una recepcion con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $regimen_pago], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $recepcion_id
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, $recepcion_id)
    {
        //var_dump($request->all());die;
        $recepcion_orden = RecepcionOrdenServicio::find($recepcion_id);
        if (!$recepcion_orden) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra una recepcion con ese código.'])], 404);
        }
        $nombre = $request->input('nombre');

        $bandera = false;
        if ($nombre) {
            $recepcion_orden->nombre = $nombre;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $recepcion_orden->usuario_id = $user->findByName($request->header('username'), true)->id;
            $recepcion_orden->save();
            return response()->json(['status' => 'ok', 'data' => $recepcion_orden], 200);
        } else {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningúna recepcion de papo.'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @internal param int $id
     */
    public function destroy($id)
    {
        $recepcion = RecepcionOrdenServicio::find($id);
        if (!$recepcion) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un registro con ese código.'], 200);
        }

        if ($recepcion->cotizacionOrden()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este registro tiene otros registros asociados.'], 200);
        }

        $recepcion->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el registro correctamente.'], 200);
    }

}