<?php namespace App\Modules\Configuracion\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\Configuracion\Http\Requests\TipoCorreoRequest;
use App\Models\TipoCorreo;
use Response;
use Datatables;

class TipoCorreoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        $model = TipoCorreo::all();

        return response()->json($model, 200);

        //return response ()->json (Correos::all (), 200);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param TipoCorreoRequest $request
     * @return Response
     */
    public function store(TipoCorreoRequest $request)
    {
        $nuevoTipoCorreo = TipoCorreo::create($request->all());
        return response()->json(array('status' => 'ok', 'data' => $nuevoTipoCorreo), 201);


    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
//    public function update (TipoCorreoRequest $request, $id)
//    {
//        $editcorreo = TipoCorreo::find ($id);
//        if (!$editcorreo) {
//            return response ()->json ([array('status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra este correo.']))], 404);
//        }
//        $nombre = $request->input ('nombre');
//        $correo = $request->input ('correo');
//        $id_tipo_publicacion = $request->input ('id_tipo_publicacion');
//        $usuario_id = $request->input ('usuario_id');
//        $id_tipo_correo = $request->input('id_tipo_correo');
//        $activo = true;
//        $bandera = false;
//        if ($nombre) {
//            $editcorreo->nombre = $nombre;
//            $bandera = true;
//        }
//        if ($correo) {
//            $editcorreo->correo = $correo;
//            $bandera = true;
//        }
//        if ($id_tipo_publicacion) {
//            $editcorreo->id_tipo_publicacion = $id_tipo_publicacion;
//            $bandera = true;
//        }
//        if ($id_tipo_correo) {
//            $editcorreo->id_tipo_correo = $id_tipo_correo;
//            $bandera = true;
//        }
//        if ($$usuario_id) {
//            $editcorreo->usuario_id = $usuario_id;
//            $bandera = true;
//        }
//        if ($activo) {
//            $editcorreo->activo = $activo;
//            $bandera = true;
//        }
//        if ($bandera) {
//            $editcorreo->save ();
//            return response ()->json (array('status' => 'ok', 'data' => $editcorreo), 200, array('Location' => 'sigavp.ast.com.ve/api/public/configuracion/correos/' . $editcorreo->id));
//        } else {
//            return response ()->json (array('status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato del correo.'])), 304);
//        }
//
//    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        $elicorreo = TipoCorreo::find($id);
        if (!$elicorreo) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un Correo con ese código.'])], 404);
        }
        $elicorreo->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el correo correctamente.'], 200);
    }

    public function dtIndex()
    {
        $correos = TipoCorreo::all();
        return Datatables::of($correos)->make(true);
    }

}

