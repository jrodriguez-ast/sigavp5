<?php

Route::group (['prefix' => 'configuracion'], function () {

    //Correos
    Route::get ('dt_correo', 'CorreoController@dtIndex');
    Route::resource ('correo', 'CorreoController');

    Route::get ('dt_generic', 'GenericConfigurationController@dtIndex');
    Route::resource ('generic', 'GenericConfigurationController');

    //Tipo_Correo
    Route::get ('dt_tipo_correo', 'TipoCorreoController@dtIndex');
    Route::resource ('tipo_correo', 'TipoCorreoController');

    // Operaciones
    Route::get ('operaciones/dt', 'OperacionesController@dtIndex');
    Route::get ('operaciones/icons', 'OperacionesController@icons');
    Route::get ('operaciones/renders', 'OperacionesController@renders');
    Route::get ('operaciones/visuals', 'OperacionesController@visuals');
    Route::get ('operaciones/targets', 'OperacionesController@targets');
    Route::resource ('operaciones', 'OperacionesController');

    //Regimen_pago
    Route::resource ('regimen', 'RegimenPagoController');
    Route::get ('regimen_dt', 'RegimenPagoController@dtIndex');

    //recepcion_orden_servicio
    Route::resource ('recepcion', 'RecepcionOrdenServicioController');
    Route::get ('recepcion_dt', 'RecepcionOrdenServicioController@dtIndex');

    //circulacion_servicio
    Route::resource ('circulacion', 'CirculacionController');
    Route::get ('circulacion_dt', 'CirculacionController@dtIndex');

    //Target_servicio
    Route::resource ('target', 'TargetController');
    Route::get ('target_dt', 'TargetController@dtIndex');

  //Redes_sociales
    Route::resource ('redsocial', 'RedesSocialesController');
    Route::get ('redsocial_dt', 'RedesSocialesController@dtIndex');


});