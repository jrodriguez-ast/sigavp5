<?php namespace App\Modules\Configuracion\Http\Requests;

use App\Modules\Configuracion\Http\Requests;


class TipoCorreoRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'nombre' => 'required',
            'descripcion' => 'required',
//          'username' => 'required',

        ];
    }

    //En la funcion response se devuelve un error 422 donde dice los campos requeridos segun el rules

    public function response(array $errors)
    {
        return response()->json(array('errors' => array(['code' => 422, 'message' => $errors])), 422);
    }
}
