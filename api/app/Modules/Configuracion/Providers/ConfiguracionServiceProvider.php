<?php
namespace App\Modules\Configuracion\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class ConfiguracionServiceProvider extends ServiceProvider
{
    /**
     * Register the Configuracion module service provider.
     *
     * @return void
     */
    public function register()
    {
        // This service provider is a convenient place to register your modules
        // services in the IoC container. If you wish, you may make additional
        // methods or service providers to keep the code more focused and granular.
        App::register('App\Modules\Configuracion\Providers\RouteServiceProvider');

        $this->registerNamespaces();
    }

    /**
     * Register the Configuracion module resource namespaces.
     *
     * @return void
     */
    protected function registerNamespaces()
    {
        Lang::addNamespace('configuracion', realpath(__DIR__ . '/../Resources/Lang'));

        View::addNamespace('configuracion', realpath(__DIR__ . '/../Resources/Views'));
    }
}
