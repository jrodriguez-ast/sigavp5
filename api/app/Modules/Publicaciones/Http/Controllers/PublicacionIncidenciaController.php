<?php namespace App\Modules\Publicaciones\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Publicacion;
use App\Models\PublicacionIncidencias;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use yajra\Datatables\Datatables;

class PublicacionIncidenciaController extends Controller
{

    public function __construct()
    {
        $this->middleware('data.user');
    }

    /**
     * Guarda las incidencias asociadas a las versiones de una publicación
     * @param Request $request
     * @param $publicacion_id
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function store(Request $request, $publicacion_id)
    {
        // INICIO(2) - Valido la información recibida por el server
        $rules = [
            'incidencia' => 'required|integer',
            'version' => 'required|integer',
            'exonerado' => 'required',
            'monto' => 'sometimes|required|numeric',
            'precio' => 'sometimes|required|numeric',
            'username' => 'required',
        ];

        $validator = Validator::make(
            array_merge($request->all(), array('username' => $request->header('username'))),
            $rules
        );

        if ($validator->fails()) {
            return response()->json([
                'status' => 'fail',
                'errors' => [
                    ['code' => 422, 'message' => $validator->errors()]
                ]
            ], 422);
        }

        // FIN(2) - Valido la información recibida por el server

        // Verifico que versión no tenga ya incidencia registrada.
        $version_id = $request->input('version');
        if (PublicacionIncidencias::where('version_id', $version_id)->exists()) {
            return response()->json([
                'status' => 'fail',
                'errors' => [
                    [
                        'code' => 422,
                        'message' => 'Ya agrego una incidencia a la versión seleccionada'
                    ]
                ]
            ], 422);
        }

        $dataForIncidencia = [
            'orden_publicacion_id' => $publicacion_id,
            'tipo_incidencia_id' => $request->input('incidencia'),
            'version_id' => $version_id,
            'usuario_id' => session()->get('userId'),
            'exonerado' => $request->input('exonerado'),
            'precio' => $request->input('precio'),
            'monto' => $request->input('monto'),
        ];
        $TipoIncidencia = PublicacionIncidencias::create($dataForIncidencia);

        // Operación Exitosa!!
        if ($TipoIncidencia) {
            return response()->json([
                'status' => 'ok',
                'data' => Publicacion::data_for_publicacion($publicacion_id),
                'message' => 'Se agregado la incidencia exitosamente!'
            ], 200);
        }

        // Ocurrió un error en la operación a nivel de BD.
        return response()->json([
            'status' => 'fail',
            'errors' => [
                [
                    'code' => 304,
                    'message' => 'No se pudo realizar el registro de la incidencia.'
                ]
            ]
        ], 304);
    }

    /**
     * Listado de Incidencias
     * @param Request $request
     * @return mixed
     */
    public function dtIndex(Request $request)
    {
        $data = PublicacionIncidencias::leftJoin('acl.users', 'public.ordenes_publicacion_incidencias.usuario_id', '=', 'acl.users.id')
            ->leftJoin('public.ordenes_publicacion', 'public.ordenes_publicacion.id', '=', 'public.ordenes_publicacion_incidencias.orden_publicacion_id')
            ->leftJoin('public.tipos_incidencias', 'public.tipos_incidencias.id', '=', 'public.ordenes_publicacion_incidencias.tipo_incidencia_id')
            ->select(array(
                'public.ordenes_publicacion.id',
                'public.ordenes_publicacion.codigo',
                'public.tipos_incidencias.nombre as incidencia',
                'public.ordenes_publicacion_incidencias.created_at',
                'acl.users.first_name',
                'acl.users.last_name'
            ))->orderBy('public.ordenes_publicacion_incidencias.created_at', 'desc');

        if ($request->clientID)
            $data->where(array('public.ordenes_publicacion.cliente_id' => $request->clientID));
        return Datatables::of($data)
            ->editColumn('public.ordenes_publicacion_incidencias.created_at', function ($data) {
                $date = date_create($data->created_at);
                return date_format($date, 'd/m/Y');
            })
            ->editColumn('creator', function ($data) {
                return "{$data->first_name}  {$data->last_name}";
            })
            ->make(true);
    }
}
