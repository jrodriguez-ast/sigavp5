<?php namespace App\Modules\Publicaciones\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use App\Models\Iva;
use App\Models\Publicacion;
use App\Models\PublicacionVersion;
use App\Models\RecargoGestion;
use App\Models\Utilities\GenericConfiguration;
use App\Modules\Notificacion;
use Carbon\Carbon;
use DateTime;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\URL;
use yajra\Datatables\Datatables;

//Es necesario agregarlo aquí por ser una clase nativa del lenguaje

class PublicacionController extends Controller
{
    private $attach_mail = null;
    private $data_email = null;
    private $all_emails = null;
    private $data_cliente = null;
    private $id_publicacion = null;

    public function __construct()
    {
        $this->middleware('data.user');
    }

    /**
     * Permite crear una publicación
     *
     * @access Public
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:01 AM
     */
    public function store()
    {
        $p_iva = Iva::where('activo', true)->firstOrFail();
        $p_avp = RecargoGestion::where('activo', true)->firstOrFail();
        $fecha = date('Y-m-j');
        $dias_validez = GenericConfiguration::where('_label', 'publicacion_validez')->pluck('value');
        $validez = ($dias_validez ? '+' . $dias_validez . ' day' : '+15 day');
        $nuevafecha = strtotime($validez, strtotime($fecha));
        $nuevafecha = date('Y-m-j', $nuevafecha);
        $defaultPublicacion = [
            "cliente_id" => Input::get('clienteId'),
            "medio_id" => Input::get('medioId'),
            "fecha_creacion" => Carbon::now()->toDateTimeString(),
            "fecha_expiracion" => $nuevafecha,
            "descuento" => 0,
            "con_impuestos" => false,
            "porcentaje_impuesto" => (float)$p_iva->data['iva'],
            'porcentaje_avp' => (float)$p_avp->data['porcentaje'],
            'usuario_id' => session()->get('userId'),
            'firmante_nombre' => GenericConfiguration::where('_label', 'publicacion_firmante_nombre')->pluck('value'),
            'firmante_cargo' => GenericConfiguration::where('_label', 'publicacion_firmante_cargo')->pluck('value')
        ];

        $publicacion = Publicacion::create($defaultPublicacion);

        if ($publicacion) {
            Publicacion::change_estatus($publicacion['id'], 'Por Aprobar', session()->get('userId'));

            //Datos para la notificación
            $publicacion = Publicacion::data_for_publicacion($publicacion['id']);
            $datosNotificacion = [

                'chekk' => 0,
                'tit' => 'Publicación Creada',
                'des' => 'Se ha creado la publicación #' . $publicacion['codigo'] . ' exitosamente',
                'lin' => ' ',
                'titu_icon' => 'Publicación Creada',
                'tipo_not_ico' => 'fa fa-picture-o',
            ];
            $notificacion = \App\Models\Notificacion::create($datosNotificacion);
        }

        return response()->json($publicacion->toArray(), 200);
    }

    /**
     * Muestra los datos de una publicación y aplica los cálculos con
     * respecto a los datos cargados.
     *
     * @access public
     * @param integer $id Identificador de la publicación
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 10:53 AM
     */
    public function show($id)
    {
        Publicacion::calculate_publicacion($id);
        $publicacion = Publicacion::data_for_publicacion($id);

        if ($publicacion && sizeof($publicacion['versiones'])):
            //Verifico si la fecha es igual para establecer fecha unica en el frontEnd
            foreach ($publicacion['versiones'] as $key => &$cotiVersion) :
                if (!empty($cotiVersion['version_publicacion_fecha_inicio']) && !empty($cotiVersion['version_publicacion_fecha_fin'])):
                    $datetime1 = date_create($cotiVersion['version_publicacion_fecha_inicio']);
                    $datetime2 = date_create($cotiVersion['version_publicacion_fecha_fin']);
                    $interval = date_diff($datetime1, $datetime2);
                    if ($interval->days == 0)
                        $cotiVersion['fecha_unica'] = true;
                endif;
                $fecha_inicio = $cotiVersion['version_publicacion_fecha_inicio'];
                $fecha_final = $cotiVersion['version_publicacion_fecha_fin'];
                $diasporpublicacion = Carbon::createFromTimestamp(strtotime(($fecha_final)))->diffInDays(Carbon::createFromTimestamp(strtotime($fecha_inicio)));
                $diasporpublicacion++;
                $publicacion['versiones'][$key]['diasporpublicacion'] = $diasporpublicacion;
            endforeach;
        endif;

        return response()->json($publicacion, 200);
    }

    /**
     * Permite actualizar una publicación y sus entidades dependientes
     *
     * @access Public
     * @param Request $request
     * @param integer $id Identificador de la Publicación
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:01 AM
     */
    public function update(Request $request, $id)
    {
        $publicacion = Publicacion::find($id);
        $publicacion->porcentaje_impuesto = $request->input('porcentaje_impuesto');
        $publicacion->usuario_id = session()->get('userId');
        $publicacion->porcentaje_avp = $request->input('porcentaje_avp');
        $publicacion->fecha_expiracion = $request->input('fecha_expiracion');
        $publicacion->es_terminada = ($request->has('es_terminada') ? $request->input('es_terminada') : false);

        //OJO COLOCAR UNA TRANSACCIÓN AQUÍ
        //http://laravel.com/docs/5.0/database#database-transactions

        if (count($request->versiones) > 0):
            foreach ($request->versiones as $version):
                if (!empty($version['tmp_aprobada']) && $version['tmp_aprobada']):
                    $publicacion_version = PublicacionVersion::find($version['id']);
                    $publicacion_version->publicado = $version['tmp_aprobada'];

                    //Se inserta en la entidad PublicaciónVersion
                    //Si la inserción es exitosa de agregan los detalles de la condición
                    $publicacion_version->save();
                    // if ($publicacion_version->save()): endif;
                endif;
            endforeach;
            $publicacion->publicada = TRUE;
        endif;

        $con_email = $request->has('con_email') ? $request->input('con_email') : false;
        $data_email = $request->has('data_email') ? $request->input('data_email') : false;
        if ($con_email):
            $publicacion->fue_enviada = (count($this->send_mail_publicacion($id, $data_email)) > 0 ? true : false);
        endif;


        if ($publicacion->save()):
            Publicacion::change_estatus($id, $request->input('estatus'), session()->get('userId'));
            Publicacion::calculate_publicacion($id);
            $response = Publicacion::data_for_publicacion($id);
            return response()->json(array('status' => 'ok', 'data' => $response), 200);
        else:
            return response()->json(array('status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato de la cotizacion.'])), 304);

        endif;
    }

    /**
     * Provee los datos al datatable
     *
     * @access Public
     * @param Request $request Datos del request
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:01 AM
     */
    public function dtIndex(Request $request)
    {
        $data = Publicacion::with([
            'version.servicio', 'medio', 'cliente', 'status',
            //'user',
        ])->orderBy('created_at', 'desc')
            ->select([
                'id', 'codigo', 'fecha_creacion', 'fecha_expiracion', 'cliente_id', 'medio_id'
            ]);

        if ($request->cotizacionId)
            $data->where(['cotizacion_id' => $request->cotizacionId]);

        if ($request->clientID)
            $data->where(['cliente_id' => $request->clientID]);
//        $data = new Collection($data->get());

        return Datatables::of($data)
            ->editColumn('cliente', function ($data) {
                return $data->cliente['razon_social'];
            })
            ->editColumn('medio', function ($data) {
                return $data->medio['razon_social'];
            })
            ->editColumn('status', function ($data) {
                return (count($data->status) > 0) ? $data->status[0]['estatus'] : '';
            })
            ->editColumn('fecha_creacion', function ($data) {
                $date = date_create($data->fecha_creacion);
                return date_format($date, 'd/m/Y');
            })
            ->editColumn('fecha_expiracion', function ($data) {
                $date = date_create($data->fecha_expiracion);
                return date_format($date, 'd/m/Y');
            })
            ->make(true);
    }

    /**
     * Permite previsualizar una publicación
     *
     * @access public
     * @param Request $request Datos recibidos del frontEnd
     * @param integer $id Identificador de la Publicación
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:01 AM
     */
    public function send_publicacion_medio(Request $request, $id)
    {
        $publicacion = Publicacion::find($id);

        $data_email = $request->has('data_email') ? $request->input('data_email') : false;

        $publicacion->fue_enviada = (count($this->send_mail_publicacion($id, $data_email)) > 0 ? true : false);

        if ($publicacion->save()):
            $response = Publicacion::data_for_publicacion($id);
            return response()->json(array('status' => 'ok', 'data' => $response), 200);
        else:
            return response()->json(array('status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún dato de la cotizacion.'])), 304);

        endif;
    }

    /**
     * @param null $publicacionId
     * @param null $data_email
     * @return bool
     */
    public function send_mail_publicacion($publicacionId = null, $data_email = null)
    {
        if (empty($publicacionId))
            return false;

        $response = false;
        $generate_pdf = $this->_print('PUBLICACION', $publicacionId);
        if (!empty($generate_pdf['save'])):
            $publicacion = $generate_pdf['data']['view_data']['publicacion'];
            $public_path = public_path();
            $this->attach_mail = $public_path . "/" . $generate_pdf['url'];

            $this->id_publicacion = $publicacionId;
            $this->data_email = $data_email;
            $this->all_emails = (!empty($publicacion['medio']['correos_electronicos']) ? $publicacion['medio']['correos_electronicos'] : array());
            $this->data_cliente = $publicacion['cliente'];
            //verificamos si el archivo existe y lo retornamos
            if (Storage::exists("tmp/orden_publicacion_" . $publicacion['codigo'] . ".pdf")) {
                $response = Mail::send('EmailPublicacion', array('data' => $publicacion), function ($message) {
                    $message->from('no-responder@sigavp.ast.com.ve', 'Asociacion Venezolana de Publicidad');
                    if (!empty($this->data_email)):
                        if (!empty($this->data_email['todos']) && $this->data_email['todos']):
                            if (!empty($this->all_emails)):
                                foreach (json_decode($this->all_emails, true) as $key => $value):
                                    $message->to($value, $this->data_cliente['razon_social']);
                                endforeach;
                            endif;
                        elseif (!empty($this->data_email['email_send'])):
                            foreach ($this->data_email['email_send'] as $clave => $valor):
                                if ($valor['activo'])
                                    $message->to($valor['mail'], $valor['mail']);
                            endforeach;
                        endif;

                    endif;

                    //Envío con copia tipeada (custom)
                    if (!empty($this->data_email['cc'])):
                        $message->cc($this->data_email['cc']);
                    endif;


                    $data_publicacion = Publicacion::data_for_publicacion($this->id_publicacion);
                    $tipo_publicaciones = array();

                    //Agrupo los Tipos de publicaciones para evitar claves repetidas
                    if (!empty($data_publicacion) && !empty($data_publicacion['versiones'])):
                        foreach ($data_publicacion['versiones'] as $clave => $valor):
                            $tipo_publicaciones[$valor['tipo_publicacion_name']] = $valor['tipo_publicacion_id'];
                        endforeach;
                    endif;

                    //Envío de correos a cuentas AVP para Tipos de Publicaciones  BY MARIANNNE AND FREDERICK
                    if (!empty($tipo_publicaciones)):
                        foreach ($tipo_publicaciones as $key => $value):
                            $cuentas_correo_avp = GenericConfiguration::where(
                                array(
                                    '_label' => 'publicacion_correo_' . strtolower($key),
                                    'tipo_publicacion_id' => $value
                                )
                            )->pluck('value');
                            if ($cuentas_correo_avp):
                                $cuentas = explode(";", $cuentas_correo_avp);
                                foreach ($cuentas as $valor):
                                    $message->cc(trim($valor));
                                endforeach;
                            endif;
                        endforeach;
                    endif;

                    //Envio de a contactos administrativos y publicitarios asociados al cliente
                    if (!empty($this->data_email['contactos'])):
                        foreach ($this->data_email['contactos'] as $clave_c => $valor_c):

                            if (($valor_c['tipo'] == 'administrativo' || $valor_c['tipo'] == 'publicitario') && !empty($valor_c['correos_electronicos'])):
                                foreach ($valor_c['correos_electronicos'] as $clave_c_correos => $valor_c_correos):
                                    if ($valor_c_correos['activo'])
                                        $message->cc($valor_c_correos['a'], $valor_c['nombre'] . " " . $valor_c['apellido']);
                                endforeach;
                            endif;
                        endforeach;
                    endif;
                    $message->subject('Generada Orden de Publicación AVP para ' . $this->data_cliente['razon_social']);
                    $message->attach($this->attach_mail);
                });
            }
        endif;
        return $response;
    }

    /**
     * Dada una cadena de texto obtiene las iniciales de cada palabra
     *
     *
     * @access public
     * @param string $name Cadena a procesar
     * @param string $case Establece como sera la salida de la cadena procesada (mayúscula, minúscula)
     * @return string
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:23 AM
     */
    public function get_iniciales_string($name, $case = 'L')
    {
        if (empty($name)) {
            return $name;
        }

        $initials = $name;
        $splitted_name = explode(' ', trim($name));
        if (!empty($splitted_name)) {
            $initials = '';
            foreach ($splitted_name as $row)
                $initials .= substr(trim($row), 0, 1);
        }
        return ($case == 'U') ? strtoupper($initials) : strtolower($initials);
    }

    /**
     * @param Request $request
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function header(Request $request)
    {
        return view('pdf.header', $request->all());
    }

    /**
     * @param Request $request
     * @return \BladeView|bool|\Illuminate\View\View
     */
    public function footer(Request $request)
    {
        return view('pdf.footer', $request->all());
    }

    /**
     * @param $modelo
     * @param $modelo_id
     * @param bool $invalid
     * @return array|\Symfony\Component\HttpFoundation\Response
     */
    public function _print($modelo, $modelo_id, $invalid = false)
    {
        if (empty($modelo) || empty($modelo_id) || !is_string($modelo) || !is_numeric($modelo_id)) {
            return response()->json(['status' => 'fail', 'errors' => array([
                'code' => 422, 'message' => 'Parámetros Inválidos.'])
            ], 422);
        }

        $data = [];
        $modelo = strtoupper($modelo);

        if ($modelo == 'PUBLICACION') {
            $data = $this->orden_publicacion($modelo_id);

        } else {
            return response()->json(['status' => 'fail', 'errors' => array([
                'code' => 422, 'message' => 'modelo inválido.'])
            ], 422);
        }

        if (!is_array($data)) {
            return $data;
        }

        $data['header_data']['avp_rif'] = GenericConfiguration::where('_label', 'avp_rif')->pluck('value');
        $data['header_data']['avp_direccion'] = GenericConfiguration::where('_label', 'avp_direccion')->pluck('value');
        $data['view_data']['no_valido'] = $invalid;

        // Construcción del PDF
        $pdf = App::make('snappy.pdf.wrapper');
        $pdf->setOption('margin-top', '65mm');
        $pdf->setOption('margin-bottom', '30mm');
        $pdf->setOption('load-error-handling', 'ignore');
        $pdf->setOption('header-html', URL::to('/') . '/pdf/header?' . http_build_query($data['header_data']));
        $pdf->setOption('footer-html', URL::to('/') . '/pdf/footer?' . http_build_query($data['footer_data']));
        $pdf->setPaper('a4');
        $pdf->loadView($data['view_path'], $data['view_data']);
        if (file_exists('storage/tmp/' . $data['file_name']))
            unlink('storage/tmp/' . $data['file_name']);
        if ($pdf->save('storage/tmp/' . $data['file_name']))
            return array('save' => true, 'url' => 'storage/tmp/' . $data['file_name'], 'data' => $data);
        array('save' => false);
    }

    /**
     * Recibe una cadena de texto y retorna solo sus iniciales en minúsculas (L) o mayúsculas (U) de acuerdo al
     * parámetro $case.
     *
     * @param        $name
     * @param string $case
     * @author  Jose Rodriguez
     * @version V-1.0 22-07-2015
     * @return string
     */
    public function getInitials($name, $case = 'L')
    {
        if (empty($name)) {
            return $name;
        }

        $initials = $name;
        $splitted_name = explode(' ', trim($name));
        if (!empty($splitted_name)) {
            $initials = '';
            foreach ($splitted_name as $row)
                $initials .= substr(trim($row), 0, 1);
        }
        return ($case == 'U') ? strtoupper($initials) : strtolower($initials);
    }

    /**
     * Método que genera el PDF de la Orden de Publicación
     *
     * @access private
     * @param integer $id_orden Identificador de la Publicación
     * @return string
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 28/08/15 11:23 AM
     */
    private function orden_publicacion($id_orden)
    {
        $publicacion = Publicacion::data_for_publicacion($id_orden)->toArray();
        $publicacion['versiones'] = $publicacion['versiones']->toArray();
        $totalconiva = 0;
        $rotacion = 0;
        $fecha_inicio = 0;
        $fecha_final = 0;
        $dias = 0;
        $diasporpublicacion = 0;

        foreach ($publicacion['versiones'] as $key => $version) {
            $totalconiva += $version['subtotal'] + ($version['subtotal'] * $publicacion['porcentaje_impuesto'] / 100);
            $rotacion += $version['version_apariciones'];
            $fecha_inicio = $version['version_publicacion_fecha_inicio'];
            $fecha_final = $version['version_publicacion_fecha_fin'];
            $diasporpublicacion = Carbon::createFromTimestamp(strtotime(($fecha_final)))->diffInDays(Carbon::createFromTimestamp(strtotime($fecha_inicio)));
            $diasporpublicacion++;
            $publicacion['versiones'][$key]['diasporpublicacion'] = $diasporpublicacion;
            //dd($publicacion['versiones']);
            $dias += 1 + Carbon::createFromTimestamp(strtotime(($fecha_final)))->diffInDays(Carbon::createFromTimestamp(strtotime($fecha_inicio)));
        }

        $total = 0.00;
        $subtotal = 0.00;
        $fecha = new DateTime($publicacion['fecha_creacion']);
        $fecha_creacion = $fecha->format("d-m-Y");
        $data = [
            'header_data' => [
                'titulo' => 'Orden de Publicacion',
                'to' => $publicacion['medio']['razon_social'],
                'fecha' => $fecha_creacion,
                'code' => $publicacion['codigo'],
                'code_name' => GenericConfiguration::where("_label", "publicacion_code_name")->pluck('value'),
                'subject' => GenericConfiguration::where("_label", "publicacion_subject")->pluck('value'),
                'cliente' => $publicacion['cliente']['razon_social'],
                'rifcli' => $publicacion['cliente']['rif'],
                'rifmed' => $publicacion['medio']['rif'],
            ],
            'footer_data' => [
                'firmante' => $publicacion['firmante_nombre'],
                'cargo' => $publicacion['firmante_cargo'],
                'iniciales' => $this->getInitials($publicacion['firmante_nombre'], 'U'),
            ],
            'view_data' => [
                'publicacion' => $publicacion,
                'subtotal' => $subtotal,
                'total' => $total,
                'totalconiva' => $totalconiva,
                'rotacion' => $rotacion,
                'dias' => $dias,
                'diasporpublicacion' => $diasporpublicacion,
            ],
            'view_path' => 'pdf.orden_publicacion',
            'file_name' => 'orden_publicacion_' . $publicacion['codigo'] . '.pdf',
        ];
        return $data;
    }

    public function destroy($publicacion_id)
    {
        $publicacion = Publicacion::find($publicacion_id);
        if (!$publicacion) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra una publicacion con ese código.'], 404);
        }
        $publicacion->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado la publicacion correctamente.'], 200);
    }

}