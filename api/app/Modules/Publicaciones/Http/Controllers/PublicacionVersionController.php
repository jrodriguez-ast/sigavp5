<?php namespace App\Modules\Publicaciones\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\PublicacionVersion;
use yajra\Datatables\Datatables;
use App\Models\PublicacionVersionCondicion;

use Illuminate\Support\Facades\Validator;


use App\Models\Negociaciones;
use App\Models\NegociacionesServicios;
use App\Models\NegociacionesCondiciones;
use App\Models\NegociacionesCondicionesDetalles;
use App\Models\Publicacion;

class PublicacionVersionController extends Controller
{

    public function __construct()
    {
        $this->middleware('data.user');
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @param $publicacion_id
     * @return Response
     */
    public function store(Request $request, $publicacion_id)
    {
        //Id del usuario que esta haciendo la petición
        $currentUserId = session()->get('userId');

        //Numero de fallas al validar datos
        $fails = 0;
        $data = array();

        //Recorro versiones para evaluar inserción masiva
        if (count($request->versiones) > 0):

            foreach ($request->versiones as $key => $value):
                //Conformación de datos
                $data[$key] = array(
                    'orden_publicacion_id' => (int)$publicacion_id,
                    'servicio_id' => $request->servicio['id'],
                    'tipo_publicacion_name' => $request->tipoPublicacion['nombre'],
                    'tipo_publicacion_id' => $request->tipoPublicacion['id'],
                    'version_nombre' => (!empty($value['version_nombre']) ? $value['version_nombre'] : ''),
                    'version_segundos' => (!empty($value['version_segundos']) ? $value['version_segundos'] : 0),
                    'version_apariciones' => (!empty($value['version_apariciones']) ? $value['version_apariciones'] : 1),
                    'version_file' => (!empty($value['version_file']) ? $value['version_file'] : ''),
//                    'version_publicacion_fecha_inicio' => (!empty($value['version_publicacion_fecha_inicio']) ? $value['version_publicacion_fecha_inicio'] : ''),
//                    'version_publicacion_fecha_fin' => (!empty($value['version_publicacion_fecha_fin']) ? $value['version_publicacion_fecha_fin'] : ''),
                    'usuario_id' => $currentUserId,
                    'medio_id' => $request->medioId

                );
                $fecha_inicio = $fecha_fin = '';
                if (!empty($value['fecha_unica']) && $value['fecha_unica']) {
                    $fecha_fin = $fecha_inicio = (!empty($value['version_publicacion_fecha_inicio']) ? $value['version_publicacion_fecha_inicio'] : '');

                } else {
                    $fecha_inicio = (!empty($value['version_publicacion_fecha_inicio']) ? $value['version_publicacion_fecha_inicio'] : '');
                    $fecha_fin = (!empty($value['version_publicacion_fecha_fin']) ? $value['version_publicacion_fecha_fin'] : '');
                }
                $data[$key]['version_publicacion_fecha_inicio'] = $fecha_inicio;
                $data[$key]['version_publicacion_fecha_fin'] = $fecha_fin;
                if ($negociaciones = $this->get_negociaciones(
                    $request->medioId,
                    $request->servicio['id']

                )
                ):
                    $data[$key] = $this->check_negociacion(null, $negociaciones, $data[$key]);
                endif;
                $validator = [
                    'orden_publicacion_id' => 'required',
                    'tipo_publicacion_id' => 'required',
                    'servicio_id' => 'required',
                    'version_nombre' => 'required',
                    'usuario_id' => 'required'
                ];

                if ($request->tipoPublicacion['nombre'] == 'Televisivo' || $request->tipoPublicacion['nombre'] == 'Radial') {
                    $validator['version_apariciones'] = 'required';
                    $validator['version_segundos'] = 'required';
                }
                //Datos a evaluar
                $v = Validator::make($data[$key], $validator);
                //Evaluo datos
                if ($v->fails()) {
                    $fails++;
                }
            endforeach;
        endif;

        if ($fails == 0 && (count($data) > 0)):
            foreach ($data as $key => $value):
                try {
                    //Se inserta en la entidad PublicacionVersion
                    $PublicacionVersion = PublicacionVersion::create($value);

                    //Si la inserción es exitosa de agregan los detalles de la condición
                    if ($PublicacionVersion):
                        foreach ($request->add_condiciones as $key => $value):
                            $data_version_condicion = array(
                                'ordenes_publicacion_versiones_id' => $PublicacionVersion->id,
                                'detalle_condicion_id' => $value['detallecondicion']['id'],
                                'es_porcentaje' => $value['condicion']['es_porcentaje'],
                                'tarifa' => $value['detallecondicion']['tarifa'],
                                'usuario_id' => $currentUserId,
                                'condicion_id' => $value['condicion']['id'],
                                'es_base' => $value['condicion']['es_base'],
                                'data_extra' => (!empty($value['detallecondicion']['data_extra']) ? json_encode($value['detallecondicion']['data_extra']) : null)
                            );

                            if ($negociaciones = $this->get_negociaciones(
                                $request->medioId,
                                $request->servicio['id'],
                                $value['condicion']['id'],
                                $value['detallecondicion']['id']

                            )
                            ):

                                $data_version_condicion = $this->check_negociacion($value, $negociaciones, $data_version_condicion);
                            endif;
                            //Se inserta en la entidad PublicacionVersionCondicion
                            $PublicacionVersionCondicion = PublicacionVersionCondicion::create($data_version_condicion);
                        endforeach;
                        $DatosParaPublicacion = Publicacion::calculate_publicacion($publicacion_id);

                    endif;
                } catch (Exception $e) {
                    return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Error al procesar los datos1!']))], 422);
                }
            endforeach;
            return response()->json(array('status' => 'ok', 'data' => $PublicacionVersionCondicion, 'message' => "Medio - Servicio - Condiciones, agregados exitosamente!"), 200);
        else:
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Error al procesar los datos2!']))], 422);
        endif;
    }

    /**
     * Display the specified resource.
     *
     * @param $publicacion_id
     * @param $version_id
     * @return Response
     */
    public function show($publicacion_id, $version_id)
    {

        $PublicacionVersion = PublicacionVersion::version_and_condicion($version_id);


        if ($PublicacionVersion):
            //Verifico si la fecha es igual para establecer fecha unica en el frontEnd
            if (!empty($PublicacionVersion['version_publicacion_fecha_inicio']) && !empty($PublicacionVersion['version_publicacion_fecha_fin'])):
                $datetime1 = date_create($PublicacionVersion['version_publicacion_fecha_inicio']);
                $datetime2 = date_create($PublicacionVersion['version_publicacion_fecha_fin']);
                $interval = date_diff($datetime1, $datetime2);
                if ($interval->days == 0)
                    $PublicacionVersion['fecha_unica'] = true;
            endif;
        endif;
        return response()->json($PublicacionVersion, 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $publicacion_id
     * @param $publicacionVersionId
     * @return Response
     */
    public function update(Request $request, $publicacion_id, $publicacionVersionId)
    {
        //Id del usuario que esta haciendo la petición
        $currentUserId = session()->get('userId');
        $PublicacionVersion = PublicacionVersion::find($publicacionVersionId);
        //Numero de fallas al validar datos
        $fails = 0;
        $data = array();
        //Recorro versiones para evaluar inserción masiva
        if (count($request->versiones) > 0):

            foreach ($request->versiones as $key => $value):
                //Conformacion de datos

                $data[$key] = array(
                    'orden_publicacion_id' => (int)$publicacion_id,
                    'servicio_id' => $request->servicio['id'],
                    'tipo_publicacion_name' => $request->tipoPublicacion['nombre'],
                    'tipo_publicacion_id' => $request->tipoPublicacion['id'],
                    'version_nombre' => (!empty($value['version_nombre']) ? $value['version_nombre'] : ''),
                    'version_segundos' => (!empty($value['version_segundos']) ? $value['version_segundos'] : 0),
                    'version_apariciones' => (!empty($value['version_apariciones']) ? $value['version_apariciones'] : 1),
                    'version_file' => (!empty($value['version_file']) ? $value['version_file'] : ''),
//                    'version_publicacion_fecha_inicio' => (!empty($value['version_publicacion_fecha_inicio']) ? $value['version_publicacion_fecha_inicio'] : ''),
//                    'version_publicacion_fecha_fin' => (!empty($value['version_publicacion_fecha_fin']) ? $value['version_publicacion_fecha_fin'] : ''),
                    'usuario_id' => $currentUserId,
                    'medio_id' => $value['medio_id']

                );
                $fecha_inicio = $fecha_fin = '';
                if (!empty($value['fecha_unica']) && $value['fecha_unica']) {
                    $fecha_fin = $fecha_inicio = (!empty($value['version_publicacion_fecha_inicio']) ? $value['version_publicacion_fecha_inicio'] : '');

                } else {
                    $fecha_inicio = (!empty($value['version_publicacion_fecha_inicio']) ? $value['version_publicacion_fecha_inicio'] : '');
                    $fecha_fin = (!empty($value['version_publicacion_fecha_fin']) ? $value['version_publicacion_fecha_fin'] : '');
                }
                $data[$key]['version_publicacion_fecha_inicio'] = $fecha_inicio;
                $data[$key]['version_publicacion_fecha_fin'] = $fecha_fin;

                if ($negociaciones = $this->get_negociaciones(
                    $value['medio_id'],
                    $request->servicio['id']

                )
                ):
                    $data[$key] = $this->check_negociacion(null, $negociaciones, $data[$key]);
                endif;
                $validator = [
                    'orden_publicacion_id' => 'required',
                    'tipo_publicacion_id' => 'required',
                    'servicio_id' => 'required',
                    'version_nombre' => 'required',
                    'usuario_id' => 'required'
                ];

                if ($request->tipoPublicacion['nombre'] == 'Televisivo' || $request->tipoPublicacion['nombre'] == 'Radial') {
                    $validator['version_apariciones'] = 'required';
                    $validator['version_segundos'] = 'required';
                }
                //Datos a evaluar
                $v = Validator::make($data[$key], $validator);
                //Evaluo datos
                if ($v->fails()) {
                    $fails++;
                }
            endforeach;
        endif;

        if ($fails == 0 && (count($data) > 0)):
            $PublicacionVersionCondicion = PublicacionVersionCondicion::where('ordenes_publicacion_versiones_id', '=', $publicacionVersionId)->delete();
            foreach ($data as $key => $value):
                try {
                    $PublicacionVersion->orden_publicacion_id = $value['orden_publicacion_id'];
                    $PublicacionVersion->servicio_id = $value['servicio_id'];
                    $PublicacionVersion->version_nombre = $value['version_nombre'];
                    $PublicacionVersion->version_file = $value['version_file'];
                    $PublicacionVersion->version_publicacion_fecha_inicio = $value['version_publicacion_fecha_inicio'];
                    $PublicacionVersion->version_publicacion_fecha_fin = $value['version_publicacion_fecha_fin'];
                    $PublicacionVersion->tipo_publicacion_name = $value['tipo_publicacion_name'];
                    $PublicacionVersion->tipo_publicacion_id = $value['tipo_publicacion_id'];
                    $PublicacionVersion->version_segundos = $value['version_segundos'];
                    $PublicacionVersion->version_apariciones = $value['version_apariciones'];
                    $PublicacionVersion->medio_id = $value['medio_id'];
                    //Se inserta en la entidad CotizaciónServicio


                    //Si la inserción es exitosa de agregan los detalles de la condición
                    if ($PublicacionVersion->save()):
                        foreach ($request->add_condiciones as $key => $value):
                            $data_version_condicion = array(
                                'ordenes_publicacion_versiones_id' => $PublicacionVersion->id,
                                'detalle_condicion_id' => $value['detallecondicion']['id'],
                                'es_porcentaje' => $value['condicion']['es_porcentaje'],
                                'tarifa' => $value['detallecondicion']['tarifa'],
                                'usuario_id' => $currentUserId,
                                'condicion_id' => $value['condicion']['id'],
                                'es_base' => $value['condicion']['es_base'],
                                'data_extra' => (!empty($value['detallecondicion']['data_extra']) ? json_encode($value['detallecondicion']['data_extra']) : null)
                            );

                            if ($negociaciones = $this->get_negociaciones(
                                $request->medioId,
                                $request->servicio['id'],
                                $value['condicion']['id'],
                                $value['detallecondicion']['id']

                            )
                            ):

                                $data_version_condicion = $this->check_negociacion($value, $negociaciones, $data_version_condicion);
                            endif;
                            //Se inserta en la entidad PublicaciónVersionCondición
                            $PublicacionVersionCondicion = PublicacionVersionCondicion::create($data_version_condicion);
                        endforeach;
                    endif;

                } catch (Exception $e) {
                    return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Error al procesar los datos1!']))], 422);
                }
            endforeach;
            return response()->json(array('status' => 'ok', 'data' => $PublicacionVersionCondicion, 'message' => "Publicacion - Versiones - Condiciones, agregados exitosamente!"), 200);
        else:
            return response()->json([array('status' => 'fail', 'errors' => array(['code' => 422, 'message' => 'Error al procesar los datos2!']))], 422);
        endif;
    }


    /**
     * Método que se ejecuta para enviar los datos al datatable
     * @param Request $request
     * @return mixed
     */
    public function dtIndex(Request $request)
    {
        $publicacion_id = $request->publicacionId;
        $ordenes_publicacion_versiones = PublicacionVersion::leftJoin('public.servicios', 'public.ordenes_publicacion_versiones.servicio_id', '=', 'public.servicios.id')
            ->leftJoin('public.medios', 'public.servicios.medio_id', '=', 'public.medios.id')->leftJoin('public.tipos_publicaciones', 'public.servicios.tipo_publicacion_id', '=', 'public.tipos_publicaciones.id')
            ->where(array('orden_publicacion_id' => $publicacion_id))
            ->select(array(
                'public.medios.id',
                'public.medios.nombre_comercial as nombre_comercial',
                'public.tipos_publicaciones.nombre as tipo_publicacion',
                'servicio_id',
                'ordenes_publicacion_versiones.id as ordenes_publicacion_versiones',
                'public.servicios.nombre as servicio',
                'version_nombre',
                'version_file',
                'version_publicacion_fecha',
                'public.medios.razon_social as razon_social'
            ));

        return Datatables::of($ordenes_publicacion_versiones)
            ->editColumn('condiciones', function ($data) {
                $replace = array("{", "}");
                $data_format = str_replace($replace, "", $data->condiciones);
                return (!empty($data_format) ? $data_format : 'Sin Informacion');
            })
            ->editColumn('version_publicacion_fecha', function ($data) {
                $date = date_create($data->version_publicacion_fecha);
                return date_format($date, 'd/m/Y');
            })
            ->make(true);

    }

    public function get_negociaciones($medio_id = null, $servicio_id = null, $condicion_id = null, $condicion_detalle_id = null)
    {

        $response = false;
        if (empty($medio_id))
            return false;
        //Se verifica si existe negociacion
        $negociaciones = Negociaciones::where(array('medio_id' => $medio_id))->get();

        if (!$negociaciones->isEmpty()):
            $response = $negociaciones[0];
            if (!empty($servicio_id)):
                //Servicios negociados
                $negociaciones_servicios = NegociacionesServicios::where(
                    array(
                        'negociacion_id' => $negociaciones[0]['id'],
                        'servicio_id' => $servicio_id
                    )
                )->get();

                if (!$negociaciones_servicios->isEmpty())://Verifica si tiene servicio
                    $response['servicio'] = $negociaciones_servicios[0];
                    $response['servicio']['negociacion'] = true;

                endif;
            endif;

            if (!empty($condicion_id)):
                //Condiciones negociadas
                $negociaciones_condiciones = NegociacionesCondiciones::where(
                    array(
                        'negociacion_id' => $negociaciones[0]['id'],
                        'condicion_id' => $condicion_id
                    )
                )->get();

                if (!$negociaciones_condiciones->isEmpty()): //Verifica si hay condicion
                    $response['condicion'] = $negociaciones_condiciones[0];
                    $response['condicion']['negociacion'] = true;
                endif;
            endif;

            if (!empty($condicion_detalle_id)):
                $negociaciones_condiciones_detalle = NegociacionesCondicionesDetalles::where(
                    array(
                        'negociacion_id' => $negociaciones[0]['id'],
                        'condicion_detalle_id' => $condicion_detalle_id
                    )
                )->get();

                if (!$negociaciones_condiciones_detalle->isEmpty())://Verifica si hay condicion detalles
                    $response['detalle'] = $negociaciones_condiciones_detalle[0];
                    $response['detalle']['negociacion'] = true;
                endif;
            endif;
        endif;

        return $response;
    }


    public function check_negociacion($value = null, $negociaciones = null, $data_cotizacion_servicio_condicion = null)
    {


        $fecha_actual = strtotime(date("d-m-Y H:i:00", time()));
        $aplica_servicio = false;
        $poncentaje_servicio = 0.00;
        //Se evalua si esta activa la negociacion o esta vencida
        if (!empty($negociaciones) && strtotime($negociaciones['fecha_fin']) >= $fecha_actual):


            //Cuando es detalle
            if (!empty($negociaciones['detalle'])):
                if ($negociaciones['detalle']['estado']):
                    if ($negociaciones['detalle']['exonerado']):
                        $data_cotizacion_servicio_condicion['tarifa'] = '0.00';
                    else:
                        if ($value['condicion']['es_base']):
                            if (!$negociaciones['detalle']['es_porcentaje'] && $negociaciones['detalle']['sustituyo']):
                                $data_cotizacion_servicio_condicion['tarifa'] = $negociaciones['detalle']['monto'];
                            endif;
                        else:
                            if ($negociaciones['detalle']['sustituye'] && !$negociaciones['detalle']['es_porcentaje']):
                                $data_cotizacion_servicio_condicion['es_porcentaje'] = false;
                                $data_cotizacion_servicio_condicion['tarifa'] = $negociaciones['detalle']['monto'];
                            else:
                                if ($value['condicion']['es_porcentaje'] && $negociaciones['detalle']['es_porcentaje']):
                                    $data_cotizacion_servicio_condicion['es_porcentaje'] = true;
                                    $data_cotizacion_servicio_condicion['tarifa'] = ($data_cotizacion_servicio_condicion['tarifa'] - $negociaciones['detalle']['monto']);
                                elseif (!$value['condicion']['es_porcentaje'] && $negociaciones['detalle']['es_porcentaje']):
                                    $data_cotizacion_servicio_condicion['es_porcentaje'] = false;
                                    $porcentaje_aplicado = ($value['detalle']['tarifa'] * $negociaciones['detalle']['monto'] / 100);
                                    $data_cotizacion_servicio_condicion['tarifa'] = ($value['detalle']['tarifa'] - $porcentaje_aplicado);
                                endif;
                            endif;
                        endif;
                    endif;
                endif;
            endif;

            //Cuando es condicion
            if (!empty($negociaciones['condicion'])):
                if ($negociaciones['condicion']['estado']):
                    if ($negociaciones['condicion']['exonerado']):
                        $data_cotizacion_servicio_condicion['tarifa'] = '0.00';
                    endif;
                endif;

            endif;
        endif;

        //Verifico si existe servicio activo para aplicar
        if (!empty($negociaciones['servicio'])):
            if ($negociaciones['servicio']['estado']):
                if ($negociaciones['servicio']['exonerado']):
                    $data_cotizacion_servicio_condicion['tarifa'] = '0.00';
                else:
                    if ($negociaciones['servicio']['es_porcentaje']):
                        $data_cotizacion_servicio_condicion['monto_servicio'] = $negociaciones['servicio']['monto'];
                    endif;
                endif;
            endif;
        endif;

        return $data_cotizacion_servicio_condicion;

    }

    /**
     * Permite eliminar versiones de la publicacion de forma permanente
     *
     * @access public
     * @param $publicacionId Identificador de la Publicacion
     * @param $publicacionVersionId Identificador de la version de la publicacion
     * @return mixed
     * @author  Frederick D. Bustamante G. <frederickdanielb@gmail.com>
     * @version V 1.0 05/11/15 11:34 AM
     */
    public function destroy($publicacionId, $publicacionVersionId)
    {
        $PublicacionServicio = PublicacionVersion::find($publicacionVersionId);

        if ($PublicacionServicio) {
            $PublicacionServicio->versioncondicion()->delete();
            $PublicacionServicio->delete();
            $CalculoCotizacion = Publicacion::calculate_publicacion($publicacionId);
            $responsePublicacion = Publicacion::data_for_publicacion($publicacionId);
            return response()->json($responsePublicacion, 200);
        }
    }

}
