<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'publicaciones'], function () {

    Route::put('enviarPublicacion/{publicacionId}', 'PublicacionController@send_publicacion_medio');
    Route::get('dt', 'PublicacionController@dtIndex');
    Route::get('publicacion_versiones_dt', 'PublicacionVersionController@dtIndex');
    Route::get('publicacion_incidencias_dt', 'PublicacionIncidenciaController@dtIndex');
    Route::get('porfacturar', 'PublicacionController@noFacturadoIndex');
    Route::resource('publicacion', 'PublicacionController');
    Route::resource('publicacion.versiones', 'PublicacionVersionController');
    Route::resource('publicacion.incidencias', 'PublicacionIncidenciaController');
});