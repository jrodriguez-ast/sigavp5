<?php
namespace App\Modules\Publicaciones\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class PublicacionesServiceProvider extends ServiceProvider
{
    /**
     * Register the Publicaciones module service provider.
     *
     * @return void
     */
    public function register()
    {
        // This service provider is a convenient place to register your modules
        // services in the IoC container. If you wish, you may make additional
        // methods or service providers to keep the code more focused and granular.
        App::register('App\Modules\Publicaciones\Providers\RouteServiceProvider');

        $this->registerNamespaces();
    }

    /**
     * Register the Publicaciones module resource namespaces.
     *
     * @return void
     */
    protected function registerNamespaces()
    {
        Lang::addNamespace('publicaciones', realpath(__DIR__ . '/../Resources/Lang'));

        View::addNamespace('publicaciones', realpath(__DIR__ . '/../Resources/Views'));
    }
}
