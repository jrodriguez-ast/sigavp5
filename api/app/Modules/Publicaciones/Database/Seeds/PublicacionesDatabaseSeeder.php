<?php
namespace App\Modules\Publicaciones\Database\Seeds;

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class PublicacionesDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        // $this->call('App\Modules\Publicaciones\Database\Seeds\FoobarTableSeeder');
    }

}
