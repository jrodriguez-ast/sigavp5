<?php
namespace App\Modules\Localidad\Providers;

use App;
use Config;
use Lang;
use View;
use Illuminate\Support\ServiceProvider;

class LocalidadServiceProvider extends ServiceProvider
{
	/**
	 * Register the Localidad module service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		// This service provider is a convenient place to register your modules
		// services in the IoC container. If you wish, you may make additional
		// methods or service providers to keep the code more focused and granular.
		App::register('App\Modules\Localidad\Providers\RouteServiceProvider');

		$this->registerNamespaces();
	}

	/**
	 * Register the Localidad module resource namespaces.
	 *
	 * @return void
	 */
	protected function registerNamespaces()
	{
		Lang::addNamespace('localidad', realpath(__DIR__.'/../Resources/Lang'));

		View::addNamespace('localidad', realpath(__DIR__.'/../Resources/Views'));
	}
}
