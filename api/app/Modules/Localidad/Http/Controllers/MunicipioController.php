<?php
namespace App\Modules\Localidad\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\Municipio;
use App\Models\Servicio;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class MunicipioController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index ()
    {
        $municipio = Municipio::all ();

        return response ()->json (['status' => 'ok', 'data' => $municipio], 200);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex ()
    {
        $municipio = Municipio::all ();
        return Datatables::of ($municipio)->make (true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store (Request $request)
    {
        $nuevomunicipio = array(
            'nombre' => $request->input ('nombre'),
            'estado_id' => $request->input ('estado_id'),
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge ($nuevomunicipio, $row);
        $municipio= Municipio::create ($row);

        return response ()->json (['status' => 'ok', 'data' => $municipio], 201);

    }


    /**
     * Display the specified resource.
     *
     * @param $municipio_id
     * @return Response
     * @internal param int $id
     */
    public function show ($municipio_id)
    {

        $municipio = Municipio::find ($municipio_id);

        if (!$municipio) {
            return response ()->json (['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un municipio con ese código.'])], 404);
        }

        return response ()->json (['status' => 'ok', 'data' => $municipio], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $municipio_id
     * @return Response
     * @internal param int $id
     */
    public function update (Request $request, $municipio_id)
    {
        //var_dump($request->all());die;
        $municipio = Municipio::find ($municipio_id);
        if (!$municipio) {
            return response ()->json (['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un municipio con ese código.'])], 404);
        }
        $nombre = $request->input ('nombre');
        $estado_id = $request->input ('estado_id');

        $bandera = false;
        if ($nombre) {
            $municipio->nombre = $nombre;
            $bandera = true;
        }
        if ($estado_id) {
            $municipio->estado_id = $estado_id;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $municipio->usuario_id = $user->findByName ($request->header ('username'), true)->id;
            $municipio->save ();
            return response ()->json (['status' => 'ok', 'data' => $municipio], 200);
        } else {
            return response ()->json (['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún pais.'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @internal param int $id
     */
    public function destroy($id)
    {
        $municipio = Municipio::find($id);
        if (!$municipio) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un municipio con ese código.'])], 404);
        }

        $medios=Servicio::all()->toArray();
        foreach($medios as $medio){
            $json=$medio['otros_detalles'];
            $array=json_decode($json, true);
            if (isset($array[0]['alcance'])) {
                if(count($array[0]['alcance']) > 0){
                    $id_municipio=$array[0]['alcance'][0]['municipio']['id'];

                    if ($id_municipio === 0) {
                        return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este municipio está asociado a un servicio.'], 404);
                    }

                    if ($id_municipio == $id) {
                        return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este municipio está asociado a un servicio.'], 404);
                    }
                }
            }
        }

        $municipio->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el municipio correctamente.'], 200);
    }

}