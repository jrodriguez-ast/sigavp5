<?php
namespace App\Modules\Localidad\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\Pais;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class PaisController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return Response
     * @internal param $idpais
     */
    public function index ()
    {
        $pais = Pais::all ();

        return response ()->json (['status' => 'ok', 'data' => $pais], 200);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex ()
    {
        $pais = Pais::all();
        return Datatables::of($pais)->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store (Request $request)
    {
        $nuevoPais = array(
            'nombre' => $request->input('nombre'),
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge ($nuevoPais, $row);
        $pais = Pais::create ($row);

        return response ()->json (['status' => 'ok', 'data' => $pais], 201);

    }


    /**
     * Display the specified resource.
     *
     * @param $idpais
     * @return Response
     * @internal param int $id
     */
    public function show ($idpais)
    {

        $pais = Pais::find ($idpais);

        if (!$pais) {
            return response ()->json (['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un pais con ese código.'])], 404);
        }

        return response ()->json (['status' => 'ok', 'data' => $pais], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $idpais
     * @return Response
     * @internal param int $id
     */
    public function update (Request $request, $idpais)
    {
        //var_dump($request->all());die;
        $pais = Pais::find ($idpais);
        if (!$pais) {
            return response ()->json (['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $nombre = $request->input ('nombre');

        $bandera = false;
        if ($nombre) {
            $pais->nombre = $nombre;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $pais->usuario_id = $user->findByName ($request->header ('username'), true)->id;
            $pais->save ();
            return response ()->json (['status' => 'ok', 'data' => $pais], 200);
        } else {
            return response ()->json (['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún pais.'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @internal param int $id
     */
    public function destroy($id)
    {
        $pais = Pais::find($id);
        if (!$pais) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'No se encuentra un país con ese código.'], 200);
        }

        if ($pais->estado()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este país tiene estados asociados.'], 200);
        }

        $pais->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el país correctamente.'], 200);
    }

}
