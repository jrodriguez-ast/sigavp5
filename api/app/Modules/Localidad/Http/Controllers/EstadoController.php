<?php
namespace App\Modules\Localidad\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Models\Acl\User;
use App\Models\Estado;
use App\Models\Servicio;
use Illuminate\Http\Request;
use yajra\Datatables\Datatables;

class EstadoController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $estado = Estado::all();

        return response()->json(['status' => 'ok', 'data' => $estado], 200);
    }


    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function dtIndex()
    {
        $estado = Estado::all();
        return Datatables::of($estado)->make(true);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return Response
     */
    public function store(Request $request)
    {
        $nuevoEstado = array(
            'nombre' => $request->input('nombre'),
            'pais_id' => $request->input('pais_id'),
        );
        $user = new User();
        $row['usuario_id'] = $user->findByName($request->header('username'), true)->id;
        $row = array_merge($nuevoEstado, $row);
        $estado = Estado::create($row);

        return response()->json(['status' => 'ok', 'data' => $estado], 201);

    }


    /**
     * Display the specified resource.
     *
     * @param $estado_id
     * @return Response
     * @internal param int $id
     */
    public function show($estado_id)
    {

        $estado = Estado::with(
            [
                'municipio'
            ]
        )->find($estado_id);

        if (!$estado) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un estado con ese código.'])], 404);
        }

        return response()->json(['status' => 'ok', 'data' => $estado], 200);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $estado_id
     * @return Response
     * @internal param int $id
     */
    public function update(Request $request, $estado_id)
    {
        //var_dump($request->all());die;
        $estado = Estado::find($estado_id);
        if (!$estado) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un estado con ese código.'])], 404);
        }
        $nombre = $request->input('nombre');
        $pais_id = $request->input('pais_id');

        $bandera = false;
        if ($nombre) {
            $estado->nombre = $nombre;
            $bandera = true;
        }
        if ($pais_id) {
            $estado->pais_id = $pais_id;
            $bandera = true;
        }
        if ($bandera) {
            $user = new User();
            $estado->usuario_id = $user->findByName($request->header('username'), true)->id;
            $estado->save();
            return response()->json(['status' => 'ok', 'data' => $estado], 200);
        } else {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 304, 'message' => 'No se ha modificado ningún pais.'])], 304);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @internal param int $id
     */
    public function destroy($id)
    {
        $estado = Estado::find($id);
        if (!$estado) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un estado con ese código.'])], 404);
        }

        $medios=Servicio::all()->toArray();
        foreach($medios as $medio){
            $json=$medio['otros_detalles'];
            $array=json_decode($json, true);
            if (isset($array[0]['alcance'])) {
                if(count($array[0]['alcance']) > 0){
                    $id_estado=$array[0]['alcance'][0]['estado']['id'];

                    if ($id_estado == 0) {
                        return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este estado está asociado a un servicio.'], 404);
                    }

                    if ($id_estado == $id) {
                        return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este estado está asociado a un servicio.'], 404);
                    }
                }
            }
        }

        if ($estado->municipio()->count() > 0) {
            return response()->json(['status' => 'fail', 'errors' => true, 'code' => 404, 'message' => 'Este estado tiene municipios asociados.'], 404);
        }

        $estado->delete();
        return response()->json(['status' => 'ok', 'code' => 204, 'message' => 'Se ha eliminado el estado correctamente.'], 200);
    }

}