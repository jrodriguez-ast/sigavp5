<?php

/*
|--------------------------------------------------------------------------
| Module Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for the module.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(['prefix' => 'localidad'], function () {

    //pais
    Route::resource('pais', 'PaisController');
    Route::get('pais_dt', 'PaisController@dtIndex');

    //estado
    Route::resource('estado', 'EstadoController');
    Route::get('estado_dt', 'EstadoController@dtIndex');

    //municipio
    Route::resource('municipio', 'MunicipioController');
    Route::get('municipio_dt', 'MunicipioController@dtIndex');

});