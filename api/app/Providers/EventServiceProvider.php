<?php namespace App\Providers;

use App\Models\Balance;
use App\Models\Cliente;
use App\Models\ContactoCliente;
use App\Models\ContactoMedio;
use App\Models\Contrato;
use App\Models\Cotizacion;
use App\Models\CotizacionOrdenServicio;
use App\Models\CotizacionServicio;
use App\Models\CotizacionServicioCondicion;
use App\Models\Iva;
use App\Models\Medio;
use App\Models\MediosMedios;
use App\Models\Observers\AuditoriaObserver;
use App\Models\Publicacion;
use App\Models\PublicacionEstatusEstatus;
use App\Models\PublicacionVersion;
use App\Models\PublicacionVersionCondicion;
use App\Models\RecargoGestion;
use App\Models\Servicio;
use App\Models\TipoIncidencia;
use App\Models\TipoPublicacion;

use Illuminate\Contracts\Events\Dispatcher as DispatcherContract;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class EventServiceProvider extends ServiceProvider
{

    /**
     * The event handler mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'event.name' => [
            'EventListener',
        ],
    ];

    /**
     * Register any other events for your application.
     *
     * @param  \Illuminate\Contracts\Events\Dispatcher $events
     * @return void
     */
    public function boot(DispatcherContract $events)
    {
        parent::boot($events);
        Balance::observe(new AuditoriaObserver);
        Cliente::observe(new AuditoriaObserver);
        ContactoCliente::observe(new AuditoriaObserver);

        Medio::observe(new AuditoriaObserver);
        ContactoMedio::observe(new AuditoriaObserver);
        Servicio::observe(new AuditoriaObserver);

        Contrato::observe(new AuditoriaObserver);

        CotizacionServicioCondicion::observe(new AuditoriaObserver);
        CotizacionServicio::observe(new AuditoriaObserver);
        Cotizacion::observe(new AuditoriaObserver);
        CotizacionOrdenServicio::observe(new AuditoriaObserver);
        Iva::observe(new AuditoriaObserver);
        RecargoGestion::observe(new AuditoriaObserver);
        TipoIncidencia::observe(new AuditoriaObserver);
        MediosMedios::observe(new AuditoriaObserver);
        Publicacion::observe(new AuditoriaObserver);
        PublicacionVersion::observe(new AuditoriaObserver);
        PublicacionVersionCondicion::observe(new AuditoriaObserver);
        TipoPublicacion::observe(new AuditoriaObserver);
        PublicacionEstatusEstatus::observe(new AuditoriaObserver);
    }

}
