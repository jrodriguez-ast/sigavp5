<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeticionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('peticiones', function(Blueprint $table)
		{
			$table->increments('id');
			$table->timestamps();
            $table->string('numero_peticion','25');
            $table->string('numero_proceso','25');
            $table->string('comprador_id','25');
            $table->date('fecha_recepcion');
            $table->date('fecha_culminacion');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('peticions');
	}

}
