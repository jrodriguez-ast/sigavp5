<?php

use App\Models\Consulta;
use App\Models\TipoPublicacion;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

// Hace uso del modelo de Fabricante.
use App\Models\Medio;

// Le indicamos que utilice también Faker.
// Información sobre Faker: https://github.com/fzaninotto/Faker
use Faker\Factory as Faker;

class MediosSeeder extends Seeder
{
    private $faker = null;
    private $obj = null;

    public function __construct()
    {
        $this->faker = Faker::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creamos una instancia de Faker
        DB::statement('TRUNCATE TABLE public.medios RESTART IDENTITY CASCADE;');
        $this->createMedio();
    }

    private function getInitials($name, $case = 'L')
    {
        if (empty($name)) {
            return $name;
        }

        $initials = $name;
        $splitted_name = explode(' ', trim($name));
        if (!empty($splitted_name)) {
            $initials = '';
            foreach ($splitted_name as $row)
                $initials .= substr(trim($row), 0, 1);
        }
        return ($case == 'U') ? strtoupper($initials) : strtolower($initials);
    }

    private function createMedio()
    {
        $proveedores = [
            ['rif' => 'J-31531203-4',],
            ['rif' => 'J-40037143-0',],
            ['rif' => 'J-00008933-7',],
            ['rif' => 'J-29852618-1',],
            ['rif' => 'J-31481428-1',],
            ['rif' => 'J-29518458-1',],
            ['rif' => 'J-40312603-8',],
            ['rif' => 'J-30340800-1',],
            ['rif' => 'J-40015383-2',],
            ['rif' => 'J-40089412-3',],
            ['rif' => 'J-29590570-0',],
            ['rif' => 'J-31755797-2',],
            ['rif' => 'J-31042455-1',],
            ['rif' => 'J-29721772-0',],
            ['rif' => 'J-00301997-6',],
            ['rif' => 'J-29721772-0',],
            ['rif' => 'J-00301997-6',],
            ['rif' => 'J-29923541-5',],
            ['rif' => 'J-31755146-0',],
            ['rif' => 'J-30000493-7',],
            ['rif' => 'J-29623867-7',],
            ['rif' => 'J-30710895-9',],
            ['rif' => 'J-30077927-0',],
            ['rif' => 'J-29703623-7',],
            ['rif' => 'J-40220457-4',],
            ['rif' => 'J-30757852-1',],
            ['rif' => 'J-00208802-8',],
            ['rif' => 'J-31061397-4',],
            ['rif' => 'G-20008293-3',],
            ['rif' => 'G-20004500-0',],
            ['rif' => 'J-31032585-5',],
            ['rif' => 'G-20009068-5',],
            ['rif' => 'J-40344283-5',],
            ['rif' => 'G-20009525-3',],
            ['rif' => 'V-03174769-0',],
            ['rif' => 'J-31433099-3',]
        ];

        $consulta = new Consulta();
        foreach ($proveedores as $proveedor) {
            $rif = trim($proveedor['rif']);

            if (Medio::where('rif', '=', $rif)->exists()) {
                continue;
            }

            $medio = [];
            $medio['seniat'] = $consulta->getSeniat(str_replace('-', '', $rif));
            $medio['rnc'] = $consulta->getDataRNC(str_replace('-', '', $rif));
            $medio['solvencia'] = $consulta->getSolvencia($rif);
            $medio['razon_social'] = (!empty($medio['seniat']['nombre'])) ? $medio['seniat']['nombre'] : $medio['rnc']['razon_social'];
            $medio['nombre_comercial'] = $medio['razon_social'];
            $medio['rif'] = $rif;
            $medio['differentiation_chain'] = $this->getInitials($medio['razon_social'], 'U');
            $medio['usuario_id'] = 1;

            $medio['direccion_fiscal'] = $this->faker->address();
            $medio['sitio_web'] = $this->faker->domainName();
            $medio['fecha_fundacion'] = $this->faker->date();
            //asi debe ser el formato de correos y telefonos
            $medio['correos_electronicos'] = json_encode([array('a'=>$this->faker->email())]);
            $medio['telefonos'] = json_encode([array('a'=>$this->faker->phoneNumber())]);
            $cliente['img_medio'] = $this->faker->imageUrl(640, 480, 'technics');

            unset($medio['seniat']['nombre']);

            $medio['seniat'] = json_encode($medio['seniat']);
            $medio['rnc'] = json_encode($medio['rnc']);
            $medio['solvencia'] = json_encode($medio['solvencia']);

            $this->obj = Medio::create($medio);
            $this->createContactos(3);
            $this->createServicio(2);

            $this->command->info("Creado el Proveedor #{$this->obj->id} con 3 Contactos y 2 Medios, cada medio con 3 Tarifas y cada tarifa con 1 valor.");
        }
    }

    private function createContactos($len)
    {
        for ($i = 0; $i < $len; $i++) {
            $tipo = 'administrativo';
            if ($i == 1) {
                $tipo = 'publicitario';
            } elseif ($i > 1) {
                $tipo = "otro tipo";
            }

            $contacto = [
                'nombre' => $this->faker->name,
                'apellido' => $this->faker->lastName,
                'direccion' => $this->faker->address,
                'usuario_id' => 1,
                'es_interno' => true,
                'cargo_id' => $this->faker->numberBetween(1, 67),
                //asi debe ser el formato de correos y telefonos
                'telefonos' => json_encode([array('a'=>$this->faker->phoneNumber())]),
                'correos_electronicos' => json_encode([array('a'=>$this->faker->email())]),
                'tipo' => $tipo,
            ];
            $this->obj->contactos()->create($contacto);
        }
    }

    private function createServicio($len)
    {
        // Crear Servicios
        for ($i = 0; $i < $len; $i++) {
            $tipo = TipoPublicacion::find($this->faker->numberBetween(1, 5));

            $servicio = [
                'tipo_publicacion_id' => $tipo->id,
                'nombre' => $this->faker->bothify("{$tipo->nombre} ???-##"),
                'descripcion' => $this->faker->sentence(10),
                'usuario_id' => 1,
            ];

            $servicio = $this->obj->servicios()->create($servicio);

            for ($j = 0, $len = 2; $j < $len; $j++) {
                $condicion = [
                    'usuario_id' => 1,
                    'es_porcentaje' => 0,
                    'es_base' => ($j == 0),
                    'nombre' => $this->faker->word
                ];

                $detalle = [
                    'usuario_id' => 1,
                    'tarifa' => $this->faker->randomFloat(2, 10, 1000),
                    'nombre' => $this->faker->word
                ];

                $condicion = $servicio->condiciones()->create($condicion);
                $condicion->detalles()->create($detalle);
            }
        }
    }

}