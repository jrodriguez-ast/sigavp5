<?php

use App\Models\Cliente;
use App\Models\ContratoCuotas;
use App\Models\ContratoPauta;
use App\Models\Cotizacion;
use App\Models\CotizacionServicio;
use App\Models\CotizacionServicioCondicion;
use App\Models\Iva;
use App\Models\Publicacion;
use App\Models\PublicacionVersion;
use App\Models\RecargoGestion;
use App\Models\TipoContrato;
use App\Models\Utilities\GenericConfiguration;
use Carbon\Carbon;
use Faker\Factory as Faker;
use Illuminate\Database\Seeder;

// Le indicamos que utilice también Faker.
// Información sobre Faker: https://github.com/fzaninotto/Faker

class CotSeeder extends Seeder
{
    private $faker = null;
    private $user_id = 1;
    private $cliente_id = 7;

    public function __construct()
    {
        $this->faker = Faker::create();
    }

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contrato_id = $this->storeContrato($this->buildContrato(), $this->cliente_id);
        $cotizacion_id = $this->storeCotizacion($this->cliente_id);
        $this->storeCotizacionVersion($this->buildVersiones($cotizacion_id), $cotizacion_id);
        $this->updateCotizacion($cotizacion_id, $contrato_id);
        $this->publicar($cotizacion_id);
    }

    private function storeContrato($request, $cliente_id)
    {
        $cliente = Cliente::find($cliente_id);
        if (!$cliente) {
            return response()->json(['status' => 'fail', 'errors' => array(['code' => 404, 'message' => 'No se encuentra un cliente con ese código.'])], 404);
        }
        $nuevoContrat = [
            'firmante_cargo' => GenericConfiguration::where('_label', 'contrato_firmante_cargo')->pluck('value'),
            'firmante_nombre' => GenericConfiguration::where('_label', 'contrato_firmante_nombre')->pluck('value'),
            "cliente_id" => $cliente_id,
            "tarifa" => $request['tarifa'],
            "monto_iva" => $request['monto_iva'],
            "usuario_id" => $this->user_id,
            'fecha_inicio' => $request['fecha_inicio'],
            'fecha_fin' => $request['fecha_fin'],
            'fecha_creacion' => $request['fecha_creacion'],
            'por_publicacion' => $request['por_publicacion'],
            'descripcion' => $request['descripcion'],
            'codigo' => $request['codigo'],
        ];
        $tipoCOntrato = TipoContrato::where('nombre', 'addendum')->pluck('id');
        if ($request['tipo_contrato_id']) {
            $nuevoContrat['tipo_contrato_id'] = $request['tipo_contrato_id'];
        }
        if (isset($request['contratos_id']) && $request['tipo_contrato_id'] == $tipoCOntrato) {
            $nuevoContrat['contratos_id'] = $request['contratos_id'];
        } elseif (isset($request['contratos_id']) && $request['tipo_contrato_id'] != $tipoCOntrato) {
            unset($nuevoContrat['contratos_id']);
        }

        $nuevoContrato = $cliente->contratos()->create($nuevoContrat);
        $tmp = ['pautas' => null, 'cuotas' => null];

        //if($request->has('pautas')) {
        foreach ($request['pautas'] as $key => $value) {
            $pautas[] = new ContratoPauta($value);
        }
        $tmp['pautas'] = $nuevoContrato->pautas()->saveMany($pautas);
        //}

        if (!$request['por_publicacion']) {
            foreach ($request['cuotas'] as $key => $value) {
                $cuotas[] = new ContratoCuotas($value);
            }

            $tmp['cuotas'] = $nuevoContrato->cuotas()->saveMany($cuotas);
        }

        $nuevoContrato->balancear = '1';
        $nuevoContrato->save();

        $this->command->info("Creado Contrato con el Código: {$nuevoContrato->codigo}.");
        return $nuevoContrato->id;
    }

    private function storeCotizacion($cliente_id)
    {
        $p_iva = Iva::where('activo', true)->firstOrFail();
        $p_avp = RecargoGestion::where('activo', true)->firstOrFail();
        $dias_validez = GenericConfiguration::where('_label', 'cotizacion_validez')->pluck('value');
        $dias_validez = ($dias_validez ? $dias_validez : 15);

        //Preparación de datos que se insertaran en la creación de la cotización
        $defaultCotizacion = [
            "cliente_id" => $cliente_id,
            "fecha_creacion" => Carbon::now()->toDateTimeString(),
            "fecha_expiracion" => Carbon::now()->addDay($dias_validez)->toDateTimeString(),
            "descuento" => 0,
            "con_impuestos" => false,
            "porcentaje_impuesto" => (float)$p_iva->iva,
            'porcentaje_avp' => (float)$p_avp->porcentaje,
            'usuario_id' => 1,
            'firmante_nombre' => GenericConfiguration::where('_label', 'cotizacion_firmante_nombre')->pluck('value'),
            'firmante_cargo' => GenericConfiguration::where('_label', 'cotizacion_firmante_cargo')->pluck('value')
        ];

        //Se eliminan las cotizaciones Sin Finalizar asociadas al cliente
        $this->destroy_cotizaciones_pendientes($cliente_id);

        //Se crea la cotización nueva
        $cotizacion = Cotizacion::create($defaultCotizacion);
        return $cotizacion->id;
    }

    private function destroy_cotizaciones_pendientes($cliente_id = null)
    {
        if (empty($cliente_id))
            return false;

        $cliente = Cliente::with(['cotizaciones' => function ($q) {
            $q->where('es_terminada', false);
        }])->find($cliente_id);

        if (!$cliente->cotizaciones->isEmpty()) {
            foreach ($cliente['cotizaciones'] as $key => $value):
                Cotizacion::destroy($value['id']);
            endforeach;
        }
    }

    public function storeCotizacionVersion($request, $cotizacion_id)
    {
        foreach ($request as $versiones):
            //Se inserta en la entidad CotizaciónServicio
            $condiciones = $versiones['condiciones'];
            unset($versiones['condiciones']);
            $version = CotizacionServicio::create($versiones);
            $version_condiciones = new CotizacionServicioCondicion();

            //Si la inserción es exitosa de agregan los detalles de la condición
            if ($version):

                foreach ($condiciones as $condicion):
//                    d($condicion);
//                    $this->command->info("---");
                    $data_cotizacion_servicio_condicion = array(
                        'cotizaciones_servicios_id' => $version->id,
                        'detalle_condicion_id' => $condicion['detalle']['id'],
                        'es_porcentaje' => $condicion['condicion']['es_porcentaje'],
                        'tarifa' => $condicion['detalle']['tarifa'],
                        'usuario_id' => $this->user_id,
                        'condicion_id' => $condicion['condicion']['id'],
                        'es_base' => $condicion['condicion']['es_base'],
                        'data_extra' => (!empty($condicion['detalle']['data_extra']) ? json_encode($condicion['detalle']['data_extra']) : null)
                    );

                    $version_condiciones->create($data_cotizacion_servicio_condicion);
                endforeach;

                $CotizacionServicio_update = CotizacionServicio::find($version->id);
                $monto_version = $version_condiciones->calcularValorConRecargos($version->id, true);
                $CotizacionServicio_update->monto_servicio = (empty($monto_version)) ? 0 : $monto_version['con_tiempo'];

                // Se incluye el tiempo xq lo que se desea es calcular el precio por dia.
                $CotizacionServicio_update->valor_con_recargo = (empty($monto_version)) ? 0 : $monto_version['sin_tiempo'];

                $CotizacionServicio_update->save();
                Cotizacion::calculate_quote($cotizacion_id, $this->user_id);
            endif;
        endforeach;
    }

    private function buildVersiones($cotizacion_id)
    {
        $tipo = \App\Models\TipoPublicacion::find(4);
        return [
            [
                'cotizacion_id' => (int)$cotizacion_id,
                'servicio_id' => 93,
                'tipo_publicacion_name' => $tipo->nombre,
                'tipo_publicacion_id' => $tipo->id,
                'version_nombre' => $this->faker->name,
                'version_segundos' => 0,
                'version_apariciones' => 1,
                'version_file' => '',
                'usuario_id' => $this->user_id,
                'medio_id' => 11,
                'version_publicacion_fecha_inicio' => Carbon::create()->addDay(),
                'version_publicacion_fecha_fin' => Carbon::create()->addDay(),
//                'version_publicacion_fecha_fin' => Carbon::create()->addDay(5),
                'aprobada' => true,
                'condiciones' => [
                    [
                        'detalle' => ['id' => 251, 'tarifa' => 200.00],
                        'condicion' => ['id' => 199, 'es_porcentaje' => '0', 'es_base' => '1'],
                    ],
                    [
                        'detalle' => ['id' => 502, 'tarifa' => 10.00],
                        'condicion' => ['id' => 200, 'es_porcentaje' => '1', 'es_base' => '0'],
                    ]
                ]
            ],
        ];
    }

    private function buildContrato()
    {
        $monto_pub = 5000;
        $tarifa = $monto_pub * 1.10;
        $iva = $tarifa * 0.12;
        return [
            'tarifa' => $tarifa,
            'monto_iva' => $iva,
            'usuario_id' => $this->user_id,
            'codigo' => 'Seeder ->' . $this->faker->randomNumber(6),
            'descripcion' => $this->faker->address,
            'por_publicacion' => 0,
            'fecha_inicio' => Carbon::create(),
            'fecha_creacion' => Carbon::create(),
            'fecha_fin' => Carbon::create()->addDay(30),
            'tipo_contrato_id' => 3,
            'pautas' => [
                ['cantidad' => 1, 'monto' => $tarifa, 'monto_iva' => $iva, 'tipo_publicacion_id' => 4]
            ],
            'cuotas' => [
                ['fecha' => '2016-01-22', 'monto' => $tarifa, 'monto_iva' => $iva, 'numero_cuota' => 1]
            ],
        ];
    }

    private function updateCotizacion($cotizacion_id, $contrato_id)
    {
        $cotizacion = Cotizacion::find($cotizacion_id);
        $cotizacion->es_terminada = true;
        $cotizacion->create_code = true;
        $cotizacion->contrato_id = $contrato_id;
        $cotizacion->aprobada = true;
        $cotizacion->save();

        $cotizacion = Cotizacion::find($cotizacion_id);
        $this->command->info("Aprobada Cotización con el Código: {$cotizacion->codigo}.");
    }

    private function publicar($cotizacion_id)
    {
        $cotizacion = Cotizacion::with(['orden_publicacion.version'])->find($cotizacion_id)->toArray();
        foreach($cotizacion['orden_publicacion'] as $cOrden){
            foreach($cOrden['version'] as $cVersion){
                $version = PublicacionVersion::find($cVersion['id']);
                $version->publicado = '1';
                $version->save();
            }
            $orden = Publicacion::find($cOrden['id']);
            $orden->publicada = '1';
            $orden->save();

            $this->command->info("Publicación con el Código: {$orden->codigo} fue Publicada.");
        }
    }
}