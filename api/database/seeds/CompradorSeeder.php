<?php

use App\Models\Importaciones\Comprador;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

// Le indicamos que utilice también Faker.
// Información sobre Faker: https://github.com/fzaninotto/Faker
use Faker\Factory as Faker;

class CompradorSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('TRUNCATE TABLE public.compradores RESTART IDENTITY CASCADE;');
        $faker = Faker::create();
        for ($i = 0; $i < 2; $i++) {
            $arr = [
                'nombre' => $faker->name(),
                'code' => $faker->lexify('??')
            ];
            Comprador::create($arr);
        }

    }
}