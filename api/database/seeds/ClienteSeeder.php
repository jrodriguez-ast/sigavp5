<?php

use App\Models\Consulta;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

// Hace uso del modelo de Fabricante.
use App\Models\Cliente;

// Le indicamos que utilice también Faker.
// Información sobre Faker: https://github.com/fzaninotto/Faker
use Faker\Factory as Faker;

class ClienteSeeder extends Seeder
{

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        DB::statement('TRUNCATE TABLE public.clientes RESTART IDENTITY CASCADE;');
        $rifs_Clientes = [
            'G-20000212-3',
            'G-20000025-2',
            'G-20005795-5',
            'G-20009148-7',
            'G-20000110-0',
            'G-20004132-3',
            'G-20004752-6',
            'G-20005187-6',
            'G-20010676-0',
            'G-20000085-6',
            'J-29759907-0',
            'J-00103909-0',
            'J-00092863-0',
            'G-20001376-1',
            'G-20000826-1',
            'J-00124134-5',
            'G-20000027-9',
            'G-20000683-8',
            'G-20000026-0',
            'G-20010014-1',
            'J-00008933-7',
            'J-00092504-6',
            'G-20001032-0',
            'G-20000028-7',
            'G-20004335-0',
            'G-20003241-3',
            'G-20000329-4',
            'G-20007861-8',
            'G-20006289-4',
            'G-20003583-8',
            'G-20006816-7',
            'G-20008056-6',
            'G-20004433-0',
            'G-20003871-3',
            'G-20000117-8',
            'G-20000044-9',
            'G-20007687-9',
            'G-20007752-2',
            'G-20008816-8',
            'G-20000128-3',
            'G-20004236-2',
            'G-20000076-7',
            'G-20000058-9',
            'G-20001823-2',
            'G-20000124-0',
            'G-20002838-6',
            'G-20000024-4',
            'G-20000024-4',
            'G-20000101-1',
            'G-20000095-3',
            'G-20003437-8',
            'G-20000438-0',
            'G-20003275-8',
            'G-20004247-8',
            'G-20002387-2',
            'G-20002414-3',
            'G-20002414-3',
            'G-20004206-0',
            'G-20004829-8',
            'G-20003275-8',
            'G-20004500-0',
            'G-20003591-9',
//            '',
            'G-20001428-8',
            'G-20010006-0',
            'G-20000408-8',
            'G-20000015-5',
            'G-20000012-0',
            'G-20004327-0',
            'G-20003090-9',
            'G-20000004-0',
            'G-20004448-9',
            'G-20010531-3',
            'G-20000009-0',
            'G-20009109-6',
            'G-20010046-0',
            'G-20011315-4',
            'G-20008830-3',
            'G-20008795-1',
            'G-20008771-4',
            'G-20000003-1',
            'G-20005466-2',
            'G-20001830-5',
            'G-20000001-5',
            'G-20000002-3',
            'G-20010006-0',
            'G-20010010-9',
            'G-20009490-7',
            'J-00076727-0',
            'J-00095036-9',
            'G-20000107-0',
            'G-20000387-1',
            'G-20008387-5',
            'G-20001596-9',
            'G-20000733-8',
            'G-20004680-5',
            'G-20000303-0',
            'G-20002451-8',
            'G-20007749-2',
            'G-20008471-5',
            'G-20004291-5',
            'G-20004131-5',
            'G-20008047-7',
            'G-20004036-0',
            'J-30000493-7',
            'G-20000030-9',
            'G-20000063-5',
            'G-20001663-9',
            'J-29535597-1',
            'G-20004495-0',
            'G-20007774-3',
            'G-20000632-3',
//            'F/A',
            'G-20004076-9',
            'G-20011296-4',
            'G-20009381-1',
            'J-00111491-2',
            'G-20005357-7',
            'G-20009900-3',
            'G-20010253-5',
            'G-20002311-2',
            'G-20009922-4',
            'G-20007785-9',
            'G-20004640-6',
            'J-30474202-9',
            'G-20010522-4',
            'G-20011050-3',
            'G-20010532-1',
            'G-20008433-2',
            'G-20009043-0',
            'G-20000064-3',
            'G-20008029-9',
            'G-20010949-1',
            'J-30568342-5',
            'G-20004755-0',
            'G-20010609-3',
            'G-20008280-1',
            'G-20000092-9',
            'G-20009997-6',
            'J-00363348-8',
            'G-20003010-0',
            'G-20010365-5',
            'G-20010657-3',
            'G-20006599-0',
            'G-20009928-3',
            'G-20004706-2',
            'G-20011588-2'
        ];

        $faker = Faker::create();
        $consulta = new Consulta();
        foreach ($rifs_Clientes as $rif) {
            $rif = trim($rif);
            if (Cliente::where('rif', '=', $rif)->exists()) {
                continue;
            }

            $cliente = [];
            $cliente['seniat'] = $consulta->getSeniat(str_replace('-', '', $rif));
            $cliente['rnc'] = $consulta->getDataRNC(str_replace('-', '', $rif));

            $cliente['razon_social'] = (!empty($cliente['seniat']['nombre'])) ? $cliente['seniat']['nombre'] : $cliente['rnc']['razon_social'];
            unset($cliente['seniat'], $cliente['rnc']);

            $cliente['rif'] = $rif;
            $cliente['usuario_id'] = 1;
            $cliente['nombre_comercial'] = $cliente['razon_social'];
            $cliente['abreviatura'] = $this->getInitials($cliente['razon_social'], 'U');

            $cliente['direccion_fiscal'] = $faker->address();
            //asi debe ser el formato de telefonos
            $cliente['telefonos'] = json_encode([array('a'=>$faker->phoneNumber())]);
            //asi debe ser el formato de correo
            $cliente['correos_electronicos'] = json_encode([array('a'=>$faker->email())]);
            $cliente['sitio_web'] = $faker->domainName();
            $cliente['img_cliente'] = $faker->imageUrl(640, 480, 'cats');

            $obj = Cliente::create($cliente);

            for($i = 0, $len=3; $i<$len; $i++ ){
                $tipo = 'administrativo';
                if($i == 1){
                    $tipo = 'publicitario';
                } elseif($i > 1){
                    $tipo = "otro tipo";
                }

                $contacto = [
                    'nombre' => $faker->name,
                    'apellido' => $faker->lastName,
                    'usuario_id' => 1,
                    'es_interno' => true,
                    'cargo_id' => $faker->numberBetween(1,67),
                    //asi debe ser el formato de telefonos
                    'telefonos' => json_encode([array('a'=>$faker->phoneNumber())]),
                    //asi debe ser el formato de correo
                    'correos_electronicos' => json_encode([array('a'=>$faker->email())]),
                    'tipo' => $tipo,
                ];


                $obj->contactos()->create($contacto);
            }

            $this->command->info("Creado Cliente #{$obj->id} con 3 Contactos");
        }
    }

    private function getInitials($name, $case = 'L')
    {
        if (empty($name)) {
            return $name;
        }

        $initials = $name;
        $splitted_name = explode(' ', trim($name));
        if (!empty($splitted_name)) {
            $initials = '';
            foreach ($splitted_name as $row)
                $initials .= substr(trim($row), 0, 1);
        }
        return ($case == 'U') ? strtoupper($initials) : strtolower($initials);
    }

}