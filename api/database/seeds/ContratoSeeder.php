<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

// Hace uso del modelo de Fabricante.
use App\Models\Contrato as Contrato;
use App\Models\Cliente;
use Faker\Factory as Faker;

class ContratoSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Creamos una instancia de Faker

        //DB::table('clientes')->delete();
        $faker = Faker::create();
        $clientes = Cliente::all()->toArray();
        foreach ($clientes as $key => $cliente) :
            for ($i=0; $i<1; $i++)
            {
                Contrato::create(
                    [
                                'techo_consumo' => $faker->numberBetween(10000, 50000),
                                'regimen_pago' => $faker->randomElement(array("'quincenal'", "'semestral'", "'trimestral'", "'anual'",)),
                                'tarifa' => $faker->numberBetween(50000, 200000),
                                //'es_postpago' => $faker->randomElement(array("B'0'::".'"bit"', "B'1'::".'"bit"',)),
                                'usuario_id' => 1,
                                'cliente_id' => $cliente['id'],
                                'activo'=>'true',
                            ]
                );
            }
        endforeach;
    }

}