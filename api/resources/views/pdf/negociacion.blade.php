<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style type="text/css">
        .app-center{ text-align: center; }
        .app-div-h{
            height: 4px;
        }
        body {
            font-size: 10pt;
        @unless( empty($no_valido) )
            background-image: url('{{asset('img/no-valido.png')}}');
            background-repeat: no-repeat;
        @endunless
        }
    </style>
</head>
<body>
<br/>
<div class="row app-div-h">&nbsp;</div>
Esta negociaci&oacute;n se encuentra en estatus <b>{{($negociacion['estado']) ? 'Activa' : 'Desactivada' }}</b>.
@unless(empty($negociacion['tarifa']))
    <br/>
    Monto Negociado: <b>{{ number_format($negociacion['tarifa'], 2, ',', '.') }}</b> Bs.

    {{--@unless(empty($negociacion['techo_consumo']))--}}
        {{--<br/>--}}
        {{--Techo Maximo de Consumo: <b>{{ number_format($negociacion['techo_consumo'], 2, ',', '.') }}</b> Bs.--}}
    {{--@endunless--}}
@endunless
<br/>
<br/>
<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th class="app-center">Medio</th>
        <th class="app-center">Tarifa</th>
        <th class="app-center">Detalle</th>
        <th class="app-center">Aplicado</th>
    </tr>
    </thead>
    <tbody>
        @unless(empty($negociacion['servicios']))
            @foreach($negociacion['servicios'] as $row)
                <tr>
                    <td>{{ $row['servicio']['nombre'] }}</td>
                    <td> - </td>
                    <td> - </td>
                    <td>
                        @if($row['exonerado'])
                            Exonerado
                        @else
                            {{ number_format ( $row['monto'] , 2 , ',' , '.') }}
                            {{ ($row['es_porcentaje']) ? '%' : 'Bs'}}
                        @endif
                    </td>
                </tr>
            @endforeach
        @endunless

        @unless(empty($negociacion['condiciones']))
            @foreach($negociacion['condiciones'] as $row)
                <tr>
                    <td>{{ $row['servicio']['nombre'] }}</td>
                    <td>{{ $row['condicion']['nombre'] }}</td>
                    <td> - </td>
                    <td>
                        @if($row['exonerado'])
                            Exonerado
                        @else
                            {{ number_format ( $row['monto'] , 2 , ',' , '.') }}
                            {{ ($row['es_porcentaje']) ? '%' : 'Bs'}}
                        @endif
                    </td>
                </tr>
            @endforeach
        @endunless

        @unless(empty($negociacion['condicionDetalles']))
            @foreach($negociacion['condicionDetalles'] as $row)
                <tr>
                    <td>{{ $row['servicio']['nombre'] }}</td>
                    <td>{{ $row['condicion']['nombre'] }}</td>
                    <td>{{ $row['detalle']['nombre'] }}</td>
                    <td>
                        @if($row['exonerado'])
                            Exonerado
                        @else
                            {{ number_format ( $row['monto'] , 2 , ',' , '.') }}
                            {{ ($row['es_porcentaje']) ? '%' : 'Bs'}}
                            {{ ($row['sustituye']) ? 'Sobreescrito' : ''}}
                        @endif
                    </td>
                </tr>
            @endforeach
        @endunless
    </tbody>
</table>

<fieldset>
    <legend>Observaciones</legend>
    {{$negociacion->observaciones}}
</fieldset>

</body>
</html>