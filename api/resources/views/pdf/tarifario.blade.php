<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style type="text/css">
        .app-center{ text-align: center; }
        .app-div-h{
            height: 4px;
        }
        body {
            font-size: 10pt;
            margin-left: 25px;
        @unless( empty($no_valido) )
            background-image: url('{{asset('img/no-valido.png')}}');
            background-repeat: no-repeat;
        @endunless
        }
    </style>
</head>
<body>
<div class="row app-div-h">&nbsp;</div>
<br/>

<div class="row">
Tarifario del servicio: <b>{{$servicio['nombre']}}</b>
</div>
<ul>
    @foreach($servicio['condiciones'] as $condicion)
    <li>
        <b>{{$condicion['nombre']}}</b> ({{($condicion['es_base'] == 0) ? 'Recargo' : 'Base'}})
        <ul>
            @foreach($condicion['detalles'] as $detalle)
            <li>
                {{$detalle['nombre']}} - {{$detalle['tarifa']}} {{($condicion['es_porcentaje'] == 1) ? '%' : 'Bs'}}
            </li>
            @endforeach
        </ul>
    </li>
    @endforeach
</ul>


</body>
</html>