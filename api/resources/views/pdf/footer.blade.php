<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        .firma{
            width: 100%;
            font-weight: bold;
            text-align: center;
            font-size: 10pt ;
        }

        .page {
            text-align: center;
            width: 100%;
            font-size: 6pt;
        }
    </style>
</head>
<body>

<div class="firma"><span>APROBADO POR</span></div>
{{-- Espacios para firma --}}
<div >&nbsp;</div>
<div >&nbsp;</div>
<div class="firma"><span>{{$firmante}}</span></div>
<div class="firma"><span>{{$cargo or ''}}</span></div>
<div class="page"><span>{{$iniciales or ''}}</span> - p&aacute;gina: {{$page or ''}} / {{$topage or ''}}</div>
</body>
</html>