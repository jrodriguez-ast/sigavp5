<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style type="text/css">
        .app-center{ text-align: center; }
        .app-div-h{
            height: 4px;
        }
        body {
            font-size: 10pt;
        @unless( empty($no_valido) )
            background-image: url('{{asset('img/no-valido.png')}}');
            background-repeat: no-repeat;
        @endunless
        }
    </style>
</head>

<body>
<br>
<br>
<div>
        <div class="row">
            <div class="col-md-6">
                        <p><b>Tipo de contrato:</b> <span>{{$tipo_contrato}}</span></p>

            </div>
        </div>
         <div class="row">
                    <div class="col-md-6">
                                <p><b>Codigo:</b> <span>{{$codigo_contrato}}</span></p>

                    </div>
                </div>
        <div class="row">
            <div class="col-md-6">
                        <p><b>Monto del Contrato:</b> <span>{{number_format($tarifa,2,',','.')}}</span> Bs.</p>

            </div>
        </div>
    </div>
    <div>
            <div class="row">
                <div class="col-md-6">
                            <p><b>Detalle de cuotas:</b></p>

                </div>
            </div>
        </div>
<table class="table">
    <thead>
        <tr>
            <th class="app-center">Descripci&oacute;n</th>
            <th class="app-center">Fecha Pago</th>
            <th class="app-center">Precio</th>
            <th class="app-center">Total</th>
        </tr>
    </thead>
    <tbody>
      @unless(empty($contrato))
       @foreach($contrato['cuotas'] as $key => $row)
         <?php
        $fecha_cuota = new DateTime($contrato['cuotas'][$key]['fecha']);
        $texto= 'Cuota #'.$contrato['cuotas'][$key]['numero_cuota'];
        $monto = $contrato['cuotas'][$key]['monto'];
        $monto_iva = $contrato['cuotas'][$key]['monto_iva'];
        ?>

            <tr>
            <td style="vertical-align:middle;text-align: center" >
            {{
            $texto
            }}</td>
            <td style="vertical-align:middle;text-align: center" >
            {{
            $fecha_cuota->format('d-m-Y')
            }}</td>
            <td style="vertical-align:middle;text-align: center" >{{ number_format($monto,2,',','.') }}</td>
            <td style="vertical-align:middle;text-align: center" >{{ number_format($monto_iva,2,',','.') }}</td>
            </tr>

  @endforeach
    <tr>
    </tbody>
</table>
<br>
<br>
<br>
<br>
 <div>
            <div class="row">
                <div class="col-md-6">
                            <p><b>Detalle de Pautas:</b></p>

                </div>
            </div>
        </div>
<table class="table">
    <thead>
        <tr>
            <th class="app-center">Tipo de Publicaci&oacute;n</th>
            <th class="app-center">Precio</th>
            <th class="app-center">Total</th>
        </tr>
    </thead>
    <tbody>

       @foreach($contrato['pautas'] as $key => $row)
         <?php
        $tipo_publicacion= $contrato['pautas'][$key]['tipo']['nombre'];
        $monto = $contrato['pautas'][$key]['monto'];
        $monto_total = $contrato['pautas'][$key]['total'];
        ?>

            <tr>
            <td style="vertical-align:middle;text-align: center" >
            {{
            $tipo_publicacion
            }}</td>
            <td style="vertical-align:middle;text-align: center" >{{ number_format($monto,2,',','.') }}</td>
            <td style="vertical-align:middle;text-align: center" >{{ number_format($monto_total,2,',','.') }}</td>
            </tr>

        @endforeach
      @endunless

    <tr>
    </tbody>
</table>
<br>
<br>
<br>
  <div class="row">
                    <div class="col-md-6">
                                <p><b>Nota: <span>Archivo de caracter informativo, no tiene validez legal.</span></b></p>

                    </div>
                </div>

</body>
</html>