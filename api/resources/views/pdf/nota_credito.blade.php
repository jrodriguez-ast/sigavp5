<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style type="text/css">
        .app-center {
            text-align: center;
        }

        .app-div-h {
            height: 4px;
        }

        body {
            font-size: 6pt;
        @unless( empty($no_valido) ) background-image: url('{{asset('img/no-valido.png')}}');
            background-repeat: no-repeat;
        @endunless
        }
    </style>
</head>
<body font face="arial">
<div class="">

       <br>
        <table style="width:100%;" cellspacing="0" class="body" border="0">
               <thead>
               <tr>
                   <th colspan="6" style="color:#A5A5A5;width:104%;text-align: left;" class="center">
                       <hr align="left" noshade="noshade" size="3" width="100%"/>
                   </th>

               </tr>
               <tr>
                   <td colspan="3" style="text-align: left;">
                       <b>CODIGO NOTA:</b> {{$codigo_nota or ''}}
                       </td>
                       <td colspan="3" style="text-align: right;">
                       <b>FECHA DE EMISI&Oacute;N DE LA NOTA:</b> <?php
                       $fecha_nota = new DateTime($fecha);
                       echo $fecha_nota->format('d-m-Y'); ?>
                       </td>
               </tr>
                <tr>
                   <td colspan="3" style="text-align: left;">
                       <b>FACTURA NRO:</b> {{$numero_factura or ''}}
                       </td>
                       <td colspan="3" style="text-align: right;">
                       <b>FECHA DE EMISI&Oacute;N:</b> <?php
                       $fecha = new DateTime($fecha_creacion);
                       echo $fecha->format('d-m-Y'); ?>
                       </td>
               </tr>

               <tr>
                   <td colspan="3" style="width:50%; text-align: left;">
                       <b>CLIENTE:</b> {{$cliente or ''}}</td>
                        <td colspan="3" style="width: 50%;text-align: right;">
                        <b>COD CLIENTE: </b>{{$cod_cliente or ''}}
                        </td>

               </tr>
               <tr>
                   <td colspan="3" style="width:50%; text-align: left;">
                       <b>RIF CLIENTE:</b> {{$rif or ''}}
                   </td>
                                <td colspan="3" style="width:50%;text-align: right;">
                                <b>TEL&Eacute;FONO:</b>
                                <?php
                                foreach($telefonos as $key => $telefono):
                                echo  $telefono.',';
                                endforeach
                                ?>
                                </td>

               </tr>
               <tr>
                   <td colspan="3" style="width:50%;text-align: left;">
                       <b>DIRECCI&Oacute;N FISCAL:</b> {{$direccion or ''}}
                   </td>

                               <td colspan="3" style="width:50%;text-align: right;">
                                <b>CORREO:</b>
                                <?php
                                foreach($correo_electronico as $key => $correo):
                                echo  $correo.',';
                                endforeach
                                ?>
                                </td>
                 </tr>
               <tr>

                   <th colspan="6" style="color:#A5A5A5;width:104%;text-align: left;" class="center">
                       <hr align="left" noshade="noshade" size="3" width="100%"/>
                   </th>

               </tr>

               </thead>
           </table>
</div>
<br/>
<table class="table table-striped table-bordered">
    <thead>
        <tr>
            <th class="app-center">Descripci&oacute;n</th>
            <th class="app-center">Total</th>
        </tr>
    </thead>
    <tbody>
      @unless(empty($nota))

            <tr>
            <td style="vertical-align:middle;text-align: left" >
            {{
            "".$nota['descripcion'].""
            }}</td>

            <td style="vertical-align:middle;text-align: center" >{{ number_format($nota['monto'],2,',','.') }}</td>
            </tr>

      @endunless

    <tr>
    </tbody>
    {{--<td> {{'Gestion de Medios (Base imponible para retencion de I.S.L.R)'}}--}}
    {{--</td>--}}
    {{--<td style="vertical-align:middle;text-align: center">{{'1'}}--}}
    {{--</td>--}}
    {{--<td style="vertical-align:middle;text-align: center" >{{ number_format($factura['publicacion'][$key]['cotizacion']['total_impuesto'],2,',','.') }}--}}
    {{--</td>--}}
   {{--<td style="vertical-align:middle;text-align: center" >{{ number_format($factura['publicacion'][$key]['cotizacion']['total_impuesto'],2,',','.') }}--}}
       {{--</td>--}}
    {{--</tr>--}}


</table>
<div class="alert alert-info" role="alert">
    <div class="row">

        <p style="text-align: right;"><b>Total:</b> <span>{{ number_format($total ,2,',','.') }}</span> Bs.</p>
        <br>
        <p style="text-align: center; text-transform: uppercase;"><b>Son:</b><span>{{$monto_letras }}</span></p>

    </div>
</div>
<div>
<p style="text-align: left;"><b>Favor emitir cheque a nombre de: </b> <span>{{ $nombre_cheque }}</span></p>
<p style="text-align: left;"><b>Para Transferencias: </b> <span>{{ $numero_cuenta }}</span></p>
</div>
</body>
</html>