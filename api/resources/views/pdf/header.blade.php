<!DOCTYPE html>
<html>
<head>
    <style type="text/css">
        table{
            font-size: 7.5pt;
            border: none;
            width: 100%;
            text-align: left;
        }
        .column {
            font-weight: bold;
            width: 20%;
        }
        .informacion-avp {
            color: #808080;
            font-size: 6pt;
        }
        .interlinea{
            background-color: #f9f9f9;
        }
    </style>
</head>
<body>
<table cellspacing="0">
    <thead>
    <tr>
        <th colspan="2" class="column">
            <img src="{{ asset('/img/logoAVP.png') }}">
        </th>
        <?php if($titulo == 'Factura'): ?>

            <th colspan="4" align="center">
                        <h3>{{ $titulo.' '.$code }}</h3>
                    </th>
        <?php  elseif($titulo == 'Orden de Publicacion'): ?>
        <th colspan="4" align="center">
            <h3>{{ $titulo.' '.$code }}</h3>
        </th>
        <?php  else: ?>
                <th colspan="4" align="center">
                    <h3>{{ $titulo }}</h3>
                </th>
        <?php endif ?>
    </tr>
    <tr>
        <th colspan="6" class="informacion-avp">{{ $avp_rif }} / Dirección:  {{ $avp_direccion }} </th>
    </tr>
    <tr>
        <td colspan="6">
            <hr noshade="noshade" size="3" class="full-width"/>

        </td>
    </tr>
     <?php if($titulo == 'Orden de Publicacion'): ?>
<tr>
                      <th class="column">Cliente:</th>
                      <td colspan="5">
                          {{ $cliente." (".$rifcli.")" }}
                      </td>
                  </tr>
            <?php endif ?>
    <tr>
        <th class="column">Para:</th>
        <td colspan="5">
            {{ $to }}
        </td>
    </tr>
    @unless(empty($rif))
                  <tr>
                      <th class="column">RIF:</th>
                      <td colspan="5">
                          {{ $rif }}
                      </td>
                  </tr>
                   @endunless
    @unless(empty($rifmed))
                  <tr>
                  <th class="column">RIF:</th>
                  <td colspan="5">
                  {{ $rifmed }}
                  </td>
                  </tr>
                  @endunless
     @unless(empty($codigo_cli))
                      <tr>
                          <th class="column">Cod cliente:</th>
                          <td colspan="5">
                              {{ $codigo_cli }}
                          </td>
                      </tr>
                       @endunless
    @unless(empty($direccion))
                      <tr>
                          <th class="column">Direccion Fiscal:</th>
                          <td colspan="5">
                              {{ $direccion }}
                          </td>
                      </tr>
    @endunless
     <?php if($titulo != 'Factura'): ?>
    <tr class="interlinea">
        <th class="column">De:</th>
        <td colspan="5">Agencia Venezolana de Publicidad</td>
    </tr>
    <?php endif ?>
    <tr>
        <th class="column">Fecha de Emisi&oacute;n:</th>
        <td colspan="5">
            {{ $fecha }}
        </td>
    </tr>



     @unless(empty($correo_electronico))
         <tr>
             <th class="column">Correos Electronicos:</th>
             <td colspan="5">
                 <?php
                 foreach($correo_electronico as $key => $correos):
                 echo  $correos.',';
                 endforeach
                  ?>
             </td>
         </tr>
     @endunless
     @unless(empty($telefonos))

              <tr>
                  <th class="column">Telefonos:</th>
                  <td colspan="5">
                   <?php
                    foreach($telefonos as $key => $telefono):
                     echo  $telefono.',';
                       endforeach
                      ?>
                  </td>
              </tr>
     @endunless

 <?php if($titulo != 'Factura'): ?>
    @unless(empty($subject))
    <tr>
        <th class="column">Asunto:</th>
        <td colspan="5">{{ $subject }}</td>
    </tr>

    <?php endif ?>
    @endunless
      <tr>
            <td colspan="6">
                <hr noshade="noshade" size="3" class="full-width"/>
            </td>
        </tr>
    </thead>
</table>
@unless(empty($nota))

<div class="informacion-avp"><b>{{ $nota.', fecha de vencimiento ('.$vencimiento.')' }}</b></div>
@endunless
</body>
</html>