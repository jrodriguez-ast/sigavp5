<!DOCTYPE html>
<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style type="text/css">
        body {
            font-size: 7.5pt;
        @unless( empty($no_valido) ) background-image: url('{{asset('img/no-valido.png')}}');
            background-repeat: no-repeat;
        @endunless


        }
         @unless(empty($publicacion))
                @foreach($publicacion['version'] as $key => $version)

                    <?php
                        $nombre_version = $version['version_nombre'];
                        $nombre_servicio = $version['servicio']['nombre'];
                        $fecha_fin = $version['fecha_final'];
                        $fecha_inicio = $version['fecha_inicial'];
                        $dias_x_publicacion = $version['diasporpublicacion'];
                        $segundos = $version['version_segundos'];
                        $rotacion = $version['version_apariciones'];
                        $subtotal = $version['monto_con_avp'];
                        $tipo_publicacion_name = $version['tipo_publicacion_name'];
                    ?>
                        @foreach($version['versioncondicion'] as $condicion)
                        <?php

                            $detalle = $condicion['detallecondicion']['nombre'];
                            $condicion = $condicion['condicion']['nombre'];


                         ?>

                        @endforeach
                @endforeach
    </style>
</head>
<body>
</br>
</br>
<h3>Versiones</h3>

<table class="table table-striped table-bordered">
    <thead>
    <tr>
        <th>Medio</th>
        <th>Nombre</th>
        @if($dias_x_publicacion == 1)
        <th>Publicado el</th>
        @else
        <th>Fecha Inicio</th>
        <th>Fecha Fin</th>
        @endif
        <th>Descripcion</th>
        <th>Subtotal</th>
    </tr>
    </thead>
    <tbody>



@if($dias_x_publicacion == 1)
            <tr>
                <td>{{$nombre_servicio}}</td>
                 <td>{{$nombre_version}}</td>
                <td> {{$fecha_inicio->format('d-m-Y')}} </td>
                <td>

                    <?php
                    $texto_seg = '('.$segundos.'/'.$rotacion.')';
                    $spost = '('.$dias_x_publicacion * $rotacion.')'

                    ?>

                    @if ($tipo_publicacion_name == 'Radial' or $tipo_publicacion_name == 'Televisivo')
                        <ul>
                        <li>{{'Seg/Rotacion'.$texto_seg}}</li>
                        </ul>
                        <ul>
                        <li>{{'Spot'.$spost}}</li>
                        </ul>

                    @else
                    <ul>
                    <li>{{$detalle}}</li>
                    </ul>
                    @endif

                </td>
                <td>
                    <?php echo number_format($subtotal, 2, ',', '.'); ?> Bs.
                </td>

            </tr>
           @else
           <tr>
                           <td>{{$nombre_servicio}}</td>
                            <td>{{$nombre_version}}</td>
                           <td> {{$fecha_inicio->format('d-m-Y')}} </td>
                           <td> {{$fecha_fin->format('d-m-Y')}} </td>
                           <td>

                               <?php
                               $texto_seg = '('.$segundos.'/'.$rotacion.')';
                               $spost = '('.$dias_x_publicacion * $rotacion.')'

                               ?>

                               @if ($tipo_publicacion_name == 'Radial' or $tipo_publicacion_name == 'Televisivo')
                                   <ul>
                                   <li>{{'Seg/Rotacion'.$texto_seg}}</li>
                                   </ul>
                                   <ul>
                                   <li>{{'Spot'.$spost}}</li>
                                   </ul>

                               @else
                               <ul>
                               <li>{{$detalle}}</li>
                               </ul>
                               @endif

                           </td>
                           <td>
                               <?php echo number_format($subtotal, 2, ',', '.'); ?> Bs.
                           </td>

                       </tr>
           @endif

    </tbody>
</table>



  @if ($tipo_publicacion_name == 'Radial' or $tipo_publicacion_name == 'Televisivo')
<div class="alert alert-info">

<div class="row">
   <u>Observaciones:</u>
</div>
<div class="row">
<div>
 <p style="text-align: left;"><b>Spot a transmitir:</b> <span>{{$dias_x_publicacion * $rotacion}}.</span></p>

</div>
</div>
<div class="row">
<div>
 <p style="text-align: left;"><b>D&iacuteas de transmisi&oacuten: </b> <span>{{$dias_x_publicacion}}.</span></p>

</div>
</div>
<div class="row">
<div>
 <p style="text-align: left;"><b>Rotaci&oacuten:</b> <span>{{$rotacion}} diarias.</span></p>

</div>
</div>
<div class="row">
<div>
 <p style="text-align: left;"><b>Total inversi&oacuten(con iva):</b> <span>{{ number_format($publicacion['total'],2,',','.') }}</span></p>

</div>
</div>

</div>
 @endif
  @if($tipo_publicacion_name == 'Impreso')


<div class="alert alert-info">

<div class="row">
   <u>Observaciones:</u>
</div>
<div class="row">
<div>
  <p style="text-align: left;"><b>Total inversi&oacuten(con iva):</b> <span>{{ number_format($publicacion['total'],2,',','.') }}</span></p>

</div>
</div>

</div>
   @endif
    @endunless
</body>
</html>