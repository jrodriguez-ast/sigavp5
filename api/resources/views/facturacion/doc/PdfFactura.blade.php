<?php
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<html>
<title></title>
<head>
    <link rel="stylesheet" type="text/css" href="{{ asset('/css/cotizacion_pdf.css') }}"/>

    <style type="text/css">
        @page {
            margin: 180px 50px;
        }

        body {
            font-size: 7.5pt;
        @if ( !empty($no_valido) ) background-image: url('../img/no-valido.png');
            background-size: 20px;
            background-repeat: no-repeat;
        @endif
        }

    </style>
</head>
<body>

<div id="header">
    <table style="width:100%;" cellspacing="0" class="body" border="0">
        <thead>
        <tr>
            <th colspan="3" style="width: 50%">
                <img src="{{ asset('/img/logoAVP.png') }}">
            </th>
            <th colspan="3" class="center" style="width: 50%">
                <h3>{{$config['pdf_cotizacion_title']['value'] or 'Cotizacion AVP '}}</h3>
            </th>
        </tr>

        <tr>
            <th colspan="6" class="informacion-avp">{{$config['avp_rif']['value'] or ''}} /
                Dirección:  {{$config['avp_direccion']['value'] or ''}} </th>
        </tr>
        <tr>

            <th colspan="6" style="color:#A5A5A5;width:104%;text-align: left;" class="center">
                <hr align="left" noshade="noshade" size="3" width="100%"/>
                <hr align="left" noshade="noshade" size="3" width="100%"/>
            </th>

        </tr>
        <tr>
            <td colspan="6" style="text-align: left;">
                <b>COTIZACION NRO:</b> {{$codigo or ''}}
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left;">
                <b>FECHA DE EMISION:</b> <?php $fecha = new DateTime($fecha_creacion);
                echo $fecha->format('d-m-Y'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left;">
                <b>CLIENTE:</b> {{$cliente['razon_social'] or ''}}</td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left;">
                <b>RIF CLIENTE:</b> {{$cliente['rif'] or ''}}
            </td>
        </tr>
        <tr>
            <td colspan="2" style="width:50%;text-align: left;">
                <b>DIRECCION FISCAL:</b> {{$cliente['direccion_fiscal'] or ''}}
            </td>
            <td colspan="2" style="width:50%;text-align: left;">
            </td>
            <td colspan="2" style="width:50%;text-align: left;">
                <b>CONTACTO ADMINISTRATIVO</b>
                <br/>
                <?php

                foreach ($cliente['contactos'] as $key => $value):
                    if ($value['tipo'] == 'administrativo'):
                        echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $value['nombre'] . " " . $value['apellido'] . (!empty($value['cargo']['descripcion']) ? "<b>(" . $value['cargo']['descripcion'] . ")</b>" : '');
                        echo "<br/>";
                        foreach (json_decode($value['correos_electronicos'], true) as $clave => $valor):
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EMAIL:</b> " . $valor['a'];
                            echo "<br/>";
                        endforeach;
                    endif;
                endforeach;

                ?>
            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left;">
                <b>TELEFONO:</b>
                <?php
                $telefonos = '';

                if (!empty($cliente['telefonos'])):
                    foreach (json_decode($cliente['telefonos'], true) as $key => $value):

                        $telefonos .= " / " . $value;
                    endforeach;
                    $telefonos = trim(trim($telefonos, '/'));
                endif;
                echo $telefonos;
                ?>

            </td>
        </tr>
        <tr>
            <td colspan="6" style="text-align: left;">
                <b>COD CLIENTE: </b>{{$cliente['codigo'] or ''}}
            </td>
        </tr>
        <tr>

            <th colspan="6" style="color:#A5A5A5;width:104%;text-align: left;" class="center">
                <hr align="left" noshade="noshade" size="3" width="100%"/>
                <hr align="left" noshade="noshade" size="3" width="100%"/>
            </th>

        </tr>

        </thead>
    </table>
</div>
<br/>

<div id="content">
    <table style="width:100%;" cellspacing="0" class="body" border="0">
        <thead>
        <tr class="center">
            <th style="width:20%" class="borde_top borde_bottom borde_left head">
                NOMBRE
            </th>
            <th style="width:10%;text-align: center;" class="borde_top borde_bottom borde_left head">
                VERSION
            </th>
            <th style="width:10%" class="borde_top borde_bottom borde_left head">
                INICIO DE PUBLICACION
            </th>
            <th style="width:10%" class="borde_top borde_bottom borde_left head">
                FIN DE PUBLICACION
            </th>
            <th style="width:15%" class="borde_top borde_bottom borde_left head">
                SEG / APARICION
            </th>
            <th style="width:20%" class="borde_top borde_bottom borde_left head">
                DESCRIPCION
            </th>

            <th style="width:15%" class="borde_top borde_bottom borde_right head">
                TOTAL
            </th>

        </tr>
        </thead>
        <?php
        $cant_items = 0;
        $total = 0;
        $count = 1;
        $cant_items = sizeof($versiones);
        $subtotal = 0.00;
        $total_a_pagar = 0.00;
        foreach($versiones as $key=>$value):
        $total += (float)$value['subtotal'];
        ?>
        <tbody>
        <tr>
            <td style="width:20%;"
                class="center borde_left borde_bottom  ">sdsds
            </td>
            <td style="width:10%;"
                class="center borde_left borde_bottom "><?php echo $value['version_nombre']; ?>
            </td>
            <td style="width:10%;"
                class="center borde_left borde_bottom "><?php
                $fecha_inicio = new DateTime($value['version_publicacion_fecha_inicio']);
                echo $fecha_inicio->format('d-m-Y'); ?>
            </td>
            <td style="width:10%;"
                class="center borde_left borde_bottom "><?php
                $fecha_fin = new DateTime($value['version_publicacion_fecha_fin']);
                echo $fecha_fin->format('d-m-Y');
                ?>
            </td>
            <td style="width:15%;"
                class="center borde_left borde_bottom ">
                <table style="width: 100%;" border="0">
                    <tr>
                        <td class="center borde_bottom borde_left borde_right borde_top"><?php echo(!empty($value['version_segundos']) ? $value['version_segundos'] : 'No aplica'); ?></td>
                        <td class="center borde_bottom borde_left borde_right borde_top"><?php echo(!empty($value['version_apariciones']) ? $value['version_apariciones'] : 1); ?></td>
                    </tr>
                </table>

            </td>
            <td style="text-align:left;width:20%;"
                class="left borde_left borde_bottom ">

                <?php
                if (is_array($value['condiciones']) && !empty($value['condiciones'])):
                    echo '<ul>';
                    foreach ($value['condiciones'] as $key => $condicion):
                        echo "<li>$condicion</li>";
                    endforeach;
                    echo '</ul>';
                endif;
                ?>

            </td>
            <td style="width:15%;"
                class="center borde_left borde_right borde_bottom"><?php  echo number_format($value['subtotal'], 2, ',', '.') ?>
                Bs
            </td>


        </tr>
        <?php
        $count = $count + 1;
        endforeach;

        $cargo_avp = $total * $porcentaje_avp / 100; //($total * $porcentaje_avp / 100);

        $subtotal = $total + $cargo_avp;
        $iva = ($subtotal * $porcentaje_impuesto / 100);
        $total_a_pagar = ($subtotal + $iva);

        $espacio_fijo = 0;//15 - $cant_items;
        $count_space = 1;
        if ($espacio_fijo > 0):
        for ($j = 1; $j <= $espacio_fijo; $j++) :
        ?>

        <tr>
            <td colspan="2" class="borde_left ">&nbsp;</td>
            <td class="borde_left ">&nbsp;</td>

            <td class="borde_left ">&nbsp;</td>
            <td class="borde_left ">&nbsp;</td>
            <td class="borde_left borde_right">&nbsp;</td>


        </tr>
        <?php
        $count_space = $count_space + 1;
        endfor;
        endif;
        ?>


        <tr>
            <td colspan="4">
                &nbsp;
            </td>
            <td colspan="2" class="left montos_color borde_left borde_right borde_bottom">
                GESTION DE MEDIOS AVP &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;  <?php echo $porcentaje_avp; ?>
                %
            </td>
            <td class="center montos_color borde_left borde_right borde_bottom"><?php echo number_format($cargo_avp, 2, ',', '.'); ?>
                Bs
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
            <td class="left montos_color borde_left borde_right borde_bottom" colspan="2">
                SUB-TOTAL
            </td>
            <td class="center montos_color borde_left borde_right borde_bottom"><?php echo number_format($subtotal, 2, ',', '.'); ?>
                Bs
            </td>


        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
            <td class="left montos_color borde_left borde_right borde_bottom">
                IVA
            </td>
            <td class="left montos_color borde_left borde_right borde_bottom">
                <?php echo $porcentaje_impuesto; ?> %
            </td>
            <td class="center montos_color borde_left borde_right borde_bottom"><?php echo number_format($iva, 2, ',', '.'); ?>
                Bs
            </td>
        </tr>


        <tr>
            <td colspan="4">
                &nbsp;
            </td>
            <td class="left montos_color borde_left borde_right borde_bottom" colspan=2">
                TOTAL A PAGAR
            </td>
            <td class="center montos_color borde_left borde_right borde_bottom"
                    ><?php echo number_format($total_a_pagar, 2, ',', '.'); ?>
                Bs
            </td>
        </tr>

        </tbody>
    </table>
</div>
<div id="footer">
    <div class="firma"><span>APROBADO POR</span></div>
    {{-- Espacios para firma --}}
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div class="firma"><span>{{$config['gerente_general_avp']['value'] or ''}}</span></div>
    <div class="firma"><span>{{$config['gerente_general_avp']['description'] or ''}}</span></div>
    <div style="text-align: center;color: #000000;font-size: 5.5pt;" class="firma"><span><i>PUBLICACION Y/O TRANSMISION SUJETA A DISPONIBILIDAD VALIDA HASTA

                <?php
                $fecha_expiracion = new DateTime($fecha_expiracion);
                echo $fecha_expiracion->format('d-m-Y'); ?></i></span></div>
    <div class="page"><span>{{$iniciales or ''}}</span> - página:</div>
</div>
</body>
</html>