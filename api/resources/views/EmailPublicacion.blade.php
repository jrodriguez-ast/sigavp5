<html>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <style type="text/css">
        body {
            font-size: 10pt;

    </style>
</head>
<body>
<h1>{{ $title or ''}}</h1>

<p>Bienvenido al Sistema de Gestion Publicitaria AVP<br>
    <img src="{{ asset('/img/logoAVP.png') }}">
    <br></p>
{{$html or ''}}
Buen dia <b>{{$data['medio']['razon_social'] or ''}}</b>
se ha generado la Orden de Publicacion de manera exitosa bajo el Nro.<b>{{ $data['codigo'] or ''}}.</b><br/>

<div class="panel">
    <div class="panel-body">
        <table border="1" class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>Version</th>
                <th>Archivo</th>
            </tr>
            </thead>
            <tbody>
            @unless(empty($data['versiones']))
                @foreach($data['versiones'] as $key => $row)
                    <tr>
                        <td>{{$row['version_nombre']}}</td>
                        <td><a href="{{ url('storage/tmp/'.$row['version_file']) }}">Descargar</a></td>
                    </tr>
                @endforeach
            @endunless
            </tbody>
        </table>
    </div>
</div>

<small>Puede descargar el documento adjunto para mas detalles.</small>
<br>

<small> Si le parece que el mensaje no es correcto, por favor ignore este correo.</small>
</body>
</html>

