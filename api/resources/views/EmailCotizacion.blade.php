<html>
<body>
<h1>{{ $title or ''}}</h1>

<p>Bienvenido al Sistema de Gestion Publicitaria AVP<br>
    <img src="{{ asset('/img/logoAVP.png') }}">
    <br></p>
{{$html or ''}}
Estimado cliente <b>{{$data['cliente']['razon_social'] or ''}}</b>
se ha generado la cotizacion de manera exitosa bajo el Nro.<b>{{ $data['codigo'] or ''}}.</b><br/>


<small>Puede descargar el documento adjunto para mas detalles.</small>
<br>

<small> Si le parece que el mensaje no es correcto, por favor ignore este correo.</small>
</body>
</html>

