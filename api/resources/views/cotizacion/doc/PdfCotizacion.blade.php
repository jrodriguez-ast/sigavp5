<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
<html>
<title></title>
<head>
    <link rel="stylesheet" type="text/css" href="{{ $novalido }}"/>

    <style type="text/css">
        @page {
            margin: 180px 50px;
        }

        {{--body {--}}
            {{--font-size: 7.5pt;--}}
        {{--@if ( !empty($no_valido) ) background-image: url('../img/no-valido.png');--}}
            {{--background-size: 20px;--}}
            {{--background-repeat: no-repeat;--}}
        {{--@endif--}}

        {{--}--}}
    </style>
</head>
<body>
<div class="header">
    <table style="width:100%;" cellspacing="0" class="body" border="0">
        <thead>
        <tr>
            <th colspan="3" style="width: 50%">
                <img src="{{ asset('/img/logoAVP.png') }}">
            </th>
            <th colspan="3" class="center" style="width: 50%">
                <h3>{{'Cotización AVP '.$codigo}}</h3>
            </th>
        </tr>

        <tr>
            <th colspan="6" class="informacion-avp">{{$config['avp_rif']['value'] or ''}} /
                Dirección: {{$config['avp_direccion']['value'] or ''}} </th>
        </tr>
        <tr>

            <th colspan="6" style="color:#A5A5A5;width:104%;text-align: left;" class="center">
                <hr align="left" noshade="noshade" size="3" width="100%"/>
                <hr align="left" noshade="noshade" size="3" width="100%"/>
            </th>

        </tr>
        {{--<tr>--}}
            {{--<td colspan="6" style="text-align: left;">--}}
                {{--<b>COTIZACIÓN NRO:</b> {{$codigo or ''}}--}}
            {{--</td>--}}
        {{--</tr>--}}

        <tr>
            <td colspan="3" style="width:50%; text-align: left;">
                <b>CLIENTE:</b> {{$cliente['razon_social'] or ''}}</td>

                <td colspan="3" style="text-align: right;">
                                <b>FECHA DE EMISIÓN:</b> <?php
                                $fecha = new DateTime($fecha_creacion);
                                echo $fecha->format('d-m-Y'); ?>
                            </td>
        </tr>
        <tr>
            <td colspan="3" style="width:50%; text-align: left;">
                <b>RIF CLIENTE:</b> {{$cliente['rif'] or ''}}
            </td>

                        <td colspan="3" style="width: 50%;text-align: right;">
                            <b>COD CLIENTE: </b>{{$cliente['codigo'] or ''}}
                        </td>

        </tr>
        <tr>
            <td colspan="3" style="width:50%;text-align: left;">
                <b>DIRECCIÓN FISCAL:</b> {{$cliente['direccion_fiscal'] or ''}}
            </td>
            <td colspan="3" style="width:50%;text-align: right;">
              <b>TELÉFONO:</b>
                            <?php
                            $telefonos = '';

                            if (!empty($cliente['telefonos'])):
                                foreach (json_decode($cliente['telefonos'], true) as $key => $value):

                                    $telefonos .= " / " . $value;
                                endforeach;
                                $telefonos = trim(trim($telefonos, '/'));
                            endif;
                            echo $telefonos;
                            ?>

                        </td>
          </tr>
          <tr>
            <td colspan="3" style="width:50%;text-align: left;">
                <b>CONTACTO ADMINISTRATIVO</b>
                <br/>
                <?php
                $contactos_permitidos = 3;
                $contador_contactos = 1;
                if ($cliente['contactos']):
                    foreach ($cliente['contactos'] as $key => $value):
                        if ($value['tipo'] == 'administrativo' && $contador_contactos <= $contactos_permitidos):
                            echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $value['nombre'] . " " . $value['apellido'];
                            echo "<br/>";
                            if (!empty($value['correos_electronicos'])):
                                foreach (json_decode($value['correos_electronicos'], true) as $clave => $valor):
                                    echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EMAIL:</b> " . (!empty($valor['a']) ? $valor['a'] : $valor);
                                    echo "<br/>";

                                endforeach;
                            endif;
                            $contador_contactos++;
                        endif;
                    endforeach;
                endif;

                ?>
            </td>

                    <td colspan="3" style="width:50%;text-align: right;">
                        <b>CONTACTO PUBLICITARIO</b>
                        <br/>
                        <?php
                        $contactos_permitidos = 3;
                        $contador_contactos = 1;
                        if ($cliente['contactos']):
                            foreach ($cliente['contactos'] as $key => $value):
                                if ($value['tipo'] == 'publicitario' && $contador_contactos <= $contactos_permitidos):
                                    echo "&nbsp;&nbsp;&nbsp;&nbsp;" . $value['nombre'] . " " . $value['apellido'];
                                    echo "<br/>";
                                    if (!empty($value['correos_electronicos'])):
                                        foreach (json_decode($value['correos_electronicos'], true) as $clave => $valor):
                                            echo "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>EMAIL:</b> " . (!empty($valor['a']) ? $valor['a'] : $valor);
                                            echo "<br/>";

                                        endforeach;
                                    endif;
                                    $contador_contactos++;
                                endif;
                            endforeach;
                        endif;

                        ?>
                    </td>
                </tr>


        <tr>

            <th colspan="6" style="color:#A5A5A5;width:104%;text-align: left;" class="center">
                <hr align="left" noshade="noshade" size="3" width="100%"/>
                <hr align="left" noshade="noshade" size="3" width="100%"/>
            </th>

        </tr>

        </thead>
    </table>
</div>
<br/>

<div id="content">
    <table style="width:100%;" cellspacing="0" class="body" border="0">
        <thead>
        <tr class="center">
            <th style="width:20%" class="borde_top borde_bottom borde_left head">
                MEDIO
            </th>
            <th style="width:15%" class="borde_top borde_bottom borde_left head">
                TIPO PUBLICACIÓN
            </th>
            <th style="width:10%;text-align: center;" class="borde_top borde_bottom borde_left head">
                VERSIÓN
            </th>
            <th style="width:10%" class="borde_top borde_bottom borde_left head">
                DESDE
            </th>
            <th style="width:10%" class="borde_top borde_bottom borde_left head">
                HASTA
            </th>
            <th style="width:20%" class="borde_top borde_bottom borde_left head">
                DESCRIPCIÓN
            </th>
            <th colspan="2" style="width:15%" class="borde_top borde_bottom borde_right head">
                TOTAL
            </th>

        </tr>
        </thead>
        <?php
        use Carbon\Carbon;
        $cant_items = 0;
        $total = 0;
        $count = 1;
        $cant_items = sizeof($versiones);
        $subtotal = 0.00;
        $total_a_pagar = 0.00;
        foreach($versiones as $key=>$value):
        $total += (float)$value['subtotal'];

        $fecha_inicio = $value['version_publicacion_fecha_inicio'];
        $fecha_final = $value['version_publicacion_fecha_fin'];
        $diasporpublicacion =  Carbon::createFromTimestamp(strtotime( ($fecha_final)))->diffInDays(Carbon::createFromTimestamp(strtotime( $fecha_inicio)));
        $diasporpublicacion++;
        $versiones[$key]['diasporpublicacion'] = $diasporpublicacion;

        ?>
        <tbody>
        <tr>
            <td style="width:20%;"
                class="center borde_left borde_bottom  "><?php echo $value['servicio']; ?>
            </td>
            <td style="width:15%;"
                class="center borde_left borde_bottom  "><?php echo $value['tipo_publicacion']; ?>
            </td>
            <td style="width:10%;"
                class="center borde_left borde_bottom "><?php echo $value['version_nombre']; ?>
            </td>
            <td style="width:10%;"
                class="center borde_left borde_bottom "><?php
                $fecha_inicio = new DateTime($value['version_publicacion_fecha_inicio']);
                echo $fecha_inicio->format('d-m-Y'); ?>
            </td>
            <td style="width:10%;"
                class="center borde_left borde_bottom "><?php
                $fecha_fin = new DateTime($value['version_publicacion_fecha_fin']);
                echo $fecha_fin->format('d-m-Y');
                ?>
            </td>
            <td style="text-align:left;width:20%;"
                class="left borde_left borde_bottom ">

                <?php
                if (is_array($value['condiciones']) && !empty($value['condiciones'])):
                    echo '<ul>';
                    foreach ($value['condiciones'] as $key => $condicion):
                        echo "<li>$condicion</li>";
                    endforeach;
                endif;
                if ($value['tipo_publicacion'] == 'Radial' || $value['tipo_publicacion'] == 'Televisivo'):

                    echo "<li>Seg/Rotacion(" . $value['version_segundos'] . "/" . $value['version_apariciones'] . ")</li>";
                    echo "<li>Spot(" . $value['diasporpublicacion'] * $value['version_apariciones'] . ")</li>";

                endif;
                 echo '</ul>';
                ?>

            </td>
            <td colspan="2" style="width:15%;"
                class="center borde_left borde_right borde_bottom"><?php  echo number_format($value['subtotal'], 2, ',', '.') ?>
                Bs
            </td>


        </tr>
        <?php
        $count = $count + 1;
        endforeach;

        $cargo_avp = $total * $porcentaje_avp / 100; //($total * $porcentaje_avp / 100);

        $subtotal = $total + $cargo_avp;
        $iva = ($subtotal * $porcentaje_impuesto / 100);
        $total_a_pagar = ($subtotal + $iva);

        $espacio_fijo = 0;//15 - $cant_items;
        $count_space = 1;
        if ($espacio_fijo > 0):
        for ($j = 1; $j <= $espacio_fijo; $j++) :
        ?>

        <tr>
            <td colspan="2" class="borde_left ">&nbsp;</td>
            <td class="borde_left ">&nbsp;</td>

            <td class="borde_left ">&nbsp;</td>
            <td class="borde_left ">&nbsp;</td>
            <td class="borde_left borde_right">&nbsp;</td>


        </tr>
        <?php
        $count_space = $count_space + 1;
        endfor;
        endif;
        ?>


        <tr>
            <td colspan="4">
                &nbsp;
            </td>
            <td colspan="2" class="left montos_color borde_left borde_right borde_bottom">
                GESTIÓN DE MEDIOS AVP &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; <?php echo $porcentaje_avp; ?>
                %
            </td>
            <td colspan="2"
                class="center montos_color borde_left borde_right borde_bottom"><?php echo number_format($cargo_avp, 2, ',', '.'); ?>
                Bs
            </td>
        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
            <td colspan="2" class="left montos_color borde_left borde_right borde_bottom">
                SUB-TOTAL
            </td>
            <td colspan="2"
                class="center montos_color borde_left borde_right borde_bottom"><?php echo number_format($subtotal, 2, ',', '.'); ?>
                Bs
            </td>


        </tr>
        <tr>
            <td colspan="4">
                &nbsp;
            </td>
            <td colspan="2" class="left montos_color borde_left borde_right borde_bottom">
                IVA &nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;
                &nbsp;&nbsp; &nbsp;&nbsp;&nbsp;<?php echo $porcentaje_impuesto; ?> %
            </td>

            <td colspan="2"
                class="center montos_color borde_left borde_right borde_bottom"><?php echo number_format($iva, 2, ',', '.'); ?>
                Bs
            </td>
        </tr>


        <tr>
            <td colspan="4">
                &nbsp;
            </td>
            <td colspan="2" class="left montos_color borde_left borde_right borde_bottom">
                TOTAL A PAGAR
            </td>
            <td colspan=2" class="center montos_color borde_left borde_right borde_bottom"
                    ><?php echo number_format($total_a_pagar, 2, ',', '.'); ?>
                Bs
            </td>
        </tr>

        </tbody>
    </table>
</div>
<div class="footer">
    <div class="firma"><span>APROBADO POR</span></div>
    {{-- Espacios para firma --}}
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div>&nbsp;</div>
    <div class="firma"><span>{{$firmante_nombre or ''}}</span></div>
    <div class="firma"><span>{{$firmante_cargo or ''}}</span></div>
    <div style="text-align: center;color: #000000;font-size: 5.5pt;" class="firma"><span><i>PUBLICACIÓN Y/O TRANSMISIÓN
                SUJETA A DISPONIBILIDAD VALIDA HASTA

                <?php echo $fecha_expiracion; ?></i></span></div>
    <div style="text-align: center;color: #000000;font-size: 5.5pt;" class="firma">
        <span>{{$iniciales_firmante or ''}} / {{$iniciales_usuario or ''}}</span>
    </div>
    <div class="pagina"> Página:<span class="page"></span></div>
</div>
</body>
</html>