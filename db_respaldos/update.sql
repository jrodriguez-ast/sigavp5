﻿INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6603, 'Borrar Publicacion', 'borrarPublicacion', 999, '0', 'glyphicon glyphicon-trash', null, null, 12181, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6604, 'Borrar Factura', 'borrarFactura', 999, '0', 'glyphicon glyphicon-trash', null, null, 14001, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6607, 'Borrar Proveedor', 'borrarProveedor', 999, '1', 'glyphicon glyphicon-trash', null, null, 12001, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6605, 'Borrar Tipo de Publicacion', 'borrarTipoPublicacion', 999, '1', 'glyphicon glyphicon-trash', null, null, 11001, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6601, 'Borrar Contrato', 'borrarContrato', 999, '0', 'glyphicon glyphicon-trash', null, null, 1008, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6606, 'Borrar Tipo de Incidencia', 'borrarTipoIncidencia', 999, '1', 'glyphicon glyphicon-trash', null, null, 11101, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6609, 'Borrar Usuario', 'borrarUsuario', 999, '0', 'glyphicon glyphicon-trash', null, null, 1401, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6613, 'Borrar Red Social', 'borrarRed', 999, '1', 'glyphicon glyphicon-trash', null, null, 1496, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6611, 'Borrar Regimen', 'borrarRegimen', 999, '1', 'glyphicon glyphicon-trash', null, null, 1480, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6615, 'Borrar Estado', 'borrarEstado', 999, '1', 'glyphicon glyphicon-trash', null, null, 1454, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6612, 'Borrar Target', 'borrarTarget', 999, '1', 'glyphicon glyphicon-trash', null, null, 1493, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6614, 'Borrar Pais', 'borrarPais', 999, '1', 'glyphicon glyphicon-trash', null, null, 1453, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6608, 'Borrar Cargo', 'borrarCargo', 999, '0', 'glyphicon glyphicon-trash', null, null, 1050, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6616, 'Borrar Municipio', 'borrarMunicipio', 999, '1', 'glyphicon glyphicon-trash', null, null, 1455, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6602, 'Borrar Cotizacion', 'borrarCotizacion', 999, '1', 'glyphicon glyphicon-trash', null, null, 1201, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6617, 'Borrar Recepcion', 'borrarRecepcion', 999, '1', 'glyphicon glyphicon-trash', null, null, 1484, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6618, 'Borrar Circulacion', 'borrarCirculacion', 999, '1', 'glyphicon glyphicon-trash', null, null, 1487, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6610, 'Borrar Recargo de Gestion', 'borrarRecargoGestion', 999, '0', 'glyphicon glyphicon-trash', null, null, 1470, 'grid', 'method_js', 'content', null, null);
INSERT INTO acl.operation (id, _name, url, _order, visible, icon, help, tooltip, id_operation, chk_render_on, chk_visual_type, chk_target_on, descripcion, imagen) VALUES (6600, 'Borrar Cliente', 'borrarCliente', 999, '1', 'glyphicon glyphicon-trash', null, null, 1001, 'grid', 'method_js', 'content', null, null);

UPDATE acl.operation SET _order=1 WHERE id=1052;
UPDATE acl.operation SET _order=2 WHERE id=1003;
UPDATE acl.operation SET _order=1 WHERE id=12100;
UPDATE acl.operation SET _order=2 WHERE id=12003;
UPDATE acl.operation SET _order=2 WHERE id=12183;

DELETE FROM public.contactos_clientes WHERE id >460 and id < 475;

ALTER TABLE public.contactos_clientes
    ADD CONSTRAINT contactos_clientes_fk FOREIGN KEY (cliente_id) REFERENCES public.clientes(id) ON UPDATE CASCADE ON DELETE CASCADE;
ALTER TABLE public.contactos_medios
  DROP CONSTRAINT medios_contactos_medios_fk;
ALTER TABLE public.contactos_medios
    ADD CONSTRAINT medios_contactos_medio_fk FOREIGN KEY (medio_id) REFERENCES public.medios(id) ON UPDATE CASCADE ON DELETE CASCADE;
