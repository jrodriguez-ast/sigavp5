CREATE OR REPLACE FUNCTION generar_ordenes_de_publicacion ()
  RETURNS trigger
AS
$BODY$
  DECLARE
  --DECLARACIÓN DE VARIABLES
  r                                     RECORD;
  r_cs                                  RECORD;
  r_csc                                 RECORD;
  id_publicacion_insert_current         INT;
  id_publicacion_version_insert_current INT;
  subtotal_publicacion                  DOUBLE PRECISION;
  total_publicacion                     DOUBLE PRECISION;
  total_impuesto_publicacion            DOUBLE PRECISION;
  i                                     INTEGER;
  total_dias                            INTEGER;
  fecha_inicio                          TIMESTAMP WITHOUT TIME ZONE;
  one_day                               CHARACTER VARYING(25);
  total_con_monto_avp                   DOUBLE PRECISION;
BEGIN
  --RECORRIDO DE LAS VERSIONES EN COTIZACIÓN
  --PARA OBTENER ID DE MEDIOS PARA LAS PUBLICACIONES
  FOR r IN
  SELECT
    medio_id,
    servicio_id
  FROM cotizaciones_servicios
  WHERE cotizacion_id = NEW.id
  GROUP BY medio_id, servicio_id
  LOOP
    subtotal_publicacion :=0.00;
    --VERIFICACIÓN QUE EJECUTE AL APROBAR LA COTIZACIÓN
    IF EXISTS(
        SELECT *
        FROM cotizaciones_servicios
        WHERE id NOT IN (
          SELECT cotizacion_servicio_id
          FROM ordenes_publicacion_versiones
          WHERE cotizacion_servicio_id IS NOT NULL
        )
              AND cotizacion_id = NEW.id
              AND servicio_id = r.servicio_id
              AND aprobada
    )
    THEN
      --SE CREAN LAS PUBLICACIONES DE ACUERDO A CADA MEDIO
      INSERT INTO ordenes_publicacion (
        cotizacion_id,
        cliente_id,
        fecha_creacion,
        con_impuestos,
        porcentaje_impuesto,
        fue_enviada,
        usuario_id,
        descuento,
        medio_id,
        porcentaje_avp,
        total_impuesto_avp,
        total_descuento,
        subtotal_con_descuento,
        firmante_nombre,
        firmante_cargo
      )
      VALUES (
        NEW.id,
        NEW.cliente_id,
        now(),
        TRUE,
        NEW.porcentaje_impuesto,
        FALSE,
        NEW.usuario_id,
        NEW.descuento,
        r.medio_id,
        NEW.porcentaje_avp,
        NEW.total_impuesto_avp,
        NEW.total_descuento,
        NEW.subtotal_con_descuento,
        (
          SELECT value
          FROM utilities.generic_configuration
          WHERE _label = 'publicacion_firmante_nombre'
        ),
        (
          SELECT value
          FROM utilities.generic_configuration
          WHERE _label = 'publicacion_firmante_cargo'
        )

      )
      RETURNING id
        INTO
          id_publicacion_insert_current;

      --Agrego el status de aprobada a la Orden de publicación
      INSERT INTO ordenes_publicacion_estatus_estatus (
        orden_publicacion_id, orden_publicacion_estatus_id, usuario_id)
      VALUES (
        id_publicacion_insert_current,
        (SELECT id
         FROM ordenes_publicacion_estatus
         WHERE estatus = 'Aprobada'),
        NEW.usuario_id
      );
      --RECORRIDO DE LAS VERSIONES EN COTIZACIÓN
      --PARA CREAR LAS VERSIONES PUBLICACIONES
      FOR r_cs IN
      SELECT *
      FROM cotizaciones_servicios
      WHERE cotizacion_id = NEW.id
            AND servicio_id = r.servicio_id
      LOOP
        IF NOT EXISTS(
            SELECT *
            FROM ordenes_publicacion_versiones
            WHERE cotizacion_servicio_id = r_cs.id
        )
        THEN
          subtotal_publicacion:=subtotal_publicacion + r_cs.monto_servicio;

          SELECT DATE_PART('day', r_cs.version_publicacion_fecha_fin :: TIMESTAMP WITHOUT TIME ZONE -
                                  r_cs.version_publicacion_fecha_inicio :: TIMESTAMP WITHOUT TIME ZONE
          )
          INTO total_dias;
          fecha_inicio := r_cs.version_publicacion_fecha_inicio :: TIMESTAMP WITHOUT TIME ZONE;
	  total_con_monto_avp = r_cs.valor_con_recargo * (1 + (select porcentaje_avp from cotizaciones cot where cot.id = NEW.id) / 100);

          FOR i IN 0..total_dias LOOP
            --          SE CREAN LAS VERSIONES PARA PUBLICACIONES por publicación de unico dia.
            INSERT INTO ordenes_publicacion_versiones (
              orden_publicacion_id,
              servicio_id,
              usuario_id,
              version_nombre,
              version_file,
              version_publicacion_fecha,
              monto_servicio,
              valor_con_recargo,
              version_segundos,
              version_apariciones,
              version_publicacion_fecha_inicio,
              version_publicacion_fecha_fin,
              medio_id,
              tipo_publicacion_id,
              tipo_publicacion_name,
              cotizacion_servicio_id,
              monto_con_avp
            )
            VALUES (
              id_publicacion_insert_current,
              r_cs.servicio_id,
              NEW.usuario_id,
              r_cs.version_nombre,
              r_cs.version_file,
              r_cs.version_publicacion_fecha,
              r_cs.valor_con_recargo,
              r_cs.valor_con_recargo,
              r_cs.version_segundos :: INT,
              r_cs.version_apariciones :: INT,
              --               r_cs.version_publicacion_fecha_inicio,
              --               r_cs.version_publicacion_fecha_fin,
              fecha_inicio :: DATE,
              fecha_inicio :: DATE,
              r_cs.medio_id,
              r_cs.tipo_publicacion_id,
              r_cs.tipo_publicacion_name,
              r_cs.id,
              total_con_monto_avp
            )
            RETURNING id
              INTO id_publicacion_version_insert_current;
            --RECORRIDO DE LOS DETALLES EN COTIZACIONES
            --PARA CREARLOS EN PUBLICACIONES
            FOR r_csc IN
            SELECT *
            FROM cotizaciones_servicios_condiciones
            WHERE cotizaciones_servicios_id = r_cs.id
            LOOP
              --SE CREAN LOS DETALLES PUBLICACIONES
              INSERT INTO
                ordenes_publicacion_versiones_condiciones (
                  ordenes_publicacion_versiones_id,
                  detalle_condicion_id,
                  usuario_id,
                  es_porcentaje,
                  tarifa,
                  condicion_id,
                  es_base
                )
              VALUES (
                id_publicacion_version_insert_current,
                r_csc.detalle_condicion_id,
                NEW.usuario_id,
                r_csc.es_porcentaje,
                r_csc.tarifa,
                r_csc.condicion_id,
                r_csc.es_base
              );
            END LOOP;

            fecha_inicio = fecha_inicio + INTERVAL '1 day';
          END LOOP;

        END IF;
      END LOOP;
      EXECUTE 'select round(' || subtotal_publicacion || '::numeric * ' || NEW.porcentaje_impuesto ||
              '/100::numeric,2) as ver'
      INTO total_impuesto_publicacion;
      EXECUTE 'select round(' || subtotal_publicacion || '::numeric +(' || subtotal_publicacion || '::numeric * ' ||
              NEW.porcentaje_impuesto || '/100::numeric),2) as ver'
      INTO total_publicacion;

      UPDATE
        ordenes_publicacion
      SET
        subtotal       = subtotal_publicacion,
        total_impuesto = total_impuesto_publicacion,
        total          = total_publicacion
      WHERE
        id = id_publicacion_insert_current;

      RAISE NOTICE '%', id_publicacion_insert_current;
    END IF;
  END LOOP;
  RETURN NEW;
END
$BODY$
LANGUAGE plpgsql VOLATILE;


--22/01/2015
-- campo nuevo en balances
ALTER TABLE balances
  ADD COLUMN nota_credito_id integer;
ALTER TABLE balances
  ADD FOREIGN KEY (nota_credito_id) REFERENCES notas_credito (id) ON UPDATE NO ACTION ON DELETE NO ACTION;
COMMENT ON COLUMN balances.nota_credito_id IS 'campo que relaciona el balance con la nota de crédito';

--24/01/2016
--nueva operacion
INSERT INTO acl.operation(
            id, _name, url,visible, icon, id_operation, 
            chk_render_on, chk_visual_type, chk_target_on, descripcion)
    VALUES (1042, 'Balance por tipo', 'balancextipo', B'1', 'glyphicon glyphicon-th-list', 1019, 
            'grid', 'method_js', 'content', 'balance por tipo');
