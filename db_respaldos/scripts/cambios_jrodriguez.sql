ALTER TABLE public.cotizaciones_servicios ADD publicada BIT DEFAULT '0' NOT NULL;

-- Actualizo las versiones que fueron publicadas.
/*
      UPDATE cotizaciones_servicios
      SET publicada = '1'
      WHERE id IN (SELECT opv2.cotizacion_servicio_id
                   FROM ordenes_publicacion_versiones opv2);
*/

DROP TRIGGER t_generar_ordenes_de_publicacion ON cotizaciones;

CREATE TRIGGER t_generar_ordenes_de_publicacion
  AFTER UPDATE OF aprobada
  ON cotizaciones
  FOR EACH ROW
  EXECUTE PROCEDURE generar_ordenes_de_publicacion();