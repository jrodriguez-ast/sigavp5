/**
 * Dependencias
 * Created by jrodriguez on 18/08/15.
 */
var gulp = require('gulp-param')(require('gulp'), process.argv),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    minifyCss = require('gulp-minify-css'),
    concatCss = require('gulp-concat-css'),
    replace = require('gulp-replace'),
    gutil = require('gulp-util'),
    runSequence = require('run-sequence');

/**
 * Min la aplicación, vendos, apps files y css.
 * @author Jose Rodriguez
 */
gulp.task('minify', function () {
    gutil.log('Corriendo minify');
    // Min app
    gulp.src([
        "app/route.js",
        "app/config/config.js",
        "app/js/lib/requestInterceptor.js",
        "app/js/lib/permission.js",
        "app/js/lib/crudAngular.js",
        "app/js/lib/app-utility.js",
        "app/js/lib/filters.js",
        'app/js/lib/utils.js',
        'app/js/lib/validate_format.js',
        'app/js/lib/rif-rnc.js',
        'app/models/*.js',
        'app/controllers/*.js',
        'app/modules/**/*.js',
        '!app/modules/template/**/*.js'
    ]).pipe(concat('todo.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/dist/'));

    // Min Vendors
    gulp.src([
        "app/js/vendor/angular-datatables/dist/angular-datatables.min.js",
        "app/js/vendor/angular-datatables/dist/plugins/bootstrap/angular-datatables.bootstrap.min.js",
        "app/js/min/angular-file-upload-shim.min.js",
        "app/js/min/angular-file-upload.min.js",
        "app/js/vendor/ngBootbox/dist/ngBootbox.min.js",
        "app/js/vendor/angular-toggle-switch/angular-toggle-switch.min.js",
        "app/js/vendor/ng-lodash/build/ng-lodash.min.js",
        "app/js/vendor/ng-password-strength/dist/scripts/ng-password-strength.min.js",
        "app/js/lib/angular-validation.js",
        "app/js/lib/angular-validation-rule.js",
        "app/js/vendor/ivh_treeview/js/ivh-treeview.js",
        "app/js/vendor/checklist-model/checklist-model.js",
        "app/js/vendor/angular-bootstrap-checkbox/angular-bootstrap-checkbox.js",
    ]).pipe(concat('vendor.concat.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/dist/'));

    // Min Css
    gulp.src([
        'app/css/app.css',
        'app/css/rdash-ast.css',
        'app/css/style_wizard.css',
        "app/js/vendor/ivh_treeview/css/ivh-treeview.css",
        "app/js/vendor/ivh_treeview/css/ivh-treeview-theme-basic.css",
        "app/js/vendor/angular-toggle-switch/angular-toggle-switch.css",
        "app/js/vendor/angular-toggle-switch/angular-toggle-switch-bootstrap.css"
    ]).pipe(concatCss('todo.min.css'))
        .pipe(minifyCss({compatibility: 'ie8'}))
        .pipe(gulp.dest('app/dist/'));
});

/**
 * Modifica la versión en el route y en index.
 * @author Jose Rodriguez
 */
gulp.task('update-version', function () {
    var _version = new Date().getTime();
    console.log(_version);
    gulp.src(['app/route.js'])
        .pipe(replace(/VERSION: '1\.0\.\d+'/g, 'VERSION: \'1.0.' + _version + '\''))
        .pipe(gulp.dest('app/'));

    return gulp.src(['app/index.html'])
        .pipe(replace(/\?v=1\.0\.\d+/g, '?v=1.0.' + _version))
        .pipe(gulp.dest('app/'));
});

/**
 * Modifica la versión de los archivos comprimidos.
 * @author Jose Rodriguez
 */
gulp.task('domain', function (env) {
    console.log(env);
    var domain = (env) ? '/sigavp' : '';
    console.log(domain);
    return gulp.src(["app/config/config.js"])
        .pipe(replace(/domain = .+;/g, 'domain = $location.host() + \'' + domain + '\';'))
        .pipe(gulp.dest('app/config/'));
});

/**
 * Corre todas la tareas en la secuencia descrita.
 * @author Jose Rodriguez
 */
gulp.task('build', function (env) {
    runSequence('domain', 'update-version', 'minify');
});


gulp.task('watch', function () {
    gulp.watch('app/**/*.js', ['minify']);
});

gulp.task('default', ['watch']);