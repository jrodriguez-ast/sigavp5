app.factory('permisionGeneric', [
    '$http', 'appConfig', '$q',
    function ($http, appConfig, $q) {

        return {
            server: {},

            /**
             * @param array location_path Ruta actual
             * @returns defer.promise
             *
             * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
             * @version 1.0 10/12/2014
             */
            get_permiso: function (location_path) {
                var _url = appConfig.getPathApi() + 'angularapp/get_permission/' + location_path;
                var defer = $q.defer();
                var server = this.server;
                $http.get(_url).
                    then(
                    function success(response) {
                        server.get = response;
                        defer.resolve(server);
                    },
                    function error(reason) {
                        return false;
                    }
                );
                return defer.promise;
            }
        };

    }
]);
