/*Modelo de las notificaciones del sistema*/
app.factory('NotifacionModel', ["$resource", "appConfig", function ($resource, appConfig) {
    var url = appConfig.getPathApi();
    var params = {};
    var _object = $resource(url, params, {
        update: {method: 'PUT'},
        //Actualizo la notificación.
        UpdateNotificaciones: {
            method: 'GET',
            url: appConfig.getPathApi() + 'notificacion/notificacionsave/:id',
            params: {
                id: '@id'
            },
            isArray: true
        },
        //Total de notificaciones
        numnotificate: {
            method: 'GET',
            //url: appConfig.getPathApi() + 'notificaciones',
            url: appConfig.getPathApi() + 'notificacion/mostrar_num_noti/',
            resolve: {},
            params: {},
            cargando: false,
            isArray: true
        },
        //listado de notificaciones del lightbox muestra las que no han visto
        listado: {
            method: 'GET',
            url: appConfig.getPathApi() + 'notificacion/notifica',
            params: {},
            isArray: true
        },
        //Ver todos las notificaciones sin condiciones sí ha sido visto o no.
        verlistado: {
            method: 'GET',
            //url: appConfig.getPathApi() + 'notificaciones',
            url: appConfig.getPathApi() + 'notificacion/notificatodo',
            params: {},
            isArray: true
        }
    });
    return _object;
}]);

