/**
 * Created by jeferson on 06/11/14.
 */

app.factory('genericApiServices', ["$injector", function ($injector) {
    var $q = $injector.get('$q', 'genericApiServices');
    var $http = $injector.get('$http', 'genericApiServices');
    var appConfig = $injector.get('appConfig', 'genericApiServices');
    var flashMessages = $injector.get('flashMessages', 'genericApiServices');
    return {
        data: {},

        /*
         * Ejecuta una petición get al api definido en appconfig.
         *
         * @param _url contiene la ruta donde se va a realizar la petición
         * @param _params contiene los parámetros a enviar en la petición
         * @return {*}
         */
        runGetServices: function (_url, _params) {
            _params = _params || {};
            return $http({
                method: 'get',
                url: appConfig.getAdminPathApi() + _url,
                params: _params
                //cache: $templateCache
                //}).success(function (data) {
            }).success(function () {
                //return data;
            }).error(function (data, status) {
                flashMessages.show_error(msjError(status));
            });
        },

        /*
         * Ejecuta una petición post al api definido en appconfig.
         *
         * @param _url contiene la ruta donde se va a realizar la petición
         * @param _params contiene los parámetros a enviar en la petición
         * @return {*}
         */
        runPostServices: function (_url, _params) {
            return $http({
                method: 'post',
                url: appConfig.getAdminPathApi() + _url,
                data: _params
                //cache: $templateCache
                //}).success(function (data) {
            }).success(function () {
                //return data;
            }).error(function (data, status) {
                //var msjText = 'Ha ocurrido un error..!! ( "Error' + status + '" )';
                flashMessages.show_error(msjError(status));
            });
        }
    };

    /**
     * Maneja un solo mensaje de error.
     * @param status
     * @returns {string}
     */
    function msjError(status) {
        return 'Ha ocurrido un error..!! ( "Error' + status + '" )';
    }
}]);