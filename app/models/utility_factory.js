/**
 * Medtodo para la control y gestion de los mensages en el sistema
 *
 * @packages Soluciones A.S.T
 * @author Jefferson Lara <jetox21@gmail.com>,Frederick Bustamante <frederickdanielb@gmail.com>
 * @version V-1.2.1
 *
 */
app.factory("flashMessages", ["$rootScope", "$timeout", function ($rootScope, $timeout) {

    var tiempoMostrar = 3500;
    var timeApplyZindex = 50;

    var close_alert = function () {
        $rootScope.flash_success = "";
        $rootScope.flash_error = "";
        $rootScope.flash_info = "";
        $rootScope.type = '';
        angular.element('#content-alert').css('z-index', '-2')
    };
    var set_zindex = function () {
        angular.element('#content-alert').css('z-index', '100000')
    };

    $rootScope.closeAlert = function () {
        $rootScope.flash_success = "";
        $rootScope.flash_error = "";
        $rootScope.flash_info = "";
        $rootScope.type = '';
    };

    return {
        show_success: function (message) {
            $rootScope.flash_success = message;
            $rootScope.type = 'success';
            $timeout(function () {
                close_alert();
            }, tiempoMostrar);
            $timeout(function () {
                set_zindex();
            }, timeApplyZindex);
        },
        show_error: function (message) {
            $rootScope.flash_error = message;
            $rootScope.type = 'error';
            $timeout(function () {
                close_alert();
            }, tiempoMostrar);
            $timeout(function () {
                set_zindex();
            }, timeApplyZindex);
        },
        show_info: function (message) {
            $rootScope.flash_info = message;
            $rootScope.type = 'info';
            $timeout(function () {
                close_alert();
            }, tiempoMostrar);
            $timeout(function () {
                set_zindex();
            }, timeApplyZindex);
        },
        clear: function () {
            close_alert();
        }
    };
}]);

/**
 * Método para almacenar data global y usarla desde otros controladores
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 */
app.factory('globalData', function () {
    return {
        data: {}
    }
});
