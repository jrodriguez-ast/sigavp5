/*Controlador del modal  Inicio del Modal*/
app.controller('ModalInstanceCtrl', ['$scope', '$modalInstance', 'NotifacionModel', 'items', '$state', function ($scope, $modalInstance, NotifacionModel, items, $state) {
    $scope.items = items;

    //$scope.senditem = function (iditem) {
    //
    //    if (angular.isDefined(iditem)) {
    //        angular.forEach($scope.items, function (value) {
    //            $scope.air = value.lin;//Contiene el link a donde iremos
    //            $scope.id = value.ids;//Contiene el link a donde iremos
    //
    //        });
    //        console.log($scope.air);
    //        console.log($scope.id);
    //
    //     //   $scope.UpDateNoti = NotifacionModel.UpdateNotificaciones({id: iditem});//Actualiza la notifiacion
    //    }
    //    $state.go($scope.air,{'cotizacion_id': $scope.id});//Nos envía al link que esta en BD
    //    $modalInstance.dismiss('cancel');//Cierra el modal
    //};

    //funcion que manda al link que esta guardado en base de datos
    $scope.senditem = function (iditem, lin, ids) {

        //console.log(lin);
        //console.log(ids);
        var parametros = JSON.parse(ids);
        //console.log(parametros);
        if (angular.isDefined(iditem)) {
             $scope.UpDateNoti = NotifacionModel.UpdateNotificaciones({id: iditem});
        }
        $state.go(lin,parametros);
        $modalInstance.dismiss('cancel');//Cierra el modal
    };

    //Ver todos las notifiaciones en lista y cierra el modal
    $scope.vertodo = function () {
        $scope.verlis = NotifacionModel.verlistado();
        $state.go('admin.notificaciontodo', {'data': $scope.verlis});
        $modalInstance.dismiss('cancel');
    };

    //cierra el modal
    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

}]);

//Numero de notificaciones
app.controller('totalnotificacion', ['$scope', '$interval', 'NotifacionModel', function ($scope, $interval, NotifacionModel) {
    //$interval(function () {
    //    $scope.cant_noti = NotifacionModel.numnotificate();
    //}, 180000);
}]);


//Listado de todas las notificaciones
app.controller('ContControllerNotificacionTodo', [
    '$scope',
    '$state',
    'NotifacionModel',
    '$stateParams',
    function ($scope,
              $state,
              NotifacionModel,
              $stateParams) {

        $scope.actualiza = function (iditem, lin, ids) {
            //console.log(lin);
            //console.log(ids);
            var parametros = JSON.parse(ids);
            if (angular.isDefined(iditem)) {
                 $scope.UpDateNoti = NotifacionModel.UpdateNotificaciones({id: iditem});
                $state.go(lin, parametros);
            }
        };

        $scope.ddatos = $stateParams.data;

    }]);

