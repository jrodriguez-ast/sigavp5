/**
 * Created by darwin on 12/01/15.
 */

app.controller("appCtrl", [
    '$scope', '$location', '$ngBootbox', '$state', '$modal', 'Session', 'AUTH_EVENTS', 'AuthService', '$window', 'flashMessages', 'appConfig',
    function ($scope, $location, $ngBootbox, $state, $modal, Session, AUTH_EVENTS, AuthService, $window, flashMessages, appConfig) {

        /**
         * Este método es ejecutado desde el factory AuthService al momento de realizar el login en la aplicación
         * @param success
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 14/01/15
         */
        $scope.$on(AUTH_EVENTS.loginSuccess, function () {
            setSettingsApp();
        });

        /**
         * Muestra un mensaje al disparar el evento de un fallo en el login
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 20/01/15
         */
        $scope.$on(AUTH_EVENTS.loginFailed, function (event, msg) {
            flashMessages.show_error(msg);
        });


        /**
         * Levanta un evento cada vez que se verifica la sesión.
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 20/01/15
         */
        $scope.$on(AUTH_EVENTS.authChecked, function () {
            setSettingsApp();
        });


        /**
         * Variable del scope que contiene las opciones que se van a mostrar en el menú de la aplicación
         * @type json
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 20/01/15
         */
        $scope.menu = null;

        /**
         * Al iniciar sesión contiene un arreglo con los datos del usuario
         * @type {null}
         */
        $scope.user = null;

        /**
         * Ejecuta el logout en el servidor
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 16/01/15
         */
        $scope.logout = function () {
            AuthService.logout().then(function () {
                $window.location.reload();
            });
        };

        /**
         * Método inicial, se encarga de verificar si el usuario tiene una sesión activa o no.
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 14/01/15
         */
        $scope.startApp = function () {
            if (!$scope.logged) {
                $state.go('login');
                $scope.slideMenu = null;
                $scope.headerBar = null;
            }
            else {
                if ($state.is('login')) {
                    //Se verifica si el usuario tiene ID de cliente definida
                    if (!localStorage.cliente_id) {
                        $location.path('admin/detalle_cliente/' + localStorage.cliente_id + '/ficha');
                    } else {
                        $state.go('admin.inicio');
                    }
                }
                $scope.headerBar = "views/layout/header_bar.html";
                $scope.slideMenu = 'views/layout/menu.html';
            }
        };


        /**
         * Configura los valores iniciales de la aplicación
         */
        var setSettingsApp = function () {
            $scope.logged = Session.sessionStatus;
            $scope.menu = Session.sessionMenu;
            $scope.user = Session.user;
        };

        /**
         * Observador de la variable logged.
         *
         * Con el cambio de esta variable se verifica la vista a mostrar si hay sesión activa o no.
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 14/01/15
         */
        $scope.$watch("logged", function (newValue, oldValue) {
            if (newValue !== oldValue) {
                $scope.startApp();
            }
        });
        /*Moises Inicio del modal*/
        $scope.animationsEnabled = true;
        $scope.open = function (size) {
            var modalInstance = $modal.open({
                animation: $scope.animationsEnabled,
                templateUrl: 'views/layout/myModalContent.html',
                controller: 'ModalInstanceCtrl',
                size: size,
                resolve: {
                    items: ['NotifacionModel', function (NotifacionModel) {
                        return NotifacionModel.listado().$promise;
                    }]
                }
            });

        };
        $scope.toggleAnimation = function () {
            $scope.animationsEnabled = !$scope.animationsEnabled;
        };
        /*Moises Fin del modal*/
        $scope.appName = appConfig.appName;
        $scope.appDescription = appConfig.appDescription;
        $scope.isTestingMode = appConfig.isTestingMode();


        /**
         * Permite verificar la ruta invocada en el menu para aplicar comportamiento personalizado
         */
        $scope.checkUrl = function (url) {

            if (angular.isDefined(url)) {


                switch (url) {
                    case 'admin/list_client':

                        //Se verifica si el usuario tiene ID de cliente definida
                        if (localStorage.cliente_id != 'false') {
                            $location.path('admin/detalle_cliente/' + localStorage.cliente_id + '/ficha');
                           // console.log("AQUI");
                        } else {
                            window.alert("ES ID ES FALSE");
                            $state.go('admin.list_client');
                        }
                        break;

                    default:
                        //Sentencias_def ejecutadas cuando no ocurre una coincidencia con los anteriores casos
                        $state.go('^.'+url.replace('/','.'));
                        break;
                }
            }
        }
    }

]);