'use strict';

/**
 * Created by Carlos BLanco.
 */
app.factory('targetModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'configuracion/target/:id', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'configuracion/target/:id',
                params: {
                    id: '@id'
                },
                isArray: false
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'configuracion/target',
                isArray: false
            },
            delete: {
                method: 'DELETE'
            }
        });
    }
]);