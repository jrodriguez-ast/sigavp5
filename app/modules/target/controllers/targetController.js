'use strict';

/**

 * @author Carlos blanco <cebs923@gmail.com>
 * @package SIGAVP
 * @version V-1.0 07/09/2015
 */
app.controller('TargetCtrlC', [
    '$scope',
    'particularPermission', '$injector',
    function
        ($scope,
         particularPermission, $injector) {
        var flashMessages = $injector.get('flashMessages', 'TargetCtrlC');
        var $ngBootbox = $injector.get('$ngBootbox','TargetCtrlC');
        var crudFactory = $injector.get('crudFactory', 'TargetCtrlC');

        $scope.title = 'Target';
        $scope.siteCrud = {url: 'configuracion/target_dt'};

        $scope.headerCrud = [

            {title: 'Nombre', label: 'nombre', visible: true},
            {title: 'ID', label: 'id', visible: false}
        ];

        $scope.operation = particularPermission.permission;

        /**
         *
         *
         * @author Amalio Velasquez
         * */
        $scope.borrarTarget = function(target_id){

            $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
                function(){
                    var target = $injector.get('targetModel','confirm');

                    target.remove(
                        {
                       id: target_id
                        },
                        function(success){
                            if(success.status == 'ok'){
                                crudFactory.reloadData();
                                flashMessages.show_success(success.message);
                            }
                        },
                    function(error){
                        crudFactory.reloadData();
                        flashMessages.show_error(error.data.message);
                        }
                    );
                }
            );
        }
    }

]);
app.controller("CrearTargetController", [
    "$scope",
    "$stateParams",
    'flashMessages',
    '$injector',
    'targetModel',
    '$state',
    function ($scope,
              $stateParams,
              flashMessages,
              $injector,
              targetModel,
              $state) {

        $scope.detalles = {};
        $scope.detalles.nombre = '';

        /*
         Objeto que se enviara al Backend
         */

        $scope.contact = {};
        $scope.contact.nombre = '';


        var $validationProvider = $injector.get('$validation');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y resive las respuestas pertinentes.
             *
             * @author Jose Rodriugez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                $scope.contact.nombre = $scope.detalles.nombre;
                targetModel.crear(
                    $scope.contact,
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors);
            },
            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando todo esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha creado el target.");
                $state.go('admin.target');
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                    }

                    //if (angular.isDefined(message.nombre)) {
                    //    mensajeToShow = mensajeToShow.concat(message.nombre);
                    //}
                    //if (angular.isDefined(message.apellido)) {
                    //    mensajeToShow = mensajeToShow.concat(message.apellido);
                    //}
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);


app.controller('EditarTargetController', [
    "$scope",
    "$stateParams",
    "$injector",
    "r_data",
    "targetID",

    function (
        $scope,
        $stateParams,
        $injector,
        r_data,
        targetID ) {

        var $validationProvider = $injector.get('$validation');

        $scope.detalles = { };
        $scope.detalles = r_data.data;

        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },

            prepare:function(object) {
                object = angular.copy(object);
                return object;
            },

            send: function () {
                var targetModel = $injector.get('targetModel');

                var params = {id: targetID};
                targetModel.update(
                    params,
                    $scope.mediocontacto_form.prepare($scope.detalles),
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors
                );
            },
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando todo esta ok.
                // if(response.status == 'ok'){
                var flashMessages = $injector.get('flashMessages');
                var $location = $injector.get('$location');
                flashMessages.show_success("El nombre del target fue actualizado");
                // }
                $location.path("admin/target");
            },

            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {
                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.description)) {
                        mensajeToShow = mensajeToShow.concat(message.description);
                    }
                    //if (angular.isDefined(message.tipo_publicacion_id)) {
                    //    mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                    //}
                }

                var flashMessages = $injector.get('flashMessages');
                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);
