'use strict';

/**
 * Created by darwin on 02/06/15.
 */
app.factory('Servicio', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        var _url = appConfig.getPathApi() + 'medios/medio/:medioId/servicio/:serviceId';
        var params = {medioId: '@medioId', serviceId: '@serviceId'};
        return $resource(
            _url,
            params,
            {
                update: {method: 'PUT'},
                listado: {
                    method: 'GET',
                    url: appConfig.getPathApi() + 'medios/:medioId/:tipoPublicacionId/servicios/listado/:modo/:servicioPadre/:servicioHijo',
                    params: {
                        medioId: '@medioId',
                        tipoPublicacionId: '@tipoPublicacionId',
                        modo: '@modo',
                        servicioPadre: '@servicioPadre',
                        servicioHijo: '@servicioHijo'
                    },
                    isArray: true
                },
                listarfrommedio: {
                    method: 'GET',
                    url: appConfig.getPathApi() + 'medios/:medioId/:tipoPublicacionId/servicios/listado',
                    params: {
                        medioId: '@medioId',
                        tipoPublicacionId: '@tipoPublicacionId'
                    },
                    isArray: true
                },
                crear: {
                    method: 'POST', // medios/medio/{medio}/servicio
                    url: appConfig.getPathApi() + 'medios/medio/:medioId/servicio',
                    params: {
                        medioId: '@medioId'
                    },
                    isArray: false
                },
                hasbasecondition: {
                    method: 'GET',
                    url: appConfig.getPathApi() + 'medios/servicio/:serviceId/hasbase',
                    params: {
                        serviceId: '@serviceId'
                    },
                    isArray: false
                },
                children: {
                    method: 'GET',
                    url: appConfig.getPathApi() + 'medios/servicio/:parentId/children',
                    params: {
                        parentId: '@parentId'
                    },
                    isArray: false
                },
                listChildren: {
                    method: 'GET',
                    url: appConfig.getPathApi() + 'medios/service/listchildren/:parentId',
                    params: {
                        parentId: '@parentId'
                    },
                    isArray: false
                }
            }
        );
    }
]);