'use strict';

/**
 * Gestiona el listado de Servicios
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 01/06/15 19:18:08
 */
app.controller("ServiciosListController", ["$scope", "$injector", 'particularPermission', 'clientID', function ($scope, $injector, particularPermission, clientID) {
    $scope.siteCrud = {url: 'medios/' + clientID + '/servicios/dt'};
    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {
            title: 'Tipo', label: 'tipo_publicacion.nombre', filter: 'link',
            filterParams: {
                relatedId: 'tipo_publicacion_id',
                url: '#/admin/TiposPublicaciones/view'
            }
        },
        {title: 'Nombre', label: 'nombre'},
        {title: 'Hijos', label: 'hijos', filter: 'listadoNombreValor'}
    ];

    $scope.operation = particularPermission.permission;

    /**
     * Crea el nuevo tab de servicios en modo edit.
     * @param serviceID
     * @param operation_id
     */
    $scope.newTabServices = function (serviceID, operation_id) {
        var tab = {
            id: operation_id,
            title: 'Editar Medio #',
            state: 'admin.detalle_medio.servicio({serviceID:' + serviceID + '})',
            object_id: serviceID
        };
        $scope.buildTab(tab);
        var $state = $injector.get('$state', 'ServiciosListController.newTabServices');
        $state.go('^.servicio', {'serviceID': serviceID});
    };

    /**
     * Crea el nuevo tab de servicios en modo view.
     * @param serviceID
     * @param operation_id
     */
    $scope.newTabServicesView = function (serviceID, operation_id) {
        var tab = {
            id: operation_id,
            title: 'Ver Medio #',
            state: 'admin.detalle_medio.servicio_view({serviceID:' + serviceID + '})',
            object_id: serviceID
        };
        $scope.buildTab(tab);
        var $state = $injector.get('$state', 'ServiciosListController.newTabServicesView');
        $state.go('^.servicio_view', {'serviceID': serviceID});
    };
}]);

/**
 * Maneja el Tab de Servicios y formulario de servicios.
 *
 * @param  {object} $scope                 AngularJS Object
 * @param  {object} $injector              AngularJS Object
 * @param  {object} resolveServices        Resolves del state asociado al controlador en route.js.
 * @param  {object} TipoPublicacionResolve Resolves del state asociado al controlador en route.js.
 * @param  {object} resolveTabId           Resolves del state asociado al controlador en route.js.
 * @param  {object} particularPermission   Resolves del state asociado al controlador en route.js.
 * @param  {object} rViewMode              Resolves del state asociado al controlador en route.js.
 * @return {void}
 * @author Jose Rodriguez
 * @version V-1.2 19/06/15 11:19:16
 */
app.controller('ServiciosTabController', [
    '$scope', "$injector", 'resolveServices', 'TipoPublicacionResolve', 'resolveTabId', 'particularPermission',
    'rViewMode', '$modal', 'rChildren', 'rListChildren',
    function ($scope, $injector, resolveServices, TipoPublicacionResolve, resolveTabId, particularPermission,
              rViewMode, $modal, rChildren, rListChildren) {

        //  $scope.service.hijos = rListChildren.data;
        // Directiva de Validación.
        var $validationProvider = $injector.get('$validation', 'ServiciosTabController');
        var globalData = $injector.get('globalData', 'ServiciosTabController');
        globalData.msjNuevoServicio = "";

        // Más información ver en el archivo router y la ruta 'condictions/add'
        $scope.tipos_publicaciones = TipoPublicacionResolve;
        $scope.service = resolveServices.data;
        $scope.getCadenas = function (tipo) {
            if (tipo == 'Radial' || tipo == 'Televisivo') {
                var Medio = $injector.get('Medio', 'getCadenas');
                Medio.listCadenas({
                    id: $scope.service.medio_id,
                    tipoPub: $scope.service.tipo_publicacion.id
                }).$promise.then(function (resp) {
                    $scope.service.cadena = $scope.service.parent[0];
                    $scope.cadenas = resp;
                });
            }
        };
        $scope.getCadenas($scope.service.tipo_publicacion.nombre);

        //console.log($scope.service);
        //console.log($scope.service.hijos);
        if (!rViewMode) {
            //$scope.service.hijos = rListChildren.data;
            if (angular.isUndefined($scope.service.otros_detalles) || $scope.service.otros_detalles === null)
                $scope.service.otros_detalles = {};

            $scope.service.otros_detalles.hijos = rListChildren.data;

            // $scope.service.ArregloHijos = [];
            /*for(var i = 0; i<$scope.service.hijos.length; i++){
             $scope.service.ArregloHijos[i].medio = {};
             $scope.service.ArregloHijos[i].medio = $scope.service.hijos[i];
             }*/
            //console.log(rListChildren.data);
        }
        $scope.permissions = preparePermissions(particularPermission.permission);
        $scope.viewMode = rViewMode;

        if (angular.isDefined($scope.service.condiciones) && $scope.service.condiciones.length == 0) {
            globalData.msjNuevoServicio = "Debe crear una Tarifa Base para el Medio que acaba de crear puesto que esta es usada en los cálculos de cotizaciones";
            angular.element('#add_condicion').trigger('click');
        }

        // Verifico el state donde estoy.
        var $state = $injector.get('$state', 'ServiciosTabController.');
        var title = ($state.current.name == 'admin.detalle_medio.servicio') ? 'Editar' : 'Ver';

        $scope.buildTab({
            id: resolveTabId,
            state: $state.current.name + '({serviceID: ' + resolveServices.data.id + '})',
            title: title + ' Medio #',
            object_id: resolveServices.data.id
        });

        $scope.services_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.services_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },

            /**
             * Prepara la data de servicios a ser enviada al servidor.
             * @param  {json} service
             * @return {json}           data preparada.
             * @author Jose Rodriguez
             * @version V-1.0 16/06/15 17:25:32
             */
            prepare: function (service) {//service.otros_detalles.hijos
                //console.log(service);
                //console.log($scope.service.otros_detalles.hijos);
                service.children = [];
                if($scope.service.otros_detalles.hasOwnProperty('hijos')){
                    for (var i = 0; i < $scope.service.otros_detalles.hijos.length; i++) {
                        service.children[i] = $scope.service.otros_detalles.hijos[i].medio.id;
                    }
                }

                if (angular.isDefined($scope.service.cadena)) {
                    service.parent = $scope.service.cadena.id;
                }

                return angular.copy(service);
            },

            /**
             * Enviar las servicios al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodriguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                var serviceModel = $injector.get('Servicio', 'ServiciosTabController.SendData');

                var params = {
                    medioId: resolveServices.data.medio_id,
                    serviceId: resolveServices.data.id
                };

                serviceModel.update(
                    params,
                    $scope.services_form.prepare($scope.service),
                    $scope.services_form.success,
                    $scope.services_form.errors
                );
            },

            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {
                // No evaluamos OK xq el servidor responde 200 siempre que este ok.
                if (response.status == 'ok') {
                    var flashMessages = $injector.get('flashMessages', 'ServiciosTabController.success');
                    flashMessages.show_success("Los datos del Medio fueron actualizados.");
                }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {
                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.nombre)) {
                        mensajeToShow = mensajeToShow.concat(message.nombre);
                    }
                    if (angular.isDefined(message.tipo_publicacion_id)) {
                        mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                    }
                }

                var flashMessages = $injector.get('flashMessages', 'ServiciosTabController.errors');
                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        };

        /**
         * prepara los permisos asociados al vista de servicios,
         * de acuerdo al lugar donde deben ubicarse.
         *
         * @param  {json} permissions permisos en bd.
         * @return {{actionbar: Array, list: Array}}
         * @author Jose Rodriguez
         * @version V-1.0 18/06/15 10:31:05
         */
        function preparePermissions(permissions) {
            var permisos = {actionbar: [], list: []};
            var $state = $injector.get('$state', 'ServiciosTabController.');

            for (var i = permissions.length - 1; i >= 0; i--) {
                permissions[i].state = permissions[i].url.split('/').join('.');

                if ($state.current.name == 'admin.detalle_medio.servicio_view') {
                    permissions[i].state = permissions[i].state.replace('servicio', 'servicio_view');
                }

                if (permissions[i].chk_render_on == 'actionbar') {
                    permisos.actionbar.push(permissions[i]);
                    continue;
                }
                permisos.list.push(permissions[i]);
            }
            return permisos;
        }

        /**
         * Modal para agregar detalles adicionales al servicio
         */
        $scope.medioServiceDetailModal = function (current_tipo_publicacion) {
            //$scope.otros_detalles_servicios = [];
            $scope.otros_detalles_servicios = angular.fromJson($scope.service.otros_detalles);
            //var scope = $scope.modalDetalleServicio;
            var globalData = $injector.get('globalData', 'medioServiceDetailModal');
            var CONSTANTES = $injector.get('CONSTANTES');
            var modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/servicios/views/modal_detalles_adicionales.html?v=' + CONSTANTES.VERSION,
                controller: 'medioServiceDetailModalCtrl',
                size: 'lg',
                resolve: {
                    datos: function () {
                        return {
                            'detalleactual': $scope.otros_detalles_servicios,
                            'current_tipo_publicacion': current_tipo_publicacion
                        }
                    },
                    r_circulacion: ["circulacionModel", function (circulacionModel) {
                        return circulacionModel.get().$promise;
                    }],
                    r_target: ["targetModel", function (targetModel) {
                        return targetModel.get().$promise;
                    }],
                    r_redSocial: ["redSocialModel", function (redSocialModel) {
                        return redSocialModel.get().$promise;
                    }]


                }
            });
            modalInstance.result.finally(function () {
                if (!angular.isUndefined(globalData.otros_detalles_servicios))
                    $scope.service.otros_detalles = globalData.otros_detalles_servicios;
            });
        };

        /**
         * Permite realizar el borrado o desactivación de una condición.
         * @param condicion_id
         */
        $scope.borrarCondicion = function (condicion_id) {
            var $ngBootbox = $injector.get('$ngBootbox', 'borrarCondición');
            $ngBootbox.confirm('¿Esta seguro de Eliminar esta tarifa?')
                .then(function () {
                    var $stateParams = $injector.get('$stateParams', 'borrarCondición.then.ok');
                    var $state = $injector.get('$state', 'model.delete.ok');
                    var flashMessages = $injector.get('flashMessages', 'model.delete.ok');
                    var model = $injector.get('CondicionModel', 'borrarCondición.then.ok');
                    var params = {medioId: $stateParams.id, servicioId: $stateParams.serviceID, id: condicion_id};

                    model.delete(params, function (success) {
                        if (success.status.toLowerCase() == 'ok') {
                            flashMessages.show_success(success.message);
                            $state.go('.', {}, {reload: true});
                        }
                    }, function (error) {
                        error = error.data.errors;
                        if (error.code == 423) {
                            $ngBootbox.confirm(error.message + '<br/>¿Desea continuar con el Borrado?').then(function () {
                                params.deactivate = true;
                                model.delete(params, function (success) {
                                    if (success.status.toLowerCase() == 'ok') {
                                        flashMessages.show_success(success.message);
                                        $state.go('.', {}, {reload: true});
                                    }
                                }, function (error) {
                                    error = error.data.errors;
                                    if (error.code == 423) {
                                        flashMessages.show_error('La tarifa no pudo ser eliminada por favor intente de nuevo.');
                                    }
                                });
                            });
                        }
                    });
                });
        };

        /**
         * Activa una condición desactivada.
         * @param condicion_id
         */
        $scope.activarCondicion = function (condicion_id) {
            var $ngBootbox = $injector.get('$ngBootbox', 'activarCondición');
            $ngBootbox.confirm('¿Esta seguro de desea Reactivar esta tarifa?').then(function () {
                var $stateParams = $injector.get('$stateParams', 'then.ok');
                var model = $injector.get('CondicionModel', 'then.ok');
                var params = {medioId: $stateParams.id, servicioId: $stateParams.serviceID, condicionId: condicion_id};
                model.activate(params, function (success) {
                    $injector.get('flashMessages', 'success').show_success(success.message);
                    $injector.get('$state', 'success').go('.', {}, {reload: true});
                    //}, function (error) {
                    //console.log(error);
                });
            });
        };

        $scope.tarifario = function (service_id, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Tarifario Medio #',
                state: 'admin.detalle_medio.tarifario',
                object_id: service_id
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state', 'tarifario');
            $state.go('^.tarifario', {'service_id': service_id});
        };
        /**
         *Modal de Hijos del servicio
         */
        $scope.serviceChildrenModal = function (index, objeto, servicio, medio) {
            var C = $injector.get('CONSTANTES', 'ServiciosTabController');

            //console.log(servicio.medio_id);
            //console.log(servicio[0]);
            //console.log("AQUI SE ABRE EL MODAL");
            var modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/medios/views/servicios/form_hijos.html?v=' + C.VERSION,
                controller: 'serviceChildModalCtrl',
                size: 'lg',
                resolve: {
                    datos: function () {
                        return {
                            'indice': index,
                            'objetoservicio': objeto,
                            'servicio': servicio,
                            'medio': medio,
                            'tipospublicaciones': $scope.tipospublicaciones
                        };
                    },
                    rListMediosServicios: ['Medio', function (Medio) {
                        return Medio.listChildren({
                            id: $scope.service.medio_id,
                            tipoPub: $scope.service.tipo_publicacion_id
                        }).$promise;
                    }]
                }
            });
            modalInstance.result.then(function () {
                // $scope.dataForm.servicios.detalles[index].otros_detalles = globalData.otros_detalles_servicios;
            });
        };
    }]);

/**
 *
 */
app.controller("medioServiceDetailModalCtrl", [
    "$scope", "$modalInstance", "$injector", "datos", "r_circulacion", 'r_target', 'r_redSocial',
    function ($scope, $modalInstance, $injector, datos, r_circulacion, r_target, r_redSocial) {
        var flashMessages = $injector.get('flashMessages', 'medioServiceDetailMCtrl');
        var estadoModel = $injector.get('estadoModel', 'medioServiceDetailMCtrl');
        var globalData = $injector.get('globalData', 'medioServiceDetailMCtrl');

        $scope.editable = [];
        $scope.todosdatos = datos;
        $scope.circulacion = r_circulacion.data;
        $scope.target = r_target.data;
        $scope.redsocial = r_redSocial.data;
        $scope.service = {};
        $scope.service.otros_detalles = [];
        $scope.service.otros_detalles[0] = {};

        if(angular.isArray(datos.detalleactual) &&
            datos.detalleactual.hasOwnProperty('0') &&
            datos.detalleactual[0].hasOwnProperty('circulacion')){
            $scope.service.otros_detalles[0].circulacion ={'id': $scope.todosdatos.detalleactual[0].circulacion.id};
        }

        $scope.estado = [];
        $scope.municipio = [];
        $scope.datos = [];
        estadoModel.get().$promise.then(function (resultado) {
            $scope.estado = resultado.data;
            $scope.estado.unshift({'id': '', 'nombre': 'Seleccione el Estado', disabled: 'out'});
        });
        $scope.tipo_publicacion = datos.current_tipo_publicacion;

        if (datos.detalleactual.length > 0) {
            $scope.detalles_servicios = angular.copy(datos.detalleactual);
            if (angular.isUndefined($scope.detalles_servicios[0]['alcance']) || angular.isUndefined($scope.detalles_servicios[0]['red_social'])) {
                $scope.detalles_servicios[0]['alcance'] = [];
                $scope.detalles_servicios[0]['red_social'] = [];
            } else {
                angular.forEach($scope.detalles_servicios[0].alcance, function (value) {
                    if (value.estado.id == 0 && value.municipio.id == 0) {
                        $scope.datos.nacional = true;
                    }
                });
            }

        } else {
            $scope.detalles_servicios = [{
                alcance: [],
                red_social: []
            }];
        }

        $scope.nivelAlcance = function () {
            if (!angular.isUndefined($scope.detalles_servicios[0].alcance)) {
                $scope.detalles_servicios[0].alcance = [];
            }
            if ($scope.datos.nacional) {
                $scope.datos.estado = {id: ''};
                $scope.municipio = [];
                $scope.detalles_servicios[0].alcance.push({
                    estado: {id: 0, nombre: 'Todos'},
                    municipio: {id: 0, nombre: 'Todos'}
                });
            }
        };
        $scope.cargarMunicipio = function () {
            $scope.municipio = [];
            if ($scope.datos.estado.id != null) {
                estadoModel.get({id: $scope.datos.estado.id}).$promise.then(function (resultado) {
                    $scope.municipio = resultado.data.municipio;
                    $scope.municipio.unshift(
                        {
                            'id': 'todos',
                            'nombre': 'TODOS'
                        }
                    );
                });
            }
        };
        $scope.agregarAlcance = function () {
            var add = true;
            if ($scope.datos.municipio == null || $scope.datos.estado == null) {
                flashMessages.show_info('Debe seleccionar un municipio para poder agregar a la lista!');
                return false;
            } else {
                var removeKeys = [];
                if ($scope.detalles_servicios != null) {
                    if (!angular.isUndefined($scope.detalles_servicios[0].alcance)) {
                        angular.forEach($scope.detalles_servicios[0].alcance, function (value, key) {
                            if (angular.isDefined($scope.datos.municipio)) {
                                if ($scope.datos.municipio.id == value.municipio.id && $scope.datos.estado.id == value.estado.id) {
                                    flashMessages.show_info('El Alcance ya fue agregado');
                                    add = false;
                                } else if ($scope.datos.municipio.id == 'todos' && $scope.datos.estado.id == value.estado.id) {
                                    removeKeys.push(key);
                                } else if ($scope.datos.municipio.id != 'todos' && $scope.datos.estado.id == value.estado.id) {
                                    angular.forEach($scope.detalles_servicios[0].alcance, function (value_in, key) {
                                        if (value_in.estado.id == value.estado.id && value_in.municipio.id == 'todos') {
                                            removeKeys.push(key);
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            }
            for (var i = removeKeys.length - 1; i >= 0; i--) {
                $scope.detalles_servicios[0].alcance.splice(removeKeys[i], 1);
            }

            if (add) {
                var next = true;
                if (next) {

                    $scope.detalles_servicios[0].alcance.push({
                        estado: $scope.datos.estado,
                        municipio: $scope.datos.municipio
                    });
                }
            }
        };


        $scope.removerAlcance = function (index) {
            $scope.detalles_servicios[0].alcance.splice(index, 1);
        };
        $scope.agregarRed = function () {

            var add = true;
            if ($scope.datos.red == null || $scope.datos.url == null) {
                flashMessages.show_info('Debe escribir la url!');
                return false;

            } else if (add) {
                var next = true;
                if (next) {
                    $scope.detalles_servicios[0].red_social.push({
                        red: $scope.datos.red,
                        url: $scope.datos.url
                    });
                }
            }
        };
        $scope.removerRed = function (index) {
            $scope.detalles_servicios[0].red_social.splice(index, 1);
        };
        $scope.guardarDetallesServicio = function () {
            if ($scope.detalles_servicios.length > 0) {
                globalData.otros_detalles_servicios = $scope.detalles_servicios;

                $injector.get('$ngBootbox','guardarDetallesServicio').alert('Para Guardar los cambios en el servidor, debe presionar el boton guardar en la pantalla principal.');
                $modalInstance.close();
            } else {
                flashMessages.show_info("No ha cargado ningún detalle");
            }
        };

        /**
         * toggleEditable
         */
        $scope.toggleEditable = function (index) {
            $scope.editable[index] = !$scope.editable[index];
        };
    }]);

/**
 * Maneja el Tab de Servicios y formulario de servicios.
 *
 * @param  {object} $scope                 AngularJS Object
 * @param  {object} $injector              AngularJS Object
 * @param  {object} TipoPublicacionResolve Resolves del state asociado al controlador en route.js.
 * @param  {object} rViewMode              Resolves del state asociado al controlador en route.js.
 * @return {void}
 * @author Jose Rodriguez
 * @version V-1.0 19/06/15 11:19:16
 */
app.filter('propsFilter', function () {
    return function (items, props) {
        var out = [];

        if (angular.isArray(items)) {
            items.forEach(function (item) {
                var itemMatches = false;

                var keys = Object.keys(props);
                for (var i = 0; i < keys.length; i++) {
                    var prop = keys[i];
                    var text = props[prop].toLowerCase();
                    if (item[prop].toString().toLowerCase().indexOf(text) !== -1) {
                        itemMatches = true;
                        break;
                    }
                }

                if (itemMatches) {
                    out.push(item);
                }
            });
        } else {
            // Let the output be the input untouched
            out = items;
        }

        return out;
    }
});
app.controller('ServiciosModalController',
    ['$scope', "$injector", 'rTipoPublicacion', 'rMedio', '$modalInstance', 'r_circulacion', "r_target", 'r_redSocial',
        function ($scope, $injector, rTipoPublicacion, rMedio, $modalInstance, r_circulacion, r_target, r_redSocial) {
            //console.log(r_MedioServices);
            // Directiva de Validación.
            $scope.medio_servicios = [];
            var $validationProvider = $injector.get('$validation', 'ServiciosMCtrl');
            var flashMessages = $injector.get('flashMessages', 'ServiciosMCtrl');
            var estadoModel = $injector.get('estadoModel', 'ServiciosMCtrl');
            var globalData = $injector.get('globalData', 'ServiciosMCtrl');

            $scope.current = 0;
            $scope.service = {};
            $scope.service.hijos = [];

            $scope.circulacion = r_circulacion.data;
            $scope.target = r_target.data;
            $scope.redsocial = r_redSocial.data;
            $scope.estado = [];
            $scope.municipio = [];
            $scope.datos = {estado: null, municipio: null};
            $scope.service.otros_detalles = [
                {
                    'alcance': [],
                    'red_social': []
                }
            ];
            $scope.loadServices = function () {
                if ($scope.service.tipo_publicacion.nombre == 'Televisivo' || $scope.service.tipo_publicacion.nombre == 'Radial') {
                    var Medio = $injector.get('Medio', 'loadServices');
                    Medio.listChildren({
                        id: rMedio,
                        tipoPub: $scope.service.tipo_publicacion.id
                    }).$promise.then(function (resp) {
                        $scope.medio_servicios = resp;
                    });
                }
                $scope.getCadenas($scope.service.tipo_publicacion.nombre);
            };
            $scope.getCadenas = function (tipo) {
                if (tipo == 'Radial' || tipo == 'Televisivo') {
                    var Medio = $injector.get('Medio', 'loadServices');
                    Medio.listCadenas({
                        id: rMedio,
                        tipoPub: $scope.service.tipo_publicacion.id
                    }).$promise.then(function (resp) {
                        $scope.cadenas = resp;
                    });
                }
            };
            // Más información ver en el archivo router y la ruta condictions/add
            $scope.tipos_publicaciones = rTipoPublicacion;

            estadoModel.get().$promise.then(function (resultado) {
                $scope.estado = resultado.data;
                $scope.estado.unshift({'id': '', 'nombre': 'Seleccione el Estado', disabled: 'out'});
            });

            $scope.nivelAlcance = function () {
                if (!angular.isUndefined($scope.service.otros_detalles[0].alcance)) {
                    $scope.service.otros_detalles[0].alcance = [];
                }
                if ($scope.datos.nacional) {
                    $scope.datos.estado = {id: ''};
                    $scope.municipio = [];
                    $scope.service.otros_detalles[0].alcance.push({
                        estado: {id: 0, nombre: 'Todos'},
                        municipio: {id: 0, nombre: 'Todos'}
                    });
                }
            };
            $scope.cargarMunicipio = function () {
                $scope.municipio = [];
                if ($scope.datos.estado != null) {
                    estadoModel.get({id: $scope.datos.estado.id}).$promise.then(function (resultado) {
                        $scope.municipio = resultado.data.municipio;
                        $scope.municipio.unshift(
                            {
                                'id': 'todos',
                                'nombre': 'TODOS'
                            }
                        );
                    });
                }
            };

            $scope.agregarAlcance = function () {
                var add = true;
                if ($scope.datos.municipio == null || $scope.datos.estado == null) {
                    flashMessages.show_info('Debe seleccionar un municipio para poder agregar a la lista!');
                    return false;
                } else {
                    var removeKeys = [];
                    if ($scope.service.otros_detalles != null) {
                        if (!angular.isUndefined($scope.service.otros_detalles[0].alcance)) {
                            angular.forEach($scope.service.otros_detalles[0].alcance, function (value, key) {
                                if (angular.isDefined($scope.datos.municipio)) {
                                    if ($scope.datos.municipio.id == value.municipio.id && $scope.datos.estado.id == value.estado.id) {
                                        flashMessages.show_info('El Alcance ya fue agregado');
                                        add = false;
                                    } else if ($scope.datos.municipio.id == 'todos' && $scope.datos.estado.id == value.estado.id) {
                                        removeKeys.push(key);
                                    } else if ($scope.datos.municipio.id != 'todos' && $scope.datos.estado.id == value.estado.id) {
                                        angular.forEach($scope.service.otros_detalles[0].alcance, function (value_in, key) {
                                            if (value_in.estado.id == value.estado.id && value_in.municipio.id == 'todos') {
                                                removeKeys.push(key);
                                            }
                                        });
                                    }
                                }
                            });
                        }
                    }
                }
                for (var i = removeKeys.length - 1; i >= 0; i--) {
                    $scope.service.otros_detalles[0].alcance.splice(removeKeys[i], 1);
                }
                if (add) {
                    var next = true;
                    if (next) {
                        $scope.service.otros_detalles[0].alcance.push({
                            estado: $scope.datos.estado,
                            municipio: $scope.datos.municipio
                        });
                    }
                }
            };

            $scope.removerAlcance = function (index) {
                $scope.service.otros_detalles[0].alcance.splice(index, 1);
            };
            $scope.agregarRed = function () {

                var add = true;
                if ($scope.datos.red == null || $scope.datos.url == null) {
                    flashMessages.show_info('Debe escribir la url!');
                    return false;

                } else if (add) {
                    var next = true;
                    if (next) {
                        $scope.service.otros_detalles[0].red_social.push({
                            red: $scope.datos.red,
                            url: $scope.datos.url
                        });
                    }
                }
            };
            $scope.removerRed = function (index) {
                $scope.service.otros_detalles[0].red_social.splice(index, 1);
            };
            $scope.services_form = {
                self: this,
                checkValid: $validationProvider.checkValid,
                submit: function (form) {
                    $validationProvider.validate(form).success(function () {
                        $scope.services_form.send();
                    });//.error($scope.error);
                },

                reset: function (form) {
                    $validationProvider.reset(form);
                },

                /**
                 * Prepara la data de servicios a ser enviada al servidor.
                 * @param  {json} service
                 * @return {json}           data preparada.
                 * @author Jose Rodriguez
                 * @version V-1.0 16/06/15 17:25:32
                 */
                prepare: function (service) {
                    var data = angular.copy(service);
                    data.children = [];
                    data.tipo_publicacion = data.tipo_publicacion.id;

                    if (angular.isDefined($scope.service.cadena)) {
                        data.parent = $scope.service.cadena.id;
                    }

                    for (var i = 0; i < data.hijos.length; i++) {
                        data.children[i] = data.hijos[i].medio.id;
                    }
                    delete data.hijos;
                    return data;
                },

                /**
                 * Enviar las servicios al servidor y recibe las respuestas pertinentes.
                 *
                 * @author Jose Rodriguez
                 * @version V-1.0 15/06/15 17:24:14
                 * @return {void}
                 */
                send: function () {
                    var serviceModel = $injector.get('Servicio', 'send');
                    var params = {medioId: rMedio, save: 1};

                    serviceModel.save(
                        params,
                        $scope.services_form.prepare($scope.service),
                        $scope.services_form.success,
                        $scope.services_form.errors
                    );
                },

                /**
                 * Obtiene la respuesta del servidor en caso de exito.
                 *
                 * @param  {JSON} response Respuesta del servidor
                 * @return {void}
                 * @author Jose Rodriguez
                 * @version V-1.1 15/06/15 14:37:36
                 */
                success: function (response) {
                    // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                    // if(response.status == 'ok'){
                    $modalInstance.close();
                    globalData.ServicioId = response.data.id;
                    var flashMessages = $injector.get('flashMessages', 'success');
                    flashMessages.show_success("Los datos del Medio fueron almacenados.");
                    // }
                },

                /**
                 * Obtiene y muestra los errores de validación generados por laravel,
                 * en una interfaz flashMessages Error.
                 *
                 * @param  {JSON} responseError Respuesta del servidor
                 * @return {void}
                 * @author Jose Rodriguez
                 * @version V-1.1 15/06/15 14:37:36
                 */
                errors: function (responseError) {
                    var errors = responseError.data.errors;
                    var mensajeToShow = [];
                    for (var i = errors.length - 1; i >= 0; i--) {
                        var message = errors[i].message;
                        if (angular.isString(message)) {
                            mensajeToShow.push(message);
                            continue;
                        }

                        if (angular.isDefined(message.nombre)) {
                            mensajeToShow = mensajeToShow.concat(message.nombre);
                        }
                        if (angular.isDefined(message.tipo_publicacion_id)) {
                            mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                        }
                    }

                    var flashMessages = $injector.get('flashMessages', 'errors');
                    flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
                }
            };
            /**
             * Métodos para la paginación del Wizard de creación de Medios
             * @returns {boolean}
             */
            $scope.showButtonWizard = function () {
                $scope.disabledNext = false;
                if ($scope.current >= 1) {
                    if (($scope.seccion - 1) == $scope.current) {
                        //$scope.disabledNext = true;
                    }
                    return true;
                }
            };

            /**
             *
             */
            $scope.wizardNext = function () {
                switch ($scope.current) {
                    case 0:
                        $scope.current++;
                        break;
                    case 1:
                        $scope.current++;
                        break;
                    case 2:
                        $scope.current++;
                        break;
                }
            };

            /**
             * Variables Usadas en la creación de detalles del servicio
             * @type {Array}
             */
            $scope.editable = [];


            /**
             * toggleEditable Cambia los detalles al modo de edición
             */
            $scope.toggleEditable = function (index) {
                $scope.editable[index] = !$scope.editable[index];
            };
            $scope.agregarHijo = function () {
                $scope.service.hijos.push({medio: {}});
            };
            $scope.removeHijo = function (index) {
                $scope.service.hijos.splice(index, 1)
            };
        }]);

/**
 * Controlador de Tarifas.
 * @author Jose Rodriguez
 */
app.controller('TarifarioController', ['$scope', '$injector', 'rServicio', 'rViewMode', 'rTabId', function ($scope, $injector, rServicio, rViewMode, rTabId) {
    $scope.viewMode = rViewMode;
    $scope.service = rServicio.data;

    var tab = {
        id: rTabId,
        title: 'Tarifario Medio #',
        state: 'admin.detalle_medio.tarifario',
        object_id: rServicio.data.id
    };
    $scope.buildTab(tab);

    /**
     * Manda a imprimir el tarifario del servicio indicado.
     * @param servicio_id
     */
    $scope.print = function (servicio_id) {
        var config = $injector.get('appConfig', 'TarifarioController.print');
        config.printPDF('Tarifario', servicio_id);
    }
}]);


//VPSLOOP vpspass:r9C8v$5wxbjq
//godaddy sitepass:hG3Jv7!Ma5v#