'use strict';

/**
 * Created by Carlos BLanco.
 */
app.factory('recepcionModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'configuracion/recepcion/:id', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'configuracion/recepcion/:id',
                params: {
                    id: '@id'
                },
                isArray: false
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'configuracion/recepcion',
                isArray: false
            },
            delete: {
                method: 'DELETE'
            }
        });
    }
]);