'use strict';

/**
 * Modelo que contiene los datos a subir al servidor
 * @author Carlos Blanco <cebs923@gmail.com>
 * @packege SIGAVP
 * @version V-1.0 22/06/2015 12:03:30
 */

app.factory('recargogestionModel', ["$resource" , "appConfig" , function($resource, appConfig){
    var url= appConfig.getPathApi() + 'cotizaciones/recargo_gestion/:id';
    var _object = $resource( url, null, {
        update: {method:'PUT'},
        delete: {method:'DELETE'},
    });

    return _object;

}]);
