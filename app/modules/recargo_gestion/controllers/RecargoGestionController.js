'use strict';

/**
 * Controlador que gestiona el listado del Recargo por Gestion
 * @author Carlos Blanco <cebs923@gmail.com>
 * @packege SIGAVP
 * @version V-1.0 22/06/2015 11:54:30
 */

app.controller('RecargoGestionListController', ['$scope', 'r_permisos', '$injector',
    function($scope, r_permisos, $injector){
    var flashMessages = $injector.get('flashMessages', 'RecargoGestionListController');
    var $ngBootbox = $injector.get('$ngBootbox','RecargoGestionListController');
    var crudFactory = $injector.get('crudFactory', 'RecargoGestionListController');


    $scope.title= 'Historico de Recargo por Gestion';
    $scope.siteCrud= {url:'cotizaciones/recargo_gestion_dt'};

    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {title: 'Valor', label: 'porcentaje', filter: 'porcentaje'},
        {title: 'Fecha de actualizacion', label: 'fecha'},
        {title: 'Activo', label: 'activo', filter: 'si_no'}
    ];

    $scope.operation = r_permisos.get.data.permission;

    /**
     *
     *
     * @author Amalio Velasquez
     * */
    $scope.borrarRecargoGestion = function(id){

        $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
            function(){
                var recargo = $injector.get('recargogestionModel','confirm');

                recargo.remove(
                    {
                        id: id
                    },
                    function(success){
                        if(success.status == 'ok'){
                            crudFactory.reloadData();
                            flashMessages.show_success(success.message);
                        }
                    },
                    function(error){
                        flashMessages.show_error(error.data.message);
                    }
                );
            }
            //, function(){}
        );
    }

}]);


/**
 * Controlador que Crea el porcentaje
 * @author Carlos Blanco <cebs923@gmail.com>
 * @packege SIGAVP
 * @version V-1.0 19/06/2015 10:32:30
 */

app.controller('RecargoGestionFormController',['$scope', 'recargogestionModel', '$injector','flashMessages','$state', function( $scope, recargogestionModel, $injector, flashMessages, $state){

    $scope.recargo_gestion = {};

    var $validationProvider = $injector.get('$validation');

    // Donde "type_form" es igual al atributo id de la etiqueta form
    // ejm: <form name="TypeForm" id="type_form">
    $scope.recargo_form = {
        checkValid: $validationProvider.checkValid,
        submit: function (form) {
            $validationProvider.validate(form).success(function () {
                 recargogestionModel.save(
                    null,
                    $scope.recargo_gestion,
                    function(correcto){

                        if(correcto.status =='ok'){
                            flashMessages.show_success("Se actualizó el porcentaje correctamente");
                            $state.go('admin.recargo_gestion');
                            return;
                        }

                        flashMessages.show_error('Ocurrio un problema intente nuevamente');


                    }, function(error){
                        var errors = error.data.errors;
                        var mensajeToShow = [];
                        for (var i = errors.length - 1; i >= 0; i--) {

                            var message = errors[i].message;
                            if (angular.isString(message)) {
                                mensajeToShow.push(message);
                                continue;
                            };

                            if(angular.isDefined(message.porcentaje)){
                                mensajeToShow = mensajeToShow.concat(message.porcentaje);
                            }
                        };

                        flashMessages.show_error('<p>'+mensajeToShow.join('</p><p>')+'</p>');
                    });
            });
        }
    }


}]);
