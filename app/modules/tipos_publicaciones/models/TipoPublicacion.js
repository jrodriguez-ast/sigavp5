'use strict';

/**
 * Created by darwin on 01/06/15.
 */
app.factory('TipoPublicacion', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'tipos_publicaciones/tipo/:id', null, {
            update: {
                method: 'PUT'
            },
            listado_for_medio: {
                method: 'GET',
                url: appConfig.getPathApi() + 'tipos_publicaciones/listado_for_medio/:medioId',
                params: {
                    medioId: '@medioId'
                },
                isArray: true
            },
            delete: {
                method: 'DELETE'
            }
        });
    }
]);