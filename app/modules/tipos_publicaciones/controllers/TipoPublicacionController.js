'use strict';
/**
 *Controlador que gestiona el listado de Tipos de Publicaciuon
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 01/06/15 19:18:08
 */
app.controller("TipoPublicacionListController",["$scope","permission", "$injector",
    function($scope,permission, $injector){

    var flashMessages = $injector.get('flashMessages', 'listClient');
    var $ngBootbox = $injector.get('$ngBootbox','listClient');
    var crudFactory = $injector.get('crudFactory', 'listClient');

    $scope.title = "Tipos de Publicaciones";
    $scope.siteCrud={url:'tipos_publicaciones/dt'};
    $scope.headerCrud= [
        {title:'ID',label:'id',visible:false},
        {title:'Nombre',label:'nombre'},
        {title:'Descripción',label:'descripcion'},
    ];

    $scope.operation =permission.data.get.data.permission;

    /**
     *
     * 
     * @author Amalio Velasquez
     * */
    $scope.borrarTipoPublicacion = function(tip_pub_id){

        $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
            function(){
                var tipoPub = $injector.get('TipoPublicacion','confirm');

                tipoPub.remove(
                    {
                        id: tip_pub_id
                    },
                    function(success){
                        if(success.status == 'ok'){
                            crudFactory.reloadData();
                            flashMessages.show_success(success.message);
                        } else if(success.status == 'fail'){
                            crudFactory.reloadData();
                            flashMessages.show_error(success.message);
                        }
                    },
                    function(error){
                        flashMessages.show_error(error.data.message);
                    }
                );
            }
            //, function(){}
        );
    }
}]);

/**
 * Controlador que gestiona la creacion de Condiciones
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 28/05/15 11:26:49
 */
app.controller('TipoPublicacionController',["$scope","$injector",'TipoPublicacionResolve',function($scope,$injector,TipoPublicacionResolve){

    // Inyectamos las dependencias a utilizar dentro del Controlador.
    var $validationProvider = $injector.get('$validation');
    var TipoPublicacion = $injector.get('TipoPublicacion');
    var flashMessages = $injector.get('flashMessages');

    // Objeto TipoPublicacion
    $scope.TipoPublicacion = {nombre:'', descripcion:''};

    // Para el metodo Update
    if(angular.isDefined(TipoPublicacionResolve.nombre)){
        $scope.TipoPublicacion.nombre = TipoPublicacionResolve.nombre;
        $scope.TipoPublicacion.descripcion = TipoPublicacionResolve.descripcion;
    }

    // Determinamos via URL estamos en modo visualización.
    var activeView = ['view'];
    var urlPath = document.URL.split('/');
    $scope.viewMode = false;
    for (var i = activeView.length - 1; i >= 0; i--) {
        if(urlPath.indexOf(activeView[i]) > -1 ){
            $scope.viewMode = true;
            break;
        }
    };


    // Donde "type_form" es igual al atributo id de la etiqueta form
    // ejm: <form name="TypeForm" id="type_form">
    $scope.tipo_publicacion_form = {
        checkValid: $validationProvider.checkValid,
        submit: function (form) {
            $validationProvider.validate(form)
                .success($scope.success);//.error($scope.error);
        },
        reset: function (form) {
            $validationProvider.reset(form);
        }
    };

    //fin de la validacion del form

    /**
     * Valida y envia la data al servidor para su guardado.
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.0 22/01/15 10:31:05
     */
    $scope.success = function() {
        var $stateParams = $injector.get('$stateParams');

        if(angular.isDefined($stateParams.id)){
            TipoPublicacion.update(
                {id:$stateParams.id},
                prepare($scope.TipoPublicacion),
                function(success) {
                    if(success.status == 'ok'){
                        flashMessages.show_success("Condición de Pauta Actulizada con Exito.");
                        goToGrid();
                    }
                }, function(error) { // Procesnado los errores.
                    var errors = error.data.errors;
                    var mensajeToShow = [];
                    for (var i = errors.length - 1; i >= 0; i--) {

                        var message = errors[i].message;
                        if (angular.isString(message)) {
                            mensajeToShow.push(message);
                            continue;
                        };

                        if(angular.isDefined(message.nombre)){
                            mensajeToShow = mensajeToShow.concat(message.nombre);
                        }
                        if(angular.isDefined(message.tipo_publicacion_id)){
                            mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                        }
                    };
                    flashMessages.show_error('<p>'+mensajeToShow.join('</p><p>')+'</p>');
                });
            return ;
        }

        TipoPublicacion.save(
            prepare($scope.TipoPublicacion),
            function(success) { // Procesando el Exito.
                if(success.status == 'ok'){
                    flashMessages.show_success("Condición de Pauta Creada con Exito.");
                    goToGrid();
                }

            },function(error) { // Procesnado los errores.
                var errors = error.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    };

                    if(angular.isDefined(message.nombre)){
                        mensajeToShow = mensajeToShow.concat(message.nombre);
                    }
                    if(angular.isDefined(message.tipo_publicacion_id)){
                        mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                    }
                };
                flashMessages.show_error('<p>'+mensajeToShow.join('</p><p>')+'</p>');
            }
        );
    };

    /**
     * Prepara la data de los Tipos productos para ser enviada al controlador
     * @param  {object}
     * @return {object}
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.0  22/01/15 10:33:29
     */
    function prepare(_object) {
        var _prepare = angular.copy(_object);
        return _prepare;
    };

    function goToGrid() {
    	// Dirección del Grid
    	var gridUrl = 'admin/TiposPublicaciones';
    	var $location = $injector.get('$location');
    	$location.path(gridUrl);
    }

}]);
/* End of file conditionsController.js */