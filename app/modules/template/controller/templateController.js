'use strict';
/**
 * Plantilla para un controlador de Lista
 ***** Cambia las palabras que salen en mayúsculas. *****
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 01/06/15 19:18:08
 */
app.controller("TEMPLATE-ListController",["$scope",'r_permissions',function($scope,r_permissions){
    $scope.siteCrud = {url: 'TU-URL-DE-DATATABLE'};
    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {title: 'Nombre', label: 'nombre'}
    ];

    $scope.operation = r_permissions.permission;
}]);

/**
 * Controlador que gestiona tu modelo.
 * VER ROUTE ANEXO A LA PLANTILLA PARA QUE VEAS COMO DEFINIR TU STATE.
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.1  15:43:32
 */
app.controller('TEMPLATE-FormController',
    ['$scope','$injector','rViewMode','rAction','rModel',
        function($scope,$injector,rViewMode,rAction,rModel){

    // El prefijo "r", para indicar que es un resolve.
    //
    // rViewMode Indica si el controlador esta en modo readonly
    // rAction: Indica si el controlador es update o new
    // rModel: Data existente en BD de tu modelo.

    // Inyectamos las dependencias a utilizar dentro del Controlador.
    var $validationProvider = $injector.get('$validation','-FormController');

    // Definimos el model, como el a utilizar en la vista
    $scope.model = rModel;

    // Recomiendo que dentro del objeto form, coloquemos todos los selects de la vista.
    $scope.form.SELECTOPTIONS = 'TU-RESOLVE-DATA';

    // Donde "formObject" es igual al atributo id de la etiqueta form
    // ejm: <form name="formName" id="formObject">
    $scope.formObject = {
        checkValid: $validationProvider.checkValid,
        submit:function (form){
            $validationProvider.validate(form).success(function() {
                $scope.formObject.send();
            });
        },

        reset: function (form) {
            $validationProvider.reset(form);
        },

        /**
         * Prepara la data de Negociación a ser enviada al servidor.
         * @param  {json} object
         * @return {json}           data preparada.
         * @author Jose Rodriguez
         * @version V-1.0 16/06/15 17:25:32
         */
        prepare:function(object) {
            object = angular.copy(object);
            return object;
        },

        /**
         * Enviar las Negociación al servidor y recibe las respuestas pertinentes.
         *
         * @author Jose Rodriguez
         * @version V-1.0 15/06/15 17:24:14
         * @return {void}
         */
        send:function() {
            var model = $injector.get('TU-MODELO','$scope.formObject.send');

            // Aquí define los parámetros que necesita su modelo.
            var params = {};

            if (rAction == 'new') {
                model.save(
                    params,
                    $scope.formObject.prepare($scope.model), // Objeto de modelo.
                    $scope.formObject.success,
                    $scope.formObject.errors
                );
            } else if (rAction == 'update') {
                model.update(
                    params,
                    $scope.formObject.prepare($scope.model), // Objeto de modelo.
                    $scope.formObject.success,
                    $scope.formObject.errors
                );
            }
        },

        /**
         * Obtiene la respuesta del servidor en caso de exito.
         *
         * @param  {JSON} response Respuesta del servidor
         * @return {void}
         * @author Jose Rodriguez
         * @version V-1.1 15/06/15 14:37:36
         */
        success:function(response){
            // No evaluamos OK xq el servidor responde 200 siempre y este ok.
            if(response.status == 'ok'){
                var msg = 'TU-MENSJAE-DE-EXITO';
                $injector.get('flashMessages','success').show_success(msg);
            }
        },

        /**
         * Obtiene y muestra los errores de validación generados por laravel,
         * en una interfaz flashMessages Error.
         *
         * @param  {JSON} response Respuesta del servidor
         * @return {void}
         * @author Jose Rodriguez
         * @version V-1.1 15/06/15 14:37:36
         */
        errors:function (response){
            var errors = response.data.errors;
            var mensajeToShow = [];

            // Indica los nombres de los campos que van a venir en la respuesta de validación
            var fields = ['FIELD-1','FIELD-2'];

            for (var i = errors.length - 1; i >= 0; i--) {

                var message = errors[i].message;
                if (angular.isString(message)) {
                    mensajeToShow.push(message);
                    continue;
                }

                for (var j = fields.length - 1; j >= 0; j--) {
                    if(angular.isDefined(message[fields[j]])){
                        mensajeToShow = mensajeToShow.concat(message[fields[j]]);
                        fields.splice(j);
                        // @todo Investigar xq break rompe ambos ciclos.
                        //break;
                    }
                }
            }

            $injector.get('flashMessages','errors').show_error('<p>'+mensajeToShow.join('</p><p>')+'</p>');
        }
    };
}]);

/* End of file templateController.js */