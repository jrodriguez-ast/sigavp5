/**
 * Created by jrodriguez on 07/07/15.
 */
state('TU-STATE',{
    url:'TU-URL',
    templateUrl: "EL-PATH-DONDE-COPIASTE-LA-PLANTILLA",
    controller: "EL-CONTROLADOR-Q-DOMINA-LA-VISTA",
    resolve:{
        rViewMode:function() {return false},
        rAction:function() { return 'new'},
        // Definimos este, para cuando la vista es Crear
        // Si la vista es Ver o Editar, resuelve los datos.
        rModel:function() { return {} }
    }
});