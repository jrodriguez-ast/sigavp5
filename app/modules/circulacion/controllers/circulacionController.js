'use strict';

/**
 * @author Carlos blanco <cebs923@gmail.com>
 * @package SIGAVP
 * @version V-1.0 07/09/2015
 */
app.controller('CirculacionCtrlC', ['$scope', 'particularPermission', '$injector',
    function ($scope, particularPermission, $injector) {
        var flashMessages = $injector.get('flashMessages', 'CirculacionCtrlC');
        var $ngBootbox = $injector.get('$ngBootbox','CirculacionCtrlC');
        var crudFactory = $injector.get('crudFactory', 'CirculacionCtrlC');

    $scope.title = 'Circulación';
    $scope.siteCrud = {url: 'configuracion/circulacion_dt'};
    $scope.headerCrud = [
        {title: 'Nombre', label: 'nombre', visible: true},
        {title: 'ID', label: 'id', visible: false}
    ];
    $scope.operation = particularPermission.permission;

    /**
     *
     *
     * @author Amalio Velasquez
     * */
    $scope.borrarCirculacion = function(id){

        $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
            function(){
                var model = $injector.get('circulacionModel','confirm');

                model.remove(
                    {
                        id: id
                    },
                    function(success){
                        if(success.status == 'ok'){
                            crudFactory.reloadData();
                            flashMessages.show_success(success.message);
                        }
                    },
                function(error){
                    crudFactory.reloadData();
                    flashMessages.show_error(error.data.message);
                    }
                );
            }
            //, function(){}
        );
    }
}]);

/**
 *
 */
app.controller("CrearCirculacionController", ["$scope", '$injector', function ($scope, $injector) {
    var $validationProvider = $injector.get('$validation', 'AddCirculaciónCtrl');
    var flashMessages = $injector.get('flashMessages', 'AddCirculaciónCtrl');

    /*
     Objeto que se enviara al Backend
     */
    $scope.contact = {nombre: ''};
    $scope.detalles = {nombre: ''};

    // Donde "type_form" es igual al atributo id de la etiqueta form
    // ejm: <form name="TypeForm" id="type_form">
    $scope.mediocontacto_form = {
        checkValid: $validationProvider.checkValid,
        submit: function (form) {
            $validationProvider.validate(form).success(function () {
                $scope.mediocontacto_form.send();
            });//.error($scope.error);
        },

        reset: function (form) {
            $validationProvider.reset(form);
        },
        /**
         * Enviar las condiciones al servidor y recibe las respuestas pertinentes.
         *
         * @author Jose Rodriguez
         * @version V-1.0 15/06/15 17:24:14
         * @return {void}
         */
        send: function () {
            $scope.contact.nombre = $scope.detalles.nombre;
            $injector.get('circulacionModel', 'send').crear(
                $scope.contact,
                $scope.mediocontacto_form.success,
                $scope.mediocontacto_form.errors
            );
        },
        /**
         * Obtiene la respuesta del servidor en caso de exito.
         *
         * @param  {JSON} response Respuesta del servidor
         * @return {void}
         * @author Jose Rodriguez
         * @version V-1.1 15/06/15 14:37:36
         */
        success: function (response) {
            // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
            // if(response.status == 'ok'){
            flashMessages.show_success("Se ha creado la circulación.");
            $injector.get('$state', 'success').go('admin.circulacion');
            // }
        },

        /**
         * Obtiene y muestra los errores de validación generados por laravel,
         * en una interfaz flashMessages Error.
         *
         * @param  {JSON} responseError Respuesta del servidor
         * @return {void}
         * @author Jose Rodriguez
         * @version V-1.1 15/06/15 14:37:36
         */
        errors: function (responseError) {
            var errors = responseError.data.errors;
            var mensajeToShow = [];
            for (var i = errors.length - 1; i >= 0; i--) {

                var message = errors[i].message;
                if (angular.isString(message)) {
                    mensajeToShow.push(message);
                }

                //if (angular.isDefined(message.nombre)) {
                //    mensajeToShow = mensajeToShow.concat(message.nombre);
                //}
                //if (angular.isDefined(message.apellido)) {
                //    mensajeToShow = mensajeToShow.concat(message.apellido);
                //}
            }

            flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
        }
    }
}]);

/**
 *
 */
app.controller('EditarCirculacionController', ["$scope", "$injector", "r_data", "circulacionID",
    function ($scope, $injector, r_data, circulacionID) {
        var $validationProvider = $injector.get('$validation', 'EditCirculaciónCtrl');
        var flashMessages = $injector.get('flashMessages', 'EditCirculaciónCtrl');

        $scope.detalles = r_data.data;

        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },

            prepare: function (object) {
                object = angular.copy(object);
                return object;
            },

            send: function () {
                var circulacionModel = $injector.get('circulacionModel', 'send');

                var params = {id: circulacionID};
                circulacionModel.update(
                    params,
                    $scope.mediocontacto_form.prepare($scope.detalles),
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors
                );
            },
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("El nombre de la circulación fue actualizado");
                $injector.get('$location', 'success').path("admin/circulacion");
                // }
            },

            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {
                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.description)) {
                        mensajeToShow = mensajeToShow.concat(message.description);
                    }
                    //if (angular.isDefined(message.tipo_publicacion_id)) {
                    //    mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                    //}
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);
