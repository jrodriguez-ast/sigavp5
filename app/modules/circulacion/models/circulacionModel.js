'use strict';

/**
 * Created by Carlos BLanco.
 */
app.factory('circulacionModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'configuracion/circulacion/:id', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'configuracion/circulacion/:id',
                params: {
                    id: '@id'
                },
                isArray: false
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'configuracion/circulacion',
                isArray: false
            },
            delete: {
                method: 'DELETE'
            }
        });
    }
]);