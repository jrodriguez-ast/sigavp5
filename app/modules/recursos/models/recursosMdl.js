'use strict';

/**
 * Created by darwin on 19/02/15.
 */
/**
 *Modelo encargado de la manipulacion de la data de cliente
 *
 * @author Jefferson Lara <jetox21@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 *
 */
app.factory("recursosMdl", [
    '$http',
    'appConfig',
    '$q',
    'flashMessages',
    "genericApiServices",
    function ($http, appConfig, $q, flashMessages, genericApiServices) {

        return ({
            data: [],
            get: function (id) {
                var defer = $q.defer(), data = this.data;

                var _url2 = 'recursos/recurso/get', params = {id: id};
                genericApiServices.runGetServices(_url2, params).success(function (response) {
                    data.recurso = response;
                    defer.resolve(data);
                });
                return defer.promise;
            },
            /**
             * Retorna todos los datos relacionados al cargo
             * @return {*}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             * @version V1.0 27/01/15
             */
            getAll: function () {
                var url = appConfig.getPathApi() + 'recursos/recurso/todos';
                return $http.get(url).then(
                    function (response) {
                        return response.data;
                    }
                )
            },
            /**
             * Obtiene una referencia de los productos existentes en el sistema
             * @param text
             * @returns {*}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             * @version V1.0 08/04/15
             */
            getReference: function (text) {
                var _thisUrl = appConfig.getPathApi() + "recursos/recurso/reference";
                return $http.get(_thisUrl, {
                    params: {
                        text: text
                    }
                }).then(function (response) {
                    return response.data.map(function (item) {
                        return item;
                    });
                });
            }
        });
    }
]);