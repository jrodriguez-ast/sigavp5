'use strict';

/**
 * Created by darwin on 19/02/15.
 */
app.controller('recursosCtrl', [
    '$scope',
    'permission',
    function ($scope, permission) {
        $scope.title = 'Recursos';

        $scope.siteCrud = {url: 'recursos/recurso/listar'};

        $scope.headerCrud = [
            {title: 'ID', label: 'id', visible: false},
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'Descripción', label: 'descripcion', visible: true}
        ];

        $scope.operation = permission.data.get.data.permission;


    }
]);


/**
 * Controlador que gestiona la creacion de Productos
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 */
app.controller('recursoFormCtrl', [
    "$scope",
    "$location",
    "$http",
    "$stateParams",
    "genericApiServices",
    "flashMessages",
    "$injector",
    "recursosMdl",
    '$window',
    function ($scope, $location, $http, $stateParams, genericApiServices, flashMessages, $injector, recursosMdl, $window) {
        $scope.recurso = {};
        $scope.recurso.informacion_adicional = [];

        if (angular.isDefined($stateParams.id)) {
            $scope.recurso = recursosMdl.data.recurso;
        }


        //inicializamos la validacion del form
        var $validationProvider = $injector.get('$validation');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.recurso_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form)
                    .success($scope.success);
                //.error($scope.error);
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },
            back: function () {
                $window.history.back();
            }
        };

        //fin de la validacion del form

        /**
         * Valida y envia la data al servidor para su guardado.
         * @author Jose Rodriguez <josearodrigueze@gmail.com>
         * @version V-1.0 22/01/15 10:31:05
         */
        $scope.success = function () {
            var method = (angular.isDefined($stateParams.id)) ? 'edit' : 'add';
            var _url = 'recursos/recurso/' + method, object = {};
            // Donde Object es el Objecto del formulario, ne este caso TypeProduct.
            object = prepare($scope.recurso);
            genericApiServices.runPostServices(_url, object).success(function (data) {
                if (data.error) { //Fracaso
                    flashMessages.show_error(data.msg);
                } else { //Exito
                    if (method == 'edit') {
                        flashMessages.show_success("Recurso editado con éxito");
                    }
                    else {
                        flashMessages.show_success("Recurso creado con éxito");
                    }
                    $location.path("admin/recurso");
                }
            });
        };

        /**
         * Prepara la data de los Tipos productos para ser enviada al controlador
         * @param  {object}
         * @return {object}
         * @author Jose Rodriguez <josearodrigueze@gmail.com>
         * @version V-1.0  22/01/15 10:33:29
         */
        function prepare(_position) {
            return angular.copy(_position);
        }


        $scope.informacion_adicional = {};
        $scope.itemEdit = null;
        $scope.showInformacion = false;

        /**
         * Agrega o actualiza un ítem de información adicional para un recurso
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0
         */
        $scope.addInfo = function () {

            if ($scope.informacion_adicional.titulo == '' || $scope.informacion_adicional.descripcion == '') {
                flashMessages.show_error("Llene el título y la descripción para agregar información al recurso");
                return;
            }

            if ($scope.itemEdit != null) {
                $scope.recurso.informacion_adicional[$scope.itemEdit] = angular.copy($scope.informacion_adicional);
            }
            else {
                $scope.recurso.informacion_adicional.push($scope.informacion_adicional);
            }

            $scope.informacion_adicional = {};
            $scope.itemEdit = null;
            $scope.showInformacion = false;

        };


        /**
         * Selecciona un ítem de la información adicional del recurso y la coloca en el formulario para su edición
         * @param indInfo
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0
         */
        $scope.editarInfo = function (indInfo) {
            $scope.itemEdit = indInfo;
            $scope.informacion_adicional = angular.copy($scope.recurso.informacion_adicional[indInfo]);
            $scope.showInformacion = true;
        };

        /**
         * Elimina un ítem de la información adicional que tenga el recurso
         * @param indInfo
         */
        $scope.eliminarInfo = function (indInfo) {
            $scope.recurso.informacion_adicional.splice(indInfo, 1);
            $scope.$apply();
        };


        /**
         * Mueve los tabs para reordenar la información adicional del recurso
         * @param position
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 09/03/15
         */
        $scope.tabMoveLeft = function (position) {
            $scope.recurso.informacion_adicional = moveTab($scope.recurso.informacion_adicional, position, position - 1);
        };

        /**
         * Mueve los tabs una posición hacia arriba para ordenar la información del recurso
         * @param position
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 09/03/15
         */
        $scope.tabMoveRight = function (position) {
            $scope.recurso.informacion_adicional = moveTab($scope.recurso.informacion_adicional, position, position + 1);
        };


        /**
         * Ubica un ítem de un array en otra posición
         * @param obj Array a organizar
         * @param position Integer posición actual del elemento a mover
         * @param newPosition Integer nueva posición del elemento
         * @returns {Array} Array con la nueva organización
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 09/03/15
         */
        var moveTab = function (obj, position, newPosition) {
            var originalOrder = obj;
            var objMove = originalOrder[position];
            var newOrder = [];
            angular.forEach(originalOrder, function (obj, i) {
                if (newPosition == i && position < newPosition) {
                    newOrder.push(obj);
                    newOrder.push(objMove);
                }
                else if (newPosition == i && position > newPosition) {
                    newOrder.push(objMove);
                    newOrder.push(obj);
                }
                else if (position != i) {
                    newOrder.push(obj);
                }
            });

            return newOrder;
        }
    }]);


