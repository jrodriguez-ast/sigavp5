'use strict';

/**
 * Archivo que contiene los controladores que intervienen en la creación, edcición y lista de usuarios
 *
 * @author Jefferson Lara <jetox21@gmailcom>
 * @version V-1.0 07/02/15.
 * @packages Cotizaciones A.S.T
 *
 */

//Controlador del listado de usuarios
app.controller("crudListUser", [
    "$scope",
    "permission",
    "data_source_admin",
    "$state",
    'DTOptionsBuilder',
    'DTColumnBuilder',
    'appConfig',
    function ($scope, permission, data_source_admin, DTOptionsBuilder, DTColumnBuilder, $state, appConfig) {
        $scope.siteCrud = {
            url: 'administracion/user_controller/list_user',
            pathApi: appConfig.getAdminPathApi()
        };
        $scope.headerCrud = [
            {title: 'ID', label: 'id', visible: false},
            {title: 'Cedula', label: 'cedula', visible: true},
            {title: 'Nombre', label: 'first_name', visible: true},
            {title: 'Apellido', label: 'last_name', visible: true},
            {title: 'Telefono', label: 'phone', visible: true},
            {title: 'Direccion', label: 'direccion', visible: true},
            {title: 'Email', label: 'email', visible: true},
            {title: 'Estatus', label: 'active', visible: true}
        ];
        $scope.reload = function () {
            /*         DTInstances.getLast().then(function(dtInstance) {
             dtInstance.reloadData();
             });*/
        };
        $scope.operation = permission.data.get.data.permission;

        $scope.io_user = function (id) {
            /*         $scope.data_user=data_source_admin._get_data_user(id);
             console.log($scope.data_user)*/

            bootbox.confirm("Se procederá a Activar/Desactivar el usuario! Desea continuar?", function (result) {
                if (result) {
                    $scope.data_user = data_source_admin._io_user(id);
                }

            });
        }

    }
]);

//controlador para la gestión de usuarios
app.controller("controlUser", [ "$scope", "$injector", 'r_clientes',
    function ($scope, $injector, r_clientes) {
        var data_source_admin = $injector.get('data_source_admin', 'controlUser');
        var $stateParams = $injector.get('$stateParams', 'controlUser');
        var $window = $injector.get('$window', 'controlUser');

        //Declaraciones de variables principales en el scope
        var $validationProvider = $injector.get('$validation', 'controlUser');

        $scope.data_user = {};
        $scope.groups = data_source_admin.data.get.data.groups;
        $scope.group = data_source_admin.data.get.data.group_selected;
        $scope.mode = data_source_admin.data.mode;
        $scope.clientes = r_clientes;
        $scope.data_user.cliente_id = '';
        if(!angular.isUndefined(data_source_admin.data.get.data.info_user)){
            $scope.cliente_id = {id: data_source_admin.data.get.data.info_user.cliente_id * 1};
        } else{
            $scope.cliente_id = {};
        }

        if ($scope.mode == 'create') {
            $scope.msg_top = 'Creación';
            $scope.msg_button = 'Guardar';
        } else {
            $scope.msg_top = 'Edición';
            $scope.msg_button = 'Actualizar';
        }
        if (angular.isDefined($stateParams.id)) {
            $scope.data_user = data_source_admin.data.get.data.info_user;
        }

        //Objeto que contiene las variables y métodos para la gestión de usuarios

        var $event_user = {
            _create_user: function () {
                $scope.data_user.group = $scope.group.id;
                $scope.data_user.cliente_id = $scope.cliente_id.id;
                data_source_admin._create_user($scope.data_user);
            },
            _edit_user: function () {
                $scope.data_user.group = $scope.group.id;
                $scope.data_user.cliente_id = $scope.cliente_id.id;
                data_source_admin._edit_user($scope.data_user);
            }
        };

        $scope.process_create_user = function () {
            $validationProvider.checkValid;
            if (!angular.isDefined($stateParams.id)) {
                $validationProvider.validate($scope.frmuser).success($event_user._create_user);
            } else {
                $validationProvider.validate($scope.frmuser).success($event_user._edit_user);
            }
        };

        $scope.back_list_user = function () {
            $window.history.back();
        }
    }]);

//Controlador del listado de Grupos de Usuarios
app.controller("crudListGroup", [ "$scope", "permission", "appConfig",
    function ($scope, permission, appConfig) {

        $scope.siteCrud = {
            url: 'administracion/group_controller/list_group',
            pathApi: appConfig.getAdminPathApi()
        };

        $scope.headerCrud = [
            {title: 'ID', label: 'id', visible: false},
            {title: 'Nombre', label: 'name', visible: true},
            {title: 'Descripción', label: 'description', visible: true},
            {title: 'Estatus', label: '_status', visible: true}
        ];

        $scope.operation = permission.data.get.data.permission;

    }
]);

//controlador para la gestión de grupos
app.controller("controlGroup", ["$scope", "$injector", function ($scope, $injector) {
    var data_source_admin = $injector.get('data_source_admin', 'controlGroup');
    var $stateParams = $injector.get('$stateParams', 'controlGroup');
    var $window = $injector.get('$window', 'controlGroup');


    //Declaraciones de variables principales en el scope
    var $validationProvider = $injector.get('$validation', 'controlGroup');
    $scope.data_group = {};

    if (angular.isDefined($stateParams.id)) {
        $scope.data_group = data_source_admin.data.get.data.groups;
    }

    //Objeto que contiene las variables y métodos para la gestión de grupos
    var $event_group = {
        _create_group: function () {
            data_source_admin._create_group($scope.data_group[0]);
        },
        _edit_group: function () {
            data_source_admin._edit_group($scope.data_group[0]);
        }
    };

    $scope.process_create_group = function () {
        $validationProvider.checkValid;
        if (!angular.isDefined($stateParams.id)) {
            $validationProvider.validate($scope.frmgroup).success($event_group._create_group);
        } else {
            $validationProvider.validate($scope.frmgroup).success($event_group._edit_group);
        }
    };

    $scope.back_list_group = function () {
        $window.history.back();
    }
}]);

//controlador para la gestión de permisos de grupos de usuarios
app.controller("controlGroupOperation", [
    "$scope", "$injector", "data_source_admin", "$window",
    function ($scope, $injector, data_source_admin, $window) {
        $scope.data = [];

        $scope.data = data_source_admin.data.get.data;
        $scope.process_operation_group = function () {
            data_source_admin._operation_group($scope.data);
        };
        $scope.back_list_group = function () {
            $window.history.back();
        };

        $scope.expandPens = function () {
            ivhTreeviewMgr.expand($scope.data.roles, $scope.data.roles[1]);
        };

        $scope.collapsePens = function () {
            ivhTreeviewMgr.collapse($scope.data.roles, '_pens_');
        };

    }

]);


//controlador para la gestión activación y desactivación de grupos de usuarios
app.controller("controlIoOperation", ["$scope",
    "$injector", "data_source_admin", "$window",
    function ($scope, $injector, data_source_admin, $window) {

        $scope.salida = data_source_admin.data.get.data;
        if ($scope.salida[0]._disabled == 'f') {
            $scope.salida.mensaje = 'Se procederá a Desactivar el Grupo de Usuario ' + $scope.salida[0].name;
            $scope.salida.mensaje_button = 'Desactivar';
        } else {
            $scope.salida.mensaje = 'Se procederá a Activar el Grupo de Usuario ' + $scope.salida[0].name;
            $scope.salida.mensaje_button = 'Activar';
        }

        $scope.process_io_group = function () {
            data_source_admin._io_group($scope.salida[0].id);
        };
        $scope.back_list_group = function () {
            $window.history.back();
        }
    }
]);


/**
 * Controlador para administrar la opción de cambio de contraseña
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 03/03/15
 */
app.controller('cambiarContrasenaCtrl', [
    '$scope',
    'data_source_admin',
    'flashMessages',
    '$location',
    '$window',
    function ($scope, data_source_admin, flashMessages, $location, $window) {
        $scope.datos = {
            actual: null,
            nueva: null,
            confirmacion: null
        };

        /**
         * Almacena las clases que se van a mostrar en los controles del formulario cuando hay error
         * @type {string}
         */
        $scope.statusConfirmacion = '';

        $scope.$watch('datos.confirmacion', function (newValue) {
            if (newValue != null) {
                if (newValue == $scope.datos.nueva) {
                    $scope.statusConfirmacion = 'has-success';
                } else {
                    $scope.statusConfirmacion = 'has-error';
                }
            }
        });


        /**
         * Envía los datos del formulario al servicio
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 04/03/15
         */
        $scope.submit = function () {
            data_source_admin.setNewPassword($scope.datos).then(
                function (response) {
                    if (response.success == false) {
                        flashMessages.show_error(response.message);
                    } else {
                        flashMessages.show_success(response.message);
                        $location.path('/');
                    }
                }
            );
        };

        $scope.volver = function () {
            $window.history.back();
        };
    }
]);


/**
 * Controlador para administrar la opción de recuperación de contraseña
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 11/03/15
 */
app.controller('olvidoContrasenaCtrl', [
    '$scope', 'data_source_admin', 'flashMessages', '$log', '$location',
    function ($scope, data_source_admin, flashMessages, $log, $location) {
        $scope.user_email = null;

        $scope.procesando = false;

        $scope.submit = function () {
            if (!$scope.user_email) {
                return;
            }
            $scope.procesando = true;
            data_source_admin.forgotPassword($scope.user_email).then(
                function (response) {
                    $scope.procesando = false;
                    if (!response.status) {
                        flashMessages.show_error(response.message);
                    }
                    else if (response.message) {
                        flashMessages.show_success(response.message);
                        $scope.volver();
                    }
                },
                function (response) {
                    $log.error(response);
                }
            );
        };

        $scope.volver = function () {
            $location.path('/login');
        }
    }
]);
