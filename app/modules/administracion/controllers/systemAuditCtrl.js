/**
 * Created by darwin on 24/03/15.
 */

'use strict';

app.constant('AUDIT_FILTERS', [
    {id: 'user', name: 'Usuario'},
    {id: 'transaction', name: 'Transacción'},
    {id: 'group', name: 'Grupo Usuario'}
]);

app.constant('AUDIT_FILTERS_COMPARE', [
    {id: 'equal', name: 'Igual'},
    {id: 'different', name: 'Diferente'}
]);

app.constant('AUDIT_TRANSACTIONS', [
    {id: 'ACCESS', name: 'Acceso'},
    {id: 'INSERT', name: 'Inserción de datos'},
    {id: 'UPDATE', name: 'Actualización de datos'},
    {id: 'DELETE', name: 'Borrado de datos'}
]);


/**
 * Controlador para administrar el listado de registros
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 25/03/15
 */
app.controller('systemAuditCtrl', [
    '$scope',
    'data_source_admin',
    'permission',
    'DTInstances',
    'AUDIT_TRANSACTIONS',
    '$modal',
    '$log',
    'AUDIT_FILTERS',
    'AUDIT_FILTERS_COMPARE',
    'flashMessages',
    'appConfig',
    '$filter',
    function ($scope, data_source_admin, permission, DTInstances, AUDIT_TRANSACTIONS, $modal, $log, AUDIT_FILTERS, AUDIT_FILTERS_COMPARE, flashMessages, appConfig, $filter) {

        var hoy = new Date();

        $scope.data = {
            dateStart: hoy,
            dateEnd: hoy,
            filters: []
        };

        $scope.dataSend = angular.copy($scope.data);

        var ajustarDataEnvio = function () {
            $scope.dataSend.dateStart = $filter('date')($scope.data.dateStart, 'yyyy-MM-dd');
            $scope.dataSend.dateEnd = $filter('date')($scope.data.dateEnd, 'yyyy-MM-dd');
            var filter = angular.toJson($scope.data.filters);
            $scope.dataSend.filters = angular.copy(filter);
        };
        ajustarDataEnvio();
        $scope.options = null;
        $scope.filterTypes = AUDIT_FILTERS;
        $scope.filterCompare = AUDIT_FILTERS_COMPARE;
        $scope.filter = {};

        $scope.$watch('filter.type', function (newValue) {
            $scope.options = null;
            if (newValue) {
                if (newValue.id == 'user') {
                    data_source_admin.getAllUsers().then(
                        function (response) {
                            $scope.options = response;
                        }
                    );
                }
                else if (newValue.id == 'group') {
                    data_source_admin.getGroups().then(
                        function (response) {
                            $scope.options = response;
                        }
                    );
                }
                else if (newValue.id == 'transaction') {
                    $scope.options = AUDIT_TRANSACTIONS;
                }
            }
        });

        $scope.$watch('data.dateStart', function () {
            $scope.reload();
        });

        $scope.$watch('data.dateEnd', function () {
            $scope.reload();
        });

        var objInArray = function (array, obj) {
            var filtersCopy = angular.fromJson(angular.toJson(angular.copy(array)));
            var filterCopy = angular.fromJson(angular.toJson(angular.copy(obj)));

            var isFilter = false;
            angular.forEach(filtersCopy, function (obj) {
                if (angular.equals(obj, filterCopy)) {
                    isFilter = true;
                    return;
                }
            });

            return isFilter;
        };

        $scope.addFilter = function () {
            if (!$scope.filter || !$scope.filter.type || !$scope.filter.compare || !$scope.filter.value) {
                flashMessages.show_error("Seleccione todos los elementos para construir el filtro");
                return;
            }

            if (objInArray($scope.data.filters, $scope.filter)) {
                flashMessages.show_error("Ya se agregó el la expresión al filtro");
                return;
            }

            $scope.data.filters.push($scope.filter);
            $scope.filter = null;
            $scope.reload();


        };

        $scope.removeFilter = function (index) {
            $scope.data.filters.splice(index, 1);
            $scope.reload();
        };

        /*
         Para manejo de multiples Datapicker en el formulario
         */
        $scope.calendar = {
            /**
             * Maneja el estado de los calendarios
             */
            opened: {},
            /**
             * Indica el formato de la fecha en los calendarios
             */
            dateFormat: 'dd/MM/yyyy',
            /**
             * Opciones del calendario
             */
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            maxDate: new Date(),
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        /*
         Fin de manejo de Datapicker
         */


        /*
         Tabla con listado
         */
        $scope.siteCrud = {
            url: 'auditoria/auditoria/dt_lista',
            data: $scope.dataSend,
            pathApi: appConfig.getAdminPathApi(),
            httpMethod: 'POST'
        };
        $scope.headerCrud = [
            {title: 'ID', label: 'id', visible: true},
            {title: 'Grupo Usuario', label: 'nombre_grupo', visible: true},
            {title: 'Usuario', label: 'nombre_usuario', visible: true},
            {title: 'Transaccion', label: 'transaccion', visible: true},
            {title: 'Tabla', label: 'tabla_transaccion', visible: true},
            {title: 'Fecha', label: 'fecha', visible: true}
        ];
        $scope.operation = permission.data.get.data.permission;

        $scope.reload = function () {
            DTInstances.getLast().then(function (lastDTInstance) {
                ajustarDataEnvio();
                lastDTInstance.reloadData();
            });
        };

        $scope.open = function (transactionId) {

            var modalInstance = $modal.open({
                templateUrl: "modules/administracion/views/form_system_audit_view.html",
                controller: 'systemAuditViewCtrl',
                size: 'lg',
                resolve: {
                    transactionId: function () {
                        return transactionId;
                    }
                }
            });

            modalInstance.result.then(function (selectedItem) {
                $scope.selected = selectedItem;
            }, function () {
                $log.info('Modal dismissed at: ' + new Date());
            });
        };


    }
]);


app.controller('systemAuditViewCtrl', [
    '$scope',
    'systemAuditMdl',
    '$modalInstance',
    'transactionId',
    function ($scope, systemAuditMdl, $modalInstance, transactionId) {
        $scope.data = null;
        systemAuditMdl.getTransaction(transactionId).then(
            function (response) {
                $scope.data = response;
            }
        );

        $scope.cerrar = function () {
            $modalInstance.close();
        };
    }
]);