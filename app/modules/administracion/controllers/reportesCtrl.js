'use strict';
/**
 * @author Maykol Purica <puricamaykol@gmail.com>
 * @return mixed
 * Controlador que gestiona la interfaz del balance del cliente de AVP
 */
app.controller("reportesCtrl", [
    "$scope",
    "$injector",
    "appConfig",
    "r_tiposPublicacion",
    "r_clientes",
    "r_medios",
    "r_tiposReportes",
    function ($scope,
              $injector,
              appConfig,
              r_tiposPublicacion,
              r_clientes,
              r_medios,
              r_tiposReportes) {
        $scope.title = "Filtros de reportes";
        $scope.filtro = {};
        $scope.filtroOut = {
            medio_id: '',
            cliente_id: '',
            tipoReporte: '',
            tipo_publicacion_id: '',
            contrato_id: '',
            dateStart: '',
            dateEnd: ''
        };

         
        $scope.filtroHabilitado = {};
        $scope.filtroHabilitado.medio = false;
        $scope.filtroHabilitado.tipo_publicacion = false;
        $scope.tipoPublicacion = r_tiposPublicacion;
        $scope.clientes = r_clientes;
        $scope.medios = r_medios;
        $scope.tipoReporte = r_tiposReportes;
  
        
        $scope.change = function($cliente_id) {
  
             var contratoModel = $injector.get('contratoModel', 'change');       
             $scope.contratos = contratoModel.listar({client: $cliente_id});         
      };
        //Se inician los calendarios y ambas fechas del filtro en la fecha actual
        var toDay = new Date();
        var hoy = moment(toDay).format('YYYY-MM-DD');

        /**
         * Objeto del calendario
         */

        $scope.calendar = {
            opened: {},
            now: new Date(),
            dateFormat: 'dd/MM/yyyy',
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        $scope.filtrarMedios = function () {
            var Medio = $injector.get('Medio');
            if ($scope.filtro.tipo_publicacion != null) {
                var mediosFiltrados = Medio.listado(
                    {tipoPublicacionId: $scope.filtro.tipo_publicacion.id}
                ).$promise.then(function (resultado) {
                        $scope.medios = resultado;
                        $scope.filtro.medio = null;
                    });
            } else {
                Medio.query().$promise.then(function (resultado) {
                    $scope.medios = resultado[0].data;
                    $scope.filtro.medio = null;
                });
            }
        };
        $scope.imprimir = function () {
            $scope.setUpFilter();
            
            window.location.href = appConfig.getPathApi() + 'reportes/generar?' + $.param($scope.filtroOut);


        };
        /**
         * Esta funcion organiza los parametros de los filtros usados en DataTables y en el reporte
         */
        $scope.setUpFilter = function () {
            //Si la fecha "Hasta" no esta definida se setea con la fecha actual
            if ($scope.filtro.fecha_hasta) {
                $scope.filtroOut.dateEnd = angular.isDefined(moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD') : moment(toDay).format('YYYY-MM-DD');
            } else {
                $scope.filtroOut.dateEnd = moment(toDay).format('YYYY-MM-DD');
            }
            if ($scope.filtro.fecha_desde) {
                $scope.filtroOut.dateStart = angular.isDefined(moment($scope.filtro.fecha_desde).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_desde).format('YYYY-MM-DD') : false;
            } else {
                $scope.filtroOut.dateStart = false;
            }
            if ($scope.filtro.medio) {
                $scope.filtroOut.medio_id = $scope.filtro.medio.id;
            } else if ($scope.filtro.medio == null) {
                $scope.filtroOut.medio_id = '';
            }
            if ($scope.filtro.tipo_reporte) {
                $scope.filtroOut.tipoReporte = $scope.filtro.tipo_reporte.id;
            } else if ($scope.filtro.tipo_reporte == null) {
                $scope.filtroOut.tipoReporte = '';
            }
            if ($scope.filtro.cliente) {
                $scope.filtroOut.cliente_id = $scope.filtro.cliente.id;
            } else if ($scope.filtro.cliente == null) {
                $scope.filtroOut.cliente_id = '';
            }
            if ($scope.filtro.contrato) {
                $scope.filtroOut.contrato_id = $scope.filtro.contrato.id;
            } else if ($scope.filtro.contrato == null) {
                $scope.filtroOut.contrato_id = '';
            }
            if ($scope.filtro.tipo_publicacion) {
                $scope.filtroOut.tipo_publicacion_id = $scope.filtro.tipo_publicacion.id;
            } else if ($scope.filtro.tipo_publicacion == null) {
                $scope.filtroOut.tipo_publicacion_id = '';
            }
        };
        /**
         * @aythor Maykol Purica
         * Se setea si el filtro estara habilitado (true) o no (false) en cada tipo de reporte
         */
        $scope.toggleFilters = function () {
            switch ($scope.filtro.tipo_reporte.id) {
                case 1:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                                        
                    break;
                case 2:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                                        
                    break;
                case 3:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                                        
                    break;
                case 4:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = true;
                      $scope.filtroHabilitado.contratos = false;
                    
                    break;
                case 5:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                      $scope.filtroHabilitado.contratos = false;
                    
                    break;
                case 7:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                      $scope.filtroHabilitado.contratos = false;
                    
                    break;
                case 8:
                    $scope.filtroHabilitado.medio = true;
                    $scope.filtroHabilitado.tipo_publicacion = true;
                      $scope.filtroHabilitado.contratos = false;
                    
                    break;
                case 9:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.cliente = true;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                      $scope.filtroHabilitado.contratos = true;

                    break;
                case 10:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                      $scope.filtroHabilitado.contratos = false;
                    
                    break;
                case 11:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                      $scope.filtroHabilitado.contratos = false;
                    
                    break;
                case 12:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                      $scope.filtroHabilitado.contratos = false;
                    
                    break;
                case 13:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                      $scope.filtroHabilitado.contratos = false;
                    
                    break;
                default:
                    $scope.filtroHabilitado.medio = false;
                    $scope.filtroHabilitado.tipo_publicacion = false;
                      $scope.filtroHabilitado.contratos = false;
                    
            }
        }

    }]);