'use strict';

app.factory("data_source_admin", ['$injector', function ($injector) {
    var $log = $injector.get('$log', 'data_source_admin');
    var $location = $injector.get('$location', 'data_source_admin');
    var flashMessages = $injector.get('flashMessages', 'data_source_admin');
    var appConfig = $injector.get('appConfig', 'data_source_admin');
    var $http = $injector.get('$http', 'data_source_admin');
    var $q = $injector.get('$q', 'data_source_admin');

    var _pathApi = appConfig.getAdminPathApi();

    var onSuccess = function (response) {
        return response.data;
    };

    var onError = function (response) {
        $log.error(response);
        flashMessages.show_error('Ocurrió un error al realizar la solicitud de datos.');
    };

    return ({
        data: {},
        resolve_user: function (id, mode) {
            var _url = _pathApi + 'administracion/user_controller/store?id=' + id;
            var defer = $q.defer();
            var data = this.data;
            $http.get(_url).
                then(
                function success(response) {
                    data.mode = mode;
                    data.get = response;
                    defer.resolve(data);
                },
                //function error(reason) {
                function error() {
                    return false;
                }
            );
            return defer.promise;

        }, resolve_group: function (id) {

            var _url = _pathApi + 'administracion/group_controller/store?id=' + id;
            var defer = $q.defer();
            var data = this.data;
            $http.get(_url).
                then(
                function success(response) {
                    data.get = response;
                    defer.resolve(data);
                },
                //function error(reason) {
                function error() {
                    return false;
                }
            );
            return defer.promise;

        }, resolve_group_operation: function (id) {
            var _url = appConfig.getPathApi() + 'group_operation?id=' + id;
            var defer = $q.defer();
            var data = this.data;
            $http.get(_url).
                then(
                function success(response) {
                    data.get = response;
                    defer.resolve(data);
                },
                //function error(reason) {
                function error() {
                    return false;
                }
            );
            return defer.promise;

        }, resolve_io_operation: function (id) {
            var _url = _pathApi + 'administracion/group_controller/io_operation?id=' + id;
            var defer = $q.defer();
            var data = this.data;
            $http.get(_url).
                then(
                function success(response) {
                    data.get = response;
                    defer.resolve(data);
                },
                //function error(reason) {
                function error() {
                    return false;
                }
            );
            return defer.promise;

        },
        _create_user: function (param) {
            var url = 'auth_cotizaciones/create_user';

            return $http({
                method: 'post',
                url: _pathApi + url,
                data: param
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                    //$window.history.back();
                    $location.path('admin/list_user')
                } else {
                    flashMessages.show_error(data.messages);
                }
            }).error(function (data, status) {
                var msjText = 'Ha ocurrido un error..!! ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });
        },
        _edit_user: function (param) {
            var url = 'auth_cotizaciones/edit_user';

            return $http({
                method: 'post',
                url: _pathApi + url,
                data: param
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    //AuthService.logout().then(function () {
                    //    $window.location.reload();
                    //});
                    flashMessages.show_success(data.messages);
                    //$window.history.back();
                    $location.path('admin/list_user')
                } else {
                    flashMessages.show_error(data.messages);
                }
            }).error(function (data, status) {
                var msjText = 'Ha ocurrido un error..!! ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });
        },
        _edit_group: function (param) {
            //console.log(param)
            var url = 'auth_cotizaciones/edit_group';
            return $http({
                method: 'post',
                url: _pathApi + url,
                data: param
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                    //$window.history.back();
                    $location.path('admin/list_group')
                } else {
                    flashMessages.show_error(data.messages);
                }
            }).error(function (data, status) {

                var msjText = 'Ha ocurrido un error..!! ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });
        },
        _create_group: function (param) {
            var url = 'auth_cotizaciones/create_group';

            return $http({
                method: 'post',
                url: _pathApi + url,
                data: param
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                    //$window.history.back();
                    $location.path('admin/list_group')
                } else {
                    flashMessages.show_error(data.messages);
                }
            }).error(function (data, status) {
                var msjText = 'Ha ocurrido un error..!! ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });
        },
        _io_group: function (id) {
            var _url = _pathApi + 'administracion/group_controller/proccess_io_operation?id=' + id;

            return $http({
                method: 'get',
                url: _url
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                    //$window.history.back();
                    $location.path('admin/list_group')
                } else {
                    flashMessages.show_error(data.messages);
                }
            }).error(function (data, status) {
                var msjText = 'Ha ocurrido un error..!! ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });
        },

        setNewPassword: function (datos) {
            var _url = _pathApi + 'auth_cotizaciones/change_password';
            return $http.post(_url, datos).then(
                function (response) {
                    return response.data;
                },
                function (response) {
                    $log.error(response);
                }
            );
        },
        _operation_group: function (param) {
            //var url = 'administracion/group_controller/add_remove_operation';
            var url = appConfig.getPathApi() + 'add_remove_operation';
            return $http({
                method: 'post',
                url: url,//_pathApi + url,
                data: param
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                    //$window.history.back();
                    $location.path('admin/list_group')
                } else {
                    flashMessages.show_error(data.messages);
                }
            }).error(function (data, status) {
                var msjText = 'Ha ocurrido un error..!! ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });
        },
        _io_user: function (id) {
            var url = 'administracion/user_controller/io_user?id=' + id;
            return $http({
                method: 'get',
                url: _pathApi + url
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                    /*      $location.path('admin/list_group')
                     $location.path('admin/list_user')*/
                    $injector.get('$state', 'data_source_admin').go($state.current, {}, {reload: true});
                } else {
                    flashMessages.show_error(data.messages);
                }
            }).error(function (data, status) {
                var msjText = 'Ha ocurrido un error..!! ( "Error' + status + '" )';
                flashMessages.show_error(msjText);
            });
        },
        _get_data_user: function (id) {
            var url = 'administracion/user_controller/data_user?id=' + id;
            return $http({
                method: 'get',
                url: _pathApi + url
            });
        },

        getGroups: function () {
            var _url = _pathApi + 'administracion/group_controller/all_group';
            return $http.get(_url).then(onSuccess, onError);
        },

        /**
         * Envía la solicitud de reseteo de contraseña
         * @param userEmail
         * @returns {*}
         */
        forgotPassword: function (userEmail) {
            var url = 'auth_cotizaciones/forgot_password';
            return $http.post(_pathApi + url, {email: userEmail}).then(
                function (response) {
                    return response.data;
                }
            );
        },

        /**
         * Obtiene un listado de usuarios existente en la aplicación
         * @returns {*}
         */
        getAllUsers: function () {
            var _url = _pathApi + 'administracion/user_controller/users';
            return $http.get(_url).then(onSuccess, onError);
        }
    });
}]);
