'use strict';

app.factory('reporteModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        var _url = appConfig.getPathApi() + 'reportes';
        var data = $resource(_url, null, {
            generar: {
                method: 'POST',
                url: appConfig.getPathApi() + 'reportes/generar'
            }

        });
        return data;
    }

]);
