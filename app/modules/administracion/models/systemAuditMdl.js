'use strict';
/**
 * Created by darwin on 24/03/15.
 */
app.factory('systemAuditMdl', [
    '$http',
    'appConfig',
    '$log',
    'flashMessages',
    function ($http, appConfig, $log, flashMessages) {

        var onSuccess = function (response) {
            return response.data;
        };

        var onError = function (response) {
            $log.error(response);
            flashMessages.show_error('Ocurrió un error al realizar la solicitud de datos.');
        };

        return {
            /**
             * Obtiene el detalle de una transacción
             * @param id
             * @returns {*}
             */
            getTransaction: function (id) {
                var _url = appConfig.getAdminPathApi() + 'auditoria/auditoria/transaccion?id=' + id;
                return $http.get(_url).then(onSuccess, onError);
            }
        }
    }
]);
