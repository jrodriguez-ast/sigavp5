'use strict';

/**
 * Provee el listado de facturas en el estatus entregada al cliente
 *
 * @author Carlos blanco <cebs923@gmail.com>
 * @package SIGAVP
 * @version V-1.0 07/09/2015
 */
app.controller('PagosCtrlC', [
    '$scope',
    'particularPermission',
    'clientID',
    '$injector',
    function ($scope,
              particularPermission,
              clientID,
              $injector) {
        $scope.title = 'Facturas por Cobrar';
        $scope.siteCrud = {url: 'clientes/ ' + clientID + '/listadofacturaporcobrar'};
        $scope.headerCrud = [
            {title: 'Cobrar', label: 'pagar', visible: true},
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'Estatus', label: 'estatus', visible: true},
            {title: 'Monto total', label: 'total_factura', visible: true, filter: 'formatNumberMoney'},
            {title: 'Monto restante', label: 'restante', visible: true, filter: 'formatNumberMoney'}

        ];
        $scope.checkFactura = {};
        $scope.aFacturar = [];
        $scope.restantestotal = '';
        $scope.permisos = particularPermission.permission;


        /**
         * Esta función obtiene los ID de las publicaciones marcadas para ser facturadas
         */
        $scope.getChecked = function (facturar) {
            var restante = 0;
            var id = '';
            $scope.aFacturar = [];
            angular.forEach($scope.checkFactura, function (value) {
                if (value.chckd == true) {
                    $scope.aFacturar.push(value);
                    restante = restante + value.restante;
                    id =  value.id;
                }
            });
            facturar = $scope.aFacturar;
            $scope.restantestotal = restante;
            $scope.id = id;
           
            if (facturar.length > 0) {
                $scope.pagoFactura(facturar);
            } else {
                var flashMessages = $injector.get('flashMessages', 'getChecked');
                flashMessages.show_info("Debe seleccionar al menos una factura.");
            }

        };


        $scope.pagoFactura = function (facturar) {
            $scope.aFacturar = facturar;

            var $modal = $injector.get('$modal', 'pagoFactura');
            var $modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/clientes/views/clientes/pagos/form.html',
                controller: 'realizarPagoController',
                size: 'lg',
                resolve: {
                    r_afacturar: function () {
                        return $scope.aFacturar;
                    },
                    r_totalrestante: function () {
                        return $scope.restantestotal;
                    },
                    rdata_factura_con_nota: ['$stateParams', 'facturaModel', function ($stateParams, facturaModel) {
                        return facturaModel.datos_factura_nota({clienteID: $stateParams.id, facturaID: $scope.id }).$promise;
                    }]
                }
            });
            $modalInstance.result.then(function () {
                var $state = $injector.get('$state', 'then');
                $state.reload('admin.detalle_cliente.pagos_list');
            });
        };

    }

]);

/**
 * Provee el listado de facturas pagadas con el estatus canceladas
 *
 * @author Carlos blanco <cebs923@gmail.com>
 * @package SIGAVP
 * @version V-1.0 07/09/2015
 */
app.controller('FacturasPagadasCtrlC', [
    '$scope', 'particularPermission', 'clientID',
    function ($scope, particularPermission, clientID) {
        $scope.title = 'Facturas Cobradas';
        $scope.siteCrud = {url: 'clientes/ ' + clientID + '/factpagadasIndex'};

        $scope.headerCrud = [
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'Estatus', label: 'estatus', visible: true},
            {title: 'Monto total', label: 'total_factura', visible: true, filter: 'formatNumberMoney'},
            {title: 'Monto restante', label: 'restante', visible: true, filter: 'formatNumberMoney'}

        ];
        $scope.operation = particularPermission.permission;

    }

]);

app.controller("realizarPagoController", [
    "$scope",
    "$injector",
    '$modalInstance',
    'r_afacturar',
    'r_totalrestante',
    'rdata_factura_con_nota',
    function ($scope,
              $injector,
              $modalInstance,
              r_afacturar,
              r_totalrestante,
              rdata_factura_con_nota) {

        var Pagos = $injector.get('Pagos', 'realizarPagoController');
        var $stateParams = $injector.get('$stateParams', 'realizarPagoController');
        var flashMessages = $injector.get('flashMessages', 'realizarPagoController');
        var $modal = $injector.get('$modal', 'realizarPagoController');

        $scope.facturas_con_nota = rdata_factura_con_nota.data;
        $scope.facturas = r_afacturar;
        $scope.totalRestante = r_totalrestante;
        $scope.detalles = {};
        $scope.detalles.monto = '';
        $scope.detalles.descripcion = '';
        $scope.formas_pago = [
            {id: 'cheque', descripcion: 'Cheque'},
            {id: 'tranferencia', descripcion: 'Transferencia'},
            {id: 'retencion_iva', descripcion: 'Retención IVA'},
            {id: 'retencion_islr', descripcion: 'Retención ISLR'},
            {id: 'deposito', descripcion: 'Deposito'}];



        /**
         * Métodos para el DatePicker de fecha de fundación
         */
        $scope.calendar = {
            opened: {},
            dateFormat: 'dd/MM/yyyy',
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };

        /*
         Objeto que se enviara al Backend
         */


        $scope.contact = {};
        $scope.contact.monto = '';
        $scope.contact.descripcion = '';
        $scope.contact.facturas = '';
        $scope.contact.cliente = $stateParams.id;
        $scope.contact.banco = '';
        $scope.contact.numero_transaccion = '';
        $scope.contact.formas_pagos = '';
        $scope.contact.fecha_pago = '';


        var $validationProvider = $injector.get('$validation', 'realizarPagoController');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },

            prepare: function (service) {
                var data = angular.copy(service);
                //data.monto = data.monto.replace(/\./g, '').replace(',', '.');
                data.monto = $injector.get('$filter', 'prepare')('formatMoneyToNumber')(data.monto);
                return data;
            },
            /**
             * Enviar las condiciones al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodriguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                $scope.contact.monto = $scope.detalles.monto;
                $scope.contact.descripcion = $scope.detalles.descripcion;
                $scope.contact.facturas = $scope.facturas;
                $scope.contact.banco = $scope.detalles.banco;
                $scope.contact.numero_transaccion = $scope.detalles.numero_transaccion;
                $scope.contact.formas_pagos = $scope.formas_pagos.id;
                $scope.contact.fecha_pago = $scope.detalles.fecha_pago;
                Pagos.crear({clienteID: $stateParams.id},

                    $scope.mediocontacto_form.prepare($scope.contact),
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors);
                $modalInstance.close();
            },
            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha cobrado de manera exitosa.");
                $scope.$close(true);
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                    }

                    //if (angular.isDefined(message.nombre)) {
                    //    mensajeToShow = mensajeToShow.concat(message.nombre);
                    //}
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');

            }
        }
    }


]);

app.controller('abonoContratoCtrl',
    [
        '$scope',
        "$injector",
        'rContrato',
        'contratoID',
        'rAction',
        'rTitle',
        'rCliente',
        'rClienteID',
        '$modalInstance',
        'rTotal',
        'particularPermission',

        function ($scope,
                  $injector,
                  rContrato,
                  contratoID,
                  rAction,
                  rTitle,
                  rCliente,
                  rClienteID,
                  $modalInstance,
                  rTotal,
                  particularPermission) {
            $scope.contratos = rContrato.data;
            //console.log(rContrato);
            // Directiva de Validación.

           // var flashMessages = $injector.get('flashMessages', 'abonoContratoCtrl');

            $scope.detalles = {};
            $scope.detalles.contrato = {};
            $scope.detalles.contrato.tipo = {};
            $scope.formAction = rAction;
            //$scope.title = rTitle;

            $scope.change = function () {

                $scope.contrato_id = $scope.detalles.contrato.id;
                $scope.nombre = $scope.detalles.contrato.tipo.nombre;
                $scope.permisos = particularPermission;
                $scope.checkFactura = {};
                $scope.aFacturar = [];
                $scope.chckdCount = rTotal.cantidad;

                if ($scope.nombre == 'orden_servicio') {
                    $scope.title = "Publicaciones no facturadas del cliente";
                    $scope.siteCrud = {url: 'clientes/' + rClienteID + '/public_nofact/' + $scope.contrato_id + '?/' };
                    $scope.headerCrud = [
                        {title: 'Facturar', label: 'facturar', visible: true},
                        {title: 'id', label: 'id', visible: false},
                        {title: 'Código', label: 'codigo', visible: true},
                        {title: 'Version', label: 'version_nombre', visible: true},
                        {title: 'cliente_id', label: 'cliente_id', visible: false},
                        {title: 'Fecha creación', label: 'fecha_creacion', visible: true},
                        {title: 'Proveedor', label: 'medio.razon_social', visible: true},
                        {title: 'Medio', label: 'nombre', visible: true},
                        {title: 'Importe (Bs.)', label: 'valor_con_recargo', visible: true, filter: 'formatNumberMoney'},
                        //{title: 'Importe (Bs.)', label: 'importe', visible: true, filter: 'formatNumberMoney'},
                        {title: 'Estatus', label: 'estatus', visible: true}
                    ];

                    $scope.totalSinIva = rTotal.sinIva;
                    $scope.totalConIva = rTotal.conIva;
                    $scope.total_impuesto = rTotal.total_impuesto;
                    $scope.total_impuesto_avp = rTotal.total_impuesto_avp;
                    $scope.importe = rTotal.importe;
                    $scope.detalles.n_control = '';

                    // $scope.operation = $injector.get('permission', 'abonoContratoCtrl').data.get.data.permission;


                } else {
                     $scope.detalles.n_control = '';

                    $scope.title = "Cuotas no facturadas del cliente";
                    $scope.siteCrud = {url: 'clientes/' + rClienteID + '/public_cuotanofact/' + $scope.contrato_id};
                    $scope.headerCrud = [
                        {title: 'Facturar Cuota', label: 'facturar', visible: true},
                        {title: '#', label: 'numero_cuota', visible: true},
                        {title: 'id', label: 'id', visible: false},
                        {title: 'cliente_id', label: 'cliente_id', visible: false},
                        {title: 'Monto', label: 'monto', visible: true, filter: 'formatNumberMoney'},
                        {title: 'Fecha de pago', label: 'fecha', visible: true, filter: "date", filterParams: "dd-MM-yyyy"}

                    ];
                }
                /**
                 * Esta función obtiene los ID de las publicaciones marcadas para ser facturadas
                 */
                $scope.getChecked1 = function (parametro, parametrow, ncontrol) {


                    angular.forEach($scope.checkFactura, function (value) {
                        if (value.chckd == true) {
                            $scope.aFacturar.push(value);
                        }
                    });

                    if(ncontrol.length <= 0 ){
                        //cuando se cancela o acepta el mensaje se pierde el scroll por eso se coloco el otro
                        //mensaje de aceptacion
                        //var $ngBootbox = $injector.get('$ngBootbox', 'getChecked1');
                        //$ngBootbox.confirm('Debes agregar un numero de control')
                        window.confirm('Debes agregar un numero de control')
                    }else{
                        $scope.facturar($scope.aFacturar );
                    }

                };
                /**
                 * Llama al servicio que carga la factura
                 * @param publicacionArray
                 */
                $scope.facturar = function (publicacionArray) {

                    var $ngBootbox = $injector.get('$ngBootbox', 'facturar');
                    $ngBootbox.confirm('¿Esta seguro de generar la factura para lo seleccionadas?')

                        .then(function () {

                            var facturaModel = $injector.get('facturaModel', 'facturar');
                            var datos = {};
                            if ($scope.nombre == 'orden_servicio') {

                                datos.n_control = $scope.detalles.n_control;
                                datos.factura = publicacionArray;
                                facturaModel.crear(null, datos).$promise.then(function (respuesta) {
                                    var newFactura = respuesta;
                                    var appConfig = $injector.get('appConfig', 'facturar');
                                    var $location = $injector.get('$location', 'facturar');
                                    window.open(appConfig.printPDF('FACTURACION', newFactura.data.id));
                                    //$location.path("admin/detalle_cliente/" + rClienteID + "/facturas_list");
                                    //$location.path("admin/detalle_cliente/" + rClienteID + "/facturar");
                                    var $state = $injector.get('$state', 'facturaModel.crear');
                                    $state.go('admin.detalle_cliente.facturas_list');
                                    //$state.reload('admin.detalle_cliente');
                                    //ventana.onbeforeunload = ventana.opener.location.reload();
                                });
                            } else {
                                
                                datos.n_control = $scope.detalles.n_control;
                                datos.factura = publicacionArray;
                                facturaModel.crear2(null, datos).$promise.then(function (respuesta) {
                                    var newFactura = respuesta;
                                    var appConfig = $injector.get('appConfig', 'facturar');
                                    var $location = $injector.get('$location', 'facturar');
                                    window.open(appConfig.printPDF('FACTURACIONCUOTA', newFactura.data.id));
                                    //$location.path("admin/detalle_cliente/" + rClienteID + "/facturar");
                                    var $state = $injector.get('$state', 'facturaModel.crear2');
                                    $state.go('admin.detalle_cliente.facturas_list')
                                    //$state.reload('admin.detalle_cliente');
                                    //ventana.onbeforeunload = ventana.opener.location.reload();
                                });

                            }
                        }, function () {
                            return false;
                        });
                };
                /**
                 * Suma el costo de las publicaciones cada vez que se hace click en uno de los checkboxes
                 */
                $scope.sumarPublicaciones = function () {
                    var count = 0;
                    if ($scope.nombre == 'orden_servicio') {
                        var total = 0;
                        var subtotal = 0;
                        var importe = 0;
                        var total_impuesto_avp = 0;
                        $scope.totalSinIva = 0;
                        $scope.totalConIva = 0;
                        //console.log($scope.checkFactura);
                        angular.forEach($scope.checkFactura, function (value) {
                            if (value.chckd == true) {
                                count++;
                                total = total + value.totalpub;
                                subtotal = subtotal + value.subtotalpub;
                                importe = importe + value.importe;
                                total_impuesto_avp = total_impuesto_avp + value.total_impuesto_avp;
                            }
                        });
                        $scope.chckdCount = count;
                        $scope.total_impuesto_avp = total_impuesto_avp;
                        $scope.importe = importe;
                        $scope.totalSinIva = subtotal;
                        $scope.totalConIva = total;

                    } else {
                        var totalcuota = 0;
                        var totaliva = 0;
                        $scope.totalSinIva = 0;
                        $scope.totalConIva = 0;
                        //console.log($scope.checkFactura);
                        angular.forEach($scope.checkFactura, function (value) {
                            if (value.chckd == true) {
                                count++;
                                totalcuota = totalcuota + value.montototal;
                                totaliva = totaliva + value.montoiva;

                            }
                        });
                        $scope.chckdCount = count;
                        $scope.totalConIva = totalcuota;
                        $scope.totalIva = totaliva;
                    }

                }
            };
        }]);