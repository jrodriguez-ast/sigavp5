'use strict';
/**
 * @author Maykol Purica <puricamaykol@gmail.com>
 * @return mixed
 * Controlador que gestiona la interfaz del balance del cliente de AVP
 */
app.controller("balanceListController", [
    "$scope", "particularPermission", "clientID", "rSaldo", "$injector", "rContratoID","rTabId",
    function ($scope, particularPermission, clientID, rSaldo, $injector, rContratoID, rTabId) {
        var DTInstances = $injector.get("DTInstances", "balanceListCtrl");
        var appConfig = $injector.get("appConfig", "balanceListCtrl");
        var $state = $injector.get('$state', 'balanceListCtrl');

        $scope.title = "Balance del Cliente";
        $scope.title_detalle_saldo = 'Total disponible para publicar: ';
        $scope.title_detalle_deuda = 'Deuda total: ';
        $scope.saldo = rSaldo.data.saldo;
        $scope.deuda = rSaldo.data.deuda;
        $scope.filtro = {};
        $scope.filtro.todos = true;

        //Se inician los calendarios y ambas fechas del filtro en la fecha actual
        var toDay = new Date();
        var hoy = moment(toDay).format('YYYY-MM-DD');
        $scope.tipo = [{id: 0, nombre: 'Todos'}, {id: 1, nombre: 'Cargos'}, {id: 2, nombre: 'Débitos'}];
        $scope.filtro.tipo = $scope.tipo[0];
        $scope.siteCrud = {
            url: 'clientes/balanceClienteDt/' + clientID + '/' + rContratoID,
            is_orderable: false,
            data: {
                dateStart: hoy,
                dateEnd: hoy,
                tipomovimiento: 'Todos',
                todos: true
            }
        };

        /*
         factura_id integer, -- Id de la Factura
         usuario_id integer NOT NULL, -- Codigo del usuario, No puede ser nulo
         contrato_cuota double precision NOT NULL DEFAULT 0, -- Pagos necesarios para el contrato
         observaciones character varying(250) DEFAULT NULL::character varying, -- Observaciones que se puedan encontrar del balance
         */

        $scope.headerCrud = [
            {title: 'id', label: 'id', visible: false, orderable: false}, // 0
            {title: 'Relaciones', label: 'acciones', searchable: false}, // 1
            {title: 'Transacción', label: 'codigo'}, // 2
            {title: 'Cod. Contrato', label: 'contrato.codigo', searchable: false}, // 3
            //{title: 'cliente_id', label: 'cliente_id', visible: false, orderable: false},
            //{title: 'usuario_id', label: 'usuario_id', visible: false, orderable: false},
            {title: 'Saldo (Bs.)', label: 'saldo', orderable: false, filter: 'moneySaldo'}, // 4
            {title: 'Cargo (Bs.)', label: 'cargo', filter: 'formatNumberMoney', orderable: false}, // 5
            {title: 'Débito (Bs.) ', label: 'debito', filter: 'formatNumberMoney', orderable: false}, // 6
            {title: 'Deuda (Bs.) ', label: 'deuda', filter: 'formatNumberMoney', orderable: false}, // 7
            {title: 'Cuota (Bs.) ', label: 'contrato_cuota', filter: 'formatNumberMoney', orderable: false}, // 8
            {title: 'Descripción', label: 'descripcion', orderable: false}, // 8
            {title: 'Fecha', label: 'created_at', orderable: false, filter: 'date', filterParams: 'dd-MM-yyyy'} // 9
        ];

        if (rContratoID != 0) {
            $scope.headerCrud[4].label = 'contrato_saldo';
            $scope.headerCrud[7].label = 'contrato_deuda';
            $scope.title = "Balance del Contrato";
            $scope.title_detalle_deuda = 'Total deuda contrato: ';


            var tab = {
                id: rTabId,
                title: 'Balance Contrato #',
                state: 'admin.detalle_cliente.balance_contrato({contratoID:' + rContratoID + '})',
                object_id: rContratoID
            };
            $scope.buildTab(tab);
            //$state.go('^.balance_contrato', {'contratoID': rContratoID});
        }

        $scope.operation = particularPermission.permission;

        /**
         * Objeto del calendario
         */
        $scope.calendar = {
            opened: {},
            now: new Date(),
            dateFormat: 'dd/MM/yyyy',
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        /**
         * Función que carga los datos del filtro y recarga la tabla
         */
        $scope.filtrar = function () {
            DTInstances.getLast().then(function (lastDTInstance) {
                $scope.setUpFilter();
                lastDTInstance.reloadData();
            });
        };
        /**
         * Función que gestiona la apertura de la pestaña de detalles de la operación del balance
         * @param balanceID
         * @param operation_id
         */
        $scope.detalleOperacion = function (balanceID, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Detalle Operación #',
                state: 'admin.detalle_cliente.detalles_balance({balanceID:' + balanceID + '})',
                object_id: balanceID
            };
            $scope.buildTab(tab);
            $state.go('^.detalles_balance', {'balanceID': balanceID});
        };
        /**
         * Función que gestiona la pestaña de observaciones de la operación
         * @param balanceID
         * @param operation_id
         */
        $scope.addObservacion = function (balanceID, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Observaciones Operación #',
                state: 'admin.detalle_cliente.addobservacion_balance({balanceID:' + balanceID + '})',
                object_id: balanceID
            };
            $scope.buildTab(tab);
            $state.go('^.addobservacion_balance', {'balanceID': balanceID});
        };

        $scope.imprimir = function () {
            $scope.setUpFilter();
            window.open(appConfig.getPathApi() + 'clientes/balanceClienteDt/' + clientID + '?reporte=true&dateStart=' + $scope.siteCrud.data.dateStart + '&dateEnd=' + $scope.siteCrud.data.dateEnd + '&tipomovimiento=' + $scope.siteCrud.data.tipomovimiento + '&todos=' + $scope.siteCrud.data.todos);
        };
        /**
         * Esta función organiza los parámetros de los filtros usados en DataTables y en el reporte
         */
        $scope.setUpFilter = function () {
            $scope.siteCrud.data.dateStart = moment($scope.filtro.fecha_desde).format('YYYY-MM-DD');
            //Si la fecha "Hasta" no esta definida se setea con la fecha actual
            if ($scope.filtro.fecha_hasta) {
                $scope.siteCrud.data.dateEnd = angular.isDefined(moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD') : moment(toDay).format('YYYY-MM-DD');
            } else {
                $scope.siteCrud.data.dateEnd = moment(toDay).format('YYYY-MM-DD');
            }
            if ($scope.filtro.fecha_desde) {
                $scope.siteCrud.data.dateStart = angular.isDefined(moment($scope.filtro.fecha_desde).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_desde).format('YYYY-MM-DD') : false;
            } else {
                $scope.siteCrud.data.dateStart = false;
            }
            $scope.siteCrud.data.tipomovimiento = $scope.filtro.tipo.nombre;
            $scope.siteCrud.data.todos = $scope.filtro.todos;
        }

    }]);

/**
 * Controlador que da acceso a los detalles del movimiento del balance.
 */
app.controller('balanceDetalleController', [
    '$scope', 'rBalance', '$injector', 'rAction', 'balanceID', 'rTabId',
    function ($scope, rBalance, $injector, rAction, balanceID, rTabId) {
        $scope.balance = rBalance.data;
        $scope.formAction = rAction;

        var $validationProvider = $injector.get('$validation', 'balanceDetalleCtrl');
        var $stateParams = $injector.get('$stateParams', 'balanceDetalleCtrl');

        $scope.balance_form = {
            self: this,
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.balance_form.send();
                });
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },
            prepare: function (balance) {
                return angular.copy(balance);
            },
            send: function () {
                var balanceModel = $injector.get('balanceModel', 'send');
                if ($scope.formAction == 'update') {
                    var params = {balanceID: balanceID};
                    balanceModel.update(
                        params,
                        $scope.balance_form.prepare($scope.balance),
                        $scope.balance_form.success
                    );
                }
            },
            success: function () {
                $injector.get('flashMessages', 'ps').show_success("Datos guardados exitosamente.");
                $injector.get('$location', 'ps').path("admin/detalle_cliente/" + $scope.balance.cliente_id + "/balance");
            }
        };

        var $state = $injector.get('$state', 'addObservacionMedio');
        var tab = {
            id: rTabId,
            title: 'Observaciones Operación #',
            state: 'admin.detalle_medio.addobservacion_balance({balanceID:' + balanceID + '})',
            object_id: balanceID
        };
        if (rAction == 'update') {
            $scope.buildTab(tab);
            $state.go('^.addobservacion_balance', {'balanceID': balanceID});
        } else {
            tab = {
                id: rTabId,
                title: 'Detalle Operación #',
                state: 'admin.detalle_medio.detalles_balance({balanceID:' + balanceID + '})',
                object_id: balanceID
            };
            $scope.buildTab(tab);
            $state.go('^.detalles_balance', {'balanceID': balanceID});
        }
    }
]);
/**
 * Controlador que permite la vista del listado del balance
 * por tipo de publicacion
 */
app.controller("balancexTipoListController", [
    "$scope", "particularPermission", "clientID",  "$injector", "rContratoID","rTabId", "rSaldo",
    function ($scope, particularPermission, clientID,  $injector, rContratoID, rTabId, rSaldo) {
        var DTInstances = $injector.get("DTInstances", "balancexTipoListController");
        var appConfig = $injector.get("appConfig", "balancexTipoListController");
        var $state = $injector.get('$state', 'balancexTipoListController');

        $scope.title = "Balance por tipo de publicacion";
        $scope.title_detalle_saldo = 'Total disponible para publicar:';
        //$scope.title_detalle_deuda = 'Deuda total:';
        $scope.saldo = rSaldo.data.monto;
        //$scope.deuda = rSaldo.data.deuda;
        $scope.filtro = {};
        $scope.filtro.todos = true;

        //Se inician los calendarios y ambas fechas del filtro en la fecha actual
        var toDay = new Date();
        var hoy = moment(toDay).format('YYYY-MM-DD');
        $scope.tipo = [{id: 0, nombre: 'Todos'}, {id: 1, nombre: 'Cargos'}, {id: 2, nombre: 'Débitos'}];
        $scope.filtro.tipo = $scope.tipo[0];
        $scope.siteCrud = {
            url: 'clientes/balancexTipoDt/' + clientID + '/' + rContratoID,
            is_orderable: false,
            data: {
                dateStart: hoy,
                dateEnd: hoy,
                tipomovimiento: 'Todos',
                todos: true
            }
        };

        $scope.headerCrud = [
            {title: 'id', label: 'id', visible: false, orderable: false}, // 0      
            {title: 'Tipo Publicacion', label: 'tipo_publicacion.nombre', orderable: false}, // 1
            {title: 'Monto (Bs.)', label: 'monto', orderable: false, filter: 'moneySaldo'}, // 2
            {title: 'Cargo (Bs.)', label: 'cargo', filter: 'formatNumberMoney', orderable: false}, // 3
            {title: 'Débito (Bs.) ', label: 'debito', filter: 'formatNumberMoney', orderable: false}, // 4
            {title: 'Fecha', label: 'created_at', orderable: false, filter: 'date', filterParams: 'dd-MM-yyyy'} // 5
        ];


        $scope.operation = particularPermission.permission;

        /**
         * Objeto del calendario
         */
        $scope.calendar = {
            opened: {},
            now: new Date(),
            dateFormat: 'dd/MM/yyyy',
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        /**
         * Función que carga los datos del filtro y recarga la tabla
         */
        $scope.filtrar = function () {
            DTInstances.getLast().then(function (lastDTInstance) {
                $scope.setUpFilter();
                lastDTInstance.reloadData();
            });
        };
        ///**
        // * Función que gestiona la apertura de la pestaña de detalles de la operación del balance
        // * @param balanceID
        // * @param operation_id
        // */
        //$scope.detalleOperacion = function (balanceID, operation_id) {
        //    var tab = {
        //        id: operation_id,
        //        title: 'Detalle Operación #',
        //        state: 'admin.detalle_cliente.detalles_balance({balanceID:' + balanceID + '})',
        //        object_id: balanceID
        //    };
        //    $scope.buildTab(tab);
        //    $state.go('^.detalles_balance', {'balanceID': balanceID});
        //};
        ///**
        // * Función que gestiona la pestaña de observaciones de la operación
        // * @param balanceID
        // * @param operation_id
        // */
        //$scope.addObservacion = function (balanceID, operation_id) {
        //    var tab = {
        //        id: operation_id,
        //        title: 'Observaciones Operación #',
        //        state: 'admin.detalle_cliente.addobservacion_balance({balanceID:' + balanceID + '})',
        //        object_id: balanceID
        //    };
        //    $scope.buildTab(tab);
        //    $state.go('^.addobservacion_balance', {'balanceID': balanceID});
        //};

        //$scope.imprimir = function () {
        //    $scope.setUpFilter();
        //    window.open(appConfig.getPathApi() + 'clientes/balanceClienteDt/' + clientID + '?reporte=true&dateStart=' + $scope.siteCrud.data.dateStart + '&dateEnd=' + $scope.siteCrud.data.dateEnd + '&tipomovimiento=' + $scope.siteCrud.data.tipomovimiento + '&todos=' + $scope.siteCrud.data.todos);
        //};
        /**
         * Esta función organiza los parámetros de los filtros usados en DataTables y en el reporte
         */
        $scope.setUpFilter = function () {
            $scope.siteCrud.data.dateStart = moment($scope.filtro.fecha_desde).format('YYYY-MM-DD');
            //Si la fecha "Hasta" no esta definida se setea con la fecha actual
            if ($scope.filtro.fecha_hasta) {
                $scope.siteCrud.data.dateEnd = angular.isDefined(moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD') : moment(toDay).format('YYYY-MM-DD');
            } else {
                $scope.siteCrud.data.dateEnd = moment(toDay).format('YYYY-MM-DD');
            }
            if ($scope.filtro.fecha_desde) {
                $scope.siteCrud.data.dateStart = angular.isDefined(moment($scope.filtro.fecha_desde).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_desde).format('YYYY-MM-DD') : false;
            } else {
                $scope.siteCrud.data.dateStart = false;
            }
            $scope.siteCrud.data.tipomovimiento = $scope.filtro.tipo.nombre;
            $scope.siteCrud.data.todos = $scope.filtro.todos;
        }

    }]);