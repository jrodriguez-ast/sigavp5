'use strict';
/**
 * Plantilla para un controlador de Lista
 ***** Cambia las palabras que salen en mayúsculas. *****
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 13/11/15 19:18:08
 */
app.controller("TechosListController", [
    "$scope", 'rPermissions', 'clientID',
    function ($scope, rPermissions, clientID) {
        $scope.siteCrud = {url: 'clientes/' + clientID + '/techo_historial'};
        $scope.headerCrud = [
            {title: 'ID', label: 'id', visible: false},
            {title: 'Monto Máximo', label: 'techo'},
            {title: 'Activo', label: 'activo', filter: "si_no"},
            {title: 'Desde', label: 'created_at', filter: 'date'},
            {title: 'Hasta', label: 'updated_at', filter: 'date'}
        ];

        $scope.operation = rPermissions.permission;
    }]);

/**
 * Controlador que gestiona el techo de consumo.
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.1  15:43:32
 */
app.controller('TechoFormController', ['$scope', '$injector', "rModel", "$filter", "$modalInstance",
    function ($scope, $injector, rModel, $filter, $modalInstance) {

        // Inyectamos las dependencias a utilizar dentro del Controlador.
        var $validationProvider = $injector.get('$validation', 'TechoFormController');

        // Definimos el model, como el a utilizar en la vista
        $scope.model = rModel.data.data;
        $scope.model.techo_consumo = $filter('formatNumberMoney')($scope.model.techo_consumo);

        // Donde "formObject" es igual al atributo id de la etiqueta form
        // ejm: <form name="formName" id="formObject">
        $scope.formObject = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.formObject.send();
                });
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },

            /**
             * Prepara la data de techo de consumo a ser enviada al servidor.
             * @param  {json} object
             * @return {json}           data preparada.
             * @author Jose Rodriguez
             * @version V-1.0 16/06/15 17:25:32
             */
            prepare: function (object) {
                var _obj = angular.copy(object);
                _obj.techo = $filter('formatMoneyToNumber')(_obj.techo);
                return _obj;
            },

            /**
             * Enviar las Negociación al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodriguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                var model = $injector.get('clienteMdl', '$scope.formObject.send');
                model.updateTecho(
                    {clientID: rModel.id},
                    $scope.formObject.prepare($scope.model), // Objeto de modelo.
                    $scope.formObject.success,
                    $scope.formObject.errors
                );
            },

            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {
                // No evaluamos OK xq el servidor responde 200 siempre y este ok.
                if (response.status == 'ok') {
                    var flashMessages = $injector.get('flashMessages', 'success');
                    var msg = 'Monto Maximo de deuda permitido actualizado.';
                    flashMessages.show_success(msg);
                    $modalInstance.close();
                    $injector.get('$state', 'success').go('admin.detalle_cliente.techos', {
                        id: rModel.id
                    });
                }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (response) {
                var errors = response.data.errors;
                var mensajeToShow = [];

                // Indica los nombres de los campos que van a venir en la respuesta de validación
                var fields = ['techo'];

                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    for (var j = fields.length - 1; j >= 0; j--) {
                        if (angular.isDefined(message[fields[j]])) {
                            mensajeToShow = mensajeToShow.concat(message[fields[j]]);
                            fields.splice(j);
                            // @todo Investigar xq break rompe ambos ciclos.
                            //break;
                        }
                    }
                }

                var flashMessages = $injector.get('flashMessages', '$scope.formObject.errors');
                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        };
    }]);
/* End of file techoController.js */