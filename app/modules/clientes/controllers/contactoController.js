'use strict';

/**
 *
 */
app.controller('listarContactosCliente', [
    '$scope', 'clientID', 'particularPermission', '$injector',
    function ($scope, clientID, particularPermission, $injector) {
        var $state = $injector.get('$state', 'listarContactosCliente');
        var $modal = $injector.get('$modal', 'listarContactosCliente');
        var globalData = $injector.get('globalData', 'listarContactosCliente');
        var crudFactory = $injector.get('crudFactory', 'listarContactosCliente');

        $scope.title = "Contactos del Cliente";
        $scope.siteCrud = {url: 'clientes/' + clientID + '/contacto/dt'};
        $scope.headerCrud = [
            {title: 'ID', label: 'id', visible: false},
            {title: 'Nombre', label: 'nombre', visible: true},
            {title: 'Apellido', label: 'apellido', visible: true},
            {title: 'Teléfonos', label: 'telefonos', visible: true, filter: "listadoContMed"},
            {title: 'Correos', label: 'correos_electronicos', visible: true, filter: "listadoContMed"},
            {title: 'cargo_id', label: 'cargo_id', visible: false},
            {title: 'Interno', label: 'es_interno', visible: true, filter: "si_no"},
            {title: 'ContactoCliente_id', label: 'Cliente', visible: false},
            {title: 'Tipo Contacto', label: 'tipo', visible: true}
        ];
        $scope.operation = particularPermission.get.data.permission;

        $scope.newTabContactoCliente = function (contacto_id, operation_id) {
            globalData.data.ContactoClienteId = contacto_id;
            globalData.data.ClienteId = $state.params.id;
            var tab = {
                id: operation_id,
                title: 'Contacto #',
                state: 'admin.detalle_cliente.contacto_detail({idcont:' + contacto_id + '})',
                object_id: contacto_id
            };
            $scope.buildTab(tab);
            $state.go('^.contacto_detail', {'idcont': contacto_id});
        };

        /**
         * Función para abrir la pantalla de edición en un modal
         */
        $scope.agregarClienteContacto = function (contactoId) {
            var CONSTANTES = $injector.get('CONSTANTES', 'listarContactosCliente');
            var modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/clientes/views/clientes/contactos/editar.html?v=' + CONSTANTES.VERSION,
                controller: 'EditarContactoClienteController',
                size: 'lg',
                resolve: {
                    datos: function () {
                        return {
                            'clienteid': $state.params.id,
                            'contactoid': contactoId
                        };
                    }
                }
            });

            modalInstance.result.then(function () {
                crudFactory.reloadData();
            });
        };

        /**
         * Borrado de contactos.
         * Ejemplo de operación. (// 5500	Borrar Contacto	borrarClienteContacto	8	1	glyphicon glyphicon-trash			1012	grid	method_js	content	Detalles del contacto)
         * @author Jose Rodriguez, Amalio Velasquez
         * */
        $scope.borrarClienteContacto = function(contacto_id){
            var $ngBootbox = $injector.get('$ngBootbox','borrarClienteContacto');
            $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
                function(){
                    var ContactoCliente = $injector.get('ContactoCliente','confirm');
                    var flashMessages = $injector.get('flashMessages', 'confirm');
                    ContactoCliente.remove(
                        {
                            clienteId: clientID,
                            contactoId:contacto_id
                        },
                        function(success){
                            if(success.status == 'ok'){
                                crudFactory.reloadData();
                                flashMessages.show_success(success.message);
                            }
                        },
                    function(error){
                        crudFactory.reloadData();
                        flashMessages.show_error(error.data.message);
                        }
                    );
                }
                //, function(){}
            );
        }
    }
]);

/**
 *
 */
app.controller("DetallesContactoClienteController", ["$scope", "rContacto", "rTabId", "$state",
    function ($scope, rContacto, rTabId, $state) {
        $scope.detalles = rContacto.data;
        var tab = {
            id: rTabId,
            title: 'Contacto #',
            state: 'admin.detalle_cliente.contacto_detail({idcont:' + rContacto.data.id + '})',
            object_id: rContacto.data.id
        };
        $scope.buildTab(tab);
        $state.go('^.contacto_detail', {'idcont': rContacto.data.id});
    }
]);

/**
 *
 */
app.controller("CrearContactoClienteController", ["$scope", 'datos', 'r_cargos', '$injector',
    function ($scope, datos, r_cargos, $injector) {
        var $modal = $injector.get('$modal', 'AddContactoClienteCtrl');
        var $stateParams = $injector.get('$stateParams', 'AddContactoClienteCtrl');
        var flashMessages = $injector.get('flashMessages', 'AddContactoClienteCtrl');
        var ContactoCliente = $injector.get('ContactoCliente', 'AddContactoClienteCtrl');
        var $validationProvider = $injector.get('$validation', 'AddContactoClienteCtrl');

        $scope.detalles = {};
        $scope.detalles.nombre = '';
        $scope.detalles.apellido = '';
        $scope.detalles.telefonos = [{'a': ''}];
        $scope.detalles.correos_electronicos = [{'a': ''}];
        $scope.detalles.cargo_id = '';
        $scope.detalles.deleted_at = '';
        $scope.detalles.es_interno = '';
        $scope.detalles.id = '';
        $scope.detalles.Cliente_id = '';
        $scope.detalles.cargos = r_cargos;
        $scope.detalles.cargo = {};
        $scope.tiposContacto = [{id: 'administrativo', descripcion: 'Administrativo'}, {
            id: 'publicitario',
            descripcion: 'Publicitario'
        }, {id: 'otro tipo', descripcion: 'Otro tipo'}];

        //CargoMdl.get(null).$promise.then(function(respuesta){
        //     $scope.detalles.cargos = respuesta.data;
        //});

        $scope.contact = {};
        $scope.contact.nombre = '';
        $scope.contact.apellido = '';
        $scope.contact.telefonos = '';
        $scope.contact.correos_electronicos = '';
        $scope.contact.cliente_id = datos.clienteid;
        $scope.addTelefono = function () {
            $scope.detalles.telefonos.push({a: ''});
        };

        $scope.removeTelefono = function (index) {
            if ($scope.detalles.telefonos.length > 1) {
                $scope.detalles.telefonos.splice(index, 1)
            }
        };
        $scope.addCorreo = function () {
            $scope.detalles.correos_electronicos.push({a: ''});
        };
        $scope.removeCorreo = function (index) {
            if ($scope.detalles.correos_electronicos.length > 1) {
                $scope.detalles.correos_electronicos.splice(index, 1)
            }
        };
        $scope.cerrarCrear = function () {
            $modal.close();
        };

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodriguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                var $filter = $injector.get('$filter', 'send');
                $scope.contact.telefonos = $filter('json')($scope.detalles.telefonos, 0);
                $scope.contact.correos_electronicos = $filter('json')($scope.detalles.telefonos, 0);
                $scope.contact.nombre = $scope.detalles.nombre;
                $scope.contact.apellido = $scope.detalles.apellido;
                $scope.contact.telefonos = $filter('json')($scope.detalles.telefonos, 0);
                $scope.contact.correos_electronicos = $filter('json')($scope.detalles.correos_electronicos, 0);
                $scope.contact.Cliente_id = datos.Clienteid;
                $scope.contact.cargo_id = $scope.detalles.cargo.id;
                $scope.contact.tipo = $scope.tipo.id;
                $scope.contact.es_interno = $scope.detalles.es_interno;
                ContactoCliente.crear({clienteId: $stateParams.id},
                    $scope.contact,
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors);
            },
            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha creado el contacto.");
                $scope.$close(true);
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                    }

                    //if (angular.isDefined(message.nombre)) {
                    //    mensajeToShow = mensajeToShow.concat(message.nombre);
                    //}
                    //if (angular.isDefined(message.apellido)) {
                    //    mensajeToShow = mensajeToShow.concat(message.apellido);
                    //}
                    //if (angular.isDefined(message.cargo_id)) {
                    //    mensajeToShow = mensajeToShow.concat(message.cargo_id);
                    //}
                    //if (angular.isDefined(message.telefonos)) {
                    //    mensajeToShow = mensajeToShow.concat(message.telefonos);
                    //}
                    //if (angular.isDefined(message.correos_electronicos)) {
                    //    mensajeToShow = mensajeToShow.concat(message.correos_electronicos);
                    //}
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);

/**
 *
 */
app.controller("EditarContactoClienteController", ["$scope", 'datos', '$modalInstance', "$injector",
    function ($scope, datos, $modalInstance, $injector) {
        var CargoMdl = $injector.get('CargoMdl', 'EditContactoClienteCtrl');
        var flashMessages = $injector.get('flashMessages', 'EditContactoClienteCtrl');
        var ContactoCliente = $injector.get('ContactoCliente', 'EditContactoClienteCtrl');

        $scope.editedcontact = {};
        $scope.editedcontact.nombre = '';
        $scope.editedcontact.apellido = '';
        $scope.editedcontact.telefonos = '';
        $scope.editedcontact.correos_electronicos = '';
        $scope.editedcontact.deleted_at = '';
        $scope.editedcontact.es_interno = '';
        $scope.editedcontact.id = '';
        $scope.editedcontact.Cliente_id = '';
        $scope.tiposContacto = [{id: 'administrativo', descripcion: 'Administrativo'}, {
            id: 'publicitario',
            descripcion: 'Publicitario'
        }, {id: 'otro tipo', descripcion: 'Otro tipo'}];
        ContactoCliente.get({
            contactoId: datos.contactoid,
            clienteId: datos.clienteid
        }).$promise.then(function (respuesta) {
                $scope.editedcontact.cargo = {'id': respuesta.data.cargo_id};
                $scope.tipo = {'id': respuesta.data.tipo};
                $scope.detalles = respuesta.data;
                $scope.detalles.telefonos = angular.fromJson($scope.detalles.telefonos);
                $scope.detalles.correos_electronicos = angular.fromJson($scope.detalles.correos_electronicos);
            });

        $scope.editedcontact.cargos = CargoMdl.query();
        //$scope.detalles.cargo='{"id":47,"descripcion":"odio","deleted_at":null}';
        $scope.addTelefono = function () {
            $scope.detalles.telefonos.push({nombre: ''});
        };
        $scope.removeTelefono = function (index) {
            if ($scope.detalles.telefonos.length > 1) {
                $scope.detalles.telefonos.splice(index, 1)
            }
        };
        $scope.addCorreo = function () {
            $scope.detalles.correos_electronicos.push({nombre: ''});
        };
        $scope.removeCorreo = function (index) {
            if ($scope.detalles.correos_electronicos.length > 1) {
                $scope.detalles.correos_electronicos.splice(index, 1)
            }
        };

        var $validationProvider = $injector.get('$validation', 'EditarContactoClienteController');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                })
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodriguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                var $filter = $injector.get('$filter', 'EditContactoClienteCtrl');
                $scope.editedcontact.telefonos = $filter('json')($scope.detalles.telefonos, 0);
                $scope.editedcontact.correos_electronicos = $filter('json')($scope.detalles.telefonos, 0);
                $scope.editedcontact.nombre = $scope.detalles.nombre;
                $scope.editedcontact.apellido = $scope.detalles.apellido;
                $scope.editedcontact.telefonos = $filter('json')($scope.detalles.telefonos, 0);
                $scope.editedcontact.correos_electronicos = $filter('json')($scope.detalles.correos_electronicos, 0);
                $scope.editedcontact.cargo_id = $scope.editedcontact.cargo.id;
                $scope.editedcontact.deleted_at = $scope.detalles.deleted_at;
                $scope.editedcontact.es_interno = $scope.detalles.es_interno;
                $scope.editedcontact.id = $scope.detalles.id;
                $scope.editedcontact.Cliente_id = $scope.detalles.Cliente_id;
                $scope.editedcontact.tipo = $scope.tipo.id;
                ContactoCliente.update({
                    clienteId: datos.clienteid,
                    contactoId: datos.contactoid
                }, $scope.editedcontact).$promise.then(function () {
                        $modalInstance.close();
                        flashMessages.show_success("Se ha modificado el contacto");
                    }
                );
            },

            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha creado el contacto.");
                $scope.$close(true);
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.nombre)) {
                        mensajeToShow = mensajeToShow.concat(message.nombre);
                    }
                    if (angular.isDefined(message.apellido)) {
                        mensajeToShow = mensajeToShow.concat(message.apellido);
                    }
                    if (angular.isDefined(message.cargo_id)) {
                        mensajeToShow = mensajeToShow.concat(message.cargo_id);
                    }
                    if (angular.isDefined(message.telefonos)) {
                        mensajeToShow = mensajeToShow.concat(message.telefonos);
                    }
                    if (angular.isDefined(message.correos_electronicos)) {
                        mensajeToShow = mensajeToShow.concat(message.correos_electronicos);
                    }
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);

