'use strict';
/**
 *Controlador que gestiona el listado de clientes
 *
 * @author Jefferson Lara <jetox21@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 *
 */
app.controller("listClient", ["$scope", "rPermisos", "$location", "$injector", 
    function ($scope, rPermisos, $location, $injector) {

    var flashMessages = $injector.get('flashMessages', 'listClient');
    var $ngBootbox = $injector.get('$ngBootbox','listClient');
    var crudFactory = $injector.get('crudFactory', 'listClient');

    if (window.localStorage.cliente_id != 'false') {
        //console.log(window.localStorage.cliente_id, "LISTADO DE CLIENTES");
        $location.path('admin/detalle_cliente/' + localStorage.cliente_id + '/ficha');
    } else {
        $scope.title = "Clientes";
        $scope.siteCrud = {url: 'clientes/cliente/dt'};
        $scope.headerCrud = [
            {title: 'ID', label: 'id', visible: false},
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'Razón social', label: 'razon_social', visible: true},
            {title: 'Abreviatura', label: 'abreviatura', visible: false},
            {title: 'Nombre comercial', label: 'nombre_comercial', visible: false},
            {title: 'R.I.F.', label: 'rif', visible: true},
            {title: 'Teléfonos', label: 'telefonos', visible: true, filter: "listado"},
            {title: 'Email', label: 'correos_electronicos', visible: true, filter: "listado"}
        ];
        $scope.operation = rPermisos.get.data.permission;


        /**
         *
         * 
         * @author Amalio Velasquez
         * */
        $scope.borrarCliente = function(cliente_id){

            $ngBootbox.confirm("¿Esta seguro que desea borrar este registro? <br> <b><i class='glyphicon glyphicon-info-sign'></i> ¡Se perderán todos los contactos asociados a este</b>!").then(
                function(){
                    var Cliente = $injector.get('clienteMdl','confirm');

                    Cliente.remove(
                        {
                            id: cliente_id
                        },
                        function(success){
                            if(success.status == 'ok'){
                                crudFactory.reloadData();
                                flashMessages.show_success(success.message);
                            }
                        },
                    function(error){
                        crudFactory.reloadData();
                        flashMessages.show_error(error.data.message);
                        }
                    );
                }
                //, function(){}
            );
        }
    }

}]);

/**
 * Controlador que arma la pantalla de detalles del cliente y se encarga de cargar las tabs
 * @author Maykol Purica
 *         Jose Rodriguez
 * @version V-1.1 08/06/15 17:01:41
 */
app.controller("FichaClienteController", ["$scope", '$injector', "clientID", 'globalPermission', 'clientResolve', 'rLugar',
    function ($scope, $injector, clientID, globalPermission, clientResolve, rLugar) {
        var appConfig = $injector.get('appConfig', 'FichaClienteController');
        var $state = $injector.get('$state', 'FichaClienteController');
        var components = preparePermissionComponets(globalPermission.permission);

        $scope.tabs = components.tabs;
        $scope.buttons = components.buttons;
        $scope.detalles = clientResolve.data;
        $scope.lugar = rLugar;
        $scope.clienteid = clientID;

        $scope.xImagen = appConfig.getPathApi() + 'img/avatar.jpg';
        if ($scope.detalles.img_cliente) {
            var xImagen = appConfig.getPathApi() + 'storage/img_clientes/' + $scope.detalles.img_cliente;
            var $http = $injector.get('$http', 'imageverify');
            $http.get(xImagen).then(function () {
                $scope.xImagen = xImagen;
            });
        }

        $scope.setActiveTab = function(){
            var tabs = $scope.tabs;
            // Activamos el Tab.
            for (var i = tabs.length - 1; i >= 0; i--) {
                tabs[i].active = (tabs[i].state == $state.current.name);
            }
        };

        $scope.setActiveTab();

        /**
         * Prepara y retorna la data de permisos para tabs y botones de la ficha
         * de clientes.
         *
         * @param  {array} permission permisos dentro de la vista.
         * @return {{tabs: Array, buttons: Array}}
         * @version V-1.0 08/06/15 10:46:39
         * @author Jose Rodriguez
         */
        function preparePermissionComponets(permission) {
            var components = {tabs: [], buttons: []};

            // Obtenemos la url sin IDs
            for (var i = 0, len = permission.length; i < len; i++) {
                var p = permission[i], current = {
                    title: p._name, icon: p.icon, url: p.url,
                    state: p.url.split('/').join('.'),
                    // por defecto todas las pestañas inactivas
                    id:p.id, active: false
                };

                if (p.chk_render_on == 'tabpanel') {
                    components.tabs.push(current);
                } else if (p.chk_render_on == 'button') {
                    components.buttons.push(current);
                }
            }
            return components;
        }

        /**
         * [removeTab description]
         * @param  {[type]} tabIndex [description]
         * @return {[type]}          [description]
         */
        $scope.removeTab = function (tabIndex) {
            $scope.tabs.splice(tabIndex, 1);
            window.history.back();

            // Truco con timeout
            $injector.get('$timeout', 'removeTab')($scope.setActiveTab(), 150);
        };

        $scope.buildTab = function (tab) {
            if (!angular.isDefined(tab) && !angular.isDefined(tab.id) && !angular.isDefined(tab.state) && !angular.isDefined(tab.title) && !angular.isDefined(tab.object_id)) {
                //console.error('El objeto tab debe contener las siguientes posiciones:{id:1,state:"state.a.b",title:"title",object_id:"ID del Recurso Mostrado"}');
                return;
            }

            var tabs = $scope.tabs;
            var existeEnTabs = false, title = tab.title + tab.object_id;

            // Busco si el tab ya existe en el arreglo de tabs.
            // y Actualizo información
            for (var i = tabs.length - 1; i >= 0; i--) {
                tabs[i].active = false;
                if (tabs[i].id == tab.id) {
                    tabs[i].active = true;
                    tabs[i].title = title;
                    existeEnTabs = true;
                }
            }

            // Si no existe el tab lo agrego.
            if (!existeEnTabs) {
                tabs.push({
                    id: tab.id, title: title,
                    state: tab.state,
                    // Pestaña tab que se mostrara activa.
                    active: true, closable: true
                });
            }
        };

        $scope.editarImagen = function () {
            var $modal = $injector.get('$modal', 'editarImagen');
            var $modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/clientes/views/clientes/edit_img.html',
                controller: 'CambiarImagenController',
                size: 'lg',
                resolve: {
                    clientID: function () {
                        return clientID;
                    }
                }
            });
            $modalInstance.result.then(function () {
                $state.reload('admin.detalle_cliente');
            });
        };

        $scope.editarImagenMedio = function () {
            var $modal = $injector.get('$modal', 'editarImagen');
            var $modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/medios/views/medios/edit_imagen.html',
                controller: 'CambiarImagenMedioController',
                size: 'lg',
                resolve: {
                    clientID: function () {
                        return clientID;
                    }
                }
            });
            $modalInstance.result.then(function () {
                $state.reload('admin.detalle_medio');
            });
        };

    }]);

/**
 * Detalles de Cliente pestaña Ficha de Cliente
 * @author Maykol Purica
 *         Jose Rodriguez
 * @version V-1.1 08/06/15 17:01:41
 */
app.controller('DatosClienteController', ['$scope', 'clientResolve', function ($scope, clientResolve) {
    $scope.detalles = clientResolve.data;
    $scope.setActiveTab();

    /**
     * Convierte la fecha entregada por la solvencia laboral en un objeto js Date.
     * @param date
     * @returns {Date}
     * @author Jose Rodriguez
     * @version V-1.1 03/11/15 11:57:41
     */
    $scope.detalles.stringToDate = function (date) {
        var porciones = date.split('.');
        return new Date(porciones[2], porciones[1], porciones[0]);
    }
}]);

/**
 *
 */
app.controller("CrearClienteController", ["$scope", '$injector',
    function ($scope, $injector) {
        var $validationProvider = $injector.get('$validation', 'CrearClienteController');
        var flashMessages = $injector.get('flashMessages', 'CrearClienteController');
        var clienteMdl = $injector.get('clienteMdl', 'CrearClienteController');
        var $location = $injector.get('$location', 'CrearClienteController');

        $scope.detalles = {};
        $scope.detalles.telefonos = [''];
        $scope.detalles.correos_electronicos = [''];
        $scope.telefonos = [{nombre: ''}];
        $scope.correos = [{nombre: ''}];
        $scope.detalles.abreviatura = '';
        $scope.habilitado_razon_social = false;

        $scope.addTelefono = function () {
            $scope.telefonos.push({nombre: ''});
        };
        $scope.removeTelefono = function (index) {
            if ($scope.telefonos.length > 1) {
                $scope.telefonos.splice(index, 1)
            }
        };
        $scope.addCorreo = function () {
            $scope.correos.push({nombre: ''});
        };
        $scope.removeCorreo = function (index) {
            if ($scope.correos.length > 1) {
                $scope.correos.splice(index, 1)
            }
        };

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                })
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodriguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                angular.forEach($scope.telefonos, function (value, key) {
                        $scope.detalles.telefonos[key] = '"' + value['nombre'] + '"';
                    }
                );
                $scope.detalles.telefonos = "[" + $scope.detalles.telefonos + "]";
                angular.forEach($scope.correos, function (value, key) {
                        $scope.detalles.correos_electronicos[key] = '"' + value['nombre'] + '"';
                    }
                );
                $scope.detalles.correos_electronicos = "[" + $scope.detalles.correos_electronicos + "]";
                clienteMdl.save($scope.detalles, function (resp) {
                    $location.path("admin/detalle_cliente/" + resp.data.id);//
                });

                $scope.detalles.differentiation_chain = $scope.detalles.abreviatura;
            },


            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha creado el cliente.");
                $scope.$close(true);
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.telefonos)) {
                        mensajeToShow = mensajeToShow.concat(message.telefonos);
                    }
                    if (angular.isDefined(message.correos_electronicos)) {
                        mensajeToShow = mensajeToShow.concat(message.correos_electronicos);
                    }
                }
                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            },

            /**
             * Consulta de RIF
             * @param rif
             * @returns {boolean}
             * @author Jose Rodriguez
             * @version V-2.0 04/11/15 10:00:36
             */
            consulta: function (rif) {
                $scope.habilitado_razon_social = false;

                //J-00087626-6
                var pattern = /^[JGVEP][-][0-9]{8}[-][0-9]$/i;
                if (!pattern.test(rif)) {
                    flashMessages.show_error("Debe ingresar un RIF Valido.");
                    return false;
                }

                //Es necesario consultar si ya existe este rif en BD
                var Cliente = $injector.get('clienteMdl', 'consulta');
                Cliente.getbyrif({rif: rif}).$promise.then(function (response) {
                    $scope.respconsinterna = response;
                    if (response.status == 'ok') {
                        flashMessages.show_info("El Cliente ya esta registrado!!");
                        $location.path("admin/detalle_cliente/" + $scope.respconsinterna.data[0].id);
                    }
                });

                // Consulto RIF en servicio de Terceros
                var consultaRifModel = $injector.get('consultaRifModel', 'consulta');
                consultaRifModel.get({rif: rif}, function (response) {
                    $scope.respuestaconsulta = response;
                    var mensaje = "Datos encontrados.";

                    // Verificamos si el SENIAT respondió datos.
                    if (response.seniat.status == 'ok') {
                        $scope.detalles.razon_social = response.seniat.nombre;
                    } else if (response.rnc.status == 'ok') {
                        $scope.detalles.razon_social = response.rnc.razon_social;
                    } else if (response.solvencia != null && response.solvencia.status == 'ok') {
                        $scope.detalles.razon_social = response.solvencia.razon_social;
                    } else {
                        // Habilito campo Razón Social
                        $scope.habilitado_razon_social = true;
                        mensaje = "No se consiguieron los datos asociadas al RIF.";
                    }
                    flashMessages.show_info(mensaje);
                });
            }
        }

    }
]);

/**
 *
 */
app.controller("EditarClienteController", ["$scope", '$injector',
    function ($scope, $injector) {
        var $validationProvider = $injector.get('$validation', 'EditarClienteController');
        var flashMessages = $injector.get('flashMessages', 'EditarClienteController');
        var $stateParams = $injector.get('$stateParams', 'EditarClienteController');
        var clienteMdl = $injector.get('clienteMdl', 'EditarClienteController');
        var $location = $injector.get('$location', 'EditarClienteController');
        var $filter = $injector.get('$filter', 'EditarClienteController');

        // $scope.cliente = {};
        $scope.cliente = clienteMdl.get({id: $stateParams.id}, function (respuesta) {
            $scope.detalles = respuesta.data;
            $scope.fecha = moment($scope.detalles.fecha_fundacion, 'YYYY-MM-DD hh:mm:ss').toDate();
            $scope.telefonos = $filter('objcliente')($scope.detalles.telefonos);
            $scope.detalles.telefonos = [];
            $scope.correos = $filter('objcliente')($scope.detalles.correos_electronicos);
            $scope.detalles.correos_electronicos = [];
            $scope.rif = $scope.detalles.rif;
        });

        $scope.addTelefono = function () {
            $scope.telefonos.push({nombre: ''});
        };
        $scope.removeTelefono = function (index) {
            if ($scope.telefonos.length > 1) {
                $scope.telefonos.splice(index, 1)
            }
        };
        $scope.addCorreo = function () {
            $scope.correos.push({nombre: ''});
        };
        $scope.removeCorreo = function (index) {
            if ($scope.correos.length > 1) {
                $scope.correos.splice(index, 1)
            }
        };

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                })
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodriguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                angular.forEach($scope.telefonos, function (value, key) {
                        $scope.detalles.telefonos[key] = '"' + value['nombre'] + '"';
                    }
                );
                $scope.detalles.telefonos = "[" + $scope.detalles.telefonos + "]";
                angular.forEach($scope.correos, function (value, key) {
                        $scope.detalles.correos_electronicos[key] = '"' + value['nombre'] + '"';
                    }
                );
                $scope.detalles.correos_electronicos = "[" + $scope.detalles.correos_electronicos + "]";
                $scope.detalles.fecha_fundacion = $filter('date')($scope.fecha, 'yyyy-MM-dd');

                clienteMdl.update({id: $stateParams.id}, $scope.detalles).$promise.then(
                    $location.path("admin/detalle_cliente/" + $stateParams.id)
                );
            },


            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha actualizado el cliente.");
                $scope.$close(true);
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.telefonos)) {
                        mensajeToShow = mensajeToShow.concat(message.telefonos);
                    }
                    if (angular.isDefined(message.correos_electronicos)) {
                        mensajeToShow = mensajeToShow.concat(message.correos_electronicos);
                    }
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            },

            /**
             * Consulta de RIF
             * @param rif
             * @returns {boolean}
             * @author Jose Rodriguez
             * @version V-2.0 04/11/15 10:00:36
             */
            consulta: function (rif) {
                $scope.habilitado_razon_social = false;

                //J-00087626-6
                var pattern = /^[JGVEP][-][0-9]{8}[-][0-9]$/i;
                if (!pattern.test(rif)) {
                    flashMessages.show_error("Debe ingresar un RIF Valido.");
                    return false;
                }

                //Es necesario consultar si ya existe este rif en BD
                var Cliente = $injector.get('clienteMdl', 'consulta');
                Cliente.getbyrif({rif: rif}).$promise.then(function (response) {
                    $scope.respconsinterna = response;
                    if (response.status == 'ok') {
                        flashMessages.show_info("El Cliente ya esta registrado!!");
                        $location.path("admin/detalle_cliente/" + $scope.respconsinterna.data[0].id);
                    }
                });


                // Consulto RIF en servicio de Terceros
                var consultaRifModel = $injector.get('consultaRifModel', 'consulta');
                consultaRifModel.get({rif: rif}, function (response) {
                    $scope.respuestaconsulta = response;
                    var mensaje = "Datos encontrados.";

                    // Verificamos si el SENIAT respondió datos.
                    if (response.seniat.status == 'ok') {
                        $scope.detalles.razon_social = response.seniat.nombre;
                    } else if (response.rnc.status == 'ok') {
                        $scope.detalles.razon_social = response.rnc.razon_social;
                    } else if (response.solvencia != null && response.solvencia.status == 'ok') {
                        $scope.detalles.razon_social = response.solvencia.razon_social;
                    } else {
                        // Habilito campo Razón Social
                        $scope.habilitado_razon_social = true;
                        mensaje = "No se consiguieron los datos asociadas al RIF.";
                    }
                    flashMessages.show_info(mensaje);
                });
            }
        }
    }
]);

/**
 *
 */
app.controller("CambiarImagenController", [
    "$scope", '$injector', '$modalInstance', 'clientID',
    function ($scope, $injector, $modalInstance, clientID) {
        var clienteLogo = $injector.get('clienteLogo', 'CambiarImagenController');
        var clienteMdl = $injector.get('clienteMdl', 'CambiarImagenController');
        var $compile = $injector.get('$compile', 'CambiarImagenController');

        $scope.datos = {};
        $scope.clientID = clientID;

        $scope.process_file = function (file_up) {
            if (file_up.length > 0) {
                var file = file_up[0];
                $scope.datos.file = file_up;
                $scope.datos.file.progress = 0;
                var progressHtml = '<div class="progress" style="position: absolute;margin-left: 3%;width: 87%;margin-top: 5px;">' +
                    '<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{{datos.file.progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{datos.file.progress}}%">' +
                    '<span>Subiendo... ({{datos.file.progress}}%)</span>' +
                    '</div>' +
                    '</div>';
                // parseo el string append a html
                var element = angular.element(progressHtml);
                // compilamos el elemento parseado dentro del $scope
                var html = $compile(element)($scope);
                // agregamos el html del progressbar
                angular.element('#content-upload').html(html);
                // ejecutamos la subida del archivo seleccionado
                clienteLogo.upload_logo(file).progress(function (evt) {

                    file.progress = parseInt(100.0 * evt.loaded / evt.total);

                }).success(function (data) {

                    var icon = 'glyphicon glyphicon-ok-sign';

                    if (!data.success)
                        icon = 'glyphicon glyphicon-warning-sign';

                    if (data.success) {
                        //$scope.name_adjunto = data.file_name;
                        $scope.datos.file_name = data.file_name
                    }

                    // file is uploaded successfully
                    var suceesHtml = '<div style="text-align: left; position: absolute;margin-left: 3%;width: 87%;margin-top: 5px;">' +
                        '<span class="' + icon + '">' + data.message + '</span>' +
                        '</div>';
                    // parseo el string append a html
                    var ele = angular.element(suceesHtml);
                    // compilamos el elemento parseado dentro del $scope
                    var append = $compile(ele)($scope);
                    // agregamos el html
                    angular.element('#content-upload').html(append);
                    //console.log('file ' + config.file.name + 'is uploaded successfully. Response: ' + data);
                })
            }
        };

        $scope.guardar = function (filename) {
            var logo = {file_name: filename};
            //console.log(logo);
            clienteMdl.moverLogo({clientID: $scope.clientID}, logo).$promise.then(function () {
                $modalInstance.close();
            });

        };
    }
]);