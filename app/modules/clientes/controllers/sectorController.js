'use strict';
/**
 *Controlador que gestiona el listado de sectores
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 */
app.controller("sectorList", ["$scope", "permission", 'appConfig', function ($scope, permission, appConfig) {
    $scope.title = "Sectores";

    $scope.siteCrud = {
        url: 'clientes/sector/sectors',
        pathApi: appConfig.getAdminPathApi(),
        httpMethod: 'POST'
    };

    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {title: 'Nombre', label: 'nombre', visible: true}
    ];

    $scope.operation = permission.data.get.data.permission;
}]);

/**
 * Controlador que gestiona la creacion de sectores
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 */
app.controller('sectorAdmin', ["$scope", "$location", "$http", "$stateParams", "genericApiServices", "flashMessages", "$injector", "sectorModel", function ($scope, $location, $http, $stateParams, genericApiServices, flashMessages, $injector, sectorModel) {

    if (angular.isDefined($stateParams.id))
        $scope.sector = sectorModel.data.object;

    //inicializamos la validacion del form
    var $validationProvider = $injector.get('$validation');

    // Donde "type_form" es igual al atributo id de la etiqueta form
    // ejm: <form name="TypeForm" id="type_form">
    $scope.sector_form = {
        checkValid: $validationProvider.checkValid,
        submit: function (form) {
            $validationProvider.validate(form)
                .success($scope.success);
            //.error($scope.error);
        },
        reset: function (form) {
            $validationProvider.reset(form);
        }
    };

    //fin de la validacion del form

    /**
     * Valida y envia la data al servidor para su guardado.
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.0 22/01/15 10:31:05
     */
    $scope.success = function () {
        var method = (angular.isDefined($stateParams.id)) ? 'edit' : 'add';
        var _url = 'clientes/sector/' + method, object = {};
        // Donde Object es el Objecto del formulario, ne este caso TypeProduct.
        object = prepare($scope.sector);
        genericApiServices.runPostServices(_url, object).success(function (data) {
            if (data.error) { //Fracaso
                flashMessages.show_error(data.msg);
            } else { //Exito
                if (method == 'edit') {
                    flashMessages.show_success("Sector editado con éxito");
                }
                else {
                    flashMessages.show_success("Sector creado con éxito");
                }
                $location.path("admin/sectors");
            }
        });
    };

    /**
     * Prepara la data de los Tipos sectores para ser enviada al controlador
     * @param  {object}
     * @return {object}
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.0  22/01/15 10:33:29
     */
    function prepare(_sector) {
        return angular.copy(_sector);
    };
}]);