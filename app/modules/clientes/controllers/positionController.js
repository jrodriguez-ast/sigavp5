'use strict';
/**
 *Controlador que gestiona el listado de Productos
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 */
app.controller("positionList", ["$scope", "permission", 'appConfig',  '$injector', 
    function ($scope, permission, appConfig, $injector) {
    var flashMessages = $injector.get('flashMessages', 'positionModel');
    var $ngBootbox = $injector.get('$ngBootbox','positionModel');
    var crudFactory = $injector.get('crudFactory', 'positionModel');

    $scope.title = "Cargos";
    $scope.siteCrud = {
        url: 'clientes/position/positions',
        pathApi: appConfig.getAdminPathApi(),
        httpMethod: 'POST'
    };

    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {title: 'Nombre', label: 'descripcion', visible: true}
    ];

    $scope.operation = permission.data.get.data.permission;

    /**
     *
     *
     * @author Amalio Velasquez
     * */
    $scope.borrarCargo = function(id){

        $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
            function(){

                var modelo = $injector.get('positionModel','confirm');

                modelo.delete(
                    {
                        id: id
                    }
                ).then(
                    function (response) {
                        if (response.data.status === 'ok') {
                            flashMessages.show_success(response.data.message);
                            crudFactory.reloadData();
                        } else if (response.data.status === 'fail') {
                            flashMessages.show_error(response.data.message);
                            crudFactory.reloadData();
                        }

                    },
                    function (response) {
                       flashMessages.show_error(response.data.message);
                       crudFactory.reloadData();
                    }
                )
            }
        );
    }

}]);

/**
 * Controlador que gestiona la creación de Productos
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 */
app.controller('positionAdmin', ["$scope", "$injector", "rPosition", function ($scope, $injector,rPosition) {
    var $stateParams = $injector.get('$stateParams', 'positionAdmin');

    if (angular.isDefined($stateParams.id))
        $scope.position = rPosition.position;
        //$scope.position = rPosition.data.position;

    //inicializamos la validación del form
    var $validationProvider = $injector.get('$validation', 'positionAdmin');

    // Donde "type_form" es igual al atributo id de la etiqueta form
    // ejm: <form name="TypeForm" id="type_form">
    $scope.position_form = {
        checkValid: $validationProvider.checkValid,
        submit: function (form) {
            $validationProvider.validate(form)
                .success($scope.success);
            //.error($scope.error);
        },
        reset: function (form) {
            $validationProvider.reset(form);
        }
    };

    //fin de la validación del form

    /**
     * Valida y envía la data al servidor para su guardado.
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.0 22/01/15 10:31:05
     */
    $scope.success = function () {
        var method = (angular.isDefined($stateParams.id)) ? 'edit' : 'add';
        var _url = 'clientes/position/' + method;

        // Donde Object es el Objeto del formulario, ne este caso TypeProduct.
        var object = prepare($scope.position);
        $injector.get('genericApiServices', 'positionAdmin')
            .runPostServices(_url, object)
            .success(function (data) {
                var flashMessages = $injector.get('flashMessages', 'positionAdmin');
                if (data.error) { //Fracaso
                    flashMessages.show_error(data.msg);
                } else { //Exito
                    if (method == 'edit') {
                        flashMessages.show_success("Cargo editado con éxito");
                    }
                    else {
                        flashMessages.show_success("Cargo creado con éxito");
                    }
                    $injector.get('$location', 'positionAdmin').path("admin/positions");
                }
            });
    };

    /**
     * Prepara la data de los Tipos productos para ser enviada al controlador
     * @return {object}
     * @author Jose Rodriguez <josearodrigueze@gmail.com>
     * @version V-1.0  22/01/15 10:33:29
     * @param _position
     */
    function prepare(_position) {
        return angular.copy(_position);
    }
}]);