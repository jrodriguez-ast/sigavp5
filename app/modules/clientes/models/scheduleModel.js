'use strict';

/**
 *Modelo encargado de la manipulacion de la data de cliente
 *
 * @author Jefferson Lara <jetox21@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 *
 */

app.factory("scheduleModel", ['$http', 'appConfig', '$q', 'flashMessages', "genericApiServices", function ($http, appConfig, $q, flashMessages, genericApiServices) {

    return ({
        data: [],
        get: function (id) {
            var defer = $q.defer(), data = this.data;

            var _url2 = 'clientes/schedule/get', params = {id: id};
            genericApiServices.runGetServices(_url2, params).success(function (response) {
                data.object = response;
                defer.resolve(data);
            });
            return defer.promise;
        },
        /**
         * Retorna todos los datos relacionados a horarios de disponibilidad
         * @return {*}
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 27/01/15
         */
        getAll: function () {
            var url = appConfig.getPathApi() + 'clientes/schedule/schedules';
            return $http.get(url).then(
                function (response) {
                    return response.data;
                }
            )
        }
    });
}]);
