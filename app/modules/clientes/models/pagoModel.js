'use strict';

/**
 * Created by Carlos Blanco.
 */
app.factory('Pagos', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'clientes/cliente/:clienteID/pagos', null, {
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'clientes/cliente/:clienteID/pagos',
                params: {
                    clienteID: '@clienteID'
                }

            }

        });
    }
]);