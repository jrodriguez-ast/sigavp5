'use strict';

/**
 *Modelo encargado de la manipulación de la data de cliente
 *
 * @author Jefferson Lara <jetox21@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 *
 */
app.factory("positionModel", ['$http', 'appConfig', '$q', 'flashMessages', "genericApiServices",
    function ($http, appConfig, $q, flashMessages, genericApiServices) {
        return ({
            data: [],
            get: function (id) {
                var defer = $q.defer(), data = this.data;

                var _url2 = 'clientes/position/get', params = {id: id};
                genericApiServices.runGetServices(_url2, params).success(function (response) {
                    data.position = response;
                    defer.resolve(data);
                });
                return defer.promise;
            },
            /**
             * Retorna todos los datos relacionados al cargo
             * @return {*}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             * @version V1.0 27/01/15
             */
            getAll: function () {
                var url = appConfig.getPathApi() + 'clientes/position/positions';
                return $http.get(url).then(
                    function (response) {
                        return response.data;
                    }
                )
            },
            delete: function (obj) {
                var id=obj.id

                var url = appConfig.getAdminPathApi() + 'clientes/position/positions/'+id, params = {id: id};
                return $http.delete(url);
            }
        });
    }]);