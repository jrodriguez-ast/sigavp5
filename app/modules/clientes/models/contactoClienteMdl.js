'use strict';

/**
 * Created by Maykol Purica.
 */
app.factory('ContactoCliente', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'clientes/cliente/:clienteId/contacto/:contactoId', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'clientes/cliente/:clienteId/contacto/:contactoId',
                params: {
                    clienteId: '@clienteId',
                    contactoId: '@contactoId'
                },
                isArray: false
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'clientes/cliente/:clienteId/contacto',
                params: {
                    clienteId: '@clienteId'
                },
                isArray: false
            },
            delete: {
                method: 'DELETE'

            }
        });
    }
]);