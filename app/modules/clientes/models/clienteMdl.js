'use strict';

app.factory('clienteMdl', ['$resource', 'appConfig', function ($resource, appConfig) {
    var _url = appConfig.getPathApi() + 'clientes/cliente/:id';
    return $resource(_url, null, {
        update: {
            method: 'PUT'
            //params: {id: "@_id"},
            //url: appConfig.getPathApi() + 'clientes/cliente/:id'
        },
        referencia: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/referencia/:texto',
            params: {texto: '@_texto'}
        },
        data: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/data/:id',
            params: {id: '@_id'}
        },
        get: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/cliente/:id',
            params: {id: '@id'}
        },
        saldo: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/:id/saldo/:contratoID',
            params: {id: '@id', contratoID: '@contratoID'}
        },
        saldoxtipo: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/:id/saldoxtipo/:contratoID',
            params: {id: '@id', contratoID: '@contratoID'}
        },
        getbyrif: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/rif/:rif',
            params: {
                rif: '@rif'
            },
            isArray: false
        },
        moverLogo: {
            method: 'POST',
            url: appConfig.getPathApi() + 'clientes/moverLogo/:clientID',
            params: {
                clientID: '@clientID'
            }
        },
        list: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/cliente',
            isArray: true
        },
        updateTecho: {
            method: 'PUT',
            url: appConfig.getPathApi() + 'clientes/:clientID/update_techo',
            params: {clientID: '@clientID'}
        },
        delete: {
            method: 'DELETE'
        }
    });
}]);

/**
 * Factory que gestiona la subida de archivos.
 */
app.factory('clienteLogo', ["appConfig", "$upload", function (appConfig, $upload) {
    return {
        save: function () {
        },
        upload_logo: function (file) {
            var _url = appConfig.getPathApi() + 'clientes/guardarLogo';
            return $upload.upload({
                url: _url,
                headers: {'Content-Type': file.type},
                method: 'POST',
                file: file
                //}).error(function (success, error, progress) {
                //console.log(success, error, progress);
            });
        }
    };
}]);