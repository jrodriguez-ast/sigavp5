'use strict';
/**
 *Modelo encargado de la manipulacion de la data de cliente
 *
 * @author Jefferson Lara <jetox21@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 *
 */

app.factory("data_source_client", ['$http', 'appConfig', '$q', 'flashMessages', function ($http, appConfig, $q, flashMessages) {

    return ({
        data: {},
        resolve_client: function resolve_client() {

            var _url = appConfig.getPathApi() + 'clientes/cliente/store_client';
            var defer = $q.defer();
            var data = this.data;
            $http.get(_url).
                then(
                function success(response) {
                    data.get = response;
                    defer.resolve(data);
                },
                function error(reason) {
                    return false;
                }
            );
            return defer.promise;

        },
        info_dni: function info_dni(type, dni) {

            var url = 'clientes/cliente/verifica_dni'
            return $http({
                method: 'get',
                url: appConfig.getPathApi() + url,
                params: {
                    type: type,
                    dni: dni
                }
                //cache: $templateCache
            }).success(function (data) {

            }).error(function (data, status, headers, config) {

            });
        },
        create_client: function create_client(param) {

            var url = 'clientes/cliente/add_client';

            return $http({
                method: 'post',
                url: appConfig.getPathApi() + url,
                data: param
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                } else {
                    flashMessages.show_error(data.messages);
                }


            }).error(function (data, status, headers, config) {

                var msjText = '';
                msjText = 'Ha ocurrido un error..!!' + ' ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });
        },
        edit_client: function edit_client(param) {

            var url = 'clientes/cliente/edit_client';

            return $http({
                method: 'post',
                url: appConfig.getPathApi() + url,
                data: param
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                } else {
                    flashMessages.show_error(data.messages);
                }


            }).error(function (data, status, headers, config) {

                var msjText = '';
                msjText = 'Ha ocurrido un error..!!' + ' ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });
        },
        create_list_contact: function create_list_contact(_params) {

            var url = 'clientes/cliente/create_contact';

            return $http({
                method: 'post',
                url: appConfig.getPathApi() + url,
                data: _params
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                } else {
                    flashMessages.show_error(data.messages);
                }


            }).error(function (data, status, headers, config) {

                var msjText = '';
                msjText = 'Ha ocurrido un error..!!' + ' ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });

        },
        resolve_edit_client: function resolve_edit_client(params) {

            var _url = appConfig.getPathApi() + 'clientes/cliente/store_client?id=' + params;
            var defer = $q.defer();
            var data = this.data;
            $http.get(_url).
                then(
                function success(response) {
                    data.get = response;
                    defer.resolve(data);
                },
                function error(reason) {
                    return false;
                }
            );
            return defer.promise;

        },
        edit_list_contact: function edit_list_contact(_params) {

            var url = 'clientes/cliente/edit_contact';

            return $http({
                method: 'post',
                url: appConfig.getPathApi() + url,
                data: _params
                //cache: $templateCache
            }).success(function (data) {
                if (data.success) {
                    flashMessages.show_success(data.messages);
                } else {
                    flashMessages.show_error(data.messages);
                }


            }).error(function (data, status, headers, config) {

                var msjText = '';
                msjText = 'Ha ocurrido un error..!!' + ' ( "Error' + status + '" )';
                flashMessages.show_error(msjText);

            });

        },
        eliminarContacto: function (id) {
            var url = appConfig.getPathApi() + 'clientes/cliente/delete_contact';

            return $http.get(url, {params: {id: id}}).then(
                function (response) {
                    return response.data;
                }
            );
        },
        /**
         * Obtiene los contactos asociados al cliente
         * @param id Identificador del cliente
         * @returns {*}
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 11/02/15
         */
        getContactos: function (id) {
            var url = appConfig.getPathApi() + 'clientes/cliente/contactos/' + id;

            return $http.get(url).then(
                function (response) {
                    return response.data;
                }
            );
        }


    });

}]);
