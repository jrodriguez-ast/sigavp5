'use strict';

app.factory('balanceModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        var _url = appConfig.getPathApi() + 'clientes/cliente/:clienteID/balance';
        var data = $resource(_url, null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'clientes/balance/:balanceID',
                isArray: false,
                params: {balanceID: '@balanceID'}
            },
            detalleBalance: {
                method: 'GET',
                url: appConfig.getPathApi() + 'clientes/balance/:balanceID',
                params: {balanceID: '@balanceID'}
            },
        });
        return data;
    }

]);