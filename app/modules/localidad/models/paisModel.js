'use strict';

/**
 * Created by Carlos BLanco.
 */
app.factory('paisModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'localidad/pais/:id', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'localidad/pais/:id',
                params: {
                    id: '@id'
                },
                isArray: false
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'localidad/pais',
                isArray: false
            },
            delete: {
                method: 'DELETE'
            }
        });
    }
]);