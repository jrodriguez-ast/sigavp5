'use strict';

/**
 * Created by Carlos BLanco.
 */
app.factory('estadoModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'localidad/estado/:id', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'localidad/estado/:id',
                params: {
                    id: '@id'
                },
                isArray: false
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'localidad/estado',
                isArray: false
            },
            delete: {
                method: 'DELETE'
            }
        });
    }
]);