'use strict';

/**
 * Created by Carlos BLanco.
 */
app.factory('municipioModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'localidad/municipio/:id', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'localidad/municipio/:id',
                params: {
                    id: '@id'
                },
                isArray: false
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'localidad/municipio',
                isArray: false
            },
            delete: {
                method: 'DELETE'
            }
        });
    }
]);