'use strict';


/**
 * Provee el listado de facturas en el estatus entregada al cliente
 *
 * @author Carlos blanco <cebs923@gmail.com>
 * @package SIGAVP
 * @version V-1.0 07/09/2015
 */
app.controller('EstadoCtrlC', [
    '$scope',
    'particularPermission', '$injector',
    function
        ($scope,
         particularPermission, $injector) {
        var flashMessages = $injector.get('flashMessages', 'EstadoCtrlC');
        var $ngBootbox = $injector.get('$ngBootbox','EstadoCtrlC');
        var crudFactory = $injector.get('crudFactory', 'EstadoCtrlC');

        $scope.title = 'Estados';
        $scope.siteCrud = {url: 'localidad/estado_dt'};

        $scope.headerCrud = [

            {title: 'Nombre', label: 'nombre', visible: true}
            //{title: 'ID', label: 'id', visible: true}
        ];

        $scope.operation = particularPermission.permission;

        /**
         *
         *
         * @author Amalio Velasquez
         * */
        $scope.borrarEstado = function(id){

            $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
                function(){
                    var estado = $injector.get('estadoModel','confirm');

                    estado.remove(
                        {
                            id: id
                        },
                        function(success){
                            if(success.status == 'ok'){
                                crudFactory.reloadData();
                                flashMessages.show_success(success.message);
                            }
                        },
                    function(error){
                        crudFactory.reloadData();
                        flashMessages.show_error(error.data.message);
                        }
                    );
                }
                //, function(){}
            );
        }
    }

]);


app.controller("CrearEstadoController", [
    "$scope",
    "$stateParams",
    'flashMessages',
    '$injector',
    'estadoModel',
    '$state',
    'r_pais',
    function ($scope,
              $stateParams,
              flashMessages,
              $injector,
              estadoModel,
              $state,
              r_pais) {

        $scope.detalles = {};
        $scope.detalles.paises = r_pais.data;
        $scope.detalles.pais = {};
        $scope.detalles.nombre = '';
        /*
         Objeto que se enviara al Backend
         */

        $scope.contact = {};
        $scope.contact.nombre = '';


        var $validationProvider = $injector.get('$validation');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y resive las respuestas pertinentes.
             *
             * @author Jose Rodriugez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                $scope.contact.nombre = $scope.detalles.nombre;
                $scope.contact.pais_id = $scope.detalles.pais.id;
                estadoModel.crear(
                    $scope.contact,
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors);
            },
            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando todo esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha creado el estado.");
                $state.go('admin.estado');
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                    }

                    //if (angular.isDefined(message.nombre)) {
                    //    mensajeToShow = mensajeToShow.concat(message.nombre);
                    //}
                    //if (angular.isDefined(message.apellido)) {
                    //    mensajeToShow = mensajeToShow.concat(message.apellido);
                    //}
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);


app.controller('EditarEstadoController', [
    "$scope",
    "$stateParams",
    "$injector",
    "r_data",
    "estadoID",
    'estadoModel',
    'paisModel',
    'r_pais',
    function ($scope,
              $stateParams,
              $injector,
              r_data,
              estadoID,
              estadoModel,
              paisModel,
              r_pais) {

        var $validationProvider = $injector.get('$validation');

        $scope.editedcontact = {};
        $scope.detalles = r_data.data;
        $scope.editedcontact.pais = {'id': $scope.detalles.pais_id};
        $scope.editedcontact.paises = r_pais.data;


        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },

            prepare: function (object) {
                object = angular.copy(object);
                return object;
            },

            send: function () {
                var estadoModel = $injector.get('estadoModel');
                var flashMessages = $injector.get('flashMessages');
                var $location = $injector.get('$location');
                $scope.editedcontact.pais_id = $scope.editedcontact.pais.id;
                $scope.editedcontact.nombre = $scope.detalles.nombre;
                estadoModel.update({id: estadoID}, $scope.editedcontact).$promise.then(function (response) {
                        flashMessages.show_success("Se ha modificado el Estado");
                        $location.path("admin/estado");
                    }
                );
            }

        }
    }
]);


