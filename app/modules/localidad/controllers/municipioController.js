'use strict';


/**
 * Provee el listado de facturas en el estatus entregada al cliente
 *
 * @author Carlos blanco <cebs923@gmail.com>
 * @package SIGAVP
 * @version V-1.0 07/09/2015
 */
app.controller('MunicipioCtrlC', [
    '$scope',
    'particularPermission', '$injector',
    function
        ($scope,
         particularPermission, $injector) {
        var flashMessages = $injector.get('flashMessages', 'MunicipioCtrlC');
        var $ngBootbox = $injector.get('$ngBootbox','MunicipioCtrlC');
        var crudFactory = $injector.get('crudFactory', 'MunicipioCtrlC');

        $scope.title = 'Municipios';
        $scope.siteCrud = {url: 'localidad/municipio_dt'};

        $scope.headerCrud = [

            {title: 'Nombre', label: 'nombre', visible: true}
          //  {title: 'ID', label: 'id', visible: true}
        ];

        $scope.operation = particularPermission.permission;

        /**
         *
         *
         * @author Amalio Velasquez
         * */
        $scope.borrarMunicipio = function(id){

            $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
                function(){
                    var municipio = $injector.get('municipioModel','confirm');

                    municipio.remove(
                        {
                            id: id
                        },
                        function(success){
                            if(success.status == 'ok'){
                                crudFactory.reloadData();
                                flashMessages.show_success(success.message);
                            }
                        },
                    function(error){
                        crudFactory.reloadData();
                        flashMessages.show_error(error.data.message);
                        }
                    );
                }
                //, function(){}
            );
        }
    }

]);


app.controller("CrearMunicipioController", [
    "$scope",
    "$stateParams",
    'flashMessages',
    '$injector',
    'municipioModel',
    '$state',
    'r_estado',
    function ($scope,
              $stateParams,
              flashMessages,
              $injector,
              municipioModel,
              $state,
              r_estado) {

        $scope.detalles = {};
        $scope.detalles.estados = r_estado.data;
        $scope.detalles.estado = {};
        $scope.detalles.nombre = '';


        /*
         Objeto que se enviara al Backend
         */

        $scope.contact = {};
        $scope.contact.nombre = '';


        var $validationProvider = $injector.get('$validation');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y resive las respuestas pertinentes.
             *
             * @author Jose Rodriugez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                $scope.contact.nombre = $scope.detalles.nombre;
                $scope.contact.estado_id = $scope.detalles.estado.id;
                municipioModel.crear(
                    $scope.contact,
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors);
            },
            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando todo esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha creado el Municipio.");
                $state.go('admin.municipio');
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                    }

                    //if (angular.isDefined(message.nombre)) {
                    //    mensajeToShow = mensajeToShow.concat(message.nombre);
                    //}
                    //if (angular.isDefined(message.apellido)) {
                    //    mensajeToShow = mensajeToShow.concat(message.apellido);
                    //}
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);


app.controller('EditarMunicipioController', [
    "$scope",
    "$stateParams",
    "$injector",
    "r_data",
    "municipioID",
    'r_estado',
    function ($scope,
              $stateParams,
              $injector,
              r_data,
              municipioID,
              r_estado) {

        var $validationProvider = $injector.get('$validation');

        $scope.editedcontact = {};
        $scope.detalles = r_data.data;
        $scope.editedcontact.estado = {'id': r_data.data.estado_id};
        $scope.editedcontact.estados = r_estado.data;

        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },

            send: function () {
                var municipioModel = $injector.get('municipioModel');
                var flashMessages = $injector.get('flashMessages');
                var $location = $injector.get('$location');
                $scope.editedcontact.estado_id = $scope.editedcontact.estado.id;
                $scope.editedcontact.nombre = $scope.detalles.nombre;

                municipioModel.update({id: municipioID}, $scope.editedcontact).$promise.then(function (response) {
                        flashMessages.show_success("Se ha modificado el Municipio");
                        $location.path("admin/municipio");
                    }
                );
            }

        }
    }
]);


