'use strict';

app.factory('facturaModel', ['$resource', 'appConfig', function ($resource, appConfig) {
    var _url = appConfig.getPathApi() + 'clientes/contratos/:id';
    return $resource(_url, null, {
        update: {
            method: 'PUT'
        },
        crear: {
            method: 'POST',
            url: appConfig.getPathApi() + 'clientes/cliente/facturar',
            isArray: false
        },
        crear2: {
            method: 'POST',
            url: appConfig.getPathApi() + 'clientes/cliente/facturarcuota',
            isArray: false
        },
        anularfacturacuota: {
            method: 'PUT',
            url: appConfig.getPathApi() + 'clientes/cliente/anularfacturacuota/:facturaID',
            params: {
                facturaID: '@facturaID'
            },
            isArray: false
        },
        crearnotaCredito: {
            method: 'PUT',
            url: appConfig.getPathApi() + 'clientes/cliente/crearnotaCredito/:facturaID/:cliente_id',
            params: {
                facturaID: '@facturaID',
                cliente_id: '@cliente_id'
            },
            isArray: false
        },
        anularfacturapublicacion: {
            method: 'PUT',
            url: appConfig.getPathApi() + 'clientes/cliente/anularfacturapublicacion/:facturaID',
            params: {
                facturaID: '@facturaID'
            },

            isArray: false
        },
        getTotal: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/:client/total',
            params: {
                client: '@client'
            },
            isArray: false
        },
        printFacturaPDF: {
            method: 'GET',
            url: appConfig.getPathApi() + 'pdf/print/:modelo/:modelo_id',
            params: {
                modelo: '@modelo',
                modelo_id: '@modelo_id'
            },
            isArray: false
        },
        showFactura: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/:clienteID/facturas/:facturaID',
            params: {
                facturaID: '@facturaID',
                clienteID: '@clienteID'
            },
            isArray: false
        },
        showNota: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/:clienteID/nota/:notaID',
            params: {
                facturaID: '@facturaID',
                notaID: '@notaID'
            },
            isArray: false
        },
        changeStatus: {
            method: 'PUT',
            url: appConfig.getPathApi() + 'clientes/facturas/:facturaID/:estado',
            params: {
                facturaID: '@facturaID',
                estado: '@estado'
            },
            isArray: false
        },
        changeStatusnota: {
            method: 'PUT',
            url: appConfig.getPathApi() + 'clientes/nota/:notaID/:estado',
            params: {
                notaID: '@notaID',
                estado: '@estado'
            },
            isArray: false
        },
        getClienteId: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/getClienteId/:facturaID',
            params: {
                facturaID: '@facturaID'
            },
            isArray: false
        },
        datos_factura_nota: {
            method: 'GET',
            url: appConfig.getPathApi() + 'clientes/:clienteID/facturas/:facturaID',
            params: {
                clienteID: '@clienteID',
                facturaID: '@facturaID'

            },
            isArray: false
        },
        dalete: {
            method: 'DELETE'
        }
    });
}]);