'use strict';

/**
 * Created by darwin on 08/01/15.
 */

/**
 * Servicio para interacción de con el REST de facturas
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0
 */
app.factory('facturasMdl', [
    "$http", "appConfig", "$log", "flashMessages",
    function ($http, appConfig, $log, flashMessages) {
        /*
         URL base a consumir para realizar las transacciones
         */
        var _url = appConfig.getPathApi() + "facturas/factura";

        var _get = function (_url, params) {
            return $http.get(_url, params).then(onSuccess, onError);
        };

        var _post = function (_url, data) {
            return $http.post(_url, data).then(onSuccess, onError)
        };

        var _put = function (_url, data) {
            return $http.put(_url, data).then(onSuccess, onError)
        };

        var _delete = function (_url) {
            return $http.delete(_url).then(onSuccess, onError)
        };


        var onSuccess = function (response) {
            return response.data;
        };

        var onError = function (response) {
            $log.error(response);
            flashMessages.show_error('Ocurrió un error al realizar la solicitud de datos.');
        };


        return {
            /**
             * Ejecuta en el servidor la instrucción para crear una factura
             * @param clienteId
             * @returns {*}
             */
            crearFactura: function (cotizacionId) {
                var _thisUrl = _url + "/crear";

                var datos = {
                    id: cotizacionId
                };

                return _post(_thisUrl, datos)
            },

            /**
             * Obtiene del servidor los datos de una factura.
             * @param facturaId
             * @returns {*}
             */
            getFactura: function (id) {
                var _thisUrl = _url + "/";

                var datos = {
                    params: {
                        id: id
                    }
                };

                return $http.get(_thisUrl, datos).then(
                    function (response) {
                        return response.data;
                    }
                );
            },

            /**
             * Actualiza los valores de una factura
             * @param datos
             * @returns {*}
             */
            setFactura: function (datos) {
                var _thisUrl = _url + "/";
                return $http.put(_thisUrl, datos).then(
                    function (response) {
                        return response.data;
                    }
                );
            },

            /**
             * Agrega un ìtem al presupuesto
             * @param solicitudId
             * @param presupuestoId
             * @param datos
             * @return {ng.IHttpPromise<T>|*}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             * @version V1.0 08/12/14 02:07 PM
             */
            agregarProducto: function (facturaId, datos) {
                var _thisUrl = _url + "/producto";
                var datosItem = {
                    factura_id: facturaId,
                    datos: datos
                };

                return $http.post(_thisUrl, datosItem)
                    .then(
                    function (response) {
                        return response.data;
                    }
                );
            },

            /**
             * Elimina un producto asociado a la factura
             * @param productoId
             * @returns {HttpPromise}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             * @version V1.0
             */
            eliminarProducto: function (productoId) {
                var _thisUrl = _url + "/eliminar_producto";

                var datosItem = {
                    params: {
                        id: productoId
                    }
                };

                return $http.get(_thisUrl, datosItem);
            },

            /**
             * Solicita al servidor que emita la factura
             *
             * @param id
             * @returns {HttpPromise}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             * @version V1.0 21/04/15
             */
            finalizarFactura: function (id) {
                var _thisUrl = _url + "/finalizar/" + id;
                return _put(_thisUrl);
            },

            /**
             * Solicita un listado de todos los productos pertenecientes a una factura
             * @param facturaId
             * @return {*}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             * @version V1.0
             */
            getProductos: function (facturaId) {
                var _thisUrl = _url + "/productos";
                return _get(_thisUrl, {params: {id: facturaId}});
            },

            /**
             * Consulta las facturas asociadas a una cotización
             * @param cotizacionId
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             * @version V1.0 28/04/15
             */
            getFacturasCotizacion: function (cotizacionId) {
                var _thisUrl = _url + "/facturas_cotizacion/" + cotizacionId;
                return _get(_thisUrl);
            }

        };
    }
]);