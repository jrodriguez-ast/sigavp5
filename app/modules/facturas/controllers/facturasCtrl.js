'use strict';
/**
 *Controlador que gestiona el listado de clientes
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 * @package AVP
 *
 */
app.controller("listarPublicacionesNoFacturadas", [
    "$scope", "rClienteID", "$injector", "rTotal", 'particularPermission',
    function ($scope, rClienteID, $injector, rTotal, particularPermission) {
        $scope.title = "Publicaciones no facturadas del cliente";
        $scope.siteCrud = {url: 'clientes/' + rClienteID + '/public_nofact'};
        $scope.headerCrud = [
            {title: 'Facturar', label: 'facturar', visible: true},
            {title: 'id', label: 'id', visible: false},
            {title: 'id', label: 'id', visible: false},
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'cliente_id', label: 'cliente_id', visible: false},
            {title: 'Fecha creación', label: 'fecha_creacion', visible: true},
            {title: 'Proveedor', label: 'medio.razon_social', visible: true},
            {title: 'Importe (Bs.)', label: 'subtotal', visible: true, filter: 'formatNumberMoney'},
            //{title: 'Importe (Bs.)', label: 'importe', visible: true, filter: 'formatNumberMoney'},
            {title: 'Estatus', label: 'estatus', visible: true}
        ];
        //console.log(rTotal);
        $scope.totalSinIva = rTotal.sinIva;
        $scope.totalConIva = rTotal.conIva;
        $scope.total_impuesto = rTotal.total_impuesto;
        $scope.total_impuesto_avp = rTotal.total_impuesto_avp;
        $scope.importe = rTotal.importe;
        $scope.chckdCount = rTotal.cantidad;
        $scope.checkFactura = {};
        $scope.aFacturar = [];
        $scope.operation = $injector.get('permission', 'listarPublicacionesNoFacturadas').data.get.data.permission;
        $scope.permisos = particularPermission;

        /**
         * Esta función obtiene los ID de las publicaciones marcadas para ser facturadas
         */
        $scope.getChecked1 = function () {
            angular.forEach($scope.checkFactura, function (value) {
                if (value.chckd == true) {
                    //console.log($scope.aFacturar);
                    $scope.aFacturar.push(value);
                }
            });
            $scope.facturar($scope.aFacturar);
        };
        /**
         * Llama al servicio que carga la factura
         * @param publicacionArray
         */
            //$scope.facturar = function (publicacionArray) {
            // //  var confirmacion = window.confirm("¿Esta seguro de generar la factura para las publicaciones seleccionadas?");
            // if (confirmacion) {
            //     var facturaModel = $injector.get('facturaModel', 'facturar');
            //    var datos = {};
            //   datos.factura = publicacionArray;
            //  facturaModel.crear(null, datos).$promise.then(function (respuesta) {
            //      var newFactura = respuesta;
            //      var appConfig = $injector.get('appConfig', 'facturar');
            //      var $location = $injector.get('$location', 'facturar');
            //       window.open(appConfig.printPDF('FACTURACION', newFactura.data.id));
            //      //$location.path("admin/detalle_cliente/" + rClienteID + "/facturar");
            //      // $state.go('admin.detalle_cliente.facturar')
            //      var $state = $injector.get('$state', 'facturaModel.crear');
            //      $state.reload('admin.detalle_cliente.facturar');
            //ventana.onbeforeunload = ventana.opener.location.reload();
            //  });
            // }
            //};

        $scope.facturar = function (publicacionArray) {
            var $ngBootbox = $injector.get('$ngBootbox', 'facturar');
            $ngBootbox.confirm('¿Esta seguro de generar la factura para las publicaciones seleccionadas?')

                .then(function () {

                    var facturaModel = $injector.get('facturaModel', 'facturar');
                    var datos = {};
                    datos.factura = publicacionArray;
                    facturaModel.crear(null, datos).$promise.then(function (respuesta) {
                        var newFactura = respuesta;
                        var appConfig = $injector.get('appConfig', 'facturar');
                        var $location = $injector.get('$location', 'facturar');
                        window.open(appConfig.printPDF('FACTURACION', newFactura.data.id));
                        //$location.path("admin/detalle_cliente/" + rClienteID + "/facturar");
                        // $state.go('admin.detalle_cliente.facturar')
                        var $state = $injector.get('$state', 'facturaModel.crear');
                        $state.reload('admin.detalle_cliente.facturar');
                        //ventana.onbeforeunload = ventana.opener.location.reload();
                    });
                }, function () {
                    return false;
                });
        };
        /**
         * Suma el costo de las publicaciones cada vez que se hace click en uno de los checkboxes
         */
        $scope.sumarPublicaciones = function () {
            $scope.totalSinIva = 0;
            $scope.totalConIva = 0;
            var total = 0;
            var subtotal = 0;
            var count = 0;
            var importe = 0;
            var total_impuesto_avp = 0;
            angular.forEach($scope.checkFactura, function (value) {
                if (value.chckd == true) {
                    count++;
                    total = total + value.totalpub;
                    subtotal = subtotal + value.subtotalpub;
                    importe = importe + value.importe;
                    total_impuesto_avp = total_impuesto_avp + value.total_impuesto_avp;
                }
            });
            $scope.chckdCount = count;
            $scope.total_impuesto_avp = total_impuesto_avp;
            $scope.importe = importe;
            $scope.totalSinIva = subtotal;
            $scope.totalConIva = total;
        };
    }]);
app.controller("listarFacturas", [
    "$scope",
    "rPermisos",
    "rClienteID",
    "$injector",
    function ($scope,
              rPermisos,
              rClienteID,
              $injector) {
        $scope.title = "Facturas asociadas al cliente";
        $scope.siteCrud = {url: 'clientes/' + rClienteID + '/facturas_del_cliente'};
        $scope.headerCrud = [
            {title: 'id', label: 'id', visible: false},
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'Fecha creación', label: 'created_at', visible: true, filter: "date", filterParams: "dd-MM-yyyy"}
        ];
        $scope.operation = rPermisos.permission;
        /**
         * Funciones que administran las pestañas de vista y edición de contratos del cliente
         * @param facturaID
         * @param operation_id
         */
        $scope.detalleFactura = function (facturaID, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Detalle Factura #',
                state: 'admin.detalle_cliente.facturas_detail({facturaID:' + facturaID + '})',
                object_id: facturaID
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state', 'listarFacturas.detalleFactura');
            $state.go('^.facturas_detail', {'facturaID': facturaID});
        };
    }]);

/**
 * Visualizar ek detalle de la factura.
 */
app.controller("detalleFacturaController", [
    "$scope", "rClienteID", "$injector", "rFacturaID", "rFactura", 'particularPermission', 'rOperation',
    function ($scope, rClienteID, $injector, rFacturaID, rFactura, particularPermission, rOperation) {
        var $state = $injector.get('$state', 'DetalleFacCtrl');
        $scope.permisos_estatus = particularPermission;
        $scope.factura = rFactura.data;
        $scope.totales = rFactura.totales;
        $scope.changeStatus = function (factura, estado) {
            var facturaModel = $injector.get('facturaModel', 'changeStatus');
            facturaModel.changeStatus({facturaID: factura, estado: estado}).$promise.then(function (resultado) {
                $scope.factura = resultado.data;
            });
        };

        var fechaGenerada = moment($scope.factura.fecha_generada, "DD MM YYYY");
        var hoy = moment().unix();
        var diferencia = moment(fechaGenerada).add(1, "month");
        var fecha_anulacion = "05-" + diferencia.format("MM-YYYY");
        fecha_anulacion = moment(fecha_anulacion, "DD-MM-YYYY").unix();

        $scope.imprimirFactura = function (facturaID) {
            var appConfig = $injector.get('appConfig', 'imprimirFactura'), ventana = false;
            if ($scope.factura.tipo_factura == 'publicacion') {
                ventana = window.open(appConfig.printPDF('FACTURACION', facturaID));
            } else {
                ventana = window.open(appConfig.printPDF('FACTURACIONCUOTA', facturaID));
            }

            if (ventana) {
                ventana.close();
            }
        };

        $scope.buildTab({
            id: rOperation,
            state: $state.current.name + '({facturaID: ' + rFacturaID + '})',
            title: 'Detalle Factura #',
            object_id: rFacturaID
        });

        /**
         * esta función es la que llama al controlador donde se cambian las fechas y estatus
         * @param lugar
         */
        $scope.fechaEntregado = function (lugar) {
            $scope.lugar = lugar;
            var $modal = $injector.get('$modal', 'fechaEntregado');
            var $modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/facturas/views/cambiar_fecha.html',
                controller: 'configurarFechaController',
                size: 'lg',
                resolve: {
                    rFacturaID: ['$stateParams', function ($stateParams) {
                        return $stateParams.facturaID;
                    }],
                    rFactura: function () {
                        return rFactura;
                    },
                    r_estoy: function () {
                        return $scope.lugar;
                    },
                    particularPermission: function () {
                        return $scope.permisos_estatus;

                    }
                }
            });
            $modalInstance.result.then(function () {
                var $state = $injector.get('$state', 'result.then');
                $state.reload('admin.detalle_cliente.facturas_detail');
            });
        };

        /**
         *
         */
        $scope.anularNotaCredito = function () {
            var $modal = $injector.get('$modal');
            var $modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/facturas/views/nota_credito.html',
                controller: 'notaCreditoController',
                size: 'lg',
                resolve: {
                    rFacturaID: ['$stateParams', function ($stateParams) {
                        return $stateParams.facturaID;
                    }],
                    rFactura: function () {
                        return rFactura;
                    },

                    rtotal_Factura: function () {
                        return $scope.factura.total_factura;
                    },
                    particularPermission: function () {
                        return $scope.permisos_estatus;

                    }
                }
            });
            $modalInstance.result.then(function () {
                var $state = $injector.get('$state', 'result.then');
                $state.reload('admin.detalle_cliente.facturas_detail');
            });
        };

        /**
         * Aquí se llama a la función que anulara la factura por cuota
         */
        $scope.anularfacturacuota = function () {
            confirmarAnuluacion();
        };

        /**
         *Aquí se llama a la función que anulara la factura por publicación
         */
        $scope.anularfacturapublicacion = function () {
            confirmarAnuluacion(true);
        };

        /**
         * Esta función es donde se llaman los diferentes mensajes para confirmar que queremos anular de acuerdo a la
         * comparación de la fecha actual, también se hace lo que corresponde en cada caso
         * @param isFactura
         */
        function confirmarAnuluacion(isFactura) {
            var msg = (fecha_anulacion < hoy) ? 'La factura esta fuera de rango ¿La deseas anular?' : 'Esta seguro de anular la factura?';
            var $ngBootbox = $injector.get('$ngBootbox', 'anularfacturapublicacion');
            $ngBootbox.confirm(msg)
                .then(function () {
                    var facturaModel = $injector.get('facturaModel', 'confirmarAnluacion');
                    if (isFactura) {

                        facturaModel.anularfacturapublicacion({
                            facturaID: rFacturaID,
                            datos: $scope.factura
                        }).$promise.then(function (resultado) {
                                $scope.factura = resultado.data;
                                var $state = $injector.get('$state', 'result.then');
                                $state.reload('admin.detalle_cliente.facturas_detail');
                            });
                    } else {
                        facturaModel.anularfacturacuota({
                            facturaID: rFacturaID,
                            datos: $scope.factura
                        }).$promise.then(function (resultado) {
                                $scope.factura = resultado.data;
                                var $state = $injector.get('$state', 'result.then');
                                $state.reload('admin.detalle_cliente.facturas_detail');
                            });
                    }
                }, function () {
                    return false;
                });
        }

    }
]);

/**
 * Controlador de Configuración de Fecha
 */
app.controller("configurarFechaController", [
    "$scope", "$injector", 'rFacturaID', 'rFactura', 'r_estoy', '$modalInstance', 'particularPermission',
    function ($scope, $injector, rFacturaID, rFactura, r_estoy, $modalInstance, particularPermission) {

        $scope.factura = rFactura.data;
        $scope.lugar = r_estoy;
        $scope.facturaid = rFacturaID.data;
        $scope.permisos_estatus = particularPermission;
        $scope.detalles = {};
        $scope.detalles.fecha_motorizado = '';
        $scope.detalles.fecha_cliente = '';
        $scope.detalles.fecha_cancelada = '';


        if ($scope.lugar == 'mensajero') {
            /**
             * Métodos para el Cambio de estatus entregada al mensajero
             */
            $scope.changeStatus = function (factura, estado) {
                var facturaModel = $injector.get('facturaModel', 'changeStatus');
                facturaModel.changeStatus({
                    facturaID: rFacturaID,
                    estado: estado
                }, {fechaMotorizado: $scope.detalles.fecha_motorizado}).$promise.then(function (resultado) {
                        $scope.factura = resultado.data;
                        $modalInstance.close();
                    });
            };
            /**
             * Métodos para el DatePicker de fecha de fundación
             */
            $scope.calendar = {
                opened: {},
                dateFormat: 'dd/MM/yyyy',
                dateOptions: {
                    formatYear: 'yy',
                    startingDay: 1
                },
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    angular.forEach($scope.calendar.opened, function (value, key) {
                        $scope.calendar.opened[key] = false;
                    });

                    $scope.calendar.opened[which] = true;
                },
                disabled: function (date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                }
            };
        }
        if ($scope.lugar == 'cliente') {
            /**
             * Métodos para el Cambio de estatus entregada al cliente
             */
            $scope.changeStatus = function (factura, estado) {
                var facturaModel = $injector.get('facturaModel', 'changeStatus');
                facturaModel.changeStatus({
                    facturaID: rFacturaID,
                    estado: estado
                }, {fechaCliente: $scope.detalles.fecha_cliente}).$promise.then(function (resultado) {
                        $scope.factura = resultado.data;
                        $modalInstance.close();
                    });
            };
            /**
             * Métodos para el DatePicker de fecha de fundación
             */
            $scope.calendar = {
                now: new Date(),
                opened: {},
                dateFormat: 'dd/MM/yyyy',
                dateOptions: {
                    formatYear: 'yy',
                    startingDay: 1
                },
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    angular.forEach($scope.calendar.opened, function (value, key) {
                        $scope.calendar.opened[key] = false;
                    });

                    $scope.calendar.opened[which] = true;
                },
                disabled: function (date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                }
            };
        }
        if ($scope.lugar == 'cancelada') {
            /**
             * Métodos para el Cambio de estatus fecha cancelada
             */
            $scope.changeStatus = function (factura, estado) {
                var facturaModel = $injector.get('facturaModel', 'changeStatus');
                facturaModel.changeStatus({
                    facturaID: rFacturaID,
                    estado: estado
                }, {fechaCancelada: $scope.detalles.fecha_cancelada}).$promise.then(function (resultado) {
                        $scope.factura = resultado.data;
                        $modalInstance.close()
                    });
            };
            /**
             * Métodos para el DatePicker de fecha de fundación
             */
            $scope.calendar = {
                // now: new Date(),
                opened: {},
                dateFormat: 'dd/MM/yyyy',
                dateOptions: {
                    formatYear: 'yy',
                    startingDay: 1
                },
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    angular.forEach($scope.calendar.opened, function (value, key) {
                        $scope.calendar.opened[key] = false;
                    });

                    $scope.calendar.opened[which] = true;
                },
                disabled: function (date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                }
            };
        }


    }]);

/**
 *
 */
app.controller("listarFacturasTodas", [
    "$scope", "rPermisos", "$injector",
    function ($scope, rPermisos, $injector) {
        $scope.title = "Facturas asociadas al cliente";
        $scope.siteCrud = {url: 'clientes/listado_facturas_todas'};
        $scope.headerCrud = [
            {title: 'id', label: 'id', visible: false},
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'Cliente', label: 'razon_social', visible: true},
            {title: 'Código Cliente', label: 'cliente_codigo', visible: true},
            {title: 'Fecha creación', label: 'created_at', visible: true}
        ];
        $scope.operation = rPermisos.permission;
        $scope.imprimirFacturaTodas = function (facturaID) {
            var appConfig = $injector.get('appConfig', 'imprimirFacturaTodas');
            var ventana = window.open(appConfig.printPDF('FACTURACION', facturaID));
            if (ventana) {
                ventana.close();
            }
        };
        /**
         * Funciones que administran las pestañas de vista y edición de contratos del cliente
         * @param facturaID
         */
        $scope.detalleFacturaTodas = function (facturaID /*, operation_id*/) {
            $injector.get('facturaModel', 'detalleFacturaTodas')
                .getClienteId({facturaID: facturaID})
                .$promise
                .then(function (response) {
                    if (response.data.tipo_factura == 'cuota') {
                        $injector.get('$location', 'detalleFacturaTodas')
                            .path('admin/detalle_cliente/' + response.data.cuota[0].contrato.cliente_id + '/facturas_detail/' + facturaID);

                    } else {
                        $injector.get('$location', 'detalleFacturaTodas')
                            .path('admin/detalle_cliente/' + response.data.publicacion[0].cliente_id + '/facturas_detail/' + facturaID);

                    }
                });
        };
    }]);

/**
 *
 */
app.controller("listarNotasdeCredito", [
    "$scope", "rClienteID", "$injector", 'rPermisos',
    function ($scope, rClienteID, $injector, rPermisos) {
        $scope.title = "Notas de Credito";
        $scope.siteCrud = {url: 'clientes/' + rClienteID + '/dt'};
        $scope.headerCrud = [
            {title: 'id', label: 'id', visible: false},
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'Fecha creación', label: 'created_at', visible: true, filter: "date", filterParams: "dd-MM-yyyy"},
            {title: 'Monto', label: 'monto', visible: true, filter: 'formatNumberMoney'},
            {title: 'Estatus', label: 'estatus', visible: false}
        ];
        //console.log(rTotal);

        //$scope.operation = $injector.get('permission', 'listarPublicacionesNoFacturadas').data.get.data.permission;

        $scope.operation = rPermisos.permission;
        $scope.detalleNota = function (notaID, permisos_id) {
            var tab = {
                id: permisos_id,
                title: 'Detalle Nota #',
                state: 'admin.detalle_cliente.notas_credito({notaID:' + notaID + '})',
                object_id: notaID
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state', 'listarNotasdeCredito.detalleNota');
            $state.go('^.notas_detail', {'notaID': notaID});
        };

        $scope.detalleFactura = function (facturaID, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Detalle Factura #',
                state: 'admin.detalle_cliente.facturas_detail({facturaID:' + facturaID + '})',
                object_id: facturaID
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state', 'listarFacturas.detalleFactura');
            $state.go('^.facturas_detail', {'facturaID': facturaID});
        };
    }]);

/**
 * Controlador de nota de crédito
 */
app.controller("notaCreditoController", [
    "$scope", "$injector", 'rFacturaID', 'rFactura', '$modalInstance', 'particularPermission', 'rtotal_Factura',
    function ($scope, $injector, rFacturaID, rFactura, $modalInstance, particularPermission, rtotal_Factura) {

        var $stateParams = $injector.get('$stateParams', 'notaCreditoController');
        var flashMessages = $injector.get('flashMessages', 'notaCreditoController');

        $scope.factura = rFactura.data;
        $scope.facturaid = rFacturaID.data;
        $scope.total_factura = rtotal_Factura;
        $scope.permisos_estatus = particularPermission;
        $scope.detalles = {};
        $scope.detalles.monto = '';
        $scope.detalles.descripcion = '';
        $scope.detalles.n_control = '';
        if ($scope.factura.tipo_factura == 'publicacion') {
            $scope.detalles.cliente_id = $scope.factura.factura_orden_publicacion[0].cliente_id;

        } else {
            $scope.detalles.cliente_id = $scope.factura.factura_contrato_cuotas[0].contrato.cliente_id;
        }

        // Objeto que se enviara al Backend
        $scope.contact = {};
        $scope.contact.monto = '';
        $scope.contact.descripcion = '';
        $scope.contact.n_control = '';

        var $validationProvider = $injector.get('$validation', 'notaCreditoController');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },

            prepare: function (service) {
                var data = angular.copy(service);
                //data.monto = data.monto.replace(/\./g, '').replace(',', '.');
                data.monto = $injector.get('$filter', 'prepare')('formatMoneyToNumber')(data.monto);
                return data;
            },
            /**
             * Enviar las condiciones al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodriguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                $scope.contact.monto = $scope.detalles.monto;
                $scope.contact.descripcion = $scope.detalles.descripcion;
                $scope.contact.n_control = $scope.detalles.n_control;
                var Factura = $injector.get('facturaModel', 'notaCreditoController');
                Factura.crearnotaCredito({
                        facturaID: rFacturaID,
                        cliente_id: $scope.detalles.cliente_id,
                        datos: $scope.factura
                    },

                    $scope.mediocontacto_form.prepare($scope.contact),
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors);
                $modalInstance.close();
            },
            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha modificado la factura por nota de crédito.");
                $scope.$close(true);
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                    }

                    //if (angular.isDefined(message.nombre)) {
                    //    mensajeToShow = mensajeToShow.concat(message.nombre);
                    //}
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');

            }
        };

    }]);
/**
 * Visualizar el detalle de la Nota de crédito.
 */
app.controller("detalleNotaController", [
    "$scope",
    "rClienteID",
    "$injector",
    "rFactura",
    'particularPermission',
    'rOperation',
    'rNotaID',
    function ($scope,
              rClienteID,
              $injector,
              rFactura,
              particularPermission,
              rOperation,
              rNotaID) {
        var $state = $injector.get('$state', 'detalleNotaController');
        $scope.permisos_estatus = particularPermission;
        $scope.nota = rFactura.data;
        $scope.totales = rFactura.totales;
        $scope.changeStatus = function (nota, estado) {
            var facturaModel = $injector.get('facturaModel', 'changeStatus');
            facturaModel.changeStatusnota({notaID: nota, estado: estado}).$promise.then(function (resultado) {
                $scope.nota = resultado.data;
            });
        };
        $scope.imprimirNota = function (facturaID) {
            //console.log(facturaID);
            var appConfig = $injector.get('appConfig', 'imprimirNota');
            var ventana = window.open(appConfig.printPDF('NOTACREDITO', facturaID));
            if (ventana) {
                ventana.close();
            }
        };
        $scope.imprimirFactura = function (facturaID) {
            var appConfig = $injector.get('appConfig', 'imprimirFactura'), ventana = false;
            if ($scope.factura.tipo_factura == 'publicacion') {
                ventana = window.open(appConfig.printPDF('FACTURACION', facturaID));
            } else {
                ventana = window.open(appConfig.printPDF('FACTURACIONCUOTA', facturaID));
            }

            if (ventana) {
                ventana.close();
            }
        };

        $scope.buildTab({
            id: rOperation,
            state: $state.current.name + '({rNotaID: ' + rNotaID + '})',
            title: 'Detalle Nota #',
            object_id: rNotaID
        });


        $scope.fechaEntregadoN = function (lugar) {
            $scope.lugar = lugar;
            var $modal = $injector.get('$modal', 'fechaEntregadoN');
            var $modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/facturas/views/cambiar_fecha.html',
                controller: 'configurarFechaNotaController',
                size: 'lg',
                resolve: {
                    rFacturaID: ['$stateParams', function ($stateParams) {
                        return $stateParams.facturaID;
                    }],
                    rFactura: function () {
                        return rFactura;
                    },
                    r_estoy: function () {
                        return $scope.lugar;
                    },
                    rNotaID: function () {
                        return rNotaID;
                    },
                    particularPermission: function () {
                        return $scope.permisos_estatus;

                    }
                }
            });
            $modalInstance.result.then(function () {
                var $state = $injector.get('$state', 'result.then');
                $state.reload('admin.detalle_cliente.facturas_detail');
            });
        };

    }]);
/**
 * Controlador de Configuración de Fecha
 */
app.controller("configurarFechaNotaController", [
    "$scope", "$injector", 'rNotaID', 'rFactura', 'r_estoy', '$modalInstance', 'particularPermission',
    function ($scope, $injector, rNotaID, rFactura, r_estoy, $modalInstance, particularPermission) {

        $scope.factura = rFactura.data;
        $scope.lugar = r_estoy;
        $scope.notaid = rNotaID;
        $scope.permisos_estatus = particularPermission;
        $scope.detalles = {};
        $scope.detalles.fecha_motorizado = '';
        $scope.detalles.fecha_cliente = '';


        if ($scope.lugar == 'mensajero') {
            /**
             * Métodos para el Cambio de estatus entregada al mensajero
             */
            $scope.changeStatus = function (factura, estado) {
                var facturaModel = $injector.get('facturaModel', 'changeStatus');
                facturaModel.changeStatusnota({
                    notaID: rNotaID,
                    estado: estado
                }, {fechaMotorizado: $scope.detalles.fecha_motorizado}).$promise.then(function (resultado) {
                        $scope.factura = resultado.data;
                        $modalInstance.close();
                    });
            };
            /**
             * Métodos para el DatePicker de fecha de fundación
             */
            $scope.calendar = {
                opened: {},
                dateFormat: 'dd/MM/yyyy',
                dateOptions: {
                    formatYear: 'yy',
                    startingDay: 1
                },
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    angular.forEach($scope.calendar.opened, function (value, key) {
                        $scope.calendar.opened[key] = false;
                    });

                    $scope.calendar.opened[which] = true;
                },
                disabled: function (date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                }
            };
        }
        if ($scope.lugar == 'cliente') {
            /**
             * Métodos para el Cambio de estatus entregada al cliente
             */
            $scope.changeStatus = function (factura, estado) {
                var facturaModel = $injector.get('facturaModel', 'changeStatus');
                facturaModel.changeStatusnota({
                    notaID: rNotaID,
                    estado: estado
                }, {fechaCliente: $scope.detalles.fecha_cliente}).$promise.then(function (resultado) {
                        $scope.factura = resultado.data;
                        $modalInstance.close();
                    });
            };
            /**
             * Métodos para el DatePicker de fecha de fundación
             */
            $scope.calendar = {
                now: new Date(),
                opened: {},
                dateFormat: 'dd/MM/yyyy',
                dateOptions: {
                    formatYear: 'yy',
                    startingDay: 1
                },
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    angular.forEach($scope.calendar.opened, function (value, key) {
                        $scope.calendar.opened[key] = false;
                    });

                    $scope.calendar.opened[which] = true;
                },
                disabled: function (date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                }
            };
        }


    }]);