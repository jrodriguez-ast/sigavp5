'use strict';

/**
 * Controlador que gestiona el listado de Tipo Correos
 * @author Carlos Blanco <cebs923@gmail.com>
 * @packege SIGAVP
 * @version V-1.0 10/07/2015 14:32:30
 */

app.controller('TipoCorreoListController',
    ['$scope', 'r_permisos',
        function ($scope, r_permisos) {

            $scope.title = 'Tipo de Correos';
            $scope.siteCrud = {url: 'configuracion/dt_tipo_correo'};

            $scope.headerCrud = [
                {title: 'ID', label: 'id', visible: false},
                {title: 'Nombre', label: 'nombre', visible: true},
                {title: 'Descripcion', label: 'descripcion', visible: true}

            ];

            $scope.operation = r_permisos.get.data.permission;

        }]);

app.controller("CrearTipoCorreoController", [
    "$scope",
    'flashMessages',
    '$injector',
    function ($scope,
              flashMessages,
              $injector) {

        $scope.detalles = {};
        $scope.detalles.nombre = '';
        $scope.detalles.descripcion = '';

        /*
         Objeto que se enviara al Backend
         */
        $scope.contact = {};
        $scope.contact.nombre = '';
        $scope.contact.descripcion = '';

        var $validationProvider = $injector.get('$validation');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodríguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                var tipocorreoModel = $injector.get('tipocorreoModel', 'send');

                $scope.contact.nombre = $scope.detalles.nombre;
                $scope.contact.descripcion = $scope.detalles.descripcion;
                $scope.datos = '';
                $scope.datos = $scope.contact;
                //  console.log();
                tipocorreoModel.crear(
                    $scope.datos,
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors);
            },
            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {

                // No evaluamos OK xq el servidor responde 200 siempre y este ok.
                if (response.status == 'ok') {
                    var flashMessages = $injector.get('flashMessages', 'success');
                    var $location = $injector.get('$location');
                    var msg = 'Tipo de Correo creado';
                    flashMessages.show_success(msg);
                    $location.path("admin/tipo_correo");

                }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    //if(angular.isDefined(message.nombre)){
                    //    mensajeToShow = mensajeToShow.concat(message.nombre);
                    //}
                    //if(angular.isDefined(message.apellido)){
                    //    mensajeToShow = mensajeToShow.concat(message.apellido);
                    //}
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);
