
'use strict';

app.factory('tipocorreoModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        var _url = appConfig.getPathApi() + 'configuracion/tipo_correo/:id';
        var data = $resource(_url, null, {
            update: {
                method: 'PUT'
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'configuracion/tipo_correo',
                isArray: false
            }
        });
        return data;
    }

]);