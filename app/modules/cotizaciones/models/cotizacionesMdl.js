'use strict';

app.factory('Cotizacion', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'cotizaciones/cotizacion/:id', null, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            pendiente: {
                method: 'GET',
                url: appConfig.getPathApi() + 'cotizaciones/pendiente/:clienteId',
                params: {id: '@_clienteId'}
            },
            cotizacion_servicio: {
                method: 'POST',
                url: appConfig.getPathApi() + 'cotizaciones/cotizacion/:cotizacionId/servicios',
                params: {cotizacionId: '@cotizacionId'},
                isArray: false
            },
            cotizacion_servicio_data: {
                method: 'GET',
                url: appConfig.getPathApi() + 'cotizaciones/cotizacion/:id_cotizacion_servicio/servicios',
                params: {id_cotizacion_servicio: '@id_cotizacion_servicio'},
                isArray: false
            },
            cotizacion_servicio_update: {
                method: 'PUT',

                url: appConfig.getPathApi() + 'cotizaciones/cotizacion/:cotizacionId/servicios/:cotizacionServicioId',
                params: {
                    cotizacionId: '@cotizacionId',
                    cotizacionServicioId: '@cotizacionServicioId'
                },
                isArray: false
            },
            cotizacion_servicio_delete: {
                method: 'DELETE',

                url: appConfig.getPathApi() + 'cotizaciones/cotizacion/:cotizacionId/servicios/:cotizacionServicioId',
                params: {
                    cotizacionId: '@cotizacionId',
                    cotizacionServicioId: '@cotizacionServicioId'
                },
                isArray: false
            },
            enviar_cotizacion: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'cotizaciones/enviarCotizacion/:cotizacionId',
                params: {cotizacionId: '@cotizacionId'}
            },
            asociar_contrato:{
                method: 'PUT',
                url: appConfig.getPathApi() + 'cotizaciones/asociarContrato/:cotizacion_id/:contrato_id',
                params: {cotizacion_id: '@cotizacion_id', contrato_id: '@contrato_id'}
            },
            delete:{
                method: 'DELETE'
            }
        });
    }
]);


/**
 * Factory para administrar funciones que no están atadas al api REST estándar
 */
app.factory('cotizacionesMdl', ["appConfig", "$upload", function (appConfig, $upload) {
    return {
        upload_cotizacion: function (file) {
            var _url = appConfig.getPathApi() + 'storage/create';
            return $upload.upload({

                url: _url,
                headers: {'Content-Type': file.type},
                method: 'POST',
                file: file
                //}).error(function (success, error, progress) {
                //console.log(success, error, progress);
            });
        },
        save_orden_servicio: function (file, data) {
            var _url = appConfig.getPathApi() + 'cotizaciones/save_orden_servicio';
            return $upload.upload({
                url: _url,
                headers: {'Content-Type': file.type},
                method: 'POST',
                file: file,
                data: data
                //}).error(function (success, error, progress) {
                //console.log(success, error, progress);
            });
        }
    };
}]);