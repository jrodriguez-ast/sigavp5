'use strict';

/**
 * Controlador que gestiona el listado de Cotizaciones
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('CotizacionCtrl', [
    '$scope', 'module_permission', '$injector',
    function ($scope, module_permission, $injector) {
        var flashMessages = $injector.get('flashMessages', 'MunicipioCtrlC');
        var $ngBootbox = $injector.get('$ngBootbox','MunicipioCtrlC');
        var crudFactory = $injector.get('crudFactory', 'MunicipioCtrlC');

        $scope.title = 'Cotizaciones';

        $scope.siteCrud = {url: 'cotizaciones/dt', order: [[4, 'asc']]};

        $scope.headerCrud = [
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'Razón Social', label: 'razon_social', visible: true},
            {title: 'Fecha', label: 'fecha_creacion', visible: true},
            {title: 'Finalizada', label: 'es_terminada', visible: true, filter: 'io_finalizada'},
            {title: 'Estatus', label: 'aprobada', visible: true, filter: 'estatus_cotizacion'}
        ];

        $scope.operation = module_permission.get.data.permission;

        /**
         *
         *
         * @author Amalio Velasquez
         * */
        $scope.borrarCotizacion = function(id){

            $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
                function(){
                    var municipio = $injector.get('Cotizacion','confirm');

                    municipio.remove(
                        {
                            id: id
                        },
                        function(success){
                            if(success.status == 'ok'){
                                crudFactory.reloadData();
                                flashMessages.show_success(success.message);
                            }else if(success.status == 'fail'){
                                crudFactory.reloadData();
                                flashMessages.show_error(success.message);
                            }
                        },
                    function(error){
                        crudFactory.reloadData();
                        flashMessages.show_error(error.message);
                        }
                    );
                }
                //, function(){}
            );
        }

    }
]);

/**
 * Controlador que gestiona el listado de Cotizaciones de acuerdo a un cliente
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('CotizacionCtrlC', [
    '$scope', 'particularPermission', 'clientID', '$injector',
    function ($scope, particularPermission, clientID, $injector) {
        $scope.title = 'Cotizaciones';

        $scope.siteCrud = {url: 'cotizaciones/dt_client/' + clientID};

        $scope.headerCrud = [
            {title: 'Código', label: 'codigo', visible: true},
            {title: 'Razón Social', label: 'razon_social', visible: true},
            {title: 'Fecha', label: 'fecha_creacion', visible: true},
            {title: 'Aprobada', label: 'aprobada_text', visible: true}
        ];

        $scope.operation = particularPermission.permission;

        /**
         * Crea el nuevo tab de servicios en modo edit.
         * @param cotizacion_id
         * @param operation_id
         */
        $scope.abrirClienteCotizacionPublicaciones = function (cotizacion_id, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Publicaciones relacionadas con Cotización #',
                state: 'admin.detalle_cliente.cotizacion_publicacion({cotizacion_id:' + cotizacion_id + '})',
                object_id: cotizacion_id
            };
            $scope.buildTab(tab, 1);
            var $state = $injector.get('$state', 'abrirClienteCotizacionPublicaciones');
            $state.go('^.cotizacion_publicacion', {'cotizacion_id': cotizacion_id});
        };

    }
]);

/**
 * Controlador que gestiona la creación de la Cotización
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('CotizacionCrearCtrl', [
    '$scope', '$stateParams', '$location', 'Cotizacion',
    function ($scope, $stateParams, $location, Cotizacion) {
        Cotizacion.save({clienteId: $stateParams.cliente_id}, function (response) {
            $location.path('admin/cotizacion_editar/' + response.id);
        });
    }
]);

/**
 * Controlador para gestionar la Cotización
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('CotizacionFormCtrl', ['$scope', "$injector", "cotizacionData", 'particularPermission',
    function ($scope, $injector, cotizacionData, particularPermission) {
        var flashMessages = $injector.get('flashMessages', 'CotizaciónFormCtrl');
        var $ngBootbox = $injector.get('$ngBootbox', 'CotizaciónFormCtrl');
        var CONSTANTES = $injector.get('CONSTANTES', 'CotizaciónFormCtrl');
        var Cotizacion = $injector.get('Cotizacion', 'CotizaciónFormCtrl');
        var $timeout = $injector.get('$timeout', 'CotizaciónFormCtrl');
        var $filter = $injector.get('$filter', 'CotizaciónFormCtrl');
        var $modal = $injector.get('$modal', 'CotizaciónFormCtrl');

        $scope.permisos = particularPermission;
        var components = preparePermissionComponets($scope.permisos.permission);
        $scope.buttons = components.buttons;
        /**
         * Prepara y retorna la data de permisos para tabs y botones de la ficha
         * de clientes.
         *
         * @param  {array} permission permisos dentro de la vista.
         * @return {{tabs: Array, buttons: Array}}
         * @version V-1.0 08/06/15 10:46:39
         * @author Jose Rodriguez
         */
        function preparePermissionComponets(permission) {
            var components = {tabs: [], buttons: []};

            // Obtenemos la url sin IDs
            for (var i = 0, len = permission.length; i < len; i++) {
                var p = permission[i], current = {
                    title: p._name, icon: p.icon, url: p.url,
                    state: p.url.split('/').join('.'),
                    // por defecto todas las pestañas inactivas
                    active: false
                };

                if (p.chk_render_on == 'tabpanel') {
                    components.tabs.push(current);
                } else if (p.chk_render_on == 'button') {
                    components.buttons.push(current);
                }
            }
            return components;
        }

        // Validamos si contrato esta definido en base de datos.
        $scope.view = {contrato_bloqueado: ( angular.isNumber(cotizacionData.contrato_id) && cotizacionData.contrato_id > 0 )};
        $scope.pre_data = {iva: 0.00, recargo_avp: 0.00};
        $scope.solo_envio = false;
        $scope.datos = cotizacionData;

        $scope.datos.fecha_creacion = moment($scope.datos.fecha_creacion, 'YYYY-MM-DD').toDate();
        $scope.datos.fecha_expiracion = moment($scope.datos.fecha_expiracion, 'YYYY-MM-DD').toDate();

        //Se obtiene el iva para pintar en la interfaz
        $injector.get('ivaModel', 'Cotización_Form_Ctrl').get(function (response) {
            if (response.status == 'ok') {
                $scope.datos.porcentaje_impuesto = response.data.iva;
            }
        });
        //Se obtiene el recargo AVP para pintar en la interfaz
        $injector.get('recargogestionModel', 'Cotización_Form_Ctrl').get(function (response) {
            if (response.status == 'ok') {
                $scope.datos.porcentaje_avp = response.data.porcentaje;
            }
        });


        $injector.get('contratoModel', 'getContratos').listar(
            {client: cotizacionData.cliente_id},
            function (response) {
                $scope.contratos = [];
                var len = response.data.length;
                if (len <= 0) {
                    return false;
                }

                var cntt, lenChildren, j;
                for (var i = len - 1; i >= 0; i--) {
                    cntt = response.data[i];
                    if (cntt.activo != true) {
                        continue;
                    }

                    cntt.disabled = 'out';
                    cntt.total = cntt.tarifa * 1 + cntt.monto_iva * 1;

                    lenChildren = cntt.children.length;
                    if (lenChildren > 0) {
                        var child;
                        for (j = lenChildren - 1; j >= 0; j--) {
                            child = cntt.children[j];
                        }
                    }

                    for (j = cntt.cuotas.length - 1; j >= 0; j--) {
                        cntt.cuotas[j].total = cntt.cuotas[j].monto * 1 + cntt.cuotas[j].monto_iva * 1;
                    }

                    for (j = cntt.pautas.length - 1; j >= 0; j--) {
                        cntt.pautas[j].total = cntt.pautas[j].monto * 1 + cntt.pautas[j].monto_iva * 1;
                    }

                    $scope.contratos.push(cntt);
                }
            }
        );

        /**Arreglo de tab para la vista**/
        $scope.sectionTabs = [
            {title: 'Detalles', content: './modules/cotizaciones/views/tab_detalles.html?v=' + CONSTANTES.VERSION},
            {
                title: 'Detalles del Contrato',
                content: './modules/cotizaciones/views/tab_detalles_contrato.html?v=' + CONSTANTES.VERSION
            },
            {title: 'Pautas', content: './modules/cotizaciones/views/tab_medios.html?v=' + CONSTANTES.VERSION},
            {
                title: 'Contactos del Cliente',
                content: './modules/cotizaciones/views/tab_contactos.html?v=' + CONSTANTES.VERSION
            },
            {
                title: 'PreVisualización',
                content: './modules/cotizaciones/views/tab_previsualiza.html?v=' + CONSTANTES.VERSION
            }
        ];

        /**
         * Maneja un timeout para retrasar la petición de actualización con el fin de no sobrecargar las peticiones
         *
         * @type $timeout
         */
        var tPeticion = null;

        /**
         * Envía la solicitud para actualizar la cotización
         */
        $scope.guardar = function (finalizar, aprobar) {
            $scope.send_data = [];
            $scope.send_data = angular.copy($scope.datos);


            if (tPeticion !== null) {
                $timeout.cancel(tPeticion);
            }
            tPeticion = $timeout(function () {
                if ($scope.send_data.descuento === null) {
                    $scope.send_data.descuento = 0;
                }

                if (angular.isDefined(finalizar) && finalizar) {
                    $scope.send_data.es_terminada = true;
                    $scope.send_data.create_code = true;
                }

                if (angular.isDefined(aprobar) && aprobar) {
                    //Se verifica que al menos exista una orden de servicio si no hay contrato valido
                    if (angular.isDefined(aprobar) && aprobar) {
                        if (!$scope.send_data.cliente.contrato_valido && $scope.send_data.orden_servicio.length == 0) {
                            flashMessages.show_info('Debe cargar al menos una orden de servicio para continuar!');
                            $scope.sectionTabs[1].active = true;
                            return false;
                        }
                    }
                    $scope.send_data.aprobada = true;
                    $scope.send_data.create_code = false;
                }

                //Actualiza la cotización
                Cotizacion.update({id: $scope.send_data.id}, $scope.send_data).$promise.then(function (response) {
                    if (response.status == 'ok') {
                        $scope.datos = response.data;
                        if (angular.isDefined(finalizar) && finalizar || angular.isDefined(aprobar) && aprobar) {
                            $scope.volver();
                        }
                    }

                });
            }, 1);

        };

        /* Para manejo de multiples Datapicker en el formulario */
        $scope.calendar = {
            /**
             * Maneja el estado de los calendarios
             */
            opened: {},
            /**
             * Indica el formato de la fecha en los calendarios
             */
            dateFormat: 'dd/MM/yyyy',
            /**
             * Opciones del calendario
             */
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            /**
             * Este atributo es opcional, se utiliza para indicar el min-date o menor día posible a elegir
             */
            now: new Date(),
            /**
             * Método que controla la apertura de los calendarios, dada una clave indicada en el método del click
             *
             * @param $event
             * @param which
             */
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            /**
             * Método que desactiva la selección de fines de semana en el calendario
             *
             * @param date
             * @param mode
             * @returns {boolean}
             */
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        /* Fin de manejo de Datapicker */


        /**
         * Ejecuta la acción volver desde la cotización al listado de cotizaciones
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 22/01/15
         */
        $scope.volver = function () {
            contratoWatch();
            $injector.get('$location', 'volver').path('admin/cotizacion');
        };

        /**Ejecuta acción de finalizar la cotización sin envío de de correos
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.finalizar = function () {
            /*            if (!$scope.datos.cliente.contrato_valido && $scope.datos.orden_servicio.length == 0) {
             flashMessages.show_info('Debe cargar al menos una orden de servicio para continuar!');
             $scope.sectionTabs[4].active = true;
             return false;
             }*/
            if ($scope.check_publicaciones(2)) {
                var msg = "Procederá a finalizar la cotización, una vez finalizada no podrá editarla. Desea continuar?";
                bootbox.confirm(msg, function (result) {
                    if (result) {
                        $scope.guardar(true);
                    }
                });
            }
        };

        /**
         * Abre una ventana con una previsualización en pdf de la cotización final
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 24/02/15
         */
        $scope.previsualizar = function (novalido) {
            if ($scope.check_publicaciones(1)) {
                var appConfig = $injector.get('appConfig', 'previsualizar');
                window.open(appConfig.getPathApi() + 'cotizaciones/pdf_pre/' + getCotizacionID() + '/' + novalido);
            }
        };

        /**
         * Invoca el modal para agregar Ordenes de Servicio a la Cotización
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.agregarOrden = function () {

            var modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/cotizaciones/views/orden_servicio.html?v=' + CONSTANTES.VERSION,
                controller: 'CotizacionOrdenServicioCtrl',
                resolve: {
                    cotizacionData: function () {
                        return $scope.datos;
                    },
                    r_recepcion: ["recepcionModel", function (recepcionModel) {
                        return recepcionModel.get().$promise;
                    }]
                }
            });
            modalInstance.result.then(function (data) {
                if (data != null) {
                    $scope.datos = data;
                }
            });
        };

        /**
         * Invoca el modal para agregar Versiones de Publicaciones a la Cotización
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.agregarMedio = function (id_cotizacion_servicio, clone) {
            if (angular.isDefined($scope.sectionTabs[2])) {
                $scope.sectionTabs[2].active = true;
            }

            $modal.open({
                animation: true,
                params: {contrato: $scope.datos.contrato},
                //backdrop: 'static',
                templateUrl: 'modules/cotizaciones/views/condicion_medio.html?v=' + CONSTANTES.VERSION,
                controller: 'CotizacionCondicionesCtrl',
                size: 'xl',
                resolve: {
                    //Provee el ID de la cotización
                    cotizacionId: function () {
                        return getCotizacionID();
                    },
                    //Provee los datos de la cotización al modal
                    datosEdit: function () {
                        if (angular.isDefined(id_cotizacion_servicio) && id_cotizacion_servicio != null) {
                            return Cotizacion.cotizacion_servicio_data({
                                id_cotizacion_servicio: id_cotizacion_servicio
                            }).$promise.then(function (response) {
                                    if (angular.isDefined(response)) {
                                        return response;
                                    }
                                });
                        }
                    },
                    cotizacionAprobada: function () {
                        return $scope.datos.aprobada;
                    },
                    clone: function () {
                        return !!(angular.isDefined(clone) && clone);
                    },
                    rContrato: function () {
                        return angular.copy($scope.datos.contrato);
                    }
                }
            }).result.then(function (selectedItem) {
                    //Maneja los eventos que se ejecutan al cerrar el modal (Agregar Medio)
                    $scope.selected = selectedItem;
                    //crudFactory.reloadData();
                    $scope.guardar();
                });
        };
        /**
         * Permite suprimir publicaciones de la cotización
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 15/09/2015
         */
        $scope.suprimirVersion = function (id_cotizacion_servicio) {
            if ($scope.datos.es_terminada) {
                $ngBootbox.alert('La cotización ha sido finalizada por ello no puede aplicar cambios!');
            } else {
                $ngBootbox.confirm('Se procederá a eliminar la publicación de forma permanente. Desea continuar?')
                    .then(function () {
                        Cotizacion.cotizacion_servicio_delete(
                            {
                                cotizacionId: $scope.datos.id,
                                cotizacionServicioId: id_cotizacion_servicio
                            },
                            function (response) {
                                if (response) {
                                    $scope.datos = response;
                                }
                            }
                        );
                    }, function () {
                        return false;
                    });
            }

        };

        /**
         * Verifica antes de ser finalizada la cotización si tiene todas las validaciones pertinentes
         * @params {integer} opc  Opción para determinar el mensaje a mostrar
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.check_publicaciones = function (opc) {
            var msg = [];
            msg[1] = 'Debe cargar al menos una publicación para previsualizar el documento!';
            msg[2] = 'Debe cargar al menos una publicación para poder finalizar la cotización!';
            if ($scope.datos.versiones.length == 0) {
                flashMessages.show_info(msg[opc]);
                return false;
            }
            return true;
        };

        /**
         * Finaliza la cotización y envía un email a los contactos del cliente
         * @params {integer} solo_envio Evalúa si se envía el correo a los contactos del cliente
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.finalizar_send = function (solo_envio) {
            /*            if (!$scope.datos.cliente.contrato_valido && $scope.datos.orden_servicio.length == 0) {
             flashMessages.show_info('Debe cargar al menos una orden de servicio para continuar!');
             $scope.sectionTabs[4].active = true;
             return false;
             }*/
            if ($scope.check_publicaciones(2)) {
                if (angular.isDefined(solo_envio))
                    $scope.solo_envio = solo_envio;
                var modalInstance = $modal.open({
                    size: 'lg',
                    templateUrl: 'modal_send_email.html',
                    controller: ['$scope', '$modalInstance', 'dataCliente', '$injector',
                        function ($scope, $modalInstance, dataCliente, $injector) {
                            var $validationProvider = $injector.get('$validation', 'finalizar_send.controller');
                            $scope.datosView = {
                                email_send: [],
                                contactos: []
                            };
                            $scope.cliente = dataCliente;
                            $scope.cliente.correos_electronicos = angular.fromJson($scope.cliente.correos_electronicos);
                            if ($scope.cliente.correos_electronicos != null && $scope.cliente.correos_electronicos.length > 0) {
                                angular.forEach($scope.cliente.correos_electronicos, function (value) {
                                    $scope.datosView.email_send.push({mail: value, activo: true})
                                });
                            }

                            if ($scope.cliente.contactos.length > 0) {
                                angular.forEach($scope.cliente.contactos, function (contactos) {
                                    if (contactos.correos_electronicos != null && contactos.correos_electronicos.length > 0) {
                                        var tmp = [];
                                        angular.forEach(angular.fromJson(contactos.correos_electronicos), function (correos) {
                                            tmp.push({a: correos.a, activo: true})
                                        });
                                        contactos.correos_electronicos = tmp
                                    }
                                    $scope.datosView.contactos.push(contactos);
                                });
                            }
                            $scope.cancel = function () {
                                $modalInstance.close(null);
                            };

                            $scope.formObject = {
                                checkValid: $validationProvider.checkValid,
                                submit: function (form) {
                                    $validationProvider.validate(form).success(function () {
                                        $modalInstance.close($scope.datosView);
                                    });
                                },

                                reset: function (form) {
                                    $validationProvider.reset(form);
                                }
                            }
                        }],
                    resolve: {
                        dataCliente: function () {
                            return $scope.datos['cliente'];
                        }
                    }
                });
                modalInstance.result.then(function (data) {
                    if (angular.isDefined(data) && data) {
                        $scope.datos.con_email = true;
                        $scope.datos.data_email = data;
                        if ($scope.solo_envio) {
                            $scope.datos.create_code = false;
                            $scope.enviar();
                        } else {
                            $scope.guardar(true);
                        }
                    }
                });
            }
        };

        var tPeticionSend = null;
        $scope.enviar = function () {
            if (tPeticionSend !== null) {
                $timeout.cancel(tPeticionSend);
            }
            tPeticionSend = $timeout(function () {
                Cotizacion.enviar_cotizacion({cotizacionId: $scope.datos.id}, $scope.datos).$promise.then(function (response) {
                    if (response.status == 'ok') {
                        $scope.datos = response.data;
                        /*                        if (angular.isDefined(finalizar) && finalizar) {
                         $state.go('^.cotizacion_ver', {'id': $scope.datos.id});
                         }*/
                    }
                    flashMessages.show_info('La cotización se ha enviado a los destinatarios!');
                });
            }, 1);
        };

        /**
         * Ejecuta la acción de  aprobar la cotización
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         *         Jose Rodriguez <josearodrigueze@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.aprobar = function () {
            // Si no tiene contrato seleccionado
            if (angular.isUndefined($scope.datos.contrato)) {
                // 4, que es la posición del tab de contrato.
                $scope.sectionTabs[1].active = true;

                var $ngBootbox = $injector.get('$ngBootbox', 'isUndefined');
                $ngBootbox.alert('Debe asociar la cotización a un contrato u orden de servicio.');

                return false;
            }

            //$ngBootbox.confirm('Se procederá a aprobar la cotización. Desea continuar?')
            //    .then(function () {
            //$scope.guardar(true, true);
            $injector.get('$state', 'aprobar').go('admin.cotizacion_editar.cotizacion_aprobar', {'data': $scope.datos});
            //}, function () {
            //    return false;
            //});
        };

        /**
         * Obtiene el ID de Cotización en Actual.
         * @returns {*|.asociar_contrato.params.cotizacion_id|cotizacion_id}
         */
        function getCotizacionID() {
            return $injector.get('$stateParams', 'getCotizacionID').cotizacion_id;
        }

        /**
         * Verifica que la cotización cumpla con lo establecido en el contrato.
         * @param contrato
         * @returns {number}
         */
        $scope.verificarContrato = function (contrato) {
            var versiones = $scope.datos.versiones, vLen = versiones.length;
            if (vLen <= 0) {
                // No tengo pautas por tanto puedo asociar contrato.
                // Pero no Valido publicaciones.
                return 1;
            }

            contrato = contrato || $scope.datos.contrato;
            if (!angular.isObject(contrato)) {
                flashMessages.show_error('Debe Seleccionar un contrato Para Ejecutar esta acción.');
                $scope.sectionTabs[1].active = true;
            }

            var pautas = contrato.pautas, es_version_valida, invalidas = [];
            for (var i = vLen - 1; i >= 0; i--) {
                es_version_valida = false;
                for (var j = pautas.length - 1; j >= 0; j--) {
                    if (versiones[i].tipo_publicacion_id == pautas[j].tipo_publicacion_id) {
                        es_version_valida = true;
                        break;
                    }
                }
                if (!es_version_valida) {
                    invalidas.push('<li>Nombre de Versión: <b>' + versiones[i].version_nombre + '</b>, de Tipo : <b>'
                    + versiones[i].tipo_publicacion_name + '</b></li>');
                }
            }

            if (invalidas.length > 0) {
                $ngBootbox.alert('La cotización contiene versiones con un tipo de publicación no incluido en el ' +
                'contrato: ' + contrato.codigo + '.<br/> Las versiones en afectadas son:<ul>' + invalidas.join() + '</ul>');
                $scope.datos.contrato = null;
                return 0;
            }
            return 1;
        };

        /**
         * Botón para asociar el contrato.
         */
        $scope.asociar_contrato = function () {
            if (!$scope.verificarContrato()) {
                return;
            }

            Cotizacion
                .asociar_contrato({contrato_id: $scope.datos.contrato.id, cotizacion_id: getCotizacionID()})
                .$promise
                .then(function (response) {
                    if (response.status == 'ok') {
                        $scope.datos.contrato_asociado = 1;
                        //$scope.datos = response.data;
                        flashMessages.show_info('Contrato asociado exitosamente.');
                    }
                }, function (response) {
                    flashMessages.show_error(response.errors.message);
                });
        };

        /**
         *
         */
        var contratoWatch = $scope.$watch('datos.contrato', function (_new, _old) {
            if ($scope.datos.contrato_asociado != 1 || !angular.isObject(_new) || _new == _old) {
                return;
            }

            if (!$scope.verificarContrato(_new)) {
                return;
            }

            var msg = 'Esta cotización ya posee un contrato asociado, esta seguro de cambiarlo? <br/>' +
                '<span class="small text-warning">Recuerde, para guardar el cambio de contrato debe presionar ' +
                '"Asociar Contrato".</span>';
            $ngBootbox.confirm(msg).then(null,
                function () {
                    $scope.datos.contrato_asociado = 0;
                    $scope.datos.contrato = _old;
                });
        });
    }
]);


/**
 * Controlador que gestiona el modal las Condiciones y las tarifas de la Cotizaciones.
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('CotizacionCondicionesCtrl', [
    '$scope', '$injector', "cotizacionId", "$modalInstance", "datosEdit", 'cotizacionAprobada', 'clone', 'rContrato',
    function ($scope, $injector, cotizacionId, $modalInstance, datosEdit, cotizacionAprobada, clone, rContrato) {
        var TipoPublicacion = $injector.get('TipoPublicacion', 'Cotización_Condiciones_Ctrl');
        var cotizacionesMdl = $injector.get('cotizacionesMdl', 'Cotización_Condiciones_Ctrl');
        var CondicionModel = $injector.get('CondicionModel', 'Cotización_Condiciones_Ctrl');
        var flashMessages = $injector.get('flashMessages', 'Cotización_Condiciones_Ctrl');
        var Cotizacion = $injector.get('Cotizacion', 'Cotización_Condiciones_Ctrl');
        var Servicio = $injector.get('Servicio', 'Cotización_Condiciones_Ctrl');
        var Medio = $injector.get('Medio', 'Cotización_Condiciones_Ctrl');
        //var crudFactory = $injector.get('crudFactory', 'Cotización_Condiciones_Ctrl');
        //var $ngBootbox = $injector.get('$ngBootbox', 'Cotización_Condiciones_Ctrl');
        //var $location = $injector.get('$location', 'Cotización_Condiciones_Ctrl');
        //var $state = $injector.get('$state', 'Cotización_Condiciones_Ctrl');

        // Solicitamos Medio en función de Contrato.
        var params = {negociacion: true};
        if (angular.isObject(rContrato)) {
            params.contrato_id = rContrato.id;
        }

        /********************Inicialización de variables a utilizar en el modal**********/
        $scope.aprobada = cotizacionAprobada;
        $scope.edit = (angular.isDefined(datosEdit) ? true : false);
        $scope.mode = (datosEdit != null ? 'Actualizar' : 'Agregar');
        $scope.clone = clone;
        $scope.active_add_conditions = true;
        $scope.con_hijos = false;
        $scope.medios = Medio.query(params);
        $scope.datos = {
            versiones: [],
            tipoPublicacion: null,
            oldTipoPublicacion: null,
            medio: null,
            servicio: null,
            servicio_hijo: null,
            oldMedio: null,
            oldServicio: null,
            condicion: null,
            detalleCondiciones: null,
            add_condiciones: []
        };
        $scope.condiciones = [];
        $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
        $scope.datos.condicion = {id: 0};
        $scope.negociacion = {
            servicio: {},
            condicion: {},
            tarifa: {}

        };
        /*******************Fin de Inicialización de Variables********/

        /**
         * Ejecuta la acción de cancelar el modal
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.cancel = function () {
            $modalInstance.close();
        };

        /*****Condición que determina si el modal esta en modo edición o creación********/
        if (datosEdit != null) {
            $scope.mensaje = 'Puede actualizar la información ya cargada si asi lo desea.';
            var DataServicios = datosEdit.cotizaciones_servicios[0];
            var DataServicioscondiciones = datosEdit.cotizaciones_servicios_condiciones;

            if (DataServicios != null) {
                $scope.cotizaciones_servicios_id = DataServicios.cotizaciones_servicios_id;
                $scope.datos.tipoPublicacion = {
                    "id": DataServicios.tipo_publicacion_id,
                    "nombre": DataServicios.tipo_publicacion
                };
                $scope.medios = Medio.query(params);
                $scope.datos.medio = {"id": DataServicios.medio_id, "razon_social": DataServicios.razon_social};


                $scope.tiposPublicaciones = TipoPublicacion.listado_for_medio({
                    medioId: $scope.datos.medio.id
                });

                Servicio.listado({
                    tipoPublicacionId: DataServicios.tipo_publicacion_id,
                    medioId: DataServicios.medio_id,
                    modo: 'children',
                    servicioPadre: false,
                    servicioHijo: DataServicios.servicio_id

                }).$promise.then(function (response) {
                        if (response.length > 0) {
                            $scope.servicios_hijos = [response[0]];
                            $scope.datos.servicio_hijo = {"id": response[0].id};
                            Servicio.listado({
                                tipoPublicacionId: DataServicios.tipo_publicacion_id,
                                medioId: DataServicios.medio_id,
                                modo: 'parent'

                            }).$promise.then(function (r) {
                                    $scope.servicios = [r[0]];
                                    $scope.datos.servicio = {"id": response[0].id_padre};
                                });
                        } else {

                            $scope.servicios = Servicio.listado({
                                tipoPublicacionId: DataServicios.tipo_publicacion_id,
                                medioId: DataServicios.medio_id,
                                modo: 'parent'

                            });
                            $scope.datos.servicio = {"id": DataServicios.servicio_id};
                        }
                    });
                CondicionModel.listado({
                    tipoPublicacionId: DataServicios.tipo_publicacion_id,
                    servicioId: DataServicios.servicio_id
                }).$promise.then(function (response) {
                        $scope.condiciones = response;
                        $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa'});
                        $scope.condiciones[0].disabled = 'out';
                        $scope.datos.condicion = {id: 0};
                    });
                if (!clone) {
                    $scope.datos.versiones.push({
                        nombre: DataServicios.version_nombre,
                        file_name: DataServicios.version_file,
                        fecha: DataServicios.version_publicacion_fecha,
                        fecha_inicio: DataServicios.version_publicacion_fecha_inicio,
                        fecha_fin: DataServicios.version_publicacion_fecha_fin,
                        apariciones: (DataServicios.version_apariciones == '' ? 1 : DataServicios.version_apariciones * 1),
                        segundos: (DataServicios.version_segundos == '' ? 1 : DataServicios.version_segundos * 1),
                        fecha_unica: DataServicios.fecha_unica
                    });
                }

                angular.forEach(DataServicioscondiciones, function (value) {
                    if (value.condicion_id != null) {
                        $scope.datos.add_condiciones.push({
                            condicion: {
                                id: value.condicion_id,
                                nombre: value.condicion_nombre,
                                servicio_id: value.servicio_id,
                                es_porcentaje: value.es_porcentaje,
                                es_base: value.es_base,
                                label: value.label
                            },
                            detalle: {
                                id: value.condiciones_detalles_id,
                                nombre: value.condiciones_detalles_nombre,
                                condicion_id: value.condicion_id,
                                tarifa: value.tarifa
                            }
                        });

                    }

                });
                $scope.$watch('condiciones', function (newVal) {
                    if (angular.isDefined(newVal)) {
                        if (newVal != null) {
                            angular.forEach($scope.condiciones, function (value, key) {
                                if ($scope.datos.add_condiciones.length > 0) {
                                    angular.forEach($scope.datos.add_condiciones, function (valor) {
                                        if (value.id == valor.condicion.id) {
                                            if (angular.isDefined($scope.condiciones[key]))
                                                $scope.condiciones[key].disabled = 'out'
                                        }

                                    });
                                }

                            });
                        }
                    }
                });
            }
        } else {
            $scope.mensaje_inicial = 'Para iniciar debe seleccionar un proveedor.';
            $scope.mensaje = $scope.mensaje_inicial;
            /**
             * Gestiona la lógica de los combos dependientes existentes en el modal de versiones
             * @param {integer} opc  Opción que se aplicara en el switch de el método
             * @author Frederick Bustamante <frederickdanielb@gmail.com>
             * @version V-1.0 27/08/2015
             */
            $scope.charge_reset_selects = function (opc) {
                var msg = "Si cambia esta opción perderá los datos de tarifas y versiones precargados. Desea continuar?";
                switch (opc) {
                    case 1:
                        if ($scope.datos.add_condiciones.length > 0 || $scope.datos.versiones.length > 0) {
                            if (!window.confirm(msg)) {
                                $scope.datos.medio = $scope.datos.oldMedio;
                                return 1;
                            }
                        }

                        $scope.tiposPublicaciones = [];
                        $scope.datos.tipoPublicacion = {id: 0};
                        $scope.servicios = [];
                        $scope.datos.servicio = {id: 0};
                        $scope.servicios_hijos = [];
                        $scope.datos.servicio_hijo = {id: 0};
                        $scope.condiciones = [];
                        $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
                        $scope.datos.condicion = {id: 0};
                        $scope.detalleCondiciones = [];
                        $scope.datos.add_condiciones = [];
                        $scope.datos.versiones = [];
                        if ($scope.datos.medio != null) {
                            var tipos = [];
                            $scope.mensaje = 'Ahora seleccione un tipo de publicación';
                            if (angular.isObject(rContrato)) {
                                for (var i = rContrato.pautas.length - 1; i >= 0; i--) {
                                    tipos.push(rContrato.pautas[i].tipo);
                                }
                                $scope.tiposPublicaciones = tipos;
                                return 1;
                            }

                            $scope.tiposPublicaciones = TipoPublicacion.listado_for_medio({
                                medioId: $scope.datos.medio.id
                            });
                        }

                        break;
                    case 2:
                        if ($scope.datos.add_condiciones.length > 0 || $scope.datos.versiones.length > 0) {
                            if (!window.confirm(msg)) {
                                $scope.datos.tipoPublicacion = $scope.datos.oldTipoPublicacion;
                                return 1;
                            }
                        }

                        $scope.servicios = [];
                        $scope.datos.servicio = {id: 0};
                        $scope.servicios_hijos = [];
                        $scope.datos.servicio_hijo = {id: 0};
                        $scope.condiciones = [];
                        $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
                        $scope.datos.condicion = {id: 0};
                        $scope.detalleCondiciones = [];
                        $scope.datos.add_condiciones = [];
                        $scope.datos.versiones = [];
                        if ($scope.datos.medio != null) {
                            $scope.servicios = Servicio.listado({
                                tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                medioId: $scope.datos.medio.id,
                                modo: 'parent'
                            });
                            $scope.mensaje = 'Ahora seleccione un medio';
                        }
                        break;
                    case 3:
                        if ($scope.datos.add_condiciones.length > 0 || $scope.datos.versiones.length > 0) {
                            if (!window.confirm(msg)) {
                                $scope.datos.servicio = $scope.datos.oldServicio;
                                return 1;
                            }
                        }

                        $scope.condiciones = [];
                        $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
                        $scope.datos.condicion = {id: 0};
                        $scope.detalleCondiciones = [];
                        $scope.datos.add_condiciones = [];
                        $scope.datos.versiones = [];
                        if ($scope.datos.servicio != null) {
                            CondicionModel.listado({
                                tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                servicioId: (!$scope.con_hijos ? $scope.datos.servicio_hijo.id : $scope.datos.servicio.id)
                            }).$promise.then(function (response) {
                                    $scope.condiciones = response;
                                    $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa'});
                                    $scope.condiciones[0].disabled = 'out';
                                    $scope.datos.condicion = {id: 0};
                                });
                            if ($scope.con_hijos) {
                                $scope.servicios_hijos = [];
                                $scope.servicios_hijos = Servicio.listado({
                                    tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                    medioId: $scope.datos.medio.id,
                                    modo: 'children',
                                    servicioPadre: $scope.datos.servicio.id
                                });
                                $scope.datos.servicio_hijo = {id: 0};

                                $scope.mensaje = 'Ahora puede seleccionar un medio dependiente o seleccionar la tarifa y el detalle de la tarifa que desea agregar';
                            } else {
                                $scope.mensaje = 'Seleccione la tarifa y el detalle de la tarifa que desea agregar';
                            }
                        }
                        break;

                    case 4:
                        $scope.detalleCondiciones = [];
                        break;
                    default:
                }

            }
        }
        /**
         * Prepara las variables que se gestionaran en los combos dependientes
         * @param {integer} opc  Opción que se aplicara en el switch de el método
         * @param {boolean} servicio_hijo Determina si la opción de servicio gestiona a un padre a un hijo
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.chargeSelectChildren = function (opc, servicio_hijo) {
            var negociacion = $injector.get('NegociacionModel', 'chargeSelectChildren');
            if (opc == 3) {
                $scope.negociacion.servicio = negociacion.servicio({servicioId: $scope.datos.servicio.id});
            }
            $scope.con_hijos = (angular.isDefined(servicio_hijo) && servicio_hijo ? true : false);
            $scope.charge_reset_selects(opc);
        };
        /**
         * Provee los detalles de las condiciones al select basado en una condición previamente seleccionada
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.cargarDetalleCondiciones = function () {
            var negociacion = $injector.get('NegociacionModel', 'cargarDetalleCondiciones');
            $scope.negociacion.condicion = negociacion.condicion({condicionId: $scope.datos.condicion.id});
            if ($scope.datos.condicion != null) {
                if ($scope.datos.condicion != '') {
                    $scope.detalleCondiciones = CondicionModel.listado_detalle_condicion({
                        servicioId: $scope.datos.servicio.id,
                        condicionId: $scope.datos.condicion.id
                    });
                    $scope.mensaje = 'Seleccione el detalle de la tarifa';
                } else {
                    flashMessages.show_info('Debe seleccionar un detalle de tarifa para poder agregar a la lista!');
                }

            }
        };

        /**
         * Mensaje de sistema con respecto a las condiciones
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.detalle_condicion_msg = function () {
            if (!angular.isObject($scope.datos.detalleCondiciones)) {
                return 0;
            }
            var negociacion = $injector.get('NegociacionModel', 'detalle_condición_msg');
            $scope.negociacion.tarifa = negociacion.tarifa({tarifaId: $scope.datos.detalleCondiciones.id});
            $scope.mensaje = 'Proceda a agregar las tarifas con sus detalles.';
        };

        /**
         * ejecuta la acción de pre-agregar versiones  al listado
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.agregarVersion = function () {
            $scope.datos.oldTipoPublicacion = $scope.datos.tipoPublicacion;
            $scope.datos.oldMedio = $scope.datos.medio;
            $scope.datos.oldServicio = $scope.datos.servicio;
            $scope.datos.versiones.push({nombre: null});
        };

        /**
         * Ejecuta la acción de agregar condiciones y tarifas al listado inferior
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.agregarCondicionDetalles = function () {
            var add = true;
            if ($scope.datos.detalleCondiciones == null || $scope.datos.detalleCondiciones == '') {
                flashMessages.show_info('Debe seleccionar un detalle para poder agregar a la lista!');
                return false;
            } else {
                angular.forEach($scope.datos.add_condiciones, function (value) {
                    if (angular.isDefined($scope.datos.detalleCondiciones)) {
                        if ($scope.datos.detalleCondiciones.id == value.detalle.id) {
                            flashMessages.show_info('El detalle ya fue agregado a la tarifa!');
                            add = false;
                        }
                    }
                });
            }
            if (add) {
                var next = true;
                angular.forEach($scope.condiciones, function (value, key) {
                    if (value.id == $scope.datos.condicion.id) {
                        if (angular.isDefined($scope.condiciones[key]))
                            $scope.condiciones[key].disabled = 'out'
                    }
                });
                if ($scope.datos.add_condiciones.length > 0) {
                    angular.forEach($scope.datos.add_condiciones, function (value) {
                        if (value.condicion.id == $scope.datos.condicion.id) {
                            flashMessages.show_info('La tarifa ya esta cargada');
                            $scope.datos.condicion = {id: 0};
                            $scope.detalleCondiciones = null;
                            next = false;
                        }

                    });
                }
                if (next) {
                    $scope.datos.oldTipoPublicacion = $scope.datos.tipoPublicacion;
                    $scope.datos.oldMedio = $scope.datos.medio;
                    $scope.datos.oldServicio = $scope.datos.servicio;


                    $scope.datos.add_condiciones.push({
                        condicion: $scope.datos.condicion,
                        negociacion_condicion: $scope.negociacion.condicion.data,
                        detalle: $scope.datos.detalleCondiciones,
                        negociacion_detalle: $scope.negociacion.tarifa.data
                    });
                    $scope.negociacion.condicion = {};
                    $scope.negociacion.tarifa = {};

                    $scope.datos.condicion = {id: 0};
                    $scope.detalleCondiciones = null;
                    $scope.mensaje = 'Puede seleccionar mas (Tarifas-Detalles) si asi lo desea, también debe cargar al menos una version para terminar.';
                }
            }
        };

        /**
         * Ejecuta la acción de remover condiciones y tarifas al listado inferior
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.removerDatos = function (index, arreglo, id) {

            switch (arreglo) {
                case 'version':
                    $scope.datos.versiones.splice(index, 1);
                    break;
                case 'condicion':
                    $scope.datos.add_condiciones.splice(index, 1);
                    angular.forEach($scope.condiciones, function (value, key) {
                        if (value.id == id) {
                            if (angular.isDefined($scope.condiciones[key])) {
                                $scope.condiciones[key].disabled = 'g';
                                $scope.datos.condicion = {id: 0};
                            }
                        }
                    });
                    break;
                default:
                    break;
            }
        };

        /**
         * Ejecuta de crear o actualizar una version con respecto a la cotización
         * @param {string} mode Determina el modo que aplicara el método (Actualizar o Agregar)
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.processData = function (mode) {
            var count_base = 0;
            if ($scope.datos.add_condiciones.length == 0) {
                flashMessages.show_error('Debe cargar al menos un detalle tarifa.');
                return false;
            } else if ($scope.datos.versiones.length == 0) {
                flashMessages.show_error('Debe cargar al menos una version.');
                return false;
            }

            angular.forEach($scope.datos.add_condiciones, function (value) {
                if (value.condicion.es_base == '1') {
                    count_base++;
                }

            });
            if (count_base == 0) {
                flashMessages.show_error('Debe cargar al menos un detalle de una tarifa base.');
                return false;
            }
            if (mode == 'Agregar') {
                $scope.datos.servicio.id = ($scope.datos.servicio_hijo != null && $scope.datos.servicio_hijo.id > 0 ? $scope.datos.servicio_hijo.id : $scope.datos.servicio.id);
                Cotizacion.cotizacion_servicio(
                    {cotizacionId: cotizacionId},
                    $scope.datos,
                    function (response) {
                        if (response !== undefined && response.status == 'ok') {
                            $modalInstance.close();
                            flashMessages.show_success(response.message);
                            if (angular.isDefined($scope.sectionTabs))
                                $scope.sectionTabs[2].active = true;
                        }
                        else {
                            flashMessages.show_error(response.message);
                        }
                    }
                );
            } else if (mode == 'Actualizar') {
                $scope.datos.servicio.id = ($scope.datos.servicio_hijo != null ? $scope.datos.servicio_hijo.id : $scope.datos.servicio.id);
                if (clone) {
                    Cotizacion.cotizacion_servicio(
                        {cotizacionId: cotizacionId},
                        $scope.datos,
                        function (response) {
                            if (response !== undefined && response.status == 'ok') {
                                $modalInstance.close();
                                flashMessages.show_success(response.message);
                                if (angular.isDefined($scope.sectionTabs))
                                    $scope.sectionTabs[2].active = true;
                            }
                            else {
                                flashMessages.show_error(response.message);
                            }
                        }
                    );
                } else {
                    Cotizacion.cotizacion_servicio_update(
                        {
                            cotizacionId: cotizacionId,
                            cotizacionServicioId: $scope.cotizaciones_servicios_id
                        },
                        $scope.datos,
                        function (response) {
                            if (response !== undefined && response.status == 'ok') {
                                $modalInstance.close();
                                flashMessages.show_success(response.message);
                            }
                            else {
                                flashMessages.show_error(response.message);
                            }
                        }
                    );
                }
            }

        };
        /**
         * Método que atacha el archivo a la version
         *
         * @param {array} file_up Provee el arreglo de archivos a subir al servidor
         * @param {integer} index índice referente al ciclo donde se invoca la subida de archivos (ng-repeat de la vista)
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 23/01/2015
         */
        $scope.process_file = function (file_up, index) {
            if (file_up.length > 0) {
                var $compile = $injector.get('$compile', 'Cotización_Condiciones_Ctrl');
                var file = file_up[0];

                $scope.datos.versiones[index].file = file_up[0];
                $scope.datos.versiones[index].file.progress = 0;

                var progressHtml = '<div class="progress" style="position: absolute;margin-left: 3%;width: 87%;margin-top: 5px;">' +
                    '<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{{datos.versiones[' + index + '].file.progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{datos.versiones[' + index + '].file.progress}}%">' +
                    '<span>Subiendo... ({{datos.versiones[' + index + '].file.progress}}%)</span>' +
                    '</div>' +
                    '</div>';
                // parseo el string append a html
                var element = angular.element(progressHtml);
                // compilamos el elemento parseado dentro del $scope
                var html = $compile(element)($scope);
                // agregamos el html del progressbar
                angular.element('#content-upload' + index).html(html);
                // ejecutamos la subida del archivo seleccionado
                cotizacionesMdl.upload_cotizacion(file).progress(function (evt) {
                    file.progress = parseInt(100.0 * evt.loaded / evt.total);
                }).success(function (data) {
                    var icon = 'glyphicon glyphicon-ok-sign';

                    if (!data.success)
                        icon = 'glyphicon glyphicon-warning-sign';

                    if (data.success) {
                        //$scope.name_adjunto = data.file_name;
                        $scope.datos.versiones[index].file_name = data.file_name
                    }

                    // file is uploaded successfully
                    var successHtml = '<div style="text-align: left; position: absolute;margin-left: 3%;width: 87%;margin-top: 5px;">' +
                        '<span class="' + icon + '">' + data.message + '</span>' +
                        '</div>';
                    // parseo el string append a html
                    var ele = angular.element(successHtml);
                    // compilamos el elemento parseado dentro del $scope
                    var append = $compile(ele)($scope);
                    // agregamos el html
                    angular.element('#content-upload' + index).html(append);
                })
            }

        };

        // Para manejo de multiples Datapicker en el formulario
        $scope.calendar = {

            // Maneja el estado de los calendarios
            openedi: {},
            openede: {},

            // Indica el formato de la fecha en los calendarios
            dateFormat: 'dd/MM/yyyy',
            //Opciones del calendario
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },

            // Este atributo es opcional, se utiliza para indicar el min-date o menor día posible a elegir
            now: new Date(),
            /**
             * Método que controla la apertura de los calendarios, dada una clave indicada en el método del click
             *
             * @param $event
             * @param which
             * @param v
             */
            open: function ($event, which, v) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.openedi, function (value, key) {
                    $scope.calendar.openedi[key] = false;
                });
                angular.forEach($scope.calendar.openede, function (value, key) {
                    $scope.calendar.openede[key] = false;
                });
                if (v == 'init') {
                    $scope.calendar.openedi[which] = true;
                }
                if (v == 'end') {
                    $scope.calendar.openede[which] = true;
                }
            },
            /**
             * Método que desactiva la selección de fines de semana en el calendario
             *
             * @param date
             * @param mode
             * @returns {boolean}
             */
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        // Fin de manejo de Datapicker
    }
]);

/**
 * Controlador para mostrar un formulario de selección de cliente
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('CotizacionClienteFormCtrl', ['$scope', '$injector', function ($scope, $injector) {
    var $location = $injector.get('$location', 'CotizacionClienteFormCtrl');
    $scope.codigoTmp = null;
    $scope.datosCliente = null;
    $scope.show = false;

    /**
     * Obtiene un listado de clientes como referencia al texto ingresado en el cuadro
     * @param {string} texto Cadena de búsqueda
     * @returns {*}
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @version V-1.0 27/08/2015
     */
    $scope.getItemsReferencia = function (texto) {
        return $injector.get('clienteMdl', 'getItemsReferencia').referencia({texto: texto}).$promise.then(
            function (response) {
                return response.data;
            });
    };

    /**
     * Dada la selección en las sugerencias, agrega el objeto con todos los datos del cliente
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @version V-1.0 27/08/2015
     */
    $scope.$watch('codigoTmp', function () {
        if ($scope.codigoTmp !== null && typeof $scope.codigoTmp === 'object') {
            $scope.datosCliente = $scope.codigoTmp;
            $scope.show = true;
        } else {
            $scope.show = false;
            $scope.datosCliente = null;
        }
    });

    /**
     * Ejecuta la acción para crear la cotización
     * @author Frederick Bustamante <frederickdanielb@gmail.com>
     * @version V-1.0 27/08/2015
     */
    $scope.crearCotizacion = function () {
        if ($scope.datosCliente == null) {
            $injector.get('flashMessages', 'getItemsReferencia').show_error("Debe seleccionar un cliente de la lista");
            return 0;
        }
        $location.path('admin/cotizacion_crear/' + $scope.datosCliente.id);
    };

    /**
     * Ejecuta la acción para crear la cotización
     */
    $scope.editarCotizacion = function (id) {
        $location.path('admin/cotizacion_editar/' + id);
    }
}]);

/**
 * Controlador para gestionar las Ordenes de Servicios de la Cotización
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('CotizacionOrdenServicioCtrl', [
    "$scope",
    "cotizacionData",
    "$injector",
    "$modalInstance",
    'r_recepcion',
    function ($scope, cotizacionData, $injector, $modalInstance, r_recepcion) {

        /************Inicialización de variables***********/
        var $validationProvider = $injector.get('$validation', 'CotizaciónOrdenServicioCtrl');
        var cotizacionesMdl = $injector.get('cotizacionesMdl', 'CotizaciónOrdenServicioCtrl');
        var $stateParams = $injector.get('$stateParams', 'CotizaciónOrdenServicioCtrl');

        $scope.datos = {
            cliente_id: cotizacionData.cliente_id,
            id: cotizacionData.id
        };
        $scope.recepcion = r_recepcion.data;

        //$scope.datos.via_recepcion_id = $scope.datos.via_recepcion_id.id;
        //console.log($scope.datos.via_recepcion_id);
        $scope.file = null;
        /*****************Fin de Inicialización de variables*******/

        /**
         * Método que atacha el archivo a la version
         *
         * @param {array} file_up Provee el arreglo de archivos a subir al servidor
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 23/01/2015
         */
        $scope.process_file = function (file_up) {
            if (file_up.length > 0) {
                $scope.$apply(function () {
                    $scope.file = file_up[0];
                    $scope.datos.file_name = $scope.file.name;
                });
            }
        };

        /**
         * Ejecuta la acción de crear la orden de servicio en la cotización
         *
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 23/01/2015
         */
        $scope.process_orden_servicio = function () {
            if ($scope.file.length > 0) {
                cotizacionesMdl.save_orden_servicio($scope.file, $scope.datos);
            }
        };

        /**
         * Ejecuta la acción para cancelar el modal
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 23/01/2015
         */
        $scope.cancelar = function () {
            $modalInstance.close(null);
        };

        /**
         * Ejecuta la acción de validación y envío de formulario de creación de Ordenes de Servicio
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 23/01/2015
         */
        $scope.orden_servicio_form = {
            self: this,
            checkValid: $validationProvider.checkValid,
            /**
             * Ejecuta la acción de envío del formulario
             * @param {object} form Formulario gestionado
             */
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.orden_servicio_form.send();
                });
            },
            /**
             * Ejecuta la acción limpiar el formulario
             * @param {object} form Formulario gestionado
             */
            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Prepara los datos antes del envío
             * @return {*}
             * @param params
             */
            prepare: function (params) {
                return angular.copy(params);
            },
            /**
             * Ejecuta la acción envío de datos al servidor (backend)
             */
            send: function () {
                var flashMessages = $injector.get('flashMessages', 'SendData');

                if ($scope.file != null) {
                    cotizacionesMdl.save_orden_servicio(
                        $scope.file,
                        $scope.datos,
                        $scope.datos.via_recepcion_id = $scope.datos.via_recepcion_id.id
                    ).success(function (data) {
                            if (data.status == 'ok') {
                                $modalInstance.close(data.data);
                                flashMessages.show_success("Los datos de la Orden de Servicio fueron almacenados.");
                            }
                        })
                }
            },
            /**
             * Procesa envíos exitosos del formulario
             */
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                // if(response.status == 'ok'){
                $injector.get('flashMessages', 'PS').show_success("Los datos de la Orden de Servicio fueron almacenados.");
                // }

            },
            /**
             * Ejecuta la errores en el formulario
             * @param responseError
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {
                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.nombre)) {
                        mensajeToShow = mensajeToShow.concat(message.nombre);
                    }
                    if (angular.isDefined(message.tipo_publicacion_id)) {
                        mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                    }
                }
                $injector.get('flashMessages', 'pFE').show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        };

        // Para manejo de multiples Datapicker en el formulario
        $scope.calendar = {
            // Maneja el estado de los calendarios
            openedi: {},
            openede: {},
            // Indica el formato de la fecha en los calendarios
            dateFormat: 'dd/MM/yyyy',
            // Opciones del calendario
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            /**
             * Método que controla la apertura de los calendarios, dada una clave indicada en el método del click
             *
             * @param $event
             * @param which
             * @param v
             */
            open: function ($event, which, v) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.openedi, function (value, key) {
                    $scope.calendar.openedi[key] = false;
                });
                angular.forEach($scope.calendar.openede, function (value, key) {
                    $scope.calendar.openede[key] = false;
                });
                if (v == 'init') {
                    $scope.calendar.openedi[which] = true;
                }
                if (v == 'end') {
                    $scope.calendar.openede[which] = true;
                }
            },
            /**
             * Método que desactiva la selección de fines de semana en el calendario
             *
             * @param date
             * @param mode
             * @returns {boolean}
             */
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        // Fin de manejo de Datapicker
    }]);

/**
 * Controlador para verificar la creación de una cotización
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('CotizacionClienteFormCtrlFicha', [
    '$scope', '$location', 'datos', '$state', '$modalInstance',
    function ($scope, $location, datos, $state, $modalInstance) {
        $scope.datosCliente = datos;
        $scope.show = true;
        /**
         * Ejecuta la acción para crear la cotización
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.crearCotizacion = function (id) {
            $modalInstance.close(true);
            $location.path('admin/cotizacion_crear/' + id);
        };
        /**
         * Ejecuta la acción para editar
         * la cotización
         */
        $scope.editarCotizacion = function (id) {
            $modalInstance.close(true);
            $location.path('admin/cotizacion_editar/' + id);
        };

        $scope.cancelar = function () {
            $modalInstance.close(false);
            $state.go('admin.detalle_cliente');
        }
    }
]);

/**
 * Controlador para verificar la aprobación de la cotización de manera total y parcial
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 12/11/2015
 */
app.controller('AprobarCotizacionModalController', [
    '$scope', '$injector', 'rCotizacion', '$modalInstance',
    function ($scope, $injector, rCotizacion, $modalInstance) {
        var flashMessages = $injector.get('flashMessages', 'AprobarCotizaciónMCtrl');
        var $timeout = $injector.get('$timeout', 'AprobarCotizaciónMCtrl');
        var $state = $injector.get('$state', 'AprobarCotizaciónMCtrl');

        $scope.datos = rCotizacion;

        //$state.current.name == 'admin.editar_publicacion_cotizacion.aprobar'
        //$state.current.name == 'admin.editar_publicacion.aprobar'
        // Hay 2 State para Publicar las versiones, eso depende de donde se venga en los states padres.
        $scope.state = {
            cotizacion: false, publicacion: true,
            title: 'Versiones Disponibles para Publicar',
            button: 'Publicar'
        };

        var versiones;
        //console.log($state.current.name);
        if ($state.current.name == 'admin.cotizacion_editar.cotizacion_aprobar') {
            $scope.state = {
                cotizacion: true, publicacion: false,
                title: 'Pautas Disponibles para Aprobación',
                button: 'Aprobar'
            };
            versiones = angular.copy($scope.datos.versiones);
            for (var i = versiones.length - 1; i >= 0; i--) {
                versiones[i].tmp_aprobada = versiones[i].aprobada == 1;
                versiones[i].aprobada = versiones[i].aprobada == 1;
            }
        } else {
            // Necesito filtrar las publicaciones que no poseen incidencias registradas.
            versiones = $injector.get('$filter', 'publicacion.aprobar')('sinIncidencias')(angular.copy($scope.datos.version));
            for (var i = versiones.length - 1; i >= 0; i--) {
                versiones[i].tmp_aprobada = versiones[i].publicado == 1;
                versiones[i].aprobada = versiones[i].publicado == 1;
            }
        }
        $scope.datos.versiones = versiones;


        var verify_observer = function (string) {
            var response = false;
            $.each($scope.$$watchers, function (k, v) {
                if (v.exp == string) {
                    response = true;
                    return false;
                }
            });
            return response;
        };

        var tPeticion = null;

        /**
         *
         * @param finalizar
         * @param aprobar
         */
        $scope.guardar = function (finalizar, aprobar) {
            $scope.send_data = [];
            $scope.send_data = angular.copy($scope.datos);

            if (tPeticion !== null) {
                $timeout.cancel(tPeticion);
            }
            tPeticion = $timeout(function () {
                if ($scope.send_data.descuento === null) {
                    $scope.send_data.descuento = 0;
                }

                if (angular.isDefined(finalizar) && finalizar) {
                    $scope.send_data.es_terminada = true;
                    $scope.send_data.create_code = true;
                }

                if (angular.isDefined(aprobar) && aprobar) {
                    //Se verifica que al menos exista una orden de servicio si no hay contrato valido
                    if (angular.isDefined(aprobar) && aprobar) {
                        if (!$scope.send_data.cliente.contrato_valido && $scope.send_data.orden_servicio.length == 0) {
                            flashMessages.show_info('Debe cargar al menos una orden de servicio para continuar!');
                            //$scope.sectionTabs[4].active = true;
                            return false;
                        }
                    }
                    $scope.send_data.aprobada = true;
                    $scope.send_data.create_code = true;
                }

                if ($scope.state.cotizacion) {
                    $scope.send_data.aprobada = true;
                    var cotizacion = $injector.get('Cotizacion', 'cot.up');

                    //Actualiza la cotización
                    cotizacion.update(
                        {
                            id: $scope.send_data.id,
                            validar_monto: true
                        },
                        $scope.send_data
                    ).$promise.then(
                        function (response) {
                            if (response.status == 'ok') {
                                $scope.datos = response.data;
                                //$scope.view.contrato_bloqueado = true;
                                $modalInstance.close(false);
                                //var $location = $injector.get('$location', 'success');
                                //$location.path('admin/cotizacion_editar/' + $scope.datos.id + '/panel');
                                //if (angular.isDefined(finalizar) && finalizar || angular.isDefined(aprobar) && aprobar) {
                                //    $location.path('admin/cotizacion');
                                //}
                            } else if (response.status == 'fail' && response.errors.code == 9999) {
                                $scope.todos = false;
                                flashMessages.show_error(response.errors.message);
                            }
                        });
                } else if ($scope.state.publicacion) {
                    $injector.get('Publicacion', 'publicación_aprobar_version').update(
                        {id: $scope.send_data.id},
                        $scope.send_data,
                        function (response) {
                            // No evaluamos OK xq el servidor responde 200 siempre y este ok.
                            if (response.status == 'ok') {
                                var msg = 'Publicada las Versiones seleccionadas.';
                                $injector.get('flashMessages', 'success').show_success(msg);
                                //todo sincronizar los datos de versiones en version.
                                $modalInstance.close(false);
                            }
                        },
                        function (response) {
                            var errors = response.data.errors;
                            var mensajeToShow = [];

                            // Indica los nombres de los campos que van a venir en la respuesta de validación
                            var fields = [];
                            for (var i = errors.length - 1; i >= 0; i--) {
                                var message = errors[i].message;
                                if (angular.isString(message)) {
                                    mensajeToShow.push(message);
                                    continue;
                                }

                                for (var j = fields.length - 1; j >= 0; j--) {
                                    if (angular.isDefined(message[fields[j]])) {
                                        mensajeToShow = mensajeToShow.concat(message[fields[j]]);
                                        fields.splice(j);
                                        //break;
                                    }
                                }
                            }
                            $injector.get('flashMessages', 'errors').show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
                        }
                    );
                } else {
                    //Do Nothing
                }
            }, 1);
        };

        $scope.msj_btn = 'Seleccione';
        $scope.contador = 0;
        $scope.check = false;
        $scope.check_todos = false;

        $scope.contador_init = 0;
        angular.forEach($scope.datos.versiones, function (value) {
            if (value.aprobada) {
                $scope.contador_init++;
            }
        });

        if (rCotizacion != 'data' && $scope.contador_init == rCotizacion.versiones.length) {
            $scope.msj_btn = 'Aprobación total aplicada';
            $scope.todos = true;
            $scope.check = true;
            $scope.check_todos = true;
            $scope.contador = $scope.contador_init;
        }

        $scope.check_aprobacion = function () {
            if (!verify_observer('todos')) {
                $scope.$watch('todos', function () {
                    if ($scope.todos) {
                        angular.forEach($scope.datos.versiones, function (value, key) {
                            $scope.datos.versiones[key].tmp_aprobada = true;
                        });
                        $scope.contador = $scope.datos.versiones.length;
                        $scope.check_aprobacion();
                    } else {
                        angular.forEach($scope.datos.versiones, function (value, key) {
                            if ($scope.check == false && !value.aprobada) {
                                $scope.datos.versiones[key].tmp_aprobada = false;
                            }
                        });
                        $scope.contador = 0;
                        $scope.check_aprobacion();
                    }
                });
            }

            $scope.contador = 0;
            $scope.check = false;
            angular.forEach($scope.datos.versiones, function (value) {
                if (value.tmp_aprobada) {
                    $scope.contador++;
                }
            });

            $scope.datos.aprobada = ($scope.contador == $scope.datos.versiones.length);
            $scope.datos.todos = ($scope.contador == $scope.datos.versiones.length);
            $scope.check = ($scope.contador < $scope.datos.versiones.length);

            if ($scope.contador == 0) {
                $scope.msj_btn = 'Seleccione';
            } else if ($scope.contador == $scope.datos.versiones.length) {
                $scope.msj_btn = $scope.state.button + ' Todas';
            } else if ($scope.contador < $scope.datos.versiones.length) {
                $scope.msj_btn = $scope.state.button + ' Selección';
            }
        };

        $scope.cancelar = function () {
            $modalInstance.close(false);
        }
    }
]);