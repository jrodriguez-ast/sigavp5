'use strict';

/**
 *Controlador que gestiona el listado de Productos
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 */
app.controller("NegociacionListController", ["$scope", '$injector', "particularPermission", 'clientID', function ($scope, $injector, particularPermission, clientID) {
    $scope.siteCrud = {
        url: 'medios/' + clientID + '/negociaciones/dtx',
        is_orderable: false
    };
    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {title: 'Código', label: 'codigo'},
        {title: 'Activa', label: 'estado', filter: 'si_no'},
        {
            title: '# Medios',
            label: 'servicios',
            sortable: false,
            filter: 'formatear_condiciones',
            filterParams: 'Servicio'
        },
        {
            title: '# Tarifas',
            label: 'condiciones',
            sortable: false,
            filter: 'formatear_condiciones',
            filterParams: 'Condicion'
        },
        {
            title: '# Detalles',
            label: 'condicion_detalles',
            sortable: false,
            filter: 'formatear_condiciones',
            filterParams: 'Detalle'
        },
        {title: 'Fecha', label: 'fecha_negociacion'}
    ];
    $scope.operation = particularPermission.permission;

    /**
     * Crea el nuevo tab de Negociación en modo edit.
     * @param negociacion_id
     * @param operation_id
     */
    $scope.newTabNegociacion = function (negociacion_id, operation_id) {
        var tab = {
            id: operation_id,
            title: 'Editar Negociación #',
            state: 'admin.detalle_medio.negociacion_edit({negociacion_id:' + negociacion_id + '})',
            object_id: negociacion_id
        };
        $scope.buildTab(tab);
        var $state = $injector.get('$state', 'ServiciosListController.newTabServices');
        $state.go('^.negociacion_edit', {'negociacion_id': negociacion_id});
    };

    /**
     * Crea el nuevo tab de Negociación en modo view.
     * @param negociacion_id
     * @param operation_id
     */
    $scope.newTabNegociacionView = function (negociacion_id, operation_id) {
        var tab = {
            id: operation_id,
            title: 'Ver Negociación #',
            state: 'admin.detalle_medio.negociacion_view({negociacion_id:' + negociacion_id + '})',
            object_id: negociacion_id
        };
        $scope.buildTab(tab);
        var $state = $injector.get('$state', 'ServiciosListController.newTabServicesView');
        $state.go('^.negociacion_view', {'negociacion_id': negociacion_id});
    };

    $scope.printNegociacion = function (negociacion_id) {
        var config = $injector.get('appConfig', 'ServiciosListController.printNegociacion');
        config.printPDF('negociacion', negociacion_id);
    }
}]);

/**
 * Controlador que gestiona la creación de Negociación
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.1  15:43:32
 */
app.controller('NegociacionModalController', ['$scope', '$modalInstance', '$injector', 'rMedio', 'rMedioRIF', function ($scope, $modalInstance, $injector, rMedio, rMedioRIF) {
    // Inyectamos las dependencias a utilizar dentro del Controlador.
    var $validationProvider = $injector.get('$validation', 'NegociaciónModalController');
    var CONSTANTES = $injector.get('CONSTANTES', 'NegociaciónModalController');

    $scope.rif = rMedioRIF.data.rif;
    // En la variable "rMedio.data" se encuentra toda información relacionada de medios,
    // con sus servicios, condiciones del servicios y los detalles de ls condiciones

    // Objeto para el manejo de Negociación.
    $scope.negociacion = {acuerdos: []};

    // Variable de vista.
    $scope.form = {
        medio: {
            medio_id: rMedio.data.id,
            razon_social: rMedio.data.razon_social
        },
        servicios: rMedio.data.servicios,
        negociacion: rMedio.data.negociaciones[0],
        aplicaciones: [{id: 1, nombre: '%'}, {id: 2, nombre: 'Bs'}],
        moment: function (date) {
            return (date) ? moment(date).format('DD-MM-YYYY') : '';
        }
    };

    var master = angular.copy($scope.form);

    // Donde "type_form" es igual al atributo id de la etiqueta form
    // ejm: <form name="TypeForm" id="type_form">
    $scope.formObject = {
        checkValid: $validationProvider.checkValid,
        submit: function (form) {
            $validationProvider.validate(form).success(function () {
                $scope.formObject.send();
            });
        },

        reset: function (form) {
            $validationProvider.reset(form);
        },

        /**
         * Prepara la data de Negociación a ser enviada al servidor.
         * @param  {json} object
         * @return {json}           data preparada.
         * @author Jose Rodriguez
         * @version V-1.0 16/06/15 17:25:32
         */
        prepare: function (object) {
            console.log(object);
            object = angular.copy(object);
            var $filter = $injector.get('$filter', 'prepare');
            var tmp = object.acuerdos;

            for (var i = tmp.length - 1; i >= 0; i--) {
                // Pasamos a Cero ya que es el valor esperado por el WebService
                tmp[i].aplicado_en = (tmp[i].aplicado_en.id == 2) ? 0 : 1;
                tmp[i].monto = $filter('formatMoneyToNumber')(tmp[i].monto);
            }

            if (angular.isDefined(object.tarifa)) {
                object.tarifa = $filter('formatMoneyToNumber')(object.tarifa);
            }
            //object.techo_consumo = $filter('formatMoneyToNumber')(object.techo_consumo);
            return object;
        },

        /**
         * Enviar las Negociación al servidor y recibe las respuestas pertinentes.
         *
         * @author Jose Rodriguez
         * @version V-1.0 15/06/15 17:24:14
         * @return {void}
         */
        send: function () {
            var model = $injector.get('NegociacionModel', 'NegociaciónModalController.send');
            var params = {medio_id: $scope.form.medio.medio_id};
            model.save(
                params,
                $scope.formObject.prepare($scope.negociacion),
                $scope.formObject.success,
                $scope.formObject.errors
            );
        },

        /**
         * Obtiene la respuesta del servidor en caso de exito.
         *
         * @param  {JSON} response Respuesta del servidor
         * @return {void}
         * @author Jose Rodriguez
         * @version V-1.1 15/06/15 14:37:36
         */
        success: function (response) {
            // No evaluamos OK xq el servidor responde 200 siempre y este ok.
            if (response.status == 'ok') {
                var flashMessages = $injector.get('flashMessages', 'NegociaciónModalController.success');
                flashMessages.show_success("Acuerdos registrados con Exito.");
                $modalInstance.close();
            }
        },

        /**
         * Obtiene y muestra los errores de validación generados por laravel,
         * en una interfaz flashMessages Error.
         *
         * @param  {JSON} response Respuesta del servidor
         * @return {void}
         * @author Jose Rodriguez
         * @version V-1.1 15/06/15 14:37:36
         */
        errors: function (response) {
            var errors = response.data.errors;
            var mensajeToShow = [];

            // Indica los nombres de los campos que van a venir en la respuesta de validación
            var fields = ['fecha', 'tarifa', 'acuerdos'];
            for (var i = errors.length - 1; i >= 0; i--) {

                var message = errors[i].message;
                if (angular.isString(message)) {
                    mensajeToShow.push(message);
                    continue;
                }

                for (var j = fields.length - 1; j >= 0; j--) {
                    if (angular.isDefined(message[fields[j]])) {
                        mensajeToShow = mensajeToShow.concat(message[fields[j]]);
                        fields.splice(j);
                        // @todo Investigar xq break rompe ambos ciclos.
                        //break;
                    }
                }
            }

            var flashMessages = $injector.get('flashMessages', '$scope.formObject.errors');
            flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
        },

        /**
         * Agrega los acuerdos registrados en la negociación
         * @param from Indica donde se va agregar el acuerdo.
         * @version V-1.0 29-06-2015 17:30
         * @author Jose Rodriguez
         */
        addAgreement: function (from) {

            // Reducimos el nombre de algunos objetos del $scope para trabajar mas cómodos
            var acuerdos = $scope.negociacion.acuerdos, fm = $scope.form;
            var esta_registrada = false;

            // Verifico si la condición Ya esta agregada en el Arreglo de Acuerdos.
            for (var i = acuerdos.length - 1; i >= 0; i--) {
                var row = acuerdos[i];
                if (from == 'Servicio' && row.to == 'Servicio' && row.servicio && row.servicio.id == fm.servicio.id) {
                    esta_registrada = true;
                } else if (from == 'Condicion' && row.to == 'Condicion' && row.condition && row.condition.id == fm.condicion.id) {
                    esta_registrada = true;
                } else if (from == 'Detalle' && row.to == 'Detalle' && row.detalle && row.detalle.id == fm.condicion_detalle.id) {
                    esta_registrada = true;
                } else {
                    // Do nothing
                }

                if (esta_registrada) {
                    var flashMessages = $injector.get('flashMessages', 'NegociaciónModalController.addAgreement');
                    flashMessages.show_error('Esta condición de negociación ya se encuentra en la lista de registradas.');
                    return;
                }
            }

            // Creo servicio ya que siempre lo agrego.
            var service_tmp = {id: fm.servicio.id, nombre: fm.servicio.nombre};
            var condition_tmp = null, detalle_tmp = null;

            // Evalúo from para saber donde agregar los valores
            if (from != 'Servicio') {
                // Vengo de 'Condicion' o de 'Detalle'
                // Como sea igual registro la condición
                condition_tmp = {
                    id: fm.condicion.id,
                    nombre: fm.condicion.nombre,
                    es_base: fm.condicion.es_base,
                    es_porcentaje: fm.condicion.es_porcentaje
                };

                // Valido si vengo de detalles para agregar el detalle.
                if (from == 'Detalle') {
                    detalle_tmp = {
                        id: fm.condicion_detalle.id,
                        nombre: fm.condicion_detalle.nombre,
                        sustituye: (fm.condicion.es_base == 1),
                        tarifa: fm.condicion_detalle.tarifa
                    }
                }
            }

            var acuerdo = {to: from, servicio: service_tmp, condition: condition_tmp, detalle: detalle_tmp};
            acuerdos.push(acuerdo);
            $scope.form = angular.copy(master);
        },

        /**
         * Elimina un acuerdo según la posición dada.
         * @param index
         * @version V-1.0 29-06-2015 18:15
         * @author Jose Rodriguez
         */
        removeAgreement: function (index) {
            $scope.negociacion.acuerdos.splice(index, 1);
        },

        /**
         * Evalúa el origen del item agregado, según le agrega comportamientos a ese item a nivel de vista.
         * @param item (JSON) sombre el cual se agrega ñla negociación
         * @param spec (String) el originen del item.
         * @author Jose Rodriguez
         * @version V-1.1 06-07-2015 11:35
         */
        evalSpecs: function (item, spec) {
            if (spec == 'exonerado') {
                item.monto = (item.exonerado) ? 0 : '-';
            } else if (spec == 'sustituye') {
                if (!item.detalle.sustituye && item.condition.es_porcentaje == 1) {
                    item.aplicado_en = {id: 1};
                }
            } else if (spec == 'aplicado_en') {

                // IF para cuando la condición agregada es un servicio o condición
                // Recordemos que con el servicio y condición solo se negocian porcentajes.
                // {id:1} apuntado al objeto $scope.form.aplicaciones
                if (item.to == 'Servicio' || item.to == 'Condicion') {
                    item.aplicado_en = {id: 1};
                } else {
                    // Cuando item.to == 'Detalle'
                    if (item.condition.es_base == 1) {
                        item.aplicado_en = {id: 2};
                    }

                    if (item.condition.es_porcentaje == 1) {
                        item.aplicado_en = {id: 1};
                    }
                }
            } else {
                // do nothing
            }
        },

        tabs: [
            './modules/negociaciones/views/steps/tipo_negociacion.html?v=' + CONSTANTES.VERSION,
            './modules/negociaciones/views/steps/condiciones.html?v=' + CONSTANTES.VERSION,
            './modules/negociaciones/views/steps/contratos.html?v=' + CONSTANTES.VERSION,
            './modules/negociaciones/views/steps/finalizar.html?v=' + CONSTANTES.VERSION
        ]
    };

    /**
     * Objeto para la manipulación de los datapicker.
     * @type {{
     *  opened: {fecha: boolean, start: boolean, end: boolean},
     *  format: string,
     *  minDate: string,
     *  maxDate: string,
     *  dateOptions: {startingDay: number, showWeeks: boolean},
     *  open: Function, disabled: Function
     * }}
     * @author Jose Rodriguez
     * @version V-1.1 06-07-2015 11:35
     */
    $scope.calendar = {
        opened: {fecha: false, start: false, end: false},
        format: 'dd/MM/yyyy', minDate: '01-01-2010', maxDate: '01-01-2035',
        dateOptions: {startingDay: 1, showWeeks: false},

        /**
         * Abre el Datepicker en el input adecuado, según quien (which) lo llama.
         * @param $event
         * @param which
         */
        open: function ($event, which) {
            $event.preventDefault();
            $event.stopPropagation();

            angular.forEach($scope.calendar.opened, function (value, key) {
                $scope.calendar.opened[key] = false;
            });

            $scope.calendar.opened[which] = true;
        },

        /**
         * Evalúa las fechas que no podrán ser tomadas en cuanta.
         * @param date
         * @param mode
         * @returns {boolean}
         */
        disabled: function (date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        }
    };

    /**
     * Objeto para gobernar el wizard.
     * @type {{current: number, title: string, titles: string[], next: Function, back: Function, setTitle: Function}}
     */
    $scope.wizard = {
        current: 0,
        title: '',
        titles: [
            'Indique las opciones a negociar',
            'Agregar Tarifas Negociadas',
            'Registro de Pago a Proveedor',
            'Consolidado'
        ],

        /**
         * Actualiza current cuando se hace click en el botón "next".
         * @author Jose Rodriguez
         * @version V-1.0 08-06-2015
         */
        next: function () {
            var neg = $scope.negociacion, current = $scope.wizard.current;
            if (current == 0 && neg.con_condiciones) {
                $scope.wizard.current = 1;
            } else if ((current == 0 || current == 1) && neg.con_contrato) {
                $scope.wizard.current = 2;
            } else if (current == 2 || (current == 1 && !neg.con_contrato)) {
                $scope.wizard.current = 3;
            }
            $scope.wizard.setTitle();
        },

        /**
         * Actualiza current cuando se hace click en el botón "back".
         * @author Jose Rodriguez
         * @version V-1.0 08-06-2015
         */
        back: function () {
            var neg = $scope.negociacion, current = $scope.wizard.current;
            if (current == 0) {
                return;
            } else if (current == 1 || (current == 2 && !neg.con_condiciones)) {
                $scope.wizard.current = 0;
            } else if (current == 2 || (current == 3 && !neg.con_contrato)) {
                $scope.wizard.current = 1;
            } else if (current == 3 && neg.con_contrato) {
                $scope.wizard.current = 2;
            }
            $scope.wizard.setTitle();
        },

        /**
         * Setea el titulo del tap en el modal header.
         * @author Jose Rodriguez
         * @version V-1.0 08-06-2015
         */
        setTitle: function () {
            $scope.wizard.title = $scope.wizard.titles[$scope.wizard.current];
        }
    };

    // Inicializamos en titulo.
    $scope.wizard.setTitle();

}]);

/**
 * Controlador que gestiona la Visualización de Negociación
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0  15:43:32
 */
app.controller('NegociacionTabController', ['$scope', '$injector', 'rNegociacion', 'resolveTabId', function ($scope, $injector, rNegociacion, resolveTabId) {
    $scope.wizard = {current: 3};

    // Objeto para el manejo de Negociación.
    $scope.negociacion = {
        codigo: rNegociacion.data.codigo,
        fecha: rNegociacion.data.fecha_negociacion,
        observaciones: rNegociacion.data.observaciones,
        tarifa: rNegociacion.data.tarifa,
        techo_consumo: rNegociacion.data.techo_consumo,
        acuerdos: []
    };

    var tmp = rNegociacion.data.servicios.concat(rNegociacion.data.condiciones).concat(rNegociacion.data.condicion_detalles);

    for (var i = tmp.length - 1; i >= 0; i--) {
        var acuerdo = {servicio: null, condition: null, detalle: null};

        acuerdo.servicio = {nombre: tmp[i].servicio.nombre};
        acuerdo.exonerado = (tmp[i].exonerado == 1);
        acuerdo.monto = tmp[i].monto;
        acuerdo.aplicado_en = {id: tmp[i].es_porcentaje};

        if (!angular.isUndefined(tmp[i].condicion)) {
            acuerdo.condition = {
                nombre: tmp[i].condicion.nombre,
                es_porcentaje: tmp[i].condicion.es_porcentaje
            };

            if (!angular.isUndefined(tmp[i].detalle)) {
                acuerdo.detalle = {
                    nombre: tmp[i].detalle.nombre,
                    tafifa: tmp[i].detalle.tarifa,
                    sustituye: (tmp[i].sustituye == 1)
                };
            }
        }
        $scope.negociacion.acuerdos.push(acuerdo);
    }

    // Variable de vista.
    $scope.form = {
        isTab: true,
        medio: {
            medio_id: rNegociacion.data.medio.id,
            razon_social: rNegociacion.data.medio.razon_social
        },
        moment: function (date) {
            return (date) ? moment(date).format('DD-MM-YYYY') : '';
        }
    };

    // Verifico el state donde estoy.
    var $state = $injector.get('$state', 'NegociaciónTabController.');
    var title = ($state.current.name == 'admin.detalle_medio.negociacion_view') ? 'Ver' : '';
    $scope.buildTab({
        id: resolveTabId,
        state: $state.current.name + '({negociacion_id: ' + rNegociacion.data.id + '})',
        title: title + ' Negociación #',
        object_id: rNegociacion.data.id
    });
}]);
/* End of file NegociaciónController.js */