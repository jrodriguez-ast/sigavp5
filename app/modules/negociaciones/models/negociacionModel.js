'use strict';

/**
 * Modelo encargado de la manipulacion de la data de Negocaciones.
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 22/06/15 14:23:00
 */
app.factory('NegociacionModel', ["$resource", "appConfig", function ($resource, appConfig) {
    var url = appConfig.getPathApi() + 'medios/medio/:medio_id/negociaciones/:negociacion_id';
    var params = {medio_id: '@medio_id', negociacion_id:'@negociacion_id'};
    var _object = $resource(url, params, {
        update: { method: 'PUT'},
        servicio: {
            method: 'GET',
            url: appConfig.getPathApi() + 'medios/negociaciones/servicio/:servicioId',
            params: {
                servicioId: '@servicioId'
            },
            isArray: false
        },
        condicion: {
            method: 'GET',
            url: appConfig.getPathApi() + 'medios/negociaciones/condicion/:condicionId',
            params: {
                condicionId: '@condicionId'
            },
            isArray: false
        },
        tarifa: {
            method: 'GET',
            url: appConfig.getPathApi() + 'medios/negociaciones/tarifa/:tarifaId',
            params: {
                tarifaId: '@tarifaId'
            },
            isArray: false
        }

    });
    return _object;
}]);