'use strict';

/**
 * @author Maykol Purica.
 */
app.factory('consultaRifModel', ['$resource', 'appConfig', function ($resource, appConfig) {
    return $resource(appConfig.getPathApi() + 'rif/show/:rif', null, {
        rnc: {
            method: 'GET',
            url: appConfig.getPathApi() + 'rif/rnc/:Attr_rif',
            params: {
                Attr_rif: '@Attr_rif'
            },
            isArray: false
        }
    })
}]);