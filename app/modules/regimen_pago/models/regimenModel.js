'use strict';

/**
 * Created by Carlos BLanco.
 */
app.factory('regimenModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'configuracion/regimen/:id', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'configuracion/regimen/:id',
                params: {
                    id: '@id'
                },
                isArray: false
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'configuracion/regimen',
                isArray: false
            },
            delete: {
                method: 'DELETE'
            }
        });
    }
]);