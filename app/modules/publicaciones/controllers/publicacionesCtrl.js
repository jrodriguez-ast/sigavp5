'use strict';

/**
 * Provee el listado de publicaciones
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('PublicacionCtrl', [
    '$scope', 'module_permission', '$injector',
    function ($scope, module_permission, $injector) {
        var flashMessages = $injector.get('flashMessages', 'PublicacionCtrl');
        var $ngBootbox = $injector.get('$ngBootbox','PublicacionCtrl');
        var crudFactory = $injector.get('crudFactory', 'PublicacionCtrl');

        $scope.title = 'Ordenes de Publicación';
        $scope.siteCrud = {url: 'publicaciones/dt', order: [[1, 'desc']]};

        $scope.headerCrud = [
            {title: 'Código', label: 'codigo'},
            {title: 'Cliente', label: 'cliente', searchable: false, orderable: false},
            {title: 'Proveedor', label: 'medio', searchable: false, orderable: false},
            {
                title: 'Medio',
                label: 'version',
                filter: "listado_key_publicacion_medios",
                searchable: false,
                orderable: false
            },
            {title: 'Fecha', label: 'fecha_creacion'},
            {title: 'Estatus', label: 'status', filter: 'estatus_publicaciones', searchable: false, orderable: false}
        ];

        $scope.operation = module_permission.get.data.permission;

        /**
         *
         *
         * @author Amalio Velasquez
         * */
        $scope.borrarPublicacion = function(publicacion_id){

            $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
                function(){
                    var Publicacion = $injector.get('Publicacion','confirm');

                    Publicacion.remove(
                        {
                            id: publicacion_id
                        },
                        function(success){
                            if(success.status == 'ok'){
                                crudFactory.reloadData();
                                flashMessages.show_success(success.message);
                            }
                        },
                        function(error){
                            flashMessages.show_error(error.data.message);
                        }
                    );
                }
                //, function(){}
            );
        }
    }
]);

/**
 * Provee el listado de publicaciones de acuerdo a un Cliente
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('PublicacionCtrlC', [
    '$scope', 'particularPermission', 'clientID',
    function ($scope, particularPermission, clientID) {
        //$scope.title = 'Ordenes de Publicación';
        $scope.siteCrud = {url: 'publicaciones/dt', data: {clientID: clientID}, order: [[1, 'desc']]};

        $scope.headerCrud = [
            {title: 'Código', label: 'codigo'},
            {title: 'Proveedor', label: 'medio', searchable: false, orderable: false},
            {
                title: 'Medio',
                label: 'version',
                filter: "listado_key_publicacion_medios",
                searchable: false,
                orderable: false
            },
            {title: 'Fecha', label: 'fecha_creacion'},
            {title: 'Estatus', label: 'status', filter: 'estatus_publicaciones', searchable: false, orderable: false}
        ];

        $scope.operation = particularPermission.permission;

    }
]);

/**
 * Provee el listado de publicaciones de acuerdo a una cotización
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('PublicacionCotizacionCtrl', [
    '$scope', '$injector', 'module_permission', 'cotizacionData', 'resolveTabId',
    function ($scope, $injector, module_permission, cotizacionData, resolveTabId) {
        $scope.title = 'Ordenes de Publicación para:';
        $scope.title_2 = 'Cotización Nro.' + cotizacionData.codigo;

        $scope.siteCrud = {url: 'publicaciones/dt', data: {cotizacionId: cotizacionData.id}, order: [[1, 'desc']]};

        $scope.headerCrud = [
            {title: 'Código', label: 'codigo'},
            {title: 'Cliente', label: 'cliente', searchable: false, orderable: false},
            {title: 'Proveedor', label: 'medio', searchable: false, orderable: false},
            {
                title: 'Medio',
                label: 'version',
                filter: "listado_key_publicacion_medios",
                searchable: false,
                orderable: false
            },
            {title: 'Fecha', label: 'fecha_creacion'},
            {title: 'Estatus', label: 'status', filter: 'estatus_publicaciones', searchable: false, orderable: false}
        ];
        $scope.operation = module_permission.permission;

        // Verifico el state donde estoy.
        var $state = $injector.get('$state', 'PublicaciónCotizaciónCtrl.');
        if (resolveTabId != null) {
            $scope.buildTab({
                id: resolveTabId,
                state: $state.current.name + '({cotizacion_id: ' + cotizacionData.id + '})',
                title: 'Publicaciones relacionadas con Cotización #',
                object_id: cotizacionData.id
            });
        }
    }
]);
 
/**
 * Permite gestionar la publicación
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('PublicacionFormCtrl', [
    '$scope',
    "$injector",
    "publicacionData",
    "module_permission",
    "dataIncidencias",
    function ($scope, $injector, publicacionData, module_permission, dataIncidencias) {
        var $validationProvider = $injector.get('$validation', 'PublicaciónFormCtrl');
        var flashMessages = $injector.get('flashMessages', 'PublicaciónFormCtrl');
        var $stateParams = $injector.get('$stateParams', 'PublicaciónFormCtrl');
        var Publicacion = $injector.get('Publicacion', 'PublicaciónFormCtrl');
        var $location = $injector.get('$location', 'PublicaciónFormCtrl');
        var appConfig = $injector.get('appConfig', 'PublicaciónFormCtrl');
        var $timeout = $injector.get('$timeout', 'PublicaciónFormCtrl');
        var $modal = $injector.get('$modal', 'PublicaciónFormCtrl');
        var C = $injector.get('CONSTANTES', 'PublicaciónFormCtrl');

        $scope.permisos_estatus = module_permission.permission;
        $scope.apiUrl = appConfig.getPathApi();
        $scope.select_incidencias = (dataIncidencias ? dataIncidencias.data : []);
        $scope.incidencia = {};
        $scope.tmp = {dateSelect: new Date()};
        $scope.pre_data = {iva: 0.00, recargo_avp: 0.00};
        $scope.solo_envio = false;
        $scope.datos = publicacionData;

        $scope.datos.versiones_sin_publicar = getVersionesSinPublicar($scope.datos.version);

        //console.log(publicacionData);
        $scope.datos_send = [];
        $scope.tmp.dateSelect = moment($scope.datos.fecha_expiracion, 'YYYY-MM-DD hh:mm:ss').toDate();
        $scope.datos.fecha_creacion = moment($scope.datos.fecha_creacion, 'YYYY-MM-DD hh:mm:ss').toDate();
        $scope.datos.fecha_expiracion = moment($scope.datos.fecha_expiracion, 'YYYY-MM-DD hh:mm:ss').toDate();

        /**Arreglo de tab para la vista**/
        $scope.sectionTabs = [
            //{title: 'Detalles', content: './modules/publicaciones/views/tab_detalles.html'},
            {title: 'Versiones', content: './modules/publicaciones/views/tab_versiones.html?Q=' + C.VERSION},
            {title: 'Historial Estatus', content: './modules/publicaciones/views/tab_estatus.html?Q=' + C.VERSION},
            {title: 'Incidencias', content: './modules/publicaciones/views/tab_incidencias.html?Q=' + C.VERSION}

            //,{
            //    title: 'PreVisualización',
            //    content: './modules/publicaciones/views/tab_previsualiza_publicaciones.html?Q=' + C.VERSION
            //}
        ];

        /**
         * Ejecuta la acción de recargar el modulo de cotizaciones o redirigir hacia el.
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.cargarCotizacion = function (cotizacion_id) {
            $location.path('admin/cotizacion_editar/' + cotizacion_id);
        };

        /**
         * Ejecuta la acción de invocar el modal para agregar versiones a la publicación
         * @param {integer} version_id Identificador de la version a invocar
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.execute_button = function (version_id) {
            $scope.displayVersion(version_id);
        };

        /**
         * Maneja un timeout para retrasar la petición de actualización con el fin de no sobrecargar las peticiones
         * @type $timeout
         */
        var tPeticion = null;

        /**
         * Permite guardar los avances de la publicación
         * @param {boolean} finalizar Establece si se finalizara o solo se guardaran los avances.
         * @param {string} estatus Estatus que se aplicara a la cotización
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.guardar = function (finalizar, estatus) {
            if (tPeticion !== null) {
                $timeout.cancel(tPeticion);
            }
            $scope.datos.fecha_expiracion = moment($scope.datos.fecha_expiracion, 'YYYY-MM-DD hh:mm:ss').toDate();
            $scope.datos.fecha_creacion = moment($scope.datos.fecha_creacion, 'YYYY-MM-DD hh:mm:ss').toDate();
            tPeticion = $timeout(function () {
                if ($scope.datos.descuento === null) {
                    $scope.datos.descuento = 0;
                }

                if (angular.isDefined(finalizar) && finalizar) {
                    $scope.datos.es_terminada = true;
                } else {
                    $scope.datos.fecha_expiracion = moment($scope.datos.fecha_expiracion, 'YYYY-MM-DD hh:mm:ss').toDate();
                    $scope.datos.fecha_creacion = moment($scope.datos.fecha_creacion, 'YYYY-MM-DD hh:mm:ss').toDate();
                }

                if (angular.isDefined(estatus)) {
                    $scope.datos.estatus = estatus;
                }
                Publicacion.update({id: $scope.datos.id}, $scope.datos).$promise.then(function (response) {
                    if (response.status == 'ok') {
                        $scope.datos = response.data;
                        if (angular.isDefined(finalizar) && finalizar) {
                            //$state.go('^.cotizacion_ver', {'id': $scope.datos.id});
                        }
                    }
                });
            }, 1);
        };

        /* Para manejo de multiples Datapicker en el formulario */
        $scope.calendar = {
            /**
             * Maneja el estado de los calendarios
             */
            opened: {},
            /**
             * Indica el formato de la fecha en los calendarios
             */
            dateFormat: 'dd/MM/yyyy',
            /**
             * Opciones del calendario
             */
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            /**
             * Este atributo es opcional, se utiliza para indicar el min-date o menor día posible a elegir
             */
            now: new Date(),
            /**
             * Método que controla la apertura de los calendarios, dada una clave indicada en el método del click
             *
             * @param $event
             * @param which
             */
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            /**
             * Método que desactiva la selección de fines de semana en el calendario
             *
             * @param date
             * @param mode
             * @returns {boolean}
             */
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        /* Fin de manejo de Datapicker */

        /**
         * Ejecuta la acción volver desde la cotización al listado de cotizaciones
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 22/01/15
         */
        $scope.volver = function () {
            $location.path('admin/listar_publicaciones');
        };

        /**
         * Ejecuta acción de finalizar la publicación sin envío de de correos
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.finalizar = function () {
            var msg = "Procederá a finalizar la publicación, una vez finalizada no podrá editarla. Desea continuar?";
            bootbox.confirm(msg, function (result) {
                if (result) {
                    $scope.guardar(true);
                }
            });
        };

        /**
         * Abre una ventana con una previsualización en pdf de la publicación final
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 24/02/15
         */
        $scope.previsualizar = function () {
         //   if ($scope.check_publicaciones(1)) {
                var ventana = window.open(appConfig.printPDF('PUBLICACION', $scope.datos.id));
             //   if (ventana) {
             //       ventana.close();
            //    }

          //  }
        };

        /**
         * Invoca el modal para agregar Versiones de Publicaciones
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.displayVersion = function (version_id) {
            var modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/publicaciones/views/publicacion_version.html',
                controller: 'PublicacionCondicionesCtrl',
                size: 'xl',
                resolve: {
                    publicacionId: function () {
                        return $stateParams.publicacion_id;
                    },
                    medioId: function () {
                        return publicacionData.medio_id;
                    },
                    datosVersion: function () {
                        if (angular.isDefined(version_id) && version_id != null) {
                            return Publicacion.version_condicion_data({
                                version_id: version_id,
                                publicacion_id: $stateParams.publicacion_id
                            }).$promise.then(function (response) {
                                    if (angular.isDefined(response)) {
                                        return response;
                                    }
                                });
                        }
                    }
                }
            });
            modalInstance.result.then(function (selectedItem) {
                if (angular.isDefined(selectedItem) && selectedItem) {
                    $scope.guardar();
                }
            }, function () {

            });
        };

        /**
         * Verifica entes de ser finalizada la publicación si tiene todas las validaciones pertinentes
         * @params {integer} opc  Opción para determinar el mensaje a mostrar
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.check_publicaciones = function (opc) {
            var msg = [];
            msg[1] = 'Debe cargar al menos una publicación para previsualizar el documento!';
            msg[2] = 'Debe cargar al menos una version para poder finalizar la publicación!';
            if ($scope.datos.versiones.length == 0) {
                flashMessages.show_info(msg[opc]);
                return false;
            }
            return true;
        };

        /**
         * Finaliza la publicación y envía un email a los contactos del medio
         * @params {integer} solo_envio Evalúa si se envía el correo a los contactos del medio
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.finalizar_send = function (solo_envio) {
            if ($scope.check_publicaciones(2)) {
                if (angular.isDefined(solo_envio))
                    $scope.solo_envio = solo_envio;
                var modalInstance = $modal.open({
                    size: 'lg',
                    templateUrl: 'modal_send_email.html',
                    controller: ['$scope', '$modalInstance', 'dataMedio', '$injector', 'versiones', function ($scope, $modalInstance, dataMedio, $injector, versiones) {
                        var $validationProvider = $injector.get('$validation', 'finalizar_send.modalInstance.controller');
                        $scope.datosView = {
                            email_send: [],
                            contactos: [],
                            versiones: versiones
                        };

                        $scope.medio = dataMedio;
                        $scope.medio.correos_electronicos = angular.fromJson($scope.medio.correos_electronicos);
                        if ($scope.medio.correos_electronicos != null && $scope.medio.correos_electronicos.length > 0) {
                            angular.forEach($scope.medio.correos_electronicos, function (value) {
                                $scope.datosView.email_send.push({mail: value, activo: true})
                            });
                        }

                        if ($scope.medio.contactos.length > 0) {
                            angular.forEach($scope.medio.contactos, function (contactos) {
                                if (contactos.correos_electronicos != null && contactos.correos_electronicos.length > 0) {
                                    var tmp = [];
                                    angular.forEach(angular.fromJson(contactos.correos_electronicos), function (correos) {

                                        tmp.push({a: correos.a, activo: true})
                                    });
                                    contactos.correos_electronicos = tmp
                                }
                                $scope.datosView.contactos.push(contactos);


                            });
                        }
                        $scope.cancel = function () {
                            $modalInstance.close(null);
                        };

                        $scope.formObject = {
                            checkValid: $validationProvider.checkValid,
                            submit: function (form) {
                                $validationProvider.validate(form).success(function () {
                                    $modalInstance.close($scope.datosView);
                                });
                            },

                            reset: function (form) {
                                $validationProvider.reset(form);
                            }
                        }
                    }],
                    resolve: {
                        dataMedio: function () {
                            return $scope.datos['medio'];
                        },
                        versiones: function () {
                            return $scope.datos['versiones'];
                        }
                    }
                });
                modalInstance.result.then(function (data) {
                    if (angular.isDefined(data) && data) {
                        $scope.datos.con_email = true;
                        $scope.datos.data_email = data;
                        if ($scope.solo_envio) {
                            $scope.enviar();
                        } else {
                            $scope.guardar(true);
                        }
                    }
                });
            }
        };

        var tPeticionSend = null;
        $scope.enviar = function () {
            if (tPeticionSend !== null) {
                $timeout.cancel(tPeticionSend);
            }
            tPeticionSend = $timeout(function () {
                Publicacion.enviar_publicacion({publicacionId: $scope.datos.id}, $scope.datos).$promise.then(function (response) {
                    if (response.status == 'ok') {
                        $scope.datos = response.data;
                        /*                        if (angular.isDefined(finalizar) && finalizar) {
                         $state.go('^.cotizacion_ver', {'id': $scope.datos.id});
                         }*/
                    }
                    flashMessages.show_info('La publicación se ha enviado a los destinatarios!');
                });
            }, 1);
        };

        /**
         * Ejecuta la acción de  aprobar la publicación
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.aprobar = function (estatus) {
            $scope.sectionTabs[1].active = true;
            if (estatus == 'publicarPublicacion') {
                $injector.get('$state', 'PublicaciónFormCtrl').go(
                    '.aprobar',
                    {'data': $scope.datos}
                );
            }
            /*
             //Código Original
             var msg = '', status = '';
             switch (estatus) {
             case 'aprobarPublicacion':
             msg = 'Se procederá a aprobar la publicación. Desea continuar?';
             status = 'Aprobada';
             break;
             case 'publicarPublicacion':
             msg = 'Se procederá a publicar la publicación. Desea continuar?';
             status = 'Publicada';
             break;
             }

             // Validamos si se cargo el mensaje.
             if (msg === '' && status !== '') {
             return false;
             }

             //Mostramos mensaje de confirmación
             $ngBootbox.confirm(msg)
             .then(function () {
             $scope.guardar(true, status);
             }, function () {
             return false;
             });
             */
        };

        /**
         * Permite suprimir versiones  de la publicación
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 05/11/2015
         */
        $scope.suprimirVersion = function (id_publicacion_servicio) {
            var $ngBootbox = $injector.get('$ngBootbox', 'PublicaciónFormCtrl');
            if ($scope.datos.es_terminada || $scope.datos.aprobada) {
                $ngBootbox.alert('La publicación ha sido finalizada por ello no puede aplicar cambios!');
            } else {
                $ngBootbox.confirm('Se procederá a eliminar la version de forma permanente. Desea continuar?')
                    .then(function () {
                        Publicacion.publicacion_servicio_delete(
                            {
                                publicacionId: $scope.datos.id,
                                publicacionVersionId: id_publicacion_servicio
                            },
                            function (response) {
                                if (response) {
                                    $scope.datos = response;
                                }
                            }
                        );
                    }, function () {
                        return false;
                    });
            }
        };

        /**
         * Para el registro de incidencias.
         * @type {{checkValid: *, submit: Function, reset: Function, prepare: Function, send: Function, success: Function, errors: Function}}
         */
        $scope.formObject = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.formObject.send();
                });
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },

            /**
             * Prepara la data de Negociación a ser enviada al servidor.
             * @param  {json} object
             * @return {json}           data preparada.
             * @author Jose Rodriguez
             * @version V-1.0 16/06/15 17:25:32
             */
            prepare: function (object) {
                var $filter = $injector.get('$filter', 'prepare');
                object = angular.copy(object);
                object.incidencia = object.incidencia.id;
                object.version = object.version.id;
                if (!object.exonerado) {
                    object.monto = $filter('formatMoneyToNumber')(object.monto);
                    object.precio = $filter('formatMoneyToNumber')(object.precio);
                }
                return object;
            },

            /**
             * Enviar las Negociación al servidor y recibe las respuestas pertinentes.
             *
             * @author Jose Rodriguez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                triggerKeyDown('#monto');
                triggerKeyDown('#precio');
                Publicacion.publicacion_incidencia(
                    {publicacionId: publicacionData.id},
                    $scope.formObject.prepare($scope.incidencia), // Objeto de modelo.
                    $scope.formObject.success,
                    $scope.formObject.errors
                );
            },

            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {
                // No evaluamos OK xq el servidor responde 200 siempre y este ok.
                if (response.status == 'ok') {
                    var msg = 'Incidencia Registrada con Exito!!';
                    flashMessages.show_success(msg);
                    $scope.datos = response.data;
                    $scope.datos.versiones_sin_publicar = getVersionesSinPublicar($scope.datos.version);
                    $scope.incidencia = {};
                    $scope.formObject.reset('formName');
                }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (response) {
                var errors = response.data.errors;
                var mensajeToShow = [];

                // Indica los nombres de los campos que van a venir en la respuesta de validación
                var fields = ['incidencia', 'version', 'exonerado', 'monto', 'precio'];

                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    for (var j = fields.length - 1; j >= 0; j--) {
                        if (angular.isDefined(message[fields[j]])) {
                            mensajeToShow = mensajeToShow.concat(message[fields[j]]);
                            fields.splice(j);
                            // @todo Investigar xq break rompe ambos ciclos.
                            //break;
                        }
                    }
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        };

        $scope.view = {
            clase: function (version) {
                if (version.incidencias.length > 0) {
                    return 'warning';
                }

                if (version.publicado == 1) {
                    return 'success';
                }
            }
        };

        /**
         * Filtra y retorna las versiones sin publicar y sin incidencias.
         * @author Jose Rodriguez
         * @param versiones
         * @returns {Array}
         */
        function getVersionesSinPublicar(versiones) {
            var sin_publicar = [];
            for (var i = versiones.length - 1; i >= 0; i--) {
                var version = versiones[i];
                if (version.publicado == 1 || version.incidencias.length > 0) {
                    continue;
                }
                sin_publicar.push(version);
            }
            return sin_publicar;
        }

        /**
         * Dispara un evento keydown de la tecla espacio.
         * @param element
         * @author Jose Rodriguez
         */
        function triggerKeyDown(element) {
            var inputEl = angular.element(element);
            var e = $.Event('keydown');
            e.which = 27;
            inputEl.trigger(e);
        }

    }
]);

/**
 * Controlador que gestiona el modal las Condiciones y las tarifas de las Publicaciones.
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('PublicacionCondicionesCtrl', [
    '$scope', "medioId", "publicacionId", "$modalInstance", "datosVersion", '$injector',
    function ($scope, medioId, publicacionId, $modalInstance, datosVersion, $injector) {

        var Medio = $injector.get('Medio', 'PublicaciónCondicionesCtrl');
        var Servicio = $injector.get('Servicio', 'PublicaciónCondicionesCtrl');
        var $compile = $injector.get('$compile', 'PublicaciónCondicionesCtrl');
        var crudFactory = $injector.get('crudFactory', 'PublicaciónCondicionesCtrl');
        var Publicacion = $injector.get('Publicacion', 'PublicaciónCondicionesCtrl');
        var flashMessages = $injector.get('flashMessages', 'PublicaciónCondicionesCtrl');
        var publicacionMdl = $injector.get('publicacionMdl', 'PublicaciónCondicionesCtrl');
        var CondicionModel = $injector.get('CondicionModel', 'PublicaciónCondicionesCtrl');
        var TipoPublicacion = $injector.get('TipoPublicacion', 'PublicaciónCondicionesCtrl');

        /********************Inicialización de variables a utilizar en el modal**********/
        $scope.edit = (angular.isDefined(datosVersion) ? true : false);
        $scope.mode = (datosVersion != null ? 'Actualizar' : 'Agregar');
        $scope.active_add_conditions = true;
        $scope.con_hijos = false;
        $scope.tiposPublicaciones = TipoPublicacion.listado_for_medio({
            medioId: medioId
        });
        $scope.datos = {
            versiones: (datosVersion ? [datosVersion] : []),
            tipoPublicacion: (datosVersion ? datosVersion.tipo_publicacion_id : null),
            oldTipoPublicacion: null,
            medio: null,
            servicio: null,
            servicio_hijo: null,
            oldMedio: null,
            oldServicio: null,
            condicion: null,
            detalleCondiciones: null,
            add_condiciones: (datosVersion ? datosVersion.versioncondicion : [])
        };
        $scope.condiciones = [];
        $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
        $scope.datos.condicion = {id: 0};
        /*******************Fin de Inicialización de Variables********/

        /**
         * Ejecuta la acción de cancelar el modal
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.cancel = function () {
            $modalInstance.close();
        };

        /*****Tarifa que determina si el modal esta en modo edición o creación********/
        if (datosVersion != null) {
            $scope.mensaje = 'Puede actualizar la información ya cargada si asi lo desea.';
            $scope.publicacion_version_id = datosVersion.id;
            $scope.datos.tipoPublicacion = {
                id: datosVersion.tipo_publicacion_id,
                nombre: datosVersion.tipo_publicacion_name
            };
            Servicio.listado({
                tipoPublicacionId: datosVersion.tipo_publicacion_id,
                medioId: datosVersion.medio_id,
                modo: 'children',
                servicioPadre: false,
                servicioHijo: datosVersion.servicio_id

            }).$promise.then(function (response) {
                    if (response.length > 0) {
                        $scope.servicios_hijos = [response[0]];
                        $scope.datos.servicio_hijo = {"id": response[0].id};
                        Servicio.listado({
                            tipoPublicacionId: datosVersion.tipo_publicacion_id,
                            medioId: datosVersion.medio_id,
                            modo: 'parent'

                        }).$promise.then(function (r) {
                                $scope.servicios = [r[0]];
                                $scope.datos.servicio = {"id": response[0].id_padre};
                            });
                    } else {

                        $scope.servicios = Servicio.listado({
                            tipoPublicacionId: datosVersion.tipo_publicacion_id,
                            medioId: datosVersion.medio_id,
                            modo: 'parent'
                        });
                        $scope.datos.servicio = {"id": datosVersion.servicio_id};
                    }
                });


            CondicionModel.listado({
                tipoPublicacionId: datosVersion.tipo_publicacion_id,
                servicioId: datosVersion.servicio_id
            }).$promise.then(function (response) {
                    $scope.condiciones = response;
                    $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa'});
                    $scope.condiciones[0].disabled = 'out';
                    $scope.datos.condicion = {id: 0};
                });

            $scope.$watch('condiciones', function (newVal) {
                if (angular.isDefined(newVal)) {
                    if (newVal != null) {
                        angular.forEach($scope.condiciones, function (value, key) {
                            if ($scope.datos.add_condiciones.length > 0) {
                                angular.forEach($scope.datos.add_condiciones, function (valor) {
                                    if (angular.isDefined(valor.condicion) && valor.condicion != null) {
                                        if (value.id == valor.condicion.id) {
                                            if (angular.isDefined($scope.condiciones[key]))
                                                $scope.condiciones[key].disabled = 'out'
                                        }
                                    }
                                });
                            }
                        });
                    }
                }
            });
        } else {
            $scope.mensaje_inicial = 'Para iniciar debe seleccionar un tipo de publicación.';
            $scope.mensaje = $scope.mensaje_inicial;

            /**
             * Gestiona la lógica de los combos dependientes existentes en el modal de versiones
             * @param {integer} opc  Opción que se aplicara en el switch de el método
             * @author Frederick Bustamante <frederickdanielb@gmail.com>
             * @version V-1.0 27/08/2015
             */
            $scope.charge_reset_selects = function (opc) {
                var msg = "Si cambia esta opción perderá los datos de tarifas y versiones precargados. Desea continuar?";
                var result = false;
                switch (opc) {
                    case 1:
                        if ($scope.datos.add_condiciones.length > 0 || $scope.datos.versiones.length > 0) {
                            result = window.confirm(msg);
                            if (result) {
                                $scope.medios = [];
                                $scope.servicios = [];
                                $scope.datos.servicio = {id: 0};
                                $scope.servicios_hijos = [];
                                $scope.datos.servicio_hijo = {id: 0};
                                $scope.condiciones = [];
                                $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
                                $scope.datos.condicion = {id: 0};
                                $scope.detalleCondiciones = [];
                                $scope.datos.add_condiciones = [];
                                $scope.datos.versiones = [];
                                if ($scope.datos.tipoPublicacion != null) {
                                    $scope.servicios = Servicio.listado({
                                        tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                        medioId: medioId
                                    });
                                }
                            } else {
                                $scope.datos.tipoPublicacion = $scope.datos.oldTipoPublicacion;
                            }
                            $scope.mensaje = 'Ahora seleccione el servicio.';
                        } else {
                            $scope.medios = [];
                            $scope.servicios = [];
                            $scope.datos.servicio = {id: 0};
                            $scope.servicios_hijos = [];
                            $scope.datos.servicio_hijo = {id: 0};
                            $scope.condiciones = [];
                            $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
                            $scope.datos.condicion = {id: 0};
                            $scope.detalleCondiciones = [];
                            $scope.datos.add_condiciones = [];
                            $scope.datos.versiones = [];
                            if ($scope.datos.tipoPublicacion != null) {
                                $scope.servicios = Servicio.listado({
                                    tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                    medioId: medioId,
                                    modo: 'parent'
                                });
                            }
                        }
                        $scope.mensaje = 'Ahora seleccione el servicio.';
                        break;
                    case 2:
                        if ($scope.datos.add_condiciones.length > 0 || $scope.datos.versiones.length > 0) {
                            result = window.confirm(msg);
                            if (result) {
                                if (result) {
                                    $scope.servicios = [];
                                    $scope.datos.servicio = {id: 0};
                                    $scope.servicios_hijos = [];
                                    $scope.datos.servicio_hijo = {id: 0};
                                    $scope.condiciones = [];
                                    $scope.condiciones.unshift({
                                        id: 0,
                                        nombre: 'Seleccione Tarifa',
                                        disabled: 'out'
                                    });
                                    $scope.datos.condicion = {id: 0};
                                    $scope.detalleCondiciones = [];
                                    $scope.datos.add_condiciones = [];
                                    $scope.datos.versiones = [];
                                    if ($scope.datos.medio != null) {
                                        $scope.servicios = Servicio.listado({
                                            tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                            medioId: medioId,
                                            modo: 'parent'
                                        });
                                    }
                                }
                            } else {
                                $scope.datos.medio = $scope.datos.oldMedio;
                            }

                        } else {
                            $scope.servicios = [];
                            $scope.datos.servicio = {id: 0};
                            $scope.servicios_hijos = [];
                            $scope.datos.servicio_hijo = {id: 0};
                            $scope.condiciones = [];
                            $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
                            $scope.datos.condicion = {id: 0};
                            $scope.detalleCondiciones = [];
                            $scope.datos.add_condiciones = [];
                            $scope.datos.versiones = [];
                            if ($scope.datos.medio != null) {
                                $scope.servicios = Servicio.listado({
                                    tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                    medioId: medioId,
                                    modo: 'parent'
                                });
                            }
                        }
                        break;
                    case 3:
                        if ($scope.datos.add_condiciones.length > 0 || $scope.datos.versiones.length > 0) {
                            result = window.confirm(msg);
                            if (result) {
                                $scope.condiciones = [];
                                $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
                                $scope.datos.condicion = {id: 0};
                                $scope.detalleCondiciones = [];
                                $scope.datos.add_condiciones = [];
                                $scope.datos.versiones = [];
                                if ($scope.datos.servicio != null) {
                                    CondicionModel.listado({
                                        tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                        servicioId: (!$scope.con_hijos ? $scope.datos.servicio_hijo.id : $scope.datos.servicio.id)
                                    }).$promise.then(function (response) {
                                            $scope.condiciones = response;
                                            $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa'});
                                            $scope.condiciones[0].disabled = 'out';
                                            $scope.datos.condicion = {id: 0};
                                        });
                                    if ($scope.con_hijos) {
                                        $scope.servicios_hijos = [];
                                        $scope.servicios_hijos = Servicio.listado({
                                            tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                            medioId: medioId,
                                            modo: 'children',
                                            servicioPadre: $scope.datos.servicio.id
                                        });
                                        $scope.datos.servicio_hijo = {id: 0};
                                        $scope.mensaje = 'Ahora puede seleccionar un medio dependiente o seleccionar la tarifa y el detalle que desea agregar';
                                    } else {
                                        $scope.mensaje = 'Seleccione la tarifa y el detalle que desea agregar';
                                    }

                                }
                            } else {
                                $scope.datos.servicio = $scope.datos.oldServicio;
                            }
                        } else {
                            $scope.condiciones = [];
                            $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa', disabled: 'out'});
                            $scope.datos.condicion = {id: 0};
                            $scope.detalleCondiciones = [];
                            $scope.datos.add_condiciones = [];
                            $scope.datos.versiones = [];
                            if ($scope.datos.servicio != null) {
                                CondicionModel.listado({
                                    tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                    servicioId: (!$scope.con_hijos ? $scope.datos.servicio_hijo.id : $scope.datos.servicio.id)
                                }).$promise.then(function (response) {
                                        $scope.condiciones = response;
                                        $scope.condiciones.unshift({id: 0, nombre: 'Seleccione Tarifa'});
                                        $scope.condiciones[0].disabled = 'out';
                                        $scope.datos.condicion = {id: 0};
                                    });

                                if ($scope.con_hijos) {
                                    $scope.servicios_hijos = [];
                                    $scope.servicios_hijos = Servicio.listado({
                                        tipoPublicacionId: $scope.datos.tipoPublicacion.id,
                                        medioId: medioId,
                                        modo: 'children',
                                        servicioPadre: $scope.datos.servicio.id
                                    });
                                    $scope.datos.servicio_hijo = {id: 0};
                                    $scope.mensaje = 'Ahora puede seleccionar un medio dependiente o seleccionar la tarifa y el detalle que desea agregar';
                                } else {
                                    $scope.mensaje = 'Seleccione la tarifa y el detalle que desea agregar';
                                }
                            }
                        }
                        break;

                    case 4:
                        $scope.detalleCondiciones = [];
                        break;
                    default:
                }

            }
        }

        /**
         * Prepara las variables que se gestionaran en los combos dependientes
         * @param {integer} opc  Opción que se aplicara en el switch de el método
         * @param {boolean} servicio_hijo Determina si la opción de servicio gestiona a un padre a un hijo
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.chargeSelectChildren = function (opc, servicio_hijo) {
            $scope.con_hijos = (angular.isDefined(servicio_hijo) && servicio_hijo ? true : false);
            $scope.charge_reset_selects(opc);
        };

        /**
         * Provee los detalles de las condiciones al select basado en una Tarifa previamente seleccionada
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.cargarDetalleCondiciones = function () {
            if ($scope.datos.condicion != null) {
                if ($scope.datos.condicion != '') {
                    $scope.detalleCondiciones = CondicionModel.listado_detalle_condicion({
                        servicioId: $scope.datos.servicio.id,
                        condicionId: $scope.datos.condicion.id
                    });
                    $scope.mensaje = 'Seleccione el detalle de la Tarifa';
                } else {
                    flashMessages.show_info('Debe seleccionar un detalle para poder agregar a la lista!');
                }

            }
        };
        /**
         * Mensaje de sistema con respecto a las condiciones
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.detalle_condicion_msg = function () {
            $scope.mensaje = 'Proceda a agregar las condiciones con sus detalles.';
        };

        /**
         * Ejecuta la acción de pre-agregar versiones  al listado
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.agregarVersion = function () {
            $scope.datos.oldTipoPublicacion = $scope.datos.tipoPublicacion;
            $scope.datos.oldMedio = $scope.datos.medio;
            $scope.datos.oldServicio = $scope.datos.servicio;
            $scope.datos.versiones.push({nombre: null, medioId: medioId});
        };

        /**
         * Ejecuta la acción de agregar condiciones y tarifas al listado inferior
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.agregarCondicionDetalles = function () {
            var add = true;
            if ($scope.datos.detalleCondiciones == null || $scope.datos.detalleCondiciones == '') {
                flashMessages.show_info('Debe seleccionar un detalle para poder agregar a la lista!');
                return false;
            } else {
                angular.forEach($scope.datos.add_condiciones, function (value) {
                    if (angular.isDefined($scope.datos.detalleCondiciones)) {
                        if ($scope.datos.detalleCondiciones.id == value.detallecondicion.id) {
                            flashMessages.show_info('El detalle ya fue agregado a la tarifa!');
                            add = false;
                        }
                    }
                });
            }
            if (add) {
                var next = true;
                angular.forEach($scope.condiciones, function (value, key) {
                    if (value.id == $scope.datos.condicion.id) {
                        if (angular.isDefined($scope.condiciones[key]))
                            $scope.condiciones[key].disabled = 'out'
                    }

                });
                if ($scope.datos.add_condiciones.length > 0) {
                    angular.forEach($scope.datos.add_condiciones, function (value) {
                        if (value.condicion.id == $scope.datos.condicion.id) {
                            flashMessages.show_info('La Tarifa ya esta cargada');
                            $scope.datos.condicion = {id: 0};
                            $scope.detalleCondiciones = null;
                            next = false;
                        }

                    });
                }
                if (next) {
                    $scope.datos.oldTipoPublicacion = $scope.datos.tipoPublicacion;
                    $scope.datos.oldMedio = $scope.datos.medio;
                    $scope.datos.oldServicio = $scope.datos.servicio;


                    $scope.datos.add_condiciones.push({
                        condicion: $scope.datos.condicion,
                        detallecondicion: $scope.datos.detalleCondiciones
                    });
                    $scope.datos.condicion = {id: 0};
                    $scope.detalleCondiciones = null;
                    $scope.mensaje = 'Puede seleccionar mas (Condiciones-Detalles) si asi lo desea, también debe cargar al menos  una version para terminar.';
                }
            }
        };

        /**
         * Ejecuta la acción de remover condiciones y tarifas al listado inferior
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.removerDatos = function (index, arreglo, id) {
            switch (arreglo) {
                case 'version':
                    $scope.datos.versiones.splice(index, 1);
                    break;
                case 'condicion':
                    $scope.datos.add_condiciones.splice(index, 1);
                    angular.forEach($scope.condiciones, function (value, key) {
                        if (value.id == id) {
                            if (angular.isDefined($scope.condiciones[key])) {
                                $scope.condiciones[key].disabled = 'g';
                                $scope.datos.condicion = {id: 0};
                            }
                        }

                    });
                    break;
                default:
                    break;
            }
        };

        /**
         * Ejecuta de crear o actualizar una version
         * @param {string} mode Determina el modo que aplicara el método (Actualizar o Agregar)
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.processData = function (mode) {
            var count_base = 0;
            if ($scope.datos.add_condiciones.length == 0) {
                flashMessages.show_error('Debe cargar al menos un detalle ');
                return false;
            } else if ($scope.datos.versiones.length == 0) {
                flashMessages.show_error('Debe cargar al menos una version');
                return false;
            }

            angular.forEach($scope.datos.add_condiciones, function (value) {
                if (angular.isDefined(value.condicion) && value.condicion != null) {
                    if (value.condicion.es_base == '1') {
                        count_base++;
                    }
                } else if (angular.isDefined(value.es_base) && value.es_base != null) {
                    if (value.es_base) {
                        count_base++;
                    }
                }

            });
            if (count_base == 0) {
                flashMessages.show_error('Debe cargar al menos un detalle de una tarifa base');
                return false;
            }
            if (mode == 'Agregar') {
                $scope.datos.medioId = medioId;
                $scope.datos.servicio.id = ($scope.datos.servicio_hijo != null && $scope.datos.servicio_hijo.id > 0 ? $scope.datos.servicio_hijo.id : $scope.datos.servicio.id);
                Publicacion.publicacion_version(
                    {publicacion_id: publicacionId},
                    $scope.datos,
                    function (response) {
                        if (response !== undefined && response.status == 'ok') {
                            $modalInstance.close(true);
                            flashMessages.show_success(response.message);
                            if (angular.isDefined($scope.sectionTabs))
                                $scope.sectionTabs[0].active = true;
                        }
                        else {
                            flashMessages.show_error(response.message);
                        }
                    }
                );
            } else if (mode == 'Actualizar') {
                $scope.datos.servicio.id = ($scope.datos.servicio_hijo != null && $scope.datos.servicio_hijo.id > 0 ? $scope.datos.servicio_hijo.id : $scope.datos.servicio.id);
                Publicacion.publicacion_version_update(
                    {
                        publicacionId: publicacionId,
                        publicacionVersionId: $scope.publicacion_version_id
                    },
                    $scope.datos,
                    function (response) {
                        if (response !== undefined && response.status == 'ok') {
                            $modalInstance.close(true);
                            flashMessages.show_success(response.message);
                        }
                        else {
                            flashMessages.show_error(response.message);
                        }
                    }
                );
            }

        };

        /**
         * Método que atacha el archivo a la version
         *
         * @param {array} file_up Provee el arreglo de archivos a subir al servidor
         * @param {integer} index Indice referente al ciclo donde se invoca la subida de archivos (ng-repeat de la vista)
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 23/01/2015
         */
        $scope.process_file = function (file_up, index) {
            if (file_up.length > 0) {
                var file = file_up[0];
                $scope.datos.versiones[index].file = file_up[0];
                $scope.datos.versiones[index].file.progress = 0;

                var progressHtml = '<div class="progress" style="position: absolute;margin-left: 3%;width: 87%;margin-top: 5px;">' +
                    '<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{{datos.versiones[' + index + '].file.progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{datos.versiones[' + index + '].file.progress}}%">' +
                    '<span>Subiendo... ({{datos.versiones[' + index + '].file.progress}}%)</span>' +
                    '</div>' +
                    '</div>';
                // parseo el string append a html
                var element = angular.element(progressHtml);
                // compilamos el elemento parseado dentro del $scope
                var html = $compile(element)($scope);
                // agregamos el html del progressbar
                angular.element('#content-upload' + index).html(html);
                // ejecutamos la subida del archivo seleccionado
                publicacionMdl.upload_cotizacion(file).progress(function (evt) {

                    file.progress = parseInt(100.0 * evt.loaded / evt.total);

                }).success(function (data) {

                    var icon = 'glyphicon glyphicon-ok-sign';

                    if (!data.success)
                        icon = 'glyphicon glyphicon-warning-sign';

                    if (data.success) {
                        //$scope.name_adjunto = data.file_name;
                        $scope.datos.versiones[index].version_file = data.file_name
                    }

                    // file is uploaded successfully
                    var suceesHtml = '<div style="text-align: left; position: absolute;margin-left: 3%;width: 87%;margin-top: 5px;">' +
                        '<span class="' + icon + '">' + data.message + '</span>' +
                        '</div>';
                    // parseo el string append a html
                    var ele = angular.element(suceesHtml);
                    // compilamos el elemento parseado dentro del $scope
                    var append = $compile(ele)($scope);
                    // agregamos el html
                    angular.element('#content-upload' + index).html(append);
                })
            }

        };

        /* Para manejo de multiples Datapicker en el formulario */
        $scope.calendar = {
            /**
             * Maneja el estado de los calendarios
             */
            openedi: {},
            openede: {},
            /**
             * Indica el formato de la fecha en los calendarios
             */
            dateFormat: 'dd/MM/yyyy',
            /**
             * Opciones del calendario
             */
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            /**
             * Este atributo es opcional, se utiliza para indicar el min-date o menor día posible a elegir
             */
            now: new Date(),
            /**
             * Método que controla la apertura de los calendarios, dada una clave indicada en el método del click
             *
             * @param $event
             * @param which
             * @param v
             */
            open: function ($event, which, v) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.openedi, function (value, key) {
                    $scope.calendar.openedi[key] = false;
                });
                angular.forEach($scope.calendar.openede, function (value, key) {
                    $scope.calendar.openede[key] = false;
                });
                if (v == 'init') {
                    $scope.calendar.openedi[which] = true;
                }
                if (v == 'end') {
                    $scope.calendar.openede[which] = true;
                }
            },
            /**
             * Método que desactiva la selección de fines de semana en el calendario
             *
             * @param date
             * @param mode
             * @returns {boolean}
             */
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        /* Fin de manejo de Datapicker */

    }
]);

/**
 * Controlador para mostrar un formulario de selección de medio
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('publicacionMedioFormCtrl', [
    '$scope', 'Publicacion', '$location', 'flashMessages', 'Medio', 'clienteMdl',
    function ($scope, Publicacion, $location, flashMessages, Medio, clienteMdl) {

        $scope.codigoTmpM = null;
        $scope.codigoTmp = null;

        $scope.datosMedio = null;
        $scope.datosCliente = null;

        $scope.showM = false;
        $scope.show = false;

        /**
         * Obtiene un listado de Medios como referencia al texto ingresado en el cuadro
         * @param {string} texto Cadena de búsqueda
         * @returns {*}
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.getItemsReferenciaM = function (texto) {
            return Medio.referencia({texto: texto}).$promise.then(function (response) {
                return response.data;
            });
        };

        /**
         * Obtiene un listado de Clientes como referencia al texto ingresado en el cuadro
         * @param {string} texto Cadena de búsqueda
         * @returns {*}
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.getItemsReferencia = function (texto) {
            return clienteMdl.referencia({texto: texto}).$promise.then(function (response) {
                return response.data;
            });
        };

        /**
         * Dada la selección en las sugerencias, agrega el objeto con todos los datos del medio
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.$watch('codigoTmpM', function () {
            if ($scope.codigoTmpM !== null) {
                if (typeof $scope.codigoTmpM === 'object') {
                    $scope.datosMedio = $scope.codigoTmpM;
                    $scope.showM = true;
                }
                else {
                    $scope.showM = false;
                    $scope.datosMedio = null;
                }
            }
        });

        /**
         * Dada la selección en las sugerencias, agrega el objeto con todos los datos del cliente
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.$watch('codigoTmp', function () {
            if ($scope.codigoTmp !== null) {
                if (typeof $scope.codigoTmp === 'object') {
                    $scope.datosCliente = $scope.codigoTmp;
                    $scope.show = true;
                }
                else {
                    $scope.show = false;
                    $scope.datosCliente = null;
                }
            }
        });

        /**
         * Ejecuta la acción para crear la publicación
         * @author Frederick Bustamante <frederickdanielb@gmail.com>
         * @version V-1.0 27/08/2015
         */
        $scope.crearPublicacion = function () {
            if ($scope.datosMedio != null && $scope.datosCliente != null) {
                //$location.path('admin/publicacion_crear/' + $scope.datosCliente.id+'/'+$scope.datosMedio.id);
                Publicacion.save({
                    clienteId: $scope.datosCliente.id,
                    medioId: $scope.datosMedio.id
                }).$promise.then(function (response) {
                        $location.path('admin/editar_publicacion/' + response.id);
                    });
            }
            else {
                flashMessages.show_error("Debe seleccionar un proveedor de la lista");
            }

        }
    }
]);


/**
 * Controlador para gestionar las Incidencias de la Publicación
 *
 * @author Frederick Bustamante <frederickdanielb@gmail.com>
 * @package SIGAVP
 * @version V-1.0 27/08/2015
 */
app.controller('listarAllIncidencias', [
    '$scope', 'particularPermission', 'clientID',
    function ($scope, particularPermission, clientID) {
        //$scope.title = 'Ordenes de Publicación';
        $scope.siteCrud = {url: 'publicaciones/publicacion_incidencias_dt', data: {clientID: clientID}};
        $scope.headerCrud = [
            {title: 'ID', label: 'id', visible: false},
            {title: 'Código Publicación', label: 'codigo', visible: true},
            {title: 'Incidencia', label: 'incidencia', visible: true},
            {title: 'Reportada por', label: 'creator', visible: true},
            {title: 'Fecha de Creación', label: 'created_at', visible: true}
        ];
        $scope.operation = particularPermission.permission;

    }
]);