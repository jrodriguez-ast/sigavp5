'use strict';

app.factory('Publicacion', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'publicaciones/publicacion/:id', null, {
            update: {
                method: 'PUT' // this method issues a PUT request
            },
            pendiente: {
                method: 'GET',
                url: appConfig.getPathApi() + 'publicaciones/pendiente/:clienteId',
                params: {id: '@_clienteId'}
            },
            publicacion_version: {
                method: 'POST',
                url: appConfig.getPathApi() + 'publicaciones/publicacion/:publicacion_id/versiones',
                params: {publicacion_id: '@publicacion_id'},
                isArray: false
            },
            publicacion_incidencia: {
                method: 'POST',
                url: appConfig.getPathApi() + 'publicaciones/publicacion/:publicacionId/incidencias',
                params: {publicacionId: '@publicacionId'},
                isArray: false
            },
            version_condicion_data: {
                method: 'GET',
                url: appConfig.getPathApi() + 'publicaciones/publicacion/:publicacion_id/versiones/:version_id',
                params: {
                    version_id: '@version_id',
                    publicacion_id: '@publicacion_id'
                },
                isArray: false
            },
            publicacion_version_update: {
                method: 'PUT',

                url: appConfig.getPathApi() + 'publicaciones/publicacion/:publicacionId/versiones/:publicacionVersionId',
                params: {
                    publicacionId: '@publicacionId',
                    publicacionVersionId: '@publicacionVersionId'
                },
                isArray: false
            },
            enviar_publicacion: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'publicaciones/enviarPublicacion/:publicacionId',
                params: {publicacionId: '@publicacionId'}
            },
            publicacion_servicio_delete: {
                method: 'DELETE',

                url: appConfig.getPathApi() + 'publicaciones/publicacion/:publicacionId/versiones/:publicacionVersionId',
                params: {
                    publicacionId: '@publicacionId',
                    publicacionVersionId: '@publicacionVersionId'
                },
                isArray: false
            },
            delete: {
                method: 'DELETE'
            }
        });
    }
]);

/**
 * Factory para administrar funciones que no están atadas al api REST estándar
 */
app.factory('publicacionMdl', ["appConfig", "$upload", function (appConfig, $upload) {
    return {
        upload_cotizacion: function (file) {
            var _url = appConfig.getPathApi() + 'storage/create';
            return $upload.upload({
                url: _url,
                headers: {'Content-Type': file.type},
                method: 'POST',
                file: file
                //}).error(function (success, error, progress) {
                //console.log(success, error, progress);
            });
        }
    };
}
]);