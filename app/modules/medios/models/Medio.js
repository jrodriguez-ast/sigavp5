'use strict';

/**
 * Created by darwin on 01/06/15.
 */
app.factory('Medio', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'medios/medios/:id', null, {
            update: {
                method: 'PUT',
                isArray: false
            },
            listado: {
                method: 'GET',
                url: appConfig.getPathApi() + 'medios/listado/:tipoPublicacionId',
                params: {
                    tipoPublicacionId: '@tipoPublicacionId'
                },
                isArray: true
            },
            getbyrif: {
                method: 'GET',
                url: appConfig.getPathApi() + 'medios/rif/:rif',
                params: {
                    rif: '@rif'
                },
                isArray: false
            },
            children: {
                method: 'GET', params: {id: '@id'},
                url: appConfig.getPathApi() + 'medios/:id/children'
            },
            referencia: {
                method: 'GET',
                url: appConfig.getPathApi() + 'medios/referencia/:texto',
                params: {texto: '@_texto'}
            },
            moverLogo: {
                method: 'POST',
                url: appConfig.getPathApi() + 'medios/moverLogo/:clientID',
                params: {
                    clientID: '@clientID'
                }
            },
			saldo: {
                method: 'GET',
                url: appConfig.getPathApi() + 'medios/:id/saldo',
                params: {id: '@id'}
            },
            tipos: {
                method: 'GET',
                url: appConfig.getPathApi() + 'medios/tipo',
            },
            listar: {
                method: 'GET',
                url: appConfig.getPathApi() + 'medios/cadenas',
                params: null,
                isArray: true
            },
            listChildren: {
                method: 'GET', params: {id: '@id', tipoPub: '@tipoPub'},
                url: appConfig.getPathApi() + 'medios/:id/:tipoPub/servicios',
                isArray: true
            },
            listCadenas: {
                method: 'GET', params: {id: '@id', tipoPub: '@tipoPub'},
                url: appConfig.getPathApi() + 'medios/:id/:tipoPub/cadenas',
                isArray: true
            },
            delete: {
                method: 'DELETE'
            }
        })
    }
]);

app.factory('clienteLogo', [
    "$http", "appConfig", "flashMessages", "$log", "$upload",
    function ($http, appConfig, flashMessages, $log, $upload) {

        return {
            save:function(){},
            upload_logo: function (file) {
                var _url = appConfig.getPathApi() + 'medios/guardarLogo';
                return $upload.upload({

                    url: _url,
                    headers: {'Content-Type': file.type},
                    method: 'POST',
                    file: file
                }).error(function (success, error, progress) {
                    //console.log(success, error, progress);
                });

            }
        };
    }
]);