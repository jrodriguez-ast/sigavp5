'use strict';

app.factory('balanceMedioModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        var _url = appConfig.getPathApi() + 'medios/medios/:clienteID/balance';
        var data = $resource(_url, null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'medios/balance/:balanceID',
                isArray: false,
                params: {balanceID: '@balanceID'}
            },
            detalleBalance: {
                method: 'GET',
                url: appConfig.getPathApi() + 'medios/balance/:balanceID',
                params: {balanceID: '@balanceID'}
            },
        });
        return data;
    }

]);