'use strict';

/**
 * Created by Maykol Purica.
 */
app.factory('ContactoMedio', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'medios/medio/:medioId/contacto/:contactoId', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'medios/medio/:medioId/contacto/:contactoId',
                params: {
                    medioId: '@medioId',
                    contactoId: '@contactoId'
                },
                isArray: false
            },
            listado: {
                method: 'GET',
                url: appConfig.getPathApi() + 'medios/:medioId/:tipoPublicacionId/servicios/listado',
                params: {
                    medioId: '@medioId',
                    tipoPublicacionId: '@tipoPublicacionId'
                },
                isArray: true
            },
            crear: {
                method: 'POST', // medios/medio/{medio}/servicio
                url: appConfig.getPathApi() + 'medios/medio/:medioId/contacto',
                params: {
                    medioId: '@medioId'
                },
                isArray: false
            }

        });
    }
]);