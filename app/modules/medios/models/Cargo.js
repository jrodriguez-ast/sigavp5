'use strict';

app.factory('CargoMdl', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        var _url = appConfig.getPathApi() + 'cargos/:cargoId';
        var data = $resource(_url, null, {
            update: {
                url: 'clientes/position/positions',
                pathApi: appConfig.getAdminPathApi(),
                Method: 'POST'
            },
            crear: {
                // medios/medio/{medio}/servicio
                url: appConfig.getAdminPathApi()+ 'cargos',
                isArray: false,
                Method: 'POST'
            }
        });
        return data;
    }

]);