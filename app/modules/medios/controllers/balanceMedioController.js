'use strict';
/**
 * @author Maykol Purica <puricamaykol@gmail.com>
 * @return mixed
 * Controlador que gestiona la interfaz del balance del cliente de AVP
 */
app.controller("balanceMedioListController", [
    "$scope",
    "particularPermission",
    "clientID",
    "rSaldo",
    "DTInstances",
    "$injector",
    "appConfig",
    function ($scope,
              particularPermission,
              clientID,
              rSaldo,
              DTInstances,
              $injector,
              appConfig) {
        $scope.title = "Balance de AVP con el Proveedor";
        $scope.saldo = rSaldo.data.saldo;
        $scope.filtro = {};
        $scope.filtro.todos = true;

        //Se inician los calendarios y ambas fechas del filtro en la fecha actual
        var toDay = new Date();
        var hoy = moment(toDay).format('YYYY-MM-DD');
        $scope.tipo = [{id: 0, nombre: 'Todos'}, {id: 1, nombre: 'Cargos'}, {id: 2, nombre: 'Débitos'}];
        $scope.filtro.tipo = $scope.tipo[0];
        $scope.siteCrud = {
            url: 'medios/balanceMedioDt/' + clientID,
            is_orderable: false,
            data:{
                dateStart: hoy,
                dateEnd:hoy,
                tipomovimiento: 'Todos',
                todos: true
            }
        };
        $scope.headerCrud = [
            {title: 'id', label: 'id', visible: false, orderable: false},
            {title: 'cliente_id', label: 'cliente_id', visible: false, orderable: false},
            {title: 'orden_publicacion', label: 'orden_publicacion', visible: false, orderable: false},
            {title: 'Saldo (Bs.)', label: 'saldo', orderable: false, filter: 'moneySaldo'},
            {title: 'usuario_id', label: 'usuario_id', visible: false, orderable: false},
            {title: 'Débito (Bs.) ', label: 'debito', filter: 'formatNumberMoney', orderable: false},
            {title: 'Cargo (Bs.)', label: 'cargo', filter: 'formatNumberMoney', orderable: false},
            {title: 'Observaciones', label: 'observaciones', orderable: false},
            {title: 'Fecha', label: 'created_at', orderable: false}
        ];
        $scope.operation = particularPermission.permission;

        /**
         * Objeto del calendario
         */

        $scope.calendar = {
            opened: {},
            now: new Date(),
            dateFormat: 'dd/MM/yyyy',
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        /**
         *Funcion que carga los datos del filtro y recarga la tabla
         */
        $scope.filtrar = function(){
            DTInstances.getLast().then(function (lastDTInstance) {
                $scope.setUpFilter();
                lastDTInstance.reloadData();
            });
        };
        /**
         * Funcion que gestiona la apertura de la pestana de detalles de la operacion del balance
         * @param balanceID
         * @param operation_id
         */
        $scope.detalleOperacionMedio = function(balanceID, operation_id) {
            var tab = {
                id:operation_id,
                title:'Detalle Operación #',
                state:'admin.detalle_medio.detalles_balance({balanceID:'+balanceID+'})',
                object_id:balanceID
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state','balanceMedioListController.detalleOperacionMedio');
            $state.go('^.detalles_balance',{'balanceID':balanceID});
        };
        /**
         * Funcion que gestiona la pestana de observaciones de la operacion
         * @param balanceID
         * @param operation_id
         */
        $scope.addObservacionMedio = function(balanceID, operation_id) {
            var tab = {
                id:operation_id,
                title:'Observaciones Operación #',
                state:'admin.detalle_medio.addobservacion_balance({balanceID:'+balanceID+'})',
                object_id:balanceID
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state','balanceMedioListController.addObservacionMedio');
            $state.go('^.addobservacion_balance',{'balanceID':balanceID});
        };

        $scope.imprimir = function () {
            $scope.setUpFilter();
            window.open(appConfig.getPathApi() + 'clientes/balanceClienteDt/'+clientID+'?reporte=true&dateStart='+$scope.siteCrud.data.dateStart+'&dateEnd='+$scope.siteCrud.data.dateEnd+'&tipomovimiento='+$scope.siteCrud.data.tipomovimiento+'&todos='+$scope.siteCrud.data.todos);
        };
        /**
         * Esta funcion organiza los parametros de los filtros usados en DataTables y en el reporte
         */
        $scope.setUpFilter = function(){
            $scope.siteCrud.data.dateStart = moment($scope.filtro.fecha_desde).format('YYYY-MM-DD');
            //Si la fecha "Hasta" no esta definida se setea con la fecha actual
            if($scope.filtro.fecha_hasta ){
                $scope.siteCrud.data.dateEnd = angular.isDefined(moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD') : moment(toDay).format('YYYY-MM-DD');
            }else{
                $scope.siteCrud.data.dateEnd = moment(toDay).format('YYYY-MM-DD');
            }
            if($scope.filtro.fecha_desde ){
                $scope.siteCrud.data.dateStart = angular.isDefined(moment($scope.filtro.fecha_desde).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_desde).format('YYYY-MM-DD') : false;
            }else{
                $scope.siteCrud.data.dateStart = false;
            }
            $scope.siteCrud.data.tipomovimiento = $scope.filtro.tipo.nombre;
            $scope.siteCrud.data.todos = $scope.filtro.todos;
        }

    }]);
app.controller('balanceMedioDetalleController', [
    '$scope',
    '$stateParams',
    'rBalance',
    '$injector',
    'rAction',
    'balanceID','resolveTabId',
    function ($scope, $stateParams, rBalance,$injector,rAction,balanceID,resolveTabId) {
        $scope.balance = rBalance.data;
        $scope.formAction = rAction;
        var $validationProvider = $injector.get('$validation');
        $scope.balance_form = {
            self: this,
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.balance_form.send();
                });
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },
            prepare: function (balance) {
                return angular.copy(balance);
            },
            send: function () {
                var balanceModel = $injector.get('balanceMedioModel');
                if ($scope.formAction == 'update') {
                    var params = {balanceID: balanceID };
                    balanceModel.update(
                        params,
                        $scope.balance_form.prepare($scope.balance),
                        $scope.balance_form.success,
                        $scope.balance_form.errors
                    );
                }
            },
            success: function () {
                var flashMessages = $injector.get('flashMessages');
                var $location = $injector.get('$location');
                flashMessages.show_success("Datos guardados exitosamente.");
                $location.path("admin/detalle_medio/"+$scope.balance.medio_id+"/balance");
            }
        };

        var tab = {id:resolveTabId, object_id:balanceID};
        var $state = $injector.get('$state','balanceMedioListController');
        if(rAction == 'update'){
            tab.title = 'Observaciones Operación #';
            tab.state ='admin.detalle_medio.addobservacion_balance({balanceID:'+balanceID+'})';

            $scope.buildTab(tab);
            $state.go('^.addobservacion_balance',{'balanceID':balanceID});
        } else {
            tab.title = 'Detalle Operación #';
            tab.state = 'admin.detalle_medio.detalles_balance({balanceID:'+balanceID+'})';

            $scope.buildTab(tab);
            $state.go('^.detalles_balance',{'balanceID':balanceID});
        }
    }
]);