'use strict';
/**
 *Controlador que gestiona el listado de clientes
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 * @package AVP
 *
 */
app.controller("listarMedios", ["$scope", "permission", '$injector',function ($scope, permission, $injector) {
    var flashMessages = $injector.get('flashMessages', 'EstadoCtrlC');
    var $ngBootbox = $injector.get('$ngBootbox','EstadoCtrlC');
    var crudFactory = $injector.get('crudFactory', 'EstadoCtrlC');

    $scope.title = "Proveedores";
    $scope.siteCrud = {url: 'medios/dt'};

    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {title: 'Razón social', label: 'razon_social', visible: true},
        {title: 'R.I.F.', label: 'rif', visible: true},
        {title: 'Nombre comercial', label: 'nombre_comercial', visible: true}

    ];

    $scope.operation = permission.data.get.data.permission;

    /**
     *
     *
     * @author Amalio Velasquez
     * */
    $scope.borrarProveedor = function(id){

        $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
            function(){
                var proveedor = $injector.get('Medio','confirm');

                proveedor.remove(
                    {
                        id: id
                    },
                    function(success){
                        if(success.status == 'ok'){
                            crudFactory.reloadData();
                            flashMessages.show_success(success.message);
                        }
                    },
                    function(error){
                        flashMessages.show_error(error.data.message);
                    }
                );
            }
            //, function(){}
        );
    }
}]);

app.controller("DetallesMediosController", ["$scope", "Medio", function ($scope, Medio) {
    $scope.detalles = {};
    $scope.medio = Medio.get({id: globalData.data.medioId}, function (respuesta) {
        $scope.detalles = respuesta.data;
    });
    //$scope.cliente.update({id: $scope.cliente.data.id});
}]);

/**
 * Controlador que gestiona el modal de detalles de servicios
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 * @package AVP
 */
app.controller("serviceDetailModalCtrl", [
    "$scope",
    "$modalInstance",
    "datos",
    "$injector",
    "r_circulacion",
    "r_target",
    "r_redSocial",
    function ($scope,
              $modalInstance,
              datos,
              $injector,
              r_circulacion,
              r_target,
              r_redSocial) {

        var globalData = $injector.get('globalData', 'serviceDetailModalCtrl');
        var estadoModel = $injector.get('estadoModel', 'serviceDetailModalCtrl');
        var flashMessages = $injector.get('flashMessages', 'serviceDetailModalCtrl');

        $scope.tipo_publicacion = {nombre: datos.servicio.tipo_publicacion_nombre};
        $scope.editable = [];
        $scope.estado = [];
        $scope.municipio = [];
        $scope.datos = [];
        $scope.circulacion = r_circulacion.data;
        $scope.target = r_target.data;
        $scope.red = r_redSocial.data;
        $scope.titulo = datos.servicio;
        estadoModel.get().$promise.then(function (resultado) {
            $scope.estado = resultado.data;
            $scope.estado.unshift(
                {
                    'id': '',
                    'nombre': 'Seleccione el Estado',
                    disabled: 'out'
                }
            );
        });
        if (datos.detalleactual.length > 0) {
            $scope.detalles_servicios = angular.copy(datos.detalleactual);
            if (angular.isUndefined($scope.detalles_servicios[0]['alcance'])) {
                $scope.detalles_servicios[0]['alcance'] = [];
            } else {
                angular.forEach($scope.detalles_servicios[0].alcance, function (value) {
                    if (value.estado.id == 0 && value.municipio.id == 0) {
                        $scope.datos.nacional = true;
                    }
                });
            }

        } else {
            $scope.detalles_servicios = [{
                alcance: [],
                red_social: []
            }];
        }

        $scope.nivelAlcance = function () {
            if (!angular.isUndefined($scope.detalles_servicios[0].alcance)) {
                $scope.detalles_servicios[0].alcance = [];
            }
            if ($scope.datos.nacional) {
                $scope.datos.estado = {id: ''};
                $scope.municipio = [];
                $scope.datos.municipio = {id: ''};
                $scope.detalles_servicios[0].alcance.push({
                    estado: {id: 0, nombre: 'Todos'},
                    municipio: {id: 0, nombre: 'Todos'}
                });
            }
        };
        $scope.cargarMunicipio = function () {
            //console.log(1)
            $scope.municipio = [];
            if ($scope.datos.estado.id != null) {
                estadoModel.get({id: $scope.datos.estado.id}).$promise.then(function (resultado) {
                    $scope.municipio = resultado.data.municipio;
                    $scope.municipio.unshift(
                        {
                            'id': 'todos',
                            'nombre': 'TODOS'
                        }
                    );
                });
            }
        };
        $scope.agregarAlcance = function () {
            var add = true;
            if ($scope.datos.municipio == null || $scope.datos.estado == null) {
                flashMessages.show_info('Debe seleccionar un municipio para poder agregar a la lista!');
                return false;
            } else {
                var removeKeys = [];
                if ($scope.detalles_servicios != null) {
                    if (!angular.isUndefined($scope.detalles_servicios[0].alcance)) {
                        angular.forEach($scope.detalles_servicios[0].alcance, function (value, key) {
                            if (angular.isDefined($scope.datos.municipio)) {
                                if ($scope.datos.municipio.id == value.municipio.id && $scope.datos.estado.id == value.estado.id) {
                                    flashMessages.show_info('El Alcance ya fue agregado');
                                    add = false;
                                } else if ($scope.datos.municipio.id == 'todos' && $scope.datos.estado.id == value.estado.id) {
                                    removeKeys.push(key);
                                } else if ($scope.datos.municipio.id != 'todos' && $scope.datos.estado.id == value.estado.id) {
                                    angular.forEach($scope.detalles_servicios[0].alcance, function (value_in, key) {
                                        if (value_in.estado.id == value.estado.id && value_in.municipio.id == 'todos') {
                                            removeKeys.push(key);
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            }

            for (var i = removeKeys.length - 1; i >= 0; i--) {
                $scope.detalles_servicios[0].alcance.splice(removeKeys[i], 1);
            }
            if (add) {
                var next = true;
                if (next) {
                    $scope.detalles_servicios[0].alcance.push({
                        estado: $scope.datos.estado,
                        municipio: $scope.datos.municipio
                    });
                }
            }
        };

        $scope.removerAlcance = function (index) {
            $scope.detalles_servicios[0].alcance.splice(index, 1);
        };
        $scope.agregarAlcance = function () {
            var add = true;
            if ($scope.datos.municipio == null || $scope.datos.estado == null) {
                flashMessages.show_info('Debe seleccionar un municipio para poder agregar a la lista!');
                return false;
            } else {
                var removeKeys = [];
                if ($scope.detalles_servicios != null) {
                    if (!angular.isUndefined($scope.detalles_servicios[0].alcance)) {
                        angular.forEach($scope.detalles_servicios[0].alcance, function (value, key) {
                            if (angular.isDefined($scope.datos.municipio)) {
                                if ($scope.datos.municipio.id == value.municipio.id && $scope.datos.estado.id == value.estado.id) {
                                    flashMessages.show_info('El Alcance ya fue agregado');
                                    add = false;
                                } else if ($scope.datos.municipio.id == 'todos' && $scope.datos.estado.id == value.estado.id) {
                                    removeKeys.push(key);
                                } else if ($scope.datos.municipio.id != 'todos' && $scope.datos.estado.id == value.estado.id) {
                                    angular.forEach($scope.detalles_servicios[0].alcance, function (value_in, key) {
                                        if (value_in.estado.id == value.estado.id && value_in.municipio.id == 'todos') {
                                            removeKeys.push(key);
                                        }
                                    });
                                }
                            }
                        });
                    }
                }
            }

            for (var i = removeKeys.length - 1; i >= 0; i--) {
                $scope.detalles_servicios[0].alcance.splice(removeKeys[i], 1);
            }
            if (add) {
                var next = true;
                if (next) {
                    $scope.detalles_servicios[0].alcance.push({
                        estado: $scope.datos.estado,
                        municipio: $scope.datos.municipio
                    });
                }
            }
        };

        $scope.removerAlcance = function (index) {
            $scope.detalles_servicios[0].alcance.splice(index, 1);
        };
        $scope.agregarRed2 = function () {
            var add = true;
            if ($scope.datos.red == null || $scope.datos.url == null) {
                flashMessages.show_info('debe agregar una url');
                return false;
            } else if (add) {
                var next = true;
                if (next) {
                    $scope.detalles_servicios[0].red_social.push({
                        red: $scope.datos.red,
                        url: $scope.datos.url
                    });
                }
            }
        };

        $scope.removerAlcance = function (index) {
            $scope.detalles_servicios[0].red_social.splice(index, 1);
        };
        $scope.guardarDetallesServicio = function () {
            if ($scope.detalles_servicios.length > 0) {
                globalData.otros_detalles_servicios = $scope.detalles_servicios;
                $modalInstance.close();
            } else {
                flashMessages.show_info("No ha cargado ningún detalle");
            }
        };

        /**
         * toggleEditable
         */
        $scope.toggleEditable = function (index) {
            $scope.editable[index] = !$scope.editable[index];
        };


        /*  $scope.otros_detalles_servicios = [];
         $scope.editable = [];
         $scope.tipo_publicacion={nombre:datos.servicio.tipo_publicacion_nombre}
         */
        /**
         * Métodos para la creación de los Detalles adicionales del medio
         */
        /*

         // console.log(datos.servicio);
         $scope.nombremedio = datos.medio;
         $scope.titulo = datos.servicio;
         $scope.otros_detalles_servicios = datos.detalleactual;
         $scope.addNewDetailServicio = function () {
         if (!angular.isUndefined($scope.nuevodetalleservicio)) {
         if ($scope.nuevodetalleservicio.nombre == null || $scope.nuevodetalleservicio.nombre == '') {
         flashMessages.show_info("Indique el Nombre");
         return false;

         } else if ($scope.nuevodetalleservicio.descripcion == null || $scope.nuevodetalleservicio.descripcion == '') {
         flashMessages.show_info("Indique la Descripcion");
         return false;
         }
         } else {
         flashMessages.show_info("Debe Rellenar el Formulario");
         return false;
         }
         $scope.otros_detalles_servicios.push($scope.nuevodetalleservicio);
         $scope.reset = function () {
         $scope.nuevodetalleservicio = angular.copy({});
         };
         $scope.reset();
         };

         $scope.removeDetail = function (index) {
         $scope.otros_detalles_servicios.splice(index, 1)
         };

         $scope.guardarDetallesServicio = function () {
         if ($scope.otros_detalles_servicios.length > 0) {
         globalData.otros_detalles_servicios = $scope.otros_detalles_servicios;
         $modalInstance.close();
         } else {
         flashMessages.show_info("No ha cargado ningun detalle");
         }
         };

         */
        /**
         * toggleEditable
         */
        /*
         $scope.toggleEditable = function (index) {
         if ($scope.editable[index]) {
         $scope.editable[index] = false;
         } else {
         $scope.editable[index] = true;
         }
         };
         //console.log($scope.operation);*/
    }]);

/**
 *
 */
app.controller("serviceChildModalCtrl", [
    "$scope", "flashMessages", "globalData", "$modalInstance", "datos", "rListMediosServicios",
    function ($scope, flashMessages, globalData, $modalInstance, datos, rListMediosServicios) {
        //console.log(rListMediosServicios);
        $scope.medio_servicios = rListMediosServicios;
        $scope.otros_detalles_servicios = [];
        $scope.editable = [];
        // console.log(datos.servicio);
        $scope.nombremedio = datos.medio;
        $scope.titulo = datos.servicio;
        $scope.nuevo = {};
        $scope.nuevo.hijos = datos.objetoservicio;
        //console.log(datos.objetoservicio);
        $scope.tipospublicaciones = datos.tipospublicaciones;

        $scope.agregarHijo = function () {
            $scope.nuevo.hijos.push({});
        };

        $scope.removeHijo = function (index) {
            $scope.nuevo.hijos.splice(index, 1)
        };

        $scope.guardarDetallesServicio = function () {
            //console.log("AQUI");
            if ($scope.nuevo.hijos.length > 0) {
                //$scope.service.hijos = $scope.nuevo.hijos;
                // globalData.otros_detalles_servicios.hijos = $scope.nuevo.hijos;

                datos.objetoservicio = $scope.nuevo.hijos;
                $modalInstance.close();
            } else {
                $modalInstance.close();
            }
        };

        /**
         * toggleEditable
         */
        $scope.toggleEditable = function (index) {
            $scope.editable[index] = !$scope.editable[index];
        };
        //console.log($scope.operation);
    }]);
/**
 *
 */
app.controller("CrearMedioController", ["$scope", "$injector", "r_cargos", "r_tipos", "r_medios",
    function ($scope, $injector, r_cargos, r_tipos, r_medios) {
        var consultaRifModel = $injector.get('consultaRifModel', 'CrearMedioCtrl');
        var TipoPublicacion = $injector.get('TipoPublicacion', 'CrearMedioCtrl');
        var flashMessages = $injector.get('flashMessages', 'CrearMedioCtrl');
        var ContactoMedio = $injector.get('ContactoMedio', 'CrearMedioCtrl');
        var $stateParams = $injector.get('$stateParams', 'CrearMedioCtrl');
        //var $window = $injector.get('$window', 'CrearMedioController');
        var globalData = $injector.get('globalData', 'CrearMedioCtrl');
        var $location = $injector.get('$location', 'CrearMedioCtrl');
        var Servicio = $injector.get('Servicio', 'CrearMedioCtrl');
        var $filter = $injector.get('$filter', 'CrearMedioCtrl');
        var $modal = $injector.get('$modal', 'CrearMedioCtrl');
        var Medio = $injector.get('Medio', 'CrearMedioCtrl');

        $scope.current = 0;
        $scope.title = ['Ficha del proveedor', 'Medios del proveedor', 'Contactos del proveedor'];
        $scope.tiposMedios = r_tipos.data;
        $scope.ListadoMedios = r_medios[0].data;
        $scope.dataForm = {};
        /**
         *Variable auxiliares
         **/
        $scope.contactonuevo = {};
        $scope.contactonuevo.es_interno = '';
        $scope.contactonuevo.telefonos = [{a: ""}];
        $scope.contactonuevo.correos_electronicos = [{a: ""}];
        $scope.tipospublicaciones = [];
        $scope.arreglotipo = [];
        $scope.cargosContacto = r_cargos;
        $scope.tiposContacto = [
            {id: 'administrativo', descripcion: 'Administrativo'},
            {id: 'publicitario', descripcion: 'Publicitario'},
            {id: 'otro tipo', descripcion: 'Otro tipo'}
        ];

        $scope.tipospublicaciones = TipoPublicacion.query();
        $scope.habilitado = false;
        $scope.habilitado_razon_social = false;
        $scope.habilitadosolvencia = false;
        $scope.habilitadornc = false;
        $scope.habilitadoseniat = false;

        /**
         * Variables para el formulario de Medio
         * @type {{}}
         */
        $scope.dataForm.medio = {};
        $scope.dataForm.medio.detalles = {};
        $scope.dataForm.medio.detalles.rif = '';
        $scope.dataForm.medio.detalles.fecha_fundacion = '';
        $scope.dataForm.medio.detalles.razon_social = '';
        $scope.dataForm.medio.detalles.direccion_fiscal = '';
        $scope.dataForm.medio.detalles.rnc = '';
        $scope.dataForm.medio.detalles.solvencia = '';
        $scope.dataForm.medio.detalles.seniat = '';
        $scope.dataForm.medio.detalles.nombre_comercial = '';
        $scope.dataForm.medio.detalles.medio_padre = [{id: 0}];
        $scope.nuevo = {};
        $scope.nuevo.hijos = [];
        $scope.dataForm.servicios = [];

        /**
         * Métodos para agregar y eliminar los campos de correo y teléfonos
         */
        $scope.addTelefonoCont = function () {
            $scope.contactonuevo.telefonos.push({a: ''});
        };
        $scope.removeTelefonoCont = function (index) {
            if ($scope.contactonuevo.telefonos.length > 1) {
                $scope.contactonuevo.telefonos.splice(index, 1)
            }
        };
        $scope.addCorreoCont = function () {
            $scope.contactonuevo.correos_electronicos.push({a: ''});
        };
        $scope.removeCorreoCont = function (index) {
            if ($scope.contactonuevo.correos_electronicos.length > 1) {
                $scope.contactonuevo.correos_electronicos.splice(index, 1)
            }
        };
        $scope.addCorreoContAdded = function (index) {
            $scope.dataForm.contactos.detalles[index].correos_electronicos.push({a: ''});
        };
        $scope.removeCorreoContAdded = function (index, parent) {
            if ($scope.dataForm.contactos.detalles[parent].correos_electronicos.length > 1) {
                $scope.dataForm.contactos.detalles[parent].correos_electronicos.splice(index, 1)
            }
        };
        $scope.addTelefonoContAdded = function (index) {
            $scope.dataForm.contactos.detalles[index].telefonos.push({a: ''});
        };
        $scope.removeTelefonoContAdded = function (index, parent) {
            if ($scope.dataForm.contactos.detalles[parent].telefonos.length > 1) {
                $scope.dataForm.contactos.detalles[parent].telefonos.splice(index, 1)
            }
        };
        /**
         * Métodos para agregar y remover los medios padre
         */
        $scope.addPadre = function () {
            $scope.dataForm.medio.detalles.medio_padre.push({id: 0});
        };
        $scope.removePadre = function (index) {
            if ($scope.dataForm.medio.detalles.medio_padre.length > 1) {
                $scope.dataForm.medio.detalles.medio_padre.splice(index, 1)
            }
        };
        /**
         * toggleEditable
         */
        $scope.editableContact = [];
        $scope.toggleEditableContact = function (index) {
            $scope.editableContact[index] = !$scope.editableContact[index];
        };

        /**
         * Variables para la consulta del RIF
         * @type {{}}
         */
        $scope.respuestaconsulta = {};
        $scope.respconsinterna = {};
        $scope.consulta = function (rif) {
            $scope.habilitadoseniat = $scope.habilitadornc = $scope.habilitadosolvencia = $scope.habilitado_razon_social = false;

            var pattern = /^[JGVEP][-][0-9]{8}[-][0-9]$/i;
            if (!pattern.test(rif)) {
                flashMessages.show_error("Debe ingresar un RIF Valido.");
                return false;
            }

            //Consulto RIF si ya existe en BD
            Medio.getbyrif({rif: rif}).$promise.then(function (response) {
                if (response.status == 'ok') {//
                    flashMessages.show_info("El Proveedor ya esta registrado");
                    $location.path("admin/detalle_medio/" + response.data[0].id);
                }
            });

            // Consulto RIF en servicio de Terceros
            consultaRifModel.get({rif: rif}, function (response) {
                $scope.respuestaconsulta = response;
                var mensaje = "Datos encontrados";

                // Verificamos si el SENIAT respondió datos.
                if (response.seniat.status == 'ok') {
                    $scope.dataForm.medio.detalles.razon_social = response.seniat.nombre;
                } else {
                    // Habilito campos SENIAT
                    $scope.habilitadoseniat = true;
                    if (response.rnc.status == 'ok') {
                        $scope.dataForm.medio.detalles.razon_social = response.rnc.razon_social;
                    } else {
                        // Habilito campos RNC
                        $scope.habilitadornc = true;
                        if (response.solvencia != null && response.solvencia.status == 'ok') {
                            $scope.dataForm.medio.detalles.razon_social = response.solvencia.razon_social;
                        } else {
                            // Habilito campos Solvencia y Razón Social
                            $scope.habilitadosolvencia = true;
                            $scope.habilitado_razon_social = true;
                            mensaje = "No se consiguieron los datos asociadas al RIF.";
                        }
                    }
                }
                flashMessages.show_info(mensaje);
            });
            $scope.habilitado = true;
        };

        /**
         * Variables para el formulario de Servicios
         * @type {{}}
         */
        $scope.dataForm.servicios = {};
        $scope.dataForm.servicios.detalles = [];
        /**
         * Variables para el formulario de Contactos
         * @type {{}}
         */
        $scope.dataForm.contactos = {};
        $scope.dataForm.contactos.detalles = [];


        /**
         *Modal de detalles adicionales del servicio
         */
        $scope.serviceDetailModal = function (index, objeto, servicio, medio) {
            var C = $injector.get('CONSTANTES', 'serviceDetailModal');
            var modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/medios/views/servicios/detalles_adicionales.html?V=' + C.VERSION,
                //controller: 'EditarContactoMedioController',
                controller: 'serviceDetailModalCtrl',
                size: 'lg',
                resolve: {
                    datos: function () {
                        return {
                            'indice': index,
                            'detalleactual': objeto,
                            'servicio': servicio,
                            'medio': medio
                        };
                    },
                    r_circulacion: ["circulacionModel", function (circulacionModel) {
                        return circulacionModel.get().$promise;
                    }],
                    r_target: ["targetModel", function (targetModel) {
                        return targetModel.get().$promise;
                    }],
                    r_redSocial: ["redSocialModel", function (redSocialModel) {
                        return redSocialModel.get().$promise;
                    }]
                }
            });
            modalInstance.result.then(function () {
                $scope.dataForm.servicios.detalles[index].otros_detalles = globalData.otros_detalles_servicios;
            });
        };

        /**
         * Modal de Hijos del servicio
         */
        $scope.serviceChildrenModal = function (index, objeto, servicio, medio) {
            var C = $injector.get('CONSTANTES', 'serviceDetailModal');
            var modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/medios/views/servicios/form_hijos.html?V=' + C.VERSION,
                controller: 'serviceChildModalCtrl',
                size: 'lg',
                resolve: {
                    datos: function () {
                        return {
                            'indice': index,
                            'objetoservicio': objeto,
                            'servicio': servicio,
                            'medio': medio,
                            'tipospublicaciones': $scope.tipospublicaciones
                        };
                    }
                }
            });
            modalInstance.result.then(function () {
                $scope.dataForm.servicios.detalles[index].otros_detalles = globalData.otros_detalles_servicios;
            });
        };
        $scope.agregarHijo = function () {
            $scope.nuevo.hijos.push({nombre: null, descripcion: null});
        };

        /**
         * Métodos para la creación de de Servicios
         */
        $scope.addNewService = function () {
            if (!angular.isUndefined($scope.nuevo)) {
                if ($scope.nuevo.nombre == null || $scope.nuevo.nombre == '') {
                    flashMessages.show_info("Indique el Nombre");
                    return false;

                } else if ($scope.nuevo.descripcion == null || $scope.nuevo.descripcion == '') {
                    flashMessages.show_info("Indique la Descripción");
                    return false;
                } else if ($scope.selectTipo == null || $scope.selectTipo == '' || $scope.selectTipo == 0) {
                    flashMessages.show_info("Seleccione el tipo de publicación");
                    return false;
                }
            } else {
                flashMessages.show_info("Debe Rellenar el Formulario");
                return false;
            }

            $scope.nuevo.tipo_medio = 2;
            $scope.nuevo.tipo_publicacion_id = $scope.selectTipo.id;
            $scope.nuevo.tipo_publicacion_nombre = $scope.selectTipo.nombre;
            $scope.nuevo.otros_detalles = [];
            //var longitud = $scope.dataForm.servicios.detalles.length;
            $scope.dataForm.servicios.detalles.push($scope.nuevo);
            $scope.nuevo.tipo_publicacion_id = $scope.selectTipo.id;
            $scope.nuevo.tipo_publicacion_nombre = $scope.selectTipo.nombre;
            $scope.nuevo.otros_detalles = [];

            $scope.reset = function () {
                $scope.nuevo = angular.copy({});
                $scope.nuevo.hijos = [];
                $scope.nuevo.es_cadena = "false";
                $scope.selectTipo = angular.copy({});
            };

            $scope.reset();
        };

        $scope.removeService = function (index) {
            $scope.dataForm.servicios.detalles.splice(index, 1)
        };
        $scope.removeHijo = function (index) {
            $scope.nuevo.hijos.splice(index, 1)
        };

        $scope.setTooltip = function (data) {
            $scope.tooltipdata = data;
        };

        /**
         * Métodos para la creación de Contactos
         */
        $scope.addNewContacto = function () {
            var msg = "";
            if(angular.isUndefined($scope.contactonuevo)){
                msg = "Debe Rellenar el Formulario de contacto.";
            } else if ($scope.contactonuevo.nombre == null || $scope.contactonuevo.nombre == '') {
                msg = "Indique el Nombre.";
            } else if ($scope.contactonuevo.apellido == null || $scope.contactonuevo.apellido == '') {
                msg = "Indique la Apellido.";
            } else if ($scope.cargos == null || $scope.cargos == '' || $scope.cargos == 0) {
                msg = "Indique el Cargo.";
            } else if (angular.isUndefined($scope.tipo)) {
                msg = "Indique el Tipo de Contacto.";
            } else {
                var obj = $scope.contactonuevo.telefonos, i = 0;
                for (i = obj.length - 1; i >= 0; i--) {
                    if (obj[i].a.length == 0) {
                        msg = "Debe llenar todas las casillas de número telefónico.";
                        break;
                    }
                }

                //if(msg === "") {
                //    obj = $scope.contactonuevo.correos_electronicos;
                //    for (i = obj.length - 1; i >= 0; i--) {
                //        if (obj[i].a.length == 0) {
                //            msg = "Debe llenar todas las casillas de correo electrónico.";
                //            break;
                //        }
                //    }
                //}
            }

            if(msg !== ""){
                flashMessages.show_info(msg);
                return false;
            }

            $scope.contactonuevo.cargo = $scope.cargos;
            $scope.contactonuevo.tipo = $scope.tipo;
            $scope.dataForm.contactos.detalles.push($scope.contactonuevo);

            $scope.reset = function () {
                $scope.contactonuevo = angular.copy({});
                $scope.contactonuevo.telefonos = [{a: ""}];
                $scope.contactonuevo.correos_electronicos = [{a: ""}];
                $scope.cargos = angular.copy({});
            };

            $scope.reset();
        };
        $scope.removeContacto = function (index) {
            $scope.dataForm.contactos.detalles.splice(index, 1)
        };
        $scope.setFormScope = function (scope) {
            this.formScope = scope;
        };


        $scope.create = function () {
            if(angular.isUndefined($scope.dataForm.contactos.detalles)){
                flashMessages.show_error("Debe cargar al menos un contacto al proveedor.");
                return false;
            }

            /**
             * Preparación de los datos consultados por RIF
             */
            if ($scope.respuestaconsulta.status == 'ok') {
                if (false) {//$scope.respuestaconsulta.rnc != null
                    $scope.dataForm.medio.detalles.rnc = '{"estatus":"' + $scope.respuestaconsulta.rnc['estatus'] + '", "nivel": "' + $scope.respuestaconsulta.rnc['nivel'] + '"}';
                } else {
                    $scope.dataForm.medio.detalles.rnc = '{"estatus":"", "nivel": ""}';
                }
                if ($scope.respuestaconsulta.solvencia != null) {
                    $scope.dataForm.medio.detalles.solvencia = '{"estatus":"' + $scope.respuestaconsulta.solvencia['estatus'] + '", "ultima_solicitud": "' + $scope.respuestaconsulta.solvencia['ultima_solicitud'] + '"}';
                } else {
                    $scope.dataForm.medio.detalles.solvencia = '{"estatus":"", "ultima_solicitud": ""}';
                }
                if ($scope.respuestaconsulta.seniat != null) {
                    $scope.dataForm.medio.detalles.seniat = '{"agenteretencioniva":"' + $scope.respuestaconsulta.seniat['agenteretencioniva'] + '", "contribuyenteiva": "' + $scope.respuestaconsulta.seniat['contribuyenteiva'] + '", "tasa":"' + $scope.respuestaconsulta.seniat['tasa'] + '"}';
                } else {
                    $scope.dataForm.medio.detalles.seniat = '{"agenteretencioniva":"", "contribuyenteiva": "", "tasa":""}';
                }
            } else {
                $scope.dataForm.medio.detalles.rnc = '{"estatus":"", "nivel": ""}';
                $scope.dataForm.medio.detalles.solvencia = '{"estatus":"", "ultima_solicitud": ""}';
                $scope.dataForm.medio.detalles.seniat = '{"agenteretencioniva":"", "contribuyenteiva": "", "tasa":""}';
            }




            Medio.save($scope.dataForm.medio.detalles, function (resp) {
                $scope.id_medio = resp.data.id;
                if ($scope.dataForm.servicios.detalles.length > 0) {
                    $scope.dataForm.servicios.detalles.otros_detalles = $filter('json')($scope.dataForm.servicios.detalles.otros_detalles);
                    Servicio.crear({medioId: $scope.id_medio}, $scope.dataForm.servicios.detalles).$promise.then(
                    );
                }
                if ($scope.dataForm.contactos.detalles.length > 0) {
                    $scope.dataForm.contactos.detalles.telefonos = $filter('json')($scope.dataForm.contactos.detalles.telefonos);
                    $scope.dataForm.contactos.detalles.correos_electronicos = $filter('json')($scope.dataForm.contactos.detalles.correos);

                    ContactoMedio.crear({medioId: $scope.id_medio}, $scope.dataForm.contactos.detalles);
                }
                flashMessages.show_success("Se ha creado el proveedor exitosamente");
                $location.path("admin/detalle_medio/" + $scope.id_medio)
            });
        };
        /**
         * Métodos para el DatePicker de fecha de fundación
         */
        $scope.calendar = {
            opened: {},
            dateFormat: 'dd/MM/yyyy',
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        /**
         * Funciones para la paginación del Wizard de creación de Medios
         * @returns {boolean}
         */
        $scope.showButtonWizard = function () {
            $scope.disabledNext = false;
            if ($scope.current >= 1) {
                if (($scope.seccion - 1) == $scope.current) {
                    //$scope.disabledNext = true;
                }
                return true;
            }
        };

        $scope.wizardNext = function () {
            switch ($scope.current) {
                case 0:
                    $scope.formmedio.submitted = true;
                    if ($scope.formmedio.date_fundacion.$error.required ||
                        $scope.formmedio.direccion.$error.required ||
                        $scope.formmedio.nom_comercial.$error.required
                    ) {
                        return false;
                    } else {
                        $scope.current++;
                    }
                    break;
                case 1:
                    $scope.current++;
                    break;
                case 2:
                    break;
            }
        };


    }
]);
/**
 * Controlador para la edición de la ficha del medio
 */
app.controller("EditarMedioController",
    ["$scope", "$injector", 'r_tipos', 'r_medios', function ($scope, $injector, r_tipos, r_medios) {
        //var $http = $injector.get('$http', 'EditMedioCtrl');
        //var $window = $injector.get('$window', 'EditMedioCtrl');
        //var globalData = $injector.get('globalData', 'EditMedioCtrl');
        var consultaRifModel = $injector.get('consultaRifModel', 'EditMedioCtrl');
        var flashMessages = $injector.get('flashMessages', 'EditMedioCtrl');
        var $stateParams = $injector.get('$stateParams', 'EditMedioCtrl');
        var $location = $injector.get('$location', 'EditMedioCtrl');
        var $filter = $injector.get('$filter', 'EditMedioCtrl');
        var Medio = $injector.get('Medio', 'EditMedioCtrl');

        $scope.current = 0;
        $scope.title = ['Editar Ficha', 'Otros detalles del proveedor'];
        $scope.dataForm = {};
        $scope.dataForm.detalles = {};
        $scope.tiposMedios = r_tipos.data;
        $scope.ListadoMedios = r_medios[0].data;
        /**
         *Variable auxiliares
         **/
        $scope.editable = [];
        $scope.habilitado_razon_social = false;
        $scope.habilitadosolvencia = false;
        $scope.habilitadornc = false;
        $scope.habilitadoseniat = false;

        /**
         * Variables para el formulario de Medio
         * @type {{}}
         */
        $scope.dataForm.medio = {};
        $scope.dataForm.medio.detalles = {};
        $scope.dataForm.medio.detalles.rif = '';
        $scope.dataForm.medio.detalles.sitio_web = '';
        $scope.dataForm.medio.detalles.fecha_fundacion = '';
        $scope.dataForm.medio.detalles.razon_social = '';
        $scope.dataForm.medio.detalles.direccion_fiscal = '';
        $scope.dataForm.medio.detalles.nombre_comercial = '';
        $scope.dataForm.medio.detalles.medio_padre = [{id: 0}];
        /**
         * Se obtienen los datos del medio y se cargan en las variables del $scope
         */
        Medio.get({id: $stateParams.id}, function (respuesta) {
            $scope.detalles = {};
            $scope.detalles = respuesta.data;
            $scope.dataForm.medio.detalles.rif = $scope.detalles.rif;
            $scope.dataForm.medio.detalles.razon_social = $scope.detalles.razon_social;
            $scope.dataForm.medio.detalles.direccion_fiscal = $scope.detalles.direccion_fiscal;

            if($scope.detalles.fecha_fundacion > 0){
                $scope.dataForm.medio.detalles.fecha_fundacion =
                    moment($scope.detalles.fecha_fundacion, 'YYYY-MM-DD hh:mm:ss').toDate();
            }

            $scope.telefonos = $filter('objmedio')($scope.detalles.telefonos);
            $scope.correos_electronicos = $filter('objmedio')($scope.detalles.correos_electronicos);
            $scope.dataForm.medio.detalles.tipo_medio = $scope.detalles.tipo;
            $scope.dataForm.medio.detalles.medio_padre = $scope.detalles.padres;
            $scope.dataForm.medio.detalles.sitio_web = $scope.detalles.sitio_web;
            $scope.dataForm.medio.detalles.nombre_comercial = $scope.detalles.nombre_comercial;
            $scope.dataForm.medio.detalles.seniat = $scope.detalles.seniat;
            $scope.dataForm.medio.detalles.solvencia = $scope.detalles.solvencia;
            $scope.dataForm.medio.detalles.rnc = $scope.detalles.rnc;
        });
        /**
         * toggleEditable
         */
        $scope.toggleEditable = function (index) {
            $scope.editable[index] = !$scope.editable[index];
        };


        /**
         *Método que persiste los datos a la BD
         */
        $scope.update = function () {

            Medio.update({id: $stateParams.id}, $scope.dataForm.medio.detalles).$promise.then(
                flashMessages.show_success("Se han guardado los cambios exitosamente"),
                $location.path("admin/detalle_medio/" + $stateParams.id)
            );
        };
        /**
         * Métodos para el DatePicker de fecha de fundación
         */
        $scope.calendar = {
            opened: {},
            dateFormat: 'dd/MM/yyyy',
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            },
            disabled: function (date, mode) {
                return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            }
        };
        /**
         * Funciones para la paginación del Wizard de creación de Medios
         * @returns {boolean}
         */
        $scope.showButtonWizard = function () {
            $scope.disabledNext = false;
            if ($scope.current >= 1) {
                if (($scope.seccion - 1) == $scope.current) {
                    //$scope.disabledNext = true;
                }
                return true;
            }
        };

        $scope.consulta = function (rif) {
            $scope.habilitadoseniat = $scope.habilitadornc = $scope.habilitadosolvencia = $scope.habilitado_razon_social = false;

            var pattern = /^[JGVEP][-][0-9]{8}[-][0-9]$/i;
            if (!pattern.test(rif)) {
                flashMessages.show_error("Debe ingresar un RIF");
                return false;
            }

            //Consulto RIF si ya existe en BD
            Medio.getbyrif({rif: rif}).$promise.then(function (response) {
                if (response.status == 'ok') {//
                    flashMessages.show_info("El Proveedor ya esta registrado");
                    $location.path("admin/detalle_medio/" + response.data[0].id);
                }
            });

            // Consulto RIF en servicio de Terceros
            consultaRifModel.get({rif: rif}, function (response) {
                $scope.respuestaconsulta = response;
                var mensaje = "Datos encontrados";

                // Verificamos si el SENIAT respondió datos.
                if (response.seniat.status == 'ok') {
                    $scope.dataForm.medio.detalles.razon_social = response.seniat.nombre;
                } else {
                    // Habilito campos SENIAT
                    $scope.habilitadoseniat = true;
                    if (response.rnc.status == 'ok') {
                        $scope.dataForm.medio.detalles.razon_social = response.rnc.razon_social;
                    } else {
                        // Habilito campos RNC
                        $scope.habilitadornc = true;
                        if (response.solvencia != null && response.solvencia.status == 'ok') {
                            $scope.dataForm.medio.detalles.razon_social = response.solvencia.razon_social;
                        } else {
                            // Habilito campos Solvencia y Razón Social
                            $scope.habilitadosolvencia = true;
                            $scope.habilitado_razon_social = true;
                            mensaje = "No se consiguieron los datos asociadas al RIF.";
                        }
                    }
                }
                flashMessages.show_info(mensaje);
            });
            $scope.habilitado = true;
        };
    }

    ]);

/**
 *
 */
app.controller("CambiarImagenMedioController", ["$scope", '$injector', '$modalInstance', 'clientID',
    function ($scope, $injector, $modalInstance, clientID) {
        //var $stateParams = $injector.get('$stateParams', 'CambiarImgMedioCtrl');
        var clienteLogo = $injector.get('clienteLogo', 'CambiarImgMedioCtrl');
        //var $location = $injector.get('$location', 'CambiarImgMedioCtrl');
        var $compile = $injector.get('$compile', 'CambiarImgMedioCtrl');
        //var $modal = $injector.get('$modal', 'CambiarImgMedioCtrl');
        var Medio = $injector.get('Medio', 'CambiarImgMedioCtrl');

        $scope.datos = {};
        $scope.clientID = clientID;

        $scope.process_file = function (file_up) {
            if (file_up.length > 0) {
                var file = file_up[0];
                $scope.datos.file = file_up;
                $scope.datos.file.progress = 0;
                var progressHtml = '<div class="progress" style="position: absolute;margin-left: 3%;width: 87%;margin-top: 5px;">' +
                    '<div class="progress-bar progress-bar-info progress-bar-striped" role="progressbar" aria-valuenow="{{datos.file.progress}}" aria-valuemin="0" aria-valuemax="100" style="width: {{datos.file.progress}}%">' +
                    '<span>Subiendo... ({{datos.file.progress}}%)</span>' +
                    '</div>' +
                    '</div>';
                // parseo el string append a html
                var element = angular.element(progressHtml);
                // compilamos el elemento parseado dentro del $scope
                var html = $compile(element)($scope);
                // agregamos el html del progressbar
                angular.element('#content-upload').html(html);
                // ejecutamos la subida del archivo seleccionado
                clienteLogo.upload_logo(file).progress(function (evt) {

                    file.progress = parseInt(100.0 * evt.loaded / evt.total);

                }).success(function (data) {

                    var icon = 'glyphicon glyphicon-ok-sign';

                    if (!data.success)
                        icon = 'glyphicon glyphicon-warning-sign';

                    if (data.success) {
                        //$scope.name_adjunto = data.file_name;
                        $scope.datos.file_name = data.file_name
                    }

                    // file is uploaded successfully
                    var suceesHtml = '<div style="text-align: left; position: absolute;margin-left: 3%;width: 87%;margin-top: 5px;">' +
                        '<span class="' + icon + '">' + data.message + '</span>' +
                        '</div>';
                    // parseo el string append a html
                    var ele = angular.element(suceesHtml);
                    // compilamos el elemento parseado dentro del $scope
                    var append = $compile(ele)($scope);
                    // agregamos el html
                    angular.element('#content-upload').html(append);
                    //console.log('file ' + config.file.name + 'is uploaded successfully. Response: ' + data);
                })
            }

        };

        $scope.guardar = function (filename) {
            var logo = {file_name: filename};
            //console.log(logo);
            Medio.moverLogo({clientID: $scope.clientID}, logo).$promise.then(function () {
                $modalInstance.close();
            });
        };
    }
]);

/**
 *
 */
app.controller("historicoTarifasMedioListController", [
    "$scope", "particularPermission", "clientID", "$injector",
    function ($scope, particularPermission, clientID, $injector) {
        var DTInstances = $injector.get('DTInstances', 'históricoTarifasMedioListCtrl');
        var appConfig = $injector.get('appConfig', 'históricoTarifasMedioListCtrl');
        var Medio = $injector.get('Medio', 'históricoTarifasMedioListCtrl');

        $scope.title = "Histórico de Tarifas";
        $scope.filtro = {};
        $scope.tipo = [{id: 0, nombre: 'Todos'}];
        $scope.filtro.todos = true;
        //Se inician los calendarios y ambas fechas del filtro en la fecha actual
        var toDay = new Date();
        var hoy = moment(toDay).format('YYYY-MM-DD');

        Medio.children({id: clientID}).$promise.then(function (response) {
            angular.forEach(response.data.servicios, function (value) {
                $scope.tipo.push(value);
            });

        });

        $scope.filtro.tipo = $scope.tipo[0];
        $scope.siteCrud = {
            url: 'medios/historicoTarifasDetallesCondicionMedioDt/' + clientID,
            is_orderable: false,
            data: {
                dateStart: hoy,
                dateEnd: hoy,
                servicio: 'Todos',
                todos: true
            }
        };
        $scope.headerCrud = [
            {title: 'id', label: 'id', visible: false, orderable: false},
            //{title: 'servicio_id', label: 'servicio_id', visible: false, orderable: false},
            //{title: 'condicion_id', label: 'condicion_id', visible: false, orderable: false},
            //{title: 'condicion_detalle_id', label: 'condicion_detalle_id', visible: false, orderable: false},
            //{title: 'usuario_id', label: 'usuario_id', visible: false, orderable: false},
            //{title: 'created_at', label: 'created_at', visible: false, orderable: false},
            {title: 'Proveedor', label: 'medio.nombre', visible: false, orderable: false},
            {title: 'Medio', label: 'servicio.nombre', orderable: false},
            {title: 'Condición', label: 'condicion.nombre', orderable: false},
            {title: 'Detalle', label: 'detalle_condicion.nombre', orderable: false},
            {title: 'Tarifa', label: 'tarifa', orderable: false},
            {title: 'Fecha', label: 'fecha', orderable: false},
            {title: 'Usuario', label: 'user_nombre_apellido', orderable: false}
        ];
        $scope.operation = particularPermission.permission;

        /**
         * Objeto del calendario
         */

        $scope.calendar = {
            opened: {},
            now: new Date(),
            dateFormat: 'dd/MM/yyyy',
            dateOptions: {
                formatYear: 'yy',
                startingDay: 1
            },
            open: function ($event, which) {
                $event.preventDefault();
                $event.stopPropagation();

                angular.forEach($scope.calendar.opened, function (value, key) {
                    $scope.calendar.opened[key] = false;
                });

                $scope.calendar.opened[which] = true;
            }
            //disabled: function (date, mode) {
            //    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
            //}
        };
        /**
         * Función que carga los datos del filtro y recarga la tabla
         */
        $scope.filtrar = function () {
            DTInstances.getLast().then(function (lastDTInstance) {
                $scope.setUpFilter();
                lastDTInstance.reloadData();
            });
        };
        /**
         * Función que gestiona la apertura de la pestaña de detalles de la operación del balance
         * @param balanceID
         * @param operation_id
         */
        $scope.detalleOperacionMedio = function (balanceID, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Detalle Operación #',
                state: 'admin.detalle_medio.detalles_balance({balanceID:' + balanceID + '})',
                object_id: balanceID
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state', 'detalleOperaciónMedio');
            $state.go('^.detalles_balance', {'balanceID': balanceID});
        };
        /**
         * Función que gestiona la pestaña de observaciones de la operación
         * @param balanceID
         * @param operation_id
         */
        $scope.addObservacionMedio = function (balanceID, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Observaciones Operación #',
                state: 'admin.detalle_medio.addobservacion_balance({balanceID:' + balanceID + '})',
                object_id: balanceID
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state', 'addObservaciónMedio');
            $state.go('^.addobservacion_balance', {'balanceID': balanceID});
        };

        $scope.imprimir = function () {
            $scope.setUpFilter();
            window.open(appConfig.getPathApi() + 'clientes/balanceClienteDt/' + clientID + '?reporte=true&dateStart=' + $scope.siteCrud.data.dateStart + '&dateEnd=' + $scope.siteCrud.data.dateEnd + '&tipomovimiento=' + $scope.siteCrud.data.tipomovimiento + '&todos=' + $scope.siteCrud.data.todos);
        };
        /**
         * Esta Función organiza los parámetros de los filtros usados en DataTables y en el reporte
         */
        $scope.setUpFilter = function () {
            $scope.siteCrud.data.dateStart = moment($scope.filtro.fecha_desde).format('YYYY-MM-DD');
            //Si la fecha "Hasta" no esta definida se setea con la fecha actual
            if ($scope.filtro.fecha_hasta) {
                $scope.siteCrud.data.dateEnd = angular.isDefined(moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_hasta).format('YYYY-MM-DD') : moment(toDay).format('YYYY-MM-DD');
            } else {
                $scope.siteCrud.data.dateEnd = moment(toDay).format('YYYY-MM-DD');
            }
            if ($scope.filtro.fecha_desde) {
                $scope.siteCrud.data.dateStart = angular.isDefined(moment($scope.filtro.fecha_desde).format('YYYY-MM-DD')) ? moment($scope.filtro.fecha_desde).format('YYYY-MM-DD') : false;
            } else {
                $scope.siteCrud.data.dateStart = false;
            }
            $scope.siteCrud.data.servicio = $scope.filtro.tipo.id;
            $scope.siteCrud.data.todos = $scope.filtro.todos;
        }

    }]);