'use strict';

app.controller('listarContactosMedio', [
    '$scope',
    'clientID',
    'particularPermission',
    '$state',
    'resolveTabId',
    'globalData',
    '$modal',
    'crudFactory',
    function ($scope,
              clientID,
              particularPermission,
              $state,
              resolveTabId,
              globalData,
              $modal,
              crudFactory) {
        $scope.title = "Contactos del proveedor";
        $scope.siteCrud = {url: 'medios/' + clientID + '/contactos/dt'};

        $scope.headerCrud = [
            {title: 'ID', label: 'id', visible: false},
            {title: 'Nombre', label: 'nombre', visible: true},
            {title: 'Apellido', label: 'apellido', visible: true},
            {title: 'Telefonos', label: 'telefonos', visible: true, filter: "listadoContMed"},
            {title: 'Correos', label: 'correos_electronicos', visible: true, filter: "listadoContMed"},
            {title: 'cargo_id', label: 'cargo_id', visible: false},
            {title: 'Interno', label: 'es_interno', visible: true, filter: "si_no"},
            {title: 'ContactoMedio_id', label: 'medio', visible: false},
            {title: 'Tipo Contacto', label: 'tipo', visible: true}

        ];
        $scope.operation = particularPermission.get.data.permission;

        $scope.newTabContacto = function (contactoId, operation_id) {
            globalData.data.ContactoMedioId = contactoId;
            globalData.data.MedioId = $state.params.id;

            // joiuiyuyuggytf
            var tab = {
                id: operation_id,
                title: 'Contacto #',
                state: 'admin.detalle_medio.contacto({idcont:' + contactoId + '})',
                object_id: contactoId
            };
            $scope.buildTab(tab);
            $state.go('^.contacto', {'idcont': contactoId});

        };
        $scope.newTabEditContacto = function (contactoId, operation_id) {
            globalData.data.ContactoMedioId = contactoId;
            globalData.data.MedioId = $state.params.id;

            // joiuiyuyuggytf
            var tab = {
                id: operation_id,
                title: 'Contacto #',
                state: 'admin.detalle_medio.contact({idcont:' + contactoId + '})',
                object_id: contactoId
            };
            $scope.buildTab(tab);
            $state.go('^.contact', {'idcont': contactoId});
        };
        /*
         Funcion para abrir la pantalla de edicion en un modal
         */
        $scope.agregarMedioContacto = function (contactoId) {
            var modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/medios/views/contactos/editar.html',
                controller: 'EditarContactoMedioController',
                size: 'lg',
                resolve: {
                    datos: function () {
                        return {
                            'medioid': $state.params.id,
                            'contactoid': contactoId
                        };
                    }
                }
            });

            modalInstance.result.then(function () {
                crudFactory.reloadData();
            });
        };
        $scope.crearMedioContacto = function () {//admin/detalle_medio/contacto_add
            var modalInstance = $modal.open({
                animation: true,
                //backdrop: 'static',
                templateUrl: 'modules/medios/views/contactos/crear.html',
                controller: 'CrearContactoMedioController',
                size: 'lg',
                resolve: {
                    datos: function () {
                        return {
                            'medioid': $state.params.id,
                            'contactoid': contactoId
                        };
                    }
                }
            });

            modalInstance.result.then(function () {
                crudFactory.reloadData();
            });
        };

    }
]);

app.controller("DetallesContactoController", [
    "$scope",
    "$http", '$injector',
    "ContactoMedio",
    "$stateParams",
    "globalData", 'resolveTabId',
    function ($scope, $http, $injector, ContactoMedio, $stateParams, globalData, resolveTabId) {
        $scope.detalles = {};
        $scope.ContactoMedio = ContactoMedio.get({
            contactoId: $stateParams.idcont,
            medioId: $stateParams.id
        }, function (respuesta) {
            $scope.detalles = respuesta.data;
        });

        // Verifico el state donde estoy.
        var $state = $injector.get('$state', 'DetallesContactoController.');
        $scope.buildTab({
            id: resolveTabId,
            state: $state.current.name + '({idcont: ' + $stateParams.idcont + '})',
            title: 'Contacto #',
            object_id: $stateParams.idcont
        });
    }
]);
app.controller("CrearContactoMedioController", [
    "$scope",
    "$http",
    "ContactoMedio",
    "$stateParams",
    "$window",
    'globalData',
    '$location',
    'datos',
    '$filter',
    '$modal',
    'crudFactory',
    'flashMessages',
    'r_cargos',
    '$injector',
    function ($scope,
              $http,
              ContactoMedio,
              $stateParams,
              $window,
              globalData,
              $location,
              datos,
              $filter,
              $modal,
              crudFactory,
              flashMessages,
              r_cargos,
              $injector) {

        $scope.detalles = {};
        $scope.detalles.nombre = '';
        $scope.detalles.apellido = '';
        $scope.detalles.telefonos = [{'a': ''}];
        $scope.detalles.correos_electronicos = [{'a': ''}];
        $scope.detalles.cargo_id = {};
        $scope.detalles.deleted_at = '';
        $scope.detalles.es_interno = '';
        $scope.detalles.id = '';
        $scope.detalles.medio_id = '';
        $scope.detalles.cargos = r_cargos;
        $scope.detalles.cargo = {};
        $scope.tiposContacto = [{id: 'administrativo', descripcion: 'Administrativo'}, {
            id: 'publicitario',
            descripcion: 'Publicitario'
        }, {id: 'otro tipo', descripcion: 'Otro tipo'}];
        //CargoMdl.get(null).$promise.then(function(respuesta){
        //     $scope.detalles.cargos = respuesta.data;
        //});

        /*
         Objeto que se enviara al Backend
         */
        $scope.contact = {};
        $scope.contact.nombre = '';
        $scope.contact.apellido = '';
        $scope.contact.telefonos = '';
        $scope.contact.correos_electronicos = '';
        $scope.contact.medio_id = datos.medioid;

        $scope.addTelefono = function () {
            $scope.detalles.telefonos.push({a: ''});
        };
        $scope.removeTelefono = function (index) {
            if ($scope.detalles.telefonos.length > 1) {
                $scope.detalles.telefonos.splice(index, 1)
            }
        };
        $scope.addCorreo = function () {
            $scope.detalles.correos_electronicos.push({a: ''});
        };
        $scope.removeCorreo = function (index) {
            if ($scope.detalles.correos_electronicos.length > 1) {
                $scope.detalles.correos_electronicos.splice(index, 1)
            }
        };
        $scope.cerrarCrear = function () {
            $modal.close();
        };


        var $validationProvider = $injector.get('$validation');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y resive las respuestas pertinentes.
             *
             * @author Jose Rodriugez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                $scope.contact.telefonos = $filter('json')($scope.detalles.telefonos);
                $scope.contact.correos_electronicos = $filter('json')($scope.detalles.telefonos);
                $scope.contact.nombre = $scope.detalles.nombre;
                $scope.contact.apellido = $scope.detalles.apellido;
                $scope.contact.telefonos = $scope.detalles.telefonos;
                $scope.contact.correos_electronicos = $scope.detalles.correos_electronicos;
                $scope.contact.medio_id = datos.medioid;
                $scope.contact.cargo_id = $scope.detalles.cargo.id;
                $scope.contact.es_interno = $scope.detalles.es_interno;
                $scope.datos = [];
                $scope.datos[0] = $scope.contact;
                $scope.contact.correos_electronicos = $filter('json')($scope.detalles.telefonos);
                $scope.contact.nombre = $scope.detalles.nombre;
                $scope.contact.apellido = $scope.detalles.apellido;
                $scope.contact.direccion = $scope.detalles.direccion;
                $scope.contact.telefonos = $scope.detalles.telefonos;
                $scope.contact.correos_electronicos = $scope.detalles.correos_electronicos;
                $scope.contact.medio_id = datos.medioid;
                $scope.contact.cargo = {};
                $scope.contact.cargo.id = $scope.detalles.cargo.id;
                $scope.contact.tipo = {};
                $scope.contact.tipo.id = $scope.tipo.id;
                $scope.datos = [];
                $scope.datos[0] = $scope.contact;
                //  console.log();
                ContactoMedio.crear(
                    {medioId: $stateParams.id},
                    $scope.datos,
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors);
            },
            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando todo esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha creado el contacto.");
                $scope.$close(true);
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.nombre)) {
                        mensajeToShow = mensajeToShow.concat(message.nombre);
                    }
                    if (angular.isDefined(message.apellido)) {
                        mensajeToShow = mensajeToShow.concat(message.apellido);
                    }
                    if (angular.isDefined(message.cargo_id)) {
                        mensajeToShow = mensajeToShow.concat(message.cargo_id);
                    }
                    if (angular.isDefined(message.telefonos)) {
                        mensajeToShow = mensajeToShow.concat(message.telefonos);
                    }
                    if (angular.isDefined(message.correos_electronicos)) {
                        mensajeToShow = mensajeToShow.concat(message.correos_electronicos);
                    }
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);

app.controller("EditarContactoMedioController", [
    "$scope",
    "ContactoMedio",
    "$stateParams",
    'globalData',
    '$filter',
    '$location',
    'datos',
    '$modalInstance',
    'flashMessages',
    "CargoMdl",
    "$injector",
    function ($scope, ContactoMedio, $stateParams, globalData, $filter, $location, datos, $modalInstance, flashMessages, CargoMdl, $injector) {
        $scope.editedcontact = {};
        $scope.editedcontact.nombre = '';
        $scope.editedcontact.apellido = '';
        $scope.editedcontact.telefonos = '';
        $scope.editedcontact.correos_electronicos = '';
        $scope.editedcontact.deleted_at = '';
        $scope.editedcontact.es_interno = '';
        $scope.editedcontact.id = '';
        $scope.editedcontact.medio_id = '';
        $scope.tiposContacto = [{id: 'administrativo', descripcion: 'Administrativo'}, {
            id: 'publicitario',
            descripcion: 'Publicitario'
        }, {id: 'otro tipo', descripcion: 'Otro tipo'}];
        ContactoMedio.get({
            contactoId: datos.contactoid,
            medioId: datos.medioid
        }, function (respuesta) {
            //$scope.editedcontact.cargo = respuesta.data.cargo_id};
            $scope.editedcontact.cargo = {'id': respuesta.data.cargo_id};
            $scope.editedcontact.tipo = {'id': respuesta.data.tipo};
            $scope.detalles = respuesta.data;
            $scope.detalles.telefonos = angular.fromJson($scope.detalles.telefonos);
            $scope.detalles.correos_electronicos = angular.fromJson($scope.detalles.correos_electronicos);
        });

        $scope.editedcontact.cargos = CargoMdl.query();
        //$scope.detalles.cargo='{"id":47,"descripcion":"odio","deleted_at":null}';
        $scope.addTelefono = function () {
            $scope.detalles.telefonos.push({nombre: ''});
        };
        $scope.removeTelefono = function (index) {
            if ($scope.detalles.telefonos.length > 1) {
                $scope.detalles.telefonos.splice(index, 1)
            }
        };
        $scope.addCorreo = function () {
            $scope.detalles.correos_electronicos.push({nombre: ''});
        };
        $scope.removeCorreo = function (index) {
            if ($scope.detalles.correos_electronicos.length > 1) {
                $scope.detalles.correos_electronicos.splice(index, 1)
            }
        };

        var $validationProvider = $injector.get('$validation');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                })
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y resive las respuestas pertinentes.
             *
             * @author Jose Rodriugez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                //$scope.editedcontact.telefonos = $filter('json')($scope.detalles.telefonos, 0);
                //$scope.editedcontact.correos_electronicos = $filter('json')($scope.detalles.telefonos, 0);
                $scope.editedcontact.nombre = $scope.detalles.nombre;
                $scope.editedcontact.apellido = $scope.detalles.apellido;
                $scope.editedcontact.direccion = $scope.detalles.direccion;
                $scope.editedcontact.telefonos = $filter('json')($scope.detalles.telefonos, 0);
                $scope.editedcontact.correos_electronicos = $filter('json')($scope.detalles.correos_electronicos, 0);
                $scope.editedcontact.cargo_id = $scope.editedcontact.cargo.id;
                $scope.editedcontact.deleted_at = $scope.detalles.deleted_at;
                $scope.editedcontact.es_interno = $scope.detalles.es_interno;
                $scope.editedcontact.horario_id = $scope.detalles.horario_id;
                $scope.editedcontact.id = $scope.detalles.id;
                $scope.editedcontact.medio_id = $scope.detalles.medio_id;
                $scope.editedcontact.tipo = $scope.editedcontact.tipo.id;
                delete($scope.editedcontact.tipo.descripcion);
                ContactoMedio.update({
                    medioId: datos.medioid,
                    contactoId: datos.contactoid
                }, $scope.editedcontact).$promise.then(function (response) {
                        flashMessages.show_success("Se ha modificado el contacto");
                        $modalInstance.close();
                    }
                );
            },


            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando todo esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha creado el contacto.");
                $scope.$close(true);
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.nombre)) {
                        mensajeToShow = mensajeToShow.concat(message.nombre);
                    }
                    if (angular.isDefined(message.apellido)) {
                        mensajeToShow = mensajeToShow.concat(message.apellido);
                    }
                    if (angular.isDefined(message.cargo_id)) {
                        mensajeToShow = mensajeToShow.concat(message.cargo_id);
                    }
                    if (angular.isDefined(message.telefonos)) {
                        mensajeToShow = mensajeToShow.concat(message.telefonos);
                    }
                    if (angular.isDefined(message.correos_electronicos)) {
                        mensajeToShow = mensajeToShow.concat(message.correos_electronicos);
                    }
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);

