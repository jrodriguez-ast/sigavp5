'use strict';

/**
 * Created by darwin on 14/01/15.
 */

/**
 * Este factory contiene los métodos que se encargan de buscar la información de la session y el login en el servidor.
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 13/01/2015
 */
app.factory('AuthService', [
    "$http", "Session", "appConfig", "$log", "flashMessages",
    function ($http, Session, appConfig, $log, flashMessages) {
        var _url = appConfig.getAdminPathApi() + 'auth_cotizaciones';

        return {
            /**
             * Envía los datos al servidor para el inicio de sesión.
             * @param credentials
             * @return {*}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             */
            login: function (credentials) {
                var _thisUrl = _url + '/login';
                return $http
                    .post(_thisUrl, credentials)
                    .then(function (res) {
                        Session.create(res.data);
                        return res.data;
                    },
                    function (response) {
                        if (response.status == '404') {
                            flashMessages.show_error('No hay comunicación con el servidor');
                        } else {
                            flashMessages.show_error('Ocurrió un error al intentar realizar la operación');
                        }
                        $log.error(response);
                    });
            },

            /**
             * Verifica la sesión en el servidor y actualiza los datos en la sesión cliente.
             * @return {*}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             */
            checkLogin: function () {
                var _thisUrl = _url + '/store';
                return $http
                    .get(_thisUrl)
                    .then(function (res) {
                        Session.create(res.data);
                        return res.data.success;
                    }, function (res) {
                        $log.error(res);
                        flashMessages.show_error('No hay comunicación con el servidor');
                        return res;
                    });
            },

            /**
             * Retorna si la sesión se encuentra activa
             * @return {boolean}
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             */
            isAuthenticated: function () {
                return !!Session.userId;
            },

            /**
             * Envía al servidor la solicitud para cerrar la sesión.
             *
             * @author Darwin Serrano <darwinserrano@gmail.com>
             */
            logout: function () {
                //console.log('logout')
                var _thisUrl = _url + '/logout';
                return $http
                    .get(_thisUrl)
                    .then(function () {
                        Session.destroy();
                        return null;
                    });
            }
        };

    }]);

/**
 * Este servicio almacena en cliente los datos de la session.
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 */
app.service('Session', function () {
    this.create = function (sessionData) {
        this.userId = null;
        this.userId = null;
        this.sessionData = sessionData;
        this.sessionStatus = sessionData.success;
        this.user = {
            firstName: sessionData.first_name,
            lastName: sessionData.last_name,
            login: sessionData.user_name,
            name: sessionData.first_name + ' ' + sessionData.last_name,
            cliente_id: sessionData.cliente_id
        };
        this.sessionMenu = sessionData.menu;
        localStorage.user = this.user.login;
        localStorage.cliente_id = this.user.cliente_id;
    };
    this.destroy = function () {
        this.userId = null;
        this.sessionData = null;
        this.sessionStatus = null;
        this.userFirstName = null;
        this.userLastName = null;
        this.userLogin = null;
        this.sessionMenu = null;
        this.userName = null;
    };
    return this;
});

