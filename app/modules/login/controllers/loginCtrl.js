'use strict';

/**
 * Created by darwin on 12/01/15.
 */
app.controller('loginCtrl', [
    '$scope', '$rootScope', 'AUTH_EVENTS', 'AuthService',
    function ($scope, $rootScope, AUTH_EVENTS, AuthService) {

        /**
         * Credenciales requeridas por el servidor para inicio de sesión.
         * @type {{identity: string, password: string}}
         */
        $scope.credentials = {
            identity: '',
            password: ''
        };

        /**
         * Ejecuta el incio de sesión en el servidor
         * @param credentials JSON con las credenciales de acceso que requiere el servidor.
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 14/01/15
         */
        $scope.login = function (credentials) {
            AuthService.login(credentials).then(function (response) {
                /**
                 * Se verifica el estado de la sesión y se disparan los evento que indican que la sesión se inició o no.
                 */
                if (!response.success) {
                    $rootScope.$broadcast(AUTH_EVENTS.loginFailed, response.messages);
                }
                else {
                    $rootScope.$broadcast(AUTH_EVENTS.loginSuccess);
                }

            }, function (response) {
                /**
                 * Si falla dispara el evento que notifica este suceso.
                 */
                $rootScope.$broadcast(AUTH_EVENTS.loginFailed);
            });
        };
    }
]);

/**
 * Eventos que se pueden disparar para manejar la sesión en la aplicación.
 * Se pensaron para definir eventos propios y ejecutarlos desde un $emit o $broadcast
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 14/01/15
 */
app.constant('AUTH_EVENTS', {
    loginSuccess: 'auth-login-success',
    loginFailed: 'auth-login-failed',
    logoutSuccess: 'auth-logout-success',
    sessionTimeout: 'auth-session-timeout',
    notAuthenticated: 'auth-not-authenticated',
    notAuthorized: 'auth-not-authorized',
    authChecked: 'auth-checked'
});

