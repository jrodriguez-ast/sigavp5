'use strict';

app.factory('genericconfiguracionModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        var _url = appConfig.getPathApi() + 'configuracion/generic/:id';
        var data = $resource(_url, {id: '@id'}, {
            update: {
                method: 'PUT'
            }
        });
        return data;
    }

]);