'use strict';

/**
 * Controlador que gestiona el listado de las configuraciones
 * @author Carlos Blanco <cebs923@gmail.com>
 * @packege SIGAVP
 * @version V-1.0 22/07/2015 10:32:30
 */

app.controller('GenericListController', ['$scope', 'r_permisos', function($scope, r_permisos){

    $scope.title= 'Configuraciones';
    $scope.siteCrud= {url:'configuracion/dt_generic'};

    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {title: 'Descripcion', label: 'description', visible: true},
        {title: 'Valor', label: 'value', visible: true },
        {title: 'Tipo publicacion', label: 'tipo_publicacion_id',  visible: false},
        {title: 'Validacion', label: 'validacion',  visible: false}
    ];

    $scope.operation = r_permisos.get.data.permission;

}]);


app.controller('EditarGenericConfigurationController', [
    "$scope",
    "$stateParams",
    "flashMessages",
    "$injector",
    "r_data",
    "configurationID",

    function ($scope, $stateParams, flashMessages, $injector, r_data, configurationID ) {

        var $validationProvider = $injector.get('$validation');

            $scope.generic_configuration = {};
            $scope.generic_configuration = r_data.data;

        $scope.contrato_form = {
            self: this,
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.contrato_form.send();
                });
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },
            prepare:function(object) {
                object = angular.copy(object);
                return object;
            },

            send: function () {
                var genericconfiguracionModel = $injector.get('genericconfiguracionModel');

                    var params = {id: configurationID};
                     genericconfiguracionModel.update(
                        params,
                        $scope.contrato_form.prepare($scope.generic_configuration),
                        $scope.contrato_form.success,
                        $scope.contrato_form.errors
                    );
            },
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando todo esta ok.
                // if(response.status == 'ok'){
                var flashMessages = $injector.get('flashMessages');
                var $location = $injector.get('$location');
                flashMessages.show_success("Los datos de la configuracion fueron Actualizados.");
                // }
                $location.path("admin/configuracion");
            },

            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {
                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }
                    ;

                    if (angular.isDefined(message.description)) {
                        mensajeToShow = mensajeToShow.concat(message.description);
                    }
                    //if (angular.isDefined(message.tipo_publicacion_id)) {
                    //    mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                    //}
                }
                ;
                var flashMessages = $injector.get('flashMessages');
                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);