'use strict';

/**
 * Modelo que contiene los datos a subir al servidor
 * @author Carlos Blanco <cebs923@gmail.com>
 * @packege SIGAVP
 * @version V-1.0 19/06/2015 10:32:30
 */
app.factory('ivaModel', ["$resource" , "appConfig" , function($resource, appConfig){
    var url= appConfig.getPathApi() + 'cotizaciones/iva/:id';
    return $resource(url, null, {
        update: {method: 'PUT'},
        delete: {method: 'DELETE'}
    });
}]);