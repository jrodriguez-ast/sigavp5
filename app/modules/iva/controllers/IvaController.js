'use strict';

/**
 * Controlador que gestiona el listado del Iva
 * @author Carlos Blanco <cebs923@gmail.com>
 * @packege SIGAVP
 * @version V-1.0 19/06/2015 10:32:30
 */
app.controller('IvaListController', ['$scope', 'r_permisos', function($scope, r_permisos){
    $scope.title= 'Historico de Iva';
    $scope.siteCrud= {url:'cotizaciones/iva_dt'};

    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {title: 'Valor', label: 'iva', filter: 'porcentaje'},
        {title: 'Fecha de actualización', label: 'fecha'},
        {title: 'Activo', label: 'activo', filter: 'si_no'}
    ];

    $scope.operation = r_permisos.get.data.permission;
}]);

/**
 * Controlador que Crea el iva
 * @author Carlos Blanco <cebs923@gmail.com>
 * @packege SIGAVP
 * @version V-1.0 19/06/2015 10:32:30
 */
app.controller('IvaFormController',['$scope', '$injector', function($scope, $injector){
    $scope.iva = {};

    var $validationProvider = $injector.get('$validation','IvaFormCtrl');
    var flashMessages = $injector.get('flashMessages','IvaFormCtrl');
    var $ngBootbox = $injector.get('$ngBootbox','IvaFormController');
    var crudFactory = $injector.get('crudFactory', 'IvaFormController');

    // Donde "type_form" es igual al atributo id de la etiqueta form
    // ejm: <form name="TypeForm" id="type_form">
    $scope.iva_form = {
        checkValid: $validationProvider.checkValid,
        submit: function (form) {
            $validationProvider.validate(form).success(function () {
                $injector.get('ivaModel','success').save(
                    null,
                    $scope.iva,
                    function(correcto){
                        if(correcto.status =='ok'){
                            flashMessages.show_success("Se actualizó el Iva correctamente.");
                            $injector.get('$state','fnc').go('admin.iva');
                            return;
                        }

                        flashMessages.show_error('Ocurrio un problema intente nuevamente.');

                    }, function(error){
                        var errors = error.data.errors;
                        var mensajeToShow = [];
                        for (var i = errors.length - 1; i >= 0; i--) {

                            var message = errors[i].message;
                            if (angular.isString(message)) {
                                mensajeToShow.push(message);
                                continue;
                            }

                            if(angular.isDefined(message.iva)){
                                mensajeToShow = mensajeToShow.concat(message.iva);
                            }
                        }

                        flashMessages.show_error('<p>'+mensajeToShow.join('</p><p>')+'</p>');
                });
            });//.error($scope.error);
        }
    }

    /**
     *
     *
     * @author Amalio Velasquez
     * */
    $scope.borrarIva = function(id){

        $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
            function(){
                var model = $injector.get('ivaModel','confirm');

                model.remove(
                    {
                        id: id
                    },
                    function(success){
                        if(success.status == 'ok'){
                            crudFactory.reloadData();
                            flashMessages.show_success(success.message);
                        }
                    },
                    function(error){
                        flashMessages.show_error(error.data.message);
                    }
                );
            }
            //, function(){}
        );
    }
}]);