'use strict';

/**
 * Modelo encargado de la manipulacion de la data de condiciones de pauta y negociaciones.
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 26/05/15 15:29:17
 */
app.factory('TipoIncidenciaModel', ["$resource", "appConfig", function ($resource, appConfig) {
    var url = appConfig.getPathApi() + 'cotizaciones/tipos_incidencias/:id';
    var _object = $resource(url, {id: '@_id'}, {
        update: {
            method: 'PUT' // this method issues a PUT request
        },
        tipos_incidencias_dt: {
            method: 'GET',
            url: appConfig.getPathApi() + 'cotizaciones/tipos_incidencias_dt'
        },
        delete: {
            method: 'DELETE'
        }
    });
    return _object;
}]);

