'use strict';

/**
 * Created by Carlos BLanco.
 */
app.factory('redSocialModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        return $resource(appConfig.getPathApi() + 'configuracion/redsocial/:id', null, {
            update: {
                method: 'PUT',
                url: appConfig.getPathApi() + 'configuracion/redsocial/:id',
                params: {
                    id: '@id'
                },
                isArray: false
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'configuracion/redsocial',
                isArray: false
            },
            crear: {
                delete: 'DELETE'
            }
        });
    }
]);