'use strict';

/**

 * @author Carlos blanco <cebs923@gmail.com>
 * @package SIGAVP
 * @version V-1.0 07/09/2015
 */
app.controller('RedSocialCtrlC', [
    '$scope',
    'particularPermission', '$injector',
    function
        ($scope,
         particularPermission, $injector) {
        var flashMessages = $injector.get('flashMessages', 'listClient');
        var $ngBootbox = $injector.get('$ngBootbox','listClient');
        var crudFactory = $injector.get('crudFactory', 'listClient');

        $scope.title = 'Redes Sociales';
        $scope.siteCrud = {url: 'configuracion/redsocial_dt'};

        $scope.headerCrud = [

            {title: 'Nombre', label: 'nombre', visible: true},
            {title: 'ID', label: 'id', visible: false}
        ];

        $scope.operation = particularPermission.permission;

        /**
         *
         * 
         * @author Amalio Velasquez
         * */
        $scope.borrarRed = function(red_social_id){

            $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
                function(){
                    var Cliente = $injector.get('redSocialModel','confirm');

                    Cliente.remove(
                        {
                            id: red_social_id
                        },
                        function(success){
                            if(success.status == 'ok'){
                                crudFactory.reloadData();
                                flashMessages.show_success(success.message);
                            }
                        },
                    function(error){
                        crudFactory.reloadData();
                        flashMessages.show_error(error.data.message);
                        }
                    );
                }
                //, function(){}
            );
        }
    }

]);
app.controller("CrearRedSocialController", [
    "$scope",
    "$stateParams",
    'flashMessages',
    '$injector',
    'redSocialModel',
    '$state',
    function ($scope,
              $stateParams,
              flashMessages,
              $injector,
              redSocialModel,
              $state) {

        $scope.detalles = {};
        $scope.detalles.nombre = '';

        /*
         Objeto que se enviara al Backend
         */

        $scope.contact = {};
        $scope.contact.nombre = '';


        var $validationProvider = $injector.get('$validation');

        // Donde "type_form" es igual al atributo id de la etiqueta form
        // ejm: <form name="TypeForm" id="type_form">
        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });//.error($scope.error);
            },

            reset: function (form) {
                $validationProvider.reset(form);
            },
            /**
             * Enviar las condiciones al servidor y resive las respuestas pertinentes.
             *
             * @author Jose Rodriugez
             * @version V-1.0 15/06/15 17:24:14
             * @return {void}
             */
            send: function () {
                $scope.contact.nombre = $scope.detalles.nombre;
                redSocialModel.crear(
                    $scope.contact,
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors);
            },
            /**
             * Obtiene la respuesta del servidor en caso de exito.
             *
             * @param  {JSON} response Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            success: function (response) {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando todo esta ok.
                // if(response.status == 'ok'){
                flashMessages.show_success("Se ha creado la red social.");
                $state.go('admin.red_social');
                // }
            },

            /**
             * Obtiene y muestra los errores de validación generados por laravel,
             * en una interfaz flashMessages Error.
             *
             * @param  {JSON} responseError Respuesta del servidor
             * @return {void}
             * @author Jose Rodriguez
             * @version V-1.1 15/06/15 14:37:36
             */
            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {

                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                    }

                    //if (angular.isDefined(message.nombre)) {
                    //    mensajeToShow = mensajeToShow.concat(message.nombre);
                    //}
                    //if (angular.isDefined(message.apellido)) {
                    //    mensajeToShow = mensajeToShow.concat(message.apellido);
                    //}
                }

                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);


app.controller('EditarRedSocialController', [
    "$scope",
    "$stateParams",
    "$injector",
    "r_data",
    "redSocialID",

    function (
        $scope,
        $stateParams,
        $injector,
        r_data,
        redSocialID ) {

        var $validationProvider = $injector.get('$validation');

        $scope.detalles = { };
        $scope.detalles = r_data.data;

        $scope.mediocontacto_form = {
            checkValid: $validationProvider.checkValid,
            submit: function (form) {
                $validationProvider.validate(form).success(function () {
                    $scope.mediocontacto_form.send();
                });
            },
            reset: function (form) {
                $validationProvider.reset(form);
            },

            prepare:function(object) {
                object = angular.copy(object);
                return object;
            },

            send: function () {
                var redSocialModel = $injector.get('redSocialModel');

                var params = {id: redSocialID};
                redSocialModel.update(
                    params,
                    $scope.mediocontacto_form.prepare($scope.detalles),
                    $scope.mediocontacto_form.success,
                    $scope.mediocontacto_form.errors
                );
            },
            success: function () {
                // No evaluamos OK xq el servidor responde 200 siempre y cuando todo esta ok.
                // if(response.status == 'ok'){
                var flashMessages = $injector.get('flashMessages');
                var $location = $injector.get('$location');
                flashMessages.show_success("El nombre de la red social fue actualizado");
                // }
                $location.path("admin/red_social");
            },

            errors: function (responseError) {
                var errors = responseError.data.errors;
                var mensajeToShow = [];
                for (var i = errors.length - 1; i >= 0; i--) {
                    var message = errors[i].message;
                    if (angular.isString(message)) {
                        mensajeToShow.push(message);
                        continue;
                    }

                    if (angular.isDefined(message.description)) {
                        mensajeToShow = mensajeToShow.concat(message.description);
                    }
                    //if (angular.isDefined(message.tipo_publicacion_id)) {
                    //    mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                    //}
                }

                var flashMessages = $injector.get('flashMessages');
                flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
            }
        }
    }
]);
