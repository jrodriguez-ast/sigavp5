'use strict';

/**
 * Modelo encargado de la manipulación de la data de Operaciones.
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 22/06/15 14:23:00
 */
app.factory('OperacionesModel', ["$resource", "appConfig", function ($resource, appConfig) {
    var url = appConfig.getPathApi() + 'configuracion/operaciones/:id';
    var params = {id: '@_id'};
    return $resource(
        url,
        params, {
            update: {method: 'PUT'},
            icons: {
                url: appConfig.getPathApi() + 'configuracion/operaciones/icons',
                isArray: true
            },
            renders:{
                url: appConfig.getPathApi() + 'configuracion/operaciones/renders',
                isArray: true
            },
            visuals:{
                url: appConfig.getPathApi() + 'configuracion/operaciones/visuals',
                isArray: true
            },
            targets:{
                url: appConfig.getPathApi() + 'configuracion/operaciones/targets',
                isArray: true
            }
        }
    );
}]);