'use strict';

app.factory('contratoModel', [
    '$resource',
    'appConfig',
    function ($resource, appConfig) {
        var _url = appConfig.getPathApi() + 'clientes/contratos/:id';
        var data = $resource(_url, null, {
            update: {
                method: 'PUT'
            },
            crear: {
                method: 'POST',
                url: appConfig.getPathApi() + 'clientes/cliente/:client/contratos',
                params: {
                    client: '@client'
                },
                isArray: false
            },
            listar: {
                method: 'GET',
                url: appConfig.getPathApi() + 'clientes/contratos_activos/:client',
                params: {
                    client: '@client'
                },
                isArray: false
            },
            tiposContratos: {
                method: 'GET',
                url: appConfig.getPathApi() + 'clientes/tipos_contratos',
                isArray: false
            },
            listTipoContrato: {
                method: 'GET',
                url: appConfig.getPathApi() + 'clientes/list_tipo_contrato/:client',
                isArray: false,
                params: {
                    client: '@client'
                }
            },
            delete: {
                method: 'DELETE'
            }
        });
        return data;
    }

]);