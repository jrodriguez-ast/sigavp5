'use strict';
/**
 *Controlador que gestiona el listado de clientes
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 * @package AVP
 *
 */
app.controller("listarContratos", ["$scope", "permission", "$injector", 
    function ($scope, permission, $injector) {

    var flashMessages = $injector.get('flashMessages', 'listarContratos');
    var $ngBootbox = $injector.get('$ngBootbox','listarContratos');
    var crudFactory = $injector.get('crudFactory', 'listarContratos');

    $scope.title = "Contratos con Clientes de AVP";
    $scope.siteCrud = {url: 'clientes/contratoDt'};
    $scope.headerCrud = [
        {title: 'id', label: 'id', visible: false},
        {title: 'cliente_id', label: 'cliente_id', visible: false},
        {title: 'RIF', label: 'cliente.rif'},
        {title: 'Cliente', label: 'cliente.razon_social'},
        {title: 'Tipo', label: 'tipo.nombre'},
        {title: 'Código', label: 'codigo'},
        {title: 'Monto (Bs.)', label: 'tarifa', filter: 'formatNumberMoney'},
        {title: 'IVA (Bs.)', label: 'monto_iva', filter: 'formatNumberMoney'},
        {title: 'F. Inicio', label: 'fecha_inicio', filter: 'date', filterParams: 'dd/MM/yyyy'},
        {title: 'F. Fin', label: 'fecha_inicio', filter: 'date', filterParams: 'dd/MM/yyyy'},
        {title: 'Relacionado con', label: 'relacion'}
    ];
    $scope.operation = permission.data.get.data.permission;

    /**
     *
     *
     * @author Amalio Velasquez
     * */
    $scope.borrarContrato = function(id){

        $ngBootbox.confirm("¿Esta seguro que desea borrar este registro?").then(
            function(){
                var Contrato = $injector.get('contratoModel','confirm');

                Contrato.remove(
                    {
                        id: id
                    },
                    function(success){
                        if(success.status == 'ok'){
                            crudFactory.reloadData();
                            flashMessages.show_success(success.message);
                        }
                    },
                function(error){
                    crudFactory.reloadData();
                    flashMessages.show_error(error.data.message);
                    }
                );
            }
        );
    }
}]);


/**
 *Controlador que gestiona el listado de clientes
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 * @package AVP
 *
 */
app.controller("listarContratosPorCliente", [
    "$scope",
    "particularPermission",
    "clientID",
    "$injector",
    function ($scope,
              particularPermission,
              clientID,
              $injector) {
        $scope.title = "Contratos del Cliente con AVP";
        $scope.siteCrud = {url: 'clientes/contratoClienteDt/' + clientID};
        $scope.headerCrud = [
            {title: 'id', label: 'id', visible: false},
            {title: 'Tipo', label: 'tipo.nombre'},
            {title: 'Código', label: 'codigo'},
            {title: 'Monto (Bs.)', label: 'tarifa', filter: 'formatNumberMoney'},
            {title: 'IVA (Bs.)', label: 'monto_iva', filter: 'formatNumberMoney'},
            {title: 'F. Inicio', label: 'fecha_inicio', filter: 'date', filterParams: 'dd/MM/yyyy'},
            {title: 'F. Fin', label: 'fecha_inicio', filter: 'date', filterParams: 'dd/MM/yyyy'},
            {title: 'Relacionado con', label: 'relacion'}
        ];
        $scope.operation = particularPermission.permission;
        /**
         * Funciones que administran las pestañas de vista y edición de contratos del cliente
         * @param contratoID
         * @param operation_id
         */
        $scope.detalleContrato = function (contratoID, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Detalle Contrato #',
                state: 'admin.detalle_cliente.detalle_contrato({contratoID:' + contratoID + '})',
                object_id: contratoID
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state', 'detalleContrato');
            $state.go('^.detalle_contrato', {'contratoID': contratoID});
        };
        /**
         * Acción JS para poder Editar el Contrato
         * @param contratoID
         * @param operation_id
         */
        /*$scope.editarContrato = function (contratoID, operation_id) {
         var tab = {
         id: operation_id,
         title: 'Editar Contrato #',
         state: 'admin.detalle_cliente.editar_contrato({contratoID:' + contratoID + '})',
         object_id: contratoID
         };
         $scope.buildTab(tab);
         var $state = $injector.get('$state', 'editarContrato');
         $state.go('^.editar_contrato', {'contratoID': contratoID});
         };*/

        /**
         * Permite el acceso al balance del contrato.
         * @param contrato_id
         * @param operation_id
         */
        $scope.balanceContrato = function (contrato_id, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Balance Contrato #',
                state: 'admin.detalle_cliente.balance_contrato({contratoID:' + contrato_id + '})',
                object_id: contrato_id
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state', 'balanceContrato');
            $state.go('^.balance_contrato', {'contratoID': contrato_id});
        };

        /**
         * Permite el acceso al balance del contrato por tipo.
         * @param contrato_id
         * @param operation_id
         */
        $scope.balancextipo = function (contrato_id, operation_id) {
            var tab = {
                id: operation_id,
                title: 'Balance Contrato #',
                state: 'admin.detalle_cliente.balance_x_tipo({contratoID:' + contrato_id + '})',
                object_id: contrato_id
            };
            $scope.buildTab(tab);
            var $state = $injector.get('$state', 'balancextipo');
            $state.go('^.balance_x_tipo', {'contratoID': contrato_id});
        };
    }]);
/**
 * ContratoFormController
 * @author Maykol Purica
 * Controlador que gestiona las acciones crear, editar y visualizar de contratos
 */
app.controller('ContratoFormController',
    ['$scope', "$injector", 'rContrato', 'contratoID', 'rAction', 'rTitle', 'rCliente', 'rClienteID',
        '$modalInstance', 'r_tipo_contrato', 'r_list_tipo_contrato', 'TipoPublicacionResolve', 'rTabId', /*'r_regimen',*/
        function ($scope, $injector, rContrato, contratoID, rAction, rTitle, rCliente, rClienteID,
                  $modalInstance, r_tipo_contrato, r_list_tipo_contrato, TipoPublicacionResolve, rTabId /*, r_regimen*/) {

            var IVA = 0;
            if (rAction == 'store') {
                //Se obtiene el iva para pintar en la interfaz
                $injector.get('ivaModel', 'ContratoFormCtrl').get(function (response) {
                    if (response.status == 'ok') {
                        IVA = (response.data.iva / 100);
                    }
                });
            }

            //console.log(rContrato);
            // Directiva de Validación.
            var $validationProvider = $injector.get('$validation', 'ContratoFormCtrl');
            var $filter = $injector.get('$filter', 'ContratoFormCtrl');
            var $state = $injector.get('$state', 'ContratoFormCtrl');

            $scope.formAction = rAction;
            $scope.title = rTitle;
            $scope.contratoID = contratoID;
            $scope.tipo = r_tipo_contrato.data;
            $scope.contratos = r_list_tipo_contrato.data;
            $scope.tipospublicaciones = TipoPublicacionResolve;

            $scope.calendar = {
                opened: {},
                dateFormat: 'dd/MM/yyyy',
                dateOptions: {
                    formatYear: 'yy',
                    startingDay: 1
                },
                now: new Date(),
                open: function ($event, which) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    angular.forEach($scope.calendar.opened, function (value, key) {
                        $scope.calendar.opened[key] = false;
                    });

                    $scope.calendar.opened[which] = true;
                },
                disabled: function (date, mode) {
                    return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
                }
            };
            // #########################################

            $scope.forma = [{}, {id: 1, nombre: 'Post-pago'}, {id: 0, nombre: 'Pre-pago'}];
            if ($scope.formAction != 'store') {
                $scope.contrato = angular.copy(rContrato.data);
                //$scope.contrato.regimenn = r_regimen.data;
                //$scope.contrato.regimen_pago_id = {'id': $scope.contrato.regimen_pago_id};
                //$scope.contrato.forma_pago = ($scope.contrato.es_postpago == 1) ? $scope.forma[1] : $scope.forma[2];
                $scope.contrato.obj_tipo_contrato = $scope.contrato.tipo;
                $scope.contrato.obj_contrato_original = $scope.contrato.parent;

                var tarifa = (rContrato.data.tarifa) * 1, mIva = rContrato.data.monto_iva * 1;
                $scope.contrato.tarifa = $filter('formatNumberMoney')(tarifa);
                $scope.contrato.monto_iva = $filter('formatNumberMoney')(mIva);
                $scope.contrato.total = $filter('formatNumberMoney')(tarifa + mIva);

                for (var i = $scope.contrato.pautas.length - 1; i >= 0; i--) {
                    //$scope.contrato.pautas[i].cantidad *= 1;
                    tarifa = rContrato.data.pautas[i].monto * 1;
                    mIva = rContrato.data.pautas[i].monto_iva * 1;

                    $scope.contrato.pautas[i].monto = $filter('formatNumberMoney')(tarifa);
                    $scope.contrato.pautas[i].monto_iva = $filter('formatNumberMoney')(mIva);
                    $scope.contrato.pautas[i].total = $filter('formatNumberMoney')(tarifa + mIva);

                }

                for (var i = $scope.contrato.cuotas.length - 1; i >= 0; i--) {
                    tarifa = rContrato.data.cuotas[i].monto * 1;
                    mIva = rContrato.data.cuotas[i].monto_iva * 1;

                    $scope.contrato.cuotas[i].monto = $filter('formatNumberMoney')(tarifa);
                    $scope.contrato.cuotas[i].monto_iva = $filter('formatNumberMoney')(mIva);
                    $scope.contrato.cuotas[i].total = $filter('formatNumberMoney')(tarifa + mIva);
                }

            } else {
                $scope.contrato = {};
                $scope.contrato.tarifa = '0';
                $scope.contrato.monto_iva = '0';
                $scope.contrato.total = '0';
                $scope.contrato.por_publicacion = false;
                $scope.contrato.pautas = [];
                $scope.contrato.cuotas = [];
                //$scope.contrato.regimenn = r_regimen.data;
                $scope.contrato.cliente = rCliente.data;
                $scope.contrato.fecha_creacion = new Date();
            }
            $scope.contrato_form = {
                self: this,
                checkValid: $validationProvider.checkValid,
                submit: function (form) {

                    $validationProvider.validate(form).success(function () {
                        var validPautas = true;
                        // console.log($scope.checkMontoTotalPautas(), 'MONTO PAUTAS');
                        switch ($scope.checkMontoTotalPautas()) {
                            case -1:
                                $scope.contrato.pautas.mensaje = "El monto total de las pautas es menor al monto del contrato.";
                                validPautas = false;
                                break;
                            case 0:
                                $scope.contrato.pautas.mensaje = "";
                                break;
                            case 1:
                                $scope.contrato.pautas.mensaje = "El monto total de las pautas es mayor al monto del contrato.";
                                validPautas = false;
                                break;
                            default:
                            //default code block
                        }
                        //console.log($scope.checkMontoTotalCuotas(), 'MONTO CUOTAS');
                        var validCuotas = true;
                        switch ($scope.checkMontoTotalCuotas()) {
                            case -1:
                                $scope.contrato.cuotas.mensaje = "El monto total de las cuotas es menor al monto del contrato.";
                                validCuotas = false;
                                break;
                            case 0:
                                $scope.contrato.cuotas.mensaje = "";
                                break;
                            case 1:
                                $scope.contrato.cuotas.mensaje = "El monto total de las cuotas es mayor al monto del contrato.";
                                validCuotas = false;
                                break;
                            default:
                            //default code block
                        }
                        $scope.contrato.cuotas.outrange = $scope.checkDatesCuotas();
                        //console.log($scope.checkDatesCuotas(), 'FECHAS CUOTAS');
                        if ($scope.contrato.cuotas.outrange.length == 0 && validCuotas != false && validPautas != false) {
                            $scope.contrato_form.send();
                        }
                        /* $validationProvider.validate(form).success(function () {
                         $scope.contrato_form.SendData();
                         });*/
                    });
                },
                reset: function (form) {
                    $validationProvider.reset(form);
                },
                prepare: function (service) {
                    var data = angular.copy(service);
                    if (data.obj_tipo_contrato.nombre == 'addendum') {
                        data.contratos_id = data.obj_contrato_original.id;
                    }

                    data.tipo_contrato_id = data.obj_tipo_contrato.id;

                    delete data.obj_tipo_contrato;
                    delete data.total;

                    if (data.pautas.length > 0) {
                        for (var i = data.pautas.length - 1; i >= 0; i--) {
                            data.pautas[i].monto = Number($filter('formatMoneyToNumber')(data.pautas[i].monto));
                            data.pautas[i].monto_iva = Number($filter('formatMoneyToNumber')(data.pautas[i].monto_iva));
                            data.pautas[i].tipo_publicacion_id = data.pautas[i].tipopub.id;
                            delete data.pautas[i].tipopub;
                            delete data.pautas[i].total;
                        }
                    }
                    if (data.cuotas.length > 0) {
                        for (var i = data.cuotas.length - 1; i >= 0; i--) {
                            data.cuotas[i].monto = Number($filter('formatMoneyToNumber')(data.cuotas[i].monto));
                            data.cuotas[i].monto_iva = Number($filter('formatMoneyToNumber')(data.cuotas[i].monto_iva));
                            delete data.cuotas[i].total;
                        }
                    }

                    data.tarifa = $filter('formatMoneyToNumber')(data.tarifa);
                    data.monto_iva = $filter('formatMoneyToNumber')(data.monto_iva);
                    return data;
                },

                send: function () {
                    var contratoModel = $injector.get('contratoModel', 'send');
                    var params = {};
                    /* Comentado Ya que los contratos no se pueden editar.
                     if ($scope.formAction == 'update') {
                     params.id = contratoID;
                     contratoModel.update(
                     params,
                     $scope.contrato_form.prepare($scope.contrato),
                     $scope.contrato_form.success,
                     $scope.contrato_form.errors
                     );
                     }
                     if ($scope.formAction == 'store') {
                     */
                    params.client = rClienteID;
                    contratoModel.crear(
                        params,
                        $scope.contrato_form.prepare($scope.contrato),
                        $scope.contrato_form.success,
                        $scope.contrato_form.errors
                    );
                    $modalInstance.close();
                    //}
                },
                success: function () {
                    // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
                    // if(response.status == 'ok'){
                    $injector.get('flashMessages', 'success').show_success("Los datos del contrato fueron almacenados.");
                    $injector.get('$location', 'success').path("admin/detalle_cliente/" + rClienteID + "/contratos");
                    //}
                },

                errors: function (responseError) {
                    var errors = responseError.data.errors;
                    var mensajeToShow = [];
                    for (var i = errors.length - 1; i >= 0; i--) {
                        var message = errors[i].message;
                        if (angular.isString(message)) {
                            mensajeToShow.push(message);
                            continue;
                        }

                        if (angular.isDefined(message.nombre)) {
                            mensajeToShow = mensajeToShow.concat(message.nombre);
                        }
                        if (angular.isDefined(message.tipo_publicacion_id)) {
                            mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                        }
                    }
                    var flashMessages = $injector.get('flashMessages', 'processFormErrors');
                    flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
                },

                /**
                 * Calcula el IVA de un monto introducido por el usuario.
                 * @author Jose Rodriguez
                 * @version 1.0 5/01/2016
                 */
                calculateIVA: function (destino, index) {
                    var result = 0, tarifa = 0;
                    if (destino == 'pauta_iva') {
                        tarifa = $filter('formatMoneyToNumber')($scope.contrato.pautas[index].monto) * 1;
                        result = IVA * tarifa;
                        $scope.contrato.pautas[index].monto_iva = $filter('formatNumberMoney')(result);
                        $scope.contrato.pautas[index].total = $filter('formatNumberMoney')(result + tarifa);
                    } else if (destino == 'cuota_iva') {
                        tarifa = $filter('formatMoneyToNumber')($scope.contrato.cuotas[index].monto) * 1;
                        result = IVA * tarifa;
                        $scope.contrato.cuotas[index].monto_iva = $filter('formatNumberMoney')(result);
                        $scope.contrato.cuotas[index].total = $filter('formatNumberMoney')(result + tarifa);
                    } else { // (destino == 'monto_iva')
                        tarifa = $filter('formatMoneyToNumber')($scope.contrato.tarifa) * 1;
                        result = IVA * tarifa;
                        $scope.contrato.monto_iva = $filter('formatNumberMoney')(result);
                        $scope.contrato.total = $filter('formatNumberMoney')(result + tarifa);
                    }
                }
            };


            $scope.imprimirPdf = function () {
                //console.log($scope.contratoID);
                var appConfig = $injector.get('appConfig', 'imprimirPdf');
                appConfig.printPDF('contrato', $scope.contratoID * 1);

            };

            $scope.getDetallesContrato = function (cliente, contrato) {
                $state.go('admin.detalle_cliente.editar_contrato', {contratoID: contrato, id: cliente, tabId: 1020})
            };

            $scope.agregarPautas = function () {
                $scope.contrato.pautas.push({cantidad: 1, monto: '0', monto_iva: '0'});
                //console.log($scope.contrato.pautas);
            };
            $scope.agregarCuotas = function () {
                $scope.contrato.cuotas.push({fecha: new Date(), monto: '0', monto_iva: '0'});
                //console.log($scope.contrato.pautas);
            };
            $scope.removePauta = function (index) {
                $scope.contrato.pautas.splice(index, 1)
            };
            $scope.removeCuota = function (index) {
                $scope.contrato.cuotas.splice(index, 1)
            };
            /*
             Retorna 0 si el monto total de pautas es igual al monto del contrato
             Retorna 1 si el monto total de pautas es mayor al monto del contrato
             Retorna -1 si el monto total de pautas es menor al monto del contrato
             */
            $scope.checkMontoTotalPautas = function () {
                if ($scope.contrato.tarifa.length > 0) {
                    var tarifa = $filter('formatMoneyToNumber')($scope.contrato.tarifa) * 1;
                    tarifa = tarifa + ($filter('formatMoneyToNumber')($scope.contrato.monto_iva) * 1);

                    var total = 0;
                    for (var i = $scope.contrato.pautas.length - 1; i >= 0; i--) {
                        total = total + ($filter('formatMoneyToNumber')($scope.contrato.pautas[i].monto) * 1);
                        total = total + ($filter('formatMoneyToNumber')($scope.contrato.pautas[i].monto_iva) * 1);

                    }

                    if (total == tarifa) {
                        return 0;
                    } else if (total > tarifa) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            };
            /*
             Retorna 0 si el monto total de pautas es igual al monto del contrato
             Retorna 1 si el monto total de pautas es mayor al monto del contrato
             Retorna -1 si el monto total de pautas es menor al monto del contrato
             */
            $scope.checkMontoTotalCuotas = function () {
                if ($scope.contrato.cuotas.length > 0) {
                    var tarifa = $filter('formatMoneyToNumber')($scope.contrato.tarifa) * 1;
                    tarifa = tarifa + ($filter('formatMoneyToNumber')($scope.contrato.monto_iva) * 1);

                    var total = 0;
                    for (var i = $scope.contrato.cuotas.length - 1; i >= 0; i--) {
                        total = total + ($filter('formatMoneyToNumber')($scope.contrato.cuotas[i].monto) * 1);
                        total = total + ($filter('formatMoneyToNumber')($scope.contrato.cuotas[i].monto_iva) * 1);
                    }
                    if (total == tarifa) {
                        return 0;
                    } else if (total > tarifa) {
                        return 1;
                    } else {
                        return -1;
                    }
                }
            };
            /**
             * Retorna FALSE si alguna de las fechas de las cuotas no se encuentra entre
             la fecha de inicio y fecha de fin del contrato
             */
            $scope.checkDatesCuotas = function () {
                var tmstmp_inicio = $scope.contrato.fecha_inicio.getTime();
                var tmstmp_fin = $scope.contrato.fecha_fin.getTime();
                var out = [];
                //console.log($scope.contrato.cuotas[0]);
                for (var i = 0; i < $scope.contrato.cuotas.length; i++) {
                    if ($scope.contrato.cuotas[i].fecha.getTime() >= tmstmp_inicio && $scope.contrato.cuotas[i].fecha.getTime() <= tmstmp_fin) {
                        //Continua
                    } else {
                        out.push({cuota: i + 1});
                    }
                }

                return out;
            };

            $scope.togglePorPublicacion = function () {
                if ($scope.contrato.por_publicacion) {
                    $scope.contrato.cuotas = [];
                }
            };

            if (rTabId != null) {
                var tab = {
                    id: rTabId,
                    title: 'Ver Contrato #',
                    state: 'admin.detalle_cliente.detalle_contrato({contratoID:' + contratoID + '})',
                    object_id: contratoID
                };
                $scope.buildTab(tab);
                $state.go('^.detalle_contrato', {'contratoID': contratoID});
            }

            if (rAction == 'store') {
                $scope.agregarPautas();
                $scope.agregarCuotas();
            }

        }]);
