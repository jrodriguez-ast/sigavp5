'use strict';

/**
 * Modelo encargado de la manipulación de la data de condiciones de pauta y negociaciones.
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 26/05/15 15:29:17
 */
app.factory('CondicionModel', ["$resource", "appConfig", function ($resource, appConfig) {
    var url = appConfig.getPathApi() + 'medios/medio/:medioId/servicio/:servicioId/condiciones/:id/:deactivate';
    var params = {medioId: '@medioId',servicioId: '@servicioId', id:'@condicionId', deactivate: '@deactivate'};
    return $resource(url, params, {
        update: { method: 'PUT'},
        listado: {
            method: 'GET',
            url: appConfig.getPathApi() + 'cotizaciones/:servicioId/:tipoPublicacionId/conditions/listado',
            params: {
                servicioId: '@servicioId',
                tipoPublicacionId: '@tipoPublicacionId'
            },
            isArray: true
        },
        listado_detalle_condicion: {
            method: 'GET',
            url: appConfig.getPathApi() + 'cotizaciones/:servicioId/:condicionId/conditions/listado_detalle_condicion',
            params: {
                servicioId: '@servicioId',
                condicionId: '@condicionId'
            },
            isArray: true
        },
        activate: { method: 'PATCH' }
    });
}]);
