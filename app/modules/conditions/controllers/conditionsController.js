'use strict';
/**
 * Gestiona el listado de Condiciones.
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.0 06/01/2015
 */
app.controller("ConditionsListController", ["$scope", "permission", function ($scope, permission) {
    $scope.title = "Condiciones";

    $scope.siteCrud = {url: 'cotizaciones/conditions_dt'};

    $scope.headerCrud = [
        {title: 'ID', label: 'id', visible: false},
        {title: 'Nombre', label: 'nombre', visible: true},
        {
            title: 'Tipo de Publicación',
            label: 'nombre_tipo',
            visible: true,
            filter: 'link',
            filterParams: {relatedId: 'id_tipo', url: '#/admin/TiposPublicaciones/view'}
        },
        {title: 'Tipo ID', label: 'id_tipo', visible: false}
    ];

    $scope.operation = permission.data.get.data.permission;
}]);

/**
 * Controlador que gestiona la creación de Condiciones
 *
 * @author Jose Rodriguez <josearodrigueze@gmail.com>
 * @package Cotizaciones A.S.T
 * @version V-1.1  15:43:32
 */
app.controller('CondicionesModalController', ['$scope', '$modalInstance', '$injector', 'rService', 'rCondition', 'rViewMode', 'rAction', 'rhasBaseCondition', 'rMsj', function ($scope, $modalInstance, $injector, rService, rCondition, rViewMode, rAction, rhasBaseCondition, rMsj) {
    // Inyectamos las dependencias a utilizar dentro del Controlador.
    var $validationProvider = $injector.get('$validation', 'CondicionesModalController');
    $scope.mensaje = rMsj;
    $scope.service = rService.data;
    $scope.viewMode = rViewMode;

    // Estos valores están quemados para los selects la es_base y porcentaje.
    $scope.quemado = {
        porcentaje: [{}, {id: 1, nombre: 'Porcentaje'}, {id: 0, nombre: 'Bolivares'}],
        base: [{}, {id: 1, nombre: 'Base'}, {id: 0, nombre: 'Recargo'}]
    };
    if (rhasBaseCondition == 'false') {
        $scope.quemado.base = [{id: 1, nombre: 'Si'}];
    }
    // Objeto para el manejo de condiciones.
    $scope.condition = rCondition.data;
    $scope.condition.deleted_details = [];

    if (!angular.isDefined($scope.condition.detalles))
        $scope.condition.detalles = [];

    if (rAction != 'new') {
        $scope.condition.esPorcentaje = ($scope.condition.es_porcentaje == 1) ? $scope.quemado.porcentaje[1] : $scope.quemado.porcentaje[2];
        $scope.condition.esBase = ($scope.condition.es_base == 1) ? $scope.quemado.base[1] : $scope.quemado.base[2];
    }

    // Transformamos la tarifa en el formato de la mascara.
    // Transformamos la fecha en un objeto date, para que datepicker lo interprete correctamente.

    for (var i = $scope.condition.detalles.length - 1, p = []; i >= 0; i--) {

        $scope.condition.detalles[i].tarifa = number_format($scope.condition.detalles[i].tarifa, 2, ',', '.');
        $scope.condition.detalles[i].principal = ($scope.condition.detalles[i].principal == 1);
        if ($scope.condition.detalles[i].fecha != null) {
            p = $scope.condition.detalles[i].fecha.split('-');
            $scope.condition.detalles[i].fecha = new Date(p[0], p[1], p[2]);
        }
    }

    // Donde "type_form" es igual al atributo id de la etiqueta form
    // ejm: <form name="TypeForm" id="type_form">
    $scope.conditions_form = {
        self: this,
        checkValid: $validationProvider.checkValid,
        submit: function (form) {
            $validationProvider.validate(form).success(function () {
                $scope.conditions_form.send();
            });//.error($scope.error);
        },

        reset: function (form) {
            $validationProvider.reset(form);
        },

        /**
         * Agrega un elemento al arreglo de detalles para aparezca otro input.
         * @author Jose Rodriguez <josearodrigueze@gmail.com>
         * @version V-1.0  01/06/15 12:33:02
         */
        addDetail: function () {
            $scope.condition.detalles.push({id: new Date().getTime(), fecha: new Date()});
        },

        /**
         * Data un valor dentro de un objeto json elimina esa posición.
         *
         * @param  {integer} item TimeStamp con el valor buscado
         * @return {void}
         * @author Jose Rodriguez <josearodrigueze@gmail.com>
         * @version V-1.0  01/06/15 12:34:33
         */
        removeDetail: function (item) {
            var _details = $scope.condition.detalles, deleted = null;
            angular.forEach(_details, function (detail) {
                if (detail.id == item) {
                    deleted = _details.splice(_details.indexOf(detail), 1);
                    $scope.condition.deleted_details.push(deleted[0].id);
                    return false;
                }
            });
        },

        /**
         * Cuando la propiedad "es_base" es cambiada a true, la propiedad
         * "es_porcentaje" cambia a false, indicando que el valor esta expreso
         * en Bolivares.
         *
         * @version V-1.0 15/06/15 15:15:27
         * @author Jose Rodriguez
         * @return {void}
         */
        setPorcentajeToBs: function () {
            if ($scope.condition.esBase.id == 1) {
                $scope.condition.esPorcentaje = $scope.quemado.porcentaje[2];
            }
            if (rhasBaseCondition == 'false') {
                $scope.condition.esBase = $scope.quemado.base[0];
            }
        },

        /**
         * Prepara la data de condiciones a ser enviada al servidor.
         * @param  {json} condition
         * @return {json}           data preparada.
         * @author Jose Rodriguez
         * @version V-1.0 16/06/15 17:25:32
         */
        prepare: function (condition) {
            var data = angular.copy(condition);
            data['es_porcentaje'] = data['esPorcentaje'].id;
            data['es_base'] = data['esBase'].id;
            delete data['esBase'];
            delete data['esPorcentaje'];
            var d = data.detalles;
            for (var i = d.length - 1; i >= 0; i--) {
                d[i].tarifa = $injector.get('$filter', 'prepare')('formatMoneyToNumber')(d[i].tarifa);
                d[i].fecha = moment(d[i].fecha).format('YYYY-MM-DD');
            }

            return data;
        },

        /**
         * Enviar las condiciones al servidor y recibe las respuestas pertinentes.
         *
         * @author Jose Rodriguez
         * @version V-1.0 15/06/15 17:24:14
         * @return {void}
         */
        send: function () {
            var conditionModel = $injector.get('CondicionModel', 'send');
            // Parámetros necesarios para usar el servicio.
            // de forma semántica.
            var confirmacion = true;
            if (rhasBaseCondition == 'true' && $scope.condition.esBase.id == 1) {
                confirmacion = confirm("¿Está seguro? Esta acción cambiará la condición base actual.");
            }
            if (!confirmacion) {
                return;
            }
            var params = {
                medioId: rService.data.medio_id,
                servicioId: rService.data.id,
                id: rCondition.data.id
            };

            if (rAction == 'new') {
                conditionModel.save(
                    params,
                    $scope.conditions_form.prepare($scope.condition),
                    $scope.conditions_form.success,
                    $scope.conditions_form.errors
                );
            } else if (rAction == 'update') {
                conditionModel.update(
                    params,
                    $scope.conditions_form.prepare($scope.condition),
                    $scope.conditions_form.success,
                    $scope.conditions_form.errors
                );
            }
        },

        /**
         * Obtiene la respuesta del servidor en caso de exito.
         *
         * @param  {JSON} response Respuesta del servidor
         * @return {void}
         * @author Jose Rodriguez
         * @version V-1.1 15/06/15 14:37:36
         */
        success: function (response) {
            // No evaluamos OK xq el servidor responde 200 siempre y cuando esta ok.
            // if(response.status == 'ok'){
            $injector.get('flashMessages', 'success').show_success("Los datos de la condición fueron actualizados. (" + response.message + ")");
            $modalInstance.close();
            // }
        },

        /**
         * Obtiene y muestra los errores de validación generados por laravel,
         * en una interfaz flashMessages Error.
         *
         * @param  {JSON} responseError Respuesta del servidor
         * @return {void}
         * @author Jose Rodriguez
         * @version V-1.1 15/06/15 14:37:36
         */
        errors: function (responseError) {
            var errors = responseError.data.errors;
            var mensajeToShow = [];
            for (var i = errors.length - 1; i >= 0; i--) {

                var message = errors[i].message;
                if (angular.isString(message)) {
                    mensajeToShow.push(message);
                    continue;
                }

                if (angular.isDefined(message.nombre)) {
                    mensajeToShow = mensajeToShow.concat(message.nombre);
                }
                if (angular.isDefined(message.tipo_publicacion_id)) {
                    mensajeToShow = mensajeToShow.concat(message.tipo_publicacion_id);
                }
            }

            var flashMessages = $injector.get('flashMessages', 'conditions_form.errors');
            flashMessages.show_error('<p>' + mensajeToShow.join('</p><p>') + '</p>');
        },

        /**
         *
         * @param index
         */
        principal: function (index) {
            for (var i = $scope.condition.detalles.length - 1; i >= 0; i--) {
                if (index == i) {
                    continue;
                }
                $scope.condition.detalles[i].principal = false;
            }
        }
    };

    // Para manejo de multiples Datapicker en el formulario
    $scope.calendar = {

        // Maneja el estado de los calendarios
        openedi: {},

        // Indica el formato de la fecha en los calendarios
        dateFormat: 'dd-MM-yyyy',
        //Opciones del calendario
        dateOptions: {
            formatYear: 'yy',
            startingDay: 1
        },

        // Este atributo es opcional, se utiliza para indicar el min-date o menor día posible a elegir
        now: new Date(),
        /**
         * Método que controla la apertura de los calendarios, dada una clave indicada en el método del click
         *
         * @param $event
         * @param which
         * @param v
         */
        open: function ($event, which, v) {
            $event.preventDefault();
            $event.stopPropagation();

            angular.forEach($scope.calendar.openedi, function (value, key) {
                $scope.calendar.openedi[key] = false;
            });
            if (v == 'fecha') {
                $scope.calendar.openedi[which] = true;
            }
        },
        /**
         * Método que desactiva la selección de fines de semana en el calendario
         *
         * @param date
         * @param mode
         * @returns {boolean}
         */
        disabled: function (date, mode) {
            return ( mode === 'day' && ( date.getDay() === 0 || date.getDay() === 6 ) );
        }
    };
    // Fin de manejo de Datapicker

}]);
/* End of file conditionsController.js */
