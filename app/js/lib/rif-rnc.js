/**
 * Directiva que muestra en pantalla el estatus del RNC en función del rif pasado como parámetro.
 * @author Maykol Purica
 *         Jose Rodriguez
 * @version V-1.1
 */
app.directive("rifRnc", ['$rootScope', '$compile', 'consultaRifModel', function ($rootScope, $compile, consultaRifModel) {
    return {
        restrict: "E",
        link: function ($scope, element) {
            consultaRifModel.rnc({Attr_rif: $scope.rif}).$promise.then(function (result) {
                var style = (result.rnc.estatus.search('INHABILITADA') >= 0) ? 'danger' : 'info',
                    div = angular.element('<div/>').addClass('alert').addClass('alert-' + style),
                    h4 = angular.element('<h4/>').text(result.rnc.estatus);
                element.append(div.append(h4));
                /*
                 if (result.rnc.estatus.search('INHABILITADA') >= 0) {
                 var padre = element.parent(function (c, d) {
                 return c;
                 });
                 padre.html('<br><div class="alert alert-warning" role="alert">Estatus RNC: <h4>' + result.rnc.estatus + "</h4></div>");
                 } else {
                 element.html('<div class="alert alert-info" role="alert">Estatus RNC: <h5>' + result.rnc.estatus + "</h5></div>");
                 }
                 */
            });
        }
    };
}]);