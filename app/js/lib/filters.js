/**
 * Filtro para formato de números
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 13/12/2014
 */
app.filter('formatNumberMoney', [function () {
    return function (numero) {
        return (!isNaN(numero)) ? number_format(numero, 2, ',', '.') : numero;
    }
}]);

/**
 * Filtro para formato de números
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 13/12/2014
 */
app.filter('formatMoneyToNumber', [function () {
    return function (numero) {
        return (!isNaN(numero)) ? number_format(numero, 2, '.', ',') : numero.toString().replace(/\./g, '').replace(',', '.');
    }
}]);

/**
 *
 */
app.filter('listado_key', [function () {
    return function (arreglo, conClave, objeto, objeto_in) {
        var lista = "<ul>";
        if (!angular.isArray(arreglo)) {
            arreglo = angular.fromJson(arreglo);
        }
        angular.forEach(arreglo, function (value, key) {
            var elemento = "<li>" + value + "</li>";
            if (angular.isDefined(conClave) && conClave) {
                if (angular.isDefined(objeto) && objeto != '') {
                    if (angular.isDefined(arreglo[key][objeto])) {
                        if (angular.isDefined(objeto_in) && objeto_in != '') {
                            elemento = "<li>" + arreglo[key][objeto][objeto_in] + "</li>";
                        } else {
                            elemento = "<li>" + arreglo[key][objeto] + "</li>";
                        }

                    }
                }
            }
            lista += elemento;
        });
        lista += "</ul>";
        return lista;
    }
}]);

/**
 *
 */
app.filter('listado_key_publicacion_facturas', [function () {
    return function (arreglo, conClave, objeto, objeto_in) {
        var lista = "<ul>";
        if (!angular.isArray(arreglo)) {
            arreglo = angular.fromJson(arreglo);
        }
        angular.forEach(arreglo, function (value, key) {
            var elemento = "<li>" + value + "</li>";
            if (angular.isDefined(conClave) && conClave) {
                if (angular.isDefined(objeto) && objeto != '') {
                    if (angular.isDefined(arreglo[key][objeto])) {
                        if (angular.isDefined(objeto_in) && objeto_in != '') {
                            elemento = "<li>" + arreglo[key][objeto][objeto_in] + " (<b>Version:</b>" + arreglo[key]['version_nombre'] + ")</li>";
                        } else {
                            elemento = "<li>" + arreglo[key][objeto] + "</li>";
                        }

                    }
                }
            }
            lista += elemento;
        });
        lista += "</ul>";
        return lista;
    }
}]);
/**
 * Filtro para los campos tipo json. Convierte un arreglo en un listado no numerado
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 */
app.filter('listado', [function () {
    return function (arreglo) {
        var lista = "<ul>";
        if (!angular.isArray(arreglo)) {
            arreglo = angular.fromJson(arreglo);
        }
        angular.forEach(arreglo, function (value) {
            lista += "<li>" + value + "</li>";
        });
        lista += "</ul>";
        return lista;
    }
}]);
/**
 * Filtro para los campos tipo json. Convierte un arreglo en un listado no numerado
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 */
app.filter('listadoContMed', [function () {
    return function (arreglo) {
        var lista = "<ul>";
        if (!angular.isArray(arreglo)) {
            arreglo = angular.fromJson(arreglo);
        }
        angular.forEach(arreglo, function (value) {
            lista += "<li>" + value.a + "</li>";
        });
        lista += "</ul>";
        return lista;
    }
}]);

/**
 * Filtro para los campos tipo json. Convierte un arreglo en un listado no numerado
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 */
app.filter('listadoNombreValor', [function () {
    return function (arreglo) {

        var lista = "";
        if (typeof arreglo !== "undefined") {
            if (typeof arreglo == 'string') {
                arreglo = angular.fromJson(arreglo);
            }
            if (arreglo.length > 0 || arreglo != null) {
                angular.forEach(arreglo, function (value) {
                    lista += '<span class="label label-primary">' + value.nombre + "</span><br>";
                });
                lista += "";
                return lista;
            } else {
                lista = "No hay data";
                return lista;
            }
        }
    }
}]);
/**
 * Filtro para los campos tipo json. Convierte una entrada tipo array en un objeto
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 */
app.filter('objcliente', [function () {
    return function (arreglo) {
        var arr = angular.fromJson(arreglo);
        var objeto = [];
        angular.forEach(arr, function (value) {
            objeto.push({nombre: value});
        });
        return objeto;
    }
}]);
/**
 * Filtro para los campos tipo json. Convierte una entrada tipo array en un objeto
 *
 * @author Maykol Purica <puricamaykol@gmail.com>
 */
app.filter('objmedio', [function () {
    return function (arreglo) {
        var arr = angular.fromJson(arreglo);
        var objeto = [];
        angular.forEach(arr, function (value) {
            objeto.push({a: value});
        });
        return objeto;
    }
}]);

/**
 * Convierte una fecha en formato DateTime (yyyy-MM-dd hh:mm:ss) procedente del servidor a formato Javascript
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 27/05/15
 */
app.filter('DatetimeToJsDate', [function () {
    return function (datetime) {
        return new Date(Date.parse(datetime.replace(" ", "T")));
    }
}]);

/**
 * Filtro para agregar porcentaje a los campos
 *
 * @author Carlos Blanco <cebs923@gmail.com>
 * @version V1.0 19/06/15
 */
app.filter('porcentaje', [function () {
    return function (valor) {
        return valor + ' %';
    };
}]);

/**
 * Filtro que convierte los valores de una columna datatables en un link.
 * Para usar, colocar en el objeto del $scope.headerCrud = [{}]
 *     {...,filter:'link',filterParams:{relatedId:'id_tipo',url:'#/TU/URL'}},
 *
 * @author Jose Rodriguez
 * @version V-1.0 28/05/15 10:42:15
 */
app.filter('link', [function () {
    /**
     * Crea un link para datatables con la información suministrada en el
     * parámetro params.
     *
     * @param  {mixed}  data   Dato a ser mostrado en la celda.
     * @param  {Object}  row    Toda la fila de datos a mostrarse en el DT.
     * @param  {Object} params Parámetros definidos la variable headerCrud.filterParams
     * @return {String}        link a otro recurso.
     *
     * @author Jose Rodriguez
     * @version V-1.0 28/05/15 10:42:15
     */
    return function (data, row, params) {
        if (!angular.isDefined(params.url) || !angular.isDefined(params.relatedId)) {
            //console.log("Parece que los parámetros 'URL' o 'relatedId', no estas definidos.");
            return data;
        } else if (params.url.trim().length <= 0 || params.relatedId.trim().length <= 0) {
            //console.log("El parámetro 'URL' o 'relatedId', están vacíos.");
            return data;
        }

        if (!angular.isDefined(row[params.relatedId])) {
            //console.log("El servicio no pose la columna " + params.relatedId);
            return data;
        }

        return "<a href=\"" + params.url + "/" + row[params.relatedId] + "\">" + data + "</a>";
    };
}]);

/**
 * Filtro para ejecutar función custom mediante button
 *
 * @author Frederick Bustamante
 * @version V1.0 13/12/2014
 */
app.filter('custom_button', [function () {
    return function (dato, full_data, params) {
        var icon_class = (angular.isDefined(params.icon_class) ? params.icon_class : 'glyphicon glyphicon-plus');
        var out_param = (angular.isDefined(params.custom_param) ? params.custom_param : dato);
        return '<button class="btn btn-default btn-xs"  ng-click="execute_button(\'' + out_param + '\')" >' +
            '<span class="' + icon_class + '" ></span>' +
            '</button>&nbsp;';
    }
}]);

/**
 * Filtro para agregar porcentaje a los campos
 *
 * @author Carlos Blanco <cebs923@gmail.com>
 * @version V1.0 19/06/15
 */
app.filter('si_no', [function () {
    return function (activo) {
        return (activo) ? 'Si' : 'No';
    }
}]);

/**
 *
 */
app.filter('tipocontrato', [function () {

    return function (tipo) {
        if (tipo == 0) {
            return "Pre-pago";
        } else {
            return "Post-pago";
        }
    }
}]);
/**
 * Filtro moneySaldo le da formato a un numero como dinero y cambia el color a verde si es positivo
 * y a rojo si es negativo
 */
app.filter('moneySaldo', [function () {
    return function (numero) {
        var style = (numero >= 0) ? '#008000' : 'red';
        return '<span style="color:' + style + ';">' + number_format(numero, 2, ',', '.') + '</span>';
    }
}]);

/**
 * Formateo la data de condiciones en: "servicio.condicion.detalle", para ser mostrado en el grid.
 * @version V-1.0
 * @author Jose Rodriguez
 */
app.filter('formatear_condiciones', [function () {
    /**
     * Formateo la data de condiciones en: "servicio.condicion.detalle", para ser mostrado en el grid.
     * @param acuerdos Servicios, Condiciones, Detalles de condiciones existentes en la Negociación
     * @param row    Toda la fila de datos a mostrarse en el DT.
     * @param params Parámetros definidos la variable headerCrud.filterParams
     * @version V-1.0
     * @author Jose Rodriguez
     * @return String
     */
    return function (acuerdos, row, params) {
        var arr = ['<ul type="square">'], acuerdo = {};
        for (var i = acuerdos.length - 1; i >= 0; i--) {
            acuerdo = acuerdos[i];
            var uri = ['Sin información'];
            if (params == 'Detalle') {
                uri = [
                    acuerdo.detalle.condicion.servicio.nombre,
                    acuerdo.detalle.condicion.nombre,
                    acuerdo.detalle.nombre
                ];
            } else if (params == 'Condicion') {
                uri = [acuerdo.condicion.servicio.nombre, acuerdo.condicion.nombre];
            } else {
                // Cuando soy Servicio
                uri = [acuerdo.servicio.nombre];
            }
            arr.push('<li>' + uri.join(' - ') + '</li>');
        }
        arr.push('</ul>');
        return arr.join('');
    }
}]);

/**
 *
 */
app.filter('io_finalizada', [function () {
    return function (status) {
        var style = (status ? 'label-success' : 'label-danger');
        var out_status = (status ? 'SI' : 'NO');
        return '<span class="label ' + style + '">' + out_status + '</span>';
    }
}]);

/**
 *
 */
app.filter('estatus_cotizacion', [function () {
    return function (status) {
        var style = (status ? 'label-success' : 'label-danger');
        var out_status = (status ? 'Aprobada' : 'Por Aprobar');
        return '<span class="label ' + style + '">' + out_status + '</span>';
    }
}]);

/**
 *
 */
app.filter('estatus_publicaciones', [function () {
    return function (status) {
        var style = 'label-danger';
        switch (status) {
            case 'Aprobada':
                style = 'label-default';
                break;
            case 'Parcialmente Publicada':
                style = 'label-info';
                break;
            case 'Publicada':
                style = 'label-success';
                break;
            case 'Con Incidencias':
                style = 'label-warning';
                break;
        }
        return '<span class="label ' + style + '">' + status + '</span>';
    }
}]);

/**
 *
 */
app.filter('listado_key_publicacion_medios', [function () {
    return function (arreglo) {
        var conClave = true;
        var objeto = 'servicio';
        var objeto_in = 'nombre';
        var lista = "<ul>";
        if (!angular.isArray(arreglo)) {
            arreglo = angular.fromJson(arreglo);
        }
        if (arreglo.length == 0) {
            return 'Sin Información';
        }
        angular.forEach(arreglo, function (value, key) {
            var elemento = "<li>" + value + "</li>";
            if (angular.isDefined(conClave) && conClave) {
                if (angular.isDefined(objeto) && objeto != '') {
                    if (angular.isDefined(arreglo[key][objeto])) {
                        if (angular.isDefined(objeto_in) && objeto_in != '') {
                            elemento = "<li>" + arreglo[key][objeto][objeto_in] + " (<b>Version:</b>" + arreglo[key]['version_nombre'] + ")</li>";
                        } else {
                            elemento = "<li>" + arreglo[key][objeto] + "</li>";
                        }

                    }
                }
            }
            lista += elemento;
        });
        lista += "</ul>";
        return lista;
    }
}]);

/**
 * Filtro para colocar el estatus de la versión
 * @author Jose Rodriguez
 */
app.filter('estatus_version', [function () {
    return function (status) {
        var style = (status == 1? 'label-success' : 'label-danger');
        var msg = (status == 1 ? 'Publicada' : 'Por Publicar');
        return '<span class="label ' + style + '">' + msg + '</span>';
    }
}]);

/**
 * Filtro para la forma de pago de los contratos.
 * @author Jose Rodriguez
 */
app.filter('forma_pago', [function () {
    return function (forma) {
        return ((forma) ? 'Post' : 'Pre') + '-Pago';
    }
}]);

/**
 * Formatea las condiciones de las versiones de una publicación
 * @author Jose Rodriguez
 */
app.filter('publicacion_listar_condiciones', [function () {
    return function (condiciones) {
        var lista = ['<ul>'], aplicado_como = '';
        for (var i = 0, len = condiciones.length; i < len; i++) {
            aplicado_como = ' (Recargo) ';
            lista.push('<li>');
            lista.push(condiciones[i].detallecondicion.nombre);

            if (condiciones[i].condicion.es_base == 1) {
                aplicado_como = ' (Base) ';
            }

            lista.push(aplicado_como);
            lista.push('</li>');
        }
        lista.push('</ul>');
        return lista.join('');
    }
}]);

/**
 * Filtra las Publicaciones con incidencias para que no aparezcan al momento de publicar.
 * @author Jose Rodriguez
 */
app.filter('sinIncidencias',[function(){
    return function(versiones){
        var result = [];
        for(var i= 0, len = versiones.length;i<len;i++){
            if(versiones[i].incidencias.length == 0){
                result.push(versiones[i]);
            }
        }
        return result;
    }
}]);

/**
 * Filtro que Formatea las Fechas desde Postgresql a Formato de Usuario.
 * @author Jose Rodriguez
 */
app.filter('datetimeToDate',['$filter',function($filter){
    return function(fecha){
        var format = 'dd/MM/yyyy';
        return $filter('date')(new Date(fecha.split(' ')[0]).getTime(), format);
    }
}]);