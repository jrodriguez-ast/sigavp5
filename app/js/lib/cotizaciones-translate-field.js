(function () {
    angular.module('translate.field', ['pascalprecht.translate'])
        .config(['$translateProvider',
            function ($translateProvider) {
                $translateProvider.
                    translations('es', {

                        title: 'Cotizaciones A.S.T'

                    }).
                    translations('en', {

                        title: 'Quotes A.S.T'
                    });

                $translateProvider.preferredLanguage('en');
            }
        ]);
}).call(this);
