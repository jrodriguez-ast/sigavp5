/**
 * Created by darwin on 07/04/15.
 */
app.factory('requestInterceptor', ['$q', '$rootScope', function ($q, $rootScope) {
    $rootScope.pendingRequests = 0;

    var showLoading = function (show) {
        if (show)
            angular.element('#loading').show();
    };

    var hideLoading = function () {
        angular.element('#loading').hide();
    };

    return {
        'request': function (config) {
            var show_notification = true;
            if ($rootScope.pendingRequests == 0) {
                if (angular.isDefined(config.cargando) && !config.cargando) {
                    show_notification = false;
                }
                showLoading(show_notification);
            }
            if (angular.isDefined(localStorage.user))
                config.headers["username"] = localStorage.user;

            $rootScope.pendingRequests++;

            return config || $q.when(config);
        },

        'requestError': function (rejection) {
            $rootScope.pendingRequests--;

            if ($rootScope.pendingRequests == 0) {
                hideLoading();
            }

            return $q.reject(rejection);
        },

        'response': function (response) {
            $rootScope.pendingRequests--;

            if ($rootScope.pendingRequests == 0) {
                hideLoading();
            }


            return response || $q.when(response);
        },

        'responseError': function (rejection) {
            $rootScope.pendingRequests--;

            if ($rootScope.pendingRequests == 0) {
                hideLoading();
            }

            return $q.reject(rejection);
        }
    }
}]);
