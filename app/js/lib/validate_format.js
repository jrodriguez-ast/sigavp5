/**
 * Created by jeferson on 24/11/14.
 */
var INTEGER_REGEXP = /^\-?\d+$/; // Expresion para validar enteros
var NUMERIC_REGEXP = /[0-9]+(\.[0-9][0-9])$/; // Expresion para validar numeros
var ALFANUMERIC = /[^a-zA-Z0-9]/; // Expresion para validar alfanumérico

/**
 * Montamos la mascara al input, se debe usar de la siguiente forma <input  mask-ast="{mask: '+7(999)999-99-99'} />".
 * en el json que le estamos pasando a la directiva colocamos la forma de la mascara que deseamos colocar.
 * http://igorescobar.github.io/jQuery-Mask-Plugin/
 *
 * @autor Jefferson lara
 * @date  01-12-2014
 * @return null
 */
app.directive('maskAst', [function () {
    return {
        require: 'ngModel',
        restrict: '',
        link: function (scope, elm, attrs, ctrl) {
            $(elm).mask(scope.$eval(attrs.maskAst));
        }
    }
}]);

/**
 * Montamos la mascara al input, se debe usar de la siguiente forma <input  mask-ast="{mask: '+7(999)999-99-99'} />".
 * en el json que le estamos pasando a la directiva colocamos la forma de la mascara que deseamos colocar.
 * http://igorescobar.github.io/jQuery-Mask-Plugin/
 *
 * @autor Jefferson lara
 * @date  01-12-2014
 * @return null
 */
app.directive('maskRif', [function () {
    return {
        require: 'ngModel',
        restrict: '',
        link: function (scope, elm, attrs, ctrl) {
            var mask = 'L-00000000-0';
            var jqElm = $(elm);
            jqElm.mask(mask, {
                'translation': {
                    L: {pattern: /[JGV]/},//Letra
                    0: {pattern: /[0-9]/}//Numeros
                }
            });

            //listen for events on text element
            jqElm.on('keyup paste focus blur', function () {
                // update the ng-model value
                ctrl.$setViewValue($(this).val());
                ctrl.$render();
            });
        }
    }
}]);

/**
 * Montamos la mascara al input, se debe usar de la siguiente forma <input  mask-ast="{mask: '+7(999)999-99-99'} />".
 * en el json que le estamos pasando a la directiva colocamos la forma de la mascara que deseamos colocar.
 * http://igorescobar.github.io/jQuery-Mask-Plugin/
 *
 * @autor Jefferson lara
 * @date  01-12-2014
 * @return null
 */
app.directive('maskCi', [function () {
    return {
        require: 'ngModel',
        restrict: '',
        link: function (scope, elm, attrs, ctrl) {
            var mask = 'L-00000000';
            var jqElm = $(elm);
            jqElm.mask(mask, {
                'translation': {
                    L: {pattern: /[JGV]/},//Letra
                    0: {pattern: /[0-9]/}//Numeros
                }
            });

            //listen for events on text element
            jqElm.on('keyup paste focus blur', function () {
                // update the ng-model value
                ctrl.$setViewValue($(this).val());
                ctrl.$render();
            });
        }
    }
}]);

/**
 * Montamos la mascara al input, se debe usar de la siguiente forma <input  mask-ast="{mask: '+7(999)999-99-99'} />".
 * en el json que le estamos pasando a la directiva colocamos la forma de la mascara que deseamos colocar.
 * http://igorescobar.github.io/jQuery-Mask-Plugin/
 *
 * @autor Jefferson lara
 * @date  01-12-2014
 * @return null
 */
app.directive('maskDni', [function () {
    return {
        require: 'ngModel',
        scope: {
            typedni: '='
        },
        link: function (scope, element, attrs, ngModel) {
            angular.element(element).on('blur', function () {
                var type = scope.typedni;
                var value = angular.element(element).val();
                var partes = value.split('-');
                angular.element(element).val(value.toUpperCase());
                if (type == 'V') {
                    if (partes[1].length < 6)
                        scope.$parent.search.dni = '';
                } else {
                    if (partes[1].length < 9)
                        scope.$parent.search.dni = '';
                }
            });

            scope.$watch('typedni', function (data) {
                if (data == 'V') {
                    $(element).mask('L-00000000', {
                        'translation': {
                            L: {pattern: /[EVev]/},//Letra
                            0: {pattern: /[0-9]/}//Numeros
                        }
                    });
                    $(element).attr('placeholder', 'V/E-________')
                }
                if (data == 'J') {
                    $(element).mask('L-000000000', {
                        'translation': {
                            L: {pattern: /[JGVjgv]/},//Letra
                            0: {pattern: /[0-9]/}//Numeros
                        }
                    });
                    $(element).attr('placeholder', 'J/G/V-_________')
                }
            });
        }
    }
}]);

/**
 * Montamos la mascara para las unidades miles y decimales en un monto ingresado en el input, http://igorescobar.github.io/jQuery-Mask-Plugin/
 *
 * @autor Jefferson lara
 * @date  01-12-2014
 * @return null
 */
app.directive('maskMoney', [function () {
    return {
        require: 'ngModel',
        restrict: '',
        link: function (scope, elm, attrs, ctrl) {
            var mask = '000.000.000.000.000,00';
            var jqElm = $(elm);
            jqElm.mask(mask, {reverse: true});
            //jqElm.mask(mask);

            //listen for events on text element
            jqElm.on('keyup paste focus blur', function () {
                // update the ng-model value
                ctrl.$setViewValue($(this).val());
                ctrl.$render();
            });
        }
    }
}]);

/**
 * Montamos la mascara para las unidades mile y decimales en un monto ingresado en el input, http://igorescobar.github.io/jQuery-Mask-Plugin/
 *
 * @autor Maykol Purica
 * @date  06-06-2015
 * @return null
 */
app.directive('maskMoneyDecimal', [function () {
    return {
        require: 'ngModel',
        restrict: '',
        link: function (scope, elm, attrs, ctrl) {
            var mask = '0000000000,00';
            var jqElm = $(elm);
            jqElm.mask(mask, {reverse: true});

            //listen for events on text element
            jqElm.on('keyup paste focus blur', function () {
                // update the ng-model value
                ctrl.$setViewValue($(this).val());
                ctrl.$render();
            });
        }
    }
}]);

/**
 * Formato para códigos
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.0 13/12/2014
 */
app.directive('maskAstCodigo', [function () {
    return {
        require: 'ngModel',
        restrict: '',
        link: function (scope, elm, attrs, ctrl) {
            $(elm).mask('00000000', {
                'translation': {
                    0: {pattern: /[a-zA-Z0-9]/}
                }
            });
        }
    }
}]);

/**
 * Montamos la mascara al input, se debe usar de la siguiente forma <input  mask-ast="{mask: '+7(999)999-99-99'} />".
 * en el json que le estamos pasando a la directiva colocamos la forma de la mascara que deseamos colocar.
 * http://igorescobar.github.io/jQuery-Mask-Plugin/
 *
 * @autor Jefferson lara
 * @date  01-12-2014
 * @return null
 */
app.directive('maskEmail', [function () {
    return {
        require: 'ngModel',
        restrict: '',
        link: function (scope, elm, attrs, ctrl) {
            var mask = 'L@L.L';
            var jqElm = $(elm);
            jqElm.mask(mask, {
                'translation': {
                    L: {pattern: /\?:[A-Z0-9]/}
                }
            });

            //listen for events on text element
            jqElm.on('keyup paste focus blur', function () {
                // update the ng-model value
                ctrl.$setViewValue($(this).val());
                ctrl.$render();
            });
        }
    }
}]);

/**
 * Directiva para validar los numeros telefónicos
 *
 * @author Carlos Blanco <cebs923@gmail.com>
 * @version V1.0 21/07/2015
 */
app.directive('maskTelefono', [function () {
    return {
        require: 'ngModel',
        restrict: '',
        link: function (scope, elm, attrs, ctrl) {
            var mask = '0000-000-0000';
            var jqElm = $(elm);
            jqElm.mask(mask, {
                'translation': {
                    0: {pattern: /[0-9]/}//Numeros
                }
            });

            //listen for events on text element
            jqElm.on('keyup paste focus blur', function () {
                // update the ng-model value
                ctrl.$setViewValue($(this).val());
                ctrl.$render();
            });
        }
    }
}]);