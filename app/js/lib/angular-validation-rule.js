(function () {
    angular.module('validation.rule', ['validation'])
        .config(['$validationProvider',
            function ($validationProvider) {

                var expression = {
                    required: function (value) {
                        return !!value;
                    },
                    // url: /((([A-Za-z]{3,9}:(?:\/\/)?)(?:[-;:&=\+\$,\w]+@)?[A-Za-z0-9.-]+|(?:www.|[-;:&=\+\$,\w]+@)[A-Za-z0-9.-]+)((?:\/[\+~%\/.\w-_]*)?\??(?:[-\+=&;%@.\w_]*)#?(?:[\w]*))?)/,
                    // email: /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/,
                    number: /^\d+$/,
                    // ip: /^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,

                    numeric: /^[0-9]+$/,
                    integer: /^\-?[0-9]+$/,
                    decimal: /^[0-9]+(\.[0-9]{1,2})?$/,
                    email: /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/,
                    alpha: /^[a-z]+$/i,
                    alphaNumeric: /^[a-z0-9]+$/i,
                    alphaDash: /^[a-z0-9_\-]+$/i,
                    natural: /^[0-9]+$/i,
                    naturalNoZero: /^[1-9][0-9]*$/i,
                    ip: /^((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){3}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})$/i,
                    base64: /[^a-zA-Z0-9\/\+=]/i,

                    //url:           /^((http|https):\/\/(\w+:{0,1}\w*@)?(\S+)|)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/,
                    url: /^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/,

                    alpha_space: /^[a-zÑñÁáÉéÍíÓóÚúäëïöüÿÄËÏÖÜŸ\s]+$/i,
                    alpha_numeric_space: /^[a-z0-9ÑñÁáÉéÍíÓóÚúäëïöüÿÄËÏÖÜŸ,_\.\s\-]+$/i,
                    // Funciones de validación

                    valid_credit_card: function (_value) {
                        var numericDashRegex = /^[\d\-\s]+$/;
                        // Luhn Check Code from https://gist.github.com/4075533
                        // accept only digits, dashes or spaces
                        if (!numericDashRegex.test(_value)) return false;

                        // The Luhn Algorithm. It's so pretty.
                        var nCheck = 0, nDigit = 0, bEven = false;
                        var strippedField = _value.replace(/\D/g, "");

                        for (var n = strippedField.length - 1; n >= 0; n--) {
                            var cDigit = strippedField.charAt(n);
                            nDigit = parseInt(cDigit, 10);
                            if (bEven) {
                                if ((nDigit *= 2) > 9) nDigit -= 9;
                            }

                            nCheck += nDigit;
                            bEven = !bEven;
                        }

                        return (nCheck % 10) === 0;
                    },

                    emails: function (_value) {
                        var result = _value.split(",");
                        for (var i = 0, resultLength = result.length; i < resultLength; i++) {
                            if (!expression.email.test(result[i])) {
                                return false;
                            }
                        }
                        return true;
                    },

                    /**
                     * Valida la cantidad máxima de caracteres permitidos para un campo.
                     * [OJO]    Para utilizar esta validaciones necesario definir el atributo
                     *     'data-max-length="tamaño"' dentro de input de formulario
                     * @param  {String|Integer} _value    Valor actual del Input.
                     * @param  {Object} ignore   Scope de angular. [No utilizado]
                     * @param  {Object} _elem    Referencia la elemento JS.
                     * @return {Boolean}
                     * @version V-1.0 23/01/15 10:06:37
                     * @author Jose Rodriguez
                     * @see Librería Original http://rickharrison.github.io/validate.js/
                     */
                    max_length: function (_value, ignore, _elem) {
                        var len = _elem.attr("data-max-length");
                        if (!expression.numeric.test(len)) {
                            return false;
                        }
                        return (_value.length <= parseInt(len, 10));
                    },

                    /**
                     * Valida la cantidad de mínima caracteres permitidos para un campo.
                     * [OJO]    Para utilizar esta validaciones necesario definir el atributo
                     *     'data-min-length="tamaño"' dentro de input de formulario
                     * @param  {String|Integer} _value    Valor actual del Input.
                     * @param  {Object} ignore   Scope de angular. [No utilizado]
                     * @param  {Object} _elem    Referencia la elemento JS.
                     * @return {Boolean}
                     * @version V-1.0 23/01/15 10:06:37
                     * @author Jose Rodriguez
                     * @see Librería Original http://rickharrison.github.io/validate.js/
                     */
                    min_length: function (_value, ignore, _elem) {
                        var len = _elem.attr("data-min-length");
                        if (!expression.numeric.test(len)) {
                            return false;
                        }
                        return (_value.length >= parseInt(len, 10));
                    },

                    /**
                     * Valida la cantidad exacta de caracteres permitidos para un campo.
                     * [OJO]    Para utilizar esta validaciones necesario definir el atributo
                     *     'data-exact-length="tamaño"' dentro de input de formulario
                     * @param  {String|Integer} _value    Valor actual del Input.
                     * @param  {Object} ignore   Scope de angular. [No utilizado]
                     * @param  {Object} _elem    Referencia la elemento JS.
                     * @return {Boolean}
                     * @version V-1.0 23/01/15 10:06:37
                     * @author Jose Rodriguez
                     * @see Librería Original http://rickharrison.github.io/validate.js/
                     */
                    exact_length: function (_value, ignore, _elem) {
                        var len = _elem.attr("data-exact-length");
                        _value = _value.replace(/\./g, '').replace(',', '.');
                        if (!expression.numeric.test(len)) {
                            return false;
                        }
                        return (_value.length === parseInt(len, 10));
                    },

                    /**
                     * Compara dos numeros y valida que el valor del campo sea mayor al indicado
                     * en el campo comparación, para ello es importante definir el atributo
                     * 'data-greater-than="{{modelo.atributo a comparar}}"' dentro de input de formulario.
                     *
                     * @param  {String|Integer} _value    Valor actual del Input.
                     * @param  {Object} ignore   Scope de angular. [No utilizado]
                     * @param  {Object} _elem    Referencia la elemento JS.
                     * @return {Boolean}
                     * @version V-1.0 23/01/15 10:06:37
                     * @author Jose Rodriguez
                     * @see Librería Original http://rickharrison.github.io/validate.js/
                     */
                    greater_than: function (_value, ignore, _elem) {
                        var minor = _elem.attr("data-greater-than");
                        _value = _value.replace(/\./g, '').replace(',', '.');
                        if (!expression.decimal.test(_value)) {
                            return false;
                        }

                        return (parseFloat(_value) >= parseFloat(minor));
                    },

                    /**
                     * Compara dos numeros y valida que el valor del campo sea menor al indicado
                     * en el campo comparación, para ello es importante definir el atributo
                     * 'data-less-than="{{modelo.atributo a comparar}}"' dentro de input de formulario.
                     *
                     * @param  {String|Integer} _value    Valor actual del Input.
                     * @param  {Object} ignore   Scope de angular. [No utilizado]
                     * @param  {Object} _elem    Referencia la elemento JS.
                     * @return {Boolean}
                     * @version V-1.0 23/01/15 10:06:37
                     * @author Jose Rodriguez
                     * @see Librería Original http://rickharrison.github.io/validate.js/
                     */
                    less_than: function (_value, ignore, _elem) {
                        var mayor = _elem.attr("data-less-than");
                        _value = _value.replace(/\./g, '').replace(',', '.');
                        if (!expression.decimal.test(_value)) {
                            return false;
                        }
                        return (parseFloat(_value) <= parseFloat(mayor));
                    },

                    /**
                     * Compara dos numeros y valida que el valor del campo exactamente igual al indicado
                     * en el campo comparación, para ello es importante definir el atributo
                     * 'data-matches="{{modelo.atributo a comparar}}"' dentro de input de formulario.
                     *
                     * @param  {String|Integer} _value    Valor actual del Input.
                     * @param  {Object} ignore   Scope de angular. [No utilizado]
                     * @param  {Object} _elem    Referencia la elemento JS.
                     * @return {Boolean}
                     * @version V-1.0 23/01/15 10:06:37
                     * @author Jose Rodriguez
                     * @see Librería Original http://rickharrison.github.io/validate.js/
                     */
                    matches: function (_value, ignore, _elem) {
                        var match = _elem.attr("data-matches");
                        return (match && _value === match) ? true : false;
                    },

                    /**
                     * Valida si la extensión de un archivo esta permitida entre la lista definida por
                     * el atributo 'data-file-ext="ext1,ext2.....extn"', dentro de input de formulario.
                     *
                     * @param  {String|Integer} _value    Valor actual del Input.
                     * @param  {Object} ignore   Scope de angular. [No utilizado]
                     * @param  {Object} _elem    Referencia la elemento JS.
                     * @return {Boolean}
                     * @version V-1.0 23/01/15 10:06:37
                     * @author Jose Rodriguez
                     * @see Librería Original http://rickharrison.github.io/validate.js/
                     */
                    is_file_type: function (_value, ignore, _elem) {
                        if (_elem.attr("type") !== 'file') {
                            return true;
                        }

                        var ext = _value.substr((_value.lastIndexOf('.') + 1)),
                            typeArray = _elem.attr("data-file-ext").split(','),
                            inArray = false;

                        for (var i = 0, len = typeArray.length; i < len; i++) {
                            if (ext == typeArray[i]) {
                                inArray = true;
                                break;
                            }
                        }

                        return inArray;
                    },

                    // Validaciones Particulares de Venezuela
                    rif: /^[VEJPG]\d{9}$/i,
                    cedula: /^[VEve]-\d{7,8}$/i,
                    phone: /^\(04(1|2)(4|6)\)\s\d{3}-\d{4}$|^\(0412\)\s\d{3}-\d{4}$|^\(02\d{2}\)\s\d{3}-\d{4}$/i,
                    date: /([0-3][0-9])-([0-9]{1,2})-([1-2][0-9]{3})/


                };

                var defaultMsg = {
                    required: {
                        error: 'Este campo es Requerido!!',
                        success: ''
                    },
                    url: {
                        error: 'Este campo debe contener una URL válida.',
                        success: ''
                    },
                    email: {
                        error: 'Este campo debe contener un email válido.',
                        success: ''
                    },
                    emails: {
                        error: 'Este campo debe contener sólo emails válidos separados por coma(,).',
                        success: ''
                    },
                    number: {
                        error: 'El campo debe contener sólo números.',
                        success: ''
                    },
                    ip: {
                        error: 'Este campo debe contener una ip válida.',
                        success: ''
                    },
                    numeric: {
                        error: 'El campo debe contener sólo números.',
                        success: ''
                    },
                    integer: {
                        error: 'Este campo sólo puede contener números enteros.',
                        success: ''
                    },
                    decimal: {
                        error: 'Este campo debe contener un número decimal. Ejm: 000,00',
                        success: ''
                    },
                    alpha: {
                        error: 'Este campo sólo puede contener caracteres alfabéticos.',
                        success: ''
                    },
                    alphaNumeric: {
                        error: 'Este campo sólo puede contener caracteres alfanuméricos.',
                        success: ''
                    },
                    alphaDash: {
                        error: 'Este campo sólo puede contener caracteres alfanuméricos, guiones bajos "_" o guiones "-".',
                        success: ''
                    },
                    natural: {
                        error: 'Este campo debe contener sólo números positivos.',
                        success: ''
                    },
                    naturalNoZero: {
                        error: 'Este campo debe contener un número mayor que 0.',
                        success: ''
                    },
                    base64: {
                        error: 'Este campo debe contener una cadena base64.',
                        success: ''
                    },

                    rif: {
                        error: 'Este campo debe contener un RIF válido. Ejm: J123456789.',
                        success: ''
                    },
                    cedula: {
                        error: 'Este campo debe contener una cédula válida. Ejm: V/E-12345678.',
                        success: ''
                    },
                    phone: {
                        error: 'Este campo debe contener un teléfono válido. Ejm: 04121233443.',
                        success: ''
                    },
                    date: {
                        error: 'Este campo debe contener una fecha válida. Ejm: 16-12-1985.',
                        success: ''
                    },

                    valid_credit_card: {
                        error: 'Este campo debe contener un número de tarjeta de crédito válido. Ejm: XXXX-XXXX-XXXX-XXXX.',
                        success: ''
                    },

                    max_length: {
                        error: 'Este campo no debe exceder los %s caracteres de longitud.',
                        success: ''
                    },

                    min_length: {
                        error: 'Este campo debe superar los %s caracteres de longitud.',
                        success: ''
                    },

                    exact_length: {
                        error: 'Este campo debe contener exactamente %s caracteres de longitud.',
                        success: ''
                    },

                    greater_than: {
                        error: 'Este campo debe contener un número mayor que %s.',
                        success: ''
                    },

                    less_than: {
                        error: 'Este campo debe contener un número menor que %s.',
                        success: ''
                    },

                    alpha_space: {
                        error: 'Este campo sólo puede contener caracteres alfabéticos y espacios.',
                        success: ''
                    },

                    alpha_numeric_space: {
                        error: 'Este campo sólo puede contener caracteres alfanuméricos espacios.',
                        success: ''
                    },

                    matches: {
                        error: 'Este campo debe ser igual a %s.',
                        success: ''
                    },

                    is_file_type: {
                        error: 'Este campo debe contener sólo extensiones de archivo permitidas.',
                        success: ''
                    }

                };

                $validationProvider.setExpression(expression).setDefaultMsg(defaultMsg);

                $validationProvider.setErrorHTML(function (msg) {
                    return '<span class="has-error">' +
                    '<small class="help-block">' +
                    '<strong>' + msg + '</strong>' +
                    '</small>' +
                    '</span>';
                });

            }
        ]);

}).call(this);