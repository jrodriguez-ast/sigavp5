/**
 *
 * @author Jefferson Lara <jetox21@gmail.com>
 * @package Angular Crud
 * @version V-1.0 28/12/2014
 *
 *
 * <pre>
 *     $scope.siteCrud = {
            url: 'cotizaciones/cotizacion/listar_recursos',
            data: {
                cotizacion_id: $stateParams.cotizacion_id
            }
        };

 $scope.headerCrud = [
 {title: 'Cod.', label: 'codigo', visible: true},
 {title: 'Recurso', label: 'descripcion', visible: true},
 {title: 'Cantidad', label: 'cantidad', visible: true},
 {title: 'Orden', label: 'orden', visible: true}
 ];

 $scope.operation = [
 {
     _name: '',
     url: 'cotizacion_eliminar_recurso',
     visible: 1,
     chk_render_on: 'grid',
     icon: 'glyphicon glyphicon-remove'
 },
 {
     _name: 'Agregar Recursos',
     url: 'cotizacion_recurso',
     visible: 1,
     chk_render_on: 'actionbar',
     icon: 'glyphicon glyphicon-plus'
 }
 ];
 *
 *
 *
 *
 *     $scope.additionalColumns = [
 {
     header: {title: 'Organizar', label: 'organizar', visible: true, sortable: false},
     render: '<button class="btn btn-xs btn-default" ng-click="orderBajaPosicion(:id)"><span class="glyphicon glyphicon-chevron-down"></span></button>' +
     '<button class="btn btn-xs btn-default" ng-click="orderSubePosicion(:id)"><span class="glyphicon glyphicon-chevron-up"></span></button>'
 }
 ];
 *     </pre>
 *
 */
angular.module("angular_crud", ["datatables", "ui.bootstrap", "datatables.bootstrap"])
    .factory('crudFactory', ['$injector', function ($injector) {
        var DTOptionsBuilder = $injector.get('DTOptionsBuilder', 'crudFactory');
        var DTColumnBuilder = $injector.get('DTColumnBuilder', 'crudFactory');
        var DTInstances = $injector.get('DTInstances', 'crudFactory');
        var appConfig = $injector.get('appConfig', 'crudFactory');
        var $filter = $injector.get('$filter', 'crudFactory');

        return ({
            data: {},
            // función que nos permite crear la cabecera del la tabla
            renderColunms: function renderColumns(data, additionalColumns) {
                var col = [];
                angular.forEach(data, function (value) {
                    value.orderable = angular.isDefined(value.orderable) ? value.orderable : true;
                    value.searchable = angular.isDefined(value.searchable) ? value.searchable : true;
                    if (value.visible == undefined || value.visible) {
                        var row_title = DTColumnBuilder
                            .newColumn(value.label)
                            .withTitle(value.title)
                            .withOption('name', value.label)
                            .withOption('orderable', value.orderable)
                            .withOption('searchable', value.searchable)
                            .renderWith(function (data, type, full) {
                                if (data === null || data === "") {
                                    return "Sin Información";
                                }
                                else if (angular.isDefined(value.filter)) {
                                    var _params = (angular.isDefined(value.filterParams)) ? value.filterParams : 0;
                                    if (value.filter.indexOf('date') !== -1) {
                                        data = data.split(' ')[0];
                                        return $filter('date')(new Date(data).getTime(), _params);
                                    }
                                    return $filter(value.filter)(data, full, _params);
                                }
                                else {
                                    return data;
                                }
                            });
                        col.push(row_title);
                    }

                });
                if (additionalColumns) {
                    angular.forEach(additionalColumns, function (value) {
                        if (value.header.visible == undefined || value.header.visible) {
                            var row_title = DTColumnBuilder
                                .newColumn(value.header.label)
                                .withTitle(value.header.title)
                                .renderWith(function (data, type, full) {
                                    var render = value.render;

                                    angular.forEach(full, function (value, i) {
                                        render = render.replace(new RegExp(':' + i, 'g'), value);
                                    });

                                    return render;
                                });

                            if (!value.header.sortable) {
                                row_title.notSortable();
                            }

                            col.push(row_title);
                        }
                    });
                }

                return col;
            },
            // función que se encarga de ir al server y buscar la data que se va a pintar en la tabla
            dataServer: function dataServer(url, data, httpMethod) {
                var option;
                option = DTOptionsBuilder.newOptions()
                    .withOption('ajax', {
                        // Either you specify the AjaxDataProp here
                        //dataSrc: 'data',
                        url: url,
                        type: httpMethod,
                        beforeSend: function (request) {
                            request.setRequestHeader("username", localStorage.user);
                        },
                        data: function (d) {
                            if (data) {
                                angular.forEach(data, function (value, key) {
                                    d[key] = value;
                                });
                            }

                        }
                    })
                    /*.withOption('createdRow', function (row, data, dataIndex) {
                     // Recompiling so we can bind Angular directive to the DT
                     $compile(angular.element(row).contents())($scope);
                     })*/
                    .withDataProp('data')
                    .withOption('serverSide', true)
                    .withPaginationType('full_numbers')
                    .withOption('responsive', true)
                    .withBootstrap()
                    .withLanguage({
                        sUrl: './js/i18n/spanish_datatable.json'
                    });
                //.withOption("order", [[1, 'asc']]);
                return option;
            },
            // función que nos permite agregar las operaciones de la tabla según los permisos del usuario
            renderGrid: function renderGrid(btn) {
                var col = null;
                if (angular.isDefined(btn) && btn.length > 0 && btn !== null) {
                    col = [];
                    col.push(
                        DTColumnBuilder.newColumn(null).withTitle('Opc.').notSortable()
                            .renderWith(function (data) {
                                var operaciones = '';
                                angular.forEach(btn, function (value) {
                                    if (value.visible == 1 && value.chk_render_on == 'grid') {
                                        if (value.chk_visual_type == 'method_js') {
                                            operaciones += '<button ng-click="' + value.url + '(' + data.id + ',' + value.id + ')" class="btn btn-default btn-xs" ><span class="' + value.icon + '" ></span>' + '</button>&nbsp;';
                                        } else {
                                            operaciones += '<a href="#/' + value.url + '/' + data.id + '" class="btn btn-default btn-xs" ><span class="' + value.icon + '" ></span>' + '</a>&nbsp;';
                                        }
                                    }
                                });

                                return operaciones;

                            }).withOption('width', '10%').withOption('searchable', false)
                    );
                }
                return col;
            },

            reloadData: function () {
                DTInstances.getLast().then(function (dtInstance) {
                    dtInstance.reloadData();
                });
            }
        });
    }])
    .directive('crudTemplate', [function () {

        return {
            restrict: 'A',
            //replace: true,
            scope: true,
            template: '<div class="btn-block">' +
            '<div class="header-btn" style="text-align:left; padding: 5px"></div>' +
            '</div>' +
            '<div class="table-responsive">' +
            '<table datatable="" dt-options="dtOptions" dt-columns="dtColumns" class="table table-bordered table-striped"></table>' +
            '</div>',
            link: function (scope, element) {
                var html = ['<p>'];
                angular.forEach(scope.$parent['operation'], function (value) {

                    // armamos las operaciones u opciones tipo action bar en la sobre la tabla
                    if (value.visible == 1 && value.chk_render_on == 'actionbar') {
                        if (value.chk_visual_type == 'method_js') {
                            html.push('<button type="button" ng-click="' + value.url + '()" class="btn btn-primary"><span class="' + value.icon + '" ></span>' + value._name + '</button>&nbsp;');
                        } else {
                            html.push('<a href="#/' + value.url + '"  class="btn btn-primary" >');
                            html.push(' <i class="' + value.icon + '"></i>&nbsp;' + value._name + '</a>&nbsp;&nbsp;')
                        }

                    }

                });
                html.push('</p>');
                angular.element(element).find('div.header-btn').append(html.join(''))
            },
            controller: ['$scope', '$compile', 'crudFactory', "appConfig", function ($scope, $compile, crudFactory, appConfig) {

                var pathApi = $scope.$parent['siteCrud']['pathApi'] || appConfig.getPathApi();
                var url = pathApi + $scope.$parent['siteCrud']['url'];
                var order = angular.isDefined($scope.$parent['siteCrud']['order']) ? $scope.$parent['siteCrud']['order'] : [[1, 'asc']];
                var is_orderable = angular.isDefined($scope.$parent['siteCrud']['is_orderable']) ? $scope.$parent['siteCrud']['is_orderable'] : true;
                var params = angular.isDefined($scope.$parent['siteCrud']['data']) ? $scope.$parent['siteCrud']['data'] : '';
                var httpMethod = angular.isDefined($scope.$parent['siteCrud']['httpMethod']) ? $scope.$parent['siteCrud']['httpMethod'] : 'GET';
                var header = $scope.$parent['headerCrud'];
                var operation = $scope.$parent['operation'];
                var additionalColumns = $scope.$parent['additionalColumns'] || [];
                //Defino el arreglo de columnas
                $scope.dtColumns = crudFactory.renderColunms(header, additionalColumns);

                //vamos al server por la data
                var defaultOrder = (is_orderable) ? order : [];
                $scope.dtOptions = crudFactory.dataServer(url, params, httpMethod).
                    withOption('createdRow', function (row) {
                        // Recompiling so we can bind Angular directive to the DT
                        $compile(angular.element(row).contents())($scope);
                    }).withOption("order", defaultOrder);

                // agregamos las opciones del crud
                var grid = crudFactory.renderGrid(operation);

                angular.forEach(grid, function (value) {
                    $scope.dtColumns.unshift(value);
                });

                // inicializamos el dtoption del controlador padre

                if (angular.isDefined($scope.$parent['dtOptions'])) {
                    $scope.$parent['dtOptions'] = $scope.dtOptions;
                }

                $scope.$parent['reloadData'] = crudFactory.reloadData;

            }]
        }
    }]);

