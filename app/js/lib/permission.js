/**
 * El archivo contiene los métodos que verifican la permisologia y la session de usuario
 *
 * @author Soluciones A.S.T
 * @since Version 1.0
 * @package Cotizaciones A.S.T
 *
 */
app.factory('permission', ['$http', 'appConfig', '$q', function ($http, appConfig, $q) {

    return {
        data: {},

        /**
         * @param {String} module El nombre del modulo al cual se necesita solicitar los permisos correspondiente
         * @returns defer.promise
         *
         * @author Frederick D. Bustamante G. <frederickdanielb@gmail.com>
         * @version 1.2 05/01/2015
         */
        has_permission: function (module) {
            if (module.substr(0, 1) == '/') {
                module = module.replace('/', '');
            }
            var _url = appConfig.getAdminPathApi() + 'auth_cotizaciones/has_permissions?operation=' + module;
            var defer = $q.defer();
            var data = this.data;
            $http.get(_url).
                then(
                function success(response) {
                    data.get = response;

                    defer.resolve(data);
                },
                function error(reason) {
                    return false;
                }
            );
            return defer.promise;
        }

    }
}]);


app.factory('permisosModel', ['$resource', 'appConfig', function ($resource, appConfig) {
    var _url = appConfig.getAdminPathApi() + 'auth_cotizaciones/has_permissions';
    return $resource(_url, null, {
        update: {method: 'PUT'},
        has_permission: {method: 'GET'}
    });
}]);