/**
 *Directiva para la creacion de tabs
 *
 * @author Jefferson Lara <jetox21@gmail.com>
 * @version V-1.0
 */
app.directive("templateTabs", [function () {
    return {
        restrict: 'E',
        templateUrl: "./views/template_custom/tabs.html"
    };
}]);
/**
 *Directiva para el manejo de los mensajes en el sistema
 *
 * @author Jefferson Lara <jetox21@gmail.com>
 * @version V-1.2.0
 *
 */
app.directive("templateMessages", ['$rootScope', '$compile',function ($rootScope, $compile) {
    return {
        replace: true,
        restrict: 'E',
        templateUrl: "views/template_custom/messages.html",
        link: function (scope, el, attrs, ngModel) {
            $rootScope.$watch('type', function (data) {

                if (data) {
                    var element;
                    $(el).find('.content-messages-alert').remove();
                    if (data == 'info')
                        element = angular.element('<div class="content-messages-alert" ><span class="glyphicon glyphicon-info-sign"></span>&nbsp;&nbsp;' + $rootScope.flash_info + '</div>');
                    if (data == 'error')
                        element = angular.element('<div class="content-messages-alert" ><span class="glyphicon glyphicon-warning-sign"></span>&nbsp;&nbsp;' + $rootScope.flash_error + '</div>');
                    if (data == 'success')
                        element = angular.element('<div class="content-messages-alert" ><span class="glyphicon glyphicon-ok-sign"></span>&nbsp;&nbsp;' + $rootScope.flash_success + '</div>');

                    // compilamos el elemnto parseado dentro del $scope
                    var html = $compile(element)(scope);
                    // agregamos el html
                    angular.element('#content-alert').css('z-index', '1000');
                    angular.element('#template-' + data).append(html);
                }
            });
        }
    };
}]);


/*
 *@description
 *
 *@use  <template-modal
 *                modal-size="sm"
 *                modal-template="views/templates/body-modal.html"
 *                modal-controller=""
 *                modal-title="Modal Form3"
 *                modal-button='[{"text":"primero5","class":"btn btn-primary"},{"text":"primero2","class":"btn btn-primary"}]'
 *                >
 *        </template-modal>
 *
 *
 */
app.directive("templateModal", ['$rootScope', '$compile',function ($rootScope, $compile) {
    var base_url = './';
    //var controllerModal='';
    //var bodyTemplate='';
    //var sizeModal='';
    var getVar = function (obj) {

        $rootScope.titleModal = obj.modalTitle;
        $rootScope.buttonModal = $.parseJSON(obj.modalButton);
    };

    return {
        restrict: 'E',
        required: 'ngModel',
        template: "<script type='text/ng-template' id='' ></script>",
        link: function (scope, element, attrs, ngModel) {
            $compile(element.contents())(scope);
            //getVar(attrs);
            //bodyTemplate=attrs.modalTemplate;
            //controllerModal=attrs.modalController;
            //sizeModal=attrs.modalSize;

            //bodyModal = $http.get("assets/frontend-app/views/templates/messages.html", {cache: $templateCache});
            //bodyModal.success(function(html) {
            //    $('.body-modal-include').html(html);
            //})
        },
        controller: ['$scope', '$rootScope', '$modal',function ($scope, $rootScope, $modal) {
            $scope.open = function (view, controller, size) {
                var modalInstance = $modal.open({
                    templateUrl: base_url + view,
                    controller: controller,
                    size: size
                    //resolve: {
                    //    items: function () {
                    //        return template;
                    //    }
                    //}
                });

                $rootScope.cancel = function () {
                    modalInstance.dismiss('cancel');
                };
            };


        }],
        replace: true// con esto le indicamos que remplace el html con el nombre de la directiva
    };

}]);


/**
 * Captura el evento de ng-click y muestra un mensaje de confirmación antes de realizar la acción
 *
 * @author Darwin Serrano <darwinserrano@gmail.com>
 * @version V1.1 12/12/14 10:06 AM
 */
app.directive('confirmationNeeded', [function () {
    return {
        priority: 1,
        terminal: true,
        scope: {
            requireConfirm: '='
        },
        link: function (scope, element, attr) {
            var clickAction = attr.ngClick;
            var msg = attr.confirmationNeeded || "Está seguro de eliminar el/los registro(s) seleccionado(s)?";
            element.bind('click', function () {
                if (typeof scope.requireConfirm === 'undefined' || scope.requireConfirm === true) {
                    bootbox.confirm(msg, function (result) {
                        if (result) {
                            scope.$parent.$eval(clickAction);
                        }
                    });
                }
                else {
                    scope.$parent.$eval(clickAction);
                }
            });
        }
    };
}]);

/**
 * Directva para colocar estilos a los select <http://harvesthq.github.io/chosen/>
 *   ejemplo de uso:
 *          <select class="chosen-select" chosen-select="" dataoptions="object" dataselected="value-selected-default" chosenwidth="width" >
 *               <option value=""></option>
 *           </select>
 *
 * @param dataoptions atributo que se debe igualar al arreglo que contiene la data para los option
 * @param chosenwidth indica el ancho que va a tener el select si no lo colocan pone 100% por defecto
 * @author Jefferson Lara <jetox21@gmail.com>
 * @version V1.2 15/01/15 09:52 AM
 */
app.directive('chosenSelect', [function () {

    return {
        require: 'ngModel',
        scope: {
            dataoptions: '=',
            dataselected: '='
        },
        link: function (scope, element, attrs, ngModel) {

            var width = '100%';

            scope.$watch('dataoptions', function (data) {
                if (data) {

                    $(element)
                        .find('option')
                        .remove();

                    var html = [];
                    html.push('<option value="">');
                    html.push('');
                    html.push('</option>');
                    $.each(data, function (index, object) {
                        if (scope.dataselected == object.id) {
                            html.push('<option selected value="' + object.id + '">');
                        } else {
                            html.push('<option  value="' + object.id + '">');
                        }

                        html.push(object.name);
                        html.push('</option>');
                    });

                    $(element).append(html.join(''));

                    $(element).trigger("chosen:updated");

                }
            });

            if (attrs.chosenwidth)
                width = attrs.chosenwidth;

            $(element).chosen({width: width});
        }
    }
}]);


/**
 *
 */
app.directive('form', [function () {
    return {
        restrict: 'E',
        link: function (scope, element, attrs, ngModel) {
            var form = angular.element(element);
            var requerido = form.find('.span-requerido');
            if (requerido.length > 0) {
                var modal = form.find('div.modal-footer');
                form = (modal.length > 0) ? modal : form;
                form.append('<div class="row help-block">Los campos marcados con asterisco (*) son obligatorios.</div>');
            }
        }
    };
}]);

 app.directive('numbersOnly', function () {
        return {
            restrict: 'A',
            require: 'ngModel',
            link: function (scope, element, attrs, ctrl) {
                var validateNumber = function (inputValue) {
                    var maxLength = 6;
                    if (attrs.max) {
                        maxLength = attrs.max;
                    }
                    if (inputValue === undefined) {
                        return '';
                    }
                    var transformedInput = inputValue.replace(/[^0-9]/g, '');
                    if (transformedInput !== inputValue) {
                        ctrl.$setViewValue(transformedInput);
                        ctrl.$render();
                    }
                    if (transformedInput.length > maxLength) {
                        transformedInput = transformedInput.substring(0, maxLength);
                        ctrl.$setViewValue(transformedInput);
                        ctrl.$render();
                    }
                    var isNotEmpty = (transformedInput.length === 0) ? true : false;
                    ctrl.$setValidity('notEmpty', isNotEmpty);
                    return transformedInput;
                };

                ctrl.$parsers.unshift(validateNumber);
                ctrl.$parsers.push(validateNumber);
                attrs.$observe('notEmpty', function () {
                    validateNumber(ctrl.$viewValue);
                });
            }
        };
    });