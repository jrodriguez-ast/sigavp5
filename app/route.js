'use strict';

/**
 * El archivo contiene las rutas necesarias para el funcionamiento del sistema.
 *
 * @author Soluciones A.S.T
 * @since Version 1.0
 * @package SIGAVP
 */
var app = angular.module(
    "app", [
        "angular_crud",
        "ui.checkbox",
        "ngAnimate",
        "ui.router",
        "ngCookies",
        "ui.bootstrap",
        "validation",
        "validation.rule",
        //"translate.field", //'textAngular',
        "checklist-model",
        "ivh.treeview",
        "ngPasswordStrength",
        "ngResource",
        "angularFileUpload",
        'toggle-switch',
        "ngBootbox",
        'ngSanitize'
    ]).config(["$injector", function ($injector) {
        var $urlRouterProvider = $injector.get('$urlRouterProvider', 'route');
        var CONSTANTES = $injector.get('CONSTANTES', 'route');

        // Captura las peticiones http para mostrar un loading mientras la solicitud esté activa.
        $injector.get('$httpProvider', 'route').interceptors.push('requestInterceptor');

        $injector.get('ivhTreeviewOptionsProvider', 'route').set({
            defaultSelectedState: false,
            validate: false,
            twistieCollapsedTpl: '<span class="glyphicon glyphicon-chevron-right"></span>',
            twistieExpandedTpl: '<span class="glyphicon glyphicon-chevron-down"></span>',
            twistieLeafTpl: '&#9679;'
        });

        $urlRouterProvider.when('/admin/detalle_cliente/:id', '/admin/detalle_cliente/:id/ficha');
        $urlRouterProvider.when('/admin/detalle_medio/:id', '/admin/detalle_medio/:id/ficha');
        $urlRouterProvider.when('/admin/cotizacion_editar_cliente/:cotizacion_id', '/admin/cotizacion_editar/:cotizacion_id/panel');
        $urlRouterProvider.when('/admin/cotizacion_editar/:cotizacion_id', '/admin/cotizacion_editar/:cotizacion_id/panel');
        $urlRouterProvider.when('/admin/publicacion_editar_cliente/:id', '/admin/editar_publicacion/:id');
        // $urlRouterProvider.when('/admin/detalle_cliente/:id/crear_cotizacion', '/admin/cotizacion_crear/:id');
        $urlRouterProvider.otherwise('/admin/inicio');

        $injector.get('$stateProvider', 'route')

            // Muestra el inicio de la aplicación
            .state("admin", {
                url: "/admin",
                templateUrl: "views/layout/main.html?v=" + CONSTANTES.VERSION
            })
            .state("admin.inicio", {
                url: "/inicio",
                templateUrl: "views/welcome.html?v=" + CONSTANTES.VERSION
            })

            // Control de sesión
            .state("login", {
                url: "/login",
                templateUrl: "modules/login/views/login.html?v=" + CONSTANTES.VERSION
            })
            //****************-Rutas Modulo Cliente-**************/
            .state("admin.list_client", {
                url: "/list_client",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "listClient",
                resolve: {
                    rPermisos: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })

            .state("admin.detalle_cliente", {
                url: "/detalle_cliente/:id",
                templateUrl: "modules/clientes/views/clientes/detalles_template.html?v=" + CONSTANTES.VERSION,
                controller: "FichaClienteController",
                resolve: {
                    clientID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    globalPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    clientResolve: ['clienteMdl', 'clientID', function (clienteMdl, clientID) {
                        // Buscar como condicionar esta ejecución
                        return clienteMdl.get({id: clientID}).$promise;
                    }],
                    rLugar: function () {
                        return 'cliente';
                    }
                }
            })

            .state('admin.detalle_cliente.ficha', {
                url: '/ficha',
                templateUrl: 'modules/clientes/views/clientes/detalles_cliente.html?v=' + CONSTANTES.VERSION,
                controller: 'DatosClienteController'
            })
            .state("admin.detalle_cliente.contactos", {
                url: "/contactos",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "listarContactosCliente",
                resolve: {
                    particularPermission: ["permission", function (permission) {
                        var _url = 'admin/detalle_cliente/contactos';
                        return permission.has_permission(_url);
                    }]
                }
            })
            .state("admin.detalle_cliente.contacto_detail", {
                url: "/contacto_detail/:idcont",
                views: {
                    temp: {
                        templateUrl: "modules/medios/views/contactos/detalles.html?v=" + CONSTANTES.VERSION,
                        controller: "DetallesContactoClienteController"
                    }
                },
                resolve: {
                    rTabId: function () {
                        return 1014;
                    },
                    rContacto: ["$stateParams", "ContactoCliente", function ($stateParams, ContactoCliente) {
                        var params = {
                            contactoId: $stateParams.idcont,
                            clienteId: $stateParams.id
                        };
                        return ContactoCliente.get(params).$promise;
                    }]
                }
            })

            .state('admin.detalle_cliente.contacto_add', {
                url: "/contacto_add",
                onEnter: ['$state', '$modal', function ($state, $modal) {
                    $modal.open({
                        animation: true,
                        //backdrop: 'static',
                        templateUrl: 'modules/clientes/views/clientes/contactos/crear.html?v=' + CONSTANTES.VERSION,
                        controller: 'CrearContactoClienteController',
                        size: 'lg',
                        resolve: {
                            datos: function () {
                                return {
                                    'clienteid': $state.params.id
                                };
                            },
                            r_cargos: ["CargoMdl", function (CargoMdl) {
                                return CargoMdl.query();
                            }]
                        }
                    }).result.finally(function () {
                            $state.go('admin.detalle_cliente');
                            //crudFactory.reloadData();
                        })
                }]
            })

            .state("admin.detalle_cliente.contact", {
                url: "/editcontacto/:idcont",
                templateUrl: "modules/clientes/views/clientes/contactos/editar.html?v=" + CONSTANTES.VERSION,
                controller: "EditarContactoClienteController",
                resolve: {
                    resolveTabId: function () {
                        return 12104;
                    }
                }
            })
            .state("admin.detalle_cliente.cotizacion_publicacion", {
                url: "/cotizacion_publicacion/:cotizacion_id",
                views: {
                    temp: {
                        templateUrl: "modules/publicaciones/views/listar_publicaciones_cotizacion.html?v=" + CONSTANTES.VERSION,
                        controller: "PublicacionCotizacionCtrl"
                    }
                },
                resolve: {
                    module_permission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/cotizacion_publicacion';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    cotizacionData: ["Cotizacion", "$stateParams", function (Cotizacion, $stateParams) {
                        return Cotizacion.get({id: $stateParams.cotizacion_id}).$promise;
                    }],
                    resolveTabId: function () {
                        return 1033;
                    }
                }
            })
            .state("admin.detalle_cliente.publicacion_list", {
                url: "/publicacion_list",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "PublicacionCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/publicacion_list';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.crear_cliente", {
                url: "/create_client",
                templateUrl: "modules/clientes/views/clientes/crear.html?v=" + CONSTANTES.VERSION,
                controller: "CrearClienteController",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.editar_cliente", {
                url: "/editar_cliente/:id",
                templateUrl: "modules/clientes/views/clientes/editar.html?v=" + CONSTANTES.VERSION,
                controller: "EditarClienteController",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.detalle_cliente.pagos_list", {
                url: "/pagos_list",
                templateUrl: "modules/clientes/views/clientes/pagos/listado.html?v=" + CONSTANTES.VERSION,
                controller: "PagosCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/pagos_list';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.detalle_cliente.facturas_pagadas", {
                url: "/facturas_pagadas",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "FacturasPagadasCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/facturas_pagadas';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.detalle_cliente.techos", {
                url: "/techos",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "TechosListController",
                resolve: {
                    rPermissions: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/techos';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            /*
             .state("admin.detalle_cliente.techo", {
             url: "/techo",
             onEnter: ['$state', '$modal', "clientResolve", "clientID", function ($state, $modal, clientResolve, clientID) {
             $modal.open({
             animation: true,
             templateUrl: 'modules/clientes/views/techo.html?v=' + CONSTANTES.VERSION,
             controller: 'TechoFormController',
             size: 'lg',
             resolve: {
             rModel: function () {
             return {data: clientResolve, id: clientID};
             }
             }
             });
             }]
             })
             */

            //***************Ruta pago a contrato*************/
            .state("admin.detalle_cliente.abonar_contrato", {
                url: "/abonar_contrato",
                onEnter: ['$state', '$modal', function ($state, $modal) {
                    $modal.open({
                        animation: true,
                        //backdrop: 'static',
                        templateUrl: 'modules/clientes/views/clientes/pagos/abonar_contrato.html?v=' + CONSTANTES.VERSION,
                        controller: 'abonoContratoCtrl',
                        size: 'lg',
                        resolve: {
                            contratoID: function () {
                                return null;
                            },
                            rContrato: ['$stateParams', 'contratoModel', function ($stateParams, contratoModel) {
                                return contratoModel.listar({client: $stateParams.id}).$promise;
                            }],
                            rAction: function () {
                                return 'orden_servicio';
                            },
                            rTitle: function () {
                                return 'Crear contrato';
                            },
                            rClienteID: ['$stateParams', function ($stateParams) {
                                return $stateParams.id;
                            }],
                            rCliente: ['$stateParams', 'clienteMdl', function ($stateParams, clienteMdl) {
                                return clienteMdl.get({id: $stateParams.id}).$promise;
                            }],
                            rTotal: ['$stateParams', 'facturaModel', function ($stateParams, facturaModel) {
                                return facturaModel.getTotal({client: $stateParams.id}).$promise;
                            }],
                            particularPermission: ["permisosModel", function (permisosModel) {
                                var _url = 'admin/detalle_cliente/abonar_contrato';
                                return permisosModel.has_permission({operation: _url}).$promise;
                            }]
                        }
                    }).result.finally(function () {
                            $state.go('admin.detalle_cliente');
                            //crudFactory.reloadData();
                        });
                }]
            })

            //****************-Rutas Modulo de Medios-**************/
            .state("admin.listar_medios", {
                url: "/listar_medios",
                templateUrl: "modules/medios/views/medios/listado.html?v=" + CONSTANTES.VERSION,
                controller: "listarMedios",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.detalle_medio", {
                url: "/detalle_medio/:id",
                templateUrl: "modules/clientes/views/clientes/detalles_template.html?v=" + CONSTANTES.VERSION,
                controller: "FichaClienteController",
                resolve: {
                    clientID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    globalPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_medio';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    clientResolve: ['Medio', 'clientID', function (Medio, clientID) {
                        // Buscar como condicionar esta ejecución
                        return Medio.get({id: clientID}).$promise;
                    }],
                    rLugar: function () {
                        return 'medio';
                    }
                }
            })
            .state("admin.detalle_medio.ficha", {
                url: "/ficha",
                templateUrl: "modules/medios/views/medios/detalles_medio.html?v=" + CONSTANTES.VERSION,
                controller: "DatosClienteController"
            })
            // Medios Servicios
            .state("admin.detalle_medio.servicios", {
                url: "/servicios",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "ServiciosListController",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_medio/servicios';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            .state("admin.detalle_medio.servicio", {
                url: "/servicio/:serviceID",
                views: {
                    temp: {
                        templateUrl: "modules/servicios/views/form.html?v=" + CONSTANTES.VERSION,
                        controller: "ServiciosTabController"
                    }
                },
                resolve: {
                    serviceID: ['$stateParams', function ($stateParams) {
                        return $stateParams.serviceID;
                    }],
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'newTabServices';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    resolveServices: ['Servicio', '$stateParams', function (Servicio, $stateParams) {
                        var ser = {
                            medioId: $stateParams.id,
                            serviceId: $stateParams.serviceID
                        };
                        return Servicio.get(ser).$promise;
                    }],
                    TipoPublicacionResolve: ['TipoPublicacion', function (TipoPublicacion) {
                        return TipoPublicacion.query().$promise;
                    }],
                    rListChildren: ['Servicio', '$stateParams', function (Servicio, $stateParams) {
                        return Servicio.listChildren({parentId: $stateParams.serviceID}).$promise.then(function (resp) {
                            for (var i = 0; i < resp.data.length; i++) {
                                resp.data[i].medio = {
                                    id: resp.data[i].id,
                                    nombre: resp.data[i].nombre,
                                    descripcion: resp.data[i].descripcion
                                };
                            }
                            return resp;
                        });
                    }],
                    rViewMode: function () {
                        return false;
                    },
                    // Tab Id para cuando se refresca la página en servicios
                    resolveTabId: function () {
                        return 12152;
                    },
                    rChildren: ['Servicio', '$stateParams', function (Servicio, $stateParams) {
                        var params = {
                            parentId: $stateParams.serviceID
                        };
                        return Servicio.children(params).$promise.then(function (resultado) {
                            var children = resultado.data;
                            var _childrenArray = [];
                            for (var i = 0, len = children.length; i < len; i++) {
                                _childrenArray[i] = {
                                    selectTipo: {id: children[i].tipo_publicacion_id},
                                    tipo_publicacion_id: children[i].tipo_publicacion_id,
                                    nombre: children[i].nombre,
                                    descripcion: children[i].descripcion,
                                    servicios_hijo_id: children[i].servicios_hijo_id
                                };
                            }
                            return _childrenArray;
                        });
                    }]
                }
            })

            .state("admin.detalle_medio.servicio_view", {
                url: "/servicio_view/:serviceID",
                views: {
                    temp: {
                        templateUrl: "modules/servicios/views/form.html?v=" + CONSTANTES.VERSION,
                        controller: "ServiciosTabController"
                    }
                },
                resolve: {
                    serviceID: ['$stateParams', function ($stateParams) {
                        return $stateParams.serviceID;
                    }],
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'newTabServices';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    resolveServices: ['Servicio', '$stateParams', function (Servicio, $stateParams) {
                        var ser = {
                            medioId: $stateParams.id,
                            serviceId: $stateParams.serviceID
                        };
                        return Servicio.get(ser).$promise;
                    }],
                    TipoPublicacionResolve: ['TipoPublicacion', function (TipoPublicacion) {
                        return TipoPublicacion.query().$promise;
                    }],
                    rViewMode: function () {
                        return true;
                    },
                    // Tab Id para cuando se refresca la página en servicios
                    resolveTabId: function () {
                        return 12153;
                    },
                    rListChildren: function () {
                        return null;
                    },
                    rChildren: ['Servicio', '$stateParams', function (Servicio, $stateParams) {
                        var params = {
                            parentId: $stateParams.serviceID
                        };
                        return Servicio.children(params).$promise.then(function (resultado) {
                            var children = resultado.data;
                            var _childrenArray = [];
                            for (var i = 0, len = children.length; i < len; i++) {
                                _childrenArray[i] = {
                                    selectTipo: {id: children[i].tipo_publicacion_id},
                                    tipo_publicacion_id: children[i].tipo_publicacion_id,
                                    nombre: children[i].nombre,
                                    descripcion: children[i].descripcion,
                                    servicios_hijo_id: children[i].servicios_hijo_id
                                };
                            }
                            return _childrenArray;
                        });
                    }]
                }
            })

            .state("admin.detalle_medio.servicio_add", {
                url: "/servicio_add",
                onEnter: ['$state', '$modal', 'clientID', function ($state, $modal, clientID) {
                    $modal.open({
                        templateUrl: "modules/servicios/views/modal.html?v=" + CONSTANTES.VERSION,
                        controller: "ServiciosModalController",
                        size: 'lg',
                        resolve: {
                            rTipoPublicacion: ['TipoPublicacion', function (TipoPublicacion) {
                                return TipoPublicacion.query().$promise;
                            }],
                            rMedio: function () {
                                return clientID;
                            },
                            r_circulacion: ["circulacionModel", function (circulacionModel) {
                                return circulacionModel.get().$promise;
                            }],
                            r_target: ["targetModel", function (targetModel) {
                                return targetModel.get().$promise;
                            }],
                            r_redSocial: ["redSocialModel", function (redSocialModel) {
                                return redSocialModel.get().$promise;
                            }]
                        }
                    }).result.finally(function () {
                            $state.go('admin.detalle_medio');
                            //crudFactory.reloadData();
                        })
                }]
            })
            .state('admin.detalle_medio.servicio.condicion_add', {
                url: "/condicion_add",
                onEnter: ['$state', '$modal', 'resolveServices', function ($state, $modal, resolveServices) {
                    $modal.open({
                        templateUrl: "modules/conditions/views/form.html?v=" + CONSTANTES.VERSION,
                        controller: "CondicionesModalController",
                        size: 'lg',
                        resolve: {
                            rService: function () {
                                return resolveServices;
                            },
                            rViewMode: function () {
                                return false;
                            },
                            rCondition: function () {
                                return {data: {}};
                            },
                            rAction: function () {
                                return 'new';
                            },
                            rhasBaseCondition: ['Servicio', '$stateParams', function (Servicio, $stateParams) {
                                return Servicio.hasbasecondition({serviceId: $stateParams.serviceID}).$promise.then(function (respuesta) {
                                    return respuesta.hasbase;
                                });
                            }],
                            rMsj: ['globalData', function (globalData) {
                                return globalData.msjNuevoServicio;
                            }]
                        }
                    }).result.finally(function () {
                            $state.go('^', {}, {reload: true});
                        });
                }]
            })

            .state('admin.detalle_medio.servicio.condicion_update', {
                url: "/condicion_update/:CondicionId",
                resolve: {
                    rCondicionId: ['$stateParams', function ($stateParams) {
                        return $stateParams.CondicionId;
                    }]
                },
                onEnter: ['$state', '$modal', 'resolveServices', 'rCondicionId', function ($state, $modal, resolveServices, rCondicionId) {
                    $modal.open({
                        templateUrl: "modules/conditions/views/form.html?v=" + CONSTANTES.VERSION,
                        controller: "CondicionesModalController",
                        size: 'xl',
                        resolve: {
                            rService: function () {
                                return resolveServices;
                            },
                            rViewMode: function () {
                                return false;
                            },
                            rAction: function () {
                                return 'update';
                            },
                            rCondition: ['$stateParams', 'CondicionModel', function ($stateParams, CondicionModel) {
                                var params = {
                                    medioId: $stateParams.id,
                                    servicioId: $stateParams.serviceID,
                                    id: rCondicionId
                                };
                                return CondicionModel.get(params).$promise;
                            }],
                            rhasBaseCondition: function () {
                                return null;
                            },
                            rMsj: function () {
                                return '';
                            }
                        }
                    }).result.finally(function () {
                            $state.go('^', {}, {reload: true});
                        });
                }]
            })

            .state('admin.detalle_medio.servicio.condicion_view', {
                url: "/condicion_view/:CondicionId",
                resolve: {
                    rCondicionId: ['$stateParams', function ($stateParams) {
                        return $stateParams.CondicionId;
                    }]
                },
                onEnter: ['$state', '$modal', 'resolveServices', 'rCondicionId', function ($state, $modal, resolveServices, rCondicionId) {
                    $modal.open({
                        templateUrl: "modules/conditions/views/form.html?v=" + CONSTANTES.VERSION,
                        controller: "CondicionesModalController",
                        size: 'lg',
                        resolve: {
                            rService: function () {
                                return resolveServices;
                            },
                            rViewMode: function () {
                                return true;
                            },
                            rAction: function () {
                                return 'view'
                            },
                            rhasBaseCondition: function () {
                                return null
                            },
                            rMsj: function () {
                                return '';
                            },
                            rCondition: ['$stateParams', 'CondicionModel', function ($stateParams, CondicionModel) {
                                var params = {
                                    medioId: $stateParams.id,
                                    servicioId: $stateParams.serviceID,
                                    id: rCondicionId
                                };
                                return CondicionModel.get(params).$promise;
                            }]
                        }
                    }).result.finally(function () {
                            $state.go('^');
                        });
                }]
            })

            .state('admin.detalle_medio.servicio_view.condicion_view', {
                url: "/condicion_view/:CondicionId",
                resolve: {
                    rCondicionId: ['$stateParams', function ($stateParams) {
                        return $stateParams.CondicionId;
                    }]
                },
                onEnter: ['$state', '$modal', 'resolveServices', 'rCondicionId', function ($state, $modal, resolveServices, rCondicionId) {
                    $modal.open({
                        templateUrl: "modules/conditions/views/form.html?v=" + CONSTANTES.VERSION,
                        controller: "CondicionesModalController",
                        size: 'lg',
                        resolve: {
                            rService: function () {
                                return resolveServices;
                            },
                            rViewMode: function () {
                                return true;
                            },
                            rAction: function () {
                                return 'view';
                            },
                            rhasBaseCondition: function () {
                                return null;
                            },
                            rMsj: function () {
                                return '';
                            },
                            rCondition: ['$stateParams', 'CondicionModel', function ($stateParams, CondicionModel) {
                                var params = {
                                    medioId: $stateParams.id,
                                    servicioId: $stateParams.serviceID,
                                    id: rCondicionId
                                };
                                return CondicionModel.get(params).$promise;
                            }]
                        }
                    }).result.finally(function () {
                            $state.go('^');
                        });
                }]
            })

            .state('admin.detalle_medio.tarifario', {
                url: '/tarifario/:service_id',
                views: {
                    temp: {
                        controller: 'TarifarioController',
                        templateUrl: "modules/servicios/views/tarifario.html?v=" + CONSTANTES.VERSION
                    }
                },
                resolve: {
                    rServicio: ['Servicio', '$stateParams', function (Servicio, $stateParams) {
                        var ser = {
                            medioId: $stateParams.id,
                            serviceId: $stateParams.service_id
                        };
                        return Servicio.get(ser).$promise;
                    }],
                    rTipoPublicacion: ['TipoPublicacion', function (TipoPublicacion) {
                        return TipoPublicacion.query().$promise;
                    }],
                    rViewMode: function () {
                        return true;
                    },
                    // Tab Id para cuando se refresca la página en servicios
                    rTabId: function () {
                        return 12164;
                    }
                }
            })

            //************************* Rutas del modulo de Contratos***************************/
            .state("admin.detalle_cliente.contratos", {
                url: "/contratos",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "listarContratosPorCliente",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/contratos';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.detalle_cliente.detalle_contrato", {
                url: "/detalle_contrato/:contratoID",
                views: {
                    temp: {
                        templateUrl: "modules/contratos/views/form.html?v=" + CONSTANTES.VERSION,
                        controller: "ContratoFormController"
                    }
                },
                resolve: {
                    contratoID: ['$stateParams', function ($stateParams) {
                        return $stateParams.contratoID;
                    }],
                    rContrato: ['$stateParams', 'contratoModel', function ($stateParams, contratoModel) {
                        var params = {id: $stateParams.contratoID};
                        return contratoModel.get(params).$promise;
                    }],
                    rAction: function () {
                        return 'view';
                    },
                    $modalInstance: function () {
                        return null;
                    },
                    rTitle: function () {
                        return 'Detalles del Contrato';
                    },
                    rClienteID: function () {
                        return null;
                    },
                    rCliente: function () {
                        return null;
                    },
                    r_tipo_contrato: ["contratoModel", function (contratoModel) {
                        return contratoModel.tiposContratos().$promise;
                    }],
                    /*
                     r_regimen: ["regimenModel", function (regimenModel) {
                     return regimenModel.get().$promise;
                     }],
                     */
                    r_list_tipo_contrato: ["contratoModel", "$stateParams", function (contratoModel, $stateParams) {
                        return contratoModel.listTipoContrato({client: $stateParams.id}).$promise;
                    }],
                    rTabId: ['$stateParams', function ($stateParams) {
                        return angular.isUndefined($stateParams.tabId) ? 1020 : $stateParams.tabId;
                    }],
                    TipoPublicacionResolve: ['TipoPublicacion', function (TipoPublicacion) {
                        return TipoPublicacion.query().$promise.then(function (response) {
                            return response;
                        });
                        //return null;
                    }]
                }
            })
            .state("admin.detalle_cliente.editar_contrato", {
                url: "/editar_contrato/:contratoID",
                views: {
                    temp: {
                        templateUrl: "modules/contratos/views/form.html?v=" + CONSTANTES.VERSION,
                        controller: "ContratoFormController"
                    }
                },
                resolve: {
                    contratoID: ['$stateParams', function ($stateParams) {
                        return $stateParams.contratoID;
                    }],
                    rContrato: ['$stateParams', 'contratoModel', function ($stateParams, contratoModel) {
                        var params = {id: $stateParams.contratoID};
                        return contratoModel.get(params).$promise;
                    }],
                    $modalInstance: function () {
                        return null;
                    },
                    rAction: function () {
                        return 'update';
                    },
                    rTitle: function () {
                        return 'Actualizar Contrato';
                    },
                    rClienteID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    rCliente: function () {
                        return null;
                    },
                    /*
                     r_regimen: ["regimenModel", function (regimenModel) {
                     return regimenModel.get().$promise;
                     }],
                     */
                    r_list_tipo_contrato: ["contratoModel", "$stateParams", function (contratoModel, $stateParams) {
                        return contratoModel.listTipoContrato({client: $stateParams.id}).$promise;
                    }],
                    r_tipo_contrato: ["contratoModel", function (contratoModel) {
                        return contratoModel.tiposContratos().$promise;
                    }],
                    rTabId: ['$stateParams', function ($stateParams) {
                        return angular.isUndefined($stateParams.tabId) ? 1021 : $stateParams.tabId;
                    }],
                    TipoPublicacionResolve: ['TipoPublicacion', function (TipoPublicacion) {
                        return TipoPublicacion.query().$promise.then(function (response) {
                            return response;
                        });
                        //return null;
                    }]
                }
            })

            .state("admin.detalle_cliente.balance_contrato", {
                url: "/balance_contrato/:contratoID",
                views: {
                    temp: {
                        templateUrl: "modules/clientes/views/clientes/balance/balance.html?v=" + CONSTANTES.VERSION,
                        controller: "balanceListController"
                    }
                },
                resolve: {
                    rTabId: function () {
                        return 15001;
                    },
                    rContratoID: ['$stateParams', function ($stateParams) {
                        return $stateParams.contratoID;
                    }],
                    rSaldo: ['clienteMdl', '$stateParams', function (clienteMdl, $stateParams) {
                        var params = {id: $stateParams.id, contratoID: 0};
                        if (angular.isDefined($stateParams.contratoID)) {
                            params.contratoID = $stateParams.contratoID;
                        }
                        return clienteMdl.saldo(params).$promise;
                    }],
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/balance';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            .state("admin.detalle_cliente.balance_x_tipo", {
                url: "/balance_x_tipo/:contratoID",
                views: {
                    temp: {
                        templateUrl: "modules/clientes/views/clientes/balance/balance_x_tipo.html?v=" + CONSTANTES.VERSION,
                        controller: "balancexTipoListController"
                    }
                },
                resolve: {
                    rTabId: function () {
                        return 15001;
                    },
                    rContratoID: ['$stateParams', function ($stateParams) {
                        return $stateParams.contratoID;
                    }],
                    rSaldo: ['clienteMdl', '$stateParams', function (clienteMdl, $stateParams) {
                        var params = {id: $stateParams.id, contratoID: 0};
                        if (angular.isDefined($stateParams.contratoID)) {
                            params.contratoID = $stateParams.contratoID;
                        }
                        return clienteMdl.saldoxtipo(params).$promise;
                    }],
                    particularPermission: function () {
                        return [];
                    }
                }
            })

            .state("admin.detalle_cliente.crear_contrato", {
                url: "/crear_contrato",
                onEnter: ['$state', '$modal', function ($state, $modal) {
                    $modal.open({
                        animation: true,
                        //backdrop: 'static',
                        templateUrl: 'modules/contratos/views/form.html?v=' + CONSTANTES.VERSION,
                        controller: 'ContratoFormController',
                        size: 'xl',
                        resolve: {
                            contratoID: function () {
                                return null;
                            },
                            rContrato: function () {
                                return null;
                            },
                            rAction: function () {
                                return 'store';
                            },
                            rTitle: function () {
                                return 'Crear contrato';
                            },
                            rClienteID: ['$stateParams', function ($stateParams) {
                                return $stateParams.id;
                            }],
                            rCliente: ['$stateParams', 'clienteMdl', function ($stateParams, clienteMdl) {
                                return clienteMdl.get({id: $stateParams.id}).$promise;
                            }],
                            /*
                             r_regimen: ["regimenModel", function (regimenModel) {
                             return regimenModel.get().$promise;
                             }],
                             */
                            r_tipo_contrato: ["contratoModel", function (contratoModel) {
                                return contratoModel.tiposContratos().$promise;
                            }],
                            r_list_tipo_contrato: ["contratoModel", "$stateParams", function (contratoModel, $stateParams) {
                                return contratoModel.listTipoContrato({client: $stateParams.id}).$promise;
                            }],
                            TipoPublicacionResolve: ['TipoPublicacion', function (TipoPublicacion) {
                                return TipoPublicacion.query().$promise.then(function (response) {
                                    return response;
                                });
                                //return null;
                            }],
                            rTabId: function () {
                                return null;
                            }
                        }
                    }).result.finally(function () {
                            $state.go('admin.detalle_cliente');
                            //crudFactory.reloadData();
                        });
                }]
            })
            // Contrato desde fuera.
            .state("admin.listar_contratos", {
                url: "/listar_contratos",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "listarContratos",
                resolve: {
                    rPermisos: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.detalle_contrato", {
                url: "/detalle_contrato/:contratoID",
                templateUrl: "modules/contratos/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "ContratoFormController",
                resolve: {
                    contratoID: ['$stateParams', function ($stateParams) {
                        return $stateParams.contratoID;
                    }],
                    rContrato: ['$stateParams', 'contratoModel', function ($stateParams, contratoModel) {
                        var params = {id: $stateParams.contratoID};
                        return contratoModel.get(params).$promise;
                    }],
                    rAction: function () {
                        return 'view';
                    },
                    $modalInstance: function () {
                        return null;
                    },
                    rTitle: function () {
                        return 'Detalles del Contrato';
                    },
                    rClienteID: function () {
                        return null;
                    },
                    rCliente: function () {
                        return null;
                    },
                    r_tipo_contrato: ["contratoModel", function (contratoModel) {
                        return contratoModel.tiposContratos().$promise;
                    }],
                    /*
                     r_regimen: ["regimenModel", function (regimenModel) {
                     return regimenModel.get().$promise;
                     }],
                     */
                    r_list_tipo_contrato: function () {
                        return false;
                    },
                    rTabId: function () {
                        return null;
                    },
                    TipoPublicacionResolve: ['TipoPublicacion', function (TipoPublicacion) {
                        return TipoPublicacion.query().$promise.then(function (response) {
                            return response;
                        });
                    }]
                }
            })
        /************************* Rutas del modulo de Balances ***************************
         * @author Maykol Purica
         **********************************************************************************/
            .state("admin.detalle_cliente.balance", {
                url: "/balance",
                templateUrl: "modules/clientes/views/clientes/balance/balance.html?v=" + CONSTANTES.VERSION,
                controller: "balanceListController",
                resolve: {
                    rSaldo: ['clienteMdl', '$stateParams', function (clienteMdl, $stateParams) {
                        return clienteMdl.saldo({id: $stateParams.id}).$promise;
                    }],
                    rContratoID: function () {
                        return 0;
                    },
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/balance';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    rTabId: function () {
                        return 1018;
                    }
                }
            })
            .state("admin.detalle_cliente.detalles_balance", {
                url: "/detalles_balance/:balanceID",
                views: {
                    temp: {
                        templateUrl: "modules/clientes/views/clientes/balance/detalle_balance.html?v=" + CONSTANTES.VERSION,
                        controller: "balanceDetalleController"
                    }
                },
                resolve: {
                    balanceID: ['$stateParams', function ($stateParams) {
                        return $stateParams.balanceID;
                    }],
                    rBalance: ['$stateParams', 'balanceModel', function ($stateParams, balanceModel) {
                        return balanceModel.detalleBalance({balanceID: $stateParams.balanceID}).$promise;
                    }],
                    rAction: function () {
                        return 'view';
                    },
                    // Tab Id para cuando se refresca la página en servicios
                    rTabId: function () {
                        return 1022;
                    }
                }
            })
            .state("admin.detalle_cliente.addobservacion_balance", {
                url: "/addobservacion_balance/:balanceID",
                views: {
                    temp: {
                        templateUrl: "modules/clientes/views/clientes/balance/observacion.html?v=" + CONSTANTES.VERSION,
                        controller: "balanceDetalleController"
                    }
                },
                resolve: {
                    balanceID: ['$stateParams', function ($stateParams) {
                        return $stateParams.balanceID;
                    }],
                    rBalance: ['$stateParams', 'balanceModel', function ($stateParams, balanceModel) {
                        return balanceModel.detalleBalance({balanceID: $stateParams.balanceID}).$promise;
                    }],
                    rAction: function () {
                        return 'update';
                    },
                    // Tab Id para cuando se refresca la página en servicios
                    rTabId: function () {
                        return 1023;
                    }
                }
            })
        /****************** Rutas del modulo de Balances de AVP con el medio***************************
         * @author Maykol Purica
         **********************************************************************************/
            .state("admin.detalle_medio.balance", {
                url: "/balance",
                templateUrl: "modules/medios/views/medios/balance/balance.html?v=" + CONSTANTES.VERSION,
                controller: "balanceMedioListController",
                resolve: {
                    rSaldo: ['Medio', '$stateParams', function (Medio, $stateParams) {
                        return Medio.saldo({id: $stateParams.id}).$promise;
                    }],
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_medio/balance';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.detalle_medio.detalles_balance", {
                url: "/detalles_balance/:balanceID",
                views: {
                    temp: {
                        templateUrl: "modules/medios/views/medios/balance/detalle_balance.html?v=" + CONSTANTES.VERSION,
                        controller: "balanceMedioDetalleController"
                    }
                },
                resolve: {
                    balanceID: ['$stateParams', function ($stateParams) {
                        return $stateParams.balanceID;
                    }],
                    rBalance: ['$stateParams', 'balanceMedioModel', function ($stateParams, balanceMedioModel) {
                        return balanceMedioModel.detalleBalance({balanceID: $stateParams.balanceID}).$promise;
                    }],
                    rAction: function () {
                        return 'view';
                    },
                    // Tab Id para cuando se refresca la página en servicios
                    resolveTabId: function () {
                        return 12154;
                    }
                }
            })
            .state("admin.detalle_medio.addobservacion_balance", {
                url: "/addobservacion_balance/:balanceID",
                views: {
                    temp: {
                        templateUrl: "modules/medios/views/medios/balance/observacion.html?v=" + CONSTANTES.VERSION,
                        controller: "balanceMedioDetalleController"
                    }
                },
                resolve: {
                    balanceID: ['$stateParams', function ($stateParams) {
                        return $stateParams.balanceID;
                    }],
                    rBalance: ['$stateParams', 'balanceMedioModel', function ($stateParams, balanceMedioModel) {
                        return balanceMedioModel.detalleBalance({balanceID: $stateParams.balanceID}).$promise;
                    }],
                    rAction: function () {
                        return 'update';
                    },
                    // Tab Id para cuando se refresca la página en servicios
                    resolveTabId: function () {
                        return 12154;
                    }
                }
            })
        /************************* Rutas de Facturación ***************************
         * @author Maykol Purica
         **********************************************************************************/
            .state("admin.detalle_cliente.facturar", {
                url: "/facturar",
                templateUrl: "modules/facturas/views/listado.html?v=" + CONSTANTES.VERSION,
                controller: "listarPublicacionesNoFacturadas",
                resolve: {
                    rPermisos: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }],
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/facturar';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    rClienteID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    rTotal: ['$stateParams', 'facturaModel', function ($stateParams, facturaModel) {
                        return facturaModel.getTotal({client: $stateParams.id}).$promise;
                    }]
                }
            })
            .state("admin.detalle_cliente.facturas_list", {
                url: "/facturas_list",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "listarFacturas",
                resolve: {
                    rPermisos: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/facturas_list';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    rClienteID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }]
                }
            })
            .state("admin.facturas_list_todas", {
                url: "/facturas_list_todas",
                templateUrl: "views/template_custom/template_crud.html",
                controller: "listarFacturasTodas",
                resolve: {
                    rPermisos: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/facturas_list_todas';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]

                }
            })
            .state("admin.detalle_cliente.facturas_detail", {
                url: "/facturas_detail/:facturaID",
                views: {
                    temp: {
                        templateUrl: "modules/facturas/views/detalles.html?v=" + CONSTANTES.VERSION,
                        controller: "detalleFacturaController"
                    }
                },
                resolve: {
                    rClienteID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    rFacturaID: ['$stateParams', function ($stateParams) {
                        return $stateParams.facturaID;
                    }],
                    rFactura: ['$stateParams', 'facturaModel', function ($stateParams, facturaModel) {
                        return facturaModel.showFactura({
                            clienteID: $stateParams.id,
                            facturaID: $stateParams.facturaID
                        }).$promise;
                    }],
                    rOperation: function () {
                        return 1027;
                    },
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'detalleFactura';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.facturas_detail", {
                url: "/facturas_detail/:facturaID",
                views: {
                    temp: {
                        templateUrl: "modules/facturas/views/detalles.html",
                        controller: "detalleFacturaController"
                    }
                },
                resolve: {
                    rClienteID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    rFacturaID: ['$stateParams', function ($stateParams) {
                        return $stateParams.facturaID;
                    }],
                    rFactura: ['$stateParams', 'facturaModel', function ($stateParams, facturaModel) {
                        return facturaModel.showFactura({
                            clienteID: $stateParams.id,
                            facturaID: $stateParams.facturaID
                        }).$promise;
                    }],

                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'detalleFacturaTodas';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.detalle_cliente.notas_credito", {
                url: "/notas_credito",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "listarNotasdeCredito",
                resolve: {
                    rPermisos: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/notas_credito';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    rClienteID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    rNotaID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }]
                }
            })
            .state("admin.detalle_cliente.notas_detail", {
                url: "/notas_detail/:notaID",
                views: {
                    temp: {
                        templateUrl: "modules/facturas/views/detalles_nota.html",
                        controller: "detalleNotaController"
                    }
                },
                resolve: {
                    rClienteID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    rNotaID: ['$stateParams', function ($stateParams) {
                        return $stateParams.notaID;
                    }],
                    rFactura: ['$stateParams', 'facturaModel', function ($stateParams, facturaModel) {
                        return facturaModel.showNota({
                            clienteID: $stateParams.id,
                            notaID: $stateParams.notaID
                        }).$promise;
                    }],
                    rOperation: function () {
                        return 1027;
                    },
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'detalleNota';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            //**** Rutas del modulo de Histórico de Tarifas del Detalle de Las Condiciones****/
            .state("admin.detalle_medio.historico_tarifas", {
                url: "/historico_tarifas",
                templateUrl: "modules/medios/views/medios/historico_tarifas.html?v=" + CONSTANTES.VERSION,
                controller: "historicoTarifasMedioListController",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_medio/historico_tarifas';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            //############################# - Listado de negociaciones
            .state('admin.detalle_medio.negociaciones', {
                url: '/negociaciones',
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "NegociacionListController",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_medio/negociaciones';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            //Agregar Nueva Negociación
            .state('admin.detalle_medio.negociacion_add', {
                url: '/negociacion_add',
                onEnter: ['$state', '$modal', 'clientID', function ($state, $modal, clientID) {
                    $modal.open({
                        templateUrl: "modules/negociaciones/views/wizard.html?v=" + CONSTANTES.VERSION,
                        controller: "NegociacionModalController",
                        size: 'lg',
                        resolve: {
                            rMedio: ['Medio', function (Medio) {
                                return Medio.children({id: clientID}).$promise;
                            }],
                            rMedioRIF: ['Medio', function (Medio) {
                                return Medio.get({id: clientID}).$promise;
                            }]
                        }
                    }).result.finally(function () {
                            $state.go('^');
                        });
                }]
            })

            // Ver Negociación
            .state('admin.detalle_medio.negociacion_view', {
                url: '/negociacion_view/:negociacion_id',
                views: {
                    temp: {
                        templateUrl: "modules/negociaciones/views/steps/finalizar.html?v=" + CONSTANTES.VERSION,
                        controller: "NegociacionTabController"
                    }
                },
                resolve: {
                    rNegociacion: ['$stateParams', 'clientID', 'NegociacionModel', function ($stateParams, clientID, NegociacionModel) {
                        return NegociacionModel.get({
                            medio_id: clientID,
                            negociacion_id: $stateParams.negociacion_id
                        }).$promise;
                    }],
                    resolveTabId: function () {
                        return 12173;
                    }
                }
            })

            .state("admin.editar_medio", {
                url: "/editar_medio/:id",
                templateUrl: "modules/medios/views/medios/editar.html?v=" + CONSTANTES.VERSION,
                controller: "EditarMedioController",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }],
                    r_tipos: ["Medio", function (Medio) {
                        return Medio.tipos().$promise.then(function (resultado) {
                            return resultado;
                        });
                    }],
                    r_medios: ["Medio", function (Medio) {
                        return Medio.listar(null).$promise.then(function (resultado) {
                            return resultado;
                        });
                    }]
                }
            })
            .state("admin.crear_medio", {
                url: "/crear_medio",
                templateUrl: "modules/medios/views/medios/crear.html?v=" + CONSTANTES.VERSION,
                controller: "CrearMedioController",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }],
                    r_cargos: ["CargoMdl", function (CargoMdl) {
                        return CargoMdl.query();
                    }],
                    r_tipos: ["Medio", function (Medio) {
                        return Medio.tipos();
                    }],
                    r_medios: ["Medio", function (Medio) {
                        return Medio.listar(null).$promise.then(function (resultado) {
                            return resultado;
                        });
                    }]
                }
            })
            .state("admin.detalle_medio.contactos", {
                url: "/detalle_contacto",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "listarContactosMedio",
                resolve: {
                    particularPermission: ["permission", function (permission) {
                        var _url = 'admin/detalle_medio/contactos';
                        return permission.has_permission(_url);
                    }],
                    resolveTabId: function () {
                        return 12102;
                    }
                }
            })
            .state("admin.detalle_medio.contacto", {
                url: "/contacto/:idcont",
                views: {
                    temp: {
                        templateUrl: "modules/medios/views/contactos/detalles.html?v=" + CONSTANTES.VERSION,
                        controller: "DetallesContactoController"
                    }
                },
                resolve: {
                    resolveTabId: function () {
                        return 12103;
                    }
                }
            })
            .state("admin.detalle_medio.contact", {
                url: "/editcontacto/:idcont",
                templateUrl: "modules/medios/views/contactos/editar.html?v=" + CONSTANTES.VERSION,
                controller: "EditarContactoMedioController",
                resolve: {
                    particularPermission: ["permission", function (permission) {
                        var _url = 'admin/detalle_medio/contacto';
                        return permission.has_permission(_url);
                    }],
                    resolveTabId: function () {
                        return 12104;
                    }
                }
            })
            .state('admin.detalle_medio.contacto_add', {
                url: "/contacto_add",
                onEnter: ['$state', '$modal', function ($state, $modal) {
                    $modal.open({
                        animation: true,
                        //backdrop: 'static',
                        templateUrl: 'modules/medios/views/contactos/crear.html?v=' + CONSTANTES.VERSION,
                        controller: 'CrearContactoMedioController',
                        size: 'lg',
                        resolve: {
                            datos: function () {
                                return {
                                    'medioid': $state.params.id
                                };
                            },
                            r_cargos: ["CargoMdl", function (CargoMdl) {
                                return CargoMdl.query();
                            }]
                        }
                    }).result.finally(function () {
                            $state.go('admin.detalle_medio');
                            //crudFactory.reloadData();
                        });
                }]
            })

            //************************* Rutas del modulo de Cotizaciones ***************************/
            .state("admin.detalle_cliente.cotizacion_list", {
                url: "/cotizacion_list",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "CotizacionCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/cotizacion_list';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.detalle_cliente.crear_cotizacion", {
                url: "/crear_cotizacion",
                onEnter: ['$state', '$modal', function ($state, $modal) {
                    $modal.open({
                        animation: true,
                        //backdrop: 'static',
                        templateUrl: "modules/cotizaciones/views/form_cliente_ficha.html?v=" + CONSTANTES.VERSION,
                        controller: "CotizacionClienteFormCtrlFicha",
                        size: 'lg',
                        resolve: {
                            datos: ["clienteMdl", "$stateParams", function (clienteMdl, $stateParams) {
                                return clienteMdl.data({id: $stateParams.id}).$promise.then(function (response) {
                                    return response.data;
                                });
                            }]
                        }
                    }).result.then(function (evento) {
                            if (!evento) {
                                $state.go('admin.detalle_cliente');
                            }
                            //crudFactory.reloadData();
                        });
                }]
            })
            .state("admin.cotizacion", {
                url: "/cotizacion",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "CotizacionCtrl",
                resolve: {
                    module_permission: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.cotizacion_aprobada", {
                url: "/cotizacion_aprobada",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "CotizacionAprobadasCtrl",
                resolve: {
                    module_permission: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.cotizacion_crear", {
                url: "/cotizacion_crear/:cliente_id",
                controller: "CotizacionCrearCtrl"
            })
            .state("admin.cotizacion_editar", {
                url: "/cotizacion_editar/:cotizacion_id/panel",
                templateUrl: "modules/cotizaciones/views/form_cotizacion.html?v=" + CONSTANTES.VERSION,
                controller: "CotizacionFormCtrl",
                resolve: {
                    cotizacionData: ["Cotizacion", "$stateParams", function (Cotizacion, $stateParams) {
                        return Cotizacion.get({id: $stateParams.cotizacion_id}).$promise;
                    }],
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/cotizacion_editar';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.cotizacion_producto", {
                url: "/cotizacion_producto",
                templateUrl: "modules/cotizaciones/views/form_producto.html?v=" + CONSTANTES.VERSION,
                controller: "CotizacionProductoFormCtrl"
            })
            .state("admin.cotizacion_eliminar_producto", {
                url: "/cotizacion_eliminar_producto/:id",
                controller: "CotizacionEliminarProductoCtrl",
                resolve: {
                    cotizacionId: ["globalData", function (globalData) {
                        return globalData.data.cotizacion_id;
                    }]
                }
            })
            .state("admin.cotizacion_eliminar_recurso", {
                url: "/cotizacion_eliminar_recurso/:id",
                controller: "CotizacionEliminarRecursoCtrl",
                resolve: {
                    cotizacionId: ["globalData", function (globalData) {
                        return globalData.data.cotizacion_id;
                    }]
                }
            })
            .state("admin.cotizacion_cliente", {
                url: "/cotizacion_cliente",
                templateUrl: "modules/cotizaciones/views/form_cliente.html?v=" + CONSTANTES.VERSION,
                controller: "CotizacionClienteFormCtrl"
            })
            .state("admin.cotizacion_facturar_ver", {
                url: "/cotizacion_facturar_ver/:id",
                templateUrl: "modules/cotizaciones/views/view_cotizacion.html?v=" + CONSTANTES.VERSION,
                controller: "CotizacionVerCtrl"
            })
            .state("admin.cotizacion_recurso", {
                url: "/cotizacion_recurso",
                templateUrl: "modules/cotizaciones/views/form_recurso.html?v=" + CONSTANTES.VERSION,
                controller: "CotizacionRecursoFormCtrl"
            })

            .state('admin.cotizacion_editar.cotizacion_aprobar', {
                url: '/cotizacion_aprobar',
                params: {data: 'data'},
                onEnter: ['$state', '$modal', '$stateParams', function ($state, $modal, $stateParams) {
                    $modal.open({
                        templateUrl: "modules/cotizaciones/views/aprobar_cotizacion.html?v=" + CONSTANTES.VERSION,
                        controller: "AprobarCotizacionModalController",
                        size: 'lg',
                        resolve: {
                            rCotizacion: function () {
                                return $stateParams.data;
                            }
                        }
                    }).result.finally(function () {
                            $state.go('.^');
                        });
                }]
            })

            //*********************Publicaciones / Pautas **************************/
            .state("admin.detalle_cliente.publicacion_incidencias", {
                url: "/publicacion_incidencias",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "listarAllIncidencias",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/detalle_cliente/publicacion_incidencias';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.listar_publicaciones", {
                url: "/listar_publicaciones",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "PublicacionCtrl",
                resolve: {
                    module_permission: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.publicaciones_cotizacion", {
                url: "/publicaciones_cotizacion/:id",
                templateUrl: "modules/publicaciones/views/listar_publicaciones_cotizacion.html?v=" + CONSTANTES.VERSION,
                controller: "PublicacionCotizacionCtrl",
                resolve: {
                    module_permission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/publicaciones_cotizacion';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    cotizacionData: ["Cotizacion", "$stateParams", function (Cotizacion, $stateParams) {
                        return Cotizacion.get({id: $stateParams.id}).$promise;
                    }],
                    resolveTabId: function () {
                        return null;
                    }
                }
            })

            .state("admin.editar_publicacion", {
                url: "/editar_publicacion/:publicacion_id",
                templateUrl: "modules/publicaciones/views/form_publicacion.html?v=" + CONSTANTES.VERSION,
                controller: "PublicacionFormCtrl",
                resolve: {
                    module_permission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/editar_publicacion';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    publicacionData: ["Publicacion", "$stateParams", function (Publicacion, $stateParams) {
                        return Publicacion.get({id: $stateParams.publicacion_id}).$promise;
                    }],
                    dataIncidencias: ["TipoIncidenciaModel", function (TipoIncidenciaModel) {
                        return TipoIncidenciaModel.tipos_incidencias_dt().$promise;
                    }]
                }
            })
            .state("admin.editar_publicacion_cotizacion", {
                url: "/editar_publicacion_cotizacion/:publicacion_id",
                templateUrl: "modules/publicaciones/views/form_publicacion.html?v=" + CONSTANTES.VERSION,
                controller: "PublicacionFormCtrl",
                resolve: {
                    module_permission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/editar_publicacion';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }],
                    publicacionData: ["Publicacion", "$stateParams", function (Publicacion, $stateParams) {
                        return Publicacion.get({id: $stateParams.publicacion_id}).$promise;
                    }],
                    dataIncidencias: ["TipoIncidenciaModel", function (TipoIncidenciaModel) {
                        return TipoIncidenciaModel.tipos_incidencias_dt().$promise;
                    }]
                }
            })
            .state("admin.publicacion_cliente_medio", {
                url: "/publicacion_cliente_medio",
                templateUrl: "modules/publicaciones/views/form_cliente_medio.html?v=" + CONSTANTES.VERSION,
                controller: "publicacionMedioFormCtrl"
            })
            .state("admin.publicacion_crear", {
                url: "/publicacion_crear/:cliente_id/:medio_id",
                controller: "PublicacionCrearCtrl"
            })

            .state('admin.editar_publicacion_cotizacion.aprobar', {
                url: '/aprobar',
                params: {data: 'data'},
                onEnter: ['$state', '$modal', '$stateParams', function ($state, $modal, $stateParams) {
                    $modal.open({
                        templateUrl: "modules/publicaciones/views/publicar_version.html?v=" + new Date().getTime(),
                        //templateUrl: "modules/publicaciones/views/publicar_version.html?v=" + CONSTANTES.VERSION,
                        controller: "AprobarCotizacionModalController",
                        size: 'lg',
                        resolve: {
                            rCotizacion: function () {
                                return $stateParams.data;
                            }
                        }
                    }).result.finally(function () {
                            $state.go('.^', {}, {'reload': true});
                        });
                }]
            })

            .state('admin.editar_publicacion.aprobar', {
                url: '/aprobar',
                params: {data: 'data'},
                onEnter: ['$state', '$modal', '$stateParams', function ($state, $modal, $stateParams) {
                    $modal.open({
                        templateUrl: "modules/publicaciones/views/publicar_version.html?v=" + new Date().getTime(),
                        //templateUrl: "modules/publicaciones/views/publicar_version.html?v=" + CONSTANTES.VERSION,
                        controller: "AprobarCotizacionModalController",
                        size: 'lg',
                        resolve: {
                            rCotizacion: function () {
                                return $stateParams.data;
                            }
                        }
                    }).result.finally(function () {
                            $state.go('.^', {}, {'reload': true});
                        });
                }]
            })

            //****************-Rutas Modulo Productos-**************/
            .state("admin.positions", {
                url: "/positions",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "positionList",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.position/add", {
                url: "/position/add",
                templateUrl: "modules/clientes/views/position_form.html?v=" + CONSTANTES.VERSION,
                controller: "positionAdmin",
                resolve: {
                    rPosition: function () {
                        return false;
                    }
                }
            })
            .state("admin.position/edit/:id", {
                url: "/position/edit/:id",
                templateUrl: "modules/clientes/views/position_form.html?v=" + CONSTANTES.VERSION,
                controller: "positionAdmin",
                resolve: {
                    rPosition: ["$stateParams", "positionModel", function ($stateParams, positionModel) {
                        return positionModel.get($stateParams.id);
                    }]
                }
            })

            .state("admin.sectors", {
                url: "/sectors",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "sectorList",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            //****************************** Rutas modulo Administración                                 *
            .state("admin.list_user", {
                url: "/list_user",
                templateUrl: "modules/administracion/views/list_user.html",
                controller: "crudListUser",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            }).state("admin.create_user", {
                url: "/create_user",
                templateUrl: "modules/administracion/views/gestion_usuario.html?v=" + CONSTANTES.VERSION,
                controller: "controlUser",
                resolve: {
                    resolve_user: ["data_source_admin", function (data_source_admin) {
                        return data_source_admin.resolve_user('undefined', 'create');
                    }],
                    r_clientes: ['clienteMdl', function (clienteMdl) {
                        // Buscar como condicionar esta ejecución
                        return clienteMdl.list().$promise.then(function (resultado) {
                            return resultado[0].data;
                        });
                    }]
                }
            }).state("admin.edit_user/:id", {
                url: "/edit_user/:id",
                templateUrl: "modules/administracion/views/gestion_usuario.html?v=" + CONSTANTES.VERSION,
                controller: "controlUser",
                resolve: {
                    resolve_user: ["data_source_admin", "$stateParams", function (data_source_admin, $stateParams) {
                        return data_source_admin.resolve_user($stateParams.id, 'edit');
                    }],
                    r_clientes: ['clienteMdl', function (clienteMdl) {
                        // Buscar como condicionar esta ejecución
                        return clienteMdl.list().$promise.then(function (resultado) {
                            return resultado[0].data;
                        });
                    }]
                }
            }).state("admin.list_group", {
                url: "/list_group",
                templateUrl: "modules/administracion/views/list_group.html?v=" + CONSTANTES.VERSION,
                controller: "crudListGroup",
                resolve: {
                    resolve_group: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            }).state("admin.edit_group/:id", {
                url: "/edit_group/:id",
                templateUrl: "modules/administracion/views/gestion_grupo.html?v=" + CONSTANTES.VERSION,
                controller: "controlGroup",
                resolve: {
                    resolve_group: ["data_source_admin", "$stateParams", function (data_source_admin, $stateParams) {
                        return data_source_admin.resolve_group($stateParams.id);
                    }]
                }
            }).state("admin.create_group", {
                url: "/create_group",
                templateUrl: "modules/administracion/views/gestion_grupo.html?v=" + CONSTANTES.VERSION,
                controller: "controlGroup",
                resolve: {
                    resolve_group: ["data_source_admin", function (data_source_admin) {
                        return data_source_admin.resolve_group();
                    }]
                }
            }).state("admin.group_operation/:id", {
                url: "/group_operation/:id",
                templateUrl: "modules/administracion/views/operaciones_de_grupo.html?v=" + CONSTANTES.VERSION,
                controller: "controlGroupOperation",
                resolve: {
                    resolve_group_operation: ["data_source_admin", "$stateParams", function (data_source_admin, $stateParams) {
                        return data_source_admin.resolve_group_operation($stateParams.id);
                    }]
                }
            }).state("admin.io_operation/:id", {
                url: "/io_operation/:id",
                templateUrl: "modules/administracion/views/io_grupo.html?v=" + CONSTANTES.VERSION,
                controller: "controlIoOperation",
                resolve: {
                    resolve_io_operation: ["data_source_admin", "$stateParams", function (data_source_admin, $stateParams) {
                        return data_source_admin.resolve_io_operation($stateParams.id);
                    }]
                }
            })

            .state("admin.cambiar_contrasena", {
                url: "/cambiar_contrasena",
                templateUrl: "modules/administracion/views/form_contrasena.html?v=" + CONSTANTES.VERSION,
                controller: "cambiarContrasenaCtrl"
            })
            .state("admin.olvido_contrasena", {
                url: "/olvido_contrasena",
                templateUrl: "modules/administracion/views/form_olvido_contrasena.html?v=" + CONSTANTES.VERSION,
                controller: "olvidoContrasenaCtrl"
            })
            .state("admin.reset_password/:key", {
                url: "/reset_password/:key",
                templateUrl: "modules/administracion/views/form_reset_password.html?v=" + CONSTANTES.VERSION,
                controller: "resetPasswordCtrl"
            })

            .state("admin.system_audit", {
                url: "/system_audit",
                templateUrl: "modules/administracion/views/form_system_audit.html?v=" + CONSTANTES.VERSION,
                controller: "systemAuditCtrl",
                resolve: {
                    permisos: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.system_audit_view/:id", {
                url: "/system_audit_view/:id",
                templateUrl: "modules/administracion/views/form_system_audit_view.html?v=" + CONSTANTES.VERSION,
                controller: "systemAuditViewCtrl"
            })

            .state("admin.condiciones", {
                url: "/condiciones",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "ConditionsListController",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.condiciones/add", {
                url: "/condiciones/add",
                templateUrl: "modules/conditions/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "ConditionsFormController",
                resolve: {
                    TipoPublicacionResolve: ['TipoPublicacionModel', function (TipoPublicacionModel) {
                        return TipoPublicacionModel.query().$promise.then(function (response) {
                            return response[0].data;
                        });
                    }],
                    // Vacío xq la opción es de crear.
                    ConditionResolve: function () {
                        return {};
                    }
                }
            })
            .state("admin.condiciones/update", {
                url: "/condiciones/update/:id",
                templateUrl: "modules/conditions/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "ConditionsFormController",
                resolve: {
                    TipoPublicacionResolve: ['TipoPublicacionModel', function (TipoPublicacionModel) {
                        return TipoPublicacionModel.query().$promise.then(function (response) {
                            return response[0].data;
                        });
                    }],
                    ConditionResolve: ['$stateParams', 'CondicionModel', function ($stateParams, CondicionModel) {
                        return CondicionModel.get({id: $stateParams.id}).$promise.then(function (response) {
                            return response.data;
                        });
                    }]
                }
            })
            .state("admin.condiciones/view", {
                url: "/condiciones/view/:id",
                templateUrl: "modules/conditions/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "ConditionsFormController",
                resolve: {
                    TipoPublicacionResolve: ['TipoPublicacionModel', function (TipoPublicacionModel) {
                        return TipoPublicacionModel.query().$promise.then(function (response) {
                            return response[0].data;
                        });
                    }],
                    ConditionResolve: ['$stateParams', 'CondicionModel', function ($stateParams, CondicionModel) {
                        return CondicionModel.get({id: $stateParams.id}).$promise.then(function (response) {
                            return response.data;
                        });
                    }]
                }
            })
            .state("admin.TiposPublicaciones", {
                url: "/TiposPublicaciones",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "TipoPublicacionListController",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.TiposPublicaciones/add", {
                url: "/TiposPublicaciones/add",
                templateUrl: "modules/tipos_publicaciones/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "TipoPublicacionController",
                resolve: {
                    // Vacío xq la opción es de crear.
                    TipoPublicacionResolve: function () {
                        return {};
                    }
                }
            })
            .state("admin.TiposPublicaciones/update", {
                url: "/TiposPublicaciones/update/:id",
                templateUrl: "modules/tipos_publicaciones/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "TipoPublicacionController",
                resolve: {
                    TipoPublicacionResolve: ['$stateParams', 'TipoPublicacion', function ($stateParams, TipoPublicacion) {
                        return TipoPublicacion.get({id: $stateParams.id}).$promise.then(function (response) {
                            return response.data;
                        });
                    }]
                }
            })
            .state("admin.TiposPublicaciones/view", {
                url: "/TiposPublicaciones/view/:id",
                templateUrl: "modules/tipos_publicaciones/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "TipoPublicacionController",
                resolve: {
                    TipoPublicacionResolve: ['$stateParams', 'TipoPublicacion', function ($stateParams, TipoPublicacion) {
                        return TipoPublicacion.get({id: $stateParams.id}).$promise.then(function (response) {
                            return response.data;
                        });
                    }]
                }
            })

            .state("admin.TiposIncidencias", {
                url: "/TiposIncidencias",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "TipoIncidenciaListController",
                resolve: {
                    resolve_client: ["permission", "$location", function (permission, $location) {
                        return permission.has_permission($location.path());
                    }]
                }
            })
            .state("admin.TiposIncidencias/add", {
                url: "/TiposIncidencias/add",
                templateUrl: "modules/tipos_incidencias/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "TipoIncidenciaController",
                resolve: {
                    // Vacío xq la opción es de crear.
                    TipoIncidenciaResolve: function () {
                        return {};
                    },
                    TipoPublicacionResolve: ['TipoPublicacion', function (TipoPublicacion) {
                        return TipoPublicacion.query().$promise.then(function (response) {
                            return response;
                        });
                    }]
                }
            })
            .state("admin.TiposIncidencias/update", {
                url: "/TiposIncidencias/update/:id",
                templateUrl: "modules/tipos_incidencias/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "TipoIncidenciaController",
                resolve: {
                    TipoIncidenciaResolve: ['$stateParams', 'TipoIncidenciaModel', function ($stateParams, TipoIncidenciaModel) {
                        return TipoIncidenciaModel.get({id: $stateParams.id}).$promise.then(function (response) {
                            return response.data;
                        });
                    }],
                    TipoPublicacionResolve: ['TipoPublicacion', function (TipoPublicacion) {
                        return TipoPublicacion.query().$promise.then(function (response) {
                            return response;
                        });
                    }]
                }
            })
            .state("admin.TiposIncidencias/view", {
                url: "/TiposIncidencias/view/:id",
                templateUrl: "modules/tipos_incidencias/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "TipoIncidenciaController",
                resolve: {
                    TipoIncidenciaResolve: ['$stateParams', 'TipoIncidenciaModel', function ($stateParams, TipoIncidenciaModel) {
                        return TipoIncidenciaModel.get({id: $stateParams.id}).$promise.then(function (response) {
                            return response.data;
                        });
                    }],
                    TipoPublicacionResolve: ['TipoPublicacion', function (TipoPublicacion) {
                        return TipoPublicacion.query().$promise.then(function (response) {
                            return response;
                        });
                    }]
                }
            })

            .state("admin.iva", {
                url: "/iva",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "IvaListController",
                resolve: {
                    r_permisos: ["permission", function (permission) {
                        return permission.has_permission('admin/iva');
                    }]
                }
            })

            .state("admin.iva/add", {
                url: "/iva/add",
                templateUrl: "modules/iva/views/form_iva.html?v=" + CONSTANTES.VERSION,
                controller: "IvaFormController"
            })

            .state("admin.recargo_gestion", {
                url: "/recargo_gestion",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "RecargoGestionListController",
                resolve: {
                    r_permisos: ["permission", function (permission) {
                        return permission.has_permission('admin/recargo_gestion');
                    }]
                }
            })

            .state("admin.recargo_gestion/add", {
                url: "/recargo_gestion/add",
                templateUrl: "modules/recargo_gestion/views/form_recargo_gestion.html?v=" + CONSTANTES.VERSION,
                controller: "RecargoGestionFormController"
            })

            //********************************* Rutas modulo Configuración                                 *
            .state("admin.configuracion", {
                url: "/configuracion",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "GenericListController",
                resolve: {
                    r_permisos: ["permission", function (permission) {
                        return permission.has_permission('admin/configuracion');
                    }]
                }
            })

            .state("admin.configuracion_update", {
                url: "/configuracion_update/:id",
                templateUrl: "modules/generic_configuration/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "EditarGenericConfigurationController",
                resolve: {
                    configurationID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    r_data: ['$stateParams', 'genericconfiguracionModel', function ($stateParams, genericconfiguracionModel) {
                        var params = {
                            id: $stateParams.id
                        };
                        return genericconfiguracionModel.get(params).$promise;
                    }]
                }
            })

            .state("admin.operaciones", {
                url: "/operaciones",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "OperacionesListController",
                resolve: {
                    r_permissions: ["permission", function (permission) {
                        return permission.has_permission('admin/operaciones');
                    }]
                }
            })

            .state('admin.operaciones_add', {
                url: "/operaciones/add",
                templateUrl: "modules/operaciones/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "OperacionesFormController",
                resolve: {
                    rViewMode: function () {
                        return false;
                    },
                    rAction: function () {
                        return 'Agregar';
                    },
                    rModel: function () {
                        return false;
                    },
                    rOperations: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.query().$promise;
                    }], rIcons: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.icons().$promise;
                    }], rRenders: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.renders().$promise;
                    }], rVisuals: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.visuals().$promise;
                    }], rTargets: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.targets().$promise;
                    }]
                }
            })

            .state('admin.operaciones_edit', {
                url: "/operaciones/:id",
                templateUrl: "modules/operaciones/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "OperacionesFormController",
                resolve: {
                    rViewMode: function () {
                        return false;
                    },
                    rAction: function () {
                        return 'Actualizar';
                    },
                    rModel: ["$stateParams", 'OperacionesModel', function ($stateParams, OperacionesModel) {
                        return OperacionesModel.get({id: $stateParams.id}).$promise;
                    }],
                    rOperations: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.query().$promise;
                    }], rIcons: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.icons().$promise;
                    }], rRenders: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.renders().$promise;
                    }], rVisuals: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.visuals().$promise;
                    }], rTargets: ['OperacionesModel', function (OperacionesModel) {
                        return OperacionesModel.targets().$promise;
                    }]
                }
            })

            //************************** Rutas modulo de reportes                                        *
            .state("admin.reportes", {
                url: "/reportes",
                templateUrl: "modules/administracion/views/reportes/filtro_reportes.html?v=" + CONSTANTES.VERSION,
                controller: "reportesCtrl",
                resolve: {
                    r_clientes: ['clienteMdl', function (clienteMdl) {
                        // Buscar como condicionar esta ejecución
                        return clienteMdl.list().$promise.then(function (resultado) {
                            return resultado[0].data;
                        });
                    }],
                    r_medios: ["Medio", function (Medio) {
                        return Medio.query().$promise.then(function (resultado) {
                            return resultado[0].data;
                        });
                    }],
                    r_tiposPublicacion: ["TipoPublicacion", function (TipoPublicacion) {
                        return TipoPublicacion.query();
                    }],

                    r_tiposReportes: function () {
                        return [
                            {id: 1, nombre: 'Reporte de Facturas '},
                            {id: 2, nombre: 'Reporte de Publicaciones'},
                            {id: 3, nombre: 'Reporte de Cotizaciones '},
                            {id: 4, nombre: 'Reporte de Facturas Pagadas'},
                            {id: 5, nombre: 'Reporte de Pagos'},
                            {id: 6, nombre: 'Reporte Incidencias'},
                            {id: 7, nombre: 'Reporte de Cotizaciones Aprobadas'},
                            {id: 8, nombre: 'Reporte de Pautas con Incidencia'},
                            {id: 9, nombre: 'Reporte de Contrato Consumido'},
                            {id: 10, nombre: 'Reporte de Facturas de Cuotas Anuladas '},
                            {id: 11, nombre: 'Reporte de Facturas de Publicaciones Anuladas'},
                            {id: 12, nombre: 'Reporte de Facturas de Cuotas con Nota de Crédito'},
                            {id: 13, nombre: 'Reporte de Facturas de Publicación con Nota de Crédito'}

                        ];
                    }
                }
            })

            //******************************** Rutas del modulo de localidad                          *
            //Pais
            .state("admin.pais", {
                url: "/pais",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "PaisCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/pais';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            .state("admin.pais_add", {
                url: "/pais_add",
                templateUrl: "modules/localidad/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "CrearPaisController"
            })

            .state("admin.pais_update", {
                url: "/pais_update/:id",
                templateUrl: "modules/localidad/views/editar_pais.html?v=" + CONSTANTES.VERSION,
                controller: "EditarPaisController",
                resolve: {
                    paisID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    r_data: ['$stateParams', 'paisModel', function ($stateParams, paisModel) {
                        var params = {
                            id: $stateParams.id
                        };
                        return paisModel.get(params).$promise;
                    }]
                }
            })
            //Estado
            .state("admin.estado", {
                url: "/estado",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "EstadoCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/estado';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.estado_add", {
                url: "/estado_add",
                templateUrl: "modules/localidad/views/estado/form.html?v=" + CONSTANTES.VERSION,
                controller: "CrearEstadoController",
                resolve: {
                    r_pais: ["paisModel", function (paisModel) {
                        return paisModel.get().$promise;
                    }]
                }
            })

            .state("admin.estado_update", {
                url: "/estado_update/:id",
                templateUrl: "modules/localidad/views/estado/editar_estado.html?v=" + CONSTANTES.VERSION,
                controller: "EditarEstadoController",
                resolve: {
                    estadoID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    r_data: ['$stateParams', 'estadoModel', function ($stateParams, estadoModel) {
                        var params = {
                            id: $stateParams.id
                        };
                        return estadoModel.get(params).$promise;
                    }],
                    r_pais: ["paisModel", function (paisModel) {
                        return paisModel.get().$promise;
                    }]
                }
            })
            //Municipio
            .state("admin.municipio", {
                url: "/municipio",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "MunicipioCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/municipio';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })
            .state("admin.municipio_add", {
                url: "/municipio_add",
                templateUrl: "modules/localidad/views/municipio/form.html?v=" + CONSTANTES.VERSION,
                controller: "CrearMunicipioController",
                resolve: {
                    r_estado: ["estadoModel", function (estadoModel) {
                        return estadoModel.get().$promise;
                    }]
                }
            })

            .state("admin.municipio_update", {
                url: "/municipio_update/:id",
                templateUrl: "modules/localidad/views/municipio/editar_municipio.html?v=" + CONSTANTES.VERSION,
                controller: "EditarMunicipioController",
                resolve: {
                    municipioID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    r_data: ['$stateParams', 'municipioModel', function ($stateParams, municipioModel) {
                        var params = {
                            id: $stateParams.id
                        };
                        return municipioModel.get(params).$promise;
                    }],
                    r_estado: ["estadoModel", function (estadoModel) {
                        return estadoModel.get().$promise;
                    }]
                }
            })

            //REGIMEN DE PAGO
            .state("admin.regimen_pago", {
                url: "/regimen_pago",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "RegimenCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/regimen_pago';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            .state("admin.regimen_pago_add", {
                url: "/regimen_pago_add",
                templateUrl: "modules/regimen_pago/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "CrearRegimenController"
            })

            .state("admin.regimen_pago_update", {
                url: "/regimen_pago_update/:id",
                templateUrl: "modules/regimen_pago/views/editar_regimen.html?v=" + CONSTANTES.VERSION,
                controller: "EditarRegimenController",
                resolve: {
                    regimenID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    r_data: ['$stateParams', 'regimenModel', function ($stateParams, regimenModel) {
                        var params = {
                            id: $stateParams.id
                        };
                        return regimenModel.get(params).$promise;
                    }]
                }
            })

            //RECEPCIÓN DE ORDEN DE SERVICIO
            .state("admin.orden_servicio", {
                url: "/orden_servicio",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "RecepcionCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/orden_servicio';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            .state("admin.orden_servicio_add", {
                url: "/orden_servicio_add",
                templateUrl: "modules/recepcion_orden/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "CrearRecepcionController"
            })

            .state("admin.orden_servicio_update", {
                url: "/orden_servicio_update/:id",
                templateUrl: "modules/recepcion_orden/views/editar_recepcion.html?v=" + CONSTANTES.VERSION,
                controller: "EditarRecepcionController",
                resolve: {
                    recepcionID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    r_data: ['$stateParams', 'recepcionModel', function ($stateParams, recepcionModel) {
                        var params = {
                            id: $stateParams.id
                        };
                        return recepcionModel.get(params).$promise;
                    }]
                }
            })

            //CIRCULACIÓN_SERVICIO
            .state("admin.circulacion", {
                url: "/circulacion",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "CirculacionCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/circulacion';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            .state("admin.circulacion_add", {
                url: "/circulacion_add",
                templateUrl: "modules/circulacion/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "CrearCirculacionController"
            })

            .state("admin.circulacion_update", {
                url: "/circulacion_update/:id",
                templateUrl: "modules/circulacion/views/editar_circulacion.html?v=" + CONSTANTES.VERSION,
                controller: "EditarCirculacionController",
                resolve: {
                    circulacionID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    r_data: ['$stateParams', 'circulacionModel', function ($stateParams, circulacionModel) {
                        var params = {
                            id: $stateParams.id
                        };
                        return circulacionModel.get(params).$promise;
                    }]
                }
            })
            //TARGET_SERVICIO
            .state("admin.target", {
                url: "/target",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "TargetCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/target';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            .state("admin.target_add", {
                url: "/target_add",
                templateUrl: "modules/target/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "CrearTargetController"
            })

            .state("admin.target_update", {
                url: "/target_update/:id",
                templateUrl: "modules/target/views/editar_target.html?v=" + CONSTANTES.VERSION,
                controller: "EditarTargetController",
                resolve: {
                    targetID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    r_data: ['$stateParams', 'targetModel', function ($stateParams, targetModel) {
                        var params = {
                            id: $stateParams.id
                        };
                        return targetModel.get(params).$promise;
                    }]
                }
            })
            //REDES SOCIALES
            .state("admin.red_social", {
                url: "/red_social",
                templateUrl: "views/template_custom/template_crud.html?v=" + CONSTANTES.VERSION,
                controller: "RedSocialCtrlC",
                resolve: {
                    particularPermission: ["permisosModel", function (permisosModel) {
                        var _url = 'admin/red_social';
                        return permisosModel.has_permission({operation: _url}).$promise;
                    }]
                }
            })

            .state("admin.red_social_add", {
                url: "/red_social_add",
                templateUrl: "modules/redes_sociales/views/form.html?v=" + CONSTANTES.VERSION,
                controller: "CrearRedSocialController"
            })

            .state("admin.red_social_update", {
                url: "/red_social_update/:id",
                templateUrl: "modules/redes_sociales/views/editar_redSocial.html?v=" + CONSTANTES.VERSION,
                controller: "EditarRedSocialController",
                resolve: {
                    redSocialID: ['$stateParams', function ($stateParams) {
                        return $stateParams.id;
                    }],
                    r_data: ['$stateParams', 'redSocialModel', function ($stateParams, redSocialModel) {
                        var params = {
                            id: $stateParams.id
                        };
                        return redSocialModel.get(params).$promise;
                    }]
                }
            })

            //**************************** Muestra todas las notificaciones                           *
            .state("admin.notificaciontodo", {
                url: "/notificaciontodo",
                params: {data: 'data', todos: 'todos'},
                controller: "ContControllerNotificacionTodo",
                templateUrl: "views/notificaciontodo.html?v=" + CONSTANTES.VERSION,
                resolve: {
                    vertodosdata: ['$stateParams', function ($stateParams) {
                        return $stateParams.todos;
                    }]
                }
            })

    }]).run(["$injector", function ($injector) {
        var $rootScope = $injector.get('$rootScope', 'run');
        var $location = $injector.get('$location', 'run');
        var $window = $injector.get('$window', 'run');

        $window.ga('create', 'UA-57305668-9', 'auto');

        /**
         * Método que se encarga de consultar el estado de la sesión el el servidor a través del factory AuthService.
         * Actualiza el estado de la variable $rootScope.logged según el estado de la sesión en el servidor.
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 14/01/15
         */
        var checkLogin = function () {
            $injector.get('AuthService', 'run').checkLogin().then(
                function (response) {
                    if (response.status != undefined && (response.status != '200')) {
                        $injector.get('$location', 'run').path('/login');
                    }
                    $rootScope.$broadcast($injector.get('AUTH_EVENTS', 'run').authChecked);
                }
            );
        };

        /**
         * Dispara un evento al inicio del cambio en una url.
         *
         * @author Darwin Serrano <darwinserrano@gmail.com>
         * @version V1.0 14/01/15
         */
        $rootScope.$on('$stateChangeStart', function (event, toState) {
            $window.ga('send', 'pageview', $location.path());
            //console.log(event, toState, toParams, fromState, fromParams);
            // Con cada cambio de estado en la url se verifica el estado de la sesión.
            if (toState.name != 'olvido_contrasena') {
                checkLogin();
            }
        });

        // Para ver errores.
        $rootScope.$on("$stateChangeError", console.log.bind(console));
    }]);

app.constant('CONSTANTES', {
    VERSION: '1.0.1459266869509'
    //VERSION: new Date().getTime()
});