/**
 * El archivo contiene el base url correspondiente al servidor que se van a consumir la información
 *
 * @author Soluciones A.S.T
 * @since Version 1.0
 * @package Cotizaciones A.S.T
 */
app.factory("appConfig", ["$location", function ($location) {
    /**
     * Parámetro para colocar la url con segura, por defecto es no segura
     * @type {boolean}
     */
    var secure = false;

    /**
     * Dominio al cual se realizaran las peticiones a las apis
     * @type {string}
     */
    var domain = $location.host() + '';

    /**
     * Url donde se encuentran los servicios que va a consumir la aplicación
     * @type {string}
     */
    var pathApi = domain + '/api/public';

    /**
     * Url donde se encuentran los servicios que va a consumir la aplicación, mientras esta integrada con CI.
     * @type {string}
     */
    var adminPathApi = domain + '/apiadmin';

    /**
     * Dominio considerado entorno de pruebas
     * @type {string}
     */
    var domainTest = 'test.cotizaciones.ast.com.ve';

    /**
     * Objeto publico de configuración
     * @type {{getPathApi: Function, getAdminPathApi: Function, isTestingMode: Function, printPDF: Function, appName: string, appDescription: string}}
     */
    var config = {
        /**
         * Función para configurar la rutas del sitio y las api.
         * @returns {string}
         */
        getPathApi: function () {
            var http = 'http://';
            if (secure) {
                http = 'https://';
            }
            if (pathApi.substring(pathApi.length - 1, pathApi.length) != '/') {
                pathApi += '/';
            }
            return http + pathApi;
        },

        /**
         * Esta función arma las rutas de la apsigavpel
         * @returns {string}
         */
        getAdminPathApi: function () {
            var http = 'http://';
            if (secure) {
                http = 'https://';
            }
            if (adminPathApi.substring(adminPathApi.length - 1, adminPathApi.length) != '/') {
                adminPathApi += '/';
            }
            return http + adminPathApi;
        },

        /**
         * Indica que la aplicación se encuentra en un entorno de pruebas.
         * @returns {boolean}
         */
        isTestingMode: function () {
            var domain = $location.host() + '';
            return domain == domainTest;
        },

        /**
         *
         * @param modelo
         * @param modelo_id
         * @returns {boolean}
         */
        printPDF: function (modelo, modelo_id) {
            if (!angular.isString(modelo) || !angular.isNumber(modelo_id)) {
                alert('Los parámetros no concuerdan con su tipo, printPDF(String, Integer)');
                return false;
            }
            var url = 'pdf/print/' + modelo + '/' + modelo_id;
            window.location.href = config.getPathApi() + url;
        },

        /**
         * Nombre de la aplicación
         */
        appName: 'SIGAVP',

        /**
         * Descripción base de la aplicación
         */
        appDescription: 'Sistema de Gestión Publicitaria de AVP'
    };

    return config;
}]);
